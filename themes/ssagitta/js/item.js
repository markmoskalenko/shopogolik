$(document).ready(function () {
    $('.minus').click(function () {
        var input = $('input[name="num"]');
        var count = parseInt(input.val()) - 1;
        count = count <= 1 ? 1 : count;
        input.val(count);
        setTimeout(function () {
            getUserPrice(false);
        }, 1500);
        return false;
    });
    $('.plus').click(function () {
        var input = $('input[name="num"]');
        var count = parseInt(input.val()) + 1;
        input.val(count);
        setTimeout(function () {
            getUserPrice(false);
        }, 1500);
        return false;
    });
})

function applySku(data) {
    if (data.sku !== undefined) {
        if (data.sku.price > data.sku.promotion_price) {
            $("#price").html(data.sku.userPromotionPrice + '&nbsp;<s>' + data.sku.userPrice + '</s>');
        } else {
            $("#price").html(data.sku.userPromotionPrice);
        }
        if (data.sku.count > 0) {
            $("#item_num").html(data.sku.count);
        } else {
            $("#item_num").html('&dash;&dash;&dash;');
        }
        $("#item_totalcount").val(data.sku.count);
        if (data.sku.count > 0) {
            $('.buy-btn').removeAttr('disabled');
        }
        $("#price_val").val(data.sku.promotion_price);
        getUserPrice(true);
    } else {
        $('.buy-btn').removeAttr('disabled');
    }
    /*                if (data.prop_img !== undefined) {
     var small_src = data.prop_img.url + '_310x310.jpg';
     $("#int_small_img").attr("src", small_src);
     $('#int_zoom').attr("href", data.prop_img.url);
     $("#int_zoomWrapperImage img").attr("src", data.prop_img.url);

     //                    $("#int_main_image").html();
     //                    $("#int_main_image").html('<img src="' + data.prop_img.url + '_310x310.jpg" alt="" />');
     //                    var image = '<a href="' + data.prop_img.url + '" onclick="showMain(this.href);return false;">\
     //                         <img src="' + data.prop_img.url + '_40x40.jpg" alt="" /></a>';
     //                    $("#small_images").html(html);
     }
     */
}

function reloadSku() {
    var iid = $("#iid").val();
    var count = $("#num").val();
//    var html = $("#small_images").html();
    var params = '';
    var error = false;
    $(".input_params").each(function () {
        if ($(this).val() !== '0') {
            params = params + $(this).val() + ';';
            $(this).removeClass('error');
        }
        else {
            error = true;
            $(".buy-btn").prop("disabled", true);
            $(this).addClass('error');
        }
    });
//-------------
    var inpProc = $("#inputprops-processed");
    var inputpropsProcessed = inpProc.val();
    if (inputpropsProcessed == params) {
        return false;
    }
    inpProc.val(params);
//-------------
    var url = '/item/getsku?iid=' + iid + '&params=' + params + '&count=' + count;

    if (!error) {
        $("#item-count-price").html('<img src="/images/ajax-loader.gif">');
        $("#item_num").html('<img src="/images/ajax-loader.gif">');
        setTimeout(function () {
            $.getJSON(url, function (data) {
                    applySku(data);
                    itemSKU = data;
                }
            )
        }, 750)
    }
}

function loadInputProps() {
    // console.log('444');
    $("#item-input-props").html('<div id="ajax-loader"></div>');
    var iid = $("#iid").val();
    var url = 'http://' + document.domain + '/item/getinputprops?iid=' + iid;
    $.get(url, {
            data: 'html'
        },
        function (data) {
            //console.log(data);
            $("#item-input-props").html(data);
            //  $('.buy-btn').removeAttr('disabled');
        }
    );
}

function loadSellerRelatedBlock(nick, userid, iid) {
    $("#sellerrelated").html('<div id="ajax-loader"></div>');
    var url = 'http://' + document.domain + './item/sellerrelatedblock?nick=' + nick + '&userid=' + userid + '&iid=' + iid;
    $.get(url, function (data) {
        $("#sellerrelated").html(data);
    });
}

function loadItemDetails(iid) {
    $("#item-detail-block").html('<div id="ajax-loader"></div>');
    var url = '/item/detail?iid=' + iid;
    $.get(url, function (data) {
        $("#item-detail-block").html(data);
    });
}

function loadItemDetailsFromUrl(url) {
    $("#item-detail-block").html('<div id="ajax-loader"></div>');
    var url = 'http://' + document.domain + './item/detail?url=' + url;
    $.get(url, function (data) {
        $("#item-detail-block").html(data);
    });
}

function getUserPrice(force) {
    var count = $("#num").val();
    var numProc = $("#num-processed");
    var countProcessed = numProc.val();
    if ((countProcessed === count) && !force) {
        return false;
    }
    numProc.val(count);
    var iid = $("#iid").val();
    var price = $("#price_val").val();
    var url = 'http://' + document.domain + '/item/getuserprice?iid=' + iid + '&count=' + count + '&price=' + price;
    $("#item-count-price").html('<img src="/images/ajax-loader.gif">');
    $.get(url, {
            data: 'html'
        },
        function (data) {
            $("#item-count-price").html(data);
        }
    );
}

function loadItemDesc(iid) {
    var url = 'http://' + document.domain + '/item/detail?iid=' + iid;
    document.write('<div id="ajax-load"></div>');
    $.get(url, {
            data: 'html'
        },
        function (data) {
            $("#tab-4-content").html(data);
        }
    );
}

