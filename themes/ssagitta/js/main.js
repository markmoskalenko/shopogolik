//function loadCatalogMenu(lang) {
//    //var menuPlaceholder=$("#main-cats-menu-ajax");
//    var menuPlaceholder=$(".vmegamenu_content");
//    if (menuPlaceholder.length>0) {
////        $("#main-cats-menu-ajax").load("/category/menu");
//        $.ajax({
//            url: "/category/menu/lang/"+lang,
//            cache: true,
//            dataType: "html",
//            success: function(data) {
//                //console.log(data);
//                menuPlaceholder.html(data);
//            }
//        });
//    }
//};

function changeSortOrder(actn) {
//$("#sort_by").change(function() {
    //alert (action);
    var selection = $("#sort_by :selected").val();
    var action=actn;
    // sort_by/delistTime_desc
    if (action.match(/sort_by\/.+?(?=\/|$)/i)) {
        action= action.replace(/sort_by\/.+?(?=\/|$)/i,'sort_by/'+selection);
    } else {
        action=action+'/sort_by/'+selection;
    }
    //alert(action);
    $("#search-sort").attr("action", action);
    $('#search-sort').submit();
//    });
}
//$(function(){
//    loadCatalogMenu('ru');
//});

function clearCart() {
    if (confirm('Полностью очистить корзину?')) {
        var url = '/cart/deleteAll';
        //$.post(url,null,function(data){alert(data);},"text");
        var cartTable=document.getElementById("cart-table");
        if ((cartTable !== undefined) && (cartTable!=null)) {
            cartTable.style.visibility = "hidden";
        }
        var cartEmpty=document.getElementById("cart-empty");
        if (cartEmpty !== undefined && (cartEmpty!=null)) {
            cartEmpty.style.visibility = "visible";
        }
        $.post(url);
        location.reload();
    }
}

function addFavorite(addlink,id){
    var url=$(addlink).attr('href');
    if (typeof url == 'undefined') {
        url= $(addlink).attr('formaction');
    }
    $.get(url,null,
        function(data){
            alert(data);
        },
        "text");
}