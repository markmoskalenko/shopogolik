<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="payment.php">
 * </description>
 * Рендеринг страницы оплаты заказа при оформлении
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<form action="<?= Yii::app()->createUrl('/checkout/payment') ?>" method="post">
    <table id="cart_summary" class="table table-bordered">
        <thead>
        <tr>
            <th class="cart_product first_item">Товар</th>
            <th class="cart_description item">Описание</th>
            <th class="cart_avail item">Вес, грамм</th>
            <th class="cart_unit item">Цена</th>
            <th class="cart_quantity item">Количество</th>
            <th class="cart_total item">Сумма</th>
            <th class="cart_delete last_item">&nbsp;</th>
        </tr>
        </thead>

        <tfoot>
        <tr class="cart_total_price">
            <td rowspan="3" colspan="2" id="cart_voucher" class="cart_voucher">
                <br/>
                <span style="font-weight: bold;"><?= Yii::t('main', 'Способ доставки') ?>:</span>

                <?php $delivery = Deliveries::getDelivery(0, false, $data['delivery_id']);
                if (isset($delivery->name)) {
                    echo $delivery->name;
                } else {
                    echo Yii::t('main', ' Не определено');
                }?>
                <br/>
                <br/>
                <span style="font-weight: bold;"> <?= Yii::t('main', 'Адрес') ?>:</span>

                <?= $data['country_name'] ?>, <?= $data['index'] ?>, <?= $data['city'] ?>, <?= $data['address'] ?>,
                <?= isset($data['firstname']) ? $data['firstname'] : "" ?> <?= isset($data['lastname']) ? $data['lastname'] : "" ?>
                ,
                <?= isset($data['phone']) ? $data['phone'] : "" ?>
            </td>
            <td colspan="3" class="text-right">
                <input type="hidden" name="order_id" value="<?= $order_id ?>"/><?= Yii::t('main', 'Сумма за товары') ?>:</td>
            <td colspan="2" class="price" id="total_product"><?= Formulas::priceWrapper($cart->total) ?></td>
        </tr>
        <tr style="display: none;">
            <td colspan="3" class="text-right">
                Total gift-wrapping cost:											</td>
            <td colspan="2" class="price-discount price" id="total_wrapping">
                $0.00
            </td>
        </tr>
        <tr class="cart_total_delivery">
            <td colspan="3" class="text-right"><?= Yii::t('main', 'Сумма за доставку') ?>:</td>
            <td colspan="2" class="price" id="total_shipping"> <?=
                Formulas::priceWrapper(
                    Formulas::convertCurrency(
                        $data['delivery'],
                        DSConfig::getVal('site_currency'),
                        DSConfig::getCurrency()
                    )
                ) ?>
            </td>
        </tr>
        <tr class="cart_total_price">
            <td colspan="3" class="total_price_container text-right">
                <?= Yii::t('main', 'Итого к  оплате') ?>:
            </td>
            <td colspan="2" class="price" id="total_price_container">
                </strong><?=
                Formulas::priceWrapper(
                    (float) $cart->total + (float) Formulas::convertCurrency(
                        $data['delivery'],
                        DSConfig::getVal('site_currency'),
                        DSConfig::getCurrency()
                    )
                ) ?>
            </td>
        </tr>


        </tfoot>
        <div class="content payment">
            <?php foreach ($cart->cartRecords as $k => $item) { ?>
                <? $this->widget(
                    'application.components.widgets.OrderItem',
                    array(
                        'orderItem'   => $item,
                        'readOnly'    => true,
                        'allowDelete' => false,
                        'imageFormat' => '_200x200.jpg',
                    )
                );
                ?>
            <? } ?>
    </table>

    <?php if (Users::getBalance(Yii::app()->user->id) <
        Formulas::convertCurrency(
            $cart->total,
            DSConfig::getCurrency(),
            DSConfig::getVal('site_currency')
        ) + $data['delivery']
    ) {
        ?>
        <div class="payment-error">
            <p><?= Yii::t('main', 'На вашем счету недостаточно средств для оплаты заказа') ?>.</p>
            <br/>
            <input value='<?= Yii::t('main', 'Пополнить счёт') ?>'
                   onclick="location.href='<?=
                   Yii::app()->createUrl(
                       '/cabinet/balance/payment',
                       array(
                           'sum' => Formulas::convertCurrency(
                                   $cart->total,
                                   DSConfig::getCurrency(),
                                   DSConfig::getVal('site_currency')
                               ) +
                               $data['delivery'] - Users::getBalance(Yii::app()->user->id),
                           'r'   => '/checkout/payment'
                       )
                   )?>'"
                   class="button btn btn-default standard-checkout button-medium" type='button' style="padding:10px 25px 10px 25px"/>
        </div>
        <!--     <input type="submit" name="doGoN" value="<? //=Yii::t('main','Оплатить')?>" class="blue-btn bigger" />  -->
    <?php
    } else {
        ?>
        <div class="next-btn">
            <input type="submit" id="payment_button" name="doGo" value="<?= Yii::t('main', 'Оплатить сейчас') ?>"
                   class="button btn btn-default standard-checkout button-medium" style="padding:10px 25px 10px 25px"/>
        </div>
    <?php } ?>
    <? $checkout_payment_needed = DSConfig::getVal('checkout_payment_needed') == 1;
    if (!$checkout_payment_needed) {
        ?>
        <div class="next-btn">
            <input type="submit" id="nopayment_button" name="doGoNoPayment"
                   value="<?= Yii::t('main', 'Оплатить позднее') ?>"
                   class="button btn btn-default standard-checkout button-medium" style="padding:10px 25px 10px 25px"/>
        </div>
    <? } ?>
</form>
</div>