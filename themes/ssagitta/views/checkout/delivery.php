<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="delivery.php">
 * </description>
 * Выбор службы доставки при оформлении заказа
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<div class="content">
    <div class="order_carrier_content box">
        <h4><?= $this->pageTitle ?></h4>
        <? if (count($delivery) > 0) { ?>
            <form id="delivery" class="delivery" action="<?= Yii::app()->createUrl('/checkout/delivery') ?>" method="post">
                <table id="cart_summary" class="table table-bordered">
                    <? foreach ($delivery as $i => $del) {?>
                        <tr>
                            <td><input name="CheckoutForm[delivery]" value="<?= $del->id ?>" type="radio"/></td>
                            <td> <?= Yii::t('main', $del->name) ?>
                                <br/>
                                <div class="hint">
                                    <?= Yii::t('main', $del->description) ?>
                                </div>
                            </td>
                            <td class="delivery_option_price"> <? if ($del->summ > 0) {
                                    echo  Formulas::priceWrapper(
                                        Formulas::convertCurrency(
                                            $del->summ,
                                            DSConfig::getVal('site_currency'),
                                            DSConfig::getCurrency()
                                        )
                                    );
                                }?></td>

                        </tr>
                    <? } ?>
                </table>
                <div class="clear"></div>
            </form>
        <?
        } else {
            ?>
            <p>
                <b><?=
                    Yii::t(
                        'main',
                        'К сожалению, доставка в выбранную Вами страну или регион с указанными Вами параметрами временно не производится.'
                    ) ?></b>
            </p>
        <? } ?>
        <div class="delivery-more">
            <a target="_blank" href="<?= Yii::app()->createUrl('/article/raschet-stoimosti-dostavki') ?> ">
                <?= Yii::t('main', 'Подробнее об условиях доставки') ?>
            </a>
        </div>
    </div>
    <div class="next-btn">
        <input form="delivery" type="hidden" name="step" value="3"/>
        <?= CHtml::submitButton(Yii::t('main', 'Продолжить'), array('class' => 'button btn btn-default standard-checkout button-medium', 'form' => 'delivery','style'=>'padding:10px 25px 10px 25px')) ?>
    </div>
</div>