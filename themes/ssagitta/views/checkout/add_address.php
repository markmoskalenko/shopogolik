<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="add_address.php">
 * </description>
 * Рендеринг формы создания нового адреса доставки
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<div class="page-title"><h4><?= Yii::t('main', 'Оформление заказа') ?></h4></div>
<div class="content">
    <div class="box">

    <h1 class="page-subheading"><?= $this->pageTitle ?></h1>
        <div class="form">
        <?= $form ?>
        </div>
    </div>
</div>