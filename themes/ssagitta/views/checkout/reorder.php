<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="reorder.php">
 * </description>
 * Выбор заказа\дозаказа при оформлении
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<div class="content">
    <div class="box">
        <h4><?= $this->pageTitle ?></h4>
        <hr/>
        <? //=================================================================================?>
        <div class="form reorder">
            <form action="<?= $this->createUrl('checkout/reorder') ?>" method="post">
                <div class="row" style="padding-left: 13px;" >
                    <label>
                        <input type="radio" name="order" value="0" checked/><?= Yii::t('main', 'Создать новый заказ') ?>
                    </label>
                </div>
                <label style="margin-left: 25px;"><?= Yii::t('main', 'или добавить в существующий') ?></label>

                <div class="clear"></div>
                <?
                $this->widget(
                    'zii.widgets.grid.CGridView',
                    array(
                        'itemsCssClass' =>'table table-bordered',
                        'dataProvider'  => $orders,
                        'enableSorting' => false,
//    'filter'=>$model,
                        'pager'         => array(
                            'header'         => '',
                            'firstPageLabel' => '&lt;&lt;',
                            'prevPageLabel'  => '&lt;',
                            'nextPageLabel'  => '&gt;',
                            'lastPageLabel'  => '&gt;&gt;',
                        ),
//    'type'=>'striped bordered condensed',
                        'template'      => '{summary}{items}{pager}',
                        'summaryText'   => Yii::t('main', 'Заказы') . ' {start}-{end} ' . Yii::t('main', 'из') . ' {count}',
                        'columns'       => array(
                            array(
                                'name'  => 'id',
                                'type'  => 'raw', //$model->id
                                'value' => function ($data) {
                                    echo '<input type="radio" name="order" value="' . $data->id . '"/>' . $data->uid . '-' . $data->id;
                                },
                            ),
                            array(
                                'header'      => Yii::t('main', 'Товар'),
                                'type'        => 'raw',
                                'value'       => 'Order::getOrderItemsPreview($data->id,"_60x60.jpg")',
                                'htmlOptions' => array('style' => 'min-width:145px;height:60px;padding:3px;')
                            ),
                            array(
                                'name'  => 'date',
                                'type'  => 'raw',
                                'value' => 'date("d.m.Y H:i",$data->date)'
                            ),
                            array(
                                'name'  => 'sum',
                                'type'  => 'raw',
                                'value' => 'Formulas::priceWrapper(Formulas::convertCurrency($data->sum,DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
                            ),
                            array(
                                'name'  => 'weight',
                                'type'  => 'raw',
                                'value' => '$data->weight'
                            ),
                            array(
                                'name'  => 'delivery_id',
                                'type'  => 'raw',
                                'value' => '$data->delivery_id'
                            ),
                            array(
                                'name'  => 'delivery',
                                'type'  => 'raw',
                                'value' => 'Formulas::priceWrapper(Formulas::convertCurrency($data->delivery,DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
                            ),
                            array(
                                'header' => Yii::t('main', 'Итого'),
                                'type'   => 'raw',
                                'value'  => 'Formulas::priceWrapper(Formulas::convertCurrency($data->sum+$data->delivery,DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
                            ),
                        ),
                    )
                );
                ?>
        </div>
        <div class="next-btn" style="clear: both">
            <?= CHtml::submitButton(Yii::t('main', 'Продолжить'), array('class' => 'btn btn-default button button-medium','style' => 'padding: 10px 25px 10px 25px')) ?>
        </div>
        </form>

    </div>
</div>