<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_address.php">
 * </description>
 * Параметры формы создания нового адреса доставки
 **********************************************************************************************************************/
?>

<?php
return array(
    'class' =>'std',
    'id'=>'add_address',
    'style' => 'max-width: 290px;padding: 13px;',
    'elements' => array(
        'step'      => array(
            'type'  => 'hidden',
            'value' => '1',
        ),
        'firstname' => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control'
        ),
        'lastname'  => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control'
        ),
        'patroname' => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control'
        ),
        'country'   => array(
            'type'   => 'dropdownlist',
            'items'  => Deliveries::getCountries(), //TODO DSConfig::model()->getCountries(),
            'prompt' => Yii::t('main', 'Выбрать'),
            'class'=>'form-control'
        ),
        'index'     => array(
            'type'      => 'text',
            'maxlength' => 32,
            'class'=>'is_required validate form-control'
        ),
        'city'      => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control'
        ),
        'address'   => array(
            'type' => 'textarea',
            'rows' => '10',
            'class'=>'is_required validate form-control'
        ),
        'region'    => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control'
        ),
        'phone'     => array(
            'type'      => 'text',
            'mahlength' => 32,
            'class'=>'is_required validate form-control'
        )
    ),
    'buttons'  => array(
        'submit' => array(
            'type'  => 'submit',
            'label' => Yii::t('main', 'Добавить'),
            'class' => 'blue-btn bigger',
            'style' => 'float:left; margin-top:10px; padding: 10px 25px 10px 25px',
             'class'=>'btn btn-default button button-medium'
        ),
        'reset'  => array(
            'type'    => 'reset',
            'label'   => Yii::t('main', 'Отмена'),
            'onclick' => 'window.history.back()',
            'class'   => 'blue-btn bigger',
            'style'   => 'float:right; margin-left:5px;margin-top:10px; padding: 10px 25px 10px 25px',
            'class'=>'btn btn-default button button-medium'

        ),
    ),
);?>