<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="weight.php">
 * </description>
 * Форма ввода веса лотов заказа
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<div class="content">
        <?= cms::customContent('checkout-weight-desc') ?>
</div>
<div class="clear"></div>
<br/>
<table id="cart_summary" class="table table-bordered">
    <thead>
    <tr>
        <th class="cart_product first_item">Товар</th>
        <th class="cart_description item">Описание</th>
        <th class="cart_avail item">Вес, грамм</th>
        <th class="cart_unit item">Цена</th>
        <th class="cart_quantity item">Количество</th>
        <th class="cart_total item">Сумма</th>
        <th class="cart_delete last_item">&nbsp;</th>
    </tr>
    </thead>
<form action="<?= $this->createUrl('/checkout/weight') ?>" method="POST">
    <?php foreach ($cart->cartRecords as $k => $item) { ?>
        <? $this->widget(
          'application.components.widgets.OrderItem',
          array(
            'orderItem'   => $item,
            'readOnly'    => false,
            'allowDelete' => false,
            'imageFormat' => '_200x200.jpg',
          )
        );
        ?>
    <? } ?>
    </table>
    <input type="hidden" name="step" value="2"/>

    <div class="next-btn">
        <?= CHtml::submitButton(Yii::t('main', 'Продолжить'), array('class' => 'button btn btn-default standard-checkout button-medium', 'style'=>'padding:10px 25px 10px 25px')) ?>
    </div>
</form>
