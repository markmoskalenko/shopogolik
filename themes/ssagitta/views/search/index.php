<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Рендеринг поисковой выдачи (вобще любой, по категориям, брэндам, пользователю, запросу и т.п.)
 **********************************************************************************************************************/
?>
    <script type="text/javascript">
        var CUSTOMIZE_TEXTFIELD = 1;
        var FancyboxI18nNext = 'Next';
        var FancyboxI18nPrev = 'Previous';
        var FancyboxboxI18nClose = 'Close';
        var added_to_wishlist = 'Added to your wishlist.';
        var ajaxsearch = true;
        var baseDir = 'http://demo.posthemes.com/pos_sagitta/';
        var baseUri = 'http://demo.posthemes.com/pos_sagitta/';
        var blocksearch_type = 'top';
        var blocklayeredSliderName = {"price":"price","weight":"weight"};
        var comparator_max_item = 3;
        var comparedProductsIds = [];
        var contentOnly = false;
        var customizationIdMessage = 'Customization #';
        var delete_txt = 'Delete';
        var freeProductTranslation = 'Free!';
        var freeShippingTranslation = 'Free shipping!';
        var generated_date = 1415002671;
        var id_lang = 1;
        var img_dir = 'http://demo.posthemes.com/pos_sagitta/themes/pos_sagitta/img/';
        var instantsearch = false;
        var isGuest = 0;
        var isLogged = 0;
        var loggin_required = 'You must be logged in to manage your wishlist.';
        var max_item = 'You cannot add more than 3 product(s) to the product comparison';
        var min_item = 'Please select at least one product';
        var page_name = 'category';
        var placeholder_blocknewsletter = 'Your e-mail';
        var priceDisplayMethod = 1;
        var priceDisplayPrecision = 2;
        var quickView = true;
        var removingLinkText = 'remove this product from my cart';
        var request = '5-tshirts.html';
        var roundMode = 2;
        var search_url = 'search.html';
        var static_token = '8656ddc2d74502f428b649056b76a290';
        var token = '42b3b16e73f0b1c99eadae54d4d38e50';
        var usingSecureMode = false;
        var wishlistProductsIds = false;
    </script>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<div class="list-slide">
    <!-- /Block tags module -->
    <div class="pos-special-product pos_animated" align="center" style="margin: -25px 0 0">
        <ul class="bxslider fx-fadeInDown a3">
            <li class="ajax_block_product first_item item clearfix">
                <div class="item-inner">
                        <!--                    Залить файлы в директорию и испльзовать конструкцию ниже-->
                        <!--                    <img src="--><?//= $this->frontThemePath ?><!--/folder/image.jpeg" alt=""/>-->

                        <!--                    Это подгрузка картинки из стороннего ресурса для примера-->
                        <img src="http://shopogolik24.com/themes/ssagitta/images/banners/search_1.jpg" title="Грандиозные скидки до 80%! Успей купить!"/>
                    <h3><a class="product-name" href="#" title="Printed Summer Dress">Грандиозные скидки до 80%! Успей купить!</a></h3>
                </div>
            </li>

            <li class="ajax_block_product first_item item clearfix">
                <div class="item-inner">
                        <!--                    Залить файлы в директорию и испльзовать конструкцию ниже-->
                        <!--                    <img src="--><?//= $this->frontThemePath ?><!--/folder/image.jpeg" alt=""/>-->

                        <!--                    Это подгрузка картинки из стороннего ресурса для примера-->
                        <img src="http://shopogolik24.com/themes/ssagitta/images/banners/search_2.jpg" title="Оперативная и выгодная доставка до двери!"/>
                    <h3><a class="product-name" href="#" title="Printed Summer Dress">Оперативная и выгодная доставка до двери!</a></h3>
                </div>
            </li>
        </ul>
    </div>

    <script type="text/javascript">
        $('.list-slide .bxslider').bxSlider({
            auto: 1,
            infiniteLoop: true,
            minSlides: 1,
            maxSlides: 1,
            speed: 1000,
            pause: 4000,
            controls: false,
            pager: 1
        });
    </script>
</div>

<? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
<? // Блок вывода описания категории, если это описание есть?>
<? if (!empty($category->{'page_desc_' . Utils::TransLang()})) { ?>
    <div class="category-desc">
        <?= $category->{'page_desc_' . Utils::TransLang()} ?>
    </div>
<? } ?>
<? // Блок вывода описания бренда, если это описание есть?>
<? if (!empty($brand->{'page_desc_' . Utils::TransLang()})) { ?>
    <div class="category-desc">
        <?= $brand->{'page_desc_' . Utils::TransLang()} ?>
    </div>
<? } ?>

<? // Блок параметров поиска, таких ка сортировка и проч.?>
<?php if ($res->query): ?>
    <h1 class="page-heading  product-listing">
        Поиск:&nbsp;
            <span class="lighter">
            "<?= $res->query ?>"
        </span>
    </h1>
<?php endif; ?>
<?php if (is_object($res)) { ?>
    <div><span style="float:right"> <?=Yii::t('main','Всего предложений')?>:&nbsp;<?=(isset($res->total_results))?$res->total_results:0?></span></div>
    <div class="content_sortPagiBar clearfix">
        <div class="top-pagination-content sortPagiBar clearfix">
            <ul class="display hidden-xs">
                <li id="grid" class="selected"><a rel="nofollow" href="#" title="Grid">Grid</a></li>
                <li id="list"><a rel="nofollow" href="#" title="List">List</a></li>
            </ul>

            <div id="pagination" class="pagination clearfix" style="padding: 10px;">

                <? if ($pages) {
                    $this->renderPartial(
                        '/search/pagination',
                        array(
                            'pages' => $pages,
                        )
                    );
                } ?>
            </div>
            <div class="search-sort" style="float: right;width: 300px;">
                <? $fAction = Yii::app()->createUrl('/' . $this->id . '/index', $params); ?>
                <form id="search-sort" action="<?= $fAction; ?>" method="post" style=" width: 532px;">
                    <select name="sort_by" class="selectProductSort form-control" id="sort_by" onchange="changeSortOrder('<?= $fAction; ?>');"
                        style="max-width: 200px">
                        <option value="popularity_desc" <?= ($sort_by == 'popularity_desc') ? 'selected' : '' ?>>
                            <?= Yii::t('main', 'По популярности') ?>
                        </option>
                        <option value="credit_desc" <?= ($sort_by == 'credit_desc') ? 'selected' : '' ?>>
                            <?= Yii::t('main', 'По рейтингу продавца') ?>
                        </option>
                        <option value="delistTime_desc" <?= ($sort_by == 'delistTime_desc') ? 'selected' : '' ?>>
                            <?= Yii::t('main', 'Новинки') ?>
                        </option>
                        <option value="price_asc" <?= ($sort_by == 'price_asc') ? 'selected' : '' ?>>
                            <?= Yii::t('main', 'Цена: по возрастанию') ?>
                        </option>
                        <option value="price_desc" <?= ($sort_by == 'price_desc') ? 'selected' : '' ?>>
                            <?= Yii::t('main', 'Цена: по убыванию') ?>
                        </option>
                    </select>
                    <label for="selectProductSort"><?= Yii::t('main', 'Сортировка') ?></label>

                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <ul class="product_list grid row">
        <?php if (isset($res->items)) {
            $i = 0;
            foreach ($res->items as $item) {
                $i++;
                $this->widget(
                  'application.components.widgets.SearchItem',
                  array(
                    'searchResItem' => $item,
                    'number' => $i,
                  )
                );
                if($i == 4) {
                    $i = 0;
                }
            }
        } ?>
    </ul>
<? } ?>
<? if ($pages) {
    $this->renderPartial(
      '/search/pagination',
      array(
        'pages' => $pages,
      )
    );
} ?>
<? $this->renderPartial('/search/recommended', array()); ?>