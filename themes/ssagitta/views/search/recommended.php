    <?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="recommended.php">
 * </description>
 * Рендеринг недавно просмотренных, рекомендованных и прочихвспомогательных товаров
 **********************************************************************************************************************/
?>
<? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
<div class="products-list featured">
    <? $this->widget(
      'application.components.widgets.ShowItemList',
      array(
        'id'                         => 'recentAll-itemslist',
        'controlAddToFavorites'      => true,
        'controlAddToFeatured'       => false,
        'controlDeleteFromFavorites' => false,
        'lazyLoad'                   => true,
        'dataType'                   => 'itemsRecentAll',
        'pageSize'                   => 8,
        'disableItemForSeo'          => $seo_disable_items_index,
        'viewName'                   => 'ShowItemList4',
      )
    );
    ?>
</div>
