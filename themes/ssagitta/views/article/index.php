<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<div class="rte">
    <h1 class="page-heading bottom-indent"><?=isset($article->title)?$article->title:''?></h1>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <?=isset($article->content)?$article->content:''?>
        </div>
    </div>
</div>