<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_images.php">
 * </description>
 * Рендеринг фото товара
 **********************************************************************************************************************/
?>

<!-- product img-->
<div id="image-block" class="clearfix">
  <span id="view_full_size">
     <img id="bigpic" itemprop="image" src="<?= Img::getImagePath($item->top_item->pic_url, '_480x480.jpg', false) ?>" width="458" height="458"/>
  </span>
</div> <!-- end image-block -->
<!-- thumbnails -->
<div id="views_block" class="clearfix ">
    <span class="view_scroll_spacer">
         <a id="view_scroll_left" class="" title="Other views" href="javascript:{}" style="cursor: pointer; display: block; opacity: 1;">
             Previous
         </a>
    </span>
    <div id="thumbs_list">
        <ul id="thumbs_list_frame" style="width: 396px;">
            <?php foreach ($item->top_item->item_imgs->item_img as $image) { ?>
            <li id="thumbnail_112">
                <a href="<?= Img::getImagePath($image->url, '_480x480.jpg', false) ?>" data-fancybox-group="other-views" class="fancybox">
                    <img class="img-responsive" id="thumb_112" src="<?= $image->url ?>_120x120.jpg" height="80" width="80" itemprop="image">
                </a>
            </li>
            <?php } ?>
        </ul>


    </div> <!-- end thumbs_list -->
    <a id="view_scroll_right" title="Other views" href="javascript:{}" style="cursor: default; opacity: 0; display: none;">
        Далее
    </a>

</div><!-- end views-block -->
<div class="l-banner"><img src="http://shopogolik24.com/themes/ssagitta/images/banners/support.jpg" title="Бесплатная консультация и помощь в поиске товара!"/></div>

<!-- end thumbnails -->

