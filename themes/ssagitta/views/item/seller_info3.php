<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="seller_info.php">
 * </description>
 * Рендеринг информации о продавце
 **********************************************************************************************************************/
?>
<ul>
    <li><span class="seller-head"> <a href="<?=
        Yii::app()->createUrl(
          '/seller/index',
          array('nick' => $seller->user_id)
        ) ?>"><?= Yii::t('main', 'Товары продавца ') ?> <?= $seller->seller_nick; ?></a></span>
    </li>
</ul>
<ul>
    <? if ($item->top_item->city != false) { ?>
        <li><span class="seller-head"><?= Yii::t('main', 'находятся в') ?>:</span>
                    <span>
                        <? if (isset($item->top_item->state)) { ?>
                            <?= $item->top_item->city ?>(<?= $item->top_item->state ?>)
                        <? } else { ?>
                            <?= $item->top_item->city ?>
                        <? } ?>
                    </span>
        </li>
    <? } ?>
</ul>
<ul>
    <li><span class="seller-head"><?= Yii::t('main', 'Рейтинг') ?>:</span>
                <span>
                    <?= $seller->seller_credit; ?>
                    <? if (isset($seller->rating->level)) { ?> / <?= $seller->rating->level; ?>
                    <? } ?>
                    <? if (isset($seller->reviews)) { ?>
                        <?= Yii::t('main', 'Отзывов') ?>:
                        <? if (isset($seller->reviews)) { ?>
                            <?= $seller->reviews; ?>
                        <? } ?>
                    <? } ?>
                </span>
    </li>
</ul>
<ul>
    <li>
        <? if ($seller->paySuccess > 0) { ?>
            <span class="seller-head"><?= Yii::t('main', 'Продано') ?>:</span>
            <?= $seller->paySuccess; ?>
            <span><?= Yii::t('main', 'Возврат') ?>:</span>
            <?= $seller->refundCount; ?>
        <? } ?>
    </li>
</ul>