<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_form.php">
 * </description>
 * Форма работы с картой товара
 **********************************************************************************************************************/
?>
<form action="<?= Yii::app()->createUrl('/cart/add') ?>" method="POST">
    <div class="product_attributes clearfix form-item">
        <input type="hidden" name="cid" value="<?= $item->top_item->cid ?>" id="cid"/>
        <input type="hidden" name="iid" value="<?= $item->top_item->num_iid ?>" id="iid"/>
        <input type="hidden" name="item_totalcount" id="item_totalcount" value="<?= $item->top_item->num ?>">
        <input type="hidden" name="inputprops-processed" id="inputprops-processed" value="0"/>
        <?php
        if (!$ajax['input']) {
            echo Yii::app()->controller->renderPartial(
                'input_props',
                array(
                    'totalCount'  => $item->top_item->num,
                    'input_props' => $input_props
                )
            );
        } else {
            ?>
            <div id="item-input-props">
                <script async type="text/javascript">
                    loadInputProps();
                </script>
            </div>
        <?php } ?>




        <!-- quantity wanted -->
        <p id="quantity_wanted_p">
            <span class="quantity_label"><?= Yii::t('main', 'Количество') ?>:</span>

            <span class="quantity_body">
                <input type="text" name="num" id="quantity_wanted" class="text item-count" value="1" onchange="getUserPrice()" />
                <input type="hidden" name="num-processed" id="num-processed" value="1"/>
                <a type="button" data-field-qty="qty" class="btn btn-default button-minus product_quantity_down minus">
                    <span><i class="icon-minus"></i></span>
                </a>

                <a  data-field-qty="qty" class="btn btn-default button-plus product_quantity_up  plus">
                    <span><i class="icon-plus"></i></span>
                </a>
            </span>
            <span class="clearfix"></span>
        </p>

        <span class="clearfix"></span>

        <!-- quantity wanted -->
        <p id="stock_p">
            <span class="btn btn-default button button-small">В наличии:</span>
            <span id="item_num"><?= $item->top_item->num ?></span>
            <? if (Yii::app()->user->checkAccess('@showTaobaoLinkInUserCard')) { ?>
                <div class="item-info-param" id="item_ontaobao">
                    <a href="http://item.taobao.com/item.htm?id=<?= $item->top_item->num_iid ?>" target="_blank">
                        &nbsp;<?= Yii::t('main', 'на taobao.com') ?></a>
                </div>
            <? } ?>
            <span class="clearfix"></span>
        </p>

        <span class="clearfix"></span>

        <p id="stock_p">
            <span class="stock_label weight">Примерный вес 1 шт., грамм:</span>
            <span><?= $item->top_item->weight_calculated ?></span>
            <span class="clearfix"></span>
        </p>
    </div> <!-- end product_attributes -->


    <div class="box-cart-bottom">
        <div>
            <button type="submit" name="doGo" class="exclusive buy-btn bigger" disabled="disabled" onclick="yaCounter25369892.reachGoal('dobavit_v_korzinu')">
                <span><?= Yii::t('main', 'Добавить в корзину') ?></span>
            </button>
            <br/>
        </div>

        <p class="buttons_bottom_block no-print">
            <input type="button" id="wishlist_button" name="dofav" value="<?= Yii::t('main', 'Добавить в избранное') ?>" class="fav-btn bigger" onclick="yaCounter25369892.reachGoal('dobavit_v_izbrannoe')"
                   formaction="<?=
                   Yii::app()->createUrl(
                       '/cabinet/favorite/add',
                       array(
                           'iid'      => $item->top_item->num_iid,
                           'download' => true
                       )
                   ) ?>"
                   onclick="addFavorite(this,<?= $item->top_item->num_iid ?>); return false;"/>

        </p>
        <div class="buy-hint">Для того чтобы положить товар в корзину выберите размер, цвет и количество </div>
        <strong></strong>
    </div> <!-- end box-cart-bottom -->
</form>