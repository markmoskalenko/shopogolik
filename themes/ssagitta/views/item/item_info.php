<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_info.php">
 * </description>
 * Рендеринг блока табов описания товара
 **********************************************************************************************************************/
?>
<ul id="more_info_tabs" class="idTabs idTabsShort clearfix">
    <li><a id="more_info_tab_more_info" href="#idTab1" class="selected"><span><?= Yii::t('main', 'Подробная информация') ?></span></a></li>
    <li><a href="#idTab5" class="idTabHrefShort"><?= Yii::t('main', 'Характеристика товара') ?></a></li>
    <li><a href="#idTab69" class="idTabHrefShort relatedPostsTab"><?= Yii::t('main', 'Получить бонус') ?></a></li>
</ul>

<!-- More info -->
<section id="idTab1" class="page-product-box" style="overflow: scroll;height: 2100px; margin: 20px 0">
    <!-- full description -->
    <? if (isset($item->top_item->desc) && ($item->top_item->desc)) {
        echo $this->renderPartial('item_details', array('src' => $item->top_item->desc), true, false);
    } else {
        ?>
        <div id="item-detail-block">
            <script async type="text/javascript">
                <?if (isset($item->top_item->descUrl) && ($item->top_item->descUrl)) {?>
                loadItemDetailsFromUrl('<?=urlencode($item->top_item->descUrl)?>');
                <? } else {?>
                loadItemDetails(<?=$item->top_item->num_iid?>);
                <? } ?>
            </script>
        </div>
    <? } ?>
</section>
<!--end  More info -->
<!--HOOK_PRODUCT_TAB -->
<div id="idTab5" class="page-product-box block_hidden_only_for_screen">

    <div id="idTab5">
        <? if (isset($item->top_item->item_attributes)) { ?>
            <ul>
                <? foreach ($item->top_item->item_attributes as $attribute) { ?>
                    <li class="block_1">
                        <strong><?= $attribute->prop ?>:</strong> <?= $attribute->val ?>
                    </li>
                <? } ?>
            </ul>
        <?
        } else {
            ?>
            <!--  Old style output -->
            <table class="item-table">
                <? if (is_array($props)) {
                    foreach ($props as $pid => $prop) {
                        ?>
                        <? if ($prop->name !== '') { ?>
                            <tr>
                                <td><?= $prop->name ?></td>
                                <th><?= $prop->value ?></th>
                            </tr>
                        <? } ?>
                    <?
                    }
                }?>
            </table>
        <? } ?>
    </div>
</div>
<div id="idTab69" class="block_hidden_only_for_screen">
    <div style="margin: 0 0 10px 10px;">
        <div style="margin: 0 0 10px 10px;">
<img src="http://shopogolik24.com/themes/ssagitta/images/banners/bonus.png" title="Учавствуй и получай ценные призы!">
        </div>
    </div>
</div>

<script>
    $('#idTab1').on('click', function (e){
        return false;
    });
</script>