<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_price.php">
 * </description>
 * Рендеринг цены товара
 **********************************************************************************************************************/
?>


<div class="content_prices clearfix">
    <div class="price">


        <div class="item-price">
            <? if (!isset($item->sku)) {
                if ($item->top_item->price > $item->top_item->promotion_price) {
                    ?>
                    <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price">
                <? if (isset($item->top_item->userPromotionPrice)) { ?>
                    <span
                        title="<?=
                        (Yii::app()->user->inRole('superAdmin')) ? $item->top_item->userPromotionResUserPrice->report(
                        ) : '' ?>"><?= $item->top_item->userPromotionPrice; ?></span>
                <?
                } else {
                    ?>
                    <span
                        title="<?=
                        (Yii::app()->user->inRole('superAdmin')) ? $item->top_item->userPromotionResUserPrice1->report(
                        ) : '' ?>"><?= $item->top_item->userPromotionPrice1; ?></span>
                                -
                                <span
                        title="<?=
                        (Yii::app()->user->inRole('superAdmin')) ? $item->top_item->userPromotionResUserPrice2->report(
                        ) : '' ?>"><?= $item->top_item->userPromotionPrice2; ?></span>
                <? } ?><s>
                            <? if (isset($item->top_item->userPrice)) { ?>
                                <?= $item->top_item->userPrice; ?>
                            <?
                            } else {
                                if ($item->top_item->userPrice1 == $item->top_item->userPrice2) {
                                    ?>
                                    <?= $item->top_item->userPrice1; ?>
                                <?
                                } else {
                                    ?>
                                    <?= $item->top_item->userPrice1; ?>-<?= $item->top_item->userPrice2; ?>
                                <?
                                }
                            }?></s></span>
                <?
                } else {
                    ?>
                    <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price"><?= $item->top_item->userPromotionPrice ?></span>
                <? } ?>
                <input type="hidden" name="price" value="<?= $item->top_item->promotion_price ?>" id="price_val"/>
            <?
            } else {
                if ($item->sku->price > $item->sku->promotion_price) {
                    ?>
                    <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price"><?= $item->sku->userPromotionPrice ?>
                        &nbsp;<s><?= $item->sku->userPrice ?></s></span>
                <?
                } else {
                    ?>
                    <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price"><?= $item->sku->userPromotionPrice ?></span>
                <? } ?>
                <input type="hidden" name="price" value="<?= $item->sku->promotion_price ?>" id="price_val"/>
            <? } ?>
        </div>

    </div> <!-- end prices -->
    <p id="reduction_amount"  style="display:none">
        <span id="reduction_amount_display"></span>
    </p>
    <div class="clear"></div>
</div> <!-- end content_prices -->