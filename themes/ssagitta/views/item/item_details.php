<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_details.php">
 * </description>
 * Рендеринг описания товара
 **********************************************************************************************************************/
?>
<?
$res = preg_match_all('/<\s*img\s+.*?\ssrc\s*=\s*"(http:\/\/[a-z0-9]+?\.taobaocdn.+?)"/is', $src, $descImgs);
if ($res) {
    ?>
    <ul style="width: 900px;">
        <? foreach ($descImgs[1] as $descImg) { ?>
            <li style="width: 450px; float:left;"><img style="width:450px; margin:-1px;"
                                                       src="<?= Img::getImagePath($descImg, '_360x360.jpg', false); ?>"
                                                       alt=""></li>
        <? } ?>
    </ul>
<? } ?>
