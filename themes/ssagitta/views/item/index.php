<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Рендеринг карты товара
 **********************************************************************************************************************/
?>
<?
?>
<?php $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1;
$meta = Item::getSEOTags($item->top_item);
$this->pageTitle = $meta->title;
$this->meta_desc = $meta->description;
$this->meta_keyword = $meta->keywords;
$title = $item->top_item->title;
?>

<?php if (Yii::app()->user->hasFlash('cart')): ?>
    <div id="layer_cart2" style="display: none;">
        <h2>
            <i class="icon-ok"></i>&nbsp;Товар успешно добавлен в корзину!
        </h2>

        <a class="btn btn-default button button-medium" href="#" onclick="$.fancybox.close()" rel="nofollow">
            <span>
                Продолжить покупки
            </span>
        </a>

        <a class="btn btn-default button button-medium" href="/ru/cart/index" rel="nofollow">
            <span>
                Перейти к оформлению<i class="icon-chevron-right right"></i>
            </span>
        </a>
    </div>

    <script>
        $.fancybox({
            minWidth:600,
            href : '#layer_cart2'
        });
    </script>
<?php endif; ?>

<div class="primary_block row">
    <div class="container">
        <div class="top-hr"></div>
    </div>

    <div class="pb-left-column col-xs-12 col-sm-5 col-md-5">
        <?php Yii::app()->controller->renderPartial('item_images', array('item' => $item)); ?>
    </div>

    <div class="pb-center-column col-xs-12 col-sm-7 col-md-7">

        <h1><?= $title ?></h1>

        <div id="short_description_block">
            <div id="short_description_content" class="rte align_justify" itemprop="description">
                <p>

                </p>
            </div>
        </div>

        <div class="center">
            <img src="http://shopogolik24.com/themes/ssagitta/images/banners/doping.jpg" />
        </div>
        <div class="box-info-product">
            <?php Yii::app()->controller->renderPartial('item_price', array('item' => $item,)); ?>

            <?php Yii::app()->controller->renderPartial(
                'item_form',
                array('item' => $item, 'ajax' => $ajax, 'input_props' => $input_props)
            ); ?>

        </div>


        <!--  /Module ProductComments -->        </div>
    <!-- end pb-right-column-->
</div> <!-- end primary_block -->



<?php Yii::app()->controller->renderPartial('item_info', array('item' => $item)); ?>

<style>#message-block{display: none !important;}</style>
