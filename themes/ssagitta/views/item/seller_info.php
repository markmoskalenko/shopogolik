<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="seller_info.php">
 * </description>
 * Рендеринг информации о продавце
 **********************************************************************************************************************/
?>
<div class="newshow">
    <?
    if (isset($item->taobao_item->userRateUrl)) {
        $sellerInfo = ItemSeller::getSellerInfoFromUrl($item->taobao_item->userRateUrl);
        if (isset($item->top_item->seller)) {
            $seller = $item->top_item->seller;
        }
        else {
            //Получаем данные о продавце
            $sellerCache = Yii::app()->fileCache->get('user-' . $item->top_item->nick);
            if ($sellerCache == FALSE) {
                $ajax['seller'] = TRUE;
                $seller = array();
            }
            else {
                list($seller) = $sellerCache;
            }
        }
    } else {
        $sellerInfo = false;
    }
    ?>

    <? // Информация о продавце из товара ?>
    <div class="l">
        <label><?= Yii::t('main', 'Продавец') ?>&nbsp;ID:&nbsp;<?= $seller->user_id ?></label>
        <a href="<?= Yii::app()->createUrl('/seller/index', array('nick' => ((DSConfig::getVal('search_use_fulltext_for_seller')!=1)?$seller->user_id:$seller->seller_nick))) ?>" class="seller-name">
            Все товары продавца
        </a>
    </div>

    <div class="l">
        <? if ($item->top_item->city != false) { ?><br>
            <label><?= Yii::t('main', 'Находится в') ?>:</label>
            <? if (isset($item->top_item->state)) { ?>
                <?= $item->top_item->city ?>(<?= $item->top_item->state ?>)
            <? } else { ?>
                <?= $item->top_item->city ?>
            <?
            }
        }?>

    <? if (isset($seller->paySuccess) && ($seller->paySuccess > 0)) { ?>
        <div class="l">
        <label><?= Yii::t('main', 'Продано этого товара') ?>:</label>
        <?= $seller->paySuccess; ?>
        </div>

        <div class="l">
        <label><?= Yii::t('main', 'Возвратов') ?>:</label>
        <?= $seller->refundCount; ?>
      </div>
    <? } ?>

    </div>

    <div class="l" style="height: 24px">
        <? // Рейтинги - откуда и какие есть ?>
        <label><?= Yii::t('main', 'Рейтинг') ?>:</label>
        <?= ($sellerInfo && isset($sellerInfo->seller->sales))?$sellerInfo->seller->sales:$seller->seller_credit; ?>
            <span class="rating"><i class="i-rating rating_<?= DSGSeller::getCrownsFromSales(($sellerInfo && isset($sellerInfo->seller->sales))?$sellerInfo->seller->sales:$seller->seller_credit)-1; ?>"></i></span>
    </div>

    <? if ($sellerInfo && isset($sellerInfo->seller->rates) && count($sellerInfo->seller->rates)) { ?>

        <?if (isset($sellerInfo->seller->rates['description_q'])) {?>
            <div class="l">
                <label><?= Yii::t('main', 'Соответствие товара описанию') ?>:</label>
                <?= round($sellerInfo->seller->rates['description_q'],2) ?>
            </div>
        <? } ?>
        <?if (isset($sellerInfo->seller->rates['service_q'])) {?>
            <div class="l">
                <label><?= Yii::t('main', 'Сервис и обслуживание') ?>:</label>
                <?= round($sellerInfo->seller->rates['service_q'],2) ?>
            </div>
        <? } ?>
        <?if (isset($sellerInfo->seller->rates['delivery_q'])) {?>
            <div class="l">
                <label><?= Yii::t('main', 'Скорость отправки товара') ?>:</label>
                <?= round($sellerInfo->seller->rates['delivery_q'],2) ?>
            </div>
        <? } ?>
    <? } ?>
    <? if ($sellerInfo && isset($sellerInfo->seller->reviews) && count($sellerInfo->seller->reviews)) { ?>
            <div class="l">
                <label><?= Yii::t('main', 'Отзывы о продавце') ?>:</label>
                <?if (isset($sellerInfo->seller->reviews['positive'])) {?>
                    &nbsp;<span style="color: green;">+<?=$sellerInfo->seller->reviews['positive'] ?></span>
                <? } ?>
                <?if (isset($sellerInfo->seller->reviews['normal'])) {?>
                    &nbsp;/&nbsp;<?=$sellerInfo->seller->reviews['normal'] ?>
                <? } ?>
                <?if (isset($sellerInfo->seller->reviews['negative'])) {?>
                    &nbsp;/&nbsp;<span style="color: red;">-<?=$sellerInfo->seller->reviews['negative'] ?></span>
                <? } ?>
            </div>

    <? } ?>

    <div class="l-banner"><img src="/themes/ssagitta/images/banners/1-350.jpg" title="Грандиозные скидки до 80%!"/></div>
    <div class="l-banner"><img src="/themes/ssagitta/images/banners/2-350.jpg" title="Выбери себе подарок!"/></div>
</div>