<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="ShowItemList.php">
 * </description>
 * Рендеринг списка товаров в поисковой выдаче посредством CListView
 *
 * var $showControl - показывать кнопки
 * var $disableItemForSeo
 * var $imageFormat
 * var $controlAddToFavorites - кнопка Добавить в избранное
 * var $controlAddToFeatured - кнопка Добавить в рекомендованное
 * var $controlDeleteFromFavorites - кнопка Удалить (из избранного, в частности)
 * var $lazyLoad
 **********************************************************************************************************************/
?>

<div class="tab-category-container">
<?php if (count($dataProvider->getData())): ?>
<div class="tab_container">
    <div id="tab_12" class="tab_category" style="display: block;">
        <ul class="productTabCategory row">
            <?php $loop = 0; ?>
            <?php $newLine = 0; ?>
            <?php foreach ($dataProvider->getData() as $data): ?>
                <?php $loop++; ?>
                <?php $newLine++; ?>
                <?php
                $item = new Item($data['id'], FALSE);
                $sellerInfo = ItemSeller::getSellerInfoFromUrl($item->taobao_item->userRateUrl);
                $seller = $item->top_item->seller;
                ?>
                <li class="fx-fadeInUp a<?= $loop ?> col-xs-12 col-md-4 col-sm-4 tab-category-item  ajax_block_product<?php if ($newLine == 1): ?> first_item<?php else: ?> item<?php endif; ?> last_item_of_line ">
                    <div class="product-container" itemscope itemtype="http://schema.org/Product">

                        <div class="left-block">
                            <div class="product-image-container">
                                <a class="product_img_link" href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" title="" itemprop="url">
                                    <img class="replace-2x img-responsive" src="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt="" title="" itemprop="image" width="105" height="105">
                                </a>
                            </div>
                        </div>
                        <div class="right-block">
                            <h5 itemprop="name">
                            </h5>

                            <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
                                <?
                                $resUserPrice = Formulas::getUserPrice(
                                    array(
                                        'price'       => $data['price'],
                                        'count'       => 1,
                                        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                        'postageId'   => false,
                                        'sellerNick'  => false,
                                    )
                                );
                                $userPrice = $resUserPrice->price;
                                $resUserPrice = Formulas::getUserPrice(
                                    array(
                                        'price'       => $data['promotion_price'],
                                        'count'       => 1,
                                        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                        'postageId'   => false,
                                        'sellerNick'  => false,
                                    )
                                );
                                $userPromotionPrice = $resUserPrice->price;
                                if ($data['price'] > $data['promotion_price']) {
                                    ?>
                                    <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                                    <span class="old-price product-price"><?= Formulas::priceWrapper($userPrice) ?></span>
                                <?
                                } else {
                                    ?>
                                    <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                                <? } ?>
<!--                                <span itemprop="price" class="price product-price">$20.00</span>-->
                                <meta itemprop="priceCurrency" content="1">
                            </div>

                            <div class="button-container">
                                <a class="button btn btn-default" href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" rel="nofollow" title="Заказать товар" data-id-product="15">
                                    <span>Заказать</span>
                                </a>
                            </div>

                            <div class="comments_note" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                <div class="star_content clearfix">

                                    <div class="rating-2 no-float"><span>Рейтинг:</span><i class="i-rating rating_<?= DSGSeller::getCrownsFromSales(($sellerInfo && isset($sellerInfo->seller->sales))?$sellerInfo->seller->sales:$seller->seller_credit)-1; ?>"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <?php
                if ($newLine == 3) {
                    $newLine = 0;
                    echo '</ul>';
                    echo '<ul class="productTabCategory row">';
                }
                if ($loop == 6){
                    break;
                }
                ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endif; ?>
</div>

