<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 * Рендеринг списка товаров в поисковой выдаче посредством CListView
 *
 * var $showControl - показывать кнопки
 * var $disableItemForSeo
 * var $imageFormat
 * var $controlAddToFavorites - кнопка Добавить в избранное
 * var $controlAddToFeatured - кнопка Добавить в рекомендованное
 * var $controlDeleteFromFavorites - кнопка Удалить (из избранного, в частности)
 * var $lazyLoad
 **********************************************************************************************************************/
?>

<?php
//    var_dump($dataProvider->getData()); exit;
?>
<?php if (count($dataProvider->getData())): ?>
<div class="tab-content fx-fadeInDown a3">
    <ul id="homefeatured" class="pos_animated product_list grid row homefeatured tab-pane active">
    <?php $loop = 0; ?>
    <?php $newLine = 0; ?>
    <?php foreach ($dataProvider->getData() as $data): ?>
        <?php $loop++; ?>
        <?php $newLine++; ?>
        <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 fx-fadeInDown a<?= $loop ?>
            <?php switch ($newLine){
                case 1:
                    echo " first-in-line first-item-of-tablet-line first-item-of-mobile-line";
                    break;
                case 2:
                    echo " last-item-of-mobile-line";
                    break;
                case 3:
                    echo " last-item-of-tablet-line first-item-of-mobile-line";
                    break;
                case 4:
                    echo " last-in-line first-item-of-tablet-line last-item-of-mobile-line";
                    break;
            } ?>
         ">
            <div class="product-container" itemscope itemtype="http://schema.org/Product">
                <div class="left-block">
                    <div class="product-image-container">
                        <a class="product_img_link"	href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" title="" itemprop="url">
                            <img class="replace-2x img-responsive" src="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt="" title=""   itemprop="image" />
                        </a>
                        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
                                <?
                                $resUserPrice = Formulas::getUserPrice(
                                    array(
                                        'price'       => $data['price'],
                                        'count'       => 1,
                                        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                        'postageId'   => false,
                                        'sellerNick'  => false,
                                    )
                                );
                                $userPrice = $resUserPrice->price;
                                $resUserPrice = Formulas::getUserPrice(
                                    array(
                                        'price'       => $data['promotion_price'],
                                        'count'       => 1,
                                        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                        'postageId'   => false,
                                        'sellerNick'  => false,
                                    )
                                );
                                $userPromotionPrice = $resUserPrice->price;
                                if ($data['price'] > $data['promotion_price']) {
                                    ?>
                                    <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                                    <span class="old-price product-price"><?= Formulas::priceWrapper($userPrice) ?></span>
                                <?
                                } else {
                                    ?>
                                    <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                                <? } ?>
                            <meta itemprop="priceCurrency" content="1" />
                        </div>
                        <span class="new-box">
                            <span class="new-label">New</span>
                        </span>
                    </div>
                </div>
                <div class="right-block">
                    <h5 itemprop="name">
<!--                        <a class="product-name" href="--><?//= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?><!--" title="Etiam at metus" itemprop="url" >-->
<!--                            Etiam at metus-->
<!--                        </a>-->
                    </h5>
                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">

                        <?
                        $resUserPrice = Formulas::getUserPrice(
                            array(
                                'price'       => $data['price'],
                                'count'       => 1,
                                'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                'postageId'   => false,
                                'sellerNick'  => false,
                            )
                        );
                        $userPrice = $resUserPrice->price;
                        $resUserPrice = Formulas::getUserPrice(
                            array(
                                'price'       => $data['promotion_price'],
                                'count'       => 1,
                                'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                'postageId'   => false,
                                'sellerNick'  => false,
                            )
                        );
                        $userPromotionPrice = $resUserPrice->price;
                        if ($data['price'] > $data['promotion_price']) {
                            ?>
                            <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                            <span class="old-price product-price"><?= Formulas::priceWrapper($userPrice) ?></span>
                        <?
                        } else {
                            ?>
                            <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                        <? } ?>
                        <meta itemprop="priceCurrency" content="1" />
                    </div>
                    <div class="button-container">
                        <a class="button btn btn-default" href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" rel="nofollow" title="Заказать товар" data-id-product="35">
                            <span>Заказать</span>
                        </a>
                    </div>
                </div>
            </div>
        </li>
        <?php
            if ($newLine == 4) {
                $newLine = 0;
            }
        ?>
    <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>
