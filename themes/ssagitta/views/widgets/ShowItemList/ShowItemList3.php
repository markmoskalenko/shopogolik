<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 * Рендеринг списка товаров в поисковой выдаче посредством CListView
 *
 * var $showControl - показывать кнопки
 * var $disableItemForSeo
 * var $imageFormat
 * var $controlAddToFavorites - кнопка Добавить в избранное
 * var $controlAddToFeatured - кнопка Добавить в рекомендованное
 * var $controlDeleteFromFavorites - кнопка Удалить (из избранного, в частности)
 * var $lazyLoad
 **********************************************************************************************************************/
?>

<?php
//    var_dump($dataProvider->getData()); exit;
?>
<?php if (count($dataProvider->getData())): ?>
        <ul class="productTabCategorySlider">
            <?php $loop = 0; ?>
            <?php $newLine = 0; ?>
            <?php foreach ($dataProvider->getData() as $data): ?>
                <?php $loop++; ?>
                <?php $newLine++; ?>
                <li class="cate_item">
                    <div class="product-container" itemscope itemtype="http://schema.org/Product">			

                        <div class="left-block">
                            <div class="product-image-container">
                                <a class="product_img_link"	href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" title="Недавно просмотренный вами товар" itemprop="url">
                                    <img class="replace-2x img-responsive" src="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt="Printed Dress" title="Недавно просмотренный вами товар"   itemprop="image" />
                                </a>
                                <span class="new-box">
                                    <span class="new-label">New</span>
							    </span>
                            </div>
                            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
								    <?
                                    $resUserPrice = Formulas::getUserPrice(
                                        array(
                                            'price'       => $data['price'],
                                            'count'       => 1,
                                            'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                            'postageId'   => false,
                                            'sellerNick'  => false,
                                        )
                                    );
                                    $userPrice = $resUserPrice->price;
                                    $resUserPrice = Formulas::getUserPrice(
                                        array(
                                            'price'       => $data['promotion_price'],
                                            'count'       => 1,
                                            'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                            'postageId'   => false,
                                            'sellerNick'  => false,
                                        )
                                    );
                                    $userPromotionPrice = $resUserPrice->price;
                                    if ($data['price'] > $data['promotion_price']) {
                                        ?>
                                        <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                                        <span class="old-price product-price"><?= Formulas::priceWrapper($userPrice) ?></span>
                                    <?
                                    } else {
                                        ?>
                                        <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                                    <? } ?>
                                <meta itemprop="priceCurrency" content="1" />
                            </div>

                        </div>
                        <div class="right-block">

                            <h5 itemprop="name">
                                <br/>
                            </h5>




                            <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                <div class="star_content clearfix">
                                    <div class="star star_on"></div>
                                    <div class="star star_on"></div>
                                    <div class="star star_on"></div>
                                    <div class="star"></div>
                                    <div class="star"></div>
                                    <meta itemprop="worstRating" content = "0" />
                                    <meta itemprop="ratingValue" content = "2" />
                                    <meta itemprop="bestRating" content = "5" />
                                </div>
                                <!--<span class="nb-comments">1 Review(s)</span>-->
                            </div>

                            <div class="button-container">
                                <a class="button btn btn-default" href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" rel="nofollow" title="Заказать товар" data-id-product="3">
                                    <span>Заказать</span>
                                </a>



                            </div>

                        </div>

                    </div>
                </li>

                <?php
                if ($newLine == 4) {
                    $newLine = 0;
                }
                ?>
            <?php endforeach; ?>
        </ul>
<?php else: ?>
    <p>Информация загружается.</p>
<?php endif; ?>
