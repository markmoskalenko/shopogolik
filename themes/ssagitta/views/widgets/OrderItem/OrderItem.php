<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="OrderItem.php">
 * </description>
 * Виджет отображения лота заказа
 * var $item = OrdersItems#1
 * (
 * [status_text] => 'Не обработан'
 * [calculated_lotPrice] => 330
 * [calculated_lotWeight] => 2400
 * [calculated_lotExpressFee] => 0
 * [calculated_actualPrice] => '0.00'
 * [calculated_actualWeight] => '0.00'
 * [calculated_actualExpressFee] => '0.00'
 * [calculated_operatorProfit] => 0
 * [vars] => array
 * (
 * 'num' => FormulasVar#2
 * (
 * [val] => '6'
 * [description] => 'Заказанное количество штук товаров в лоте'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'actual_num' => FormulasVar#3
 * (
 * [val] => '6'
 * [description] => 'Фактическое количество штук товаров в лоте'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'calculated_lotPrice' => FormulasVar#4
 * (
 * [val] => 330
 * [description] => 'Стоимость закупки лота в заказе, в юанях'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'calculated_lotExpressFee' => FormulasVar#5
 * (
 * [val] => 0
 * [description] => 'Стоимость доставки лота в заказе, в юанях'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'calculated_lotWeight' => FormulasVar#6
 * (
 * [val] => 2400
 * [description] => 'Вес лота в заказе'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'calculated_actualPrice' => FormulasVar#7
 * (
 * [val] => 0
 * [description] => 'Расчётная стоимость одного товара лота'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'calculated_actualWeight' => FormulasVar#8
 * (
 * [val] => 0
 * [description] => 'Расчётный вес одного товара лота'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'calculated_actualExpressFee' => FormulasVar#9
 * (
 * [val] => 0
 * [description] => 'Расчётная стоимость доставки одного товара лота'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'billing_use_operator_account' => FormulasVar#10
 * (
 * [val] => true
 * [description] => 'Начислять ли бонусы оператору'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * 'billing_operator_profit_model' => FormulasVar#11
 * (
 * [val] => 'FROM_PURCHASE=0.1&FROM_TOTAL=0.05'
 * [description] => 'Параметры начисления бонусов'
 * [params] => array(...)
 * [vars] => array(...)
 * )
 * )
 * [CActiveRecord:_new] => false
 * [CActiveRecord:_attributes] => array
 * (
 * 'id' => '9594'
 * 'oid' => '1916'
 * 'iid' => '15582293584'
 * 'pic_url' => 'http://img02.taobaocdn.com/bao/uploaded/i2/888549075/T2OdO_XzVXXXXXXXXX-888549075.jpg'
 * 'sku_id' => '41572369030'
 * 'props' => '20518:28419;1627207:28338'
 * 'title' => '夏季女装运动裤女薄款运动休闲裤女纯棉卫裤加长裤子直筒宽松显瘦'
 * 'seller_nick' => '凯瑟雅服饰'
 * 'seller_id' => '888549075'
 * 'status' => '1'
 * 'num' => '6'
 * 'weight' => '400'
 * 'express_fee' => '0'
 * 'input_props' => '[{\"name\":\"<translation editable=\\\"1\\\" translated=\\\"0\\\" url=\\\"\\/site\\/translate\\\" type=\\\"prepareProps\\\" from=\\\"zh-CHS\\\" to=\\\"ru\\\" uid=\\\"0\\\" id=\\\"plain0\\\" title=\\\"plain[0]: \\u5c3a\\u5bf8\\\" >\\u5c3a\\u5bf8<translate  onclick=\\\"editTranslation(event,this.parentNode,\'plain\',\'0\'); stopPropagation(event); return false;\\\" ><span class=\\\"ui-icon ui-icon-pencil\\\"><span><\\/translate><\\/translation>\",\"value\":\"<translation editable=\\\"1\\\" translated=\\\"0\\\" url=\\\"\\/site\\/translate\\\" type=\\\"prepareProps\\\" from=\\\"zh-CHS\\\" to=\\\"ru\\\" uid=\\\"0\\\" id=\\\"plain0\\\" title=\\\"plain[0]: XXL\\u7801\\\" >XXL\\u7801<translate  onclick=\\\"editTranslation(event,this.parentNode,\'plain\',\'0\'); stopPropagation(event); return false;\\\" ><span class=\\\"ui-icon ui-icon-pencil\\\"><span><\\/translate><\\/translation>\",\"name_zh\":\"\\u5c3a\\u5bf8\",\"value_zh\":\"XXL\\u7801\"},{\"name\":\"<translation editable=\\\"1\\\" translated=\\\"0\\\" url=\\\"\\/site\\/translate\\\" type=\\\"prepareProps\\\" from=\\\"zh-CHS\\\" to=\\\"ru\\\" uid=\\\"0\\\" id=\\\"plain0\\\" title=\\\"plain[0]: \\u989c\\u8272\\u5206\\u7c7b\\\" >\\u989c\\u8272\\u5206\\u7c7b<translate  onclick=\\\"editTranslation(event,this.parentNode,\'plain\',\'0\'); stopPropagation(event); return false;\\\" ><span class=\\\"ui-icon ui-icon-pencil\\\"><span><\\/translate><\\/translation>\",\"value\":\"<translation editable=\\\"1\\\" translated=\\\"0\\\" url=\\\"\\/site\\/translate\\\" type=\\\"prepareProps\\\" from=\\\"zh-CHS\\\" to=\\\"ru\\\" uid=\\\"0\\\" id=\\\"plain0\\\" title=\\\"plain[0]: \\u590f\\u5b63\\u8584\\u6b3e\\u6d45\\u7070\\u8272\\\" >\\u590f\\u5b63\\u8584\\u6b3e\\u6d45\\u7070\\u8272<translate  onclick=\\\"editTranslation(event,this.parentNode,\'plain\',\'0\'); stopPropagation(event); return false;\\\" ><span class=\\\"ui-icon ui-icon-pencil\\\"><span><\\/translate><\\/translation>\",\"name_zh\":\"\\u989c\\u8272\\u5206\\u7c7b\",\"value_zh\":\"\\u590f\\u5b63\\u8584\\u6b3e\\u6d45\\u7070\\u8272\"}]'
* 'taobao_price' => '118'
 * 'taobao_promotion_price' => '55'
 * 'tid' => null
 * 'track_code' => null
 * 'actual_num' => '6'
 * 'actual_lot_weight' => null
 * 'actual_lot_express_fee' => null
 * 'actual_lot_price' => null
 * )
 * [CActiveRecord:_related] => array()
 * [CActiveRecord:_c] => null
 * [CActiveRecord:_pk] => '9594'
 * [CActiveRecord:_alias] => 't'
 * [CModel:_errors] => array()
 * [CModel:_validators] => null
 * [CModel:_scenario] => 'update'
 * [CComponent:_e] => null
 * [CComponent:_m] => null
 * )
 * var $order = false
 * var $readOnly = true
 * var $allowDelete = false
 * var $adminMode = false
 * var $imageFormat = '_200x200.jpg'
 * var $publicComments = true
 * var $lazyLoad = true
 **********************************************************************************************************************/
?>
<?
Yii::app()->clientScript->registerScriptFile(
    $this->frontThemePath . '/js/ui/' . (YII_DEBUG ? 'jquery.ui.spinner.js' : 'minified/jquery.ui.spinner.min.js'),
    CClientScript::POS_BEGIN
);
$inAdmin = strpos($_SERVER['SCRIPT_URL'], '/admin/');
?>
<script>
    $(document).ready(function(){
        $('a#down').click(function(){
            var count = $(this).next('.count');
            var val   = parseInt($(count).val())-1;
            if(val < 1) val = 1;
            $(count).val(val);
        });
        $('a#up').click(function(){
            var count = $(this).prev('.count');
            $(count).val(parseInt($(count).val())+1);
        });
        $('a.minus').click(function(){
            var count = $(this).next('.count');
            var val   = parseInt($(count).val())-1;
            if(val < 1) val = 1;
            $(count).val(val);
        });
        $('a.plus').click(function(){
            var count = $(this).prev('.count');
            $(count).val(parseInt($(count).val())+1);
        });

    });
</script>

<tr id="product_47_0_0_0" class="cart_item last_item first_item address_0 odd">
    <td class="cart_product">
        <a href="<?= Yii::app()->createUrl(
            '/item/index',
            array('iid' => $item->iid)
        ) ?>" <?= ($inAdmin) ? 'target="_blank"' : '' ?>>
            <img class="preview" src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""width="98" height="98"/>
        </a>
    </td>

    <td class="cart_description">
        <? if (!is_a($item, 'customCart')) { ?>
            <div style="display: inline-block; vertical-align: top">
                <strong><?= Yii::t('main', 'Лот №') ?>:</strong> <?= $item->oid . '-' . $item->id ?> <strong><?= Yii::t('main', 'Статус') ?>:</strong> <span
                    class="status-text<?= (in_array(
                        $item->status,
                        OrdersItemsStatuses::getOrderItemExcludedStatusesArray()
                    )) ? ' red' : '' ?>"><?= $item->status_text; ?></span>
            </div>
        <? } ?>
        <br>
        <div style="display: inline-block; vertical-align: top">
            <?=
            (is_a($item, 'customCart') || (strpos($item->title, '<translation') === 0)) ? $item->title : Yii::app(
            )->DanVitTranslator->translateText($item->title, 'zh-CHS', Utils::TransLang()); ?>
            <? if (isset($item->input_props_array)) {
                $input_props_array = $item->input_props_array;
            } else {
                $input_props_array = json_decode($item->input_props);
            }
            if (!$input_props_array || ($input_props_array == null)) {
                $input_props_array = array();
            }
            foreach ($input_props_array as $name => $value) {
                ?><br>
                <strong><?=
                    (is_a($item, 'customCart') ||
                        (preg_match(
                            '/editTranslation|<translation/s',
                            $value->name
                        ))) ? $value->name : Yii::app()->DanVitTranslator->translateText(
                        $value->name,
                        'zh-CHS',
                        Utils::TransLang()
                    ); ?></strong>:<b>
                    <?=
                    (is_a($item, 'customCart') || (preg_match(
                            '/editTranslation|<translation/s',
                            $value->value
                        ))) ? $value->value : Yii::app()->DanVitTranslator->translateText(
                        $value->value,
                        'zh-CHS',
                        Utils::TransLang()
                    ); ?></b>
            <? } ?>
        </div>
        <br>
        <? if (is_a($item, 'customCart')) { ?>
            <textarea placeholder="<?= Yii::t('main', 'Ваш комментарий к лоту') ?>" style="width: 430px; border: 1px solid #ddd; padding: 10px;"
                      name="description[<?= $item->id ?>]"><?= $item->desc ?></textarea>
        <? } ?>
        <input type="hidden" name="iid[<?= $item->id ?>]" value="<?= $item->iid ?>"/>
        <input type="hidden" name="params[<?= $item->id ?>]"
               value="<?= (isset($item->props)) ? $item->props : $item->input_props ?>"/>

    </td>
    <td class="cart_avail">
        <?php if(Yii::app()->request->requestUri != '/checkout/payment'):?>
        <a rel="nofollow" class="cart_quantity_down btn btn-default button-minus" style="position: absolute; margin-top: 30px;" >
            <span><i class="icon-minus"></i></span>
        </a>
        <?php endif;?>
        <input <?= ($readOnly) ? 'readonly' : '' ?> type="text" name="weight[<?= $item->id ?>]"
                                                    class="cart_quantity_input form-control grey count"
                                                    autocomplete="off"
                                                    size="2"
                                                    id="weight<?= $item->id ?>"
                                                    style="width: 57px;margin-bottom: 3px;text-align: center;"
                                                    value="<?= (isset($item->calculated_actualWeight) && $item->calculated_actualWeight) ? $item->calculated_actualWeight : $item->weight ?>"
            <?= (DSConfig::getVal('checkout_weight_needed') != 1) ? 'hidden="hidden"' : '' ?>/>
        <?php if(Yii::app()->request->requestUri != '/checkout/payment'):?>
        <a rel="nofollow" class="cart_quantity_up btn btn-default button-plus"  style="margin-left: 30px;"><span><i class="icon-plus"></i></span></a>
        <?php endif;?>
    </td>
    <td class="cart_unit" data-title="Unit price">

        <? if (is_a($item, 'customCart')) {
            $taobao_price = $item->price_no_discount;
            $sum = $item->sum;
            $sumResUserPrice = $item->sumResUserPrice;
            $sum_no_discount = $item->sum_no_discount;
            $sum_no_discountResUserPrice = $item->sum_no_discountResUserPrice;
        } else {
            $actualPrice = (
            (isset($item->calculated_actualPrice) && ($item->calculated_actualPrice > 0))
                ? $item->calculated_actualPrice
                : $item->taobao_promotion_price);
            $actualExpressFee = (
            (isset($item->calculated_actualExpressFee) && ($item->calculated_actualExpressFee) > 0)
                ? $item->calculated_actualExpressFee
                : $item->express_fee);
            $actualNum = ((isset($item->actual_num) && $item->actual_num) ? $item->actual_num : $item->num);
            $resUserPrice = Formulas::getUserPrice(
                array(
                    'price'       => $item->taobao_price,
                    'count'       => 1,
                    'deliveryFee' => $item->express_fee,
                    'postageId'   => 0,
                    'sellerNick'  => false,
                )
            );
            $taobao_price = $resUserPrice->price;
            $resUserPrice = Formulas::getUserPrice(
                array(
                    'price'       => $actualPrice,
                    'count'       => $actualNum,
                    'deliveryFee' => $actualExpressFee,
                    'postageId'   => 0,
                    'sellerNick'  => false,
                )
            );
            $sum = $resUserPrice->price;
            $sumResUserPrice = $resUserPrice;
            $resUserPrice = Formulas::getUserPrice(
                array(
                    'price'       => $item->taobao_price,
                    'count'       => $actualNum,
                    'deliveryFee' => $item->express_fee,
                    'postageId'   => 0,
                    'sellerNick'  => false,
                )
            );
            $sum_no_discount = $resUserPrice->price;
            $sum_no_discountResUserPrice = $resUserPrice;
            if (!is_null($item->actual_lot_express_fee)) {
                $actualLotExpressFeeInCurrency = Formulas::convertCurrency(
                    $item->actual_lot_express_fee,
                    'cny',
                    DSConfig::getCurrency()
                );
            } else {
                $actualLotExpressFeeInCurrency = Formulas::convertCurrency(
                    $actualExpressFee * $actualNum,
                    'cny',
                    DSConfig::getCurrency()
                );
            }
        }?>
        <span style="color: #e75768;font-size: 20px;"><?= Formulas::priceWrapper($taobao_price) ?></span>
    </td>

    <td class="cart_quantity text-center">

        <input type="hidden" value="1" name="quantity_47_0_0_0_hidden">
        <?php if(Yii::app()->request->requestUri != '/checkout/payment'):?>
        <a rel="nofollow" class="cart_quantity_down btn btn-default button-minus" id="cart_quantity_down_47_0_0_0" style="position: absolute; margin-top: 30px; margin-left: -10px;" title="Убрать">
            <span><i class="icon-minus"></i></span></a>
        <?php endif;?>
        <input <?= ($readOnly) ? 'readonly' : '' ?> type="text" name="num[<?= $item->id ?>]"
                                                    autocomplete="off"
                                                    size="2"
                                                    class="cart_quantity_input form-control grey"
                                                    id="num<?= $item->id ?>"
                                                    style="width: 57px;margin-bottom: 3px;"
                                                    value="<?= (isset($item->actual_num) && $item->actual_num) ? $item->actual_num : $item->num ?>"/>
        <?php if(Yii::app()->request->requestUri != '/checkout/payment'):?>
        <a rel="nofollow" class="cart_quantity_up btn btn-default button-plus" id="cart_quantity_up_47_0_0_0"  style="margin-left: -52px" title="Добавить"><span><i class="icon-plus"></i></span></a>
        <?php endif;?>
    </td>
    <td class="cart_total" data-title="Total">
        <? if ($sum != $sum_no_discount) { ?>
        <s style="text-decoration: none;">
            <? } ?>
            <span
                title="<?=
                (Yii::app()->user->role == 'superAdmin') ? $sum_no_discountResUserPrice->report() : ''; ?>" ><span class="price"><?=
                    Formulas::priceWrapper(
                        (isset($item->status) && (in_array(
                                $item->status,
                                OrdersItemsStatuses::getOrderItemExcludedStatusesArray()
                            ))) ? 0 : $sum_no_discount
                    ) ?></span></span>
            <? if ($sum != $sum_no_discount) { ?>
        </s>
    <? } ?>

    </td>
    <td class="cart_delete text-center" data-title="Delete">
            <a href="<?= Yii::app()->createUrl('/cart/delete', array('id' => $item->id)) ?>" class="button_delete">
                <i class="icon-trash"></i></a>
    </td>
</tr>