<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="userBlock.php">
 * </description>
 * Рендеринг блока пользователя в лэйауте
 * var $guest = false
 * var $user = 'Root'
 **********************************************************************************************************************/
?>
<? if ($guest) { ?>
    <a class="space" href="<?= Yii::app()->createUrl('/user/register') ?>"><?= Yii::t('main', 'Регистрация') ?></a>
    <a class="space" href="<?= Yii::app()->createUrl('/user/login') ?>"><?= Yii::t('main', 'Войти') ?></a>
<?
} else {
    ?>
    <div class="">

        <a class="space" href="<?= Yii::app()->createUrl('/cabinet') ?>"><?= Yii::t('main', 'Привет') ?>
            &nbsp;<?= $user ?></a>
        <a class="space" href="<?= Yii::app()->createUrl('/cabinet') ?>"><?= Yii::t('main', 'На счету') ?>
            &nbsp;<?=
            Formulas::priceWrapper(
              Formulas::convertCurrency(
                Users::getBalance(Yii::app()->user->id),
                DSConfig::getVal('site_currency'),
                DSConfig::getCurrency()
              )
            ) ?></a>
        <a class="space" href="<?= Yii::app()->createUrl('/user/logout') ?>"><?= Yii::t('main', 'Выйти') ?></a>

    </div>
<? } ?>