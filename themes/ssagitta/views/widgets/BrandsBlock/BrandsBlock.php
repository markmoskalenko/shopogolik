<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="BrandsBlock.php">
 * </description>
 * Виджет отображает карусель логотипов брендов на главной
 * $brands - массив моделей Brands
 * = Array of
 * Array(
 * [id] => 373
 * [name] => Fossil
 * [enabled] => 1
 * [img_src] => brand_logo_27.jpg
 * [vid] => 46470
 * [query] => Fossil
 * [url] => fossil
 * )
 **********************************************************************************************************************/
?>
<? if ($brands) {
    if ($this->beginCache(
      'brandsBlock',
      array(
        'duration' => (YII_DEBUG ||
          (Yii::app()->user->inRole(array('contentManager', 'superAdmin')))
          ) ? 10 : 1200
      )
    )
    ) {
        ?>
            <div class="container">
                <div class="container-inner">
                    <div class="pos-logo">
                        <ul class="bxslider">
                            <? foreach ($brands as $brand) {
                                if (!$brand->img_src) continue;
                                ?>

                                <li>
                                    <a href ="<?= Yii::app()->createUrl('/brand/index', array('name' => $brand->url)) ?>">
                                        <img src ="<?= Yii::app()->request->baseUrl ?>/images/brands/<?= $brand->img_src; ?>" alt ="<?= CHtml::encode($brand->name); ?>" title="<?= $brand->name ?>" />
                                    </a>
                                </li>
                            <? } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $('.pos-logo .bxslider').bxSlider({
                    auto: 1,
                    slideWidth:170,
                    autoHover: true,
                    slideMargin: 25,
                    minSlides: 1,
                    maxSlides: 6,
                    speed:  2000,
                    pause: 2000,
                    controls: 1,
                    pager: false,
                });
            </script>
        <? $this->endCache();
    }
}
?>