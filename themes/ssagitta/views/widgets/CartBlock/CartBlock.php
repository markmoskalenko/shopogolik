<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="CartBlock.php">
 * </description>
 * Виджет отображает состояние корзины в лэйауте
 * cart =
 * stdClass Object
 * (
 * [cartRecords] => Array
 * ()
 * [cartRecordsDataProvider] => CArrayDataProvider Object
 * (
 * [totalDiscount] => 0 - общая сумма скидки по корзине
 * [total] => 0 - сумма корзины
 * [totalNoDiscount] => 0 - сумма без скидок
 * [totalWeight] => 0 - общий вес корзины
 * [allowOrder] => 1 - можно ли оформлять заказ
 * [summAddToAllowOrder] => 0 - сумма, недостающая до оформления зказа
 * )
 **********************************************************************************************************************/
?>

<a href="#" title="Ваша корзина" class="mini_cart" rel="nofollow">
    <b><?= Yii::t('main', 'В корзине товаров: ') ?></b>
    <span class="ajax_cart_quantity unvisible"><?= count($cart->cartRecords) ?></span>
    <!--    <span class="ajax_cart_product_txt unvisible">Item-</span>-->
    <!--    <span class="ajax_cart_product_txt_s unvisible">Items-</span>-->
                    <span class="ajax_cart_total unvisible">
                                    </span>

    <span class="ajax_cart_no_product"><?= count($cart->cartRecords) ?> шт. - </span>
    <span class="price cart_block_total ajax_block_cart_total"><?= Formulas::priceWrapper($cart->total) ?></span>
</a>

<div class="cart_block block exclusive">

    <div class="cart-arrow"></div>
    <div class="block_content">
        <!-- block list of products -->
        <div class="cart_block_list">
            <?php if (!count($cart->cartRecords)): ?>
                <p class="cart_block_no_products">
                    Корзина пуста
                </p>
            <?php endif; ?>


            <div class="cart-prices">

                <div class="cart-prices-line first-line">

                    <span class="price cart_block_shipping_cost ajax_cart_shipping_cost">Цена</span>
                    <span class="title"></span>
                </div>

                <?php if (count($cart->cartRecords)): ?>
                    <?php foreach($cart->cartRecords as $item): ?>
                        <span> <?=$item->num.' x';?>
						    <span>
								<a href="<?= Yii::app()->createUrl('/cart/delete', array('id' => $item->id)) ?>" rel="nofollow" title="удалить товар из корзины">&nbsp;
                                    <i class="icon-trash"></i></a>
							</span>
						</span>
                        <div class="cart-prices-line last-line" style="border-bottom: 1px solid #eee;">

                            <span class="price cart_block_total ajax_block_cart_total"><?= Formulas::priceWrapper($item->price) ?></span>
                            <span class="title">
                                <a href="#">
                                    <img src="<?= $item->pic_url?>" width="60px">
                                </a>
                            </span>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

                <br/>
                <div class="cart-prices-line last-line">
                    <span class="price cart_block_total ajax_block_cart_total"><?= Formulas::priceWrapper($cart->total) ?></span>
                    <span class="title"><b>Итого:</b></span>
                </div>
            </div>
            <p class="cart-buttons">
                <a id="button_order_cart" class="btn btn-default button button-small" href="<?= Yii::app()->createUrl('/cart/index') ?>" title="Оформить заказ" rel="nofollow" style="width: 100px;margin-left: 85px;">
                    <span>Оформить</span>
                </a>
            </p>
        </div>
    </div>
</div>