<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="currencyBlock.php">
 * </description>
 * Виджет переключателя валют фронта сайта
 * $currency = cny - текущая выбранная валюта
 **********************************************************************************************************************/
?>
<!--<div class="currency">--><?//= Yii::t('main', 'Валюта') ?><!--:-->
<!--    <select class="currency-sel" name="currency" onchange="location = this.options[this.selectedIndex].value;">-->
<!--        --><?// $currency_array = explode(',', DSConfig::getVal('site_currency_block'));
//        foreach ($currency_array as $val) {
//            ?>
<!--            <option-->
<!--              value="/user/setcurrency/curr/--><?//= $val ?><!--" --><?// if ($currency == $val) {
//                echo ' selected';
//            } ?><!--><?//= strtoupper($val) ?><!--</option>-->
<!--        --><?// } ?>
<!--    </select>-->
<!--</div>-->


<div id="languages-block-top" class="fx-fadeInDown">
    <form id="setCurrency" action="" method="post">
        <span class="cur-label">Валюта :</span>

        <div class="current">
            <input type="hidden" name="id_currency" id="id_currency" value=""/>
            <input type="hidden" name="SubmitCurrency" value=""/>

            <strong><?= strtoupper($currency) ?></strong></div>
        <ul id="first-currencies" class="currencies_ul toogle_content">
            <? $currency_array = explode(',', DSConfig::getVal('site_currency_block'));
            foreach ($currency_array as $val) {
            ?>
                <li <? if ($currency == $val) { echo 'class="selected"';} ?>>
                    <a href="<?= Yii::app()->createUrl('/user/setcurrency/curr/'.$val) ?>" rel="nofollow"">
                        <span><?= strtoupper($val) ?></span>
                    </a>
                </li>
            <? } ?>
        </ul>
    </form>
</div>