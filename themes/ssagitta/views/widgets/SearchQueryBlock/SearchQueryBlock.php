<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchQueryBlock.php">
 * </description>
 * Виджет, реализующий поисковую строку
 * var $cats = - список категорий для фильтра
 * array
 * (
 * 0 => array
 * (
 * 'pkid' => '2'
 * 'cid' => '0'
 * 'parent' => '1'
 * 'status' => '1'
 * 'url' => 'mainmenu-odezhda'
 * 'query' => '女装男装'
 * 'level' => '2'
 * 'order_in_level' => '200'
 * 'view_text' => 'Одежда'
 * 'children' => array
 * (
 * 3 => array(...)
 * 18 => array(...)
 * 31 => array(...)
 * 41 => array(...)
 * 49 => array(...)
 * 60 => array(...)
 * 70 => array(...)
 * 81 => array(...)
 * )
 * )
 * )
 * var $query = '' - поисковый запрос
 * var $cid = '' - cid категории
 **********************************************************************************************************************/
?>
<?
Yii::app()->clientScript->registerCssFile($this->frontThemePath . '/css/chosen.css');
Yii::app()->clientScript->registerScriptFile(
  $this->frontThemePath . '/js/chosen.jquery.min.js',
  CClientScript::POS_BEGIN
);
Yii::app()->clientScript->registerScriptFile(
  $this->frontThemePath . '/js/' . (YII_DEBUG ? 'jquery.autocomplete.js' : 'jquery.autocomplete.min.js'),
  CClientScript::POS_BEGIN
);
?>

<!-- Block search module TOP -->
<div id="search_block_top" class="col-xs-12 col-sm-12 col-md-7">
    <?= CHtml::beginForm(array('/search/index'), 'get', array('id' => 'main-search-block')) ?>
<!--        <input type="hidden" name="controller" value="search"/>-->
<!--        <input type="hidden" name="orderby" value="position"/>-->
<!--        <input type="hidden" name="orderway" value="desc"/>-->

        <input class="search_query form-control" type="text" id="search_query_top" name="query"
               value="Например: женская блузка" value="<?= $query ?>"/>

        <button type="submit" name="submit_search" class="btn btn-default button-search">
            <span>Search</span>
        </button>
    <?= CHtml::endForm() ?>
</div>
<script type="text/javascript">

    $('#search_query_top').on('focus', function () {
        var $this = $(this);
        if ($this.val() == 'Search...') {
            $this.val('');
        }
    }).on('blur', function () {
        var $this = $(this);
        if ($this.val() == '') {
            $this.val('Search...');
        }
    });
</script>















