<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 * Рендеринг списка товаров в поисковой выдаче посредством CListView
 *
 * var $showControl - показывать кнопки
 * var $disableItemForSeo
 * var $imageFormat
 * var $controlAddToFavorites - кнопка Добавить в избранное
 * var $controlAddToFeatured - кнопка Добавить в рекомендованное
 * var $controlDeleteFromFavorites - кнопка Удалить (из избранного, в частности)
 * var $lazyLoad
 **********************************************************************************************************************/
?>
<ul class="pos_animated product_list grid row favorite">
    <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 fx-fadeInDown a0  first-in-line first-item-of-tablet-line first-item-of-mobile-line favorite">
        <div class="product-container" itemscope itemtype="http://schema.org/Product">
            <div class="left-block">
                <div class="product-image-container">
                    <a class="product_img_link"	href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" title="" itemprop="url">
                        <img class="replace-2x img-responsive" src="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt="" title=""   itemprop="image" />
                    </a>
                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
                    <span itemprop="price" class="price product-price">
                        <?
                        $resUserPrice = Formulas::getUserPrice(
                             array(
                                'price'       => $data['price'],
                                'count'       => 1,
                                'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                'postageId'   => false,
                                'sellerNick'  => false,
                            )
                        );
                        $userPrice = $resUserPrice->price;
                        $resUserPrice = Formulas::getUserPrice(
                            array(
                                'price'       => $data['promotion_price'],
                                'count'       => 1,
                                'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                                'postageId'   => false,
                                'sellerNick'  => false,
                            )
                        );
                        $userPromotionPrice = $resUserPrice->price;
                        if ($data['price'] > $data['promotion_price']) {
                            ?>
                            <?= Formulas::priceWrapper($userPromotionPrice) ?>
                            <div class="product-promotion">&nbsp;-&nbsp;<?= $userPrice - $userPromotionPrice; ?></div>
                        <?
                        } else {
                            ?>
                            <?= Formulas::priceWrapper($userPromotionPrice) ?>
                        <? } ?>
                    </span>
                        <meta itemprop="priceCurrency" content="1" />
                    </div>
                <span class="new-box">
                    <span class="new-label">New</span>
                </span>
                </div>
            </div>
            <div class="right-block">
                <h5 itemprop="name">
                    <a class="product-name" href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>" title="Etiam at metus" itemprop="url" >
                        Etiam at metus
                    </a>
                </h5>
                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">

                    <?
                    $resUserPrice = Formulas::getUserPrice(
                        array(
                            'price'       => $data['price'],
                            'count'       => 1,
                            'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                            'postageId'   => false,
                            'sellerNick'  => false,
                        )
                    );
                    $userPrice = $resUserPrice->price;
                    $resUserPrice = Formulas::getUserPrice(
                        array(
                            'price'       => $data['promotion_price'],
                            'count'       => 1,
                            'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
                            'postageId'   => false,
                            'sellerNick'  => false,
                        )
                    );
                    $userPromotionPrice = $resUserPrice->price;
                    if ($data['price'] > $data['promotion_price']) {
                        ?>
                        <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                        <span class="old-price product-price"><?= Formulas::priceWrapper($userPrice) ?></span>
                        <!--                        --><?//= Formulas::priceWrapper($userPromotionPrice) ?>
                        <!--                        <div class="product-promotion">&nbsp;-&nbsp;--><?//= $userPrice - $userPromotionPrice; ?><!--</div>-->
                    <?
                    } else {
                        ?>
                        <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($userPromotionPrice) ?></span>
                    <? } ?>
                    <meta itemprop="priceCurrency" content="1" />
                </div>
                <div class="button-container">
                    <a class="button ajax_add_to_cart_button btn btn-default" href="ordera88b.html?add=1&amp;id_product=35&amp;token=8656ddc2d74502f428b649056b76a290" rel="nofollow" title="Add to cart" data-id-product="35">
                        <span>Купить товар</span>
                    </a>
                    <!--                <div class="wishlist">-->
                    <!--                    <a class="addToWishlist wishlistProd_35" href="#" rel="35" onclick="WishlistCart('wishlist_block_list', 'add', '35', false, 1); return false;">-->
                    <!--                        + Wishlist-->
                    <!--                    </a>-->
                    <!--                </div>-->
                    <!--                <a class="quick-view" href="--><?//= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?><!--" rel="home/35-etiam-at-metus.html">-->
                    <!--                    <span>+ Quick view</span>-->
                    <!--                </a>-->
                </div>
                <!--            <p class="product-desc" itemprop="description">-->
                <!--                Faded short sleeve t-shirt with high neckline. Soft and stretchy material for a comfortable fit. Accessorize with a straw hat and you're ready for summer!-->
                <!--            </p>-->
            </div>
        </div><!-- .product-container> -->
    </li>
</ul>
