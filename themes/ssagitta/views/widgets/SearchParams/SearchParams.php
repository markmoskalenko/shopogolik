<?  if ($this->beginCache(
    'SearchParams-' . implode('-', $params),
    array(
        'duration' => (YII_DEBUG ||
            (in_array(Yii::app()->user->getRole(), array('contentManager', 'superAdmin')))
        ) ? 10 : 1200
    )
)
) : ?>
    <div id="layered_block_left">
        <div class="title_block"><h4>Фильтр</h4></div>
        <div class="block_content">
            <form class="form-search" id="layered_form"
                  action="<?= Yii::app()->createUrl('/' . $type . '/index', $params) ?>" method="post">
                <div>
                    <? foreach ($filters as $prop) : ?>
                        <? if (isset($prop->values[0])) : ?>
                            <? $pidvid = explode(':', str_replace('%3A', ':', $prop->values[0]->values_props)); ?>
                            <? $pid = $pidvid[0]; ?>
                            <? if (count($prop->values) > 4) : ?>
                                <div class="layered_filter">
                                    <div class="layered_subtitle_heading">
                                        <span class="layered_subtitle"><?= $prop->title ?></span>
                                    </div>
                                    <ul id="ul_layered_category_<?= $pid ?>" class="col-lg-12 layered_filter_ul">

                                            <? for ($j = $i; $j < count($prop->values); $j++) : ?>
                                                <? if (isset($prop->values[$j])) :
                                                    $pidvid = explode(':', str_replace('%3A', ':', $prop->values[$j]->values_props));
                                                    $vid = $pidvid[1];
                                                    ?>
                                                    <li class="nomargin">
                                                        <? if (isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                                            $ch = ' checked';
                                                        } else {
                                                            $ch = ' ';
                                                        } ?>
                                                        <div class="checker" id="uniform-layered_category_<?= $pid ?>">
                                                                <label>
                                                                    <span>
                                                                    <input
                                                                           class="radio"
                                                                           type="radio" name="props[<?= $pid ?>]"
                                                                           value="<?= $vid ?>"<?= $ch ?>>
                                                                    </span>
                                                                        <?= $prop->values[$j]->values_title ?>
                                                                    <? if (isset($prop->values[$j]->values_count) && ($prop->values[$j]->values_count > 0)) : ?>
                                                                        (<?= $prop->values[$j]->values_count ?>)
                                                                    <? endif; ?>
                                                                </label>

                                                        </div>
                                                    </li>
                                                <? endif; ?>
                                            <? endfor; ?>

                                    </ul>
                                </div>
                            <? endif; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <div class="form-item">
                        <div class="layered_price" style="">
                            <div class="layered_subtitle_heading">
                                <span class="layered_subtitle"><?= Yii::t('main', 'Цена (без доставки)') ?>:</span>
                            </div>
                            <ul id="ul_layered_price_0" class="col-lg-12 layered_filter_ul">
                                <label for="price">
                                    Интервал:
                                </label>
                                <span id="layered_price_range"><input type="text" size="4" class="small_input" name="price_min" id="price-min"
                                                                      value="<?= (isset($params['price_min'])) ? CHtml::encode($params['price_min']) : 0 ?>"/> -
                                <input type="text" size="4" class="small_input" name="price_max" id="price-max"
                                       value="<?= (isset($params['price_max'])) ? CHtml::encode($params['price_max']) : '' ?>"/>
                                </span>

                                <span><?= DSConfig::getCurrency(true) ?></span>

                            </ul>
                            <div id="layered_block_left">
                            <div id="price-slider" class="layered_slider_container"></div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id_category_layered" value="3">
                </div>
                <a style="margin-top: 20px" href="javascript:void(0);" onclick="$('#layered_form').submit(); return false;"
                   class="button btn btn-default button-medium search-terms-sbmt">
                    <?= Yii::t('main', 'Поиск') ?>
                </a>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        $('#layered_form .checker input').click(function () {
            var id = $(this).parents('ul.layered_filter_ul')[0].id;
            $('#' + id + ' .checked').removeClass('checked');
            if ($(this).parent().hasClass('checked')) {
                $(this).parent().removeClass('checked');
            } else {
                $(this).parent().addClass('checked');
            }
        });
        $(function() {



            $( "#price-slider" ).slider({
                range: "min",
                step: 1,
                min: <?= (isset($params['price_min'])) ? CHtml::encode($params['price_min']) : 10 ?>,
                max: <?= (isset($params['price_max'])) ? CHtml::encode($params['price_max']) : 30000 ?>,
                values: [ <?= (isset($params['price_min'])) ? CHtml::encode($params['price_min']) : 0 ?>, <?= (isset($params['price_max'])) ? CHtml::encode($params['price_max']) : 100000 ?> ],

                slide: function( event, ui ) {
                    $('#price-min').val(ui.values[0]);
                    $('#price-max').val(ui.values[1]);
                }
            });

            console.log(<?= (isset($params['price_max'])) ? CHtml::encode($params['price_max']) : 100000 ?>);

        });
    </script>
    <? $this->endCache(); ?>
<? endif; ?>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
