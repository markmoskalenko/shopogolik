<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="languageBlock.php">
 * </description>
 * Виджет переключателя языков фронта сайта
 * $lang_array
 **********************************************************************************************************************/
?>
<?= Yii::t('main', 'Язык') ?>:
<? foreach ($lang_array as $val) { ?>
    <a href="/user/setlang/<?= $val ?>"
       class="<?= $val ?><?= (Yii::app()->language == $val) ? ' active' : '' ?>"><span></span></a>
<? } ?>