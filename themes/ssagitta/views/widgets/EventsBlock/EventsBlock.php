<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="EventsBlock.php">
 * </description>
 * Виджет истории заказа, например, в кабинете - просмотр заказа
 * var $dataProvider =  CActiveDataProvider#1
 * ( [modelClass] => 'EventsLog' [model] => EventsLog#2 )
 * var $blockId = 'events-1916'
 **********************************************************************************************************************/
?>
<div id="accordion-events-<?= $blockId ?>">
    <h3><?= Yii::t('main', 'История заказа') ?>: <?= $dataProvider->totalItemCount ?></h3>

    <div class="events-widget-block">
        <? $this->widget(
          'bootstrap.widgets.TbGridView',
          array(
            'id'           => 'grid-' . $blockId,
            'dataProvider' => $dataProvider,
            'type'         => 'striped bordered condensed',
            'template'     => '{summary}{items}{pager}',
            'columns'      => array(
              'date',
              'eventName',
              'subject_value',
              'fromName',
            ),
          )
        );
        ?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
//        $("#accordion-events-<?//=$blockId?>//").accordion({
//            collapsible: true,
//            active: <?//=($dataProvider->totalItemCount>1) ? '0' : 'false';?>
            //disabled: true
//        });
    });
</script>