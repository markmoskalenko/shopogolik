<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="sliderBlock.php">
 * </description>
 * Виджет отображает слайдер баннеров на главной
 * $banners - массив моделей banners
 **********************************************************************************************************************/
?>
<div class="list-slide">
    <!-- /Block tags module -->
    <div class="pos-special-product pos_animated"  align="center" style="margin: -25px 0 0">
        <ul class="bxslider fx-fadeInDown a3">
            <li class="ajax_block_product first_item item clearfix">
                <div class="item-inner">
                        <!--                    Залить файлы в директорию и испльзовать конструкцию ниже-->
                        <!--                    <img src="--><?//= $this->frontThemePath ?><!--/folder/image.jpeg" alt=""/>-->

                        <!--                    Это подгрузка картинки из стороннего ресурса для примера-->
                        <img src="http://shopogolik24.com/themes/ssagitta/images/banners/search_1.jpg" title="Грандиозные скидки до 80%! Успей купить!"/>
                    <h3><a class="product-name" href="#" title="Printed Summer Dress">Грандиозные скидки до 80%! Успей купить!</a></h3>
                </div>
            </li>

            <li class="ajax_block_product first_item item clearfix">
                <div class="item-inner">
                        <!--                    Залить файлы в директорию и испльзовать конструкцию ниже-->
                        <!--                    <img src="--><?//= $this->frontThemePath ?><!--/folder/image.jpeg" alt=""/>-->

                        <!--                    Это подгрузка картинки из стороннего ресурса для примера-->
                        <img src="http://shopogolik24.com/themes/ssagitta/images/banners/search_2.jpg" title="Оперативная и выгодная доставка до двери!"/>
                    <h3><a class="product-name" href="#" title="Printed Summer Dress">Оперативная и выгодная доставка до двери!</a></h3>
                </div>
            </li>
        </ul>
    </div>

    <script type="text/javascript">
        $('.list-slide .bxslider').bxSlider({
            auto: 1,
            infiniteLoop: true,
            minSlides: 1,
            maxSlides: 1,
            speed: 1000,
            pause: 4000,
            controls: false,
            pager: 1
        });
    </script>
</div>