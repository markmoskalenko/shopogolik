<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 * Виджет, реализующий отображение товара в поисковой выдаче
 * var $item = stdClass#1
 * (
 * [num_iid] => '38289599003' - item id
 * [price] => 115 - цена в юанях
 * [promotion_price] => 115  - цена в юанях, со скидкой
 * [pic_url] => 'http://g.search.alicdn.com/img/bao/uploaded/i4/i2/T1GTIAFutbXXXXXXXX_!!0-item_pic.jpg_220x220.jpg'
 * [nick] => 'suelly2013' - ник продавца
 * [seller_rate] => 0 - рейтинг продавца
 * [post_fee] => 0 - стоимость доставки по Китаю в юанях, обычной почтой
 * [express_fee] => 0 - стоимость доставки по Китаю в юанях, экспресс
 * [ems_fee] => 0 - стоимость доставки по Китаю в юанях, EMS
 * [postage_id] => 0
 * [cid] => 0
 * [tmall] => false - товар с Tmall
 * [html_title] => 'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
 * [popup_title] => 'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
 * [html_alt] => 'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
 * [promotion_percent] => 0 - процент скидки
 * [userPrice] => 267 - конечная цена в текущей валюте
 * [userPromotionPrice] => 267 - конечная цена в текущей валюте, со скидкой
 * )
 * var $newLine = ' first' - разница в рендеринге для виджета товара, начинающего новую строку в выдаче
 * var $showControl = true - отображать ли всякие "Добавить в избранное", "Добавить в рекомендованное" и т.п.
 * var $disableItemForSeo =  true - запрещать индексировать карту товара
 * var $imageFormat = '_160x160.jpg'
 * var $lazyLoad = true - ленивая загрузка изображений
 **********************************************************************************************************************/
?>

<?php
$param = '';
if($number == 1) {
    $param = ' first-in-line last-line first-item-of-tablet-line first-item-of-mobile-line last-mobile-line';
} elseif($number == 4) {
    $param = ' last-line first-item-of-tablet-line last-item-of-mobile-line last-mobile-line';
} else {
    $param = ' last-line last-item-of-tablet-line last-mobile-line';
}
?>


<li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 <?=$param ?>">
    <div class="product-container" itemscope>

        <div class="left-block">
            <div class="product-image-container">
                <a class="product_img_link" itemprop="url" <?php echo ($disableItemForSeo)? 'rel="nofollow"' : ''; ?> href="<?= Yii::app()->createUrl('/item/index', array('iid' => $item->num_iid)) ?>" target="_blank">
                    <img class="replace-2x img-responsive"
                         src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
                </a>

                <?php $userPromotionPrice = $resUserPrice->price; ?>
                <div itemprop="offers" itemscope class="content_price">
                    <?php if($item->price > $item->promotion_price) : ?>
                            <span itemprop="price" class="price product-price" style="font-weight: bold"><?= Formulas::priceWrapper($item->userPromotionPrice) ?></span>
                            <span class="old-price product-price"><?= Formulas::priceWrapper($item->userPrice) ?></span>
                        <?php else : ?>
                            <span itemprop="price" class="price product-price"><?= Formulas::priceWrapper($item->userPromotionPrice) ?></span>
                    <?php endif ; ?>

                    <meta itemprop="priceCurrency" content="1" />
                </div>
                <span class="new-box">
				    <span class="new-label">New</span>
			    </span>
            </div>
        </div>



        <div class="right-block">

            <div class="button-container-list">

            </div>

            <p class="product-desc" itemprop="description">
                <i class="i-rating rating_<?= DSGSeller::getCrownsFromSales($item->seller_rate)-1; ?>"></i>
            </p>
            <h5 style="display: block;font-size: 15px">
                <?=$item->html_title?>
            </h5>
            <div itemprop="offers" itemscope class="content_price">
                <span itemprop="price" class="price product-price">
                    <?= Formulas::priceWrapper($item->userPromotionPrice) ?>
                </span>
                <meta itemprop="priceCurrency" content="1" />
            </div>


            <div class="button-container">
                <a class="button order-button btn btn-default" href="<?= Yii::app()->createUrl('/item/index', array('iid' => $item->num_iid)) ?>" rel="nofollow" title="Заказать товар" data-id-product="8">
                    <span>Заказать</span>
                </a>

            </div>
        </div>
    </div><!-- .product-container> -->
</li>
