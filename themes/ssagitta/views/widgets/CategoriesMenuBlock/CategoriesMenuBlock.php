<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="CategoriesMenuBlock.php">
 * </description>
 * Виджет основного меню категорий
 * $mainMenu = массив описаний категорий с массивами описаний дочерних категорий
 * Array
 * (
 * [0] => Array
 * (
 * [pkid] => 2 - PK категории из таблицы categories_ext
 * [cid] => 0 - cid категории
 * [parent] => 1 - PK родительской категории, 1 - корень
 * [status] => 1 - вкл/выкл
 * [url] => mainmenu-odezhda - часть URL категории
 * [query] => 女装男装 - запрос на китайском языке
 * [level] => 2 - уровень в дереве категорий, начиная с 1 для корня
 * [order_in_level] => 200 - порядок вывода категории в уровне
 * [view_text] => Одежда - название категории
 * [children] => Array (...) - массив аналогичных структур для подкатегорий
 * )
 * $adminMode = false/true - рендеринг меню категорий для админки
 **********************************************************************************************************************/
?>
<!--<div class="vmegamenu_content">-->
    <? $useVirtualMenu = DSConfig::getVal('search_useVirtualMenu') == 1; ?>
    <? if (isset($mainMenu)): ?>
        <?php foreach ($mainMenu as $id => $menu): ?>
            <?php $type = 'category'; ?>
            <?php if (isset($menu['children']) && (count($menu['children']) > 0) && !$useVirtualMenu) {
                $link = Yii::app()->createUrl('/category/page', array('page_id' => $menu['pkid']));
            } else {
                $link = Yii::app()->createUrl('/' . $type . '/index', array('name' => $menu['url']));
            }
            ?>
            <div id="pt_menu<?= $id ?>" class="pt_menu<?php if ((isset($menu['children']) && count($menu['children']) < 1) || !isset($menu['children'])): ?> noSub<?php endif; ?>" >
                <div class="parentMenu">
                    <?php if (($menu['cid'] == 0) && ($menu['query'] == '')): ?>
                        <a href="javascript:void(0);" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $menu['pkid'] . "')\"" : ''; ?>>
                            <span><?=$menu['decorate']?><?= $menu['view_text'] ?></span>
                        </a>
                    <?php else: ?>
                        <a href="<?= ($adminMode) ? 'javascript:void(0);' : $link ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $menu['pkid'] . "')\"" : ''; ?> >
                            <span><?=$menu['decorate']?><?= $menu['view_text'] ?></span>
                        </a>
                    <?php endif; ?>
                </div>
                <?php if (isset($menu['children']) && count($menu['children']) > 0): ?>
                    <div class="wrap-popup">
                        <div id="popup<?= $id ?>" class="popup">
                            <div class="arrow-left"></div>
                            <div class="block1">
                                <?php $loop = 0; ?>
                                <?php foreach ($menu['children'] as $id2 => $items2): ?>
                                    <?php $loop++; ?>
                                    <div class="column<?php if ($loop == 1): ?> first<?php elseif ($loop == count($menu['children'])): ?> last<?php endif; ?> col<?= $loop ?>" style="float:left;">
                                        <div class="itemMenu level1">
                                            <?php if (($items2['cid'] == 0) && ($items2['query'] == '')): ?>
                                                <a class="itemMenuName level3" href="javascript:void(0);"><span><?= $items2['view_text'] ?></span></a>
                                            <?php else: ?>
                                                <a class="itemMenuName level3" href="<?= ($adminMode) ? 'javascript:void(0);' : Yii::app()->createUrl('/' . $type . '/index', array('name' => $items2['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items2['pkid'] . "')\"" : ''; ?> >
                                                    <span><?= $items2['view_text'] ?></span>
                                                </a>
                                            <?php endif; ?>
                                            <?php if (isset($items2['children']) && $items2['children'] > 0): ?>
                                            <div class="itemSubMenu level3">
                                                <div class="itemMenu level4">
                                                    <?php foreach ($items2['children'] as $id3 => $items3): ?>
                                                        <a class="itemMenuName level4" href="<?= ($adminMode) ? 'javascript:void(0);' : Yii::app()->createUrl('/' . $type . '/index', array('name' => $items3['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items3['pkid'] . "')\"" : ''; ?> >
                                                            <span><?= $items3['view_text'] ?></span>
                                                        </a>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <div class="clearBoth"></div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
            <? if (!$adminMode) { ?>
                <div class="pt_menu">
                    <div class="parentMenu">
                    <a href="<?= Yii::app()->createUrl('/category/list') ?>">
                        <span><?= Yii::t('main', 'Все категории') ?></span>
                    </a>
                    </div>
                </div>
            <? } ?>
    <?php endif; ?>
<!--</div>-->
