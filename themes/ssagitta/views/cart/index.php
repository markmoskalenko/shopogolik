<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>

<div class="table_block table-responsive" id="order-detail-content">
    <? /* Status Block*/ ?>
    <? if (count($userCart->cartRecords) > 0) { ?>
    <table id="cart_summary" class="table table-bordered">
        <thead>
        <tr>
            <th class="cart_product first_item">Товар</th>
            <th class="cart_description item">Описание</th>
            <th class="cart_avail item">Вес, грамм</th>
            <th class="cart_unit item">Цена</th>
            <th class="cart_quantity item">Количество</th>
            <th class="cart_total item">Сумма</th>
            <th class="cart_delete last_item">&nbsp;</th>
        </tr>
        </thead>
        <tfoot>
        <tr class="cart_total_price">
            <td rowspan="3" colspan="2" id="cart_voucher" class="cart_voucher">
                <textarea placeholder="<?= Yii::t('main', 'Ваш комментарий к заказу') ?>" cols="80" name="comment"
                          style="height: 124px;width: 583px; border: 1px solid #ddd; padding: 10px;
}"></textarea>
            </td>
            <? if (!$userCart->allowOrder): ?>
                <td colspan="3"
                    class="text-right"> <?= Yii::t('main', 'Минимальная сумма заказа 1 000 руб.<br>Нужен дозаказ на сумму:') ?></td>
                <td colspan="2" class="price"
                    id="total_product"><?= Formulas::priceWrapper($userCart->summAddToAllowOrder) ?></td>
            <?php endif; ?>
        </tr>
        <tr style="display: none;">
            <td colspan="3" class="text-right">
                Total gift-wrapping cost:
            </td>
            <td colspan="2" class="price-discount price" id="total_wrapping">
                $0.00
            </td>
        </tr>
        <?php if ($userCart->totalDiscount > 0): ?>
            <tr class="cart_total_delivery">
                <td colspan="3" class="text-right"><?= Yii::t('main', 'Ваша скидка:') ?></td>
                <td colspan="2" class="price"
                    id="total_shipping"> <?= Formulas::priceWrapper($userCart->totalDiscount) . ''; ?></td>
            </tr>
        <?php endif; ?>
        <tr class="cart_total_price">
            <td colspan="3" class="total_price_container text-right">
                <?= Yii::t('main', 'Итого') ?>:
            </td>
            <td colspan="2" class="price" id="total_price_container">
                </strong><?= Formulas::priceWrapper($userCart->total) ?>
            </td>
        </tr>


        </tfoot>
        <form action="<?= Yii::app()->createUrl('/cart/save') ?>" method="POST" id="cart-save">


    <!-- ================================ -->
    <div class="table_block table-responsive" id="order-detail-content">
        <?php foreach ($userCart->cartRecords as $k => $item) { ?>
            <? $this->widget('application.components.widgets.OrderItem', array(
                'orderItem' => $item,
                'readOnly' => FALSE,
                'imageFormat' => '_200x200.jpg',
            ));
            ?>
        <?php } ?>

    </table>
    <div class="clear"></div>
    <div class="cart-footer">
        <div style="width: 190px; float: left;">
            <button class="button btn btn-default standard-checkout button-medium" onclick="clearCart();return true;"
                    type="button" name="deleteAll">
                <span class="ui-button-text"><?= Yii::t('main', 'Очистить корзину') ?></span>
            </button>
        </div>
        <div style="margin-left: 20px;width: 190px; float: left;">
            <button class="button btn btn-default standard-checkout button-medium" type="submit" name="save">
                <span class="ui-button-text"><?= Yii::t('main', 'Пересчитать') ?></span>
            </button>
        </div>
        <div style="float: right; ">
            <input <?= (!$userCart->allowOrder) ? 'disabled="disabled"' : '' ?> style="padding: 10px 25px 10px 25px;"
                                                                                class="button btn btn-default standard-checkout button-medium"
                                                                                type="submit" name="Checkout"
                                                                                value="<?= Yii::t('main', 'Оформить заказ') ?>"/>
        </div>
        <? if (isset($actualLotExpressFeeInCurrency)) { ?>
            <tr>
                <td colspan="2">
                    <label><?= Yii::t('main', 'В т.ч. доставка по Китаю') ?>:</label>
                </td>
                <td align="right">
                    <div class="cost">
                        <?=
                        (isset($item->status) && (in_array(
                                $item->status,
                                OrdersItemsStatuses::getOrderItemExcludedStatusesArray()
                            ))) ? Yii::t('main', '-') : Formulas::priceWrapper($actualLotExpressFeeInCurrency) ?>
                    </div>
                </td>
            </tr>
        <? } ?>

    </div>
</div>
    <!-- ================================ -->
<? } else { ?>
    <div class="warning"><?= Yii::t('main', 'Ваша корзина пуста!') ?></div>
<? } ?>
</form>
</div>
