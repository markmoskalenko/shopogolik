<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="payment.php">
 * </description>
 * Рендеринг начальной формы пополнения счёта
 * http://<domain.ru>/ru/cabinet/balance/payment
 * var $model = CabinetForm
 * var $check = false
 * var $paySystems = array
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>

<h1 class="page-heading"><?= $this->pageTitle ?></h1>
<div class="box">
    <div class="form" style="padding: 15px">
        <?php $form = $this->beginWidget('CActiveForm',
          array(
            'id'=>'payment-form',
            'enableAjaxValidation'=>false,
          )) ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'sum'); ?>
            <?php echo $form->textField($model, 'sum',array('class'=>'is_required validate form-control','style'=>'width:245px')); ?>
            <?php echo $form->error($model, 'sum'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'phone'); ?>
            <?php echo $form->textField($model, 'phone',array('class'=>'is_required validate form-control','style'=>'width:245px')); ?>
            <?php echo $form->error($model, 'phone'); ?>
        </div>
        <div class="payment-systems">
            <h4 style="align:center;"><?= Yii::t('main', 'Вариант оплаты') ?>:</h4>
            <? if ($paySystems) {
                foreach ($paySystems as $paySystem) {
                    ?>
                    <div class="row" style="margin-top: 10px">
                        <label for="<?= $paySystem->int_name; ?>">
                            <input  type="radio" class="top" id="<?= $paySystem->int_name; ?>"
                                   value="<?= $paySystem->int_name; ?>" name="preference"/>
                            <? if (Utils::AppLang() == 'ru') { ?>
                                <?= $paySystem->name_ru; ?>
                            <?
                            } else {
                                ?>
                                <?= $paySystem->name_en; ?>
                            <? } ?>
                            <div class="desc">
                                <img src="<?= $paySystem->logo_img; ?>" height="40" alt=""/>
                                <br/>
                                <? if (Utils::AppLang() == 'ru') { ?>
                                    <?= $paySystem->descr_ru; ?>
                                <?
                                } else {
                                    ?>
                                    <?= $paySystem->descr_en; ?>
                                <? } ?>
                            </div>
                        </label>
                    </div>
                <?
                }
            } ?>
        </div>
        <div class="row buttons" style="margin-top: 10px">
            <?= CHtml::submitButton(Yii::t('main', 'Далее'), array('class'=>'button btn btn-default standard-checkout button-medium','style'=>'padding:10px 25px 10px 25px;')) ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<script type="text/javascript">
    $('.pay-radio').prop('checked', false);
</script>