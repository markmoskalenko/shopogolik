<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="payment_online.php">
 * </description>
 * Форма онлайнового платежа
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<div class="blue-tabs">
    <div class="active-tab">
        <span></span>
    </div>
        <span></span>
</div>
<div class="cabinet-content">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= CHtml::encode(DSConfig::getVal('site_name') . ': ' . Yii::t('main', 'Пополнение счёта')) ?></title>
    <link href="//favicon.ico" type="image/x-icon" rel="icon"/>
    <?= PaySystems::preRenderForm($data, $type); ?>
</div>