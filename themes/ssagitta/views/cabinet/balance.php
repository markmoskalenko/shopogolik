<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="balance.php">
 * </description>
 * Рендеринг состояния счёта и истории платежей в кабинете
 * http://<domain.ru>/ru/cabinet/balance/index
 * var $payments = - платежи
 * CActiveDataProvider#1([modelClass] => 'Payment')
 * var $date_from = '2014-06-06'
 * var $date_to = '2014-07-06'
 **********************************************************************************************************************/
?>
<?
Yii::app()->clientScript->registerScriptFile(
    $this->frontThemePath . '/js/ui/' . (YII_DEBUG ? 'jquery.ui.datepicker.js' : 'minified/jquery.ui.datepicker.min.js'),
    CClientScript::POS_BEGIN
);
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>

<h1 class="page-heading"><?= $this->pageTitle ?></h1>
<div class="box">
    <p><strong>
            <?= Yii::t('main', 'Доступные средства') ?>
            : <?=
            Formulas::priceWrapper(
                Formulas::convertCurrency(
                    Users::getBalance(Yii::app()->user->id),
                    DSConfig::getVal('site_currency'),
                    DSConfig::getCurrency()
                ),
                DSConfig::getCurrency()
            ) ?>
        </strong></p>
    <script>
        $(function () {
            $.datepicker.setDefaults($.datepicker.regional["ru"]);
            $("#date_from").datepicker({dateFormat: "yy-mm-dd"});
            $("#date_to").datepicker({dateFormat: "yy-mm-dd"});
        });
    </script>
<div style="padding: 15px">
    <?php
    $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id'=>'balance-date-form',
            'enableAjaxValidation'=>false,
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'post'
        ,
        )
    );
    ?>
    <div class="row">
        <?= CHtml::label(Yii::t('main', 'Выписка с:'), '') ?>
        <?= CHtml::textField('date_from', $date_from, array('id' => 'date_from','class'=>'is_required validate form-control','style'=>'width:245px')) ?>
    </div>
    <div class="row">
        <?= CHtml::label(Yii::t('main', 'по:'), '') ?>
        <?= CHtml::textField('date_to', $date_to, array('id' => 'date_to','class'=>'is_required validate form-control','style'=>'width:245px')) ?>
    </div>
    <div class="row" style="margin-top: 20px">
        <?php echo CHtml::submitButton(Yii::t('main', 'Показать'), array('class'=>'button btn btn-default standard-checkout button-medium','style'=>'padding:10px 25px 10px 25px;')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
    <div class="cabinet-table">
        <!-- ===================================================================== -->
        <? $this->widget(
            'zii.widgets.grid.CGridView',
            array(
                'id'            => 'payments-grid',
                'itemsCssClass' => 'table table-bordered',
                'dataProvider'  => $payments,
                'enableSorting' => false,
                'pager'         => array(
                    'header'         => '',
                    'firstPageLabel' => '&lt;&lt;',
                    'prevPageLabel'  => '&lt;',
                    'nextPageLabel'  => '&gt;',
                    'lastPageLabel'  => '&gt;&gt;',
                ),
                'template'      => '{summary}{items}{pager}',
                'summaryText'   => Yii::t('main', 'Платежи') . ' {start}-{end} ' . Yii::t('main', 'из') . ' {count}',
                'columns'       => array(
                    array(
                        'name' => 'id',
                    ),
                    array(
                        'name'  => 'sum',
                        'type'  => 'raw',
                        'value' => function ($data) {
                            if ($data->sum > 0) {
                                return '<b>' . sprintf('%01.2f', $data->sum) . '</b>';
                            } else {
                                return sprintf('%01.2f', $data->sum);
                            }
                        }
                    ),
                    array(
                        'name' => 'text_status',
                    ),
                    array(
                        'name' => 'text_date',
                    ),
                    array(
                        'name' => 'description',
                    ),
                ),
            )
        );
        ?>
    </div>
</div>