<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_ajax_update_form.php">
 * </description>
 **********************************************************************************************************************/
?>
<div id='favorite-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>Update favorite #<?php echo $model->id; ?></h3>
    </div>

    <div class="modal-body">

        <div class="form">
            <?php $form = $this->beginWidget(
              'bootstrap.widgets.TbActiveForm',
              array(
                'id'                     => 'favorite-update-form',
                'enableAjaxValidation'   => false,
                'enableClientValidation' => false,
                'method'                 => 'post',
                'action'                 => array("favorite/update"),
                'type'                   => 'horizontal',
                'htmlOptions'            => array(
                  'onsubmit' => "return false;",
                    /* Disable normal form submit */
                    //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                ),

              )
            ); ?>
            <fieldset>
                <legend>
                    <p class="note">Fields with <span class="required">*</span> are required.</p>
                </legend>

                <?php echo $form->errorSummary(
                  $model,
                  'Opps!!!',
                  null,
                  array('class' => 'alert alert-error span12')
                ); ?>

                <div class="control-group">
                    <div class="span4">

                        <?php echo $form->hiddenField($model, 'id', array()); ?>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'uid'); ?>
                            <?php echo $form->textField($model, 'uid'); ?>
                            <?php echo $form->error($model, 'uid'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'num_iid'); ?>
                            <?php echo $form->textField($model, 'num_iid', array('size' => 20, 'maxlength' => 20)); ?>
                            <?php echo $form->error($model, 'num_iid'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'date'); ?>
                            <?php echo $form->textField($model, 'date'); ?>
                            <?php echo $form->error($model, 'date'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'cid'); ?>
                            <?php echo $form->textField($model, 'cid', array('size' => 20, 'maxlength' => 20)); ?>
                            <?php echo $form->error($model, 'cid'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'express_fee'); ?>
                            <?php echo $form->textField($model, 'express_fee'); ?>
                            <?php echo $form->error($model, 'express_fee'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'price'); ?>
                            <?php echo $form->textField($model, 'price'); ?>
                            <?php echo $form->error($model, 'price'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'promotion_price'); ?>
                            <?php echo $form->textField($model, 'promotion_price'); ?>
                            <?php echo $form->error($model, 'promotion_price'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'pic_url'); ?>
                            <?php echo $form->textField($model, 'pic_url', array('size' => 60, 'maxlength' => 512)); ?>
                            <?php echo $form->error($model, 'pic_url'); ?>
                        </div>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'seller_rate'); ?>
                            <?php echo $form->textField($model, 'seller_rate'); ?>
                            <?php echo $form->error($model, 'seller_rate'); ?>
                        </div>

                    </div>
                </div>

        </div>
        <!--end modal body-->

        <div class="modal-footer">
            <div class="form-actions">

                <?php
                $this->widget(
                  'bootstrap.widgets.TbButton',
                  array(
                    'buttonType'  => 'submit',
                      //'id'=>'sub2',
                    'type'        => 'primary',
                    'icon'        => 'ok white',
                    'label'       => $model->isNewRecord ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить'),
                    'htmlOptions' => array('onclick' => 'update();'),
                  )
                );

                ?>

            </div>
        </div>
        <!--end modal footer-->
        </fieldset>

        <?php $this->endWidget(); ?>

    </div>

</div><!--end modal-->



