<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="list.php">
 * </description>
 * Просмотр списка избранных товаров в кабинете
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>

<h1 class="page-heading"><?= $this->pageTitle ?></h1>
<div class="cabinet-content">
    <?php
    $this->widget(
        'zii.widgets.CMenu',
        array(
            'itemCssClass'=>'form-item submit',
            'htmlOptions'=>array(
                'class'=>"btn btn-default button button-medium",
                'style'=>" padding: 10px 25px 10px 25px",
            ),

            'items' => array(
                /*          array(
                            'label' => Yii::t('main', 'Экспорт в YML'),
                            'icon'  => 'icon-plus',
                            'url'   => Yii::app()->controller->createUrl('getYML', array('uid' => Yii::app()->user->id))
                          ),
                */
                array(
                    'label' => Yii::t('admin', 'Очистить весь список'),
                    'icon'  => 'icon-refresh',
                    'url'   => Yii::app()->controller->createUrl('delete', array('id' => -1)),

                ),
            )
        )
    );
    ?>
    <div style="clear: both"></div>
    <?/* Экспорт в YML   <div>
        <?= Yii::t('main', 'Категорий для экспорта') . ':' . $categoriesCount; ?>
        <?= Yii::t('main', 'Товаров для экспорта') . ':' . $itemsCount; ?>
    </div> */?>
    <? if (isset($favoriteMenu) && is_array($favoriteMenu) && count($favoriteMenu)) {?>
        <br/><br/><br/>
        <div class="cabinet-favorites-categories" >
            <? foreach ($favoriteMenu as $category) { ?>
                <a href="<?=Yii::app()->controller->createUrl('list', array('cid' => $category['cid']))?>" style="margin-right: 10px"><?=$category['view_text']?></a>
            <? } ?>
        </div>
    <? } ?><div class="container-inner row" >
        <div class="cabinet-table">
            <? $this->widget(
                'application.components.widgets.SearchItemsList',
                array(
                    'id'                         => 'favorite-itemslist',
                    'controlAddToFavorites'      => true,
                    'controlAddToFeatured'       => true,
                    'controlDeleteFromFavorites' => false,
                    'lazyLoad'                   => true,
                    'dataType'                   => 'itemsRecentUser',
                    'pageSize'                   => 10,
                    'dataProvider'               => $dataProvider,
                )
            );
            ?>
        </div>
    </div>
</div>
