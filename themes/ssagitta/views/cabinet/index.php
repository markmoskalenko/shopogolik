<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Главная страница кабинета
 * http://<domain.ru>/ru/cabinet
 * var $user = - данные о пользователе
 * var $ordersByStatuses = array - заказы по статусам
 * (0 => array(
 * 'id' => '21'
 * 'value' => 'IN_PROCESS'
 * 'name' => 'В процессе обработки'
 * 'descr' => 'Статус по умолчанию для всех новых заказов. Присваивается автоматически.'
 * 'manual' => '1'
 * 'aplyment_criteria' => 'select oo.id from orders oo where oo.status in (\'PAUSED\')
 * and (uid=:uid or :uid is null) and (manager=:manager or :manager is null)'
 * 'auto_criteria' => ''
 * 'order_in_process' => '0'
 * 'enabled' => '1'
 * 'count' => '36'
 * 'lastdate' => '1402398695'
 * 'totalsum' => '12920.15'
 * 'totalpayed' => '11056.76'
 * 'totalnopayed' => '-1863.39'
 * ))
 **********************************************************************************************************************/
?>

<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<h1 class="page-heading"><?= $this->pageTitle ?></h1>
<div class="base-info">
    <a href="<?= Yii::app()->createUrl('/cabinet/profile') ?>" class="button btn btn-default standard-checkout button-medium" style="padding:10px 25px 10px 25px" >

        <?= Yii::t('main', 'Настроить профиль') ?>
    </a>
    <br/>
    <br/>
    <div class="box">
        <h3 class="title"><?= Yii::t('main', 'Добро пожаловать') ?>, <?= Yii::app()->user->firstname ?>!</h3>
        <hr/>

        <p><?= Yii::t('main', 'Номер Вашего счета') ?>:&nbsp;<b><?= Yii::app()->user->getPersonalAccount() ?></b></p>
        <? if (true) { ?>
            <p><?= Yii::t('main', 'Ваш персональный промо-код') ?>
                :&nbsp;<b><?= Users::getPromoByUid(Yii::app()->user->id) ?></b></p>
        <? } ?>
        <p><?= Yii::t('main', 'Доступные средства') ?>:
            <strong><?=
                Formulas::priceWrapper(
                    Formulas::convertCurrency(
                        Users::getBalance(Yii::app()->user->id),
                        DSConfig::getVal('site_currency'),
                        DSConfig::getCurrency()
                    ),
                    DSConfig::getCurrency()
                ); ?></strong>
        </p>
        <? if ($user->manager) { ?>
            <?= Yii::t('main', 'Служба поддержки клиентов') ?>:<br/>
            <?= Yii::t('main', 'EMail') ?>:&nbsp;<a href="mailto:info@shopogolik24.com">info@shopogolik24.com</a>
            <? if ($user->manager->skype) { ?>
                <br/><?= Yii::t('main', 'Skype') ?>:&nbsp;<a
                    href="skype:shopogolik_24">shopogolik_24</a>
            <? } ?>
        <? } ?>

        <div class="cabinet-content">
            <?
            $isEmpty = true;
            foreach ($ordersByStatuses as $orderByStatus) {
                if ($orderByStatus['count'] > 0) {
                    $isEmpty = false;
                    break;
                }
            } ?>
            <? if (!$isEmpty) { ?>
            <h3><?= Yii::t('main', 'Распределение ваших заказов по статусам') ?>:</h3>
            <hr/>

            <div class="block-center" id="block-history">
                <table id="order-list" class="table table-bordered footab default footable-loaded footable">
                    <thead>
                    <tr>
                        <th class="first_item footable-first-column" data-sort-ignore="true">Количество</th>
                        <th class="item footable-sortable">Дата<span class="footable-sort-indicator"></span></th>
                        <th data-hide="phone" class="item footable-sortable">Цена<span class="footable-sort-indicator"></span></th>
                        <th class="item footable-sortable">Статус<span class="footable-sort-indicator"></span></th>
                        <th data-sort-ignore="true" data-hide="phone,tablet" class="last_item footable-last-column">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>


                        <? foreach ($ordersByStatuses as $orderByStatus) {
                            if ($orderByStatus['count'] > 0) {
                                ?>
                        <tr class="first_item ">
                                <td class="history_link bold footable-first-column"><span class="footable-toggle"></span>
                                    <?=$orderByStatus['count'].' шт.' ?>
                                </td>
                                <td data-value="20141120020622" class="history_date bold">
                                    <?= date('d.m.Y H:i', $orderByStatus['lastdate']) ?>
                                </td>
                                <td class="history_price" data-value="1602.00">
						        	<span class="price">
								        <?= 'P '.$orderByStatus['totalsum']  ?>
						        	</span>
                                </td>
                                <td data-value="1" class="history_state">
                                    <span class="label label-info">
                                        <?= Yii::t('main', $orderByStatus['name']) ?>
								    </span>
                                </td>
                                <td class="history_detail footable-last-column">
                                    <a class="btn btn-default button button-small" href="/cabinet/orders/index/type/<?= $orderByStatus['value'] ?>">
                                        <span>Подробнее<i class="icon-chevron-right right"></i></span>
                                    </a>
                                </td>
                        </tr>
                            <?
                            }
                        } ?>
                        <?
                        } else {
                            ?>
                            <tr>
                            <h3><strong><?= Yii::t('main', 'У вас нет заказов') ?>.</strong></h3>
                            </tr>
                        <? } ?>

                    </tbody>
                </table>
                <div id="block-order-detail" class="unvisible">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
