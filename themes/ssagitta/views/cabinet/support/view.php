<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="view.php">
 * </description>
 **********************************************************************************************************************/
?>
<h1>СЛУЖБА ПОДДЕРЖКИ</h1>
<hr/>
<div class="blue-tabs" style="margin-bottom: 20px">
    <a href="<?= Yii::app()->createUrl('/cabinet/support') ?>" class="btn btn-default button button-small" style="margin-right: 20px">
        <span><?= Yii::t('main', 'Служба поддержки') ?></span>
    </a>
    <a href="<?= Yii::app()->createUrl('/cabinet/support/history') ?>" class="btn btn-default button button-small active-tab">
        <span><?= Yii::t('main', 'История обращений') ?></span>
    </a>

</div>
<div class="cabinet-content">
    <p><b><?= Yii::t('main', 'Номер вопроса') ?>:</b> Q0000<?= CHtml::encode($question->id) ?></p>

    <p><b><?= Yii::t('main', 'Тема') ?>:</b> <?= $question->theme ?></p>

    <p><b><?= Yii::t('main', 'Категория') ?>:</b> <?= CHtml::encode($category_values[$question->category]) ?></p>
    <? if (!empty($question->order_id)) { ?>
        <p><b><?= Yii::t('main', 'Номер заказа') ?>:</b> <?= $question->order_id ?></p>
    <? } ?>
    <? if (!empty($question->file)) { ?>
        <p><b><?= Yii::t('main', 'Файл') ?>:</b> <a href="/upload/<?= $question->file ?>"><?= $question->file ?></a></p>
    <? } ?>
    <p><b><?= Yii::t('main', 'Дата обращения') ?>:</b> <?= CHtml::encode(date("d.m.Y H:i:s", $question->date)) ?></p>


    <?php foreach ($messages as $message): ?>
        <div class="view">

            <?php if ($message->uid == Yii::app()->user->id): ?>
                <b><?= Yii::app()->user->firstname ?>&nbsp;<?= isset(Yii::app()->user->lastname) ? Yii::app(
            )->user->lastname : '' ?></b><?= Yii::t('main', 'Дата обращения') ?>:<?=
                date(
                  "d.m.Y H:i:s",
                  $message->date
                ) ?>
            <?php else: ?>
                <b><?= Yii::t('main', 'Служба поддержки') ?>&nbsp;</b><?= Yii::t('main', 'Дата обращения') ?>:<?=
                date(
                  "d.m.Y H:i:s",
                  $message->date
                ) ?>
            <?php endif ?>
            <hr/>
            <p><?= $message->question ?></p>
        </div>
        <?php if ((0 == ($message->status % 2)) && !empty($answers[$message->id])): ?>
            <?php $answer = $answers[$message->id]; ?>
            <div class="view answer <?= ($message->status == 2) ? 'new' : ''; ?>">
                <b><?= $answer->user->email . ' (ID: ' . $answer->user->uid . ')'; ?></b> - <?=
                Yii::t(
                  'main',
                  'Дата ответа'
                ) ?>: <?= date("d.m.Y H:i:s", $answer->date) ?>
                <hr/>
                <p><?= $answer->question ?></p>
            </div>
        <?php endif ?>
    <?php endforeach; ?>

    <?php if ($message->uid !== false): ?>

        <script>
            $(function () {
                $('#send_mess_a').click(function () {
                    $('#colose_question').hide();
                    $('#send_mess').show();
                    return false;
                });
                $('#cansel_mess_a').click(function () {
                    $('#colose_question').show();
                    $('#send_mess').hide();
                    return false;
                });
            });
        </script>


        <div class="form">
            <? if ($question->status != 3) { ?>
                <form id="colose_question" action="<?= $this->createUrl('/cabinet/support/save') ?>" method="post">
                    <?= CHtml::hiddenField('Message[colose_question]', 1) ?>
                    <?= CHtml::hiddenField('Message[qid]', $question->id) ?>
                    <div class="buttons">
                        <?= CHtml::submitButton(Yii::t('main', 'Вопрос закрыт'), array('class' => 'blue-btn bigger btn btn-default button button-small')) ?>
                        <?=
                        CHtml::htmlButton(
                          Yii::t('main', 'Задать вопрос'),
                          array('id' => 'send_mess_a', 'class' => 'blue-btn bigger btn btn-default button button-small', 'style' => 'loat:right')
                        ) ?>
                    </div>
                </form>
            <? } else { ?>
                <div id="colose_question">
                    <b><?= Yii::t('main', 'Вопрос закрыт') ?></b>
                </div>
            <? } ?>
            <form style="display:none" id="send_mess" action="<?= $this->createUrl('/cabinet/support/save') ?>"
                  method="post">
                <div class="support-question-new">
                    <label><?= Yii::t('main', 'Вопрос') ?>:</label>
                    <?= CHtml::textArea('Message[question]') ?>
                    <?= CHtml::hiddenField('Message[email]', Yii::app()->user->email) ?>
                    <?= CHtml::hiddenField('Message[uid]', Yii::app()->user->id) ?>
                    <?= CHtml::hiddenField('Message[qid]', $question->id) ?>
                    <?= CHtml::hiddenField('Message[date]', time()) ?>
                </div>
                <div class="buttons">
                    <?= CHtml::submitButton(Yii::t('main', 'Отправить'), array('class' => 'blue-btn bigger btn btn-default button button-small')) ?>
                    <?=
                    CHtml::htmlButton(
                      Yii::t('main', 'Отмена'),
                      array('class' => 'blue-btn bigger btn btn-default button button-small', 'id' => 'cansel_mess_a')
                    ) ?>
                </div>
            </form>
        </div>

    <?php endif; ?>

</div>

<style>
    .support-question-new textarea{
        display: block;
        width: 600px;
        height: 200px;
        margin-bottom: 20px;
    }
</style>