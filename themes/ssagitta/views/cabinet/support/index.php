<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>

<h1 class="page-heading"><?= $this->pageTitle ?></h1>
<div class="box">
    <h4><?= Yii::t('main', 'Задать вопрос в службу поддержки') ?>:</h4>

    <p ><?=
        Yii::t(
          'main',
          'Символом <span style="color:red">* </span>отмечены поля, обязательные для заполнения'
        ) ?>.</p>

    <div class="form" style="padding: 15px">
        <?= $form ?>
    </div>
    <div style="clear: both"></div>
    <div class="content"><?= cms::customContent('support') ?></div>
</div>
