<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="address.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>

<h1 class="page-heading"><?= $this->pageTitle ?></h1>
<div class="cabinet-content">

    <div class="form">
        <? //$form?>
    </div>
    <?php if (count($addresses)): ?>
        <table class="table table-bordered">
            <tr>
                <th><?= Yii::t('main', 'Адрес') ?></th>
                <th><?= Yii::t('main', 'Получатель') ?></th>
                <th><?= Yii::t('main', 'Телефон') ?></th>
                <th>Действие</th>
            </tr>
            <?php foreach ($addresses as $address): ?>
                <tr>
                    <td><?= $countries[$address['country']] ?>
                        ,<? if ($address['region']) { ?>&nbsp;<?= $address['region'] ?>,<? } ?>
                        &nbsp;<?= $address['city'] ?>,<br/><?= $address['index'] ?>,&nbsp;<?= $address['address'] ?>
                    </td>
                    <td><?= $address['lastname'] ?>&nbsp;<?= $address['firstname'] ?>
                        &nbsp;<br/><?= isset($address['patroname']) ? $address['patroname'] : '' ?></td>
                    <td><?= $address['phone'] ?></td>
                    <td>
                        <a href="<?=
                        Yii::app()->createUrl(
                          '/cabinet/profile/updateaddress',
                          array('id' => $address['id'])
                        ) ?>" class='button btn btn-default standard-checkout button-medium'
                           style='padding: 3px 8px;margin-top: 20px;font-size: 12px;'>Редактировать&nbsp;
                            <i class="icon-chevron-right right"></i></a>
                        <a href="<?=
                        Yii::app()->createUrl(
                          '/cabinet/profile/deleteaddress',
                          array('id' => $address['id'])
                        ) ?>" class='button btn btn-default standard-checkout button-medium'
                        style='padding: 3px 8px;margin-top: 20px;font-size: 12px;'>Удалить&nbsp;<i class="icon-remove right"></i></a>
                    </td>
                </tr>
            <? endforeach ?>
        </table>
        <div class="row buttons" style="padding: 15px">
            <a href="<?= Yii::app()->createUrl('/cabinet/profile/createaddress') ?>" class='button btn btn-default standard-checkout button-medium'
               style='padding:10px 25px 10px 25px;margin-top:20px'>Добавить адрес</a>
        </div>
    <? else: ?>
        <p><?= Yii::t('main', 'Вы ещё не задали адресов') ?></p><br/>
        <div class="row buttons" style="padding: 15px">
            <a href="<?= Yii::app()->createUrl('/cabinet/profile/createaddress') ?>" class='button btn btn-default standard-checkout button-medium'
               style='padding:10px 25px 10px 25px;margin-top:20px'>Добавить адрес</a>
        </div>
    <?  endif; ?>
</div>