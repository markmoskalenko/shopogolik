<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_email.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

return array(
    'elements' => array(
        'email'    => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control'
        ),
        'password' => array(
            'type'      => 'password',
            'maxlength' => 128,
            'class'=>'is_required validate form-control'
        ),
    ),
    'buttons'  => array(
        'submit' => array(
            'type'  => 'submit',
            'label' => Yii::t('main', 'Сохранить'),
            'class'=>'button btn btn-default standard-checkout button-medium',
            'style'=>'padding:10px 25px 10px 25px;margin-left:15px;margin-top:20px'
        ),
    ),
);