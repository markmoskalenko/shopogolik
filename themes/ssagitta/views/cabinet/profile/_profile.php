<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_profile.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

return array(
    'elements' => array(
        'lastname'   => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;',
        ),
        'firstname'  => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;',
        ),
        'patroname'  => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;',
        ),
        'alias'      => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;',
        ),
        'd_birth'    => array(
            'type'  => 'dropdownlist',
            'items' => DSConfig::getDays(),
            'style' =>'width: 82px;',
            'class'=>'is_required validate form-control'
        ),
        'm_birth'    => array(
            'type'  => 'dropdownlist',
            'items' => DSConfig::getMounthes(),
            'style' =>'width: 82px;',
            'class'=>'is_required validate form-control'
        ),
        'y_birth'    => array(
            'type'  => 'dropdownlist',
            'items' => DSConfig::getYears(),
            'style' =>'width: 82px;',
            'class'=>'is_required validate form-control'
        ),
        /*        'sex'=>array(
                    'type'=>'dropdownlist',
                    //'prompt'=>'Выберите значение:',
                    'items'=>array('male'=>'Мужской','female'=>'Женский'),
                ),
        */
        'skype'      => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;',
        ),
        'vk'         => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;',
        ),
        'promo_code' => array(
            'type'      => 'text',
            'maxlength' => 128,
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;',
        ),
        'password'   => array(
            'type'      => 'password',
            'maxlength' => 128,
            'hint'      => Yii::t('main', 'Пожалуйста, введите текущий пароль, если <br/>Вы хотите сохранить изменения.'),
            'class'=>'is_required validate form-control',
            'style' =>'width: 360px;magrin-left:15px',
		'id' => 'pass'
        ),
    ),
    'buttons'  => array(
        'submit' => array(
            'type'  => 'submit',
            'label' => Yii::t('main', 'Сохранить'),
            'class'=>'button btn btn-default standard-checkout button-medium',
            'style'=>'padding:10px 25px 10px 25px; margin-top:25px'
        ),
//        'reset'=>array(
//            'type'=>'reset',
//            'label'=>Yii::t('main','Отмена'),
//            'onclick'=>'window.history.back()',
//            'class'=>'blue-btn bigger'
//        ),
    ),
);