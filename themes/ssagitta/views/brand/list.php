<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="list.php">
 * </description>
 * Рендеринг страницы списка брэндов
 * http://<domain.ru>/ru/article/oplata
 * var $brands = array - массив описаний брэндов
 **********************************************************************************************************************/
?>
<h1>Бренды</h1>
<hr/>
<div>
    <? foreach ($brands as $alpha => $models) { ?>
        <a class="btn btn-default button button-small" href="#<?= $alpha ?>" style="display:inline-block;"><?= $alpha ?></a>
    <? } ?>
</div>
<div class="content brands">
    <? $i = 0; ?>
    <? foreach ($brands as $alpha => $models) { ?>
        <? if ($i % 2 == 0) {
            $class = ' first';
        } else {
            $class = '';
        }
        $i++; ?>
        <div style="clear: both"></div>

        <div class="link-list<?= $class ?>">
            <h4><a name="<?= $alpha ?>"><?= $alpha ?></a></h4>
            <div style="clear: both"></div>
            <hr/>
            <ul>
                <?php foreach ($models as $brand) {
                    if (!$brand->img_src) continue;
                    ?>
                    <li>
                        <a href="<?= $this->createUrl('/brand/index', array('name' => $brand->url)) ?>">
                            <span style="margin-bottom: 15px; display: block"><?= CHtml::encode($brand->name); ?></span>
                            <img src="/images/brands/<?= $brand->img_src ?>" alt="<?= CHtml::encode($brand->name); ?>">
                        </a>
                    </li>
                <? } ?>
            </ul>
        </div>
    <? } ?>
</div>

<style>
    .brands ul li{float: left; height: 150px; margin: 15px;}
</style>