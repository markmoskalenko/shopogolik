<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="login.php">
 * </description>
 **********************************************************************************************************************/
?>

    <form action="#" method="post" id="login_form" class="box" style="width: 370px;">
        <h3 class="page-subheading" ><?= $this->pageTitle ?></h3>
        <div class="form_content clearfix" >
            <div style="padding: 15px;">
                <?= $form->render() ?>
            </div>
            <a  href="<?= $this->createUrl('/user/register') ?>" ><?= Yii::t('main', 'Зарегистрироваться') ?></a>
            <br/>
        </div>
    </form>