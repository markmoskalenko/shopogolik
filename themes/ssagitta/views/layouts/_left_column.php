<div id="pt_vmegamenu" class="pt_vmegamenu">
    <div class="vmegamenu_title">
        <h2><?= Yii::t('main', 'Категории') ?> </h2>
    </div>

    <div class="vmegamenu_content">
        <?php
        $this->widget(
            'application.components.widgets.CategoriesMenuBlock',
            array(
                'lang' => 'ru',
            )
        );
        ?>
    </div>

	<script type="text/javascript">
        //<![CDATA[
        var VMEGAMENU_POPUP_EFFECT = 0;
        //]]>

        $(document).ready(function(){
            $("#pt_ver_menu_link ul li").each(function(){
                var url = document.URL;
                $("#pt_ver_menu_link ul li a").removeClass("act");
                $('#pt_ver_menu_link ul li a[href="'+url+'"]').addClass('act');
            });

            $('.pt_menu').hover(function(){
                if(VMEGAMENU_POPUP_EFFECT == 0) $(this).find('.popup').stop(true,true).slideDown('slow');
                if(VMEGAMENU_POPUP_EFFECT == 1) $(this).find('.popup').stop(true,true).fadeIn('slow');
                if(VMEGAMENU_POPUP_EFFECT == 2) $(this).find('.popup').stop(true,true).show('slow');
            },function(){
                if(VMEGAMENU_POPUP_EFFECT == 0) $(this).find('.popup').stop(true,true).slideUp('fast');
                if(VMEGAMENU_POPUP_EFFECT == 1) $(this).find('.popup').stop(true,true).fadeOut('fast');
                if(VMEGAMENU_POPUP_EFFECT == 2) $(this).find('.popup').stop(true,true).hide('fast');
            })
        });
    </script><!-- MODULE Block best sellers -->
    <div class="left-col" style="margin-bottom: 50px">
        <? // Вывод параметров поиска в поисковых же страницах?>
        <?  if (in_array(
                $this->id,
                array('search', 'category', 'favorite', 'brand', 'seller', 'site')
            ) && ($this->body_class != 'cabinet')
        ) : ?>
            <? if (($this->id != 'site')) : ?>
                <? $this->widget(
                    'application.components.widgets.SearchParams',
                    array(
                        'type' => $this->id,
                        'params' => $this->params['params'],
                        'cids' => $this->params['cids'],
                        'bids' => $this->params['bids'],
                        'groups' => $this->params['groups'],
                        'filters' => $this->params['filters'],
                        'multiFilters' => $this->params['multiFilters'],
                        'suggestions' => $this->params['suggestions'],
                        'priceRange' => $this->params['priceRange'],
                    )
                ); ?>
            <? endif; ?>
        <? endif; ?>
    </div>
    <!-- /Block tags module -->
    <div class="pos-special-product pos_animated">
        <div class="pos-special-product-title title_block fx-fadeInDown a1"><h4>АКЦИЯ</h4></div>
        <ul class="bxslider fx-fadeInDown a3">
            <li class="ajax_block_product first_item item clearfix">
                <div class="item-inner">
                    <a href="http://shopogolik24.com/ru/category/mainmenu-verxnyaya-odezhda-kozhanye-kurtki" class="product_img_link"
                       title="Printed Summer Dress">
                        <img src="<?= $this->frontThemePath ?>/img/39-large_default/printed-summer-dress.jpg" alt=""/>
                    </a>
                    <h3><a class="product-name" href="#" title="Printed Summer Dress">Женские кожаные куртки</a></h3>
                    <div class="content_price">
						<span>Женские куртки от<span>
                        <span class="special-price">500 рублей!</span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
