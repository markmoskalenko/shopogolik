<div id="left_column" class="column col-xs-12 col-sm-3"><div class="navleft-container">
        <? $this->renderPartial('//layouts/_left_column', array()); ?>

    </div>
   
</div>
<div id="center_column" class="center_column col-xs-12 col-sm-9">
    <div class="on-top container">
        <div class="container-inner row">
            <?= $content ?>
        </div>
    </div>
</div>