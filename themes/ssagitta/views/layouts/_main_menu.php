<div class="megamenu">
    <div class="bg-half"></div>
    <div class="container"><!-- Block categories module -->


        <div class="nav-container visible-lg visible-md">
            <div id="pt_custommenu" class="pt_custommenu">
                <div id="pt_menu_home" class="pt_menu act">
                    <div class="parentMenu"><a href="/"><span>alHome</span></a></div>
                </div>

                <div id="pt_menu_pt_item_menu_our_catalog" class="pt_menu">
                    <div class="parentMenu">
                        <span class="block-title">Каталог</span>
                    </div>
                    <div id="popup_pt_item_menu_our_catalog" class="popup cmsblock" style="display: none; width: 904px;">
                        <div class="block2" id="block2_pt_item_menu_our_catalog">

                            <div class="static_block_catalog_menu">
                                <div class="custom-col custom-col1">
                                    <h4 class="title_block">Каталог Таобао</h4> 
                                    <p>Шопоголик 24 ® — это товары из каталога Taobao (Таобао) - самого большого интернет-магазина Китая, представленные на русском языке. В магазине можно купить более 700 млн. товаров (одежды, обуви, аксессуаров, автозапчастей, электроники и др.) производства Китая, Кореи, Японии и других стран.</p>
                                </div>
                                <div class="custom-col custom-col2"><a href=""><img src="/themes/ssagitta/img/cms/cat_1.png" title=""/></a>
                                    <p>Если вы нашли понравившуюся вещь на сайте Taobao, но не знаете как её купить, не расстраивайтесь! Просто скопируйте ссылку на товар и введите ее в поисковую строку сайта Шопоголик 24.</p>
                                    <a href="http://shopogolik24.com/ru/article/search_taobao"> Узнать подробнее &raquo;</a></div>
                                <div class="custom-col custom-col3"><a href="#"><img src="/themes/ssagitta/img/cms/cat_2.png" title="Выгодная доставка в ваш город!"/></a>
                                    <p>Товары доставляются в Россию, Белоруссию, Украину и многие другие страны СНГ и Европы, а оплатить их можно прямо на сайте в режиме онлайн любым удобным способом.</p>
                                    <a href="http://shopogolik24.com/ru/article/raschet-stoimosti-dostavki"> Подробнее о доставке &raquo;</a></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id ="pt_menu_link" class ="pt_menu"><div class="parentMenu" ><a href="http://shopogolik24.com/ru/brand/list"><span>Бренды</a></span></div></div>
                <div id="pt_menu_pt_item_menu_our_product" class="pt_menu">
                    <div class="parentMenu">
                        <span class="block-title">Скидки</span>
                    </div>
                    <div id="popup_pt_item_menu_our_product" class="popup cmsblock" style="display: none; width: 904px;">
                        <div class="block2" id="block2_pt_item_menu_our_product">

                            <div class="static_block_product_menu">
                                <div class="custom-col custom-col1">
                                    <h4 class="title_block">Товары со скидками</h4>
                                    <p>Шопоголик 24 представляет уникальный сервис покупок товара из Китая. Все представленные товары отобраны нашими специалистами и отображаются с актуальными на сегодня скидками.</p>
                                </div>
                                <div class="custom-col custom-col2"><a href="#"><img src="/themes/ssagitta/img/cms/sk_1.png" title="Супер цены! Скидки до 80%!"/></a>
                                    <p>Заказывай популярные товары с максимальными скидками! Наши специалисты каждый день следят за акциями и предоставляют возможность Вам покупать товары с максимальными скидками.</p>
                                    <a href="http://shopogolik24.com/ru/article/skidka-dnya"> Узнай больше о скидках &raquo;</a></div>
                                <div class="custom-col custom-col3"><a href="#"><img src="/themes/ssagitta/img/cms/sk_2.png" title="CASHBACK для новичков! Вернем 5% на счет!"/></a>
                                    <p>Срок каждой скидки или акции ограничен.</p>
                                    <a href="http://shopogolik24.com/ru/article/skidka-novichkam"> Подробнее об акции &raquo;</a></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id ="pt_menu_link" class ="pt_menu"><div class="parentMenu" ><a href="http://shopogolik24.com/ru/article/raschet-stoimosti-dostavki"><span>Доставка</a></span></div></div>
                <div id ="pt_menu_link" class ="pt_menu"><div class="parentMenu" ><a href="http://shopogolik24.com/ru/article/oplata"><span>Оплата</a></span></div></div>
				<div id ="pt_menu_link" class ="pt_menu"><div class="parentMenu" ><a href="http://shopogolik24.com/ru/article/voprosi-i-otveti"><span>Помощь</a></span></div></div>
				<div id ="pt_menu_link" class ="pt_menu"><div class="parentMenu" ><a href="http://shopogolik24.com/ru/article/reviews"><span>Отзывы</a></span></div></div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //<![CDATA[
        var CUSTOMMENU_POPUP_EFFECT = 0;
        var CUSTOMMENU_POPUP_TOP_OFFSET = 80;
        //]]>
    </script></div>
</div>