<div id="left_column" class="column col-xs-12 col-sm-3">
    <div class="navleft-container">
        <ul class="myaccount-link-list">
            <li><a href="<?= Yii::app()->createUrl('/cabinet') ?>" title="Information" ><i class="icon-user"></i><span style="width: 220px">Общая информация</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/profile/index') ?>" title="Information" style="margin-top: 10px"><i class="icon-user"></i><span>Личные данные</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/profile/email') ?>" title="Information" style="margin-top: 10px"><i class="icon-user"></i><span>Изменить E-mail</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/profile/password') ?>" title="Information" style="margin-top: 10px"><i class="icon-user"></i><span>Изменить пароль</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/profile/address') ?>" title="Addresses" style="margin-top: 10px"><i class="icon-building"></i><span>Список адресов</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/favorite/list') ?>" style="margin-top: 10px" title="My wishlists"><i class="icon-heart"></i><span>Избранное</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/balance/index') ?>" title="Credit slips" style="margin-top: 10px"><i class="icon-ban-circle"></i><span>Выписка по счету</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/balance/payment') ?>" title="Credit slips" style="margin-top: 10px"><i class="icon-ban-circle"></i><span>Пополнить счет</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/support') ?>" title="Information" style="margin-top: 10px"><i class="icon-user"></i><span>Служба поддержки</span></a></li>
            <li><a href="<?= Yii::app()->createUrl('/cabinet/support/history') ?>" title="Information" style="margin-top: 10px"><i class="icon-user"></i><span style="width: 225px">История обращений</span></a></li>
        </ul>
    </div>
</div>