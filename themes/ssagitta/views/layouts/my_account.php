<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="en"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" lang="en"><![endif]-->
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="utf-8"/>
    <title><?= $this->pageTitle ?></title>
    <meta name="description" content="Shop powered by PrestaShop"/>
    <meta name="generator" content="PrestaShop"/>
    <meta name="robots" content="index,follow"/>
    <meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="icon" href="<?= $this->frontThemePath ?>/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?= $this->frontThemePath ?>/images/favicon.ico" type="image/x-icon"/>

    <? $this->renderPartial('//layouts/_head_styles', array()); ?>
    <link rel="stylesheet" href="<?= $this->frontThemePath ?>/css/account.css" type="text/css" media="all"/>
    <? $this->renderPartial('//layouts/_head_javascripts', array()); ?>

</head>
<body id="my-account" class="my-account hide-left-column hide-right-column lang_en">
<div id="page">

    <div class="static-right-banner">
        <div class="static-right-content">
             <ul>
                <li><a href="http://shopogolik24.com/ru/article/skidka-dnya"
                       style="background-image:url(<?= $this->frontThemePath ?>/img/right_menu_banners/hint_freeshipping_jan.png)"
                       onclick="yaCounter25369892.reachGoal('skidka_Dnya_sprava')"></a></li>
					   <li><a href="http://shopogolik24.com/ru/article/oformlenie-zakaza"
                       style="background-image:url(<?= $this->frontThemePath ?>/img/right_menu_banners/hint_how_to_order_green.png)"
                       onclick="yaCounter25369892.reachGoal('kak_sdelat_zakaz_sprava')"></a></li>
                <li><a href="http://shopogolik24.com/ru/article/raschet-stoimosti-dostavki"
                       style="background-image:url(<?= $this->frontThemePath ?>/img/right_menu_banners/hint2.png)"
                       onclick="yaCounter25369892.reachGoal('dostavka_sprava')"></a></li>
                <li><a href="http://shopogolik24.com/ru/article/oplata"
                       style="background-image:url(<?= $this->frontThemePath ?>/img/right_menu_banners/hint3.png)"
                       onclick="yaCounter25369892.reachGoal('oplata_sprava')"></a></li>
            </ul>
        </div>
    </div>
    <div class="header-container">
        <? $this->renderPartial('//layouts/_header', array()); ?>
    </div>
    <? $this->renderPartial('//layouts/_main_menu', array()); ?>

    <div class="columns-container">

        <div class="columns-container">
            <div id="columns" class="container">
                <div class="row">
                    <?= $this->renderPartial('//layouts/_account_menu'); ?>
                    <div id="center_column" class="center_column col-xs-12 col-sm-9">
                        <div class="on-top">
                            <div class="container-inner row">
                                <?php $this->widget('application.components.widgets.MessagesBlock') ?>

                                <?= $content ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <? $this->renderPartial('//layouts/_brands', array()); ?>

        <? $this->renderPartial('//layouts/_footer', array()); ?>
    </div>
    <!-- #page -->
    <div class="back-top"><a href="#" class="mypresta_scrollup hidden-phone"></a></div>
</div>
</body>
</html>