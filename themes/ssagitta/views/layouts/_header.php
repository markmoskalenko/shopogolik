<header id="header">

<div class="nav pos_animated">
    <div class="container">
        <div class="row">
            <nav><!-- Block currencies module -->
                <? $this->widget('application.components.widgets.currencyBlock'); ?>
                <div class="msg fx-fadeInDown">
            <span class="welcome-msg">
                Более 700 миллионов товаров по низким ценам
            </span>
                </div>
                <ul id="header_links" class="fx-fadeInDown">
                    <?php if (Yii::app()->user->isGuest): ?>
                        <li><a class="link-myaccount" href="<?= Yii::app()->createUrl('/user/register') ?>" title="My account">Регистрация</a></li>
                        <li><a class="link-myaccount" href="<?= Yii::app()->createUrl('/user/login') ?>" title="My account">Войти</a></li>
                    <?php else: ?>
                        <li><a class="link-myaccount" href="<?= Yii::app()->createUrl('/cabinet') ?>" title="My account">Личный кабинет</a></li>
                        <li><a class="link-myaccount" href="<?= Yii::app()->createUrl('/cabinet/favorite/list') ?>" title="My account">Избранное</a></li>
                        <li><a class="link-myaccount" href="<?= Yii::app()->createUrl('/user/logout') ?>" title="My account">Выйти</a></li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
<div class="container  pos_animated">
    <div class="row">


        <div class="header-static">
            <div class="container">
                <div class="row">
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12">
                        <div class="header-time">
                            <h2>CASHBACK для новичков</h2>
                            <p>Вернем 5% от стоимости<br>вашего заказа!</p>
                        </div>
                    </div>
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12">
                        <div class="header-shipping">
                            <h2>Выгодная доставка</h2>
                            <p>Оперативная и недорогая доставка по России и СНГ</p>
                        </div>
                    </div>
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12">
                        <div class="header-money">
                            <h2>100% MONEY BACK</h2>
                            <p>Гарантированный возрат средств</p>
                        </div>
                    </div>
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12">
                        <div class="header-phone">
                            <h2>8-800 333-23-36</h2>
							<p><a class="btn btn-default button button-small" href="skype:shopogolik_24?chat" title="Задайте свой вопрос нашему специалисту по Скайпу">	
                    <span>Skype: вопрос специалисту</span></p></a>				
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<div>
    <div class="container pos_animated">
        <div class="row">
            <div id="header_logo" class="col-xs-12 col-sm-12 col-md-3 fx-pulse">
                <a href="<?= $this->createUrl('/') ?>" title="Шопоголик 24 - оперативная доставка товара из Китая">
                    <img class="logo img-responsive" src="<?= $this->frontThemePath ?>/img/logo.png" alt="Шопоголик 24 - оперативная доставка товара из Китая"
                         width="262" height="61"/>
                </a>
            </div>
            <div class="header-content col-xs-12 col-sm-12 col-md-9">
                <? // Блок поискового запроса ?>
                <?php $this->widget('application.components.widgets.SearchQueryBlock'); ?>
                <? $this->renderPartial('//layouts/_cart', array()); ?>


                <div id="layer_cart">
                    <div class="clearfix">
                        <div class="layer_cart_product col-xs-12 col-md-6">
                            <span class="cross" title="Close window"></span>

                            <h2>
                                <i class="icon-ok"></i>Product successfully added to your shopping cart
                            </h2>

                            <div class="product-image-container layer_cart_img">
                            </div>
                            <div class="layer_cart_product_info">
                                <span id="layer_cart_product_title" class="product-name"></span>
                                <span id="layer_cart_product_attributes"></span>

                                <div>
                                    <strong class="dark">Quantity</strong>
                                    <span id="layer_cart_product_quantity"></span>
                                </div>
                                <div>
                                    <strong class="dark">Total</strong>
                                    <span id="layer_cart_product_price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="layer_cart_cart col-xs-12 col-md-6">
                            <h2>
                                <!-- Plural Case [both cases are needed because page may be updated in Javascript] -->
                            <span class="ajax_cart_product_txt_s  unvisible">
                                There are <span class="ajax_cart_quantity">0</span> items in your cart.
                            </span>
                                <!-- Singular Case [both cases are needed because page may be updated in Javascript] -->
                            <span class="ajax_cart_product_txt ">
                                There is 1 item in your cart.
                            </span>
                            </h2>

                            <div class="layer_cart_row">
                                <strong class="dark">
                                    Total products
                                    (tax excl.)
                                </strong>
                            <span class="ajax_block_products_total">
                                                    </span>
                            </div>

                            <div class="layer_cart_row">
                                <strong class="dark">
                                    Total shipping&nbsp;(tax excl.)
                                </strong>
                            <span class="ajax_cart_shipping_cost">
                                                            Free shipping!
                                                    </span>
                            </div>
                            <div class="layer_cart_row">
                                <strong class="dark">
                                    Total
                                    (tax excl.)
                                </strong>
                            <span class="ajax_block_cart_total">
                                                    </span>
                            </div>
                            <div class="button-container">
                            <span class="continue btn btn-default button exclusive-medium" title="Continue shopping">
                                <span>
                                    <i class="icon-chevron-left left"></i>Continue shopping
                                </span>
                            </span>
                                <a class="btn btn-default button button-medium" href="#" title="Proceed to checkout"
                                   rel="nofollow">
                                <span>
                                    Proceed to checkout<i class="icon-chevron-right right"></i>
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="crossseling"></div>
                </div>
                <!-- #layer_cart -->
                <div class="layer_cart_overlay"></div>

                <!-- /MODULE Block cart -->                                </div>
        </div>
    </div>
</div>
</header>