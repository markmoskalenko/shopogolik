<!-- Footer -->
<div id="footer" class="footer-container">
    <div class="pos-footer-center pos_animated">
        <div class="container">
            <div class="footer-center">
                <div class="footer-static">

                    <div class="block footer-block f-col col-sm-6 col-md-3 col-sms-6 col-smb-12 fx-fadeInUp a1">
                        <div class="title_block">
                            <h4>Сервис Шопоголик 24</h4>
                        </div>
                        <div class="footer-content toggle-footer">
                            <p>Через Шопоголик 24® за 2 года работы было куплено более миллиона китайских товаров и
                                отправлено более 100 тысяч посылок. Шопоголик 24 — ваш удобный и надежный сервис заказа
                                товаров в Китае, которому можно доверять!</p>

                            <div class="author">- С уважением, Шопоголик 24 -</div>
                        </div>
                    </div>

                    <section class="myaccount block footer-block col-sm-6 col-md-2 col-sms-6 col-smb-12 fx-fadeInUp a2">
                        <div class="title_block"><h4>Помощь</h4></div>
                        <div class="block_content toggle-footer">
                            <ul class="bullet">
                                <li><a href="http://shopogolik24.com/ru/article/vibor-razmera" title="">Таблица
                                        размеров</a></li>
                                <li><a href="http://shopogolik24.com/ru/article/raschet-stoimosti-dostavki" title="">Условия
                                        доставки</a></li>
                                <li><a href="http://shopogolik24.com/ru/article/oplata" title="">Способы оплаты</a></li>
                                <li><a href="http://shopogolik24.com/ru/article/oformlenie-zakaza" title="">Оформление
                                        заказа</a></li>
                                <li><a href="http://shopogolik24.com/ru/tools/calculator" title="">Расчёт доставки</a></li>
                                <li><a href="http://shopogolik24.com/ru/article/vozvrat-tovara-nadlej-kachestva"
                                       title="">Возврат денег</a></li>
                                <li><a href="http://shopogolik24.com/ru/tools/question" title="">Служба поддержки</a>
                                </li>
                            </ul>
                        </div>
                    </section>

                    <!-- MODULE Block footer -->
                    <section class="footer-block block col-sm-6 col-md-2 col-sms-6 col-smb-12 fx-fadeInUp a3"
                             id="block_various_links_footer">
                        <div class="title_block"><h4>Личный кабинет</h4></div>
                        <ul class="toggle-footer">
                            <li><a href="http://shopogolik24.com/ru/article/procedura-registracii" title="">Вход /
                                    Регистрация</a></li>
                            <li><a href="http://shopogolik24.com/ru/cabinet" title="">Личный кабинет</a></li>
                            <li><a href="http://shopogolik24.com/ru/cabinet/favorite/list" title="">Избранное</a></li>
                            <li><a href="http://shopogolik24.com/ru/cart/index" title="">Корзина</a></li>
                            <li><a href="http://shopogolik24.com/ru/cabinet" title="">Мои покупки</a></li>
                            <li><a href="http://shopogolik24.com/ru/cabinet/support/history" title="">Мои вопросы</a>
                            </li>
                        </ul>

                    </section>

                    <div class="why-buy block  footer-block col-sm-6 col-md-2 col-sms-6 col-smb-12 fx-fadeInUp a4">
                        <div class="static-footer-title title_block ">
                            <h4>Шопоголик 24</h4>
                        </div>
                        <ul class="toggle-footer">
                            <li><a href="http://shopogolik24.com/ru/article/history">О компании</a></li>
                            <li><a href="http://shopogolik24.com/ru/article/dropshipping">Дропшиппинг</a></li>
                            <li><a href="http://shopogolik24.com/ru/article/kak-oformiti-optovij-zakaz">Оптовые
                                    продажи</a></li>
                            <li><a href="#">Условия договора</a></li>
                            <li><a href="http://shopogolik24.com/ru/article/garantii">Гарантии</a></li>
                            <li><a href="http://shopogolik24.com/ru/article/contacts">Контакты</a></li>
                        </ul>
                    </div>

                    <!-- MODULE Block contact infos -->
                    <section id="block_contact_infos" class="footer-block block col-xs-12 col-md-3 fx-fadeInUp a5">
                        <div>
                            <div class="title_block"><h4>Контактная информация</h4></div>
                            <ul class="toggle-footer">
                                <li>
                                    Телефон:
                                    <span>8-800 333-23-36</span>
                                </li>
                                <li>
                                    Email:
                                    <span>info@shopogolik24.com</span>
                                </li>
								<li>
                                    Skype:
                                    <span><a href="skype:shopogolik_24?chat" title="Задайте свой вопрос нашему специалисту по Скайпу">	
                   shopogolik_24</a></span>
                                </li>
                            </ul>
                            <div class="footer-static">

                                <div class="payment"><img src="<?= $this->frontThemePath ?>/img/payment.png" title="Оплачивайте свои покупки удобным для Вас способом!"/>
                                </div>
                                <br/>
                                <a href="http://www.z-payment.com" rel="noflollow" target="_blank"><img
                                        src="https://z-payment.com/images/banners/pay-88-31.gif"
                                        alt="Принимаем Z-Payment" border="0"/></a>

                            </div>
                            <div class="footer-static">
                            </div>
                        </div>
                    </section>
                    <!-- /MODULE Block contact infos -->
                </div>
            </div>
        </div>
    </div>
    <div class="pos-footer-bottom">
        <div class="container">
            <div class="footer-bottom">
                <div class="footer-static">
                    <div class="footer-address">
                        <p>Copyright &copy; 2012–2014 Shopogolik24.com. Все права защищены.</p>

                        <div class="copyrightdetails">
                            Любое использование либо копирование материалов или подборки материалов сайта, элементов
                            дизайна и оформления может осуществляться лишь с разрешения автора (правообладателя) и
                            только при наличии ссылки на <a href="http://shopogolik24.com">http://Shopogolik24.com</a>.
                        </div>
                        Шопоголик 24 - товары из каталога Taobao (Таобао) с доставкой в Россию и СНГ.
                        <br>
                        Разработка <a href="http://dropshop.pro/" rel="noflollow" target="_blank">DropShop</a>, поддержка <a
                            href="http://it-yes.com/" rel="noflollow" target="_blank">IT-YES</a>. <a href="http://shopogolik24.com/ru/article/politika-bezopastnosti" target="_blank">Политика безопастности.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
