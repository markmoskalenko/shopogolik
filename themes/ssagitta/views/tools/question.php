<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="question.php">
 * </description>
 * Вопрос в службу поддержки
 **********************************************************************************************************************/
?>
<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Главная','/'),
    'links'=>$this->breadcrumbs,
    'htmlOptions'=>array('class'=>'breadcrumbs breadcrum', 'style'=>'margin-bottom:20px')
));?>
<h1><?= Yii::t('main', 'Служба поддержки клиентов') ?>:</h1>
<hr/>
<?php $form = $this->beginWidget('CActiveForm',array(
    'id'                   => 'message-form validate',
    'htmlOptions'          => array('enctype' => 'multipart/form-data'),
    'enableAjaxValidation' => false,
  )
); ?>

        <?php echo $form->labelEx($model, 'theme', array('style' => 'width:180px;')); ?>
        <?php echo $form->textField($model,'theme',array('maxlength' => 128,'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'theme'); ?>
        <br/>

        <?php echo $form->labelEx($model, 'question', array('style' => 'width:180px;')); ?>
        <?php echo $form->textArea($model, 'question', array('rows'  => 6, 'cols'  => 50, 'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'question'); ?>
        <br/>


        <?php echo $form->labelEx($model, 'email', array('style' => 'width:180px;')); ?>
        <?php echo $form->textField($model,'email',array('size' => 60,'maxlength' => 128, 'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'email'); ?>
        <br/>

        <?php echo $form->labelEx($model, 'category', array('style' => 'width:180px;')); ?>
        <?php echo $form->dropDownList($model, 'category', $category_values,array('class'=>'form-control')); ?>
        <br/>

        <?php echo $form->labelEx($model, 'order_id', array('style' => 'width:180px;')); ?>

        <? if (count($orders) > 0) { ?>
            <?php echo $form->dropDownList($model, 'order_id', $orders); ?>
        <?
        } else {
            ?>
            <?php echo $form->textField($model,'order_id',array('size' => 360,'maxlength' => 128, 'class'=>'form-control')); ?>
        <? } ?>
        <br/>

        <?php echo $form->labelEx($model, 'file', array('style' => 'width:180px;')); ?>
        <?php echo $form->fileField($model, 'file', array('id' => 'file_support', 'class'=>'form-control')) ?>
        <div class="hint"><p><?= Yii::t('main', 'Размер файла не должен превышать 1 Мб') ?></p></div>

<br/>
<br/>
    <div class="buttons">
        <?php echo CHtml::submitButton(Yii::t('main', 'Отправить вопрос'),array('style' => '', 'class' => 'btn btn-default button button-small')); ?>

        <?php echo CHtml::htmlButton(Yii::t('main', 'Удалить файл'),array('style' => "float:right;",'class' => 'btn btn-default button button-small', 'id'    => 'remove_file_support')); ?>
    </div>

<?php $this->endWidget(); ?>

<div style="margin-top:40px;" class="text">
    <hr/>
    <?= cms::customContent('support') ?>
</div>