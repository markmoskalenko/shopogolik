<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="list.php">
 * </description>
 * Рендеринг списка категорий
 * http://<domain.ru>/ru/category/list
 * var $categories = array - массив, описывающий категории
 * ( 0 => array (
 * 'pkid' => '2'
 * 'cid' => '0'
 * 'parent' => '1'
 * 'status' => '1'
 * 'url' => 'mainmenu-odezhda'
 * 'query' => '女装男装'
 * 'level' => '2'
 * 'order_in_level' => '200'
 * 'view_text' => 'Одежда'
 * 'children' => array
 * (
 * 3 => array(...)
 * 18 => array(...)
 * 31 => array(...)
 * 41 => array(...)
 * 49 => array(...)
 * 60 => array(...)
 * 70 => array(...)
 * 81 => array(...)
 * )
 * )
 **********************************************************************************************************************/
?>
<div class="breadcrumb clearfix" style="margin-bottom: 15px">
    <div class="breadcrumbs">
        <a href="http://shopogolik24.com/">Главная</a><span class="navigation-pipe">&gt;</span><a href="/ru/category/list">Все категории</a>
    </div><!-- breadcrumbs -->
</div>
<div class="content">
    <? $mainMenu = $categories;
    $useVirtualMenu = DSConfig::getVal('search_useVirtualMenu') == 1; ?>
    <ul>
        <?php if (isset($mainMenu)) {
            foreach ($mainMenu as $id => $menu) {
                ?>
                <?php
                $type = 'category';
                if (isset($menu['children']) && (count($menu['children']) > 0) && !$useVirtualMenu) {
                    $link = Yii::app()->createUrl('/category/page', array('page_id' => $menu['pkid']));
                } else {
                    $link = Yii::app()->createUrl('/' . $type . '/index', array('name' => $menu['url']));
                }
                ?>
                <li class="parent">
                    <h3><a href="<?= $link ?>">
                            <?= $menu['view_text'] ?>
                        </a></h3>
                    <?php if (isset($menu['children']) && count($menu['children']) > 0) { ?>
                        <ul>
                            <? if ($useVirtualMenu) { ?>
                                <li>
                                    <table class="categories-table">
                                        <?php if (isset($menu['children']) && count($menu['children']) > 0) {
                                            foreach ($menu['children'] as $id2 => $items2) {
                                                ?>
                                                <? if (isset($items2) && isset($items2['url'])) { ?>
                                                    <tr>
                                                        <td>
                                                            <a href="<?=
                                                            Yii::app()
                                                              ->createUrl(
                                                                '/' . $type . '/index',
                                                                array('name' => $items2['url'])
                                                              ) ?>">
                                                                <b><?= $items2['view_text'] ?></b>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <?php if (isset($menu['children']) && count(
                                                                $menu['children']
                                                              ) > 0
                                                            )
                                                                //  foreach($menu['children'] as $id2=>$items2) {
                                                                ?>
                                                            <?
                                                            if (isset($items2['children']) && count(
                                                                $menu['children']
                                                              ) > 0
                                                            ) {
                                                                foreach ($items2['children'] as $id3 => $items3) {
                                                                    ?>
                                                                    <a href="<?=
                                                                    Yii::app()
                                                                      ->createUrl(
                                                                        '/' . $type . '/index',
                                                                        array('name' => $items3['url'])
                                                                      ) ?>">
                                                                        <?= $items3['view_text'] ?>
                                                                    </a>&nbsp;
                                                                <?
                                                                }
                                                            } ?>
                                                            <? // } ?>
                                                        </td>
                                                    </tr>
                                                <? } ?>
                                            <?php
                                            }
                                        } ?>
                                    </table>
                                </li>

                            <?
                            } else {
                                ?>
                                <?php if (isset($menu['children']) && count($menu['children']) > 0) { ?>
                                    <li>
                                        <table class="categories-table">
                                            <tr>
                                                <td>
                                                    <? foreach ($menu['children'] as $id2 => $items2) { ?>

                                                        <? if (isset($items2) && isset($items2['url'])) { ?>
                                                            <a href="<?=
                                                            Yii::app()
                                                              ->createUrl(
                                                                '/' . $type . '/index',
                                                                array('name' => $items2['url'])
                                                              ) ?>">
                                                                <?= $items2['view_text'] ?>
                                                            </a>&nbsp;
                                                        <? } ?>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                <? } ?>
                            <? } ?>

                        </ul>
                    <? } ?>
                </li>
            <?php
            }
        } ?>
    </ul>
</div>