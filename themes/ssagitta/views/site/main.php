<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="main.php">
 * </description>
 * Главная страница сайта (не путать с лэйаутом)
 *
 * var $itemsPopular = true
 * var $itemsRecommended = true
 * var $itemsRecentUser = false
 * var $itemsRecentAll = true
 **********************************************************************************************************************/
?>
    <?php $this->widget('application.components.widgets.sliderBlock'); ?>

    <div class="banner-static">
        <div class="row">
            <div class="banner-box banner-box1 col-sm-4 col-md-4 col-sms-12 col-smb-12"><a href="#"><img src="http://shopogolik24.com/themes/ssagitta/images/banners/lider_1.png" title="" /></a></div>
            <div class="banner-box banner-box2 col-sm-4 col-md-4 col-sms-12 col-smb-12"><a href="#"><img src="http://shopogolik24.com/themes/ssagitta/images/banners/lider_2.png" title="" /></a></div>
            <div class="banner-box banner-box3 col-sm-4 col-md-4 col-sms-12 col-smb-12"><a href="#"><img src="http://shopogolik24.com/themes/ssagitta/images/banners/lider_3.png" title=""/></a></div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function() {

            $(".tab_category-slider1").hide();
            $(".tab_category-slider1:first").show();

            $("ul.tab_cates-slider1 li").click(function() {
                $("ul.tab_cates-slider1 li").removeClass("active");
                $(this).addClass("active");
                $(".tab_category-slider1").hide();
                $(".tab_category-slider1").removeClass("animate1 wiggle");
                var activeTab = $(this).attr("rel");
                $("#"+activeTab) .addClass("animate1 wiggle");
                $("#"+activeTab).fadeIn();
            });
        });

    </script>



<div class="pos_animated">
    <ul id="home-page-tabs" class="nav nav-tabs clearfix fx-fadeInDown a1">
        <li class="active"><a data-toggle="tab" href="#homefeatured" class="homefeatured"><?= Yii::t('main', 'Рекомендованные товары') ?></a></li>
    </ul>
        <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
        <? $this->widget(
            'application.components.widgets.ShowItemList',
            array(
                'id'                         => 'recommended-itemslist',
                'controlAddToFavorites'      => true,
                'controlAddToFeatured'       => false,
                'controlDeleteFromFavorites' => false,
                'lazyLoad'                   => true,
                'dataType'                   => 'itemsRecommended',
                'pageSize'                   => 12,
                'disableItemForSeo'          => $seo_disable_items_index,
            )
        );
        ?>
</div>


    <!-- Block Newsletter module-->
    <div id="newsletter_block_left" class="block pos_animated">
        <div class="block_content">
            <p class="fx-fadeInLeft newletter-header"></p>
            <form action="http://shopogolik24.com/ru/article/skidka-dnya" method="post">
                <div class="form-group" >
                    <button type="submit" name="submitNewsletter" class="btn btn-default button button-small fx-fadeInRight ">
                        <span>Скидки и акции на Шопоголик 24! Только сегодня! Успей купить!</span> 
                    </button>
                </div>
            </form>
        </div>
    </div>
    <!-- /Block Newsletter module-->


<div class="box-category fx-fadeInDown a1">
    <div class="cate_title">
        <h4><?= Yii::t('main', 'Популярные товары') ?></h4>
    </div>
</div>
            <? $this->widget(
                'application.components.widgets.ShowItemList',
                array(
                    'id'                         => 'popular-itemslist',
                    'controlAddToFavorites'      => true,
                    'controlAddToFeatured'       => false,
                    'controlDeleteFromFavorites' => false,
                    'lazyLoad'                   => true,
                    'dataType'                   => 'itemsPopular',
                    'pageSize'                   => 8,
                    'disableItemForSeo'          => $seo_disable_items_index,
                    'viewName'                   => 'ShowItemList2',
                )
            );
            ?>

    <div class="banner-home-content row pos_animated">
        <div class="home-box home-box1 col-xs-12 col-md-8 col-sm-8 col-sms-8 col-smb-12 fx-fadeInLeft"><a href="#"><img src="<?= $this->frontThemePath ?>/img/cms/banner-images.png" alt=""></a></div>
        <div class="home-box home-box2 col-xs-12 col-md-4 col-sm-4 col-sms-4 col-smb-12 fx-fadeInRight"><a href="#"><img src="<?= $this->frontThemePath ?>/img/cms/banner1-images.png" alt=""></a></div>
    </div>


<div class="tab-category-container-slider">
    <div class="container">
        <div class="container-inner row">
            <div class="tab-category-slider1 ">
                <div class="box-category title_block">
                    <div class="cate_title">
                        <h4><?= Yii::t('main', 'Недавно просмотренный вами товар') ?></h4>
                    </div>
                </div>
                <div class="tab_container">
                    <div id="tab_3" class="tab_category-slider1">
                        <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
                        <? $this->widget(
                            'application.components.widgets.ShowItemList',
                            array(
                                'id'                         => 'recentUser-itemslist',
                                'controlAddToFavorites'      => true,
                                'controlAddToFeatured'       => false,
                                'controlDeleteFromFavorites' => false,
                                'lazyLoad'                   => true,
                                'dataType'                   => 'itemsRecentUser',
                                'pageSize'                   => 25,
                                'disableItemForSeo'          => $seo_disable_items_index,
                                'viewName'                   => 'ShowItemList3',
                            )
                        );
                        ?>
                    </div>
                    <script type="text/javascript">
                        $(".tab-category-container-slider #tab_3 .productTabCategorySlider").bxSlider({
                            slideWidth:150,
                            slideMargin: 30,
                            infiniteLoop:false,
                            minSlides: 1,
                            maxSlides: 5,
                            speed:  3000,
                            pause: 500,
                            controls: 1,
                            pager: false,
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

