<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="page.php">
 * </description>
 * Впомогательный рендеринг подменю категорий. Как на старых сайтах с АПИ.
 * Когда сама по себе категория ничего не может икать, выдавался список её подкатегорий.
 * В нашем случае давно не используется.
 **********************************************************************************************************************/
?>
<div class="page-title">
    <?= $this->pageTitle ?>
</div>
<div class="content">
    <ul>
        <?php foreach ($links as $submenu) { ?>
            <li>
                <a href="<?= Yii::app()->createUrl('/category/index', array('name' => $submenu['url'])) ?>">
                    <?= $submenu['view_text'] ?>
                </a>
                <? if (isset($submenu['children'])) { ?>
                    <ul>
                        <? foreach ($submenu['children'] as $submenu2) { ?>
                            <li>
                                <a href="<?=
                                Yii::app()->createUrl(
                                  '/category/index',
                                  array('name' => $submenu2['url'])
                                ) ?>">
                                    <?= $submenu2['view_text'] ?>
                                    <? if (isset($submenu2['children'])) { ?>
                                        <ul>
                                            <? foreach ($submenu2['children'] as $submenu3) { ?>
                                                <li>
                                                    <a href="<?=
                                                    Yii::app()->createUrl(
                                                      '/category/index',
                                                      array('name' => $submenu3['url'])
                                                    ) ?>">
                                                        <?= $submenu3['view_text'] ?>
                                                </li>
                                            <? } ?>
                                        </ul>
                                    <? } ?>
                            </li>
                        <? } ?>
                    </ul>
                <? } ?>
            </li>
        <?php } ?>
    </ul>
</div>