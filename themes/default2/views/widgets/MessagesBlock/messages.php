<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="messages.php">
 * </description>
 * Виджет отображения инлайн-сообщений и оповещений
 **********************************************************************************************************************/
?>
<div id="message-block">
    <a href="javascript:void(0);" id="message-close"></a>
    <? foreach ($messages as $key => $mess) { ?>
        <div class="message blue" id="message-<?= $key ?>">
            <div class="message-text">
                <h3 class="title"><?= Yii::t('main', 'Внимание') ?>!</h3>
                <?= $mess ?>
            </div>
            <div class="message-bottom"></div>
        </div>
    <? } ?>
</div>