<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchQueryBlock.php">
 * </description>
 * Виджет, реализующий поисковую строку
 * var $cats = - список категорий для фильтра
 * array
 * (
 * 0 => array
 * (
 * 'pkid' => '2'
 * 'cid' => '0'
 * 'parent' => '1'
 * 'status' => '1'
 * 'url' => 'mainmenu-odezhda'
 * 'query' => '女装男装'
 * 'level' => '2'
 * 'order_in_level' => '200'
 * 'view_text' => 'Одежда'
 * 'children' => array
 * (
 * 3 => array(...)
 * 18 => array(...)
 * 31 => array(...)
 * 41 => array(...)
 * 49 => array(...)
 * 60 => array(...)
 * 70 => array(...)
 * 81 => array(...)
 * )
 * )
 * )
 * var $query = '' - поисковый запрос
 * var $cid = '' - cid категории
 **********************************************************************************************************************/
?>
<?
Yii::app()->clientScript->registerCssFile($this->frontThemePath . '/css/chosen.css');
Yii::app()->clientScript->registerScriptFile(
  $this->frontThemePath . '/js/chosen.jquery.min.js',
  CClientScript::POS_BEGIN
);
Yii::app()->clientScript->registerScriptFile(
  $this->frontThemePath . '/js/' . (YII_DEBUG ? 'jquery.autocomplete.js' : 'jquery.autocomplete.min.js'),
  CClientScript::POS_BEGIN
);
?>
<div class="b-search" id="top_nav">
    <?= CHtml::beginForm(array('/search/index'), 'get', array('id' => 'main-search-block')) ?>
    <input type="text" class="query" size=100px name="query" autocomplete="off" id="query"
           placeholder="<?= Yii::t('main', 'Что вы хотите купить') ?>" value="<?= $query ?>">

    <div class="search-loupe"></div>
    <div class="search-groups">
        <select class="chosen-grp" id="category" name="cid" style="display: none;">
            <option style="font-weight:bold" value="0"><?= Yii::t('main', 'Все категории') ?></option>
            <? $useVirtualMenu = DSConfig::getVal('search_useVirtualMenu') == 1; ?>
            <? if ($useVirtualMenu) { ?>
                <? foreach ($cats as $cat) {
                    if (isset($cat['children'])) {
                        foreach ($cat['children'] as $ch_cat) {
                            ?>
                            <? if ($ch_cat['pkid'] == $cid) {
                                $sel = ' selected';
                            } else {
                                $sel = '';
                            }?>
                            <option value="<?= $ch_cat['pkid'] ?>"<?= $sel ?>>
                                <?$s = '';
                                $s = $s . '&nbsp;' . $ch_cat['view_text'];
                                echo $s; ?>
                            </option>
                        <?
                        }
                    }
                } ?>
            <?
            } else {
                ?>
                <? foreach ($cats as $cat) {
                    if (isset($cat['children'])) {
                        foreach ($cat['children'] as $ch_cat) {
                            ?>
                            <? $sel = ($ch_cat['cid'] == $cid) ? ' selected' : ''; ?>
                            <option value="<?= $ch_cat['cid'] ?>"<?= $sel ?>><?
                                $s = '';
                                $s = $s . '&nbsp;' . $ch_cat['view_text'];
                                echo $s;
                                ?></option>
                        <?
                        }
                    }
                } ?>
            <? } ?>
            <option style="font-weight:bold" value="seller"><?= Yii::t('main', 'Поиск по продавцам') ?></option>
        </select>
    </div>
    <input class="b-button" type="submit" value="">
    <?= CHtml::endForm() ?>
</div>
<script> // Mel floated blocks
    var h_hght = 80; // высота шапки
    var h_mrg = 1;     // отступ когда шапка уже не видна
    $(function () {
        $(window).scroll(function () {
            var top = $(this).scrollTop();
            var elem = $('#top_nav');
            if ((top + h_mrg) < h_hght) {
                elem.css('top', (h_hght - top));
                elem.css('position', 'absolute');
                elem.css('left', '0');
            } else {
                elem.css('top', h_mrg);
                elem.css('left', '30%');
                elem.css('position', 'fixed');
            }
        });
    });
</script>
