<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="sliderBlock.php">
 * </description>
 * Виджет отображает слайдер баннеров на главной
 * $banners - массив моделей banners
 **********************************************************************************************************************/
?>
<section>
    <? if (isset($banners) && (is_array($banners)) && (count($banners) > 0)) { ?>
        <?
        Yii::app()->clientScript->registerScriptFile(
          $this->frontThemePath . '/js/jquery.easing.1.3.js',
          CClientScript::POS_HEAD
        );
        Yii::app()->clientScript->registerCssFile($this->frontThemePath . '/css/camera.css');		
        Yii::app()->clientScript->registerScriptFile(
          $this->frontThemePath . '/js/' . (YII_DEBUG ? 'camera.js' : 'camera.min.js'),
          CClientScript::POS_BEGIN
        );
        ?>
        <div style="overflow:hidden; height: 145px; width:960px; margin: 0; padding: 5px 0 5px;">
            <div id="main_camera_slider" class="camera_wrap">
                <? foreach ($banners as $banner) { ?>
                    <div
                      data-src="<?= $this->frontThemePath . $banner['img_src'] ?>"
                      data-link="<?=((preg_match('/^http[s]*:\/\//s',$banner['href']))?$banner['href'] : Yii::app()->createUrl($banner['href'])) ?>">
                        <!-- <div class="camera_caption"><? //=$banner['title']?></div> -->
                    </div>
                <? } ?>
            </div>
        </div>
    <? } ?>
</section>