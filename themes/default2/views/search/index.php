<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Рендеринг поисковой выдачи (вобще любой, по категориям, брэндам, пользователю, запросу и т.п.)
 **********************************************************************************************************************/
?><center><a href="http://shopogolik24.com/ru/user/register"><img src="/images/gr098-2.jpg"></center></a>
<? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
    <div class="blue-tabs">
        <? $base_params = $params;
        unset($base_params['recommend']);
        unset($base_params['users']);
        ?>
        <a href="<?= Yii::app()->createUrl('/' . $this->id . '/index', $base_params) ?>"
          <?= (!isset($_GET['recommend']) || $_GET['recommend'] == 0) ? 'class="active"' : '' ?>><span><?=
                Yii::t(
                  'main',
                  'Все товары'
                ) ?></span></a>
        <? if ($this->id !== 'seller') { ?>
            <a href="<?=
            Yii::app()->createUrl(
              '/' . $this->id . '/index',
              array_merge($params, array('recommend' => 1))
            ) ?>"
              <?= (isset($_GET['recommend']) && $_GET['recommend'] == 1) ? 'class="active"' : '' ?>><span><?=
                    Yii::t(
                      'main',
                      'Рекомендованные товары'
                    ) ?></span></a>
        <? } ?>
        <? if (isset($res->zh_query) && ($res->zh_query)) { ?>
            <div style="float: right; padding-right: 8px; padding-top: 8px;">
                <?= Yii::t('main', 'Поисковый запрос') ?>: <?= $res->zh_query ?>
            </div>
        <? } ?>
    </div>
<? // Блок вывода описания категории, если это описание есть?>
<? if (!empty($category->{'page_desc_' . Utils::TransLang()})) { ?>
    <div class="category-desc">
        <?= $category->{'page_desc_' . Utils::TransLang()} ?>
    </div>
<? } ?>
<? // Блок вывода описания бренда, если это описание есть?>
<? if (!empty($brand->{'page_desc_' . Utils::TransLang()})) { ?>
    <div class="category-desc">
        <?= $brand->{'page_desc_' . Utils::TransLang()} ?>
    </div>
<? } ?>
<? // Блок параметров поиска, таких ка сортировка и проч.?>
<?php if (is_object($res)) { ?>
    <div style="padding-top: 55px;">
        <div class="search-sort">
            <? $fAction = Yii::app()->createUrl('/' . $this->id . '/index', $params); ?>
            <form id="search-sort" action="<?= $fAction; ?>" method="post">
                <?= Yii::t('main', 'Сортировка') ?>:
                <select name="sort_by" id="sort_by" onchange="changeSortOrder('<?= $fAction; ?>');">
                    <option value="popularity_desc" <?= ($sort_by == 'popularity_desc') ? 'selected' : '' ?>>
                        <?= Yii::t('main', 'По популярности') ?>
                    </option>
                    <option value="credit_desc" <?= ($sort_by == 'credit_desc') ? 'selected' : '' ?>>
                        <?= Yii::t('main', 'По рейтингу продавца') ?>
                    </option>
                    <option value="delistTime_desc" <?= ($sort_by == 'delistTime_desc') ? 'selected' : '' ?>>
                        <?= Yii::t('main', 'Новинки') ?>
                    </option>
                    <option value="price_asc" <?= ($sort_by == 'price_asc') ? 'selected' : '' ?>>
                        <?= Yii::t('main', 'Цена: по возрастанию') ?>
                    </option>
                    <option value="price_desc" <?= ($sort_by == 'price_desc') ? 'selected' : '' ?>>
                        <?= Yii::t('main', 'Цена: по убыванию') ?>
                    </option>
                </select>
            </form>
        </div>
        <div class="search-count"><?= Yii::t('main', 'Всего предложений') ?>:
            <span><?= (isset($res->total_results)) ? $res->total_results : 0 ?></span></div>
    </div>
    <? if ($pages) {
        $this->renderPartial(
          '/search/pagination',
          array(
            'pages' => $pages,
          )
        );
    } ?>
    <div class="products-list">
        <?php if (isset($res->items)) {
            $i = 0;
            foreach ($res->items as $item) {
                $first = ($i % 4 == 0) ? ' first' : '';
                $i++;?>
                <?  $this->widget(
                  'application.components.widgets.SearchItem',
                  array(
                    'searchResItem' => $item,
                    'newLine'       => $first,
                  )
                );
            }
        } ?>
    </div>
<? } ?>
<? if ($pages) {
    $this->renderPartial(
      '/search/pagination',
      array(
        'pages' => $pages,
      )
    );
} ?>
<? $this->renderPartial('/search/recommended', array()); ?>
    <!-- Виджет кнопки вверх (Test-Templates) -->
    <div class="search-sidebar">
        <div class="search-sidebar-top">▲</div>
        <div class="search-sidebar-next"><a href="#">▶</a></div>
        <div class="search-sidebar-prev"><a href="#">◀</a></div>
    </div>
<? if (isset($res->debugMessages)) {
    $this->renderPartial(
      '/search/debug',
      array(
        'debugMessages' => $res->debugMessages,
      )
    );
} ?>