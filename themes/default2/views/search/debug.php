<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="debug.php">
 * </description>
 * Рендеринг работы парсера в режиме отладки
 * var $debugMessages - dataProvider
 **********************************************************************************************************************/
?>
<br/>
<br/>
<br/>
<br/>
<hr/>
<div>
    <?
    $this->widget(
      'bootstrap.widgets.TbGridView',
      array(
        'id'           => 'dsg-debug-grid',
        'dataProvider' => $debugMessages,
        'type'         => 'striped bordered condensed',
        'template'     => '{summary}<br/>{pager}{items}{pager}',
        'columns'      => array(
          'function',
          'param_name',
          array(
            'name'  => 'param_value',
            'type'  => 'raw',
            'value' => function ($data) {
                  //$res=preg_replace("/(.{128,256})\s.*/s","\1\.\.\.",$data["param_value"]);
                  $res = '<textarea rows="3" cols="20">' . htmlspecialchars($data['param_value']) . '</textarea>';
                  return $res;
              },
          ),
          array(
            'name'  => 'subject',
            'type'  => 'raw',
            'value' => function ($data) {
                  $res = '<textarea rows="3" cols="20">' . htmlspecialchars($data['subject']) . '</textarea>';
                  return $res;
              },
          ),
          array(
            'name'  => 'result',
            'type'  => 'raw',
            'value' => function ($data) {
                  $res = '<textarea rows="3" cols="20">' . htmlspecialchars($data['result']) . '</textarea>';
                  return $res;
              },
          ),
          array(
            'name'  => 'valid',
            'type'  => 'raw',
            'value' => function ($data) {
                  $res = ($data['valid']) ? 'true' : '<span style="color: red;"><strong>false</strong></span>';
                  return $res;
              }
          ),

        ),
      )
    );
    ?>
</div>