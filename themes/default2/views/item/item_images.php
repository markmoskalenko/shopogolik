<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_images.php">
 * </description>
 * Рендеринг фото товара
 **********************************************************************************************************************/
?>
<div class="item-main-image">
    <a id="Zoomer" href="<?= Img::getImagePath($item->top_item->pic_url, '_480x480.jpg', false) ?>"
       class="MagicZoomPlus"
       rel="zoom-width: 100%; zoom-height: 100%; zoom-distance: 100%; entire-image: true; hint: false; caption-height: 0; background-color: #dddddd; background-opacity: 80; loading-msg: <?=
       Yii::t(
         'main',
         'Загрузка'
       ) ?>...; right-click: true; zoom-position: inner; zoom-align: center; expand-size: width=550; expand-effect: cubic; restore-trigger: click; selectors-change: mouseover; selectors-effect: false; slideshow-effect: expand; hint-position: tr; show-title: false; caption-source: #custom-caption-source; buttons: autohide"
       title="">
        <? if (isset($item->top_item->pic_url) && ($item->top_item->pic_url != '')) { ?>
            <img src="<?= Img::getImagePath($item->top_item->pic_url, '_360x360.jpg', false) ?>"/>
        <? } ?>
    </a>
</div>
<div class="item-small-images">
    <?php foreach ($item->top_item->item_imgs->item_img as $image) { ?>
        <a href="<?= Img::getImagePath($image->url, '_480x480.jpg', false) ?>" rel="zoom-id: Zoomer"
           rev="<?= Img::getImagePath($image->url, '_480x480.jpg', false) ?>"><img src="<?= $image->url ?>_60x60.jpg"/></a>
    <?php } ?>
</div>

