<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_form.php">
 * </description>
 * Форма работы с картой товара
 **********************************************************************************************************************/
?>
<form action="<?= Yii::app()->createUrl('/cart/add') ?>" method="POST">
    <div class="form-item" style="border: none;">
        <input type="hidden" name="cid" value="<?= $item->top_item->cid ?>" id="cid"/>
        <input type="hidden" name="iid" value="<?= $item->top_item->num_iid ?>" id="iid"/>
        <input type="hidden" name="inputprops-processed" id="inputprops-processed" value="0"/>
        <?php
        if (!$ajax['input']) {
            echo Yii::app()->controller->renderPartial(
              'input_props',
              array(
                'totalCount'  => $item->top_item->num,
                'input_props' => $input_props
              )
            );
        } else {
            ?>
            <div id="item-input-props">
                <script async type="text/javascript">
                    loadInputProps();
                </script>
            </div>
        <?php } ?>
    </div>
    <div class="form-item">
        <label for="num"><?= Yii::t('main', 'Количество') ?>:</label>

        <div class="number">
            <nobr>
                <p class="minus">-</p>
                <input type="text" onchange="getUserPrice()" name="num" id="num" class="item-count" value="1"/>
                <input type="hidden" name="num-processed" id="num-processed" value="1"/>

                <p class="plus">+</p></nobr>
        </div>
                    <span id="item-count-price" class="item-info-param">x
                        <span id="count-price">
                            <?=
                            (isset($item->sku))
                              ? Formulas::priceWrapper($item->sku->userPriceFinal) :
                              Formulas::priceWrapper($item->top_item->userPriceFinal)?>
                        </span> =
                        <span id="sum"><?=
                            (isset($item->sku)) ? $item->sku->userPromotionPrice
                              : (isset($item->top_item->userPromotionPrice)) ? $item->top_item->userPromotionPrice
                              : $item->top_item->userPromotionPrice1 . '-' . $item->top_item->userPromotionPrice2?></span>
                    </span>
    </div>
    <div class="form-item">
        <label><?= Yii::t('main', 'В наличии') ?>:</label>

        <div class="item-info-param">
            <span id="item_num"><?= $item->top_item->num ?></span>&nbsp;
        </div>
        <input type="hidden" name="item_totalcount" id="item_totalcount" value="<?= $item->top_item->num ?>">
        <? if (Yii::app()->user->checkAccess('@showTaobaoLinkInUserCard')) {?>
            <div class="item-info-param" id="item_ontaobao">
                <a href="http://item.taobao.com/item.htm?id=<?= $item->top_item->num_iid ?>" target="_blank">
                    &nbsp;<?= Yii::t('main', 'на taobao.com') ?></a>
            </div>
        <? } ?>
    </div>
    <? if ($item->top_item->weight_calculated > 0) { ?>
        <div class="form-item">
            <label><?= Yii::t('main', 'Примерный вес 1 шт., грамм') ?>:</label>

            <div class="item-info-param">
                <span id="item_weight"><?= $item->top_item->weight_calculated ?></span>&nbsp;
            </div>
        </div>
    <? } ?>
    <div>
        <input type="submit" title="<?= Yii::t('admin', 'Сначала выберите характеристики товара') ?>"
               disabled="disabled" name="doGo" value="<?= Yii::t('main', 'Добавить в корзину') ?>"
               class="buy-btn bigger"/>
        <input type="button" name="dofav" value="<?= Yii::t('main', 'Добавить в избранное') ?>" class="fav-btn bigger"
               formaction="<?=
               Yii::app()->createUrl(
                 '/cabinet/favorite/add',
                 array(
                   'iid'      => $item->top_item->num_iid,
                   'download' => true
                 )
               ) ?>"
               onclick="addFavorite(this,<?= $item->top_item->num_iid ?>); return false;"/>
        <? if ((Yii::app()->user->notInRole(array('guest', 'user')))) { ?>
            <a class="ui-icon ui-icon-heart" style="display:inline-block; cursor: pointer;"
               title="<?= Yii::t('admin', 'Добавить в рекомендованное') ?>"
               href="<?= Yii::app()->createUrl('/admin/featured/add/', array('id' => $item->top_item->num_iid)) ?>"
               onclick="addFeatured(this,<?= $item->top_item->num_iid ?>); return false;"></a>
        <? } ?>
        <?/* if ((Yii::app()->user->notInRole(array('guest', 'user')))) { ?>
            <a class="ui-icon ui-icon-star" style="display:inline-block; cursor: pointer;"
               title="<?= Yii::t('admin', '"Экспорт в XML') ?>"
               href="<?= '/item/index/iid/' . $item->top_item->num_iid . '/exportType/CSV' ?>"></a>
        <? } */?>
        <a href="javascript:void(0);" onclick="window.history.back();">
            <?= Yii::t('main', 'Продолжить покупки') ?>
        </a>

        <div style="margin: 0 0 10px 10px;">
		<!-- Начало кода Социального Замка (id:6009) -->
<script type="text/javascript">
if ('undefined' == typeof(sl_sociallockers)) { var sl_sociallockers = [6009]; } else { sl_sociallockers.push(6009); } var sl6009_jqi = false; var sl6009_ale = false; function sl6009_iJQ() { if (!window.jQuery) { if (!sl6009_jqi) { if (typeof $ == 'function') { sl6009_ale = true; } var script = document.createElement('script'); script.type = "text/javascript"; script.src = "//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"; document.getElementsByTagName('head')[0].appendChild(script); sl6009_jqi = true; } setTimeout('sl6009_iJQ()', 50); } else { if (true == sl6009_ale) { $j = jQuery.noConflict(); } else { $j = jQuery; }
var sociallocker6009 = 'eyJpZCI6IjYwMDkiLCJwYWdlX3R5cGUiOiJjdXJyZW50IiwiZGlyX3VybCI6IlwvdGhlbWVzXC9kZWZhdWx0Mlwvc29jaWFsbG9ja2VyXC8iLCJwYWdlX3VybCI6Imh0dHA6XC9cL3Nob3BvZ29saWsyNC5jb21cL3J1XC9pdGVtXC80MDc2MDk0NjQ5MiIsIndpZHRoIjoiNTAwIiwiY29sb3JfYmFja2dyb3VuZCI6IjdlYjhmNyIsImNvbG9yX2Nsb3NlZF9hcmVhX2JhY2tncm91bmQiOiJmZjYzZmYiLCJ0aXRsZTEiOiIwS2ZSZ3RDKzBMSFJpeURRdjlDKzBMdlJnOUdIMExqUmd0R01JTkdCMExyUXVOQzAwTHJSZ3lEUXNpRFJnTkN3MExmUXZOQzEwWURRdFNBeE1DVXNJTkM2MEx2UXVOQzYwTDNRdU5HQzBMVWcwTDNRc0NEUXZ0QzAwTDNSZ3lEUXVOQzNJTkM2MEwzUXZ0Q1wvMEw3UXVpRFF2ZEM0MExiUXRUbz0iLCJzdWJ0aXRsZTEiOiIwTFwvUXZ0R0IwTHZRdFNEUmpkR0MwTDdRczlDK0lOQ1wvMEw3UXRDRFF1dEM5MEw3UXY5QzYwTERRdk5DNElOQ1wvMEw3Umo5Q3kwTGpSZ3RHQjBZOGcwTDdRc2RDMTBZblFzTkM5MEwzUmk5QzVJTkN5MExEUXZDRFF2OUMrMExUUXNOR0EwTDdRdWlBbzBMXC9SZ05DNElOQzkwTERRdHRDdzBZTFF1TkM0SU5DOTBMQWdJdENjMEwzUXRTRFF2ZEdBMExEUXN0QzQwWUxSZ2RHUElpRFF2dEdDSU5DUzBMclF2dEM5MFlMUXNOQzYwWUxRdFNEUXZ0Q3gwWVwvUXQ5Q3cwWUxRdGRDNzBZelF2ZEMrSU5DOTBMRFF0dEM4MExqUmd0QzFJQ0xRb05DdzBZSFJnZEM2MExEUXQ5Q3cwWUxSakNEUXROR0EwWVBRdDlHTTBZXC9RdkNJc0lOQzQwTDNRc05HSDBMVWcwTDNRc05DMjBMRFJndEM0MExVZzBMM1F0U0RRdDlDdzBZSFJoOUM0MFlMUXNOQzEwWUxSZ2RHUEtRPT0iLCJ0aXRsZTIiOiIwSlwvUXZ0QzBJTkMwMExEUXZkQzkwWXZRdk5DNElOR0IwWUxSZ05DKzBZZlF1dEN3MEx6UXVDRFF2OUMrMFlcL1FzdEM0MFlMUmdkR1BJTkMrMExIUXRkR0owTERRdmRDOTBZdlF1U0RRc3RDdzBMd2cwTFwvUXZ0QzAwTERSZ05DKzBMbz0iLCJjaGFuZ2VfYWZ0ZXJfb3BlbmluZyI6Im9uIiwidGl0bGUyX29wZW5lZCI6Ik1pNGcwS0hRdjlDdzBZSFF1TkN4MEw0aElOQ1gwTERRc2RDNDBZRFFzTkM1MFlMUXRTRFFzdEN3MFlnZzBMZlFzTkdCMEx2Umc5QzIwTFhRdmRDOTBZdlF1U0RRdjlDKzBMVFFzTkdBMEw3UXVqbz0iLCJjbG9zZWRfYXJlYV90ZXh0X2Nsb3NlZCI6IiIsIm1vZHVsZV93aGVyZV9sb2FkZWQiOiJzaXRlIiwidGFicyI6eyJ2a29udGFrdGVfbGlrZSI6eyJhcGlfaWQiOiI0NjE1NTYzIiwiYnV0dG9uX25hbWUiOiIwIiwic2hvd19jb3VudCI6Im9uIiwidGVsbF9mcmllbmRzIjoib24iLCJkZWxheSI6IjAifSwidmtvbnRha3RlX3NoYXJlIjp7ImNhcHRpb24iOiJcdTA0MWZcdTA0M2VcdTA0MzRcdTA0MzVcdTA0M2JcdTA0MzhcdTA0NDJcdTA0NGNcdTA0NDFcdTA0NGYiLCJzaG93X2NvdW50Ijoib24ifSwibWFpbF9saWtlIjp7ImJvcmRlcl90eXBlIjoiMSIsInRleHRfbW9pbWlyIjoiMiIsInRleHRfb2Rub2tsYXNzbmlraSI6IjIiLCJ3aGF0X2J1dHRvbnMiOiJvayIsInNob3dfY291bnQiOiJvbiJ9LCJnb29nbGVwbHVzIjp7InNob3dfY291bnQiOiJvbiIsImRlbGF5IjoiNSJ9LCJmYWNlYm9va19zaGFyZSI6eyJzaG93X2NvdW50Ijoib24ifX0sImdpZnRzIjpbeyJhY3Rpb25zIjoiMSIsImNvbnRlbnQiOiI8aDQ+XG5cdDxzdHJvbmc+XHUwNDFmXHUwNDQwXHUwNDM4IFx1MDQzN1x1MDQzMFx1MDQzYVx1MDQzMFx1MDQzN1x1MDQzNSBcdTA0NDJcdTA0M2VcdTA0MzJcdTA0MzBcdTA0NDBcdTA0MzAgXHUwNDMyXHUwNDNmXHUwNDM4XHUwNDQ4XHUwNDM4XHUwNDQyXHUwNDM1IFx1MDQzMiBcdTA0M2FcdTA0M2VcdTA0M2NcdTA0M2NcdTA0MzVcdTA0M2RcdTA0NDJcdTA0MzBcdTA0NDBcdTA0MzhcdTA0MzggXHUwNDNhIFx1MDQzN1x1MDQzMFx1MDQzYVx1MDQzMFx1MDQzN1x1MDQ0MyBcdTA0NDFcdTA0M2JcdTA0M2VcdTA0MzJcdTA0M2U6ICZxdW90O1x1MDQyMVx1MDQzYVx1MDQzOFx1MDQzNFx1MDQzYVx1MDQzMCZxdW90OzxcL3N0cm9uZz48XC9oND5cbiIsImdpZnRfcHJlY29udGVudCI6IiJ9XSwiY2xvc2VkX2FyZWFfdGV4dF9vcGVuZWQiOiJQR2cwUGdvSlBITjBjbTl1Wno3UW45R0EwTGdnMExmUXNOQzYwTERRdDlDMUlOR0MwTDdRc3RDdzBZRFFzQ0RRc3RDXC8wTGpSaU5DNDBZTFF0U0RRc2lEUXV0QyswTHpRdk5DMTBMM1JndEN3MFlEUXVOQzRJTkM2SU5DMzBMRFF1dEN3MExmUmd5RFJnZEM3MEw3UXN0QytPaUFtY1hWdmREdlFvZEM2MExqUXROQzYwTEFtY1hWdmREczhMM04wY205dVp6NDhMMmcwUGdvPSJ9';
jQuery.ajax({ type: 'POST', url: '/themes/default2/sociallocker/sociallocker.php?type=js', data: {'settings' : sociallocker6009}, dataType: "script" });
}} sl6009_iJQ();
</script>
<!-- Конец кода Социального Замка -->
<div id="sociallocker-6009">Загрузка замка...</div>
        </div>
    </div>
</form>