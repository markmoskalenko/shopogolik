<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="seller_info.php">
 * </description>
 * Рендеринг информации о продавце
 **********************************************************************************************************************/
?>
<div class="newshow">
    <?
    if (isset($item->taobao_item->userRateUrl)) {
        $sellerInfo = ItemSeller::getSellerInfoFromUrl($item->taobao_item->userRateUrl);
        /*
                 [nick] => 2013_日韓style
                 [wangwang] => 2013_日韩style
                 [location] =>
                 [sales] => 49034
                 [crowns] => 13
                 [rates] => Array (
                   [description_q] => 4.86802
                   [service_q] => 4.91974
                   [delivery_q] => 4.90428
                 )
                 [reviews] => Array (
                   [positive] => 6384
                   [normal] => 247
                   [negative] => 179 )
                 )
        */
    } else {
        $sellerInfo = false;
    }
    ?>
    <? if (isset($seller->paySuccess) && ($seller->paySuccess > 0)) { ?>
        <label><?= Yii::t('main', 'Продано этого товара') ?>:</label>
        <?= $seller->paySuccess; ?>
        <label><?= Yii::t('main', 'Возвратов') ?>:</label>
        <?= $seller->refundCount; ?>
      <br/>
    <? } ?>
    <? // Информация о продавце из товара ?>
    <label><?= Yii::t('main', 'Продавец') ?>&nbsp;(ID:<?= $seller->user_id ?>):</label>
    <a href="<?= Yii::app()->createUrl('/seller/index', array('nick' => ((DSConfig::getVal('search_use_fulltext_for_seller')!=1)?$seller->user_id:$seller->seller_nick))) ?>" class="seller-name">
        <?= $seller->seller_nick ?>
    </a>
    <? if ($item->top_item->city != false) { ?><br>
        <label><?= Yii::t('main', 'Находится в') ?>:</label>
        <? if (isset($item->top_item->state)) { ?>
            <?= $item->top_item->city ?>(<?= $item->top_item->state ?>)
        <? } else { ?>
            <?= $item->top_item->city ?>
        <?
        }
    }?>
    <br/>
    <? // Рейтинги - откуда и какие есть ?>
    <label><?= Yii::t('main', 'Рейтинг') ?>:</label>
    <?= ($sellerInfo && isset($sellerInfo->seller->sales))?$sellerInfo->seller->sales:$seller->seller_credit; ?>
        <span class="rating"><i class="i-rating rating_<?= DSGSeller::getCrownsFromSales(($sellerInfo && isset($sellerInfo->seller->sales))?$sellerInfo->seller->sales:$seller->seller_credit)-1; ?>"></i></span>

    <? if ($sellerInfo && isset($sellerInfo->seller->rates) && count($sellerInfo->seller->rates)) { ?>
        <?if (isset($sellerInfo->seller->rates['description_q'])) {?>
        <br/>
        <label><?= Yii::t('main', 'Соответствие товара описанию') ?>:</label>
            <?= round($sellerInfo->seller->rates['description_q'],2) ?>
        <? } ?>
        <?if (isset($sellerInfo->seller->rates['service_q'])) {?>
            <br/>
            <label><?= Yii::t('main', 'Сервис и обслуживание') ?>:</label>
            <?= round($sellerInfo->seller->rates['service_q'],2) ?>
        <? } ?>
        <?if (isset($sellerInfo->seller->rates['delivery_q'])) {?>
            <br/>
            <label><?= Yii::t('main', 'Скорость отправки товара') ?>:</label>
            <?= round($sellerInfo->seller->rates['delivery_q'],2) ?>
        <? } ?>
    <? } ?>
    <? if ($sellerInfo && isset($sellerInfo->seller->reviews) && count($sellerInfo->seller->reviews)) { ?>
        <br/>
        <label><?= Yii::t('main', 'Отзывы о продавце') ?>:</label>
        <?if (isset($sellerInfo->seller->reviews['positive'])) {?>
            &nbsp;<span style="color: green;">+<?=$sellerInfo->seller->reviews['positive'] ?></span>
        <? } ?>
        <?if (isset($sellerInfo->seller->reviews['normal'])) {?>
            &nbsp;/&nbsp;<?=$sellerInfo->seller->reviews['normal'] ?>
        <? } ?>
        <?if (isset($sellerInfo->seller->reviews['negative'])) {?>
            &nbsp;/&nbsp;<span style="color: red;">-<?=$sellerInfo->seller->reviews['negative'] ?></span>
        <? } ?>
    <? } ?>
</div>