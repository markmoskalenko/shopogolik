<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="input_props_img.php">
 * </description>
 * Рендеринг блока выбора свойств товара в карте товара - для свойств с изображениями
 **********************************************************************************************************************/
?>
<div class="input-props-images">
    <table style="width: 555px;">
        <tr>
            <td style="vertical-align: top;">
                <div style="display: block;  text-align: right; padding-right: 10px;">
                    <span style="font-weight: bold;"><?= $prop->name ?>:</span>
                    <input hidden="hidden" id="input-<?= $pid ?>" class="input_params" value="0">

                    <div>
                        <small>
                            <div class="select">
                                <span id="selres-<?= $pid ?>"><?= Yii::t('main', 'Не выбрано') ?></span>
                                <small>
                            </div>
            </td>
            <td style="width:425px; vertical-align: top;">
                <div style="display: block;">
                    <ul class="selectable" id="<?= $pid ?>">
                        <?php foreach ($prop->childs as $i => $val) { ?>
                            <?php if (trim($val->name) !== '') { ?>
                                <? if ($val->url == '') { ?>
                                    <li class="ui-widget-content" style="width: auto; height: auto;"
                                        id="<?= $pid ?>:<?= $val->vid ?>"><?= $val->name ?></li>
                                <?
                                } else {
                                    ?>
                                    <li class="ui-widget-content" style="vertical-align: top;"
                                        id="<?= $pid ?>:<?= $val->vid ?>">
                                        <? $imgTitle = Utils::removeOnlineTranslation($val->name);
                                        $imgPath = Img::getImagePath($val->url, '_40x40.jpg', false);?>
                                        <img title="<?= $imgTitle ?>" src="<?= $imgPath ?>">
                                    </li>
                                <? } ?>
                            <? } ?>
                        <? } ?>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
</div>


