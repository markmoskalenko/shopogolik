<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_not_found.php">
 * </description>
 * Рендеринг сообщения о ненайденном товаре
 **********************************************************************************************************************/
?>
<? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1;
if ($seo_disable_items_index) {
    Yii::app()->clientScript->registerMetaTag('noindex', 'robots');
} ?>
<div class="item-block" style="text-align: center; width: 915px;">
    <p class="notfound"><?= Yii::t('main', 'Увы, данного товара нет в наличии...') ?></p>
</div>

<div class="page-title"><?= Yii::t('main', 'Недавно Вы смотрели') ?>:</div>
<div class="products-list featured" style="width: 100%">
    <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>

    <? $this->widget(
      'application.components.widgets.SearchItemsList',
      array(
        'id'                         => 'recentUser-itemslist',
        'controlAddToFavorites'      => true,
        'controlAddToFeatured'       => true,
        'controlDeleteFromFavorites' => false,
        'lazyLoad'                   => true,
        'dataType'                   => 'itemsRecentUser',
        'pageSize'                   => 10,
//      'showControl' => null,
        'disableItemForSeo'          => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
      )
    );
    ?>
</div>

<div class="page-title"><?= Yii::t('main', 'Другие пользователи смотрели') ?>:</div>
<div class="products-list featured" style="width: 100%">
    <? $this->widget(
      'application.components.widgets.SearchItemsList',
      array(
        'id'                         => 'recentAll-itemslist',
        'controlAddToFavorites'      => true,
        'controlAddToFeatured'       => true,
        'controlDeleteFromFavorites' => false,
        'lazyLoad'                   => true,
        'dataType'                   => 'itemsRecentAll',
        'pageSize'                   => 10,
//      'showControl' => null,
        'disableItemForSeo'          => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
      )
    );
    ?>
</div>
