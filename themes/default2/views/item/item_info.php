<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_info.php">
 * </description>
 * Рендеринг блока табов описания товара
 **********************************************************************************************************************/
?>
<div class="item-info">
    <div class="item-params" style="margin-top: 0;">
        <div class="blue-tabs tabs">
            <a href="javascript:void(0);" id="tab-1" class="active">
                <span><?= Yii::t('main', 'Описание товара') ?></span>
            </a>
            <a href="javascript:void(0);" id="tab-2">
                <span><?= Yii::t('main', 'Фото') ?></span>
            </a>
            <a href="javascript:void(0);" id="tab-3">
                <span><?= Yii::t('main', 'Экономия') ?></span>
            </a>
        </div>
    </div>
    <div id="tab-1-content" class="tab shown">
        <? if (isset($item->top_item->item_attributes)) { ?>
            <ul>
                <? foreach ($item->top_item->item_attributes as $attribute) { ?>
                    <li class="block_1">
                        <strong><?= $attribute->prop ?>:</strong><br><?= $attribute->val ?>
                    </li>
                <? } ?>
            </ul>
        <?
        } else {
            ?>
            <!--  Old style output -->
            <table class="item-table">
                <? if (is_array($props)) {
                    foreach ($props as $pid => $prop) {
                        ?>
                        <? if ($prop->name !== '') { ?>
                            <tr>
                                <td><?= $prop->name ?></td>
                                <th><?= $prop->value ?></th>
                            </tr>
                        <? } ?>
                    <?
                    }
                }?>
            </table>
        <? } ?>
    </div>
    <div id="tab-2-content" style="overflow: auto; height: 720px; overflow-y: scroll" class="tab">
        <? if (isset($item->top_item->desc) && ($item->top_item->desc)) {
            echo $this->renderPartial('item_details', array('src' => $item->top_item->desc), true, false);
        } else {
            ?>
            <div id="item-detail-block">
                <script async type="text/javascript">
                    <?if (isset($item->top_item->descUrl) && ($item->top_item->descUrl)) {?>
                    loadItemDetailsFromUrl('<?=urlencode($item->top_item->descUrl)?>');
                    <? } else {?>
                    loadItemDetails(<?=$item->top_item->num_iid?>);
                    <? } ?>
                </script>
            </div>
        <? } ?>
    </div>
    <? if (isset($item->top_item->priceTable)) { ?>
        <div id="tab-3-content" class="tab">
            <?= $this->renderPartial('price_table', array('prices' => $item->top_item->priceTable)) ?>
        </div>
    <? } ?>
</div>
