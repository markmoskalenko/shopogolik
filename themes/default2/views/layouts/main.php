 <?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="main.php">
 * </description>
 * Лэйаут фронта сайта
 **********************************************************************************************************************/
?>
<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $this->pageTitle ?></title>
    <link rel="icon" href="<?= $this->frontThemePath ?>/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?= $this->frontThemePath ?>/images/favicon.ico" type="image/x-icon"/>
    <? /* Подключение css и javaScript, которые используются во всех представлениях фронта,
          для сомнительного удобства вынесено в отдельные файлы.*/
    ?>
    <? $this->renderPartial('//layouts/dropshopGlobalCss', array()); ?>
    <? $this->renderPartial('//layouts/dropshopGlobalJs', array()); ?>
    <script type="text/javascript" src="<?= $this->frontThemePath ?>/js/main.js?v=<?= Search::$intversion ?>"></script>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<script src="jquery-ui-1.9.2.custom/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="jquery-ui-1.9.2.custom/js/jquery.responsive_countdown.min.js"></script>
	
	<META NAME="z-payment.label" CONTENT="z-payment label 091E7CE12A39BEA5D9E7E2E8D40A2DEC">
<meta name="w1-verification" content="166771324980" />
<script type="text/javascript">window.dload=function(e){if(document.addEventListener){if(typeof window.magictab!='undefined'&&window.magictab.__loaded===true){e()}else{document.addEventListener("mgtLoaded",e,false)}}else if(document.attachEvent){document.attachEvent("mgtLoaded",e)}};(function(c,a){window.magictab=a;var b,d,h,e;b=c.createElement("script");b.type="text/javascript";b.async=!0;b.src=("https:"===c.location.protocol?"https:":"http:")+'//api.magictab.ru/js/v2.0/magictab-1.0.js';d=c.getElementsByTagName("script")[0];d.parentNode.insertBefore(b,d);a._i=[];a.init=function(b,c,f){var g=a;"undefined"!==typeof f?g=a[f]=[]:f="magictab";g.people=g.people||[];g.plugins=g.plugins||[];a._autoload=['alerts','main-widget','post-watcher','post-checkout-widget'];a._i.push([b,c,f])};a.__SV=1.3;a._e=document.createEvent("Event");a._e.initEvent('mgtLoaded', true, true);})(document,window.magictab||[]);magictab.init("217d43d8f45858aa0fc40eab3de0e771");</script>
</head>

<body
  class="<?= $this->columns ?> <?=
  Utils::TransLang() ?> <?= $this->id ?> <?= (!($this->id == $this->body_class)) ? $this->body_class : '' ?>">
<header>
    <div class="user-info">
        <div class="user-info-block">
            <table style="width:100%;">
                <tbody>
                <tr>
                    <td style="width:50%;">
                        <div style="text-align:left;">
                            <a class="space" href="<?= $this->createUrl('/article/about') ?>"><?=
                                Yii::t(
                                  'main',
                                  'О нас'
                                ) ?></a>
                            <a class="space"
                               href="<?= $this->createUrl('/tools/question') ?>"><?=
                                Yii::t(
                                  'main',
                                  'Задать вопрос'
                                ) ?></a>
                            <a class="space" href="<?= $this->createUrl('/article/voprosi-i-otveti') ?>"><?=
                                Yii::t(
                                  'main',
                                  'Помощь'
                                ) ?></a>
                            <a class="space"
                               href="<?= $this->createUrl('/article', array('url' => 'contacts')) ?>"><?=
                                Yii::t(
                                  'main',
                                  'Контакты'
                                ) ?></a>
                        </div>
                        <div class="lang">
                            <? // Блок выбора языка отображения фронта?>
                            <? $this->widget('application.components.widgets.languageBlock'); ?>
                        </div>
                    </td>
                    <td style="text-align:right;">
                        <div class="currency">
                            <? // Блок выбора валюты отображения фронта?>
                            <? $this->widget('application.components.widgets.currencyBlock'); ?>
                        </div>
                        <div class="">
                            <? // Блок логина, входа в кабинет, остатка на счету?>
                            <? $this->widget('application.components.widgets.userBlock'); ?>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="user-info-block" style="height: 130px;">
            <div class="b-upperpart">
                <a href="/"><img src="/images/logo-shop-24.png"></a>
                <div class="upperpart-phone">
                    <div>
                        <img style="float:left;"
                             src="<?= $this->frontThemePath ?>/images/russia.png">
                        <?= cms::customContent('phone-in-russia') ?>
                    </div>
                </div>
                <div class="upperpart-skype">
                    <img src="<?= $this->frontThemePath ?>/images/skype.png">
                    <a href="skype:shopogolik_24">Skype</a>
                </div>
                <div class="upperpart-trash">
                    <div class="basket-block">
                        <?php
                        $this->widget('application.components.widgets.CartBlock');
                        ?>
                    </div>
                </div>
                <div class="upperpart-call"></div>
                <div class="upperpart-calc"></div>
                <div class="upperpart-cabinet"></div>
                <div class="b-search-wrapper">
                    <? // Блок поискового запроса ?>
                    <?php $this->widget('application.components.widgets.SearchQueryBlock'); ?>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter25369892 = new Ya.Metrika({id:25369892,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/25369892" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript"
src="http://consultsystems.ru/script/20667/" charset="utf-8">
</script>
<div class="wrapper">
    <?php if ($this->id == 'site' && $this->body_class == 'home') { ?>
       
<a href="http://shopogolik24.com/ru/user/register"><img src="/images/gr098.jpg"></a>
	<script>
$(function() {
		$("#first_countdown").ResponsiveCountdown({
		   target_date:"2020/1/1 00:00:00",
		   time_zone:0,target_future:true,
		   set_id:0,pan_id:0,day_digits:2,
		   fillStyleSymbol1:"rgba(0, 0, 0, 1)",
		   fillStyleSymbol2:"rgba(255, 255, 255, 1)",
		   fillStylesPanel_g1_1:"rgba(105, 105, 105, 1)",
		   fillStylesPanel_g1_2:"rgba(255, 255, 255, 1)",
		   fillStylesPanel_g2_1:"rgba(0, 0, 0, 1)",
		   fillStylesPanel_g2_2:"rgba(41, 51, 64, 1)",
		   text_color:"rgba(137, 155, 181, 1)",
		   text_glow:"rgba(200,200,200,200)",
		   show_ss:true,show_mm:true,
		   show_hh:true,show_dd:false,
		   f_family:"Verdana",show_labels:true,
		   type3d:"single",max_height:120,
		   days_long:"DAYS",days_short:"dd",
		   hours_long:"часы",hours_short:"hh",
		   mins_long:"минуты",mins_short:"mm",
		   secs_long:"секунды",secs_short:"ss",
		   min_f_size:10,max_f_size:30,
		   spacer:"none",groups_spacing:0,text_blur:2,
		});		
	});
</script>
<div id="first_countdown" style="position: relative; width: 100%; height: 100px;"></div>


	<? } ?>
    <div class="clear after-head"></div>
    <?php $this->widget(
      'zii.widgets.CBreadcrumbs',
      array(
        'links'    => $this->breadcrumbs,
        'homeLink' => CHtml::link(Yii::t('main', 'Главная'), array('/site/index'))
      )
    );
    ?>
    <? // Блок вывода всплывающих сообщений, если они есть?>
    <?php $this->widget('application.components.widgets.MessagesBlock') ?>
    <div class="left-col">
        <? // Вывод параметров поиска в поисковых же страницах?>
        <?  if (in_array(
            $this->id,
            array('search', 'category', 'favorite', 'brand', 'seller', 'site')
          ) && ($this->body_class != 'cabinet')
        ) {
            if (($this->id != 'site')) {
                $this->widget(
                  'application.components.widgets.SearchParams',
                  array(
                    'type'         => $this->id,
                    'params'       => $this->params['params'],
                    'cids'         => $this->params['cids'],
                    'bids'         => $this->params['bids'],
                    'groups'       => $this->params['groups'],
                    'filters'      => $this->params['filters'],
                    'multiFilters' => $this->params['multiFilters'],
                    'suggestions'  => $this->params['suggestions'],
                    'priceRange'   => $this->params['priceRange'],
                  )
                );
            }
            ?>
            <? // Вертикальный список категорий на главной и в поисковых страницах ?>
            <h4 class="page-title"><?= Yii::t('main', 'Категории товаров') ?></h4>
            <div id="main-cats-menu-ajax"><span style="text-align: center;"><img src="/images/ajax-loader.gif"></span>
            </div>
            <? if (Yii::app()->user->getRole() != 'guest') { ?>
                <h4 class="page-title"><?= Yii::t('main', 'Избранное') ?></h4>
                <div id="favorites-menu">
                    <? // Блок категорий избранного, который выводится ниже меню категорий?>
                    <?
                    $this->widget(
                      'application.components.widgets.FavoritesMenuBlock',
                      array(
                        'adminMode' => false,
                      )
                    );
                    ?>
                </div>
            <? } ?>
            <? // Блок профайлера?>
            <? $this->widget('application.components.widgets.ProfilerBlock', array()); ?>
        <?
        } elseif ($this->body_class == 'cabinet') {
            // Блок меню кабинета
            $this->widget('application.components.widgets.cabinetMenuBlock');
        } elseif ($this->id == 'article') {
            ?>
            <?= cms::menuContent('help-vertical-menu') ?>
        <?
        } ?>
    </div>
    <?php if ($this->body_class == 'cabinet') { ?>
    <div class="main-col" id="cabinet">
        <? $this->widget('application.components.widgets.UserNoticeBlock'); ?>
        <? } elseif ($this->id == 'article') { ?>
        <div class="main-col" id="article">
            <? } else { ?>
            <div class="main-col" id="content">
                <? } ?>
                <? //Здесь выводится контент представления контроллера ?>
                <?= $content ?>
            </div>
            <div class="clear" style="height: 15px;"></div>
            <?php if ($this->id == 'site' && $this->body_class == 'home') { ?>
                <div>
                    <? //Рендеринг блока брендов?>
                    <? $this->widget('application.components.widgets.BrandsBlock'); ?>
                </div>
                <div class="clear"></div>
                <?= cms::customContent('main') ?>
            <? } ?>
            <div class="footer">
                <div class="menu">
                    <?= cms::menuContent('main-footer-menu') ?>
                </div>
                <div class="info">
                    <div class="paySystems">
                        <?= cms::customContent('main-paysystems') ?>
                    </div>
                    <div class="footerText"><?= cms::customContent('main-footertext') ?></div>
                    <div class="copyright">© 2014, <?= Yii::t('main', 'разработка и сопровождение') ?>: <a
                          href="http://dropshop.pro">dropshop.pro</a></div>
                </div>
                <div class="version"><?= Search::$version; ?></div>
            </div>
            <? //Скрипт кнопки вверх, назад-вперёд в поисковых выдачах ?>
            <? if (in_array(
                $this->id,
                array('search', 'category', 'favorite', 'brand', 'seller')
              ) && ($this->body_class != 'cabinet')
            ) {
                ?>
                <script src="<?= $this->frontThemePath ?>/js/up.js" type="text/javascript"></script>
            <? } ?>
            <? // Диалог корректировки переводов - используется повсеместно?>
            <div id="translationDialog" style="display: none;">
                <? if ((!in_array(Yii::app()->user->getRole(), array('guest', 'user')))) { ?>
                    <?php $this->beginWidget(
                      'zii.widgets.jui.CJuiDialog',
                      array(
                        'id'      => 'translationDialog',
                        'options' => array(
                          'title'     => Yii::t('main', 'Редактирование перевода'),
                          'autoOpen'  => false,
                          'modal'     => true,
                          'resizable' => false,
                          'position'  => "[725,100]",
                          'style'     => "[725,100]",
                          'width'     => '70%',
                          'height'    => 'auto',
                        ),
                      )
                    );
                    echo $this->renderPartial('//site/translate', array());
                    $this->endWidget('zii.widgets.jui.CJuiDialog');
                    ?>
                <? } ?>
            </div>

            <? //Ленивая загрузка изображений. Используется в самых разных местах?>
            <? if (DSConfig::getVal('site_images_lazy_load') == 1) { ?>
                <script type="text/javascript"
                        src="<?= $this->frontThemePath ?>/js/<?= YII_DEBUG ? 'jquery.lazyload.js' : 'jquery.lazyload.min.js' ?>"></script>
                <script type="text/javascript">
                    $(function () {
                        $("img.lazy").show().lazyload({
                            effect: "fadeIn",
                            effect_speed: 500
//      skip_invisible : false
                        });
                    });
                </script>
            <? } ?>
        </div>
</body>
</html>