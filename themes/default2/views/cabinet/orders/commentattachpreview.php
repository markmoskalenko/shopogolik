<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="commentattachpreview.php">
 * </description>
 * Рендеринг предварительного просмотра аттача\картинки в комментарии к заказу или лоту
 **********************************************************************************************************************/
?>
<?

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/themes/' . DSConfig::getVal('site_front_theme').'/css/magiczoomplus.css', 'screen');
   Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/themes/' . DSConfig::getVal('site_front_theme').'/js/magiczoomplus.js', CClientScript::POS_HEAD);
?>
<div class="blocknormal">
<a href="<?= Yii::app()->createUrl('/img/commentattach', array('isItem' => $isItem, 'id' => $attach->id)) ?>"
   class="MagicZoomPlus"
   rel="disable-zoom: true; expand-size: width=800px; group: panzoom; hint-text:;">
    <img style="width:100px;height:auto;margin: 0;"
         src="<?= Yii::app()->createUrl('/img/commentattach', array('isItem' => $isItem, 'id' => $attach->id)) ?>"/></a>
</div>