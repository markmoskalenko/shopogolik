<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="view.php">
 * </description>
 * Просмотр заказа в кабинете
 **********************************************************************************************************************/
?>
<?php $checkout_order_reconfirmation_needed = DSConfig::getVal( 'checkout_order_reconfirmation_needed' ) == 1; ?>
<div class="cabinet-content content payment">
<h4><?= Yii::t( 'main', 'Заказ' ) ?>:<span><?= $order->uid . '-' . $order->id ?></span></h4>

<div class="delivery-desc">
    <table>
        <tr>
            <th><?= Yii::t( 'main', 'Дата заказа' ) ?>:</th>
            <td><?= date( 'd.m.Y H:i', $order->date ) ?></td>
        </tr>
        <? if ( $order->extstatuses ) { ?>
            <tr>
                <th><?= Yii::t( 'main', 'Статус заказа' ) ?>:</th>
                <td class="select"><?= $order->statuses->name . ' (' . $order->extstatuses . ')' ?></td>
            </tr>
        <?
        } else {
            ?>
            <tr>
                <th><?= Yii::t( 'main', 'Статус заказа' ) ?>:</th>
                <td class="select"><?= $order->statuses->name ?></td>
            </tr>
        <? } ?>
        <?php if ( $order->code ) { ?>
            <tr>
                <th><?= Yii::t( 'main', 'Трек-номер посылки' ) ?>:</th>
                <td class="select"><?= $order->code ?></td>
            </tr>
        <?php } else { ?>
            <? if ( count( $order->ordersItemsLegend ) > 0 ) { ?>
                <th><?= Yii::t( 'admin', 'Статусы лотов' ) ?></th>
                <td class="select">
                    <? foreach ( $order->ordersItemsLegend as $itemStatus ) { ?>
                        <span
                            style="<?= $itemStatus[ 'excluded' ] ? 'color:red;' : '' ?>"><?= Yii::t( 'main', $itemStatus[ 'name' ] ) ?></span>: <?= $itemStatus[ 'cnt' ] ?>&nbsp;
                    <? } ?>
                </td>
            <?php
            }
        } ?>
    </table>
</div>
<div class="order-payments">
    <? $this->widget(
        'application.components.widgets.OrderPaymentsBlock',
        array(
            'orderId'  => $order->id,
            'pageSize' => 5,
        )
    );
    ?>
</div>
<div id="order-history">
    <? $this->widget(
        'application.components.widgets.EventsBlock',
        array(
            'subjectId'    => $order->id,
            'eventsType'   => '^ORDER_|^ORDERITEM_',
            'pageSize'     => 1000,
            'showInternal' => false,
        )
    );
    ?>
</div>
<div class="order-comments">
    <? $this->widget(
        'application.components.widgets.OrderCommentsBlock',
        array(
            'orderId'     => $order->id,
            'orderItemId' => false,
            'public'      => true,
            'pageSize'    => 5,
            'imageFormat' => '_200x200.jpg',
        )
    );
    ?>
</div>

<h4><?= Yii::t( 'main', 'Список товаров' ) ?></h4>

<table class="cart-header">
    <tbody>
    <th style="width: 110px;"><?= Yii::t( 'main', 'Товар' ) ?></th>
    <th style="width: 335px;"><?= Yii::t( 'main', 'Описание' ) ?></th>
    <th style="width: 90px;"><?= Yii::t( 'main', 'Количество' ) ?></th>
    <th><?= Yii::t( 'main', 'Цена' ) ?></th>
    <th style="margin-right: 25px;"><?= Yii::t( 'main', 'Сумма' ) ?></th>
    </tbody>
</table>
<?php foreach ( $order->ordersItems as $item ) { ?>
    <? $this->widget(
        'application.components.widgets.OrderItem',
        array(
            'orderItem'      => $item,
            'readOnly'       => true,
            'allowDelete'    => false,
            'publicComments' => true,
            'imageFormat'    => '_200x200.jpg',
        )
    );
    ?>
<? } ?>
<div class="clear">
</div>
<h4><?= Yii::t( 'main', 'Доставка' ) ?></h4>
<hr/>
<table class="delivery-desc">
    <tr>
        <th><?= Yii::t( 'main', 'Вес посылки' ) ?>:</th>
        <?
        $orderWeight = ( isset( $order->manual_weight ) && ( $order->manual_weight ) ) ? $order->manual_weight : $order->calculated->actual_lots_weight;
        $orderWeight = ( $orderWeight ) ? $orderWeight : $order->weight;
        ?>
        <td><?= $orderWeight ? $orderWeight . Yii::t( 'main', " грамм" ) : Yii::t( 'main', "рассчитывается..." ) ?></td>
    </tr>
    <tr>
        <th><?= Yii::t( 'main', 'Сумма за доставку' ) ?>:</th>
        <?
        $orderDelivery = ( $order->manual_delivery || $order->manual_delivery === '0' ) ? $order->manual_delivery : $order->delivery;
        ?>
        <td>
            <? if ( $orderDelivery > 0 ) { ?>
                <?=
                Formulas::priceWrapper(
                    Formulas::convertCurrency( $orderDelivery, DSConfig::getVal( 'site_currency' ), DSConfig::getCurrency() )
                ); ?>
            <?
            } else {
                ?>
                <?= Yii::t( 'main', 'рассчитывается...' ); ?>
            <? } ?>
        </td>
    </tr>
    <tr>
        <th><?= Yii::t( 'main', 'Почтовый идентификатор' ) ?>:</th>
        <td><?= ( $order->code ) ? $order->code : Yii::t( 'main', 'не получен' ) ?></td>
    </tr>
    <tr>
        <th><?= Yii::t( 'main', 'Дата отправки' ) ?>:</th>
        <? if ( $order->status == 'SEND_TO_CUSTOMER' ) {
            $lastEvent = EventsLog::getLastEventForSubj( $order->id, 'SEND_TO_CUSTOMER' );
            if ( $lastEvent ) {
                $sendDate = $lastEvent[ 0 ]->date;
            } else {
                $sendDate = false;
            }
        } else {
            $sendDate = false;
        }
        ?>
        <td><?= ( $sendDate ) ? $sendDate : Yii::t( 'main', 'Не отправлен' ); ?></td>
    </tr>
    <tr>
        <th><?= Yii::t( 'main', 'Способ доставки' ) ?>:</th>
        <td>
            <?php $delivery = Deliveries::getDelivery( 0, false, $order[ 'delivery_id' ] );
            if ( isset( $delivery->name ) ) {
                echo $delivery->name;
            } else {
                echo Yii::t( 'main', ' Не определено' );
            }?>
        </td>
    </tr>
    <tr>
        <th><?= Yii::t( 'main', 'Адрес' ) ?>:</th>
        <td>
            <? if ( $order->addresses ) { ?>
                <?=
                Deliveries::getCountryName(
                    $order->addresses[ 'country' ]
                ) ?>, <?= $order->addresses[ 'index' ] ?>, <?= $order->addresses[ 'city' ] ?>, <?= $order->addresses[ 'address' ] ?>.
                <?= $order->addresses[ 'firstname' ] ?> <?= $order->addresses[ 'lastname' ] ?>
                <?= $order->addresses[ 'phone' ] ?>
            <? } ?>
        </td>
    </tr>
</table>

<hr/>
<div class="delivery-desc">
    <table>
        <tr>
            <th><?= Yii::t( 'main', 'Сумма за товары' ) ?>:</th>
            <td class="select"><?
                $orderSum = ( $order->manual_sum ) ? $order->manual_sum : $order->sum;
                echo Formulas::priceWrapper(
                    Formulas::convertCurrency( $orderSum, DSConfig::getVal( 'site_currency' ), DSConfig::getCurrency() )
                )?></td>
        </tr>
        <tr>
            <th><?= Yii::t( 'main', 'Сумма за доставку' ) ?>:</th>
            <td class="select"><?
                $orderDelivery = ( $order->manual_delivery || ( $order->manual_delivery === '0' ) ) ? $order->manual_delivery : $order->delivery;
                if ( $orderDelivery > 0 ) {
                    echo Formulas::priceWrapper(
                        Formulas::convertCurrency(
                            $orderDelivery,
                            DSConfig::getVal( 'site_currency' ),
                            DSConfig::getCurrency()
                        )
                    );
                } else {
                    echo Yii::t( 'main', 'не рассчитано' );
                }?></td>
        </tr>
        <tr class="total">
            <th><?= Yii::t( 'main', 'Общая сумма заказа' ) ?>:</th>
            <td class="select"><?
                $orderSumCurr = Formulas::convertCurrency( $orderSum, DSConfig::getVal( 'site_currency' ), DSConfig::getCurrency(), false, $order->date );
                $orderDeliveryCurr = Formulas::convertCurrency( $orderDelivery, DSConfig::getVal( 'site_currency' ), DSConfig::getCurrency(), false, $order->date );
                $orderTotal = Formulas::priceWrapper( $orderSumCurr + $orderDeliveryCurr );
                echo $orderTotal; ?>
                <? if ( ( DSConfig::getVal( 'rates_use_currency_log' ) == 1 ) ) { ?>
                    &nbsp;<span
                        style="font-size: small"><?= Yii::t( 'main', 'по курсу на' ) . ' ' . date( 'd.m.Y', $order->date ) ?></span>
                <? } ?>
            </td>
        </tr>
    </table>
</div>
<?php
$payed = round( OrdersPayments::getPaymentsSumForOrder( $order->id ), 2 );
$pay = round( $orderDelivery + $orderSum, 2 );
$paymentSumm = Formulas::priceWrapper( Formulas::convertCurrency( $orderSum + $orderDelivery - OrdersPayments::getPaymentsSumForOrder( $order->id ),
        DSConfig::getVal( 'site_currency' ),
        DSConfig::getCurrency()
    )
);
//================================================================================
if ( ( ( $pay > $payed ) && ( abs( $pay - $payed ) > 1 ) ) && ( ( $checkout_order_reconfirmation_needed && ( $orderDelivery >= 0 ) && ( $order->manual_delivery != '' ) ) || !$checkout_order_reconfirmation_needed ) ) {
    ?>
    <div style="padding: 10px 10px;">
        <h4><?= Yii::t( 'main', 'Требуется оплата' ) ?></h4>
        <?=
        Yii::t(
            'main',
            'В результате уточнения стоимости товаров и (или) доставки, по заказу'
        ) ?> <b>№<?= $order->uid . '-' . $order->id ?></b><br/>
        <?= Yii::t( 'main', 'вам необходимо оплатить или доплатить' ) ?>&nbsp;
        <br/><span style="color: red; font-weight: bold;"><?=
            Formulas::priceWrapper(
                Formulas::convertCurrency(
                    $orderSum + $orderDelivery - OrdersPayments::getPaymentsSumForOrder( $order->id ),
                    DSConfig::getVal( 'site_currency' ),
                    DSConfig::getCurrency()
                )
            ) ?></span>
        <br/>
        <input value="<?= Yii::t( 'main', 'Оплатить сейчас' ) ?>" onclick="location.href='<?=
        Yii::app()
            ->createUrl( '/cabinet/balance/order', array( 'oid' => $order->id ) ) ?>'" class="buy-btn bigger"
               type="button"/>
    </div>
<? } ?>
<div class="two-btn">
    <? if ( OrdersStatuses::isAllowedStatusForOrder( 'CANCELED_BY_CUSTOMER', $order->id, Yii::app()->user->id, null ) ) { ?>
        <form action="<?= Yii::app()->createUrl( '/cabinet/orders/delete', array( 'oid' => $order->id ) ) ?>">
            <button class="red-btn bigger"><?= Yii::t( 'main', 'Отменить заказ' ) ?></button>
        </form>
    <? } ?>
    <form action="<?= Yii::app()->createUrl( '/cabinet/support' ) ?>">
        <button style="float:right;"
                class="blue-btn bigger"><?= Yii::t( 'main', 'Обратиться в службу поддержки' ) ?></button>
    </form>

</div>
</div>
