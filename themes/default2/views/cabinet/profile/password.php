<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="password.php">
 * </description>
 **********************************************************************************************************************/
?>
<div class="blue-tabs">
    <a href="<?= Yii::app()->createUrl('/cabinet/profile') ?>">
        <span><?= Yii::t('main', 'Личные данные') ?></span>
    </a>
    <a href="<?= Yii::app()->createUrl('/cabinet/profile/email') ?>">
        <span><?= Yii::t('main', 'Изменить E-mail') ?></span>
    </a>

    <div class="active-tab">
        <span><?= $this->pageTitle ?></span>
    </div>
    <a href="<?= Yii::app()->createUrl('/cabinet/profile/address') ?>">
        <span><?= Yii::t('main', 'Список адресов') ?></span>
    </a>
</div>
<div class="cabinet-content">
    <div class="form">
        <?= $form ?>
    </div>
</div>