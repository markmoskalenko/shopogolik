<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Главная страница кабинета
 * http://<domain.ru>/ru/cabinet
 * var $user = - данные о пользователе
 * var $ordersByStatuses = array - заказы по статусам
 * (0 => array(
 * 'id' => '21'
 * 'value' => 'IN_PROCESS'
 * 'name' => 'В процессе обработки'
 * 'descr' => 'Статус по умолчанию для всех новых заказов. Присваивается автоматически.'
 * 'manual' => '1'
 * 'aplyment_criteria' => 'select oo.id from orders oo where oo.status in (\'PAUSED\')
 * and (uid=:uid or :uid is null) and (manager=:manager or :manager is null)'
 * 'auto_criteria' => ''
 * 'order_in_process' => '0'
 * 'enabled' => '1'
 * 'count' => '36'
 * 'lastdate' => '1402398695'
 * 'totalsum' => '12920.15'
 * 'totalpayed' => '11056.76'
 * 'totalnopayed' => '-1863.39'
 * ))
 **********************************************************************************************************************/
?>
<div class="base-info">
    <a href="<?= Yii::app()->createUrl('/cabinet/profile') ?>" class="edit-link">
        <span class="ui-icon ui-icon-power" style="display:inline-block;"/></span>
        <?= Yii::t('main', 'Настроить профиль') ?>
    </a>

    <h3 class="title"><?= Yii::t('main', 'Добро пожаловать') ?>, <?= Yii::app()->user->firstname ?>!</h3>

    <p><?= Yii::t('main', 'Номер счета') ?>:&nbsp;<b><?= Yii::app()->user->getPersonalAccount() ?></b></p>
    <? if (true) { ?>
        <p><?= Yii::t('main', 'Ваш персональный промо-код') ?>
            :&nbsp;<b><?= Users::getPromoByUid(Yii::app()->user->id) ?></b></p>
    <? } ?>
    <p><?= Yii::t('main', 'Остаток на счете') ?>:
        <strong><?=
            Formulas::priceWrapper(
              Formulas::convertCurrency(
                Users::getBalance(Yii::app()->user->id),
                DSConfig::getVal('site_currency'),
                DSConfig::getCurrency()
              ),
              DSConfig::getCurrency()
            ); ?></strong>
    </p>
    <? if ($user->manager) { ?>
        <?= Yii::t('main', 'Ваш персональный менеджер') ?>:<br/>
        <?= Yii::t('main', 'EMail') ?>:<a href="email:<?= $user->manager->email ?>"><?= $user->manager->email ?></a>
        <? if ($user->manager->skype) { ?>
            <br/><?= Yii::t('main', 'Skype') ?>:<a
              href="skype:<?= $user->manager->skype ?>"><?= $user->manager->skype ?></a>
        <? } ?>
        <? if ($user->manager->vk) { ?>
            <br/><?= Yii::t('main', 'Vk') ?>:<a href="<?= $user->manager->vk ?>"><?= $user->manager->vk ?></a>
        <? } ?>
    <? } ?>
</div>
<div class="blue-tabs">
    <div class="active-tab">
        <span><?= Yii::t('main', 'Ваши заказы, по статусам') ?></span>
    </div>
</div>
<div class="cabinet-content">
    <?
    $isEmpty = true;
    foreach ($ordersByStatuses as $orderByStatus) {
        if ($orderByStatus['count'] > 0) {
            $isEmpty = false;
            break;
        }
    } ?>
    <? if (!$isEmpty) { ?>
        <h3><strong><?= Yii::t('main', 'Распределение ваших заказов по статусам') ?>:</strong></h3>
        <ul class="orders-by-status">
            <? foreach ($ordersByStatuses as $orderByStatus) {
                if ($orderByStatus['count'] > 0) {
                    ?>
                    <li>
                        <a href="/cabinet/orders/index/type/<?= $orderByStatus['value'] ?>">
                            <div class="header">
                                <?= Yii::t('main', $orderByStatus['name']) ?>
                                <span class="header">
              <?= $orderByStatus['count'] . Yii::t('main', 'шт') ?>
          </span>
                            </div>
                            <?= date('d.m.Y H:i', $orderByStatus['lastdate']) ?>
                        </a>

                        <div style="display: table;height: 60%;">
                            <div class="comment"><?= Yii::t('main', $orderByStatus['descr']) ?></div>
                        </div>
                    </li>
                <?
                }
            } ?>
        </ul>
    <?
    } else {
        ?>
        <h3><strong><?= Yii::t('main', 'У вас еще нет заказов') ?>.</strong></h3>
    <? } ?>
</div>
