<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="list.php">
 * </description>
 * Просмотр списка избранных товаров в кабинете
 **********************************************************************************************************************/
?>
<div class="blue-tabs">
    <div class="active-tab">
        <span><?= $this->pageTitle ?></span>
    </div>
</div>
<div class="cabinet-content">
    <?php
    $this->widget(
      'zii.widgets.CMenu',
      array(
        'itemCssClass'=>'form-item submit',
        'items' => array(
/*          array(
            'label' => Yii::t('main', 'Экспорт в YML'),
            'icon'  => 'icon-plus',
            'url'   => Yii::app()->controller->createUrl('getYML', array('uid' => Yii::app()->user->id))
          ),
*/
          array(
            'label' => Yii::t('admin', 'Очистить весь список'),
            'icon'  => 'icon-refresh',
            'url'   => Yii::app()->controller->createUrl('delete', array('id' => -1)),
          ),
        )
      )
    );
    ?>
<?/* Экспорт в YML   <div>
        <?= Yii::t('main', 'Категорий для экспорта') . ':' . $categoriesCount; ?>
        <?= Yii::t('main', 'Товаров для экспорта') . ':' . $itemsCount; ?>
    </div> */?>
<? if (isset($favoriteMenu) && is_array($favoriteMenu) && count($favoriteMenu)) {?>
<div class="cabinet-favorites-categories">
    <? foreach ($favoriteMenu as $category) { ?>
        <a href="<?=Yii::app()->controller->createUrl('list', array('cid' => $category['cid']))?>"><?=$category['view_text']?></a>
    <? } ?>
</div>
<? } ?>
    <div class="cabinet-table">
        <? $this->widget(
          'application.components.widgets.SearchItemsList',
          array(
            'id'                         => 'favorite-itemslist',
            'showControl'                => true,
            'controlAddToFavorites'      => false,
            'controlAddToFeatured'       => false,
            'controlDeleteFromFavorites' => true,
            'lazyLoad'                   => true,
            'dataType'                   => 'itemsFavorite',
            'pageSize'                   => 40,
            'dataProvider'               => $dataProvider,
          )
        );
        ?>
    </div>
</div>
