<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_support.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php
$category_values = array(
  1 => Yii::t('main', 'Общие вопросы'),
  2 => Yii::t('main', 'Вопросы по моему заказу'),
  3 => Yii::t('main', 'Рекламация'),
  4 => Yii::t('main', 'Возврат денег'),
  5 => Yii::t('main', 'Оптовые заказы'),
);
return array(

  'elements' => array(
    'theme'    => array(
      'type'      => 'text',
      'maxlength' => 256,
      'style'     => 'width:350px;'
    ),
    'text'     => array(
      'type'  => 'textarea',
      'style' => 'width:350px;'
    ),
    'category' => array(
      'type'  => 'dropdownlist',
      'items' => $category_values,
      'style' => 'width:150px;'
    ),
    'order_id' => array(
      'type'      => 'text',
      'maxlength' => 500,
      'style'     => 'width:350px;'
    ),
    'file'     => array(
      'type'      => 'file',
      'id'        => 'file_support',
      'maxlength' => 256,
      'style'     => 'width:350px;',
      'hint'      => '<p>' . Yii::t('main', 'Размер файла не должен превышать 1 Мб.') . '</p>',
    ),
  ),
  'buttons'  => array(
    'submit' => array(
      'type'  => 'submit',
      'label' => Yii::t('main', 'Отправить вопрос'),
      'class' => 'blue-btn bigger',
      'style' => 'float: left;margin: 25px 0 25px 30%;'
    ),
    'button' => array(
      'type'  => 'button',
      'id'    => 'remove_file_support',
      'label' => Yii::t('main', 'Удалить файл'),
      'style' => 'float:right;margin: 25px 30% 25px 0px;',
      'class' => 'blue-btn bigger'
    ),

  ),
);