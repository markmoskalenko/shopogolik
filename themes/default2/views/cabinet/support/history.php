<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="history.php">
 * </description>
 **********************************************************************************************************************/
?>
<div class="blue-tabs">
    <a href="<?= Yii::app()->createUrl('/cabinet/support') ?>">
        <span><?= Yii::t('main', 'Служба поддержки') ?></span>
    </a>

    <div class="active-tab">
        <span><?= $this->pageTitle ?></span>
    </div>

</div>
<div class="cabinet-content">
    <div class="cabinet-table">
        <? $this->widget(
          'zii.widgets.grid.CGridView',
          array(
            'id'           => 'question-grid',
            'dataProvider' => $model->search(),
            'filter'       => $model,
            'pager'        => array(
              'header'         => '',
              'firstPageLabel' => '&lt;&lt;',
              'prevPageLabel'  => '&lt;',
              'nextPageLabel'  => '&gt;',
              'lastPageLabel'  => '&gt;&gt;',
            ),
            'template'     => '{summary}{items}{pager}',
            'summaryText'  => Yii::t('main', 'Обращения') . ' {start}-{end} ' . Yii::t('main', 'из') . ' {count}',
            'columns'      => array(
              array(
                'name'  => 'id',
                'type'  => 'raw',
                'value' => 'CHtml::link("Q0000".$data->id, array("/cabinet/support/view", "id"=>$data->id))',
              ),
              array(
                'name'   => 'category',
                'filter' => array(
                  1 => Yii::t('main', 'Общие вопросы'),
                  2 => Yii::t('main', 'Вопросы по моему заказу'),
                  3 => Yii::t('main', 'Рекламация'),
                  4 => Yii::t('main', 'Возврат денег'),
                  5 => Yii::t('main', 'Оптовые заказы'),
                ),
                'value'  => '$data->text_category',
              ),
              'theme',
              array(
                'name'  => 'date',
                'type'  => 'raw',
                'value' => 'date("d.m.Y H:i",$data->date)'
              ),
              array(
                'name'  => 'date_change',
                'type'  => 'raw',
                'value' => '$data->date_change!=null ? date("d.m.Y H:i",$data->date_change) : date("d.m.Y H:i",$data->date)',
              ),
              array(
                'name'   => 'status',
                'filter' => array(
                  1 => Yii::t('main', 'На рассмотрении'),
                  2 => Yii::t('main', 'Получен ответ'),
                  3 => Yii::t('main', 'Закрыто'),
                ),
                'value'  => '$data->text_status',
              ),
            ),
          )
        );
        ?>
    </div>
</div>
