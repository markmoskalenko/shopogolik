<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="payment.php">
 * </description>
 * Рендеринг страницы оплаты заказа при оформлении
 **********************************************************************************************************************/
?>
<div class="page-title"><h4><?= Yii::t('main', 'Оформление заказа') ?></h4></div>
<table class="cart-header">
    <tbody>
    <th style="width: 155px;"><?= Yii::t('main', 'Товар') ?></th>
    <th style="width: 380px;"><?= Yii::t('main', 'Описание') ?></th>
    <th><?= Yii::t('main', 'Параметры') ?></th>
    <th><?= Yii::t('main', 'Цена без скидки') ?></th>
    <th style="margin-right: 25px;"><?= Yii::t('main', 'Сумма') ?></th>
    </tbody>
</table>
<div class="content payment">
    <?php foreach ($cart->cartRecords as $k => $item) { ?>
        <? $this->widget(
          'application.components.widgets.OrderItem',
          array(
            'orderItem'   => $item,
            'readOnly'    => true,
            'allowDelete' => false,
            'imageFormat' => '_200x200.jpg',
          )
        );
        ?>
    <? } ?>
    <div class="clear"></div>
    <h4><?= Yii::t('main', 'Доставка') ?></h4>
    <hr/>
    <table class="delivery-desc">
        <tr>
            <th><?= Yii::t('main', 'Способ доставки') ?>:</th>
            <td>
                <?php $delivery = Deliveries::getDelivery(0, false, $data['delivery_id']);
                if (isset($delivery->name)) {
                    echo $delivery->name;
                } else {
                    echo Yii::t('main', ' Не определено');
                }?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('main', 'Адрес') ?>:</th>
            <td>
                <?= $data['country_name'] ?>, <?= $data['index'] ?>, <?= $data['city'] ?>, <?= $data['address'] ?>,
                <?= isset($data['firstname']) ? $data['firstname'] : "" ?> <?= isset($data['lastname']) ? $data['lastname'] : "" ?>
                ,
                <?= isset($data['phone']) ? $data['phone'] : "" ?>
            </td>
        </tr>
    </table>
    <form action="<?= Yii::app()->createUrl('/checkout/payment') ?>" method="post">
        <input type="hidden" name="order_id" value="<?= $order_id ?>"/>
        <h4><?= Yii::t('main', 'Оплата') ?></h4>
        <hr>
        <table class="delivery-desc">
            <tr>
                <th><?= Yii::t('main', 'Сумма за товары') ?>:</th>
                <td class="select"><?= Formulas::priceWrapper($cart->total) ?></td>
            </tr>
            <tr>
                <th><?= Yii::t('main', 'Сумма за доставку') ?>:</th>
                <td
                  class="select"><?=
                    Formulas::priceWrapper(
                      Formulas::convertCurrency(
                        $data['delivery'],
                        DSConfig::getVal('site_currency'),
                        DSConfig::getCurrency()
                      )
                    ) ?></td>
            </tr>
            <tr class="total">
                <th><?= Yii::t('main', 'Итого к  оплате') ?>:</th>
                <td
                  class="select"><?=
                    Formulas::priceWrapper(
                      (float) $cart->total + (float) Formulas::convertCurrency(
                        $data['delivery'],
                        DSConfig::getVal('site_currency'),
                        DSConfig::getCurrency()
                      )
                    ) ?></td>
            </tr>
        </table>
        <?php if (Users::getBalance(Yii::app()->user->id) <
          Formulas::convertCurrency(
            $cart->total,
            DSConfig::getCurrency(),
            DSConfig::getVal('site_currency')
          ) + $data['delivery']
        ) {
            ?>
            <div class="payment-error">
                <p><?= Yii::t('main', 'На вашем счету недостаточно средств для оплаты заказа') ?>.</p>
                <input value='<?= Yii::t('main', 'Пополнить счёт') ?>'
                       onclick="location.href='<?=
                       Yii::app()->createUrl(
                         '/cabinet/balance/payment',
                         array(
                           'sum' => Formulas::convertCurrency(
                               $cart->total,
                               DSConfig::getCurrency(),
                               DSConfig::getVal('site_currency')
                             ) +
                             $data['delivery'] - Users::getBalance(Yii::app()->user->id),
                           'r'   => '/checkout/payment'
                         )
                       )?>'"
                       class="buy-btn bigger" type='button'/>
            </div>
            <!--     <input type="submit" name="doGoN" value="<? //=Yii::t('main','Оплатить')?>" class="blue-btn bigger" />  -->
        <?php
        } else {
            ?>
            <div class="next-btn">
                <input type="submit" id="payment_button" name="doGo" value="<?= Yii::t('main', 'Оплатить') ?>"
                       class="blue-btn bigger"/>
            </div>
        <?php } ?>
        <? $checkout_payment_needed = DSConfig::getVal('checkout_payment_needed') == 1;
        if (!$checkout_payment_needed) {
            ?>
            <div class="next-btn">
                <input type="submit" id="nopayment_button" name="doGoNoPayment"
                       value="<?= Yii::t('main', 'Оплатить позже') ?>"
                       class="blue-btn bigger"/>
            </div>
        <? } ?>
    </form>
</div>