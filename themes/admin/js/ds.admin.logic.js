$(document).ready(function () {
    setTimeout(checkOrder, 600000);
});

function checkOrder() {
    /*    var uid = $("#manager_id").val();
     $.get('/admin/main/checkOrder/uid/' + uid, function (data) {
     if (data) {
     var html = '<div id="order-notice">' +
     '<a href="/admin/orders/view" id="notice' + data + '" onclick="openOrder(' + data + ', ' + uid + ');return false;">У вас новый заказ!</a></div>';
     $("body").append(html);
     }
     else {
     setTimeout(checkOrder, 300000);
     }

     });
     */
}

function refund(pid) {
    if (confirm('Вы уверены что хотите отменить данный товар?')) {
        var text = $("#comment_" + pid).val();
        var url = '/admin/orders/refund/pid/' + pid + '/msg/' + text;

        $.get(url, function (data) {
            alert(data);
            reloadSelectedTab();
        });
    }
}

function deleteFeaturedItem(dellink, id) {
    var url = $(dellink).attr('href');
    var elem = $("#item" + id);
    $.get(url, null,
        function (data) {
            $(elem).remove();
            alert(data);
        },
        "text");
}

function loadCatalogFromTaobao() {
    if (confirm("Все старые категории будут удалены. Продолжить? Обязательно дождитесь сообщения о выполнении операции (30-100 сек)")) {
        //var form = $('#form-menu');
        var postdata = 'loadFromTaobao=1';// form.serialize() + '&loadFromTaobao=1';
        var url = '/admin/menu/loadFromTaobao' //form.attr('action');
        $.post(url, postdata, function (data) {
            alert(data);
            updateMetaAndHFURL();
        }, "text");
    }
}

var res = false;

function updateMetaAndHFURLStep(url, offset, count, post, internal) {
    if (internal) {
        $('#jsupdate-res').html('Internal categories updated: ' + offset + ' (' + Math.round(100 * offset / 15000) + '%)');
    } else {
        $('#jsupdate-res').html('Virtual categories updated: ' + offset + ' (' + Math.round(100 * offset / 1500) + '%)');
    }
    var postdata = post + 'updateHFURL=1&offset=' + offset + '&count=' + count + '&internal=' + internal;
    $.post(url, postdata, function (data) {
        res = data;
        offset = offset + count;
        if ((res != 'DONE') && (offset < 20000)) {
            setTimeout(function () {
                updateMetaAndHFURLStep(url, offset, count, post, internal)
            }, 1000);
        } else {
            if (internal) {
                $('#jsupdate-res').html('Internal categories update: COMPLETE!');
                alert('All categories update: COMPLETE!');
            } else {
                $('#jsupdate-res').html('Virtual categories update: COMPLETE!');
            }
            if (!internal) {
                if (confirm("Обновить также URL и meta для классификатора товаров taobao?")) {
                    res = false;
                    setTimeout(function () {
                        updateMetaAndHFURLStep(url, 0, count, post, true)
                    }, 1000);
                } else {
                    $('#jsupdate-res').html('Virtual categories update: COMPLETE!');
                    alert('All categories update: COMPLETE!');
                }
            }
        }
    }, "text");
}

function updateMetaAndHFURL() {
    if (confirm("Все старые значения meta и HFURL будут удалены. Продолжить?")) {
        var post = 'updateHFURL=1';
        var url = '/admin/menu/updateHFURL';
        var offset = 0;
        var count = 100;
        res = false;
        setTimeout(function () {
            updateMetaAndHFURLStep(url, offset, count, post, false);
        }, 1000);
    }
}

function updateBrandMeta() {
    if (confirm("Все старые значения meta будут удалены. Продолжить?")) {
        var url = '/admin/brands/updateMeta';
        $.get(url, function (data) {
            if (data=='OK') {
                alert('Brands meta update: COMPLETE!');
            } else {
                alert('Brands meta update: ERROR!');
            }
        }, "text");

    }
}

function saveArticle(id) {
    $('.tab.source').click();
    var postform = $('#article-form-' + id).serialize();
    var url = '/ru/admin/articles/update/id/' + id;// $('#page-form-' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
    }, "text");
}

function saveSid(sendButton) {
    var url = $(sendButton).parents('form').attr('action');
    var postform = $(sendButton).parents('form').serialize();
    var elem = $(sendButton).parents('.ui-tabs-panel');
    $.post(url, postform,
        function (data) {
            $(elem).html(data);
        },
        "html");
}

function seeOrderSum(id) {
    var form = $('#manager-order-sum-' + id);
    var postform = form.serialize() + '&uid=' + id;
    var date_from = !form.find('#date_from').val();
    var date_to = !form.find('#date_to').val();
    if (date_from || date_to) {
        alert("Введите даты");
    } else {
        var url = form.attr('action');
        $.post(url, postform, function (data) {
            $('#ordersSum' + id).html(data);
        }, "html");
    }
}
function saveParcel(pid) {
    var tid = $('div.tid_controll-form-' + pid + ' input').val();
    var out_sid = $('div.out_sid_controll-form-' + pid + ' input').val();
    //var out_sid = $('div.out_sid_controll-form-'+pid+' input').val();
//    if(!tid){
//        alert("введите код");
//        return;
//    }
//    alert (tid+' ! '+out_sid);
//    return;
    var url = '/admin/orders/ParcelConfirm/pid/' + pid + '/tid/' + tid + '/out_sid/' + out_sid;
    $.post(url, null, function (data) {
        alert(data);
    }, "text");
    reloadSelectedTab();
}
function saveParcelRealPrice(pid) {
    var val = 'realprice=' + $("div#parcel-order-price-" + pid + " input").val();
    var url = '/admin/orders/saveparcelrealprice/pid/' + pid;
    $.post(url, val, function (data) {
        alert(data);
    }, "text");
    reloadSelectedTab();
}

function deleteMainMenu(id) {
    if (confirm('Удалить категорию и все её дочерние категории?')) {
        var url = '/admin/menu/deleteex/?id=' + id;
        var adminTabs = $("#admin-content");
        var tbid = adminTabs.tabs('option', 'selected');
        $.get(url, function () {
            alert("Категория ID:" + id + " удалена");
        });
        adminTabs.tabs("remove", tbid);
        adminTabs.tabs("refresh");
    }
}

function createMainMenu(id) {
    if (confirm('Добавить новую дочернюю категорию в текущую?')) {
        var url = '/admin/menu/createex/?id=' + id;
        $.getJSON(url, function (data) {
            getContent('/ru/admin/menu/update/id/' + data.id, 'Категория №' + data.id);
            return false;
        });
    }
    return false;
}

function updateCrone(sendButton) {
    var url = $(sendButton).parents('form').attr('action');
    var postform = $(sendButton).parents('form').serialize();
    var elem = $(sendButton).parents('.ui-tabs-panel');
    $.get(url, postform,
        function (data) {
            $(elem).append(data);
        },
        "html");
}

// Used
function clearCache(sendButton) {
    var postform = null;
    var url = $(sendButton).attr('formaction');
//    if(typeof url == "undefined"){
// url = $(sendButton).parents('form').attr('action');
// postform = $(sendButton).parents('form').serialize();
// }
    $.get(url, postform,
        function (data) {
            reloadSelectedTab();
            alert(data);
        },
        "text");
}

// Used
function clearMenuCache() {
    var postform = null;
    var url = '/ru/admin/cache/clear/all/2';
//    if(typeof url == "undefined"){
// url = $(sendButton).parents('form').attr('action');
// postform = $(sendButton).parents('form').serialize();
// }
    $.get(url, postform,
        function (data) {
            reloadSelectedTab();
            alert(data);
        },
        "text");
}

//used
function UpdateParamFromProxy(id) {
    var url = '/admin/config/updateparamfromproxy/param/' + id;
    // alert(url);
    $.get(url, function (data) {
        alert(data);
        var active = $("#admin-content").tabs("option", "selected");
        $("#admin-content").tabs("load", active);
    }, "text");
}

//Used
function searchArticles(sendButton) {
    var url = $(sendButton).parents('form').attr('action');
    var postform = $(sendButton).parents('form').serialize();
    var elem = $(sendButton).parents('.ui-tabs-panel');
    $.get(url, postform,
        function (data) {
            $(elem).html(data);
        },
        "html");
}

//Used
function searchCategories(sendButton) {
    var url = $(sendButton).parents('form').attr('action');
    var postform = $(sendButton).parents('form').serialize();
    var elem = $(sendButton).parents('.ui-tabs-panel');
    $.get(url, postform,
        function (data) {
            $(elem).html(data);
        },
        "html");
}
//===========================
//Used
function saveCategory(id) {
    var postform = $('#category-form-' + id).serialize();
    var url = $('#category-form-' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
    }, "text");
}

//Used
function saveMainMenu(id) {
    var postform = $('#menu-form-' + id).serialize();
    var url = $('#menu-form-' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
    }, "text");
}

//Used
function saveOperator(id) {
    var postform = $('#operator-form-' + id).serialize();
    var url = $('#operator-form-' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
    }, "text");
}

//Used
function saveQuestionForm(id) {
    var postform = $('#message-answer-' + id).serialize();
    var url = $('#message-answer-' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
        $('#message-answer-' + id).parents(".view").removeClass("answer").find('input').attr('disabled', 'disabled');
    }, "text");
}

//used
function savePaySystem(id) {
    var postform = $('#pay-systems-form-' + id).serialize();
    var url = $('#pay-systems-form-' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
    }, "text");
}

//Used
function saveForm(id) {
    var postform = $('#' + id).serialize();
    var url = $('#' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
    }, "text");
}

function cmsHistoryRestore(id) {
    if (confirm("Восстановить контент из истории изменений?")) {
        //var form = $('#form-menu');
        var postdata = 'id=' + id;
        url = '/admin/cmsHistory/restore';
        $.post(url, postdata, function (data) {
            alert(data);
            reloadSelectedTab();
        }, "text");
    }
}

// Used
function saveConfig(id) {
    var postform = $('#config-form-' + id).serialize();
    var url = $('#config-form-' + id).attr('action');
    $.post(url, postform, function (data) {
        alert(data);
    }, "text");
}
//======================================================
//Used
function showControll(name, id) {
    $(".controll-form-" + id).slideUp('slow');
    $("#" + name + "-form-" + id).slideDown('slow');
}

function menuStorageCommand(command, name) {
    if (command == 'save') {
        message = 'Вы уверены, что хотите сохранить категории под именем "' + name + '"?';
    } else if (command == 'delete') {
        message = 'Вы уверены, что хотите удалить категории под именем "' + name + '"?';
    } else if (command == 'restore') {
        message = 'Вы уверены, что хотите восстановить категории под именем "' + name + '"?';
    } else {
        alert('Ошибка обработки сохраненных каталогов!');
        return;
    }
    if (confirm(message)) {
        url = '/admin/menu/storage/command/' + command + '/name/' + name;
        $.get(url, function (data) {
            alert(data);
            reloadSelectedTab();
        });
    }
}