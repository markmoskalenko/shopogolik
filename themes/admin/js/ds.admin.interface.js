function dsAlert(text,title,uidlg) {
    if ((text!=undefined) && (text.length>0)) {
        if (!uidlg) {
        alert(text);
        } else {
            $('#ds-interface-alert').remove();
            $('<div id="ds-interface-alert" title="'+title+'">'+text+'</div>').dialog(
            {
                closeOnEscape: true,
                resizable: false,
                modal: true,
                    buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                    $('#ds-interface-alert').remove();
                }
            }
            }
            )
        }
    }
}
function dsConfirm(text,title,uidlg) {
    if ((text!=undefined) && (text.length>0)) {
        if (!uidlg) {
            return confirm(text);
        } else {
            $('#ds-interface-confirm').remove();
            $('<div id="ds-interface-confirm" title="'+title+'">'+text+'</div>').dialog(
                {
                    closeOnEscape: true,
                    resizable: false,
                    modal: true,
                    buttons: {
                        Ok: function() {
                            $( this ).dialog( "close" );
                            $('#ds-interface-confirm').remove();
                            return true;
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                            $('#ds-interface-confirm').remove();
                            return false;
                        }
                    }
                }
            )
    }
    }
}

$(document).ready(function () {
    setTimeout(updateAdminNews, 300000);

    $('.close').live("click", function () {
        $('.overlay').hide();
        $('.popup').hide();
    });
    $(".accordion").accordion({
        accordion: false,
        speed: 500,
        closedSign: '[+]',
        openedSign: '[-]',
        autoHeight: false,
        clearStyle: true,
        showCount: true,
        autoClose: false
    });
//== Tabs ==============================
    var tabs = $("#admin-content").tabs({
        spinner: 'Загрузка...',
        ajaxOptions: { async: true, cache: false },
        tabTemplate: '<li><a href="#{href}"><span>#{label}</span></a>' +
            '<a href="#" style="cursor: pointer; padding:0.5em 0.1em;"><span class="ui-icon ui-icon-refresh ui-refresh-tab"></span>' +
            '<a href="#" style="cursor: pointer; padding:0.5em 0.1em;"><span class="ui-icon ui-icon-close ui-closable-tab"></span></li>',
        add: function (event, ui) {
            $(this).tabs('select', ui.index);
        },
        beforeLoad: function (event, ui) {
            var notCachedURLs=[/admin\/search\/index/];
            if (ui.tab.data("loaded")) {
                var idx = $("li", $(this)).index(ui.tab);
                if (idx != $(this).tabs('option', 'selected')) {
                    var url=ui.ajaxSettings.url;
                    var index;
                    for (index = 0; index < notCachedURLs.length; index++) {
                        var match=url.match(notCachedURLs[index]);
                       if(match) {
                           event.preventDefault();
                           return;
                       }
                    }
                }
            }

            ui.jqXHR.success(function () {
                ui.tab.data("loaded", true);
            });
            ui.jqXHR.error(function (data) {
                dsAlert(data.responseText);
            });
        },
        activate: function (event, ui) {
            var url = $('#' + $(ui.newPanel.selector).attr('aria-labelledby')).attr('href');
            helpGoTo(url);
        }
    });
    $(".ui-closable-tab").live("click", function () {
        //  if (confirm('Закрыть вкладку?')) {
        var prnt = $(this).parent().parent();
        var index = $('li.ui-state-default').index(prnt);
        tabs.tabs('remove', index);
        //  }
    });
    $(".ui-refresh-tab").live("click", function () {
        reloadSelectedTab();
    });
//================
//    $("ui-layout-center").addEventListner("scroll", function(){
/*    $(".ui-layout-center").scroll(function(){
        var tabheader = $('#admin-content-tabs');
        var scrollHeight = $(".ui-layout-center").prop("scrollHeight");
        if (scrollHeight > 30) {
            // fix it to the top
            tabheader.css('position','fixed');
            tabheader.css('top','0px');
        }
        else {
            // position it below the header
            tabheader.css('position', 'absolute');
            tabheader.css('top', '30px');
        }
    });
*/
//================
});

var mainLayout = $('body').layout({
    applyDemoStyles: true,
    north__resizable: false,
    east__initClosed: true,
    east__size: '30%',
    west__size: '170'
});

function helpLayoutTogle() {
    mainLayout.toggle('east');
}

function helpGoTo(url, open) {
    var fullUrl = url;
    if (fullUrl.indexOf('http:') == -1) {
        fullUrl = 'http://wiki.dropshop.pro/' + url.replace(/^(?:[\/][a-z]{2}\/)|(?:^[\/])/i, '');
    }
    fullUrl = fullUrl.replace(/\/\d+$/i, '');
    $('#frame-help-url').val(fullUrl);
    var $iframe = $('#frame-help');
    if ($iframe.length) {
        $iframe.attr('src', fullUrl);
        if (open === true) {
            mainLayout.open('east');
        }
    }
}

function getContent(elem, name, cached) {
    var url = null;
    if (jQuery.type(elem) === "string") {
        url = elem;
    } else {
        url = $(elem).attr('href');
    }
    if (!checkTabExists(name)) {
        $("#admin-content").tabs("add", url, name);
//        var index=$("#admin-content").tabs("length");
        $("body").scrollTop();
//---------------
        var postdata = 'AdminTabsHistory[href]' + '=' + url + '&' + 'AdminTabsHistory[name]' + '=' + name;
        var logUrl = '/admin/adminTabsHistory/log';
        $.post(logUrl, postdata, function (data) {
            $.fn.yiiGridView.update('admin-tabs-history-grid');
        }, "text");

//---------------
    }
}

function checkTabExists(str) {
    var result = false;
    var p = $("#admin-content ul li a :contains(" + str + ")");
    if (p.length) {
        var id = p.parent().parent().index();
        $("#admin-content").tabs("select", id);
        result = true;
    }
    return result;
}
function reloadSelectedTab() {
    var adminTabs = $("#admin-content");
    var selected = adminTabs.tabs('option', 'selected');
    adminTabs.tabs('load', selected);
}

function reloadSelectedTabOnF5(e) {
    var result = true;
    if (e.which == 116) {
        e.preventDefault();
        reloadSelectedTab();
        result = false;
    }
    return result;
}

$(document).bind("keydown", reloadSelectedTabOnF5);

// === Translation ======================
function editTranslation(evt,obj,type,id) {
//$("#sort_by").change(function() {
//    TranslateForm_cn
    var baseUrl = $(obj).attr('url');
    var url = baseUrl+'?type=' + type + '&id=' + id;
    if (url[0]!='/') {
        url='http://'+url;
    }
    $("#TranslateForm_type").val(type);
    $("#TranslateForm_id").val(id);
    $("#TranslateForm_from").val($(obj).attr('from'));
    $("#TranslateForm_to").val($(obj).attr('to'));
    $("#TranslateForm_uid").val($(obj).attr('uid'));
    $("#TranslateForm_url").val(url);
    $.getJSON(url, function (data) {
        if((data !== undefined)&&(data.zh!='')) {
            $("#TranslateForm_zh").val(data.zh);
            $("#TranslateForm_en").val(data.en);
            $("#TranslateForm_ru").val(data.ru);
            var iframe = $('#translate-bkrs');
            if ( iframe.length ) {
                iframe.attr('src','http://bkrs.info/slovo.php?ch='+data.zh);
            }
            $("#translationDialog").dialog("open");
            stopPropagation(evt);
        } else {
            dsAlert('Перевод не найден!');
        }
    });
    return false;
}

function saveTranslation() {
    var spanBlock =$("#TranslateForm_type").val()+$("#TranslateForm_id").val();
    var lang=$("#TranslateForm_to").val();
    var t='';
    if (lang=='ru') {
        t=$("#TranslateForm_ru").val();
    } else if (lang=='en')  {
        t=$("#TranslateForm_en").val();
    } else {
        t=$("#TranslateForm_zh").val();
    }
    $('#'+spanBlock).text(t);
    var postform = $("#translation-form").serialize();
    var url = $("#TranslateForm_url").val();
    $.post(url, postform);
    $("#translationDialog").dialog("close");
    return false;
}

function stopPropagation(evt) {
    var ev = evt ? evt : window.event;
    if (ev.stopPropagation)    ev.stopPropagation();
    if (ev.cancelBubble != null) ev.cancelBubble = true;
}

function updateAdminNews() {
    //var exists = $('#grid-admin-news-desktop') != 'unsigned';
    var exists = $('#grid-admin-news-desktop') != undefined;
    if (exists) {
        $.fn.yiiGridView.update('grid-admin-news-desktop');
    }
    setTimeout(updateAdminNews, 300000);
}