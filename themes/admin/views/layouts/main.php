<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
  <? $theme = DSConfig::getVal('site_front_theme') ?>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="language" content="ru"/>
  <!-- bootstrap framework -->
  <?
  //Yii::app()->bootstrap->register();
  Yii::app()->bootstrap->registerCoreCss();
//  Yii::app()->bootstrap->registerYiiCss();
//  Yii::app()->bootstrap->registerResponsiveCss();
  Yii::app()->bootstrap->registerCoreScripts();
  ?>
  <link href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/images/favicon.ico" type="image/x-icon" rel="icon" />
  <link href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/images/favicon.ico" type="image/x-icon" rel="shortcut icon" />

  <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/css/jquery-ui-1.9.2.custom.css"/>
  <!-- Yii view styles -->
  <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/css/gridview/styles.css"/>
  <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/css/listview/styles.css"/>
  <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/css/detailview/styles.css"/>
  <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/css/treeview/jquery.treeview.css"/>
  <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/themes/admin/css/pager/pager.css"/>
  <!-- ================= -->

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/css/main.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/themes/admin/css/menu_blue.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/themes/admin/css/popup.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/themes/admin/css/layout-default-latest.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/themes/admin/css/font-awesome.css"/>
  <title><?php echo CHtml::encode('Admin | ' . $this->pageTitle . ''); ?></title>

  <script type="text/javascript">
    var baseUrl = "<?=Yii::app()->request->baseUrl?>";
  </script>
  <?// Yii::app()->clientScript->registerCoreScript('jquery');?>
  <? Yii::app()->clientScript->registerScript(YII_DEBUG ? 'jquery.js' : 'jquery.min.js', CClientScript::POS_HEAD); ?>
  <? //Yii::app()->clientScript->registerScriptFile('jquery.js',CClientScript::POS_HEAD);?>
  <? //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl .'/themes/admin/js/'.'jquery.js',CClientScript::POS_HEAD);?>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/js/ui/<?=YII_DEBUG ? 'jquery-ui.js' : 'minified/jquery-ui.min.js'?>"></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/js/jquery.cookie.js"></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/js/ui/minified/jquery.hoverIntent.minified.js"></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/js/ui/minified/jquery.dcjqaccordion.2.9.min.js"></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/js/ui/<?=YII_DEBUG ? 'ui.tabs.closable.js' : 'minified/ui.tabs.closable.min.js'?>"></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/admin/js/<?=YII_DEBUG ? 'jquery.layout-latest.js' : 'jquery.layout-latest.min.js'?>"></script>
</head>
<body>
<input type="hidden" name="manager_id" id="manager_id" value="<?= $this->manager ?>"/>

<!-- <div class="container" id="page"> -->
<!-- <div id="page-layout"> -->
<div class="ui-layout-north">
  <div id="header">
    <div class="exit-container">
      <a href="http://support.dropshop.pro"><?= Yii::t('admin', 'Техподдержка') ?></a>&nbsp;|
      <a href="#" onclick="helpLayoutTogle(); return false;"><?= Yii::t('admin', 'Документация') ?></a>&nbsp;|
      <a href="<?= Yii::app()->request->baseUrl ?>/user/logout"
         id="exit"><?= Yii::t('admin', 'Выход/смена пользователя'); ?></a>&nbsp;|
      <a href="/cabinet"><?= Yii::t('admin', 'Перейти в личный кабинет') ?></a>
    </div>
    <div class="lang">
        <?=Search::$version.' '.DSConfig::getVal('system_last_auto_update')?>&nbsp;
      <?= Yii::t('admin', 'Язык') ?>:
        <?$lang_array=explode(',',DSConfig::getVal('site_language_block'));
        foreach ($lang_array as $interfaceLang) { ?>
            <a href="/user/setlang/<?=$interfaceLang?>" class="<?=$interfaceLang?><?= (Yii::app()->language == $interfaceLang) ? ' active' : '' ?>"><span></span></a>
        <? } ?>
    </div>
  </div>
  <!-- header -->
  <?php $this->widget('zii.widgets.CBreadcrumbs', array(
        'links'     => $this->breadcrumbs,
                'homeLink'  => CHtml::link(Yii::t('admin','Главная'),array('/admin/main'))
    )); ?><!-- breadcrumbs -->
</div>
<div class="ui-layout-west">
<div id="logo" title="<?=Yii::t('admin','На главную страницу сайта');?>" style="display: block;">
  <a href="<?= $this->createUrl('/') ?>"><img src="/images/admin/logo.png" style="width:140px !important;">
    <? //php echo CHtml::encode(DSConfig::getVal('site_name')); ?></a>
</div>
<div class="admin-main-menu">
<div class="menu-container" style="height: auto !important;">
<ul class="accordion">
  <? if (Yii::app()->user->checkAccess('admin/search/index')) {?>
    <li class="menu-point"><?=AdminUtils::adminMenu('admin_search','/admin/search/index',Yii::t('admin', 'Поиск...'),Yii::t('admin', 'Общий поиск заказов, пользователей, лотов...'),'icon-search')?></li>
  <? } ?>
<? if (Yii::app()->user->checkAccess('admin/orders/dashboard')) {?>
  <li class="menu-point"><?=AdminUtils::adminMenu('orders_dashboard','/admin/orders/dashboard',Yii::t('admin', 'Заказы'),Yii::t('admin', 'Обзор заказов'),'icon-shopping-cart')?></li>
<? } ?>
<? if (Yii::app()->user->checkAccess('admin/ordersItems/dashboard')) {?>
        <li class="menu-point"><?=AdminUtils::adminMenu('ordersItems_dashboard','/admin/ordersItems/dashboard',Yii::t('admin', 'Лоты'),Yii::t('admin', 'Обзор лотов'),'icon-inbox')?></li>
<? } ?>
  <? if (Yii::app()->user->checkAccess('admin/warehouse/income')) {?>
  <li class="menu-point" id="warehouse" title="<?= Yii::t('admin', 'Приёмка, размещение товаров на складе') ?>">
    <a href="javascript:void(0);"><i class="icon-fixed-width icon-th-large"></i> <?= Yii::t('admin', 'Склад') ?></a>
    <ul class="accordion-warehouse">
      <? if (Yii::app()->user->checkAccess('admin/warehouse/income')) {?>
        <li><?=AdminUtils::adminMenu('warehouse-income','/admin/warehouse/income',Yii::t('admin', 'Приход'),Yii::t('admin', 'Приход товаров на склад'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/warehouse/expenditure')) {?>
        <li><?=AdminUtils::adminMenu('warehouse-expenditure','/admin/warehouse/expenditure',Yii::t('admin', 'Расход'),Yii::t('admin', 'Комплектация посылок и расход со склада'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/warehouse/unfound')) {?>
        <li><?=AdminUtils::adminMenu('warehouse-unfound','/admin/warehouse/unfound',Yii::t('admin', 'Без привязки'),Yii::t('admin', 'Товары, которые не привязаны к заказам по штрих-коду и требуют дополнительной идентификации'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/warehouse/map')) {?>
        <li><?=AdminUtils::adminMenu('warehouse-map','/admin/warehouse/map',Yii::t('admin', 'Схема'),Yii::t('admin', 'Схема расположения и загрузки склада'))?></li>
      <?}?>
    </ul>
  </li>
  <? } ?>
<? //=================================================================================================== ?>
<? if (Yii::app()->user->checkAccess('admin/questions/index')) {?>
  <li class="menu-point"><?=AdminUtils::adminMenu('questions','/admin/questions',Yii::t('admin', 'Вопросы'),Yii::t('admin', 'Вопросы клиентов'),'icon-question-sign')?></li>
<?}?>
<? if (Yii::app()->user->inRole(array('superAdmin', 'topManager'))) { ?>
  <li class="menu-point"><?=AdminUtils::adminMenu('operators','/admin/operators/list',Yii::t('admin', 'Менеджеры'),Yii::t('admin', 'Просмотр распределения заказов по менеджерам'),'icon-user-md')?></li>
<? } ?>
<? if (Yii::app()->user->checkAccess('admin/users/index')) {?>
  <li class="menu-point"><?=AdminUtils::adminMenu('users','/admin/users',Yii::t('admin', 'Пользователи'),Yii::t('admin', 'Управление пользователями, история операций, счета'),'icon-user')?></li>
<? } ?>
<? if (Yii::app()->user->checkAccess('admin/payments/index')) {?>
  <li class="menu-point"><?=AdminUtils::adminMenu('userPayments','/admin/payments',Yii::t('admin', 'Платежи'),Yii::t('admin', 'Просмотр платежей пользователей'),'icon-credit-card')?></li>
<?}?>
    <? if (Yii::app()->user->checkAccess('admin/message/sendMail')) {?>
        <li class="menu-point"><a href="#" onclick="$('#new-intermal-email-to-all').dialog('open');return false;"
            title="<?=Yii::t('admin','Отправка почтового сообщения всем пользователям')?>">
            <i class="icon-fixed-width icon-envelope-alt"></i><?=Yii::t('admin', 'Рассылка')?>
            </a>
        </li>
    <?}?>
  <li class="menu-point" id="contents" title="<?= Yii::t('admin', 'Настройки категорий, баннеров, текстов...') ?>">
      <a href="javascript:void(0);"><i class="icon-fixed-width icon-list"></i><?= Yii::t('admin', 'Контент') ?></a>
    <ul class="accordion-orders">
      <? if (Yii::app()->user->checkAccess('admin/category/index')) {?>
        <li><?=AdminUtils::adminMenu('classifier','/admin/category',Yii::t('admin', 'Классификатор'),Yii::t('admin', 'Управление классификатором товаров taobao - не отображается на сайте'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/menu/index')) {?>
        <li><?=AdminUtils::adminMenu('menu-control','/admin/menu',Yii::t('admin', 'Категории'),Yii::t('admin', 'Управление виртуальными категориями, которые отображаются на сайте в каталоге'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/brands/index')) {?>
        <li><?=AdminUtils::adminMenu('brands','/admin/brands',Yii::t('admin', 'Бренды'),Yii::t('admin', 'Управление брендами, которые отображаются на сайте'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/featured/index')) {?>
        <li><?=AdminUtils::adminMenu('recomendations','/admin/featured',Yii::t('admin', 'Рекомендации'),Yii::t('admin', 'Управление рекомендованными товарами, отображаемыми на главной странице сайта'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/cmsMenus/index')) {?>
        <li><?=AdminUtils::adminMenu('static-pages-menus','/admin/cmsMenus',Yii::t('admin', 'CMS: меню'),Yii::t('admin', 'Управление меню фронта сайта'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/cmsPages/index')) {?>
        <li><?=AdminUtils::adminMenu('static-pages-pages','/admin/cmsPages',Yii::t('admin', 'CMS: страницы'),Yii::t('admin', 'Управление страницами фронта сайта'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/cmsPagesContent/index')) {?>
        <li><?=AdminUtils::adminMenu('static-pages-pages-content','/admin/cmsPagesContent',Yii::t('admin', 'CMS: контент страниц'),Yii::t('admin', 'Управление контентом страниц фронта сайта'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/cmsCustomContent/index')) {?>
        <li><?=AdminUtils::adminMenu('static-pages-custom-content','/admin/cmsCustomContent',Yii::t('admin', 'CMS: контент блоков'),Yii::t('admin', 'Управление контентом блоков фронта сайта'))?></li>
      <?}?>
        <? if (Yii::app()->user->checkAccess('admin/banrules/index')) {?>
            <li><?=AdminUtils::adminMenu('banrules','/admin/banrules',Yii::t('admin', 'CMS: фильтр URL'),Yii::t('admin', 'Правила обработки перенаправления URL'))?></li>
        <?}?>
        <? if (Yii::app()->user->checkAccess('admin/cmsEmailEvents/index')) {?>
            <li><?=AdminUtils::adminMenu('cms-email-contents','/admin/cmsEmailEvents',Yii::t('admin', 'CMS: EMail'),Yii::t('admin', 'Правила обработки почтовых событий и рассылок'))?></li>
        <?}?>
      <? if (Yii::app()->user->checkAccess('admin/banners/index')) {?>
        <li><?=AdminUtils::adminMenu('appearance','/admin/banners',Yii::t('admin', 'Баннеры'),Yii::t('admin', 'Управление баннерами на главной странице сайта'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/adminNews/index')) {?>
        <li><?=AdminUtils::adminMenu('adminNews','/admin/adminNews',Yii::t('admin', 'Внутренние новости'),Yii::t('admin', 'Управление внутренними новостями и объявлениями'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/config/extsettings')) {?>
        <li><?=AdminUtils::adminMenu('extservices','/admin/config/extsettings',Yii::t('admin', 'Внешние сервисы'),Yii::t('admin', 'Блоки внешних сервисов - ВКонтакте, Одноклассники и т.п.'))?></li>
      <? } ?>
    </ul>
  </li>
  <li class="menu-point" id="settings" title="<?= Yii::t('admin', 'Настройки тарифов, скидок, наценок...') ?>">
    <a href="javascript:void(0);"><i class="icon-fixed-width icon-wrench"></i><?= Yii::t('admin', 'Настройки') ?></a>
    <ul class="accordion-orders">
      <? if (Yii::app()->user->checkAccess('admin/config/prices')) {?>
        <li><?=AdminUtils::adminMenu('prices','/admin/config/prices',Yii::t('admin', 'Ценообразование'),Yii::t('admin', 'Тарифы, наценки, скидки'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/deliveries')) {?>
        <li><?=AdminUtils::adminMenu('deliveries','/admin/deliveries',Yii::t('admin', 'Доставка'),Yii::t('admin', 'Настройки служб доставки и расценок на доставку'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/paysystems/index')) {?>
        <li><?=AdminUtils::adminMenu('paySystems','/admin/paysystems',Yii::t('admin', 'Платёжные системы'),Yii::t('admin', 'Настройки параметров платёжных систем'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/ordersStatuses/index')) {?>
        <li><?=AdminUtils::adminMenu('ordersStatuses','/admin/ordersStatuses/index',Yii::t('admin', 'Статусы заказов'),Yii::t('admin', 'Настройки статусов заказов и их поведений'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/formulas/index')) {?>
        <li><?=AdminUtils::adminMenu('formulas','/admin/formulas/index',Yii::t('admin', 'Формулы'),Yii::t('admin', 'Формулы ценообразования, расчета прибыли, бонусов и т.п.'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/events/index')) {?>
        <li><?=AdminUtils::adminMenu('events','/admin/events/index',Yii::t('admin', 'Обработка событий'),Yii::t('admin', 'Настройки обработки событий'))?></li>
      <? } ?>
      <? if (Yii::app()->user->checkAccess('admin/reportsSystem/index')) {?>
        <li><?=AdminUtils::adminMenu('mail-events','/admin/reportsSystem',Yii::t('admin', 'Настройка отчётов'),Yii::t('admin', 'Управление отчётами и аналитикой'))?></li>
      <? } ?>
    </ul>
  </li>
  <li class="menu-point" id="site-settings" title="<?= Yii::t('admin', 'Настройки сайта') ?>">
      <a href="javascript:void(0);"><i class="icon-fixed-width icon-gears"></i><?= Yii::t('admin', 'Система') ?></a>
    <ul class="accordion-orders">
      <? if (Yii::app()->user->checkAccess('admin/cache/index')) {?>
        <li><?=AdminUtils::adminMenu('cache-control','/admin/cache',Yii::t('admin', 'Управление кешем'),Yii::t('admin', 'Контроль и очистка внутреннего кэша'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/config/index')) {?>
        <li><?=AdminUtils::adminMenu('parameterAll','/admin/config/index',Yii::t('admin', 'Параметры'),Yii::t('admin', 'Настройки технических параметров системы'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/accessrights/index')) {?>
        <li><?=AdminUtils::adminMenu('accessrights','/admin/accessrights/index',Yii::t('admin', 'Права доступа'),Yii::t('admin', 'Управление правами доступа'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/translatorkeys/index')) {?>
        <li><?=AdminUtils::adminMenu('parameterBing','/admin/translatorkeys',Yii::t('admin', 'Менеджер ключей bing'),Yii::t('admin', 'Настройка ключей bing'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/config/catparser')) {?>
        <li><?=AdminUtils::adminMenu('catparser','/admin/config/catparser',Yii::t('admin', 'Парсер категорий'),Yii::t('admin', 'Обслуживание парсера категорий taobao'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/config/searchparser')) {?>
        <li><?=AdminUtils::adminMenu('searchparser','/admin/config/searchparser',Yii::t('admin', 'DropShop grabber').'©',Yii::t('admin', 'Обслуживание DropShop grabber'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/utilites')) {?>
        <li><?=AdminUtils::adminMenu('service-utils','/admin/utilites/index',Yii::t('admin', 'Сброс'),Yii::t('admin', 'Сброс параметров системы, сервис при изменении версий'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/update')) {?>
        <li><?=AdminUtils::adminMenu('service-update','/admin/update/index',Yii::t('admin', 'Обновление'),Yii::t('admin', 'Установка обновлений'))?></li>
      <?}?>
      <? if (Yii::app()->user->checkAccess('admin/errorlog/index')) {?>
        <li><?=AdminUtils::adminMenu('errorlog','/admin/errorlog',Yii::t('admin', 'Лог ошибок'),Yii::t('admin', 'Лог обработанных ошибок сайта'))?></li>
      <? } ?>
        <? if (Yii::app()->user->checkAccess('admin/fileman/view')) {?>
            <li><?=AdminUtils::adminMenu('fileman','/admin/fileman/view',Yii::t('admin', 'Менеджер файлов'),Yii::t('admin', 'Управление файлами изображений иисходного кода'))?></li>
        <? } ?>
      <? if (Yii::app()->user->checkAccess('dbadmin')) {?>
        <!-- target="_blank" -->
      <li id="db-control" title="<?= Yii::t('admin', 'Внешний менеджер управления СУБД') ?>"><a
          href="#" onclick="window.open('/dbadmin/login.php?USER=<?=Yii::app()->db->username?>&PASS=<?=Yii::app()->db->password?>&DATABASE=<?=preg_replace('/^.*dbname=(.*?)(?:;.*|$)/is','\1',Yii::app()->db->connectionString)?>',
            '<?= Yii::t('admin', 'Управление СУБД') ?>');"  ><?= Yii::t('admin', 'Управление СУБД') ?></a>
      </li>
      <? } ?>
    </ul>
  </li>
<? if (Yii::app()->user->checkAccess('admin/sitestat/index')) {?>
  <li class="menu-point"><?=AdminUtils::adminMenu('site-stat','/admin/sitestat',Yii::t('admin', 'Статистика'),Yii::t('admin', 'Статистика работы сайта'),'icon-bar-chart')?></a></li>
<? } ?>
</ul>
</div>
</div>
  <div class="admin-history">
    <?php $this->widget('bootstrap.widgets.TbGridView',array(
      'id'=>'admin-tabs-history-grid',
      'dataProvider'=>AdminTabsHistory::getHistory(),
      'type'=>'striped bordered condensed',
      'template'=>'{items}{pager}',//'{summary}{pager}{items}{pager}',
      'columns'=>array(
//        'id',
//        'href',
        array('name'=>'name',
          'type' => 'raw',
          'value' => 'CHtml::link($data->name, array($data->href), array("title"=>$data->title,"onclick"=>"getContent(this,\"$data->name\");return false;"))',
        ),
//        'title',
//        'uid',
//        'date',
      ),
    ));
    ?>
  </div>
</div>
<!-- <div class="container"> -->
  <div id="admin-content" class="ui-layout-center">
      <?php $this->widget('application.components.widgets.MessagesBlock') ?>
    <ul id="admin-content-tabs">
      <li><a href="/admin/main/dashboard"><span><?= Yii::t('admin', 'Рабочий стол') ?></span></a>
        <a style="cursor: pointer; padding:0.5em 0.1em;" href="javascript:void(0);" onclick="reloadSelectedTab();return true;">
          <span class="ui-icon ui-icon-refresh"></span>
        </a>
      </li>
    </ul>
      <div class="ui-layout-content">
    <div id="ui-tabs-1">
      <p><? //= $content ?></p>
    </div>
     </div>
  </div>
  <!-- content -->
<div class="ui-layout-south" id="footer"></div>
<div class="ui-layout-east" id="help">
    <div style="height: 100%; vertical-align: top;">
        <input type="text" id="frame-help-url" style="width: 80%" value="http://wiki.dropshop.pro/start?do=index"/>
      <a href="#" onclick="helpGoTo($('#frame-help-url').val())"><?=Yii::t('admin','Перейти')?></a>
      <a id="frame-help-href" href="#" onclick="window.open($('#frame-help-url').val()); return false;"><?=Yii::t('admin','Окно')?></a>
            <iframe name="frame-help" id="frame-help" width="100%" height="100%"
                src="http://wiki.dropshop.pro/start?do=index" seamless
                frameborder="1">Your browser doesn't support iframes.
            </iframe>
    </div>
</div>

<!-- begin of translation dialog -->
<div id="translationDialog" style="display: none;">
  <? if ((Yii::app()->user->inRole(array('superAdmin', 'manager', 'admin', 'translator')))) { ?>
    <?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
      'id' => 'translationDialog',
      'cssFile' => false,
      'scriptFile' => false,
      'options' => array(
      'title' => Yii::t('main', 'Редактирование перевода'),
      'autoOpen' => false,
      'modal' => true,
      'resizable' => false,
      'position' => '[725,100]',
      'style' => '[725,100]',
      'width'    => '70%',
      'height'   => 'auto',
      ),
    ));
    echo $this->renderPartial('/main/translate', array());
    $this->endWidget('zii.widgets.jui.CJuiDialog');
    ?>
  <? } ?>
</div>
<!-- END of translation dialog -->
<? if (Yii::app()->user->checkAccess('admin/message/sendMail')) {?>
<?//- Internal email ----------------------------------------------?>
<div id="new-intermal-email-to-all" title="<?=Yii::t('main','EMail всем пользователям')?>" style="display: none;">
    <form id="new-intermal-email-form-to-all">
        <label><b><?=Yii::t('Admin','Текст сообщения')?>:</b></label><br/>
        <input type="hidden" name="message[uid]" value="<?='all'?>" />
        <textarea cols="120" rows="6" name="message[message]" id="message-all"></textarea>
    </form><br/>
    <p><?=Yii::t('admin','Не используйте длинных сообщений! Изучите и настройте шаблон рассылки для события Mail.sendMailToAll')?>
    <br/>
    <?=Yii::t('admin','Рекомендуем для рассылок использовать спец. инструменты, получив список адресов в разделе "Пользователи".')?>
    </p>
    <br/>
    <button id="change" onclick="
      $('#new-intermal-email-to-all').dialog('close');
      var msg = $('#new-intermal-email-form-to-all').serialize();
      $.post('/admin/message/sendMail',msg, function(){
      $('#message-all').val('');
      },'text');
      return false;"><?=Yii::t('admin','Отправить')?></button>
    &nbsp;&nbsp;
    <button id="cancel" onclick="$('#new-intermal-email-to-all').dialog('close');
      $('#message-all').val('');
      return false;"><?=Yii::t('admin','Отмена')?></button>
    <br><br>
</div>
<script type="text/javascript">
    $("#new-intermal-email-to-all").dialog({
        autoOpen: false,
        width: "auto",
        modal: true,
        resizable: false
    });
</script>
<? } ?>
<!-- ======================================================================= -->
</body>
</html>
<script>
  $(function() {
  if (jQuery.support.leadingWhitespace == false){
        alert('<?=Yii::t('main','Ваш браузер не поддерживает ряд необходимых для нормальной работы функций. Обновите его до последней версии.')?>');
      }
  });
</script>