/**
 * Created by Vitaliy on 24.07.14.
 */
$(function() {
    $("img.lazy").show().lazyload({
        effect       : "fadeIn",
        effect_speed : 500
//      skip_invisible : false
    });
});
jQuery(function($) {

    if($('#rev_slider_2_1').revolution == undefined)
        revslider_showDoubleJqueryError('#rev_slider_2_1');
    else
        revapi2 = $('#rev_slider_2_1').show().revolution(
            {
                dottedOverlay:"none",
                delay:9000,
                startwidth:870,
                startheight:400,
                hideThumbs:200,

                thumbWidth:100,
                thumbHeight:50,
                thumbAmount:3,

                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"round",

                touchenabled:"on",
                onHoverStop:"on",

                navigationHAlign:"center",
                navigationVAlign:"bottom",
                navigationHOffset:0,
                navigationVOffset:20,

                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,

                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,

                shadow:0,
                fullWidth:"on",
                fullScreen:"off",

                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,


                shuffle:"off",

                autoHeight:"off",

                forceFullWidth:"off",





                hideThumbsOnMobile:"off",
                hideBulletsOnMobile:"off",
                hideArrowsOnMobile:"off",
                hideThumbsUnderResolution:0,

                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0,
                videoJsPath:"/modules/revsliderprestashop/rs-plugin/videojs/",
                fullScreenOffsetContainer: ""
            });
});



    $('.pos-logo .bxslider').bxSlider({
    auto: 1,
    slideWidth:170,
    autoHover: true,
    slideMargin: 25,
    minSlides: 1,
    maxSlides: 6,
    speed:  2000,
    pause: 2000,
    controls: 1,
    autoDelay: 5,
    pager: false
});

$(document).ready(function () {
    $('.pos_animated').waypoint(function (event, direction) {
        $(this).find('.fx-flash').addClass('animated flash');
        $(this).find('.fx-bounce').addClass('animated bounce');
        $(this).find('.fx-shake').addClass('animated shake');
        $(this).find('.fx-tada').addClass('animated tada');
        $(this).find('.fx-swing').addClass('animated swing');
        $(this).find('.fx-wobble').addClass('animated wobble');
        $(this).find('.fx-wiggle').addClass('animated wiggle');
        $(this).find('.fx-pulse').addClass('animated pulse');
        $(this).find('.fx-fadeInDownBig').addClass('animated fadeInDownBig');
        $(this).find('.fx-fadeInUpBig').addClass('animated fadeInUpBig');
        $(this).find('.fx-fadeInLeftBig').addClass('animated fadeInLeftBig');
        $(this).find('.fx-fadeInRightBig').addClass('animated fadeInRightBig');
        $(this).find('.fx-fadeInDown').addClass('animated fadeInDown');
        $(this).find('.fx-fadeInUp').addClass('animated fadeInUp');
        $(this).find('.fx-fadeInLeft').addClass('animated fadeInLeft');
        $(this).find('.fx-fadeInRight').addClass('animated fadeInRight');
        $(this).find('.fx-fadeOutDownBig').addClass('animated fadeOutDownBig');
        $(this).find('.fx-fadeOutUpBig').addClass('animated fadeOutUpBig');
        $(this).find('.fx-fadeOutLeftBig').addClass('animated fadeOutLeftBig');
        $(this).find('.fx-fadeOutRightBig').addClass('animated fadeOutRightBig');
        $(this).find('.fx-fadeOutDown').addClass('animated fadeOutDown');
        $(this).find('.fx-fadeOutUp').addClass('animated fadeOutUp');
        $(this).find('.fx-fadeOutLeft').addClass('animated fadeOutLeft');
        $(this).find('.fx-fadeOutRight').addClass('animated fadeOutRight');
        $(this).find('.fx-bounceIn').addClass('animated bounceIn');
        $(this).find('.fx-bounceOut').addClass('animated bounceOut');
        $(this).find('.fx-bounceInUp').addClass('animated bounceInUp');
        $(this).find('.fx-bounceInDown').addClass('animated bounceInDown');
        $(this).find('.fx-bounceInLeft').addClass('animated bounceInLeft');
        $(this).find('.fx-bounceInRight').addClass('animated bounceInRight');
        $(this).find('.fx-bounceOutUp').addClass('animated bounceOutUp');
        $(this).find('.fx-bounceOutDown').addClass('animated bounceOutDown');
        $(this).find('.fx-bounceOutLeft').addClass('animated bounceOutLeft');
        $(this).find('.fx-bounceOutRight').addClass('animated bounceOutRight');
        $(this).find('.fx-rotateIn').addClass('animated rotateIn');
        $(this).find('.fx-rotateInDownLeft').addClass('animated rotateInDownLeft');
        $(this).find('.fx-rotateInDownRight').addClass('animated rotateInDownRight');
        $(this).find('.fx-rotateInUpLeft').addClass('animated rotateInUpLeft');
        $(this).find('.fx-rotateInUpRight').addClass('animated rotateInUpRight');
        $(this).find('.fx-rotateOut').addClass('animated rotateOut');
        $(this).find('.fx-rotateOutDownLeft').addClass('animated rotateOutDownLeft');
        $(this).find('.fx-rotateOutDownRight').addClass('animated rotateOutDownRight');
        $(this).find('.fx-rotateOutUpLeft').addClass('animated rotateOutUpLeft');
        $(this).find('.fx-rotateOutUpRight').addClass('animated rotateOutUpRight');
        $(this).find('.fx-lightSpeedIn').addClass('animated lightSpeedIn');
        $(this).find('.fx-lightSpeedOut').addClass('animated lightSpeedOut');
        $(this).find('.fx-hinge').addClass('animated hinge');
        $(this).find('.fx-rollOut').addClass('animated rollOut');
        $(this).find('.fx-rollIn').addClass('animated rollIn');
        $(this).find('.fx-rollOut').addClass('animated rollOut');
        $(this).find('.fx-slideInLeft').addClass('animated slideInLeft');
        $(this).find('.fx-slideInRight').addClass('animated slideInRight');
        $(this).find('.fx-slideInDown').addClass('animated slideInDown');
        $(this).find('.fx-slideOutLeft').addClass('animated slideOutLeft');
        $(this).find('.fx-slideOutRight').addClass('animated slideOutRight');
        $(this).find('.fx-slideOutUp').addClass('animated slideOutUp');
        $(this).find('.fx-flip').addClass('animated flip');
        $(this).find('.fx-flipInX').addClass('animated flipInX');
        $(this).find('.fx-flipInY').addClass('animated flipInY');
        $(this).find('.fx-flipOutX').addClass('animated flipOutX');
        $(this).find('.fx-flipOutY').addClass('animated flipOutY');
    }, { offset: 400 });
});
$(document).ready(function(){
    $("#pt_ver_menu_link ul li").each(function(){
        var url = window.location.pathname;
        $("#pt_ver_menu_link ul li a").removeClass("act");
        $('#pt_ver_menu_link ul li a[href="'+url+'"]').addClass('act');
    });
    $(".pt_menu").live({
        mouseenter: function () {
            $(this).find('.popup').show();
        },
        mouseleave: function () {
            $(this).find('.popup').hide();
        }
    });
});
