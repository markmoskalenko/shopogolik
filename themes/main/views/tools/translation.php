<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="translation.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title">

    <?=$this->pageTitle?>

</div>
<div class="content">
    <div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'translationForm',
        ));?>

    <div class="clear"></div>
    <div class="row">
      <textarea name="src">每一个猎人想知道在哪里坐野鸡</textarea>
    </div>
    <div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main','Сегментировать'),array('class'=>'blue-btn bigger')); ?>
    </div>

    <?php $this->endWidget(); ?>
    </div>
    <?php if($translation) { ?>
    <div class="clear"></div>
      <?=$translation?>
      <br/>
      <textarea name="src"><?=$translation?></textarea>
    <? } ?>
</div>