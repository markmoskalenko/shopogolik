<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="commentattachpreview.php">
* </description>
**********************************************************************************************************************/?>
<div class="blocknormal">
<img style="width:50px;height:auto;margin: 0;" src="<?=Yii::app()->createUrl('/img/commentattach',array('isItem'=>$isItem,'id'=>$attach->id))?>"/>
    <div class="blockhover">
        <img style="width:100px;height:auto;margin: 0;border:0;" src="<?=Yii::app()->createUrl('/img/commentattach',array('isItem'=>$isItem,'id'=>$attach->id))?>"/>
    </div>
</div>