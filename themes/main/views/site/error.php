<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="error.php">
* </description>
**********************************************************************************************************************/?>
<?  Yii::app()->clientScript->registerMetaTag('noindex', 'robots'); ?>
<div class="page-title">

    <?=Yii::t('main','Ошибка')?> <?php echo $error->code; ?>

</div>

<div class="error">
<?
  /*
  code - the HTTP status code (e.g. 403, 500)
  type - the error type (e.g. 'CHttpException', 'PHP Error')
  message - the error message
  file - the name of the PHP script file where the error occurs
  line - the line number of the code where the error occurs
  trace - the call stack of the error
  source - the context source code where the error occurs

  */
  //print_r($error);
  echo 'Error: '.$error['type'].': '.CHtml::encode($error['message']);
//  echo '<br/>';
//  echo $error['source'];
//  echo '<br/>';
//  echo $error['file'].': '.$error['line'];
//  echo '<br/>';
//  echo $error['trace'];
?>
</div>
<hr>
 <div>
   <?=cms::customContent('error-main-sorry-message')?>

 </div>
<div>
  <br/><a href="<?=$_SERVER['HTTP_REFERER']?>"><?=Yii::t('main','Назад \ Повторить')?></a><br/>
</div>
<? if ($this->beginCache('site\error',array('duration'=>(TRUE||YII_DEBUG||
    (!in_array(Yii::app()->user->getRole(),array('guest','user')))
  )?0:3600))) { ?>
<?
//========================================================
$featured_items = DSConfig::getVal('featured_items');
$featuredTypes = explode(',', $featured_items);
$itemsPopular = (in_array('popular', $featuredTypes));
$itemsRecommended = (in_array('recommended', $featuredTypes));
$itemsRecentUser = (in_array('recentUser', $featuredTypes));
$itemsRecentAll = (in_array('recentAll', $featuredTypes));
//========================================================
?>

<?if($itemsRecommended){?>
  <div class="page-title"><?=Yii::t('main','Рекомендованные товары')?>:</div>
  <div class="products-list featured">
    <? $seo_disable_items_index=DSConfig::getVal('seo_disable_items_index')==1;?>
    <? $this->widget('application.components.blocks.SearchItemsList',array(
      'id'=>'recommended-itemslist',
      'controlAddToFavorites' => true,
      'controlAddToFeatured' => false,
      'controlDeleteFromFavorites' => false,
      'lazyLoad' => true,
      'dataType'=>'itemsRecommended',
      'pageSize' =>8,
//      'showControl' => null,
      'disableItemForSeo' => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
    ));
    ?>
  </div>
<?}?>
  <!--  begin popular items -->
<?if($itemsPopular){?>
  <div class="page-title"><?=Yii::t('main','Популярные товары')?>:</div>
  <div class="products-list featured">
    <? $seo_disable_items_index=DSConfig::getVal('seo_disable_items_index')==1;?>
    <? $this->widget('application.components.blocks.SearchItemsList',array(
      'id'=>'popular-itemslist',
      'controlAddToFavorites' => true,
      'controlAddToFeatured' => true,
      'controlDeleteFromFavorites' => false,
      'lazyLoad' => true,
      'dataType'=>'itemsPopular',
      'pageSize' =>8,
//      'showControl' => null,
      'disableItemForSeo' => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
    ));
    ?>
  </div>
<?}?>
  <!--  begin my items -->
<? if ($itemsRecentUser) { ?>
  <div class="page-title"><?=Yii::t('main','Недавно просмотренные Вами')?>:</div>
  <hr/>
  <div class="products-list featured">
    <? $seo_disable_items_index=DSConfig::getVal('seo_disable_items_index')==1;?>
    <? $this->widget('application.components.blocks.SearchItemsList',array(
      'id'=>'recentUser-itemslist',
      'controlAddToFavorites' => true,
      'controlAddToFeatured' => true,
      'controlDeleteFromFavorites' => false,
      'lazyLoad' => true,
      'dataType'=>'itemsRecentUser',
      'pageSize' =>8,
//      'showControl' => null,
      'disableItemForSeo' => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
    ));
    ?>
  </div>
<? } ?>
  <!--  begin all user items -->
<? if ($itemsRecentAll) { ?>
  <div class="page-title"><?=Yii::t('main','Недавно просмотренные другими')?>:</div>
  <div class="products-list featured">
    <? $seo_disable_items_index=DSConfig::getVal('seo_disable_items_index')==1;?>
    <? $this->widget('application.components.blocks.SearchItemsList',array(
      'id'=>'recentAll-itemslist',
      'controlAddToFavorites' => true,
      'controlAddToFeatured' => true,
      'controlDeleteFromFavorites' => false,
      'lazyLoad' => true,
      'dataType'=>'itemsRecentAll',
      'pageSize' =>8,
//      'showControl' => null,
      'disableItemForSeo' => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
    ));
    ?>
  </div>
<? } ?>
<?  $this->endCache(); } ?>