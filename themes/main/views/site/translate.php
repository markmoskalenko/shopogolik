<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="translate.php">
* </description>
**********************************************************************************************************************/?>
<? $qForm = new TranslateForm;
$form = $this->beginWidget('CActiveForm', array(
  'id'                   => 'translation-form',
  'enableAjaxValidation' => FALSE,
));
?>
<? echo $form->hiddenField($qForm, 'id'); ?>
<? echo $form->hiddenField($qForm, 'type'); ?>
<? echo $form->hiddenField($qForm, 'from'); ?>
<? echo $form->hiddenField($qForm, 'to'); ?>
<? echo $form->hiddenField($qForm, 'uid'); ?>
<? echo $form->hiddenField($qForm, 'url'); ?>
  <div>
    <div class="row" style="float:left;width:33%;">
      <?php echo $form->labelEx($qForm, '<strong>' . Yii::t('main', 'Китайский') . '</strong>'); ?>
      <?php echo $form->textArea($qForm, 'zh', array('rows' => 5, 'cols' => 18)); ?>
      <?php echo $form->error($qForm, 'zh'); ?>
    </div>
    <div class="row" style="float:left;width:33%;">
      <?php echo $form->labelEx($qForm, '<strong>' . Yii::t('main', 'Английский') . '</strong>'); ?>
      <?php echo $form->textArea($qForm, 'en', array('rows' => 5, 'cols' => 18)); ?>
      <?php echo $form->error($qForm, 'en'); ?>
    </div>
    <div class="row" style="float:left;width:33%;">
      <?php echo $form->labelEx($qForm, '<strong>' . Yii::t('main', 'Русский') . '</strong>'); ?>
      <?php echo $form->textArea($qForm, 'ru', array('rows' => 5, 'cols' => 18)); ?>
      <?php echo $form->error($qForm, 'ru'); ?>
    </div>
  </div>
  <div class="row" style="float:left;margin-top: 15px;">
    <?php echo $form->labelEx($qForm, 'global'); ?>
    <?php echo $form->checkBox($qForm, 'global'); ?>
    <?php echo $form->error($qForm, 'global'); ?>
  </div>
  <div class="row buttons" style="float:right;margin: 15px; 25px 0 0;">
    <input type="button" class="blue-btn bigger" value="<?= Yii::t('main', 'Сохранить') ?>"
           onClick="saveTranslation();">
  </div>
  <div class="clear"></div>
  <hr/>
  <iframe name="translate-bkrs" id="translate-bkrs" width="100%" height="450px"
          src="about:blank" frameborder="0"><?= Yii::t('main', 'Ваш браузер не поддерживает фрэймы...') ?></iframe>

<?php $this->endWidget(); ?>