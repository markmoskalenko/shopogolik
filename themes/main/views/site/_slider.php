<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy
 * Date: 24.07.14
 * Time: 19:41
 */
?>
<!-- START REVOLUTION SLIDER  fullwidth mode -->
<div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
     style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:400px;">
    <div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;max-height:400px;height:400;">
        <ul>    <!-- SLIDE  -->
            <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                <!-- MAIN IMAGE -->
                <img src="/modules/revsliderprestashop/uploads/4397888_2489624_3810119bg-bannersequence.jpg"
                     alt="4397888_2489624_3810119bg-bannersequence" data-bgposition="center top" data-bgfit="cover"
                     data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption medium_grey sft tp-resizeme"
                     data-x="363"
                     data-y="74"
                     data-speed="1000"
                     data-start="10"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 2">Denim Jacket
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_grey1 sfr tp-resizeme"
                     data-x="364"
                     data-y="141"
                     data-speed="1000"
                     data-start="10"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 3">Lastest spring summer collection 2014
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_grey2 sfr tp-resizeme"
                     data-x="359"
                     data-y="177"
                     data-speed="1000"
                     data-start="310"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     data-captionhidden="on"
                     style="z-index: 4"> This is Photoshop's version of Lorem Ipsum. Proin gravida <br/> nibh vel velit
                    auctor aliquet. Aenean sollicitudin...
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption link sfb tp-resizeme"
                     data-x="366"
                     data-y="226"
                     data-speed="1000"
                     data-start="910"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     data-linktoslide="2"
                     data-captionhidden="on"
                     style="z-index: 5"><a href=\"#\">shop now !</a>
                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                <!-- MAIN IMAGE -->
                <img src="/modules/revsliderprestashop/uploads/168457_7948913_9539184bg1-bannersequence.jpg"
                     alt="168457_7948913_9539184bg1-bannersequence" data-bgposition="center top" data-bgfit="cover"
                     data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption title2 sft tp-resizeme"
                     data-x="384"
                     data-y="64"
                     data-speed="1000"
                     data-start="500"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 2">Denim jacket
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption subtitle2 sft tp-resizeme"
                     data-x="382"
                     data-y="140"
                     data-speed="1000"
                     data-start="800"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 3">Lastest spring summer collection 2014
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption nitro2 sfb tp-resizeme"
                     data-x="377"
                     data-y="173"
                     data-speed="1000"
                     data-start="1100"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 4">This is Photoshops version of Lorem Ipsum. Proin gravida <br/> nibh vel velit
                    auctor aliquet..
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption big_black sfb tp-resizeme"
                     data-x="385"
                     data-y="225"
                     data-speed="1000"
                     data-start="1400"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 5"><a href=\"#\"> Shop Now ! </a>
                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                <!-- MAIN IMAGE -->
                <img src="/modules/revsliderprestashop/uploads/5879211_4105224_1976013bg2-bannersequence.jpg"
                     alt="5879211_4105224_1976013bg2-bannersequence" data-bgposition="center top" data-bgfit="cover"
                     data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption title3 sft tp-resizeme"
                     data-x="143"
                     data-y="57"
                     data-speed="1000"
                     data-start="500"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 2">Denim jacket
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption subtitle3 sft tp-resizeme"
                     data-x="47"
                     data-y="130"
                     data-speed="300"
                     data-start="800"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 3">Lastest spring summer collection 2014
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption intro3 sfl tp-resizeme"
                     data-x="111"
                     data-y="165"
                     data-speed="1000"
                     data-start="1100"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 4">This is Photoshops version of Lorem Ipsum. Proin gravida <br/> nibh vel velit
                    auctor aliquet..
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption link3 sfb tp-resizeme"
                     data-x="364"
                     data-y="219"
                     data-speed="1000"
                     data-start="1400"
                     data-easing="Power3.easeInOut"
                     data-endspeed="300"
                     style="z-index: 5"><a href=\"#\">shop now !</a>
                </div>
            </li>
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>