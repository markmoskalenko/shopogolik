<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="price_table.php">
* </description>
**********************************************************************************************************************/?>
<?=cms::customContent('item_economy_table');?>
<?
//'pricesX'=>$pricesX, 'pricesYprice'=>$pricesYprice,'pricesYeconomy'=>$pricesYeconomy
$this->widget(
  'chartjs.widgets.ChLine',
  array(
    'width'       => 555,
    'height'      => 150,
    'htmlOptions' => array(),
    'labels'      => $prices->pricesX,
    'datasets'    => array(
      array(
        "fillColor"        => "rgba(220,220,220,0.5)",
        "strokeColor"      => "rgba(220,220,220,1)",
        "pointColor"       => "rgba(220,220,220,1)",
        "pointStrokeColor" => "#ffffff",
        "data"             => $prices->pricesYprice,
      ),
      array(
        "fillColor"        => "rgba(220,220,120,0.5)",
        "strokeColor"      => "rgba(220,220,120,1)",
        "pointColor"       => "rgba(220,120,120,1)",
        "pointStrokeColor" => "#dddddd",
        "data"             => $prices->pricesYeconomy,
      ),
    ),
    'options'     => array(
//Boolean - If we show the scale above the chart data
      'scaleOverlay'        => FALSE,
//Boolean - If we want to override with a hard coded scale
      'scaleOverride'       => FALSE,
//** Required if scaleOverride is true **
//Number - The number of steps in a hard coded scale
      'scaleSteps'          => NULL,
//Number - The value jump in the hard coded scale
      'scaleStepWidth'      => NULL,
//Number - The scale starting value
      'scaleStartValue'     => NULL,
//String - Colour of the scale line
      'scaleLineColor'      => "rgba(0,0,0,.1)",
//Number - Pixel width of the scale line
      'scaleLineWidth'      => 2,
//Boolean - Whether to show labels on the scale
      'scaleShowLabels'     => TRUE,
//Interpolated JS string - can access value
      'scaleLabel'          => "<%=value%>",
//String - Scale label font declaration for the scale label
      'scaleFontFamily'     => "'Arial'",
//Number - Scale label font size in pixels
      'scaleFontSize'       => 11,
//String - Scale label font weight style
      'scaleFontStyle'      => "normal",
//String - Scale label font colour
      'scaleFontColor'      => "#666",
///Boolean - Whether grid lines are shown across the chart
      'scaleShowGridLines'  => TRUE,
//String - Colour of the grid lines
      'scaleGridLineColor'  => "rgba(0,0,0,.05)",
//Number - Width of the grid lines
      'scaleGridLineWidth'  => 1,
//Boolean - Whether the line is curved between points
      'bezierCurve'         => TRUE,
//Boolean - Whether to show a dot for each point
      'pointDot'            => TRUE,
//Number - Radius of each point dot in pixels
      'pointDotRadius'      => 4,
//Number - Pixel width of point dot stroke
      'pointDotStrokeWidth' => 2,
//Boolean - Whether to show a stroke for datasets
      'datasetStroke'       => TRUE,
//Number - Pixel width of dataset stroke
      'datasetStrokeWidth'  => 2,
//Boolean - Whether to fill the dataset with a colour
      'datasetFill'         => TRUE,
//Boolean - Whether to animate the chart
      'animation'           => TRUE,
//Number - Number of animation steps
      'animationSteps'      => 40,
//String - Animation easing effect
      'animationEasing'     => "easeOutQuart",
//Function - Fires when the animation is complete
      'onAnimationComplete' => NULL,
    )
  )
);