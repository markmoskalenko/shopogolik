<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<? $seo_disable_items_index=DSConfig::getVal('seo_disable_items_index')==1;
/*
 if ($seo_disable_items_index) {
  Yii::app()->clientScript->registerMetaTag('noindex', 'robots');
     }
*/
// title and metas section
$meta=Item::getSEOTags($item->top_item);
$this->pageTitle =$meta->title;
$this->meta_desc=$meta->description;
$this->meta_keyword=$meta->keywords;
// end of title and metas section
?>
<div class="item-block">
    <div class="item-title-container">

        <h3 class="item-title">
            <?=$item->top_item->title?>
        </h3>
    </div>
  <div class="item-cat-path">
    <strong><?=Yii::t('main','Товар из категории')?>:</strong>
    <?=$item->top_item->cat_path?>
  </div>

    <div class="item-images">
        <div class="item-main-image">
            <a id="Zoomer" href="<?=Img::getImagePath($item->top_item->pic_url,'_480x480.jpg',false)?>" class="MagicZoomPlus"
               rel="zoom-width: 100%; zoom-height: 100%; zoom-distance: 100%; entire-image: true; hint: false; caption-height: 0; background-color: #dddddd; background-opacity: 80; loading-msg: <?=Yii::t('main','Загрузка')?>...; right-click: true; zoom-position: inner; zoom-align: center; expand-size: width=550; expand-effect: cubic; restore-trigger: click; selectors-change: mouseover; selectors-effect: false; slideshow-effect: expand; hint-position: tr; show-title: false; caption-source: #custom-caption-source; buttons: autohide"
               title="">
              <? if (isset($item->top_item->pic_url) && ($item->top_item->pic_url!='')) { ?>
              <img style="height: 360px;" src="<?=Img::getImagePath($item->top_item->pic_url,'_360x360.jpg',false)?>"/>
              <? } ?>
            </a>
        </div>
        <div class="item-small-images">
            <?php foreach($item->top_item->item_imgs->item_img as $image) { ?>
                <a href="<?=Img::getImagePath($image->url,'_480x480.jpg',false)?>" rel="zoom-id: Zoomer"
                   rev="<?=Img::getImagePath($image->url,'_480x480.jpg',false)?>"><img src="<?=$image->url?>_60x60.jpg"/></a>
            <?php } ?>
        </div>

<div class="newshow">
        <label><?=Yii::t('main','Продавец')?>&nbsp;(ID:<?=$seller->user_id?>):</label>
            <a href="<?=Yii::app()->createUrl('/seller/index',array('nick'=>$seller->user_id))?>" class="seller-name">
                <?=$seller->seller_nick?>
            </a>
<? if ($item->top_item->city!=false) {?>
    <label><?=Yii::t('main','Находится в')?>:</label>
    <? if (isset($item->top_item->state)) { ?>
        <?=$item->top_item->city?>(<?=$item->top_item->state?>)
    <? } else { ?>
        <?=$item->top_item->city?>
    <? }
}?>
<br>
        <label><?=Yii::t('main','Рейтинг')?>:</label>
            <?=$seller->seller_credit;?>
            <? if (isset($seller->rating->level)) {?> / <?=$seller->rating->level;?>
            <?}?>
    <? if (isset($seller->reviews)) { ?>
            <label><?=Yii::t('main','Отзывов')?>:</label>
                <? if (isset($seller->reviews)) { ?>
                    <?=$seller->reviews;?>
                <? } ?>
    <? } ?>
<br>
    <? if ($seller->paySuccess>0) { ?>
            <label><?=Yii::t('main','Продано')?>:</label>
            <?=$seller->paySuccess;?>
            <label><?=Yii::t('main','Возврат')?>:</label>
            <?=$seller->refundCount;?>
    <? } ?>
</div>
      <? if (DSConfig::getVal('search_use_seller_related_in_item')==1) { ?>
      <div class="block" id="seller-related-block">
        <h5><?=Yii::t('main','Товары продавца')?>:</h5>

        <div class="seller-block">
          <div id="sellerrelated"></div>
          <script async type="text/javascript">
            <? if (isset($seller->user_id)) {
            $userid=$seller->user_id;
            } elseif (isset($item->top_item->seller_id)) {
            $userid=$item->top_item->seller_id;
            } else {
             $userid=0;
            } ?>
            loadSellerRelatedBlock('<?=$item->top_item->nick?>',<?=$userid?>,<?=$item->top_item->num_iid?>);
          </script>
        </div>
       </div>
        <? } ?>
</div>

    <div class="item-info">
        <div class="item-params">
            <div class="form-item">
              <div class="item-price">
                <? if (!isset($item->sku)) {
                  if ($item->top_item->price > $item->top_item->promotion_price) {
                    ?>
                    <span id="price"><?= DSConfig::getCurrency(TRUE) ?>
                      <? if (isset($item->top_item->userPromotionPriceNoDelivery)) { ?>
                        <span
                          title="<?= (Yii::app()->user->role == 'superAdmin') ? $item->top_item->userPromotionResUserPrice->report() : '' ?>"><?= $item->top_item->userPromotionPriceNoDelivery; ?></span>
                      <? }
                      else { ?>
                        <span
                          title="<?= (Yii::app()->user->role == 'superAdmin') ? $item->top_item->userPromotionResUserPrice1->report() : '' ?>"><?= $item->top_item->userPromotionPrice1NoDelivery; ?></span>
                                -
                                <span
                          title="<?= (Yii::app()->user->role == 'superAdmin') ? $item->top_item->userPromotionResUserPrice2->report() : '' ?>"><?= $item->top_item->userPromotionPrice2NoDelivery; ?></span>
                      <? } ?>&nbsp;<s>
                        <? if (isset($item->top_item->userPriceNoDelivery)) { ?>
                          <?= $item->top_item->userPriceNoDelivery; ?>
                        <?
                        }
                        else {
                          if ($item->top_item->userPrice1NoDelivery == $item->top_item->userPrice2NoDelivery) {
                            ?>
                            <?= $item->top_item->userPrice1NoDelivery; ?>
                          <? }
                          else { ?>
                            <?= $item->top_item->userPrice1NoDelivery; ?>-<?= $item->top_item->userPrice2NoDelivery; ?>
                          <?
                          }
                        }?></s></span>
                  <? }
                  else { ?>
                    <span id="price"><?= $item->top_item->userPromotionPriceNoDelivery ?></span>
                  <? } ?>
                  <input type="hidden" name="price" value="<?= $item->top_item->promotion_price ?>" id="price_val"/>
                <?
                }
                else {
                  if ($item->sku->price > $item->sku->promotion_price) {
                    ?>
                    <span id="price"><?= $item->sku->userPromotionPrice ?>
                      &nbsp;<s><?= $item->sku->userPrice ?></s></span>
                  <? }
                  else { ?>
                    <span id="price"><?= $item->sku->userPromotionPrice ?></span>
                  <? } ?>
                  <input type="hidden" name="price" value="<?= $item->sku->promotion_price ?>" id="price_val"/>
                <? } ?>
                <? if (isset($item->top_item->userPromotionDelivery) && ($item->top_item->userPromotionDelivery > 0)) { ?>
                  <?= $item->top_item->userDelivery ?>
                <? }
                elseif (isset($item->top_item->userPromotionDelivery1) && ($item->top_item->userPromotionDelivery1 > 0)) { ?>
                  <?= $item->top_item->userPromotionDelivery1 ?>
                <? }
                elseif (isset($item->top_item->userDelivery) && ($item->top_item->userDelivery > 0)) { ?>
                  <?= $item->top_item->userDelivery ?>
                <? } ?>
              </div>
            </div>
          <form action="<?=Yii::app()->createUrl('/cart/add')?>" method="POST">
            <div class="form-item"  style="border: none;">
                <input type="hidden" name="cid" value="<?=$item->top_item->cid?>" id="cid" />
                <input type="hidden" name="iid" value="<?=$item->top_item->num_iid?>" id="iid" />
                <input type="hidden" name="inputprops-processed" id="inputprops-processed" value="0" />
                <?php
                if(!$ajax['input']) {
                   echo Yii::app()->controller->renderPartial('input_props',array('totalCount'=>$item->top_item->num,'input_props'=>$input_props,'lang'=>$lang));
                } else { ?>
                    <div id="item-input-props">
                        <script async type="text/javascript">
                            loadInputProps();
                        </script>
                    </div>
                 <?php } ?>
            </div>
                <div class="form-item">
                    <label for="num"><?=Yii::t('main','Количество')?>:</label>
                    <div class="number"><nobr>
                        <p class="minus">-</p>
                        <input type="text" onchange="getUserPrice()" name="num" id="num" class="item-count" value="1" />
                        <input type="hidden" name="num-processed" id="num-processed" value="1" />
                        <p class="plus">+</p></nobr>
                    </div>
                    <span id="item-count-price" class="item-info-param">x
                        <span id="count-price">
                            <?=(isset($item->sku))
                                ?Formulas::priceWrapper($item->sku->userPriceFinal):
                              Formulas::priceWrapper($item->top_item->userPriceFinal)?>
                        </span> =
                        <span id="sum"><?=(isset($item->sku))?$item->sku->userPromotionPrice
                            :(isset($item->top_item->userPromotionPrice))?$item->top_item->userPromotionPrice
                          :$item->top_item->userPromotionPrice1.'-'.$item->top_item->userPromotionPrice2?></span>
                    </span>
		        </div>
                <div class="form-item">
                    <label><?=Yii::t('main','В наличии')?>:</label>
                    <div class="item-info-param">
                        <span id="item_num"><?=$item->top_item->num?></span>&nbsp;
                    </div>
                    <input type="hidden" name="item_totalcount" id="item_totalcount" value="<?=$item->top_item->num?>">
                  <? if(!(in_array(Yii::app()->user->getRole(),array('guest','user')))) { ?>
                    <div class="item-info-param" id="item_ontaobao">
                        <a href="http://item.taobao.com/item.htm?id=<?=$item->top_item->num_iid?>" target="_blank">&nbsp;<?=Yii::t('main','на taobao.com')?></a>
                    </div>
                  <? } ?>
                </div>
          <? if ($item->top_item->weight_calculated>0) { ?>
          <div class="form-item">
            <label><?=Yii::t('main','Примерный вес 1 шт., грамм')?>:</label>
            <div class="item-info-param">
              <span id="item_num"><?=$item->top_item->weight_calculated?></span>&nbsp;
            </div>
          </div>
          <? } ?>
                <div>
                        <input type="submit" title="<?= Yii::t('admin', 'Сначала выберите характеристики товара') ?>" disabled="disabled" name="doGo" value="<?=Yii::t('main','Добавить в корзину')?>" class="buy-btn bigger" />
                        <input type="button" name="dofav" value="<?=Yii::t('main','Добавить в избранное')?>" class="fav-btn bigger"
                         formaction="<?=Yii::app()->createUrl('/cabinet/favorite/add',array('iid'=>$item->top_item->num_iid,'download'=>true))?>"
                         onclick="addFavorite(this,<?=$item->top_item->num_iid ?>); return false;" />
                  <? if(!(in_array(Yii::app()->user->getRole(),array('guest','user')))) { ?>
                  <a class="ui-icon ui-icon-heart" style="display:inline-block; cursor: pointer;"
                     title="<?= Yii::t('admin', 'Добавить в рекомендованное') ?>"
                     href="<?= Yii::app()->createUrl('/admin/featured/add/', array('id' => $item->top_item->num_iid)) ?>"
                     onclick="addFeatured(this,<?= $item->top_item->num_iid ?>); return false;"></a>
                  <? } ?>
                  <? if(!(in_array(Yii::app()->user->getRole(),array('guest','user')))) { ?>
                    <a class="ui-icon ui-icon-star" style="display:inline-block; cursor: pointer;"
                       title="<?= Yii::t('admin', '"Экспорт в XML') ?>"
                       href="<?= '/item/index/iid/'.$item->top_item->num_iid.'/exportType/CSV'?>"></a>
                  <? } ?>
                        <a href="javascript:void(0);" onclick="window.history.back();">
                            <?=Yii::t('main','Продолжить покупки')?>
                        </a>
                </div>
          </form>
       </div>
<!-- Mel новя табуляция -->
        <div class="item-params" style="margin-top: 0;">
            <div class="blue-tabs tabs">
                <a href="javascript:void(0);" id="tab-1" class="active">
                    <span><?=Yii::t('main','Характеристики товара')?></span>
                </a>
                <a href="javascript:void(0);" id="tab-2">
                    <span><?=Yii::t('main','Дополнительно')?></span>
                </a>
                <a href="javascript:void(0);" id="tab-3">
                    <span><?=Yii::t('main','Экономия')?></span>
                </a>
<!-- DON'T KILL  <a href="javascript:void(0);" id="tab-4">
                <span><?//=Yii::t('main','Отзывы')?></span>
              </a> -->
            </div>

            <div id="tab-1-content" class="tab shown">
                <? if (isset($item->top_item->item_attributes)) { ?>
                    <ul>
                        <? foreach($item->top_item->item_attributes as $attribute) { ?>
                            <li class="block_1">
                                <strong><?=$attribute->prop?>:</strong><br><?=$attribute->val?>
                            </li>
                        <? } ?>
                    </ul>
                <? } else { ?>
                    <!--  Old style output -->
                    <table class="item-table">
                        <? if(is_array($props)) {
                            foreach($props as $pid=>$prop) { ?>
                                <? if($prop->name !== '') { ?>
                                    <tr>
                                        <td><?=$prop->name?></td>
                                        <th><?=$prop->value?></th>
                                    </tr>
                                <? } ?>
                            <? }
                        }?>
                    </table>
                <? } ?>
            </div>





            <? if (isset($item->top_item->priceTable)) {?>
                <div id="tab-3-content" class="tab">
                    <?=$this->renderPartial('price_table', array('prices' => $item->top_item->priceTable))?>
                </div>
            <? } ?>

<!-- DON'T KILL          <div id="tab-4-content" class="item-comments tab">
            <? // foreach($comments as $comment) { ?>
              <div class="comment">
                <div class="comment-info">
                  <div class="comment-author"><?//=$comment->uid?></div>
                  <div class="comment-date"><?//=date('d.m.Y',$comment->date)?></div>
                </div>
                <div class="comment-text">
                  <?//=CHtml::encode($comment->text)?>
                </div>
                <div class="comment-text-bottom"></div>
              </div>
            <?// } ?>
            <?//=$this->renderPartial('comment',array('model'=>$model=new Comment,'iid'=>$item->top_item->num_iid))?>
          </div> -->

        </div>
    </div>
  </div>
<div id="tab-2-content" style="overflow: auto; height: 720px; overflow-y: scroll" class="tab">
    <? if (isset($item->top_item->desc) && ($item->top_item->desc)) {
        echo $this->renderPartial('item_details',array('src'=>$item->top_item->desc),true,false);
    } else { ?>
        <div id="item-detail-block">
            <script async type="text/javascript">
              <?if (isset($item->top_item->descUrl) && ($item->top_item->descUrl)) {?>
              loadItemDetailsFromUrl('<?=urlencode($item->top_item->descUrl)?>');
              <? } else {?>
                loadItemDetails(<?=$item->top_item->num_iid?>);
              <? } ?>
            </script>
        </div>
    <? }?>
</div>
</div>   <!-- Mel ЭТО НИХЕРА НЕ ЛИШНИЙ ДИВ! ЭТО НУЖНЫЙ ДИВ ОН В MAIN ОТКРЫВАЕТСЯ-->

<div class="Похожие товары">
    <?
    if(!$ajax['seller']) {
        echo Yii::app()->controller->renderPartial('sellerBlock',array('seller'=>$seller));
    } else { ?>
        <div id="seller-block">
            <script async type="text/javascript">
                loadSellerBlock('<?=$item->top_item->nick?>',<?=$item->top_item->num_iid?>);
            </script>
        </div>
    <? } ?>
    <? if (FALSE && DSConfig::getVal('search_use_seller_related_in_item')==1) { ?>
        <div class="block" id="seller-related-block">
            <h4 class="title"><?=Yii::t('main','Товары продавца')?></h4>

            <div class="block-content seller-block">
                <div id="sellerrelated"></div>
                <script async type="text/javascript">
                    <? if (isset($seller->user_id)) {
                    $userid=$seller->user_id;
                    } elseif (isset($item->top_item->seller_id)) {
                    $userid=$item->top_item->seller_id;
                    } else {
                     $userid=0;
                    } ?>
                    loadSellerRelatedBlock('<?=$item->top_item->nick?>',<?=$userid?>,<?=$item->top_item->num_iid?>);
                </script>
            </div>
        </div>
    <? } ?>

    <?
    if(isset($itemRelated->items)) {
        echo Yii::app()->controller->renderPartial('itemRelatedBlock',array('itemRelated'=>$itemRelated));
    } ?>
<? $profiler_enable=DSConfig::getVal('profiler_enable')==1;?>
<? if ($profiler_enable && (Yii::app()->user->getRole()=='superAdmin')) {?>
<div class="profiler">
  <? $this->widget('application.components.blocks.ProfilerBlock', array()); ?>
</div>
<? } ?>
<? if(!Yii::app()->user->isGuest) { ?>
<div class="page-title"><?=Yii::t('main','Недавно Вы смотрели')?>:</div>
  <div class="products-list featured" style="width: 100%">
    <? $this->widget('application.components.blocks.SearchItemsList',array(
      'id'=>'recentUser-itemslist',
      'controlAddToFavorites' => true,
      'controlAddToFeatured' => true,
      'controlDeleteFromFavorites' => false,
      'lazyLoad' => true,
      'dataType'=>'itemsRecentUser',
      'pageSize' =>10,
//      'showControl' => null,
      'disableItemForSeo' => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
    ));
    ?>
  </div>
<?}?>


<script type="text/javascript" >
//    $(document).ready(function() {
        $('.minus').click(function () {
            var input = $('#num');
            var count = parseInt(input.val()) - 1;
            count = count <= 1 ? 1 : count;
            input.val(count);
            setTimeout(function() { getUserPrice(false); }, 1500);
            return false;
        });
        $('.plus').click(function () {
          var input = $('#num');
          var count = parseInt(input.val()) + 1;
          input.val(count);
           setTimeout(function() { getUserPrice(false); }, 1500);
            return false;
        });
//    });
</script>

  <? if (isset($item->top_item->debugMessages)) {?>
    <!--  <script type="text/javascript" src="/themes/admin/js/jquery.zclip.js"></script> -->
    <br/>
    <br/>
    <br/>
    <br/>
    <hr/>
    <div>
      <?
      $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'dsg-debug-grid',
        'dataProvider'=>$item->top_item->debugMessages,
        'type'=>'striped bordered condensed',
        'template'=>'{summary}<br/>{pager}{items}{pager}',
        'columns'=>array(
          'function',
          array('name'=>'param_name',
                'type'=>'raw',
                'value'=> function($data) {
                    //$res=preg_replace("/(.{128,256})\s.*/s","\1\.\.\.",$data["param_value"]);
                    $res='<textarea rows="3" cols="20">'.htmlspecialchars($data['param_name']).'</textarea>';
                    return $res;
                  },
          ),
          array('name'=>'param_value',
                'type'=>'raw',
                'value'=> function($data) {
                    //$res=preg_replace("/(.{128,256})\s.*/s","\1\.\.\.",$data["param_value"]);
                    $res='<textarea rows="3" cols="20">'.htmlspecialchars($data['param_value']).'</textarea>';
                    return $res;
                  },
          ),
          array('name'=>'subject',
                'type'=>'raw',
                'value'=> function($data) {
                    $res='<textarea rows="3" cols="20">'.htmlspecialchars($data['subject']).'</textarea>';
                    return $res;
                  },
          ),
          array('name'=>'result',
                'type'=>'raw',
                'value'=> function($data) {
                    $res='<textarea rows="3" cols="20">'.htmlspecialchars($data['result']).'</textarea>';
                    return $res;
                  },
          ),
          array('name'=>'valid',
                'type'=>'raw',
                'value' => function($data) {
                    $res=($data['valid']) ? 'true' : '<span style="color: red;"><strong>false</strong></span>';
                    return $res;
                  }
          ),
          /*    array(

                'type'=>'raw',
                'value'=>'"
                        <a href=\'javascript:void(0);\' onclick=\'renderView(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
                        <a href=\'javascript:void(0);\' onclick=\'renderUpdateForm(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
                        <a href=\'javascript:void(0);\' onclick=\'delete_record(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-trash\'></i></a>
                       "',
                'htmlOptions'=>array('style'=>'width:150px;')
              ),
          */
        ),
      ));

      ?>
    </div>
  <?}?>