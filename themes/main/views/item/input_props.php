<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="input_props.php">
* </description>
**********************************************************************************************************************/?>
<? if (is_array($input_props) && (count($input_props) > 0)) { ?>
  <? foreach ($input_props as $pid => $prop) {
    if (isset($prop->haveImages) && $prop->haveImages) {
      echo Yii::app()->controller->renderPartial('input_props_img', array('pid' => $pid, 'prop' => $prop));
    }
    else {
      echo Yii::app()->controller->renderPartial('input_props_text', array('pid' => $pid, 'prop' => $prop));
    }
  }
  $script = '
  $(".selectable").selectable(
  {
    filter: "li",
//    tolerance: "fit",
    stop: function(event, ui) {
      var el_id = $(this).attr("id");
      var result = $("#selres"+"-"+el_id ).empty();
      $("#input"+"-"+el_id ).prop("value",0);
      var liobj = $("#" + el_id+" .ui-selected");
      var liobjId = undefined;
      if (liobj!=undefined) {
       liobjId=liobj.prop("id");
       if (liobjId!=undefined) {
       $("#input"+"-"+el_id ).prop("value",liobjId);
      result.append(liobj.html());
       reloadSku();
       } else {
       $("#input"+"-"+el_id ).prop("value",0);
       result.append("' . Yii::t('main', 'Не выбрано') . '");
       $("#item_num").html("&dash;&dash;&dash;");
       $("#item_totalcount").prop("value",0);
       $("#inputprops-processed").prop("value",0);
       $(".buy-btn").prop("disabled",true);
       }
      }
    }
  }
  );';
}
elseif ($totalCount > 0) {
  $script = '$(".buy-btn").removeAttr("disabled");';
}
else {
  $script = '';
}  ?>
<!-- ===================================================== -->
<?

if (Yii::app()->request->isAjaxRequest) {
  echo '<script type="text/javascript">' . $script . '</script>';
//Yii::app()->clientScript->registerScript('inputProps',$script,CClientScript::POS_READY);
}
else {
//  echo '<script type="text/javascript">'.$script.'</script>';
  Yii::app()->clientScript->registerScript('inputProps', $script, CClientScript::POS_READY);
}

?>
