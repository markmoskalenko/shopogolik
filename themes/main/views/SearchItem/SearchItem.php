<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SearchItem.php">
* </description>
**********************************************************************************************************************/?>
<div class="product<?= $newLine ?>">
  <div class="control">
    <? if ($showControl) { ?>
  <a class="ui-icon ui-icon-heart" style="display:inline-block; cursor: pointer;"
     title="<?= Yii::t('admin', 'Добавить в рекомендованное') ?>"
     href="<?= Yii::app()->createUrl('/admin/featured/add/', array('id' => $item->num_iid)) ?>"
     onclick="addFeatured(this,<?= $item->num_iid ?>); return false;"></a>
    <? } ?>
  <a class="ui-icon ui-icon-circle-plus" style="display:inline-block; cursor: pointer;"
     title="<?= Yii::t('admin', 'Добавить в избранное') ?>" href="<?=Yii::app()->createUrl('/cabinet/favorite/add',array('iid'=>$item->num_iid))?>"
     onclick="addFavorite(this,<?= $item->num_iid ?>); return false;"></a>
</div>
<div class="product-image">
  <!--<img class="lazy" src="img/grey.gif" data-original="img/example.jpg" width="640" height="480">-->
  <a <? if ($disableItemForSeo) {
    echo 'rel="nofollow"';
  } ?> href="<?= Yii::app()->createUrl('/item/index', array('iid' => $item->num_iid)) ?>">
    <? if ($lazyLoad) { ?>
      <img class="lazy"
           src="<?= Yii::app()->request->baseUrl ?>/themes/<?= Yii::app()->theme->name ?>/images/zoomloader.gif"
         data-original="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="<?= $item->html_alt ?>"
         title="<?= $item->popup_title ?>"/>
      <!--onload="this.width=160;this.height=160;" onmouseout="this.width=160;this.height=160;" onmouseover="this.width=195;this.height=195;"-->
    <noscript><img src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt=""/></noscript>
    <? } else { ?>
      <img src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
    <? } ?>
  </a>
</div>
<div class="product-price">
  <? if ($item->price > $item->promotion_price) { ?>
    <?= Formulas::priceWrapper($item->userPromotionPrice) ?>
    <div class="product-promotion">&nbsp;-&nbsp;<?= $item->userPrice - $item->userPromotionPrice; ?></div>
  <? }
  else { ?>
    <?= Formulas::priceWrapper($item->userPromotionPrice) ?>
  <? } ?>
  <? if (isset($item->tmall)) {
    if ($item->tmall) {
      ?>
      <span class="tmall"><b>&nbsp;T</b></span>
    <?
    }
  } ?>
</div>
<? if (isset($item->seller_rate)) {
        if ($item->seller_rate>0) { ?>
       <div class="product-seller-rate">
         <?=$item->seller_rate?>
       </div>
<? }
}?>
  </div>