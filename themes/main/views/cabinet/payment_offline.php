<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="payment_offline.php">
* </description>
**********************************************************************************************************************/?><div class="blue-tabs">
    <a href="<?=Yii::app()->createUrl('/cabinet/balance')?>">
	<span><?=Yii::t('main','Информация о счёте')?></span>
    </a>
    <div class="active-tab">
	<span><?=$this->pageTitle?></span>
    </div>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/statement')?>">
        <span><?=Yii::t('main','Информация о счете')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/transfer')?>">
        <span><?=Yii::t('main','Перевод денег на другой счет')?></span>
    </a>
</div>
<div class="cabinet-content">
  <div class="form">
    <div  id="offline-payment-form">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title><?=CHtml::encode(DSConfig::getVal('site_name').': '.Yii::t('main','Платёжный документ'))?></title>
      <link href="//favicon.ico" type="image/x-icon" rel="icon" />
 <?=PaySystems::preRenderForm($data,$type); ?>
    </div>
    <br/>
    <div class="row buttons">
      <?php echo CHtml::button(Yii::t('main','Назад'),array('class'=>'blue-btn bigger','submit' => Yii::app()->createUrl('cabinet/balance/payment'))); ?>
<script type="text/javascript">
  function printOfflinePayment() {
    var w = window.open();
    var html = $("#offline-payment-form").html();
    $(w.document.body).html(html);
  }
</script>
      <?php echo CHtml::button(Yii::t('main','Для печати'),array('class'=>'blue-btn bigger','onclick' => 'printOfflinePayment();')); ?>
    </div>
  </div>

</div>