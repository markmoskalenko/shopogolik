<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_transfer.php">
* </description>
**********************************************************************************************************************/?>
<?php
return array(
    'elements'=>array(
        'id_account'=>array(
            'type'=>'text',            
            'maxlength'=>128,
        ),
        'summ'=>array(
            'type'=>'text',            
            'maxlength'=>128,
        ),
    ),
    'buttons'=>array(
        'submit'=>array(
            'type'=>'submit',
            'class'=>'blue-btn bigger',
            'style'=>'width:130px;',
            'label'=>Yii::t('main','Перевести деньги'),
        ),
    ),
    
    
);    

