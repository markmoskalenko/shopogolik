<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="balance.php">
* </description>
**********************************************************************************************************************/?><div class="blue-tabs">
    <div class="active-tab">
	<span><?=$this->pageTitle?></span>
    </div>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/payment')?>">
        <span><?=Yii::t('main','Пополнить счет')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/statement')?>">
        <span><?=Yii::t('main','Информация о счете')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/transfer')?>">
        <span><?=Yii::t('main','Перевод денег на другой счет')?></span>
    </a>
</div>
<div class="cabinet-content">
    <p><strong>
    <?=Yii::t('main','Ваш текущий баланс')?>: <?=Formulas::priceWrapper(Formulas::convertCurrency(Users::getBalance(Yii::app()->user->id),DSConfig::getVal('site_currency'),DSConfig::getCurrency()),DSConfig::getCurrency())?>
    </strong></p>
    <script>
        $(function() {
            $( "#date_from" ).datepicker({dateFormat: "yy-mm-dd"});
            $( "#date_to" ).datepicker({dateFormat: "yy-mm-dd"});
        });
    </script>
    <div class="form">
        <?php 
        $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'post',
        )); 
        ?>
        <div class="row field">
            <?=CHtml::label(Yii::t('main','Выписка с:'),'')?>
            <?=CHtml::textField('date_from',$date_from,array('id'=>'date_from'))?>
        </div>
        <div class="row field">
            <?=CHtml::label(Yii::t('main','по:'), '')?>
            <?=CHtml::textField('date_to',$date_to,array('id'=>'date_to'))?>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('main','Показать'),array('class'=>'blue-btn bigger')); ?>
        </div>
        <?php $this->endWidget(); ?>    
    </div>
    <div class="cabinet-table">
<!-- ===================================================================== -->
      <? $this->widget('zii.widgets.grid.CGridView',array(
        'id'=>'payments-grid',
        'dataProvider'=>$payments,
        'enableSorting' => FALSE,
//    'filter'=>$model,
        'pager' => array(
          'header' => '',
          'firstPageLabel' => '&lt;&lt;',
          'prevPageLabel' => '&lt;',
          'nextPageLabel' => '&gt;',
          'lastPageLabel' => '&gt;&gt;',
        ),
//    'type'=>'striped bordered condensed',
        'template' => '{summary}{items}{pager}',
        'summaryText' => Yii::t('main', 'Платежи') . ' {start}-{end} ' . Yii::t('main', 'из') . ' {count}',
        'columns'=>array(
          array('name'=>'id',
          ),
          array('name'=>'sum',
            'type'=>'raw',
            'value'=>function($data){
                if ($data->sum>0) {
                return '<b>'.$data->sum.'</b>';
                } else {
                  return $data->sum;
                }
              }
          ),
          array('name'=>'text_status',
          ),
          array('name'=>'text_date',
          ),
          array('name'=>'description',
          ),
        ),
      ));
      ?>
    </div>
<!-- ===================================================================== -->
</div>