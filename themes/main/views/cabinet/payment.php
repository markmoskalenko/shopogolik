<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="payment.php">
* </description>
**********************************************************************************************************************/?>
<div class="blue-tabs">
    <a href="<?=Yii::app()->createUrl('/cabinet/balance')?>">
	<span><?=Yii::t('main','Выписка по счету')?></span>
    </a>
    <div class="active-tab">
	<span><?=$this->pageTitle?></span>
    </div>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/statement')?>">
        <span><?=Yii::t('main','Информация о счете')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/transfer')?>">
        <span><?=Yii::t('main','Перевод денег на другой счет')?></span>
    </a>
</div>
<div class="cabinet-content">
    <div class="form">
        <?php $form=$this->beginWidget('CActiveForm') ?>
        <div class="row">
            <?php echo $form->labelEx($model,'sum'); ?>
            <?php echo $form->textField($model,'sum'); ?>
            <?php echo $form->error($model,'sum'); ?>
        </div>
        <div class="row">
          <?php echo $form->labelEx($model,'phone'); ?>
          <?php echo $form->textField($model,'phone'); ?>
          <?php echo $form->error($model,'phone'); ?>
        </div>

        <div class="payment-systems">
        <h3 style="align:center;"><?=Yii::t('main','Вариант оплаты')?>:</h3>

        <? if ($paySystems)
        foreach ($paySystems as $paySystem) { ?>

<!--        <h5><?//=$paySystem->name;?></h5> -->
        
         <div class="row">   
<!--         <label for="bankCard"> -->
                <label for="<?=$paySystem->int_name;?>">
                    <input class="pay-radio" type="radio" id="<?=$paySystem->int_name;?>" value="<?=$paySystem->int_name;?>" name="preference"/>
                  <? if (Utils::AppLang()=='ru') { ?>
                    <?=$paySystem->name_ru;?>
                  <? } else { ?>
                    <?=$paySystem->name_en;?>
                  <? }?>
                        <div class="desc">
                            <img src="<?=$paySystem->logo_img;?>" height="40" alt="" />
                          <? if (Utils::AppLang()=='ru') { ?>
                          <?=$paySystem->descr_ru;?>
                          <? } else { ?>
                          <?=$paySystem->descr_en;?>
                          <? }?>
                        </div>
                </label>
            </div>  
        <? } ?>
       </div>
        <div class="row buttons">
            <?=CHtml::submitButton(Yii::t('main','Далее'),array('class'=>'blue-btn bigger'))?>
        </div>
    <?php $this->endWidget();?>
    </div>    
</div>
<script type="text/javascript">
  $('.pay-radio').prop('checked', false);
</script>