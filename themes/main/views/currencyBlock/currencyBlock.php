<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="currencyBlock.php">
 * </description>
 **********************************************************************************************************************/
?>
<!-- Block currencies module -->
<div id="currencies-block-top" class="fx-fadeInDown">
    <span class="cur-label"><?= Yii::t('main', 'Валюта') ?> :</span>
    <? $currency_array = explode(',', DSConfig::getVal('site_currency_block'));
    foreach ($currency_array as $val) {
        ?>
        <? if ($currency == $val) { ?>
            <div class="current">
                <strong><?= strtoupper($val) ?></strong>
            </div>
        <? } ?>
    <? } ?>
    <ul id="first-currencies" class="currencies_ul toogle_content">
        <? foreach ($currency_array as $val) { ?>
            <li <? if ($currency == $val) echo 'class="selected"'; ?>>
                    <a  href="/user/setcurrency/curr/<?= $val ?>" rel="nofollow"
                   title="<?= strtoupper($val) ?>">
                    <?= strtoupper($val) ?>
                </a>
            </li>
        <? } ?>
    </ul>
</div>
<!-- /Block currencies module --><!-- Block languages module -->