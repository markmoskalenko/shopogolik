<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="CategoriesBlock.php">
 * </description>
 **********************************************************************************************************************/
?>
<? $useVirtualMenu = DSConfig::getVal('search_useVirtualMenu') == 1; ?>

<? var_dump($useVirtualMenu); ?>
<div class="vmegamenu_content">
    <div id="pt_menu3" class="pt_menu">
        <div class="parentMenu">
            <a href="http://pr.home/en/3-shirts">
                <span>Shirts</span>
            </a>
        </div>
        <div class="wrap-popup">
            <div id="popup3" class="popup">
                <div class="arrow-left"></div>
                <div class="block1">
                    <div class="column first col1" style="float:left;">
                        <div class="itemMenu level1">
                            <a class="itemMenuName level3" href="http://pr.home/en/4-tops"><span>Tops</span></a>
                            <div class="itemSubMenu level3">
                                <div class="itemMenu level4">
                                    <a class="itemMenuName level4" href="http://pr.home/en/5-tshirts"><span>T-shirts</span></a>
                                    <a class="itemMenuName level4" href="http://pr.home/en/7-blouses"><span>Blouses</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column col2" style="float:left;">
                        <div class="itemMenu level1"><a class="itemMenuName level3"
                                                        href="http://pr.home/en/8-dresses"><span>Dresses</span></a>

                            <div class="itemSubMenu level3">
                                <div class="itemMenu level4"><a class="itemMenuName level4"
                                                                href="http://pr.home/en/9-casual-dresses"><span>Casual&nbsp;Dresses</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/10-evening-dresses"><span>Evening&nbsp;Dresses</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/11-summer-dresses"><span>Summer&nbsp;Dresses</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column col3" style="float:left;">
                        <div class="itemMenu level1"><a class="itemMenuName level3"
                                                        href="http://pr.home/en/20-handbags"><span>Handbags</span></a>

                            <div class="itemSubMenu level3">
                                <div class="itemMenu level4"><a class="itemMenuName level4"
                                                                href="http://pr.home/en/22-handbags-us"><span>Handbags&nbsp;US</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/23-handbags-eu"><span>Handbags&nbsp;EU</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/24-handbags-vn"><span>Handbags&nbsp;VN</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/25-handbags-en"><span>Handbags&nbsp;EN</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column last col4" style="float:left;">
                        <div class="itemMenu level1"><a class="itemMenuName level3"
                                                        href="http://pr.home/en/21-new-hbags"><span>New&nbsp;Hbags</span></a>

                            <div class="itemSubMenu level3">
                                <div class="itemMenu level4"><a class="itemMenuName level4"
                                                                href="http://pr.home/en/26-new-hbags-vp"><span>New&nbsp;Hbags&nbsp;VP</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/27-new-hbags-kp"><span>New&nbsp;Hbags&nbsp;KP</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/28-new-hbags-xp"><span>New&nbsp;Hbags&nbsp;XP</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/29-new-hbags-up"><span>New&nbsp;Hbags&nbsp;UP</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column last col5" style="float:left;">
                        <div class="itemMenu level1"><a class="itemMenuName level3"
                                                        href="http://pr.home/en/21-new-hbags"><span>New&nbsp;Hbags</span></a>

                            <div class="itemSubMenu level3">
                                <div class="itemMenu level4"><a class="itemMenuName level4"
                                                                href="http://pr.home/en/26-new-hbags-vp"><span>New&nbsp;Hbags&nbsp;VP</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/27-new-hbags-kp"><span>New&nbsp;Hbags&nbsp;KP</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/28-new-hbags-xp"><span>New&nbsp;Hbags&nbsp;XP</span></a><a
                                        class="itemMenuName level4" href="http://pr.home/en/29-new-hbags-up"><span>New&nbsp;Hbags&nbsp;UP</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearBoth"></div>
                </div>
            </div>
        </div>
    </div>





    <div id="pt_menu19" class="pt_menu noSub">
        <div class="parentMenu">
            <a href="http://pr.home/en/19-underwear">
                <span>UnderWear</span>
            </a>
        </div>
    </div>
</div>

