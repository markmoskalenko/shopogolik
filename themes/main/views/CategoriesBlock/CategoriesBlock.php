<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="CategoriesBlock.php">
 * </description>
 **********************************************************************************************************************/
?>
<? $useVirtualMenu = DSConfig::getVal('search_useVirtualMenu') == 1;
$i = 0;
?>


<div class="vmegamenu_content">
    <?php if (isset($mainMenu)) {
        foreach ($mainMenu as $id => $menu) {
            $i++;
            ?>
            <?php
            $type = 'category';
            if (isset($menu['children']) && (count($menu['children']) > 0) && !$useVirtualMenu) {
                $link = Yii::app()->createUrl('/category/page', array('page_id' => $menu['pkid']));
            } else {
                $link = Yii::app()->createUrl('/' . $type . '/index', array('name' => $menu['url']));
            }
            ?>

            <div id="pt_menu<?= $i ?>" class="pt_menu">
                <div class="parentMenu">
                    <? if (($menu['cid'] == 0) && ($menu['query'] == '')) { ?>
                        <a href="javascript:void(0);" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $menu['pkid'] . "')\"" : ''; ?>><span><?= $menu['view_text'] ?></span></a>
                    <?
                    } else {
                        ?>
                        <a
                            href="<?= ($adminMode) ? 'javascript:void(0);' : $link ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $menu['pkid'] . "')\"" : ''; ?> ><span>
                        <?= $menu['view_text'] ?></span>
                        </a>
                    <? } ?>
                </div>

                <?php if (isset($menu['children']) && count($menu['children']) > 0) { ?>
                    <div class="wrap-popup">
                        <div id="popup<?= $i ?>" class="popup">
                            <div class="arrow-left"></div>
                            <? $j = 1; ?>

                            <div class="block1">
                                <? foreach ($menu['children'] as $id2 => $items2) { ?>

                                    <? if ($useVirtualMenu) { ?>

                                        <? if (isset($items2) && isset($items2['url'])) { ?>

                                            <div class="column <? if ($j == 1) echo 'first'; ?> col<?= $j ?>"
                                                 style="float:left;">
                                                <div class="itemMenu">
                                                    <? if (($items2['cid'] == 0) && ($items2['query'] == '')) { ?>
                                                        <a class="itemMenuName"
                                                           href="javascript:void(0);"><span><?= $items2['view_text'] ?></span></a>
                                                    <?
                                                    } else {
                                                        ?>
                                                        <a class="itemMenuName" href="<?=
                                                        ($adminMode) ? 'javascript:void(0);' :
                                                            Yii::app()
                                                                ->createUrl('/' . $type . '/index', array('name' => $items2['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items2['pkid'] . "')\"" : ''; ?> >
                                                            <span> <?= $items2['view_text'] ?></span>
                                                        </a>
                                                    <? } ?>

                                                    <? if (isset($items2['children']) && count($menu['children']) > 0) { ?>
                                                        <div style="margin-bottom: 10px" class="itemSubMenu">
                                                            <div class="itemMenu">
                                                                <? foreach ($items2['children'] as $id3 => $items3) { ?>

                                                                    <a class="itemMenuName" href="<?=
                                                                    ($adminMode) ? 'javascript:void(0);' :
                                                                        Yii::app()
                                                                            ->createUrl('/' . $type . '/index', array('name' => $items3['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items3['pkid'] . "')\"" : ''; ?> >
                                                                        <?= $items3['view_text'] ?>
                                                                    </a>


                                                                <? } ?>

                                                            </div>
                                                        </div>
                                                    <? } ?>
                                                </div>
                                            </div>

                                        <? } ?>

                                    <? } else { ?>





                                        <?php if (isset($menu['children']) && count($menu['children']) > 0) { ?>




                                            <? foreach ($menu['children'] as $id2 => $items2) { ?>

                                                <? if (isset($items2) && isset($items2['url'])) { ?>
                                                    <a href="<?=
                                                    ($adminMode) ? 'javascript:void(0);' :
                                                        Yii::app()
                                                            ->createUrl('/' . $type . '/index', array('name' => $items2['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items2['pkid'] . "')\"" : ''; ?> >
                                                        <?= $items2['view_text'] ?>
                                                    </a>&nbsp;
                                                <? } ?>
                                            <?php } ?>



                                        <? } ?>

                                    <? } ?>
                                    <? if ($j % 4 == 0) echo '<div style="clear: both"></div>' ?>
                                    <? $j++;
                                } ?>
                                <div class="clearBoth"></div>
                            </div>
                        </div>
                    </div>

                <? } ?>
            </div>


        <?
        }
    }
    ?>
</div>

