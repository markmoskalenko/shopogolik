<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 **********************************************************************************************************************/
?>
<?

$count = $this->dataProvider->itemCount;
$i = $index;

$resUserPrice = Formulas::getUserPrice(
    array(
        'price' => $data['price'],
        'count' => 1,
        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
        'postageId' => FALSE,
        'sellerNick' => FALSE,
    ));
$userPrice = $resUserPrice->price;
$resUserPrice = Formulas::getUserPrice(
    array(
        'price' => $data['promotion_price'],
        'count' => 1,
        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
        'postageId' => FALSE,
        'sellerNick' => FALSE,
    ));
$userPromotionPrice = $resUserPrice->price;


//var_dump(Formulas::priceWrapper($userPromotionPrice));

?>

<? if ($i == 0): ?>
<ul id="homefeatured" class="pos_animated product_list grid row homefeatured tab-pane active">
    <? endif; ?>



    <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 fx-fadeInDown a--><?= $i ?><? if ($i % 4 == 0) echo 'first-in-line'; ?><!--">
        <div class="product-container" itemscope itemtype="http://schema.org/Product">
            <div class="left-block">
                <div class="product-image-container">
                    <a <? if ($disableItemForSeo) {
                        echo 'rel="nofollow"';
                    } ?> class="product_img_link" itemprop="url"
                         href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>">
                        <img class="lazy"
                             src="<?= Yii::app()->request->baseUrl ?>/themes/<?= Yii::app()->theme->name ?>/images/zoomloader.gif"
                             data-original="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt=""
                             title=""/>
                    </a>

                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<span itemprop="price" class="price product-price">
								<?= Formulas::priceWrapper($userPromotionPrice) ?>							</span>
                    </div>
                </div>
            </div>
            <div class="right-block">
                <h5 itemprop="name">
                </h5>

                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<span itemprop="price" class="price product-price">
								<?= Formulas::priceWrapper($userPromotionPrice) ?>							</span>
                    <meta itemprop="priceCurrency" content="1"/>
                </div>
                <div class="button-container">
                    <a class="button ajax_add_to_cart_button btn btn-default"
                       href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>"
                       title="<?=Yii::t('main','Добавить в корзину')?>">
                        <span>+ <?=Yii::t('main','Добавить в корзину')?></span>
                    </a>

                    <div class="wishlist">
                        <a class="addToWishlist wishlistProd_35" href="#" rel="35"
                           onclick="WishlistCart('wishlist_block_list', 'add', '35', false, 1); return false;">
                            + Wishlist
                        </a>
                    </div>
                    <a class="quick-view" href="http://pr.home/en/home/35-etiam-at-metus.html"
                       rel="http://pr.home/en/home/35-etiam-at-metus.html">
                        <span>+ Quick view</span>
                    </a>
                </div>
            </div>
        </div>
        <!-- .product-container> -->
    </li>


    <? if ($i == $count - 1): ?>
</ul>
<? endif; ?>

