<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 **********************************************************************************************************************/
?>
<?

$count = $this->dataProvider->itemCount;
$i = $index;

$resUserPrice = Formulas::getUserPrice(
    array(
        'price' => $data['price'],
        'count' => 1,
        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
        'postageId' => FALSE,
        'sellerNick' => FALSE,
    ));
$userPrice = $resUserPrice->price;
$resUserPrice = Formulas::getUserPrice(
    array(
        'price' => $data['promotion_price'],
        'count' => 1,
        'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
        'postageId' => FALSE,
        'sellerNick' => FALSE,
    ));
$userPromotionPrice = $resUserPrice->price;


if ($data['price'] > $data['promotion_price']) {

    $sale = abs($userPrice - $userPromotionPrice) +  $userPromotionPrice;
} else {
    $sale = false;
}


//var_dump(Formulas::priceWrapper($userPromotionPrice));
//id="homefeatured"
?>

<? if ($i == 0): ?>
<ul class="pos_animated product_list grid row homefeatured tab-pane active">
    <? endif; ?>
    <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 fx-fadeInDown a--><?= $i ?><? if ($i % 4 == 0) echo 'first-in-line'; ?><!--">
        <div class="product-container" itemscope itemtype="http://schema.org/Product">
            <div class="left-block">
                <div class="product-image-container">
                    <a <? if ($disableItemForSeo) {
                        echo 'rel="nofollow"';
                    } ?> class="product_img_link" itemprop="url"
                         href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>">
                        <img class="lazy"
                             src="<?= Yii::app()->request->baseUrl ?>/themes/<?= Yii::app()->theme->name ?>/images/zoomloader.gif"
                             data-original="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt=""
                             title=""/>
                    </a>

                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<span itemprop="price" class="price product-price">
								<?= Formulas::priceWrapper($userPromotionPrice) ?>
                                                    </span>

                        <? if ($sale): ?>
                            <span class="old-price product-price">
									<?= Formulas::priceWrapper($sale) ?>
								</span>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="right-block">
                <h5 itemprop="name">
                </h5>

                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<span itemprop="price" class="price product-price">
								<?= Formulas::priceWrapper($userPromotionPrice) ?>
                                                    </span>
                    <? if ($sale): ?>
                        <span class="old-price product-price">
									<?= Formulas::priceWrapper($sale) ?>
								</span>
                    <? endif; ?>
                </div>
                <div class="button-container">


                    <div class="wishlist">
                        <a class="addToWishlist wishlistProd_35"
                           href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>"

                        + <?= Yii::t('main', 'Добавить в корзину') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- .product-container> -->
    </li>


    <? if ($i == $count - 1): ?>
</ul>
<? endif; ?>

