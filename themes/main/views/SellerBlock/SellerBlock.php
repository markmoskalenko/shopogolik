<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SellerBlock.php">
* </description>
**********************************************************************************************************************/?>
<div class="block" id="seller-block">
    <script>
        $(function(){
            $('#add_favorite_seller').click(function(){
                $("#add_favorite_seller_div").toggle("slow");
                return false;
            });
            $("#favorite_reset").click(function(){
                $("#add_favorite_seller_div").toggle("slow");
                return false;
            });
        });
        
    </script>
        <h4 class="title"><?=Yii::t('main','Продавец').': '.$user->seller_nick?></h4>
	<div class="block-content seller-block">
            <div class="seller-info">                
		<div class="seller-rating-help"><?=Yii::t('main','Рейтинг')?>:</div>
		<a href="javascript:void(0);" class="helper-icon" title="<?=Yii::t('main','Рейтинг продавцов')?>"></a>
		<div class="clear"></div>
		<div class="seller-rating"><?=$user->seller_credit?></div>
		<div class="rating-icons level-<?=$user->rating->level?>" style="width: <?=$user->rating->width?>px;"></div>
            </div>
            <div class="seller-reviews">
  		<?=Yii::t('main','Положительных отзывов')?>:
                <strong><?=rand(75,100)?>%</strong>
<!-- round($user->seller_credit->good_num/$user->seller_credit->total_num*100,2) -->
            </div>
        </div>
    </div>

<div class="block" id="search-terms">

    <h4 class="title"><?=Yii::t('main','Поиск по продавцу')?></h4>
    <div class="block-content">			
        <form id="search-params" action="<?=Yii::app()->createUrl('/seller/index',$params)?>" method="post">
            <div class="form-item">
		<label for="price-min"><?=Yii::t('main','Цена')?>:</label>
		<span><?=Yii::t('main','от')?></span>
		<input type="text" size="4" class="small_input" name="price_min" id="price-min" 
                       value="<?=(isset($params['price_min']))?CHtml::encode($params['price_min']):0?>" />
		<span><?=Yii::t('main','до')?></span>
		<input type="text" size="4" class="small_input" name="price_max"
                       value="<?=(isset($params['price_max']))?CHtml::encode($params['price_max']):''?>" />
		<span><?=$symb?></span>
            </div>
            <div class="form-item">
                <label for="sales-min"><?=Yii::t('main','Количество продаж')?>:</label>
                <span><?=Yii::t('main','от')?></span>
                <input type="text" size="4" class="small_input" name="sales_min" id="sales-min"
                       value="<?=(isset($params['sales_min']))?CHtml::encode($params['sales_min']):''?>" />
		<span><?=Yii::t('main','до')?></span>
		<input type="text" size="4" class="small_input" name="sales_max" 
                       value="<?=(isset($params['sales_max']))?CHtml::encode($params['sales_max']):''?>" />
		<span><?=Yii::t('main','шт.')?></span>
            </div>
            <div class="form-item submit">
                <a href="javascript:void(0);" onclick="$('#search-params').submit(); return false;" class="search-terms-sbmt">
                    <?=Yii::t('main','Поиск')?>
                </a>
            </div>            
	</form>
    </div>

</div>	

<!-- search categories --> 
<?php if($categories):?>
<div class="block" id="categories-block">

    <h4 class="title"><?=Yii::t('main','Результаты по категориям')?></h4>
    <div class="block-content categories">        
	<ul id="search-categories">
            <?php foreach($categories as $cat):?>
            <?php $params['cid'] = $cat->cid; ?>
            <li><a href="<?=Yii::app()->createUrl('/seller/index',$params)?>">
                    <?=$cat->{$lang}?> <span><!--(<?//=$count[$cat->cid]?>)--></span>
             </a></li>
            <?php endforeach ?>
	</ul>
    </div>

</div>
<?php endif; ?>