<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrderCommentsViewBlock.php">
* </description>
**********************************************************************************************************************/?>
<div>
  <?=$message?>
</div>
<div>
  <? if(!$isItem) { ?>
  <?=OrdersCommentsAttaches::getAttachesPreview($parentId)?>
  <? } else { ?>
    <?=OrdersItemsCommentsAttaches::getAttachesPreview($parentId)?>
  <? } ?>
</div>
<div class="clear"></div>
<div>
  <div>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
      'id'=>'form-img-'.$blockId,
      'enableAjaxValidation'=>false,
      'enableClientValidation'=>false,
      'method'=>'post',
      'action'=>array(Yii::app()->createUrl("/message/addimage")),
      'type'=>'inline',
      'htmlOptions'=>array(
      'enctype' => 'multipart/form-data',
      'target'=> 'upload-target-'.$blockId,
      //'onsubmit'=>"
      //alert('submt');
      //return false;",/* Disable normal form submit */
      //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key
      ),
    ));
    ?>
<!--    <fieldset> -->
      <?php echo $form->errorSummary($newAttaches,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
      <div class="comment-select">
        <iframe hidden="hidden" id="upload-target-<?=$blockId?>" name="upload-target-<?=$blockId?>"
           src="" style="width:0px; height:0px; border:0 solid #ffffff;"></iframe>
           <input type="hidden" value="<?=($isItem)? 1 : 0 ?>" name="isItem" />
           <input type="hidden" value="<?=$blockId?>" name="blockId" />

        <?
            echo $form->hiddenField($newAttaches,'comment_id',array());
            echo $form->fileField($newAttaches,'uploadedFile',array('accept'=>'image/*',
            'onchange'=>"
            $( '#form-img-".$blockId."' ).submit();
            ",
              // 4Mel
              'title'=>Yii::t('main','Добавить изображение'),
            ));
        ?>

      </div>
      <div class="comment-select-btn">+</div>
  </div>
<!--  </fieldset> -->
  <?php $this->endWidget(); ?>
<script>
  $('#upload-target-<?=$blockId?>').load(function(){$.fn.yiiGridView.update('grid-<?=$blockId?>');}).appendTo('body');
</script>
</div>