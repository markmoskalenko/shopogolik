<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="main.php">
 * </description>
 **********************************************************************************************************************/
?>
<?
$twoCol = array('search', 'category', 'favorite', 'brand', 'seller');
$theme = Yii::app()->theme->name;
?>
<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <? $this->renderPartial('//layouts/_head', array('twoCol' => $twoCol, 'theme' => $theme)) ?>
</head>

<body id="index" class="index hide-right-column lang_en">
<div id="page">
<div class="header-container">
<header id="header">
<div class="nav pos_animated">
    <div class="container">
        <div class="row">
            <nav>
                <?php $this->widget('application.components.blocks.currencyBlock'); ?>
                <!-- Block languages module -->
                <div id="languages-block-top" class="languages-block fx-fadeInDown">
                    <span class="cur-label"><?= Yii::t('main', 'Язык') ?> :</span>
                    <? $lang_array = explode(',', DSConfig::getVal('site_language_block'));
                    foreach ($lang_array as $val) {
                        ?>
                        <? if ((Yii::app()->language == $val)) { ?>
                            <div class="current">
                                <img
                                    src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/img/l/<?= $val ?>.png"
                                    alt="<?= $val ?>" width="16" height="11"/> <span><?= $val ?></span>
                            </div>
                        <?
                        }
                    }?>
                    <ul id="first-languages" class="languages-block_ul toogle_content">
                        <?
                        foreach ($lang_array as $val) {
                            ?>
                            <li class="<?= $val ?><?= (Yii::app()->language == $val) ? 'selected' : '' ?>">
                                <a href="/user/setlang/<?= $val ?>">
                                    <img
                                        src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/img/l/<?= $val ?>.png"
                                        alt="<?= $val ?>" width="16" height="11"/>
                                    <span><?= $val ?></span>
                                </a>
                            </li>
                        <? } ?>
                    </ul>
                </div>
                <!-- /Block languages module -->
                <div class="msg fx-fadeInDown">
                        <span class="welcome-msg">
                            Слоган
                        </span>
                </div>
                <?php $this->widget('application.components.blocks.userBlock'); ?>
            </nav>
        </div>
    </div>
</div>
<div class="container  pos_animated">
    <div class="row">
        <div class="header-static">
            <div class="container">
                <div class="row">
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12">
                        <div class="header-time">
                            <h2>working time</h2>

                            <p>8.00 am - 10.00 pm</p>
                        </div>
                    </div>
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12 ">
                        <div class="header-shipping">
                            <h2>free shipping & return</h2>

                            <p>On oder over $100</p>
                        </div>
                    </div>
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12 ">
                        <div class="header-money">
                            <h2>100% Money Back</h2>

                            <p>30 Days Money Back Guarantee</p>
                        </div>
                    </div>
                    <div class="f-col col-sm-6 col-md-3 col-sms-6 col-smb-12">
                        <div class="header-phone">
                            <h2>phone: 0123-456-789</h2>

                            <p>Order Online Now !</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<div>
    <div class="container pos_animated">
        <div class="row">
            <div id="header_logo" class="col-xs-12 col-sm-12 col-md-3 fx-pulse">
                <a href="/">
                    <img class="logo img-responsive"
                         src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/img/slogo.png"
                         alt="<?= Yii::app()->name ?>"
                         width="262" height="61"/>
                </a>
            </div>
            <div class="header-content col-xs-12 col-sm-12 col-md-9">
                <!-- Block search module TOP -->
                <div id="search_block_top" class="col-xs-12 col-sm-12 col-md-7">
                    <form id="searchbox" method="get" action="http://pr.home/en/search">
                        <input type="hidden" name="controller" value="search"/>
                        <input type="hidden" name="orderby" value="position"/>
                        <input type="hidden" name="orderway" value="desc"/>

                        <input class="search_query form-control" type="text" id="search_query_top" name="search_query"
                               value="Search..." value=""/>
                        <button type="submit" name="submit_search" class="btn btn-default button-search">
                            <span>Search</span>
                        </button>
                    </form>
                </div>

                <!-- /Block search module TOP --><!-- MODULE Block cart -->
                <div class="col-xs-12 col-md-5 col-sm-12">
                    <div class="shopping_cart">
                        <a href="http://pr.home/en/order" title="View my shopping cart" rel="nofollow">
                            <b>Shopping Cart</b>
                            <span class="ajax_cart_quantity unvisible">0</span>
                            <span class="ajax_cart_product_txt unvisible">Item-</span>
                            <span class="ajax_cart_product_txt_s unvisible">Items-</span>
			<span class="ajax_cart_total unvisible">
							</span>

                            <span class="ajax_cart_no_product">0 Item-</span>
                            <span class="price cart_block_total ajax_block_cart_total">$0.00</span>
                        </a>

                        <div class="cart_block block exclusive">
                            <div class="cart-arrow"></div>
                            <div class="block_content">
                                <!-- block list of products -->
                                <div class="cart_block_list">
                                    <p class="cart_block_no_products">
                                        No products
                                    </p>

                                    <div class="cart-prices">
                                        <div class="cart-prices-line first-line">
								<span class="price cart_block_shipping_cost ajax_cart_shipping_cost">
																			Free shipping!
																	</span>
								<span class="title">
									Shipping
								</span>
                                        </div>
                                        <div class="cart-prices-line last-line">
                                            <span class="price cart_block_total ajax_block_cart_total">$0.00</span>
                                            <span class="title">Total</span>
                                        </div>
                                    </div>
                                    <p class="cart-buttons">
                                        <a id="button_order_cart" class="btn btn-default button button-small"
                                           href="http://pr.home/en/order" title="Check out" rel="nofollow">
								<span>
									Check out
								</span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- .cart_block -->
                    </div>
                </div>


                <div id="layer_cart">
                    <div class="clearfix">
                        <div class="layer_cart_product col-xs-12 col-md-6">
                            <span class="cross" title="Close window"></span>

                            <h2>
                                <i class="icon-ok"></i>Product successfully added to your shopping cart
                            </h2>

                            <div class="product-image-container layer_cart_img">
                            </div>
                            <div class="layer_cart_product_info">
                                <span id="layer_cart_product_title" class="product-name"></span>
                                <span id="layer_cart_product_attributes"></span>

                                <div>
                                    <strong class="dark">Quantity</strong>
                                    <span id="layer_cart_product_quantity"></span>
                                </div>
                                <div>
                                    <strong class="dark">Total</strong>
                                    <span id="layer_cart_product_price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="layer_cart_cart col-xs-12 col-md-6">
                            <h2>
                                <!-- Plural Case [both cases are needed because page may be updated in Javascript] -->
					<span class="ajax_cart_product_txt_s  unvisible">
						There are <span class="ajax_cart_quantity">0</span> items in your cart.
					</span>
                                <!-- Singular Case [both cases are needed because page may be updated in Javascript] -->
					<span class="ajax_cart_product_txt ">
						There is 1 item in your cart.
					</span>
                            </h2>

                            <div class="layer_cart_row">
                                <strong class="dark">
                                    Total products
                                    (tax excl.)
                                </strong>
					<span class="ajax_block_products_total">
											</span>
                            </div>

                            <div class="layer_cart_row">
                                <strong class="dark">
                                    Total shipping&nbsp;(tax excl.)
                                </strong>
					<span class="ajax_cart_shipping_cost">
													Free shipping!
											</span>
                            </div>
                            <div class="layer_cart_row">
                                <strong class="dark">
                                    Total
                                    (tax excl.)
                                </strong>
					<span class="ajax_block_cart_total">
											</span>
                            </div>
                            <div class="button-container">
					<span class="continue btn btn-default button exclusive-medium" title="Continue shopping">
						<span>
							<i class="icon-chevron-left left"></i>Continue shopping
						</span>
					</span>
                                <a class="btn btn-default button button-medium" href="http://pr.home/en/order"
                                   title="Proceed to checkout" rel="nofollow">
						<span>
							Proceed to checkout<i class="icon-chevron-right right"></i>
						</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="crossseling"></div>
                </div>
                <!-- #layer_cart -->
                <div class="layer_cart_overlay"></div>

                <!-- /MODULE Block cart -->                                </div>
        </div>
    </div>
</div>
</header>
</div>


<div class="megamenu">
    <div class="bg-half"></div>
    <div class="container"><!-- Block categories module -->
        <?= cms::menuContent('main-menu'); ?>
    </div>
</div>


<div class="columns-container">
    <div id="columns" class="container">
        <div class="row">
            <div id="left_column" class="column col-xs-12 col-sm-3">
                <div class="navleft-container">
                    <div id="pt_vmegamenu" class="pt_vmegamenu">

                        <div class="vmegamenu_title"><h2><?= Yii::t('main', 'Категории') ?></h2></div>
                        <div id="main-cats-menu-ajax"><span style="text-align: center;"><img
                                    src="/images/ajax-loader.gif"></span></div>
                    </div>
                </div>
            </div>
            <div id="center_column" class="center_column col-xs-12 col-sm-9">
                <?=$content?>
            </div>
        </div>
        <!-- .row -->
    </div>
    <!-- #columns -->
</div>
<!-- .columns-container -->


<?php if ($this->id == 'site' && $this->body_class == 'home') { ?>

    <? if ($this->beginCache('site\brandsBlock', array('duration' => (YII_DEBUG ||
            (!in_array(Yii::app()->user->getRole(), array('guest', 'user')))
        ) ? 0 : 600))
    ) {
        ?>
        <? $this->widget('application.components.blocks.BrandsBlock'); ?>
        <? $this->endCache();
    } ?>
<? } ?>


<? $this->renderPartial('//layouts/_footer', array()) ?>


</div><!-- #page -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter25369892 = new Ya.Metrika({id:25369892,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/25369892" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php include_once("analyticstracking.php") ?>
<script type="text/javascript">
    var _cp = {trackerId: 2392};
    (function(d){
        var cp=d.createElement('script');cp.type='text/javascript';cp.async = true;
        cp.src='//tracker.cartprotector.com/cartprotector.js';
        var s=d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cp, s);
    })(document);
</script>
<script type="text/javascript"
src="http://consultsystems.ru/script/20667/" charset="utf-8">
</script>
</body>

<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/variables.js"></script>
<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/jquery.bxslider.js"></script>
<script type="text/javascript"
        src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/waypoints.min.js"></script>
<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/global.js"></script>
<script type="text/javascript"
        src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/custommenu.js"></script>


<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/modules/revsliderprestashop/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/modules/revsliderprestashop/jquery.themepunch.revolution.min.js"></script>



<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/jst/over.js"></script>
<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/js/jquery.lazyload.js"></script>


</html>
