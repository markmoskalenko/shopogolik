<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy
 * Date: 22.07.14
 * Time: 23:45
 */
?>
<!--[if IE]>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<? if (Utils::AppLang() == 'he') { ?>
    <STYLE type="text/css">
        html, body {
            height: 100%;
            direction: rtl;
        }
    </STYLE>
<? } ?>
<?php $cs = Yii::app()->clientScript;
$cs->scriptMap = array(
    'jquery-ui.js' => false,
    'jquery-ui.min.js' => false,
    'jquery-ui.css' => false,
    'jquery-ui.min.css' => false,
); ?>
<title><?= ($this->pageTitle) ? $this->pageTitle : cms::customContent('default-meta-title', TRUE, TRUE) ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="author" content="DanVit labs"/>
<meta name="copyright" content="DanVit labs"/>
<?
if ($this->meta_desc) {
    Yii::app()->clientScript->registerMetaTag($this->meta_desc, 'description', NULL, array('lang' => Utils::AppLang()));
} else {
    Yii::app()->clientScript->registerMetaTag(cms::customContent('default-meta-description', TRUE, TRUE), 'description', NULL, array('lang' => Utils::AppLang()));
}

if ($this->meta_keyword) {
    Yii::app()->clientScript->registerMetaTag($this->meta_keyword, 'keywords', NULL, array('lang' => Utils::AppLang()));
} else {
    Yii::app()->clientScript->registerMetaTag(cms::customContent('default-meta-keywords', TRUE, TRUE), 'keywords', NULL, array('lang' => Utils::AppLang()));
}
?>
<!-- Yii view styles -->
<link rel="stylesheet" type="text/css"
      href="<?= Yii::app()->request->baseUrl; ?>/themes/<?= $theme ?>/css/gridview/styles.css"/>
<link rel="stylesheet" type="text/css"
      href="<?= Yii::app()->request->baseUrl; ?>/themes/<?= $theme ?>/css/listview/styles.css"/>
<link rel="stylesheet" type="text/css"
      href="<?= Yii::app()->request->baseUrl; ?>/themes/<?= $theme ?>/css/pager/pager.css"/>
<!-- ================= -->
<link rel="stylesheet" type="text/css"
      href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/style.css"/>
<link rel="stylesheet" type="text/css"
      href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/jquery-ui-1.9.2.custom.css"/>
<?php if (in_array($this->id, $twoCol)) { ?>
    <link rel="stylesheet"
          href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/js/base/jquery.ui.all.css"/>
<?php } ?>
<link href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/images/favicon.ico" type="image/x-icon"
      rel="icon"/>
<link href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/images/favicon.ico" type="image/x-icon"
      rel="shortcut icon"/>









<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/autoload/highdpi.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/product_list.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockbestsellers/blockbestsellers.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockcart/blockcart.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockcategories/blockcategories.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockcurrencies/blockcurrencies.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blocklanguages/blocklanguages.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockcontact/blockcontact.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blocknewsletter/blocknewsletter.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blocksearch/blocksearch.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blocktags/blocktags.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockuserinfo/blockuserinfo.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockviewed/blockviewed.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/homeslider/homeslider.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/homefeatured/homefeatured.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/productcomments/productcomments.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/posspecialproduct/css/posspecialproduct.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/posmegamenu/css/custommenu.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/posslideshow/css/nivo-slider/nivo-slider.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/postabcategory/postabcategory.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/postabcategory/animate.delay.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/postabcategory/animate.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/posvegamenu/css/posvegamenu.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/postabcateslider/postabcateslider.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/postabcateslider/animate.delay.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/postabcateslider/animate.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/blockwishlist/blockwishlist.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/ph_simpleblog/css/ph_simpleblog.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/ph_simpleblog/css/custom.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/revsliderprestashop/rs-plugin/css/settings.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/revsliderprestashop/rs-plugin/css/static-captions.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/revsliderprestashop/rs-plugin/css/dynamic-captions.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/modules/revsliderprestashop/css/front.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl ?>/themes/<?= $theme ?>/css/global_Teal_Magenta.css"/>


<? Yii::app()->clientScript->registerScript(YII_DEBUG ? 'jquery.js' : 'jquery.min.js', CClientScript::POS_HEAD); ?>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.core.js' : 'minified/jquery.ui.core.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.widget.js' : 'minified/jquery.ui.widget.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.mouse.js' : 'minified/jquery.ui.mouse.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.button.js' : 'minified/jquery.ui.button.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.draggable.js' : 'minified/jquery.ui.draggable.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.position.js' : 'minified/jquery.ui.position.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.resizable.js' : 'minified/jquery.ui.resizable.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.dialog.js' : 'minified/jquery.ui.dialog.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.accordion.js' : 'minified/jquery.ui.accordion.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.dcjqaccordion.2.9.js' : 'minified/jquery.dcjqaccordion.2.9.min.js'?>"></script>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/<?=YII_DEBUG ? 'jquery.cookie.js' : 'jquery.cookie.min.js'?>"></script>
<script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?=$theme?>/js/ui/minified/jquery.hoverIntent.minified.js"></script>


<!-- If search pages -->
<?php if(in_array($this->id,$twoCol)) { ?>
    <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/search.js"></script>
<?php } elseif ($this->id == 'item') { ?>
    <!-- If item pages -->

    <script src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/magicscroll.js" type="text/javascript"></script>
    <script src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/magiczoomplus.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.selectable.js' : 'minified/jquery.ui.selectable.min.js'?>"></script>
    <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/item.js?v=<?=Search::$intversion?>"></script>
<?php } else {?>
    <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.spinner.js' : 'minified/jquery.ui.spinner.min.js'?>"></script>
    <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.datepicker.js' : 'minified/jquery.ui.datepicker.min.js'?>"></script>
    <!-- Для нового слайдера -->

    <script type='text/javascript' src='<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/jquery.easing.1.3.js'></script>
    <script type='text/javascript' src='<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/<?=YII_DEBUG ? 'camera.js' : 'camera.min.js'?>'></script>
<? } ?>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/main.js?v=<?=Search::$intversion?>"></script>
<!-- список категорий в поиске -->

<script type='text/javascript' src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/chosen.jquery.min.js"></script>
<!-- Скрипт для suggestion -->
<script type='text/javascript' src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/<?=YII_DEBUG ? 'jquery.autocomplete.js' : 'jquery.autocomplete.min.js'?>"></script>
<!-- Конец Скрипт для suggestion -->