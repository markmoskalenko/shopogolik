<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy
 * Date: 24.07.14
 * Time: 2:54
 */
?>
<!-- Footer -->
<div id="footer" class="footer-container">
    <div class="pos-footer-center pos_animated">
        <div class="container">
            <div class="footer-center">
                <div class="footer-static">
                    <div class="block footer-block f-col col-sm-6 col-md-3 col-sms-6 col-smb-12 fx-fadeInUp a1">
                        <div class="title_block">
                            <h4>Static block</h4>
                        </div>
                        <div class="footer-content toggle-footer">
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum &rdquo;</p>
                            <div class="author">- Danielle Sharp -</div>
                        </div>
                    </div>





                    <!-- Block myaccount module -->
                    <section class="myaccount block footer-block col-sm-6 col-md-2 col-sms-6 col-smb-12 fx-fadeInUp a2">
                        <div class="title_block"><h4>My account</h4></div>
                        <div class="block_content toggle-footer">
                            <ul class="bullet">
                                <li><a href="http://pr.home/en/order-history" title="My orders" rel="nofollow">My orders</a></li>
                                <li><a href="http://pr.home/en/order-slip" title="My credit slips" rel="nofollow">My credit slips</a></li>
                                <li><a href="http://pr.home/en/addresses" title="My addresses" rel="nofollow">My addresses</a></li>
                                <li><a href="http://pr.home/en/identity" title="Manage my personal information" rel="nofollow">My personal info</a></li>

                                <li><a href="http://pr.home/en/?mylogout" title="Sign out" rel="nofollow">Sign out</a></li>		</ul>
                        </div>
                    </section>
                    <!-- /Block myaccount module -->






                    <!-- MODULE Block footer -->
                    <section class="footer-block block col-sm-6 col-md-2 col-sms-6 col-smb-12 fx-fadeInUp a3" id="block_various_links_footer">
                        <div class="title_block"><h4>Information</h4></div>
                        <ul class="toggle-footer">
                            <li class="first_item">
                                <a href="http://pr.home/en/prices-drop" title="Specials">
                                    Specials
                                </a>
                            </li>
                            <li class="item">
                                <a href="http://pr.home/en/new-products" title="New products">
                                    New products
                                </a>
                            </li>
                            <li class="item">
                                <a href="http://pr.home/en/best-sales" title="Best sellers">
                                    Best sellers
                                </a>
                            </li>
                            <li class="item">
                                <a href="http://pr.home/en/stores" title="Our stores">
                                    Our stores
                                </a>
                            </li>
                            <li class="item">
                                <a href="http://pr.home/en/contact-us" title="Contact us">
                                    Contact us
                                </a>
                            </li>

                            <li>
                                <a href="http://pr.home/en/sitemap" title="Sitemap">
                                    Sitemap
                                </a>
                            </li>
                        </ul>

                    </section>
                    <!--	<section class="bottom-footer col-xs-12">
		<div>
			&copy; 2014 <a class="_blank" href="http://www.prestashop.com">Ecommerce software by PrestaShop™</a>
		</div>
	</section>
	-->
                    <!-- /MODULE Block footer -->

                    <div class="why-buy block  footer-block col-sm-6 col-md-2 col-sms-6 col-smb-12 fx-fadeInUp a4">
                        <div class="static-footer-title title_block ">
                            <h4>why buy from us</h4>
                        </div>
                        <ul class="toggle-footer">
                            <li><a href="#"> Orders & Returns </a></li>
                            <li><a href="#"> Search Terms </a></li>
                            <li><a href="#"> Advance Search </a></li>
                            <li><a href="#"> Affiliates </a></li>
                            <li><a href="#"> Group Sales </a></li>
                        </ul>
                    </div>


                    <!-- MODULE Block contact infos -->
                    <section id="block_contact_infos" class="footer-block block col-xs-12 col-md-3 fx-fadeInUp a5">
                        <div>
                            <div class="title_block"><h4>Taobao</h4></div>
                            <ul class="toggle-footer">
                                <li>
                                    My Company, 42 avenue des Champs Elysées
                                    75000 Paris
                                    France            	</li>
                                <li>
                                    Call us now toll free:
                                    <span>8-800-333-2336</span>
                                </li>
                                <li>
                                    Email:
                                    <span><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;%73%61%6c%65%73@%79%6f%75%72%63%6f%6d%70%61%6e%79.%63%6f%6d" >&#x73;&#x61;&#x6c;&#x65;&#x73;&#x40;&#x79;&#x6f;&#x75;&#x72;&#x63;&#x6f;&#x6d;&#x70;&#x61;&#x6e;&#x79;&#x2e;&#x63;&#x6f;&#x6d;</a></span>
                                </li>
                            </ul>
                            <div class="footer-static">
                                <div class="payment"><img src="/img/cms/payment.png" alt="" /></div>
                            </div>


                        </div>
                    </section>
                    <!-- /MODULE Block contact infos -->

                </div>


            </div>
        </div>
    </div>
    <div class="pos-footer-bottom">
        <div class="container">
            <div class="footer-bottom">
                <div class="footer-static">
                    <div class="footer-address">
                        <p>Copyright &copy; 2014 <a href="http://posthemes.com/">Posthemes.com</a>. All rights reserved.</p>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div><!-- #footer -->
