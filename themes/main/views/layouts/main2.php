<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="main.php">
* </description>
**********************************************************************************************************************/?>
<?
$twoCol=array('search','category','favorite','brand','seller');
?>
<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <? $theme=Yii::app()->theme->name;?>
  <? if (Utils::AppLang()=='he') { ?>
  <STYLE type="text/css">
    html, body {
      height:100%;
      direction:rtl;
    }
  </STYLE>
  <? } ?>
  <?php $cs=Yii::app()->clientScript;
  $cs->scriptMap=array(
    'jquery-ui.js'=>false,
    'jquery-ui.min.js'=>false,
    'jquery-ui.css'=>false,
    'jquery-ui.min.css'=>false,
  ); ?>
  <title><?=($this->pageTitle)?$this->pageTitle:cms::customContent('default-meta-title',TRUE,TRUE)?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="DanVit labs"/>
        <meta name="copyright" content="DanVit labs"/>
<?
         if ($this->meta_desc) {
            Yii::app()->clientScript->registerMetaTag($this->meta_desc, 'description', NULL,array('lang' => Utils::AppLang()));
         } else {
           Yii::app()->clientScript->registerMetaTag(cms::customContent('default-meta-description',TRUE,TRUE), 'description', NULL,array('lang' => Utils::AppLang()));
         }

         if ($this->meta_keyword) {
            Yii::app()->clientScript->registerMetaTag($this->meta_keyword, 'keywords', NULL,array('lang' => Utils::AppLang()));
         } else {
           Yii::app()->clientScript->registerMetaTag(cms::customContent('default-meta-keywords',TRUE,TRUE), 'keywords', NULL,array('lang' => Utils::AppLang()));
         }
?>
  <!-- Yii view styles -->
  <link rel="stylesheet" type="text/css"
        href="<?= Yii::app()->request->baseUrl; ?>/themes/<?=$theme?>/css/gridview/styles.css"/>
  <link rel="stylesheet" type="text/css"
        href="<?= Yii::app()->request->baseUrl; ?>/themes/<?=$theme?>/css/listview/styles.css"/>
  <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/themes/<?=$theme?>/css/pager/pager.css"/>
  <!-- ================= -->
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/css/jquery-ui-1.9.2.custom.css" />
        <?php if(in_array($this->id, $twoCol)) { ?>
        <link rel="stylesheet" href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/base/jquery.ui.all.css" />
        <?php } ?>
        <link href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/images/favicon.ico" type="image/x-icon" rel="icon" />
        <link href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/images/favicon.ico" type="image/x-icon" rel="shortcut icon" />

        <?// Yii::app()->clientScript->registerCoreScript('jquery');?>
        <? Yii::app()->clientScript->registerScript(YII_DEBUG ? 'jquery.js' : 'jquery.min.js', CClientScript::POS_HEAD); ?>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.core.js' : 'minified/jquery.ui.core.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.widget.js' : 'minified/jquery.ui.widget.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.mouse.js' : 'minified/jquery.ui.mouse.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.button.js' : 'minified/jquery.ui.button.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.draggable.js' : 'minified/jquery.ui.draggable.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.position.js' : 'minified/jquery.ui.position.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.resizable.js' : 'minified/jquery.ui.resizable.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.dialog.js' : 'minified/jquery.ui.dialog.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.accordion.js' : 'minified/jquery.ui.accordion.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.dcjqaccordion.2.9.js' : 'minified/jquery.dcjqaccordion.2.9.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/<?=YII_DEBUG ? 'jquery.cookie.js' : 'jquery.cookie.min.js'?>"></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl ?>/themes/<?=$theme?>/js/ui/minified/jquery.hoverIntent.minified.js"></script>

  <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/css/magicscroll.css" media="screen"/>
  <!-- If search pages -->
  <?php if(in_array($this->id,$twoCol)) { ?>
    <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/search.js"></script>
  <?php } elseif ($this->id == 'item') { ?>
<!-- If item pages -->
         <link href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/css/magiczoomplus.css" rel="stylesheet" type="text/css" media="screen"/>
          <script src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/magicscroll.js" type="text/javascript"></script>
          <script src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/magiczoomplus.js" type="text/javascript"></script>
          <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.selectable.js' : 'minified/jquery.ui.selectable.min.js'?>"></script>
        <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/item.js?v=<?=Search::$intversion?>"></script>
        <?php } else {?>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.spinner.js' : 'minified/jquery.ui.spinner.min.js'?>"></script>
  <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/ui/<?=YII_DEBUG ? 'jquery.ui.datepicker.js' : 'minified/jquery.ui.datepicker.min.js'?>"></script>
    <!-- Для нового слайдера -->
  <link rel='stylesheet' id='style-css'  href='<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/css/camera.css' type='text/css' media='all'>
    <script type='text/javascript' src='<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/jquery.easing.1.3.js'></script>
    <script type='text/javascript' src='<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/<?=YII_DEBUG ? 'camera.js' : 'camera.min.js'?>'></script>
        <? } ?>
        <script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/main.js?v=<?=Search::$intversion?>"></script>
<!-- список категорий в поиске -->
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/css/chosen.css" />
        <script type='text/javascript' src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/chosen.jquery.min.js"></script>
<!-- Скрипт для suggestion -->
        <script type='text/javascript' src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/<?=YII_DEBUG ? 'jquery.autocomplete.js' : 'jquery.autocomplete.min.js'?>"></script>
    <!-- Конец Скрипт для suggestion -->
</head>

<body class="<?=$this->columns?><?=Utils::TransLang()?> <?=$this->id?> <?=(!($this->id==$this->body_class))?$this->body_class:''?>">
<header>
    <div class="user-info">
        <div class="user-info-block">
            <table style="width:100%;">
                <tbody>
                <tr>
                    <td style="width:50%;">
                        <div style="text-align:left;">
                            <a class="space" href="<?=$this->createUrl('/article/about')?>"><?=Yii::t('main','О нас')?></a>
                            <a class="space" href="<?=$this->createUrl('/tools/question')?>"><?=Yii::t('main','Задать вопрос')?></a>
                            <a class="space" href="<?=$this->createUrl('/article/about')?>"><?=Yii::t('main','Помощь')?></a>
                            <a class="space" href="<?=$this->createUrl('/article',array('url'=>'contacts'))?>"><?=Yii::t('main','Контакты')?></a>
                        </div>
                        <div class="lang">
                            <?=Yii::t('main','Язык')?>:
                            <? $lang_array=explode(',',DSConfig::getVal('site_language_block'));
                            foreach ($lang_array as $val) { ?>
                                <a href="/user/setlang/<?=$val?>" class="<?=$val?><?=(Yii::app()->language==$val)?' active':''?>"><span></span></a>
                            <? } ?>
                        </div>
                        </td>
                    <td style="text-align:right;">
                        <div class="currency">
                            <?php $this->widget('application.components.blocks.currencyBlock'); ?>
                        </div>
                        <div class="">
                            <?php $this->widget('application.components.blocks.userBlock'); ?>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="user-info-block" style="height: 130px;">
            <div class="b-upperpart">
                <?=cms::customContent('main-header-logo')?>
                <div class="upperpart-description">
                    <?=cms::customContent('main-header-slogan')?>
                </div>
                <div class="upperpart-phone">
                    <div>
                        <img style="float:left;" src="<?=Yii::app()->request->baseUrl?>/themes/<?=Yii::app()->theme->name?>/images/china.png">
						<?=cms::customContent('phone-in-china')?>
                    </div>
                    <div>
                        <img style="float:left;" src="<?=Yii::app()->request->baseUrl?>/themes/<?=Yii::app()->theme->name?>/images/russia.png">
                        <?=cms::customContent('phone-in-russia')?>
                    </div>
                </div>
                <div class="upperpart-skype">
                    <img src="<?=Yii::app()->request->baseUrl?>/themes/<?=Yii::app()->theme->name?>/images/skype.png">
                    <a href="skype:skype">Skype</a>
                </div>
                <div class="upperpart-email">
                    <img src="<?=Yii::app()->request->baseUrl?>/themes/<?=Yii::app()->theme->name?>/images/mail.png">
                    <a href="mailto:mail@mail.com">E-mail</a>
                </div>
                <div class="upperpart-trash">
                    <div class="basket-block">
                        <?php
                        $this->widget('application.components.blocks.CartBlock');
                        ?>
                    </div>
                </div>
                <div class="upperpart-call"></div>
                <div class="upperpart-calc"></div>
                <div class="upperpart-cabinet"></div>
<!-------------------Search Block ----------------------------------------------------------- -->
                <div class="b-search" id="top_nav">
                    <?php $this->widget('application.components.blocks.SearchBlock'); ?>
                </div>
            </div>
<!-------------------Search Block конец------------------------------------------------------- -->
        </div>
        <div class="user-info-block">
        <div class="main-menu">
          <?=cms::menuContent('main-menu')?>
        </div>

    </div>

    </div>
</header>
<div class="wrapper">
<!------------------- Slider Begin ------------------------------------------------------------ -->
<?php if($this->id == 'site' && $this->body_class == 'home') { ?>

<?$banners=Banners::model()->findAll('enabled=1'); ?>
<section>
	<? if (isset($banners)&&(is_array($banners))&&(count($banners)>0)) { ?>
    <div style="overflow:hidden; height: 145px; width:960px; margin: 0; padding: 5px 0 5px;">
      <div id="main_camera_slider" class="camera_wrap">
                  <? foreach ($banners as $banner) { ?>
            <div data-src="<?=Yii::app()->request->baseUrl."/themes/".DSConfig::getVal("site_front_theme").$banner['img_src']?>" data-link="<?=$this->createUrl($banner['href'])?>">
              <!-- <div class="camera_caption"><? //=$banner['title']?></div> -->
            </div>
          <? } ?>
      </div>
    </div>
        <? } ?>	
</section>
<? } ?>
<!------------------- Slider end ----------------------------------------------------------- -->
<!-- User block начало
<!-- breadcrumbs -->
	<div class="clear after-head"></div>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'     => $this->breadcrumbs,
                'homeLink'  => CHtml::link(Yii::t('main','Главная'),array('/site/index'))
                ));
	 ?>


<!-- breadcrumbs -->
                
        <?php $this->widget('application.components.blocks.MessagesBlock')?>
	<div class="left-col">
<!-- ===================Categorises block begin=================================== -->
            <?  if(in_array($this->id,$twoCol) && ($this->body_class != 'cabinet')) {
               $profiler_enable=DSConfig::getVal('profiler_enable');
              if ($profiler_enable && (Yii::app()->user->getRole()=='superAdmin')) {?>
                  <div class="profiler">
              <? $this->widget('application.components.blocks.ProfilerBlock', array()); ?>
                  </div>
              <?
                  }
                $this->widget('application.components.blocks.SearchParams', array(
                  'type' => $this->id,
                  'params' => $this->params['params'],
                  'cids'=>$this->params['cids'],
                  'bids'=>$this->params['bids'],
                  'groups'=>$this->params['groups'],
                  'filters'=>$this->params['filters'],
                  'multiFilters'=>$this->params['multiFilters'],
                  'suggestions'=>$this->params['suggestions'],
                  'priceRange'=>$this->params['priceRange'],
                ));
              ?>
              <? // Вертикальный список категорий ?>
              <h4 class="page-title"><?=Yii::t('main','Категории товаров')?></h4>
              <div id="main-cats-menu-ajax"><span style="text-align: center;"><img src="/images/ajax-loader.gif"></span></div>
              <? if (Yii::app()->user->getRole()!='guest') {?>
              <h4 class="page-title"><?=Yii::t('main','Избранное')?></h4>
              <div id="favorites-menu">
                <? $this->widget('application.components.blocks.FavoritesBlock',array(
                  'adminMode'=>false,
                ));
                ?>
              </div>
               <? } ?>
              <? } elseif($this->body_class == 'cabinet') {
                $this->widget('application.components.blocks.cabinetBlock');
            } elseif($this->id == 'article') { ?>
                   <?=cms::menuContent('help-vertical-menu')?>
            <? } elseif($this->id == 'site') {
                ?>
                <? // Вертикальный список категорий ?>
                <h4 class="page-title"><?=Yii::t('main','Категории товаров')?></h4>
                <div id="main-cats-menu-ajax"><span style="text-align: center;"><img src="/images/ajax-loader.gif"></span></div>
              <? if (Yii::app()->user->getRole()!='guest') {?>
                <h4 class="page-title"><?=Yii::t('main','Избранное')?></h4>
                <div id="favorites-menu">
                  <? $this->widget('application.components.blocks.FavoritesBlock',array(
                    'adminMode'=>false,
                  ));
                  ?>
                </div>
              <? } ?>
            <?} elseif($this->id == 'seller') {
                 $this->widget('application.components.blocks.SellerBlock',array(
                   'user' => $this->params['user'],
                   'params' => $this->params['params'],
                   'cids'=>$this->params['cids'],
                   'bids'=>$this->params['bids'],
                   'groups'=>$this->params['groups'],
                   'filters'=>$this->params['filters'],
                   'multiFilters'=>$this->params['multiFilters'],
                   'suggestions'=>$this->params['suggestions'],
                   'priceRange'=>$this->params['priceRange'],
                ));
            } ?>
<!-- =====================Categorise block end==================================== -->
       
	</div>        
        <?php if($this->body_class == 'cabinet') {?>
           <div class="main-col" id="cabinet">
            <? $this->widget('application.components.blocks.UserNoticeBlock'); ?>
            <? } elseif ($this->id=='article') { ?>
            <div class="main-col" id="article">
          <? } else {?>
             <div class="main-col" id="content">
             <? } ?>
<!-- Main output -->
            <?=$content?>
<!-- End of main  -->
    </div>
<div class="clear" style="height: 15px;"></div>

<!-- Mel Brands block begin -->
        <?php if($this->id == 'site' && $this->body_class == 'home') { ?>
          <script src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/magicscroll.js" type="text/javascript"></script>
            <script type="text/javascript">
                MagicScroll.extraOptions.brandsScroll = {
                    'items': 16,
                    'width': 960,
                    'speed': 2000,
                    'height': 100,
                    'arrows': 'outside',
                    'arrows-opacity' : 20,
                    'arrows-hover-opacity' : 100,
                    'step': 4
                };
            </script>
            <div>
              <? if ($this->beginCache('site\brandsBlock',array('duration'=>(YII_DEBUG||
                (!in_array(Yii::app()->user->getRole(),array('guest','user')))
              )?0:600))) { ?>
                <? $this->widget('application.components.blocks.BrandsBlock'); ?>
                <?  $this->endCache(); } ?>
            </div>
<!-- Mel Brands block end -->

        <div class="clear"></div>
<?=cms::customContent('main')?>
        <? } ?>

<div class="footer">
    <div class="menu">
      <?=cms::menuContent('main-footer-menu')?>
    </div>
    <div class="info">
       <div class="paySystems">
         <?=cms::customContent('main-paysystems')?>
        </div>
        <div class="footerText"><?=cms::customContent('main-footertext')?></div>
        <div class="copyright">© 2014, <?=Yii::t('main','разработка и сопровождение')?>: <a href="http://dropshop.pro">Dropshop</a></div>
    </div>
    <!-- version control -->
    <div class="version"><?=Search::$version;?></div>
    <!-- end of version control -->

</div>

<!-- begin of translation dialog -->

<div id="translationDialog" style="display: none;">
<? if ((!in_array(Yii::app()->user->getRole(), array('guest','user')))) { ?>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'translationDialog',
    'options' => array(
    'title' => Yii::t('main','Редактирование перевода'),
    'autoOpen' => false,
    'modal' => true,
    'resizable' => false,
    'position'=> '[725,100]',
    'style' => '[725,100]',
    'width'    => '70%',
    'height'   => 'auto',
  ),
));
    echo $this->renderPartial('//site/translate', array());
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<? } ?>
</div>
<!-- END of translation dialog -->
<script src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/up.js" type="text/javascript"></script>
<? if (DSConfig::getVal('site_images_lazy_load')==1) { ?>
<script type="text/javascript" src="<?=Yii::app()->request->baseUrl?>/themes/<?=$theme?>/js/<?=YII_DEBUG ? 'jquery.lazyload.js' : 'jquery.lazyload.min.js'?>"></script>
<script type="text/javascript">
  $(function() {
    $("img.lazy").show().lazyload({
      effect       : "fadeIn",
      effect_speed : 500
//      skip_invisible : false
    });
  });
</script>
<? } ?>

<script> // Mel floated blocks
  var h_hght = 80; // высота шапки
  var h_mrg = 1;     // отступ когда шапка уже не видна
  $(function(){
    $(window).scroll(function(){
      var top = $(this).scrollTop();
      var elem = $('#top_nav');
      if ((top+h_mrg) < h_hght) {
        elem.css('top', (h_hght-top));
        elem.css('position', 'absolute');
        elem.css('left', '0');
      } else {
        elem.css('top', h_mrg);
        elem.css('left', '30%');
        elem.css('position', 'fixed');
      }
    });
  });
</script>

<? if((DSConfig::getVal('proxy_enabled')!=1) && (DSConfig::getVal('search_use_parser_only')==0)) {
  if (strpos(DSConfig::getKeyTaobaoSearch(false)->appkey,'usr')!==0) { ?>
  <script async="async" src="http://a.tbcdn.cn/apps/top/x/sdk.js?appkey=<?=DSConfig::getKeyTaobaoSearch(false)->appkey?>"></script>
<? }
}?>
</div>
</body>
</html>
