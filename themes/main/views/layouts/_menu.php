<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy
 * Date: 23.07.14
 * Time: 0:32
 */
?>
<table style="width:100%;">
    <tbody>
    <tr>
        <td>
            <a class="first" href="/">
                <span><?=Yii::t('main','Главная')?></span>
            </a>
        </td>
        <td>
            <a class="first" href="<?=Yii::app()->controller->createUrl('/article/about')?>">
                <span><?=Yii::t('main','О нас')?></span>
            </a>
        </td>
        <td>
            <a class="first" href="<?=Yii::app()->controller->createUrl('/article/zakaz_tovarov#1')?>">
                <span><?=Yii::t('main','Как заказать')?></span>
            </a>
        </td>
        <td>
            <a href="<?=Yii::app()->controller->createUrl('/article/index',array('url'=>'dostavka'))?>">
                <span><?=Yii::t('main','Доставка')?></span>
            </a>
        </td>
        <td>
            <a href="<?=Yii::app()->controller->createUrl('/article/index',array('url'=>'oplata'))?>">
                <span><?=Yii::t('main','Оплата')?></span>
            </a>
        </td>
        <td>
            <a href="<?=Yii::app()->controller->createUrl('/category/list')?>">
                <span class="black"><?=Yii::t('main','Каталог')?></span>
            </a>
        </td>
        <td>
            <a href="<?=Yii::app()->controller->createUrl('/brand/list')?>">
                <span class="black"><?=Yii::t('main','Бренды')?></span>
            </a>
        </td>
        <td>
            <a href="<?=Yii::app()->controller->createUrl('/tools/calculator')?>">
                <span class="black"><?=Yii::t('main','Калькулятор')?></span>
            </a>
        </td>
    </tr>
    </tbody>
</table>

<div class="nav-container visible-lg visible-md">
    <div id="pt_custommenu" class="pt_custommenu">
        <div id="pt_menu_home" class="pt_menu act">
            <div class="parentMenu"><a href="/"><span><?=Yii::app()->name?></span></a></div>
        </div>
        <div id="pt_menu_link" class="pt_menu">
            <div  class="parentMenu">
                <a href="<?=Yii::app()->controller->createUrl('/article/about')?>">
                    <span><?=Yii::t('main','О нас')?></span>
                </a>
            </div>
        </div>
        <div id="pt_menu_link" class="pt_menu">
            <div  class="parentMenu">
                <a href="<?=Yii::app()->controller->createUrl('/article/zakaz_tovarov#1')?>">
                    <span><?=Yii::t('main','Как заказать')?></span>
                </a>
            </div>
        </div>
        <div id="pt_menu_link" class="pt_menu">
            <div  class="parentMenu">
                <a href="<?=Yii::app()->controller->createUrl('/article/index',array('url'=>'dostavka'))?>">
                    <span><?=Yii::t('main','Доставка')?></span>
                </a>
            </div>
        </div>
        <div id="pt_menu_link" class="pt_menu">
            <div  class="parentMenu">
                <a href="<?=Yii::app()->controller->createUrl('/article/index',array('url'=>'oplata'))?>">
                    <span><?=Yii::t('main','Оплата')?></span>
                </a>
            </div>
        </div>
        <div id="pt_menu_link" class="pt_menu">
            <div  class="parentMenu">
                <a href="<?=Yii::app()->controller->createUrl('/category/list')?>">
                    <span><?=Yii::t('main','Каталог')?></span>
                </a>
            </div>
        </div>
        <div id="pt_menu_link" class="pt_menu">
            <div  class="parentMenu">
                <a href="<?=Yii::app()->controller->createUrl('/brand/list')?>">
                    <span><?=Yii::t('main','Бренды')?></span>
                </a>
            </div>
        </div>
        <div id="pt_menu_link" class="pt_menu">
            <div  class="parentMenu">
                <a href="<?=Yii::app()->controller->createUrl('/tools/calculator')?>">
                    <span><?=Yii::t('main','Калькулятор')?></span>
                </a>
            </div>
        </div>

    </div>
</div>