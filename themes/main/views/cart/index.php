<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title"><h4><?=$this->pageTitle?></h4></div>
<table class="cart-header">
    <tbody>
    <th style="width: 155px;"><?=Yii::t('main','Товар')?></th>
    <th style="width: 420px;"><?=Yii::t('main','Описание')?></th>
    <th><?=Yii::t('main','Параметры')?></th>
    <th style="width: 150px;"><?=Yii::t('main','Цена без скидок')?></th>
    <th style="margin-right: 25px;"><?=Yii::t('main','Сумма')?></th>
    </tbody>
</table>
<form action="<?=Yii::app()->createUrl('/cart/save')?>" method="POST" id="cart-save">
  <? if (count($userCart->cartRecords)>0) { ?>
<!-- ================================ -->
  <div id="cart-table" style="visibility:visible">
    <?php foreach($userCart->cartRecords as $k=>$item) {?>
      <? $this->widget('application.components.blocks.OrderItem', array(
        'orderItem' => $item,
        'readOnly' => FALSE,
        'imageFormat' => '_200x200.jpg',
      ));
      ?>
    <?php } ?>

      <div class="clear"></div>
<div class="cart-footer">
    <div style="width: 425px; float: left;">
    <textarea placeholder="<?=Yii::t('main','Ваш комментарий к заказу')?>" cols="188" name="comment"></textarea>
    </div>
    <div style="width: 190px; float: left;">
        <button class="ui-button update ui-widget ui-state-default ui-corner-all ui-button-text-icons ui-state-focus" type="submit" name="save" >
            <span class="ui-button-text"><?=Yii::t('main','Пересчитать')?></span>
            <span class="ui-button-icon-secondary ui-icon  ui-icon-check"/></span>
        </button>
        <button class="ui-button remove ui-widget ui-state-default ui-corner-all ui-button-text-icons ui-state-focus" onclick="clearCart();return true;" type="button" name="deleteAll">
            <span class="ui-button-text"><?=Yii::t('main','Очистить корзину')?></span>
            <span class="ui-button-icon-secondary ui-icon  ui-icon-close"/></span>
        </button>
    </div>

    <div style="float: right; width: 290px; text-align: right;">
    <div class="cart-total">
      <strong><?=Yii::t('main','Итого')?>:</strong><?=Formulas::priceWrapper($userCart->total)?>
    </div>
    <div class="cart-discont">
      <?if ($userCart->totalDiscount>0) {echo Yii::t('main','Вы экономите').': '.Formulas::priceWrapper($userCart->totalDiscount).'';} ?>
    </div><br>
    <div style="float: right;">
        <input <?=(!$userCart->allowOrder)? 'disabled="disabled"' :''?> style="margin: 5px 15px 0 0;" class="blue-btn bigger" type="submit" name="Checkout" value="<?=Yii::t('main','Заказать')?>" />
    </div>
      <div style="float: right; margin: 10px 5px 0 0;">
        <?=(!$userCart->allowOrder)? '<div>'.Yii::t('main','Нужен дозаказ на сумму').':<br><strong>'.Formulas::priceWrapper($userCart->summAddToAllowOrder).'</strong></div>':''?>
      </div>
    </div>


</div>
</div>
<!-- ================================ -->
    <? } else { ?>
<div class="warning"><?=Yii::t('main','Ваша корзина пуста!')?></div>
  <? } ?>
</form>