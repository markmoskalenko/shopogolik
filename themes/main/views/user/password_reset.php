<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="password_reset.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title">

    <?=$this->pageTitle?>

</div>
<div class="form">
    <p><?=Yii::t('main','Теперь вы можете ввести ваш новый пароль')?>:</p>
    <?=$form->render()?>    
</div>