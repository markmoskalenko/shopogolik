<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_password_reset.php">
* </description>
**********************************************************************************************************************/?>
<?php
    return array(
       'elements'=>array(
            'password'=>array(
                'type'=>'password',		
                'maxlength'=>32,
            ),
           'password2'=>array(
                'type'=>'password',		
                'maxlength'=>32,
            ),
        ),
        'buttons'=>array(
            'doGo'=>array(
                'type'=>'submit',
                'label'=>Yii::t('main','Сохранить'),
                'class'=>'blue-btn bigger'
            ),
        ),
    );