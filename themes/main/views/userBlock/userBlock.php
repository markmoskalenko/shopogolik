<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="userBlock.php">
 * </description>
 **********************************************************************************************************************/
?>


<ul id="header_links" class="fx-fadeInDown">
    <?php if ($guest) { ?>
        <li>
            <a href="<?= Yii::app()->createUrl('/user/register') ?>"><?= Yii::t('main', 'Регистрация') ?></a>
        </li>
        <li>
            <a href="<?= Yii::app()->createUrl('/user/login') ?>"><?= Yii::t('main', 'Войти') ?></a>
        </li>
    <?php } else { ?>
        <li>
            <a class="account" href="<?= Yii::app()->createUrl('/cabinet') ?>"><?= Yii::t('main', 'Привет') ?>,
                &nbsp;<span><?= $user ?></span></a>
        </li>
        <li>
            <a class="space" href="<?= Yii::app()->createUrl('/cabinet') ?>"><?= Yii::t('main', 'На счету') ?>
                &nbsp;<?= Formulas::priceWrapper(Formulas::convertCurrency(Users::getBalance(Yii::app()->user->id), DSConfig::getVal('site_currency'), DSConfig::getCurrency())) ?></a>
        </li>
        <li>
            <a class="space" href="<?= Yii::app()->createUrl('/user/logout') ?>"><?= Yii::t('main', 'Выйти') ?></a>
        </li>
    <?php } ?>
</ul>