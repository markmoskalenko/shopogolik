<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SearchBlock.php">
* </description>
**********************************************************************************************************************/?>
<?=CHtml::beginForm(array('/search/index'),'get', array('id'=>'main-search-block'))?>

<input type="text" class="query" size=100px name="query" autocomplete="off" id="query" placeholder="<?=Yii::t('main','Что вы хотите купить')?>" value="<?=$query?>">
<div class="search-loupe"></div>
<div class="search-groups">
<select class="chosen-grp" id="category" name="cid" style="display: none;">
      <option style="font-weight:bold" value="0"><?=Yii::t('main','Все категории')?></option>
	<? $useVirtualMenu=DSConfig::getVal('search_useVirtualMenu')==1; ?>
    <? if ($useVirtualMenu) { ?>
    <? foreach($cats as $cat) {
      if (isset($cat['children'])) {
        foreach($cat['children'] as $ch_cat) {
      ?>
        <? if ($ch_cat['pkid'] == $cid) {
            $sel = ' selected';
          } else {
            $sel = '';
          }?>
        <option value="<?=$ch_cat['pkid']?>"<?=$sel?>>
          <?$s='';
//          for ($i=1; $i<=$cat['level']-2; $i++) {
//           $s='&nbsp;&nbsp;&nbsp;'.$s;
//          }
          $s=$s.'&nbsp;'.$ch_cat['view_text'];
          echo $s; ?>
        </option>
        <?
        }
      }
      } ?>
  <? } else {?>
      <? foreach($cats as $cat) {
        if (isset($cat['children'])) {
          foreach($cat['children'] as $ch_cat) {
            ?>
            <?php $sel = ($ch_cat['cid'] == $cid) ? ' selected' : '';?>
            <option value="<?=$ch_cat['cid']?>"<?=$sel?>><?
              $s='';
              //          for ($i=1; $i<=$cat['level']-2; $i++) {
              //           $s='&nbsp;&nbsp;&nbsp;'.$s;
              //          }
              $s=$s.'&nbsp;'.$ch_cat['view_text'];
              echo $s;
              ?></option>
          <?
          }
        }
      } ?>
  <? } ?>
        <option style="font-weight:bold" value="seller"><?=Yii::t('main','Поиск по продавцам')?></option>

</select>
</div>
<input class="b-button" type="submit" value="">
<?=CHtml::endForm()?>

