<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="weight.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title"><h4><?=Yii::t('main','Оформление заказа')?></h4></div>
<div class="content">
    <div class="weight-desc">
      <?=cms::customContent('checkout-weight-desc')?>
    </div>
</div>
<div class="clear"></div>
<!-- TODO: Alexy - checkWeight(this) - нет такой функции -->
<form action="<?=$this->createUrl('/checkout/weight')?>" method="POST">

  <?php foreach($cart->cartRecords as $k=>$item) {?>
    <? $this->widget('application.components.blocks.OrderItem', array(
      'orderItem' => $item,
      'readOnly' => FALSE,
      'allowDelete' => FALSE,
      'imageFormat' => '_200x200.jpg',
    ));
    ?>
  <? } ?>
<input type="hidden" name="step" value="2" />
    <div class="next-btn">
<?=CHtml::submitButton(Yii::t('main','Далее'),array('class'=>'blue-btn bigger'))?>
    </div>
</form>