<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_profile.php">
* </description>
**********************************************************************************************************************/?>
<?php

return array(
  'elements' => array(
    'lastname'  => array(
      'type'      => 'text',
      'maxlength' => 128,
    ),
    'firstname' => array(
      'type'      => 'text',
      'maxlength' => 128,
    ),
    'patroname' => array(
      'type'      => 'text',
      'maxlength' => 128,
    ),
    'alias'     => array(
      'type'      => 'text',
      'maxlength' => 128,
    ),
    'd_birth'   => array(
      'type'  => 'dropdownlist',
      'items' => DSConfig::getDays(),
    ),
    'm_birth'   => array(
      'type'  => 'dropdownlist',
      'items' => DSConfig::getMounthes(),
    ),
    'y_birth'   => array(
      'type'  => 'dropdownlist',
      'items' => DSConfig::getYears(),
    ),
    /*        'sex'=>array(
                'type'=>'dropdownlist',
                //'prompt'=>'Выберите значение:',
                'items'=>array('male'=>'Мужской','female'=>'Женский'),
            ),
    */
    'skype'     => array(
      'type'      => 'text',
      'maxlength' => 128,
    ),
    'vk'     => array(
      'type'      => 'text',
      'maxlength' => 128,
    ),
    'promo_code'     => array(
      'type'      => 'text',
      'maxlength' => 128,
    ),

    'password'  => array(
      'type'      => 'password',
      'maxlength' => 128,
      'hint'      => Yii::t('main', 'Пожалуйста, введите текущий пароль, если Вы хотите сохранить изменения.')
    ),
  ),
  'buttons'  => array(
    'submit' => array(
      'type'  => 'submit',
      'label' => Yii::t('main', 'Сохранить'),
      'class' => 'blue-btn bigger'
    ),
//        'reset'=>array(
//            'type'=>'reset',
//            'label'=>Yii::t('main','Отмена'),
//            'onclick'=>'window.history.back()',
//            'class'=>'blue-btn bigger'
//        ),
  ),
);