<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="create_address.php">
* </description>
**********************************************************************************************************************/?>
<div class="blue-tabs">
    <a href="<?=Yii::app()->createUrl('/cabinet/profile')?>">
        <span><?=Yii::t('main','Личные данные')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/profile/email')?>">
        <span><?=Yii::t('main','Изменить E-mail')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/profile/password')?>">
        <span><?=Yii::t('main','Изменить пароль')?></span>
    </a>
    <div class="active-tab">
	<span><?=$this->pageTitle?></span>
    </div>  
</div>
<div class="cabinet-content">
    <span class="warning-text"><?=Yii::t('main','Символом * отмечены поля, обязательные для заполнения.')?><br/>
        <?=Yii::t('main','Обратите внимание, что текущие изменения не повлияют на адрес в уже сформированном заказе! Чтобы изменить адрес  в сформированном заказе, обратитесь в Службу поддержки')?></span>
    <div class="form">
        <?=$form?>
    </div>
</div>