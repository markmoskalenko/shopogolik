<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="address.php">
* </description>
**********************************************************************************************************************/?>
<div class="blue-tabs">
    <a href="<?=Yii::app()->createUrl('/cabinet/profile')?>">
        <span><?=Yii::t('main','Личные данные')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/profile/email')?>">
        <span><?=Yii::t('main','Изменить E-mail')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/profile/password')?>">
        <span><?=Yii::t('main','Изменить пароль')?></span>
    </a>
    <div class="active-tab">
	<span><?=$this->pageTitle?></span>
    </div>  
</div>
<div class="cabinet-content">
   
    <div class="form">
        <?//$form?>
    </div>
    <?php if(count($addresses)):?>
    <table class="cabinet-table">
        <tr>
            <th><?=Yii::t('main','Адрес')?></th>
            <th><?=Yii::t('main','Получатель')?></th>
            <th><?=Yii::t('main','Телефон')?></th>
            <th>Действие</th>
        </tr>
        <?php foreach($addresses as $address):?>
            <tr>
                <td><?=$countries[$address['country']]?>,<?if($address['region']){?>&nbsp;<?=$address['region']?>,<?}?>&nbsp;<?=$address['city']?>,<br/><?=$address['index']?>,&nbsp;<?=$address['address']?></td>
                <td><?=$address['lastname']?>&nbsp;<?=$address['firstname']?>&nbsp;<br/><?=isset($address['patroname']) ? $address['patroname'] : ''?></td>
                <td><?=$address['phone']?></td>
                <td>
                    <a href="<?=Yii::app()->createUrl('/cabinet/profile/updateaddress',array('id'=>$address['id']))?>">Редактировать</a>
                    <a href="<?=Yii::app()->createUrl('/cabinet/profile/deleteaddress',array('id'=>$address['id']))?>">Удалить</a>
                </td>
            </tr>    
        <?endforeach?>    
    </table>
    <div class="row buttons">
        <input type="hidden" id="hidden_address" value="<?=Yii::app()->createUrl('/cabinet/profile/createaddress')?>" />
        <?php echo CHtml::Button(Yii::t('main','Добавить адрес'),array('id'=>'add_address','class'=>'blue-btn bigger')); ?>
    </div>
    <?else:?>
        <p><?=Yii::t('main','Вы ещё не задали адресов')?></p><br/>
    <div class="row buttons">
        <input type="hidden" id="hidden_address" value="<?=Yii::app()->createUrl('/cabinet/profile/createaddress')?>" />
        <?php echo CHtml::Button(Yii::t('main','Добавить адрес'),array('id'=>'add_address','class'=>'blue-btn bigger')); ?>
    </div>
    <?  endif;?>
</div>