<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_address.php">
* </description>
**********************************************************************************************************************/?>
<?php

return array(        
    'elements'=>array(
        'lastname'=>array(
            'type'=>'text',
            'maxlength'=>128,
        ),
        
        'firstname'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'patroname'=>array(
            'type'=>'text',
            'maxlength'=>128,
        ),
        
        'country'=>array(
            'type'=>'dropdownlist',
            'items'=> Deliveries::getCountries(),//TODO Deliveries::model()->getCountries(),
            'prompt'=>Yii::t('main','Выбрать'),
        ),
        'index'=>array(
            'type'=>'text',            
            'maxlength'=>32,
        ),
        'city'=>array(
            'type'=>'text',
            'maxlength'=>128,
        ),
        'address'=>array(
            'type'=>'textarea',
            'rows'=>'10'
        ),
        'region'=>array(
            'type'=>'text',
            'maxlength'=>128,
        ),
        'phone'=>array(
            'type'=>'text',
            'maxlength'=>32,
            'hint'=>Yii::t('main','Телефон в формате').' 7903*******'
        )
    ),
 
    'buttons'=>array(
        'submit'=>array(
            'type'=>'submit',
            'label'=>Yii::t('main','Сохранить'),
            'class'=>'blue-btn bigger'
        ),
        'reset'=>array(
            'type'=>'reset',
            'label'=>Yii::t('main','Отмена'),
            'onclick'=>'window.history.back()',
            'class'=>'blue-btn bigger'
        ),
    ),
);