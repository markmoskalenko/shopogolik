<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="cabinetBlock.php">
* </description>
**********************************************************************************************************************/?><div class="block" id="cabinet-block">

  <h4 class="title"><?= Yii::t('main', 'Личный кабинет') ?></h4>

  <div class="block-content cabinet">
    <?php $this->widget('zii.widgets.CMenu', array(
      'items' => array(
        array('label' => Yii::t('main', 'Общая информация'), 'url' => array('/cabinet')),
        array(
          'label'   => Yii::t('main', 'Панель управления'),
          'url'     => array('/admin/main'),
          'visible' => (Yii::app()->user->checkAccess('admin/main/*')) ? true : false
        )
      )
    ));
    /*echo Yii::app()->user->role;*/
    ?>
    <h5 class="title"><?=Yii::t('main','Избранное')?></h5>
    <?php $this->widget('zii.widgets.CMenu',array(
      'items'=>array(
        array('label'=>Yii::t('main','Товары'), 'url'=>array('/cabinet/favorite/list')),
      )))
    ?>
    <h5 class="title"><?= Yii::t('main', 'Мои заказы') ?></h5>

    <?php
    $ordersByStatuses = OrdersStatuses::getAllStatusesListAndOrderCount(Yii::app()->user->id);
    $items = array();
    foreach ($ordersByStatuses as $orderByStatus) {
      if ($orderByStatus['count'] > 0) {
        $items[] = array(
          'label' => Yii::t('main', $orderByStatus['name']) . '<br/>' . $orderByStatus['count'] . ' ' . Yii::t('main', 'шт.') . ' ' . date('d.m.Y H:i', $orderByStatus['lastdate']),
          'url'   => array('/cabinet/orders/index/type/' . $orderByStatus['value'])
        );
      }
    }

    $this->widget('zii.widgets.CMenu', array(
      'items'       => $items,
      'encodeLabel' => false,
    ))
    ?>
    <h5 class="title"><?= Yii::t('main', 'Мой счёт') ?></h5>
    <?php $this->widget('zii.widgets.CMenu', array(
      'items' => array(
        array('label' => Yii::t('main', 'Выписка по счету'), 'url' => array('/cabinet/balance/index')),
        array('label' => Yii::t('main', 'Пополнить счёт'), 'url' => array('/cabinet/balance/payment')),
        array('label' => Yii::t('main', 'Информация о счете'), 'url' => array('/cabinet/balance/statement')),
        array('label' => Yii::t('main', 'Перевод средств на другой счет'), 'url' => array('/cabinet/balance/transfer')),
      )
    ))
    ?>
    <h5 class="title"><?= Yii::t('main', 'Профиль') ?></h5>
    <?php $this->widget('zii.widgets.CMenu', array(
      'items' => array(
        array('label' => Yii::t('main', 'Личные данные'), 'url' => array('/cabinet/profile/index')),
        array('label' => Yii::t('main', 'Изменить E-mail'), 'url' => array('/cabinet/profile/email')),
        array('label' => Yii::t('main', 'Изменить пароль'), 'url' => array('/cabinet/profile/password')),
        array('label' => Yii::t('main', 'Список адресов'), 'url' => array('/cabinet/profile/address')),
      )
    ))
    ?>
    <h5 class="title"><?= Yii::t('main', 'Служба поддержки') ?></h5>
    <?php $history = Yii::t('main', 'История обращений');
    $countMess = ($newAnswer) ? '(' . $newAnswer . ')' : '';
    $history = $history . $countMess;
    ?>
    <?php $this->widget('zii.widgets.CMenu', array(
      'items' => array(
        array('label' => Yii::t('main', 'Задать вопрос'), 'url' => array('/cabinet/support')),
        array('label' => Yii::t('main', 'История обращений'), 'url' => array('/cabinet/support/history')),
      )
    ))
    ?>
  </div>

</div>