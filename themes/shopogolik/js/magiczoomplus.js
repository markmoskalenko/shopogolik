(function () {
    if (window.magicJS) {
        return
    }
    var e = {version: "v2.7.2", UUID: 0, storage: {}, $uuid: function (e) {
        return e.$J_UUID || (e.$J_UUID = ++n.UUID)
    }, getStorage: function (e) {
        return n.storage[e] || (n.storage[e] = {})
    }, $F: function () {
    }, $false: function () {
        return false
    }, defined: function (e) {
        return undefined != e
    }, exists: function (e) {
        return!!e
    }, j1: function (e) {
        if (!n.defined(e)) {
            return false
        }
        if (e.$J_TYPE) {
            return e.$J_TYPE
        }
        if (!!e.nodeType) {
            if (1 == e.nodeType) {
                return"element"
            }
            if (3 == e.nodeType) {
                return"textnode"
            }
        }
        if (e.length && e.item) {
            return"collection"
        }
        if (e.length && e.callee) {
            return"arguments"
        }
        if ((e instanceof window.Object || e instanceof window.Function) && e.constructor === n.Class) {
            return"class"
        }
        if (e instanceof window.Array) {
            return"array"
        }
        if (e instanceof window.Function) {
            return"function"
        }
        if (e instanceof window.String) {
            return"string"
        }
        if (n.j21.trident) {
            if (n.defined(e.cancelBubble)) {
                return"event"
            }
        } else {
            if (e === window.event || e.constructor == window.Event || e.constructor == window.MouseEvent || e.constructor == window.UIEvent || e.constructor == window.KeyboardEvent || e.constructor == window.KeyEvent) {
                return"event"
            }
        }
        if (e instanceof window.Date) {
            return"date"
        }
        if (e instanceof window.RegExp) {
            return"regexp"
        }
        if (e === window) {
            return"window"
        }
        if (e === document) {
            return"document"
        }
        return typeof e
    }, extend: function (e, t) {
        if (!(e instanceof window.Array)) {
            e = [e]
        }
        for (var r = 0, i = e.length; r < i; r++) {
            if (!n.defined(e)) {
                continue
            }
            for (var s in t || {}) {
                try {
                    e[r][s] = t[s]
                } catch (o) {
                }
            }
        }
        return e[0]
    }, implement: function (e, t) {
        if (!(e instanceof window.Array)) {
            e = [e]
        }
        for (var r = 0, i = e.length; r < i; r++) {
            if (!n.defined(e[r])) {
                continue
            }
            if (!e[r].prototype) {
                continue
            }
            for (var s in t || {}) {
                if (!e[r].prototype[s]) {
                    e[r].prototype[s] = t[s]
                }
            }
        }
        return e[0]
    }, nativize: function (e, t) {
        if (!n.defined(e)) {
            return e
        }
        for (var r in t || {}) {
            if (!e[r]) {
                e[r] = t[r]
            }
        }
        return e
    }, $try: function () {
        for (var e = 0, t = arguments.length; e < t; e++) {
            try {
                return arguments[e]()
            } catch (n) {
            }
        }
        return null
    }, $A: function (e) {
        if (!n.defined(e)) {
            return $mjs([])
        }
        if (e.toArray) {
            return $mjs(e.toArray())
        }
        if (e.item) {
            var t = e.length || 0, r = new Array(t);
            while (t--) {
                r[t] = e[t]
            }
            return $mjs(r)
        }
        return $mjs(Array.prototype.slice.call(e))
    }, now: function () {
        return(new Date).getTime()
    }, detach: function (e) {
        var t;
        switch (n.j1(e)) {
            case"object":
                t = {};
                for (var r in e) {
                    t[r] = n.detach(e[r])
                }
                break;
            case"array":
                t = [];
                for (var i = 0, s = e.length; i < s; i++) {
                    t[i] = n.detach(e[i])
                }
                break;
            default:
                return e
        }
        return n.$(t)
    }, $: function (e) {
        if (!n.defined(e)) {
            return null
        }
        if (e.$J_EXTENDED) {
            return e
        }
        switch (n.j1(e)) {
            case"array":
                e = n.nativize(e, n.extend(n.Array, {$J_EXTENDED: n.$F}));
                e.j14 = e.forEach;
                return e;
                break;
            case"string":
                var t = document.getElementById(e);
                if (n.defined(t)) {
                    return n.$(t)
                }
                return null;
                break;
            case"window":
            case"document":
                n.$uuid(e);
                e = n.extend(e, n.Doc);
                break;
            case"element":
                n.$uuid(e);
                e = n.extend(e, n.Element);
                break;
            case"event":
                e = n.extend(e, n.Event);
                break;
            case"textnode":
                return e;
                break;
            case"function":
            case"array":
            case"date":
            default:
                break
        }
        return n.extend(e, {$J_EXTENDED: n.$F})
    }, $new: function (e, t, r) {
        return $mjs(n.doc.createElement(e)).setProps(t || {}).j6(r || {})
    }, addCSS: function (e) {
        if (document.styleSheets && document.styleSheets.length) {
            document.styleSheets[0].insertRule(e, 0)
        } else {
            var t = $mjs(document.createElement("style"));
            t.update(e);
            document.getElementsByTagName("head")[0].appendChild(t)
        }
    }};
    var n = e;
    window.magicJS = e;
    window.$mjs = e.$;
    n.Array = {$J_TYPE: "array", indexOf: function (e, t) {
        var n = this.length;
        for (var r = this.length, i = t < 0 ? Math.max(0, r + t) : t || 0; i < r; i++) {
            if (this[i] === e) {
                return i
            }
        }
        return-1
    }, contains: function (e, t) {
        return this.indexOf(e, t) != -1
    }, forEach: function (e, t) {
        for (var n = 0, r = this.length; n < r; n++) {
            if (n in this) {
                e.call(t, this[n], n, this)
            }
        }
    }, filter: function (e, t) {
        var n = [];
        for (var r = 0, i = this.length; r < i; r++) {
            if (r in this) {
                var s = this[r];
                if (e.call(t, this[r], r, this)) {
                    n.push(s)
                }
            }
        }
        return n
    }, map: function (e, t) {
        var n = [];
        for (var r = 0, i = this.length; r < i; r++) {
            if (r in this) {
                n[r] = e.call(t, this[r], r, this)
            }
        }
        return n
    }};
    n.implement(String, {$J_TYPE: "string", j26: function () {
        return this.replace(/^\s+|\s+$/g, "")
    }, eq: function (e, t) {
        return t || false ? this.toString() === e.toString() : this.toLowerCase().toString() === e.toLowerCase().toString()
    }, j22: function () {
        return this.replace(/-\D/g, function (e) {
            return e.charAt(1).toUpperCase()
        })
    }, dashize: function () {
        return this.replace(/[A-Z]/g, function (e) {
            return"-" + e.charAt(0).toLowerCase()
        })
    }, j17: function (e) {
        return parseInt(this, e || 10)
    }, toFloat: function () {
        return parseFloat(this)
    }, j18: function () {
        return!this.replace(/true/i, "").j26()
    }, has: function (e, t) {
        t = t || "";
        return(t + this + t).indexOf(t + e + t) > -1
    }});
    e.implement(Function, {$J_TYPE: "function", j24: function () {
        var e = n.$A(arguments), t = this, r = e.shift();
        return function () {
            return t.apply(r || null, e.concat(n.$A(arguments)))
        }
    }, j16: function () {
        var e = n.$A(arguments), t = this, r = e.shift();
        return function (n) {
            return t.apply(r || null, $mjs([n || window.event]).concat(e))
        }
    }, j27: function () {
        var e = n.$A(arguments), t = this, r = e.shift();
        return window.setTimeout(function () {
            return t.apply(t, e)
        }, r || 0)
    }, j28: function () {
        var e = n.$A(arguments), t = this;
        return function () {
            return t.j27.apply(t, e)
        }
    }, interval: function () {
        var e = n.$A(arguments), t = this, r = e.shift();
        return window.setInterval(function () {
            return t.apply(t, e)
        }, r || 0)
    }});
    var r = navigator.userAgent.toLowerCase();
    n.j21 = {features: {xpath: !!document.evaluate, air: !!window.runtime, query: !!document.querySelector}, touchScreen: function () {
        return"ontouchstart"in window || window.DocumentTouch && document instanceof DocumentTouch
    }(), mobile: r.match(/android|tablet|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(j21|link)|vodafone|wap|windows (ce|phone)|xda|xiino/) ? true : false, engine: window.opera ? "presto" : !!window.ActiveXObject ? "trident" : undefined != document.getBoxObjectFor || null != window.mozInnerScreenY ? "gecko" : null != window.WebKitPoint || !navigator.taintEnabled ? "webkit" : "unknown", version: "", ieMode: 0, platform: r.match(/ip(?:ad|od|hone)/) ? "ios" : (r.match(/(?:webos|android)/) || navigator.platform.match(/mac|win|linux/i) || ["other"])[0].toLowerCase(), backCompat: document.compatMode && "backcompat" == document.compatMode.toLowerCase(), getDoc: function () {
        return document.compatMode && "backcompat" == document.compatMode.toLowerCase() ? document.body : document.documentElement
    }, requestAnimationFrame: window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || undefined, cancelAnimationFrame: window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || window.msCancelAnimationFrame || window.webkitCancelRequestAnimationFrame || undefined, ready: false, onready: function () {
        if (n.j21.ready) {
            return
        }
        n.j21.ready = true;
        n.body = $mjs(document.body);
        n.win = $mjs(window);
        (function () {
            n.j21.css3Transformations = {capable: false, prefix: ""};
            if (typeof document.body.style.transform !== "undefined") {
                n.j21.css3Transformations.capable = true
            } else {
                var e = "Webkit Moz O ms Khtml".split(" ");
                for (var t = 0, r = e.length; t < r; t++) {
                    n.j21.css3Transformations.prefix = e[t];
                    if (typeof document.body.style[n.j21.css3Transformations.prefix + "Transform"] !== "undefined") {
                        n.j21.css3Transformations.capable = true;
                        break
                    }
                }
            }
        })();
        (function () {
            n.j21.css3Animation = {capable: false, prefix: ""};
            if (typeof document.body.style.animationName !== "undefined") {
                n.j21.css3Animation.capable = true
            } else {
                var e = "Webkit Moz O ms Khtml".split(" ");
                for (var t = 0, r = e.length; t < r; t++) {
                    n.j21.css3Animation.prefix = e[t];
                    if (typeof document.body.style[n.j21.css3Animation.prefix + "AnimationName"] !== "undefined") {
                        n.j21.css3Animation.capable = true;
                        break
                    }
                }
            }
        })();
        $mjs(document).raiseEvent("domready")
    }};
    (function () {
        function e() {
            return!!arguments.callee.caller
        }

        n.j21.version = "presto" == n.j21.engine ? !!document.head ? 270 : !!window.applicationCache ? 260 : !!window.localStorage ? 250 : n.j21.features.query ? 220 : e() ? 211 : document.getElementsByClassName ? 210 : 200 : "trident" == n.j21.engine ? !!(window.msPerformance || window.performance) ? 900 : !!(window.XMLHttpRequest && window.postMessage) ? 6 : window.XMLHttpRequest ? 5 : 4 : "webkit" == n.j21.engine ? n.j21.features.xpath ? n.j21.features.query ? 525 : 420 : 419 : "gecko" == n.j21.engine ? !!document.head ? 200 : !!document.readyState ? 192 : !!window.localStorage ? 191 : document.getElementsByClassName ? 190 : 181 : "";
        n.j21[n.j21.engine] = n.j21[n.j21.engine + n.j21.version] = true;
        if (window.chrome) {
            n.j21.chrome = true
        }
        n.j21.ieMode = !n.j21.trident ? 0 : document.documentMode ? document.documentMode : function () {
            var e = 0;
            if (n.j21.backCompat) {
                return 5
            }
            switch (n.j21.version) {
                case 4:
                    e = 6;
                    break;
                case 5:
                    e = 7;
                    break;
                case 6:
                    e = 8;
                    break;
                case 900:
                    e = 9;
                    break
            }
            return e
        }()
    })();
    (function () {
        n.j21.fullScreen = {capable: false, enabled: function () {
            return false
        }, request: function () {
        }, cancel: function () {
        }, changeEventName: "", errorEventName: "", prefix: ""};
        if (typeof document.cancelFullScreen != "undefined") {
            n.j21.fullScreen.capable = true
        } else {
            var e = "webkit moz o ms khtml".split(" ");
            for (var t = 0, r = e.length; t < r; t++) {
                n.j21.fullScreen.prefix = e[t];
                if (typeof document[n.j21.fullScreen.prefix + "CancelFullScreen"] != "undefined") {
                    n.j21.fullScreen.capable = true;
                    break
                }
            }
        }
        if (n.j21.fullScreen.capable) {
            n.j21.fullScreen.changeEventName = n.j21.fullScreen.prefix + "fullscreenchange";
            n.j21.fullScreen.errorEventName = n.j21.fullScreen.prefix + "fullscreenerror";
            n.j21.fullScreen.enabled = function () {
                switch (this.prefix) {
                    case"":
                        return document.fullScreen;
                    case"webkit":
                        return document.webkitIsFullScreen;
                    default:
                        return document[this.prefix + "FullScreen"]
                }
            };
            n.j21.fullScreen.request = function (e) {
                return this.prefix === "" ? e.requestFullScreen() : e[this.prefix + "RequestFullScreen"]()
            };
            n.j21.fullScreen.cancel = function (e) {
                return this.prefix === "" ? document.cancelFullScreen() : document[this.prefix + "CancelFullScreen"]()
            }
        }
    })();
    n.Element = {j13: function (e) {
        return this.className.has(e, " ")
    }, j2: function (e) {
        if (e && !this.j13(e)) {
            this.className += (this.className ? " " : "") + e
        }
        return this
    }, j3: function (e) {
        e = e || ".*";
        this.className = this.className.replace(new RegExp("(^|\\s)" + e + "(?:\\s|$)"), "$1").j26();
        return this
    }, j4: function (e) {
        return this.j13(e) ? this.j3(e) : this.j2(e)
    }, j5: function (e) {
        e = e == "float" && this.currentStyle ? "styleFloat" : e.j22();
        var t = null, r = null;
        if (this.currentStyle) {
            t = this.currentStyle[e]
        } else {
            if (document.defaultView && document.defaultView.getComputedStyle) {
                r = document.defaultView.getComputedStyle(this, null);
                t = r ? r.getPropertyValue([e.dashize()]) : null
            }
        }
        if (!t) {
            t = this.style[e]
        }
        if ("opacity" == e) {
            return n.defined(t) ? parseFloat(t) : 1
        }
        if (/^(border(Top|Bottom|Left|Right)Width)|((padding|margin)(Top|Bottom|Left|Right))$/.test(e)) {
            t = parseInt(t) ? t : "0px"
        }
        return"auto" == t ? null : t
    }, j6Prop: function (e, t) {
        try {
            if ("opacity" == e) {
                this.j23(t);
                return this
            } else {
                if ("float" == e) {
                    this.style["undefined" === typeof this.style.styleFloat ? "cssFloat" : "styleFloat"] = t;
                    return this
                } else {
                    if (n.j21.css3Transformations && /transform/.test(e)) {
                    }
                }
            }
            this.style[e.j22()] = t + ("number" == n.j1(t) && !$mjs(["zIndex", "zoom"]).contains(e.j22()) ? "px" : "")
        } catch (r) {
        }
        return this
    }, j6: function (e) {
        for (var t in e) {
            this.j6Prop(t, e[t])
        }
        return this
    }, j19s: function () {
        var e = {};
        n.$A(arguments).j14(function (t) {
            e[t] = this.j5(t)
        }, this);
        return e
    }, j23: function (e, t) {
        t = t || false;
        e = parseFloat(e);
        if (t) {
            if (e == 0) {
                if ("hidden" != this.style.visibility) {
                    this.style.visibility = "hidden"
                }
            } else {
                if ("visible" != this.style.visibility) {
                    this.style.visibility = "visible"
                }
            }
        }
        if (n.j21.trident) {
            if (!this.currentStyle || !this.currentStyle.hasLayout) {
                this.style.zoom = 1
            }
            try {
                var r = this.filters.item("DXImageTransform.Microsoft.Alpha");
                r.enabled = 1 != e;
                r.opacity = e * 100
            } catch (i) {
                this.style.filter += 1 == e ? "" : "progid:DXImageTransform.Microsoft.Alpha(enabled=true,opacity=" + e * 100 + ")"
            }
        }
        this.style.opacity = e;
        return this
    }, setProps: function (e) {
        for (var t in e) {
            this.setAttribute(t, "" + e[t])
        }
        return this
    }, hide: function () {
        return this.j6({display: "none", visibility: "hidden"})
    }, show: function () {
        return this.j6({display: "block", visibility: "visible"})
    }, j7: function () {
        return{width: this.offsetWidth, height: this.offsetHeight}
    }, j10: function () {
        return{top: this.scrollTop, left: this.scrollLeft}
    }, j11: function () {
        var e = this, t = {top: 0, left: 0};
        do {
            t.left += e.scrollLeft || 0;
            t.top += e.scrollTop || 0;
            e = e.parentNode
        } while (e);
        return t
    }, j8: function () {
        if (n.defined(document.documentElement.getBoundingClientRect)) {
            var e = this.getBoundingClientRect(), r = $mjs(document).j10(), i = n.j21.getDoc();
            return{top: e.top + r.y - i.clientTop, left: e.left + r.x - i.clientLeft}
        }
        var s = this, o = t = 0;
        do {
            o += s.offsetLeft || 0;
            t += s.offsetTop || 0;
            s = s.offsetParent
        } while (s && !/^(?:body|html)$/i.test(s.tagName));
        return{top: t, left: o}
    }, j9: function () {
        var e = this.j8();
        var t = this.j7();
        return{top: e.top, bottom: e.top + t.height, left: e.left, right: e.left + t.width}
    }, changeContent: function (e) {
        try {
            this.innerHTML = e
        } catch (t) {
            this.innerText = e
        }
        return this
    }, j33: function () {
        return this.parentNode ? this.parentNode.removeChild(this) : this
    }, kill: function () {
        n.$A(this.childNodes).j14(function (e) {
            if (3 == e.nodeType || 8 == e.nodeType) {
                return
            }
            $mjs(e).kill()
        });
        this.j33();
        this.je3();
        if (this.$J_UUID) {
            n.storage[this.$J_UUID] = null;
            delete n.storage[this.$J_UUID]
        }
        return null
    }, append: function (e, t) {
        t = t || "bottom";
        var n = this.firstChild;
        "top" == t && n ? this.insertBefore(e, n) : this.appendChild(e);
        return this
    }, j32: function (e, t) {
        var n = $mjs(e).append(this, t);
        return this
    }, enclose: function (e) {
        this.append(e.parentNode.replaceChild(this, e));
        return this
    }, hasChild: function (e) {
        if (!(e = $mjs(e))) {
            return false
        }
        return this == e ? false : this.contains && !n.j21.webkit419 ? this.contains(e) : this.compareDocumentPosition ? !!(this.compareDocumentPosition(e) & 16) : n.$A(this.byTag(e.tagName)).contains(e)
    }};
    n.Element.j19 = n.Element.j5;
    n.Element.j20 = n.Element.j6;
    if (!window.Element) {
        window.Element = n.$F;
        if (n.j21.engine.webkit) {
            window.document.createElement("iframe")
        }
        window.Element.prototype = n.j21.engine.webkit ? window["[[DOMElement.prototype]]"] : {}
    }
    n.implement(window.Element, {$J_TYPE: "element"});
    n.Doc = {j7: function () {
        if (n.j21.presto925 || n.j21.webkit419) {
            return{width: window.innerWidth, height: window.innerHeight}
        }
        return{width: n.j21.getDoc().clientWidth, height: n.j21.getDoc().clientHeight}
    }, j10: function () {
        return{x: window.pageXOffset || n.j21.getDoc().scrollLeft, y: window.pageYOffset || n.j21.getDoc().scrollTop}
    }, j12: function () {
        var e = this.j7();
        return{width: Math.max(n.j21.getDoc().scrollWidth, e.width), height: Math.max(n.j21.getDoc().scrollHeight, e.height)}
    }};
    n.extend(document, {$J_TYPE: "document"});
    n.extend(window, {$J_TYPE: "window"});
    n.extend([n.Element, n.Doc], {j29: function (e, t) {
        var r = n.getStorage(this.$J_UUID), i = r[e];
        if (undefined != t && undefined == i) {
            i = r[e] = t
        }
        return n.defined(i) ? i : null
    }, j30: function (e, t) {
        var r = n.getStorage(this.$J_UUID);
        r[e] = t;
        return this
    }, j31: function (e) {
        var t = n.getStorage(this.$J_UUID);
        delete t[e];
        return this
    }});
    if (!(window.HTMLElement && window.HTMLElement.prototype && window.HTMLElement.prototype.getElementsByClassName)) {
        n.extend([n.Element, n.Doc], {getElementsByClassName: function (e) {
            return n.$A(this.getElementsByTagName("*")).filter(function (t) {
                try {
                    return 1 == t.nodeType && t.className.has(e, " ")
                } catch (n) {
                }
            })
        }})
    }
    n.extend([n.Element, n.Doc], {byClass: function () {
        return this.getElementsByClassName(arguments[0])
    }, byTag: function () {
        return this.getElementsByTagName(arguments[0])
    }});
    if (n.j21.fullScreen.capable) {
        n.Element.requestFullScreen = function () {
            n.j21.fullScreen.request(this)
        }
    }
    n.Event = {$J_TYPE: "event", stop: function () {
        if (this.stopPropagation) {
            this.stopPropagation()
        } else {
            this.cancelBubble = true
        }
        if (this.preventDefault) {
            this.preventDefault()
        } else {
            this.returnValue = false
        }
        return this
    }, j15: function () {
        var e, t;
        e = /touch/i.test(this.type) ? this.changedTouches[0] : this;
        return!n.defined(e) ? {x: 0, y: 0} : {x: e.pageX || e.clientX + n.j21.getDoc().scrollLeft, y: e.pageY || e.clientY + n.j21.getDoc().scrollTop}
    }, getTarget: function () {
        var e = this.target || this.srcElement;
        while (e && 3 == e.nodeType) {
            e = e.parentNode
        }
        return e
    }, getRelated: function () {
        var e = null;
        switch (this.type) {
            case"mouseover":
                e = this.relatedTarget || this.fromElement;
                break;
            case"mouseout":
                e = this.relatedTarget || this.toElement;
                break;
            default:
                return e
        }
        try {
            while (e && 3 == e.nodeType) {
                e = e.parentNode
            }
        } catch (t) {
            e = null
        }
        return e
    }, getButton: function () {
        if (!this.which && this.button !== undefined) {
            return this.button & 1 ? 1 : this.button & 2 ? 3 : this.button & 4 ? 2 : 0
        }
        return this.which
    }};
    n._event_add_ = "addEventListener";
    n._event_del_ = "removeEventListener";
    n._event_prefix_ = "";
    if (!document.addEventListener) {
        n._event_add_ = "attachEvent";
        n._event_del_ = "detachEvent";
        n._event_prefix_ = "on"
    }
    n.extend([n.Element, n.Doc], {je1: function (e, t) {
        var r = "domready" == e ? false : true, i = this.j29("events", {});
        i[e] = i[e] || {};
        if (i[e].hasOwnProperty(t.$J_EUID)) {
            return this
        }
        if (!t.$J_EUID) {
            t.$J_EUID = Math.floor(Math.random() * n.now())
        }
        var s = this, o = function (e) {
            return t.call(s)
        };
        if ("domready" == e) {
            if (n.j21.ready) {
                t.call(this);
                return this
            }
        }
        if (r) {
            o = function (e) {
                e = n.extend(e || window.e, {$J_TYPE: "event"});
                return t.call(s, $mjs(e))
            };
            this[n._event_add_](n._event_prefix_ + e, o, false)
        }
        i[e][t.$J_EUID] = o;
        return this
    }, je2: function (e) {
        var t = "domready" == e ? false : true, r = this.j29("events");
        if (!r || !r[e]) {
            return this
        }
        var i = r[e], s = arguments[1] || null;
        if (e && !s) {
            for (var o in i) {
                if (!i.hasOwnProperty(o)) {
                    continue
                }
                this.je2(e, o)
            }
            return this
        }
        s = "function" == n.j1(s) ? s.$J_EUID : s;
        if (!i.hasOwnProperty(s)) {
            return this
        }
        if ("domready" == e) {
            t = false
        }
        if (t) {
            this[n._event_del_](n._event_prefix_ + e, i[s], false)
        }
        delete i[s];
        return this
    }, raiseEvent: function (e, t) {
        var n = "domready" == e ? false : true, r = this, i;
        if (!n) {
            var s = this.j29("events");
            if (!s || !s[e]) {
                return this
            }
            var o = s[e];
            for (var u in o) {
                if (!o.hasOwnProperty(u)) {
                    continue
                }
                o[u].call(this)
            }
            return this
        }
        if (r === document && document.createEvent && !r.dispatchEvent) {
            r = document.documentElement
        }
        if (document.createEvent) {
            i = document.createEvent(e);
            i.initEvent(t, true, true)
        } else {
            i = document.createEventObject();
            i.eventType = e
        }
        if (document.createEvent) {
            r.dispatchEvent(i)
        } else {
            r.fireEvent("on" + t, i)
        }
        return i
    }, je3: function () {
        var e = this.j29("events");
        if (!e) {
            return this
        }
        for (var t in e) {
            this.je2(t)
        }
        this.j31("events");
        return this
    }});
    (function () {
        if ("complete" === document.readyState) {
            return n.j21.onready.j27(1)
        }
        if (n.j21.webkit && n.j21.version < 420) {
            (function () {
                $mjs(["loaded", "complete"]).contains(document.readyState) ? n.j21.onready() : arguments.callee.j27(50)
            })()
        } else {
            if (n.j21.trident && n.j21.ieMode < 9 && window == top) {
                (function () {
                    n.$try(function () {
                        n.j21.getDoc().doScroll("left");
                        return true
                    }) ? n.j21.onready() : arguments.callee.j27(50)
                })()
            } else {
                $mjs(document).je1("DOMContentLoaded", n.j21.onready);
                $mjs(window).je1("load", n.j21.onready)
            }
        }
    })();
    n.Class = function () {
        var e = null, t = n.$A(arguments);
        if ("class" == n.j1(t[0])) {
            e = t.shift()
        }
        var r = function () {
            for (var e in this) {
                this[e] = n.detach(this[e])
            }
            if (this.constructor.$parent) {
                this.$parent = {};
                var t = this.constructor.$parent;
                for (var r in t) {
                    var i = t[r];
                    switch (n.j1(i)) {
                        case"function":
                            this.$parent[r] = n.Class.wrap(this, i);
                            break;
                        case"object":
                            this.$parent[r] = n.detach(i);
                            break;
                        case"array":
                            this.$parent[r] = n.detach(i);
                            break
                    }
                }
            }
            var s = this.init ? this.init.apply(this, arguments) : this;
            delete this.caller;
            return s
        };
        if (!r.prototype.init) {
            r.prototype.init = n.$F
        }
        if (e) {
            var i = function () {
            };
            i.prototype = e.prototype;
            r.prototype = new i;
            r.$parent = {};
            for (var s in e.prototype) {
                r.$parent[s] = e.prototype[s]
            }
        } else {
            r.$parent = null
        }
        r.constructor = n.Class;
        r.prototype.constructor = r;
        n.extend(r.prototype, t[0]);
        n.extend(r, {$J_TYPE: "class"});
        return r
    };
    e.Class.wrap = function (e, t) {
        return function () {
            var n = this.caller;
            var r = t.apply(e, arguments);
            return r
        }
    };
    n.win = $mjs(window);
    n.doc = $mjs(document)
})();
(function (e) {
    if (!e) {
        throw"MagicJS not found";
        return
    }
    if (e.FX) {
        return
    }
    var t = e.$;
    e.FX = new e.Class({options: {fps: 60, duration: 500, transition: function (e) {
        return-(Math.cos(Math.PI * e) - 1) / 2
    }, onStart: e.$F, onComplete: e.$F, onBeforeRender: e.$F, onAfterRender: e.$F, forceAnimation: false, roundCss: true}, styles: null, init: function (n, r) {
        this.el = t(n);
        this.options = e.extend(this.options, r);
        this.timer = false
    }, start: function (t) {
        this.styles = t;
        this.state = 0;
        this.curFrame = 0;
        this.startTime = e.now();
        this.finishTime = this.startTime + this.options.duration;
        this.loopBind = this.loop.j24(this);
        this.options.onStart.call();
        if (!this.options.forceAnimation && e.j21.requestAnimationFrame) {
            this.timer = e.j21.requestAnimationFrame.call(window, this.loopBind)
        } else {
            this.timer = this.loop.j24(this).interval(Math.round(1e3 / this.options.fps))
        }
        return this
    }, stopAnimation: function () {
        if (this.timer) {
            if (!this.options.forceAnimation && e.j21.requestAnimationFrame && e.j21.cancelAnimationFrame) {
                e.j21.cancelAnimationFrame.call(window, this.timer)
            } else {
                clearInterval(this.timer)
            }
            this.timer = false
        }
    }, stop: function (t) {
        t = e.defined(t) ? t : false;
        this.stopAnimation();
        if (t) {
            this.render(1);
            this.options.onComplete.j27(10)
        }
        return this
    }, calc: function (e, t, n) {
        return(t - e) * n + e
    }, loop: function () {
        var t = e.now();
        if (t >= this.finishTime) {
            this.stopAnimation();
            this.render(1);
            this.options.onComplete.j27(10);
            return this
        }
        var n = this.options.transition((t - this.startTime) / this.options.duration);
        if (!this.options.forceAnimation && e.j21.requestAnimationFrame) {
            this.timer = e.j21.requestAnimationFrame.call(window, this.loopBind)
        }
        this.render(n)
    }, render: function (e) {
        var t = {};
        for (var n in this.styles) {
            if ("opacity" === n) {
                t[n] = Math.round(this.calc(this.styles[n][0], this.styles[n][1], e) * 100) / 100
            } else {
                t[n] = this.calc(this.styles[n][0], this.styles[n][1], e);
                if (this.options.roundCss) {
                    t[n] = Math.round(t[n])
                }
            }
        }
        this.options.onBeforeRender(t);
        this.set(t);
        this.options.onAfterRender(t)
    }, set: function (e) {
        return this.el.j6(e)
    }});
    e.FX.Transition = {linear: function (e) {
        return e
    }, sineIn: function (e) {
        return-(Math.cos(Math.PI * e) - 1) / 2
    }, sineOut: function (t) {
        return 1 - e.FX.Transition.sineIn(1 - t)
    }, expoIn: function (e) {
        return Math.pow(2, 8 * (e - 1))
    }, expoOut: function (t) {
        return 1 - e.FX.Transition.expoIn(1 - t)
    }, quadIn: function (e) {
        return Math.pow(e, 2)
    }, quadOut: function (t) {
        return 1 - e.FX.Transition.quadIn(1 - t)
    }, cubicIn: function (e) {
        return Math.pow(e, 3)
    }, cubicOut: function (t) {
        return 1 - e.FX.Transition.cubicIn(1 - t)
    }, backIn: function (e, t) {
        t = t || 1.618;
        return Math.pow(e, 2) * ((t + 1) * e - t)
    }, backOut: function (t, n) {
        return 1 - e.FX.Transition.backIn(1 - t)
    }, elasticIn: function (e, t) {
        t = t || [];
        return Math.pow(2, 10 * --e) * Math.cos(20 * e * Math.PI * (t[0] || 1) / 3)
    }, elasticOut: function (t, n) {
        return 1 - e.FX.Transition.elasticIn(1 - t, n)
    }, bounceIn: function (e) {
        for (var t = 0, n = 1; 1; t += n, n /= 2) {
            if (e >= (7 - 4 * t) / 11) {
                return n * n - Math.pow((11 - 6 * t - 11 * e) / 4, 2)
            }
        }
    }, bounceOut: function (t) {
        return 1 - e.FX.Transition.bounceIn(1 - t)
    }, none: function (e) {
        return 0
    }}
})(magicJS);
(function (e) {
    if (!e) {
        throw"MagicJS not found";
        return
    }
    if (!e.FX) {
        throw"MagicJS.FX not found";
        return
    }
    if (e.FX.Slide) {
        return
    }
    var t = e.$;
    e.FX.Slide = new e.Class(e.FX, {options: {mode: "vertical"}, init: function (t, n) {
        this.el = $mjs(t);
        this.options = e.extend(this.$parent.options, this.options);
        this.$parent.init(t, n);
        this.wrapper = this.el.j29("slide:wrapper");
        this.wrapper = this.wrapper || e.$new("DIV").j6(e.extend(this.el.j19s("margin-top", "margin-left", "margin-right", "margin-bottom", "position", "top", "float"), {overflow: "hidden"})).enclose(this.el);
        this.el.j30("slide:wrapper", this.wrapper).j6({margin: 0})
    }, vertical: function () {
        this.margin = "margin-top";
        this.layout = "height";
        this.offset = this.el.offsetHeight
    }, horizontal: function (e) {
        this.margin = "margin-" + (e || "left");
        this.layout = "width";
        this.offset = this.el.offsetWidth
    }, right: function () {
        this.horizontal()
    }, left: function () {
        this.horizontal("right")
    }, start: function (e, t) {
        this[t || this.options.mode]();
        var n = this.el.j5(this.margin).j17(), r = this.wrapper.j5(this.layout).j17(), i = {}, s = {}, o;
        i[this.margin] = [n, 0], i[this.layout] = [0, this.offset], s[this.margin] = [n, -this.offset], s[this.layout] = [r, 0];
        switch (e) {
            case"in":
                o = i;
                break;
            case"out":
                o = s;
                break;
            case"toggle":
                o = 0 == r ? i : s;
                break
        }
        this.$parent.start(o);
        return this
    }, set: function (e) {
        this.el.j6Prop(this.margin, e[this.margin]);
        this.wrapper.j6Prop(this.layout, e[this.layout]);
        return this
    }, slideIn: function (e) {
        return this.start("in", e)
    }, slideOut: function (e) {
        return this.start("out", e)
    }, hide: function (e) {
        this[e || this.options.mode]();
        var t = {};
        t[this.layout] = 0, t[this.margin] = -this.offset;
        return this.set(t)
    }, show: function (e) {
        this[e || this.options.mode]();
        var t = {};
        t[this.layout] = this.offset, t[this.margin] = 0;
        return this.set(t)
    }, toggle: function (e) {
        return this.start("toggle", e)
    }})
})(magicJS);
(function (e) {
    if (!e) {
        throw"MagicJS not found";
        return
    }
    if (e.PFX) {
        return
    }
    var t = e.$;
    e.PFX = new e.Class(e.FX, {init: function (t, n) {
        this.el_arr = t;
        this.options = e.extend(this.options, n);
        this.timer = false
    }, start: function (e) {
        this.$parent.start([]);
        this.styles_arr = e;
        return this
    }, render: function (e) {
        for (var n = 0; n < this.el_arr.length; n++) {
            this.el = t(this.el_arr[n]);
            this.styles = this.styles_arr[n];
            this.$parent.render(e)
        }
    }})
})(magicJS);
var MagicZoomPlus = function (e) {
    var t = e.$;
    e.$Ff = function (e) {
        $mjs(e).stop();
        return false
    };
    e.insertCSS = function (t, n, r) {
        var i, s, o, u = [], a = -1;
        r || (r = e.stylesId);
        i = e.$(r) || (document.head || document.body).appendChild(e.$new("style", {id: r, type: "text/css"}));
        s = i.sheet || i.styleSheet;
        if ("object" == e.j1(n)) {
            for (o in n) {
                u.push(o + ":" + n[o])
            }
            n = u.join(";")
        }
        if (s.insertRule) {
            a = s.insertRule(t + " {" + n + "}", s.cssRules.length)
        } else {
            a = s.addRule(t, n)
        }
        return a
    };
    var n = {version: "v4.5.10", options: {}, defaults: {opacity: 50, opacityReverse: false, smoothingSpeed: 40, fps: 25, zoomWidth: 300, zoomHeight: 300, zoomDistance: 15, zoomPosition: "right", zoomAlign: "top", zoomWindowEffect: "shadow", dragMode: false, moveOnClick: true, alwaysShowZoom: false, preservePosition: false, x: -1, y: -1, clickToActivate: false, clickToDeactivate: false, initializeOn: "load", smoothing: true, showTitle: "top", titleSource: "title", zoomFade: true, zoomFadeInSpeed: 400, zoomFadeOutSpeed: 200, hotspots: "", hint: true, hintText: "Zoom", hintPosition: "tl", hintOpacity: 75, hintClass: "MagicZoomHint", showLoading: true, loadingMsg: "Loading zoom..", loadingOpacity: 75, loadingPositionX: -1, loadingPositionY: -1, selectorsChange: "click", selectorsMouseoverDelay: 60, selectorsEffect: "dissolve", selectorsEffectSpeed: 400, preloadSelectorsSmall: true, preloadSelectorsBig: false, selectorsClass: "", fitZoomWindow: true, entireImage: false, rightClick: false, disableZoom: false, onready: e.$F}, z39: $mjs([/^(opacity)(\s+)?:(\s+)?(\d+)$/i, /^(opacity-reverse)(\s+)?:(\s+)?(true|false)$/i, /^(smoothing\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(fps)(\s+)?:(\s+)?(\d+)$/i, /^(zoom\-width)(\s+)?:(\s+)?(\d+\%?)(px)?/i, /^(zoom\-height)(\s+)?:(\s+)?(\d+\%?)(px)?/i, /^(zoom\-distance)(\s+)?:(\s+)?(\d+)(px)?/i, /^(zoom\-position)(\s+)?:(\s+)?(right|left|top|bottom|custom|inner|#([a-z0-9_\-:\.]+))$/i, /^(zoom\-align)(\s+)?:(\s+)?(right|left|top|bottom|center)$/i, /^(zoom\-fit\-screen)(\s+)?:(\s+)?(true|false)$/i, /^(zoom\-window\-effect)(\s+)?:(\s+)?(shadow|glow|false)$/i, /^(drag\-mode)(\s+)?:(\s+)?(true|false)$/i, /^(move\-on\-click)(\s+)?:(\s+)?(true|false)$/i, /^(always\-show\-zoom)(\s+)?:(\s+)?(true|false)$/i, /^(preserve\-position)(\s+)?:(\s+)?(true|false)$/i, /^(x)(\s+)?:(\s+)?([\d.]+)(px)?/i, /^(y)(\s+)?:(\s+)?([\d.]+)(px)?/i, /^(click\-to\-activate)(\s+)?:(\s+)?(true|false)$/i, /^(click\-to\-deactivate)(\s+)?:(\s+)?(true|false)$/i, /^(initialize\-on)(\s+)?:(\s+)?(load|click|mouseover)$/i, /^(click\-to\-initialize)(\s+)?:(\s+)?(true|false)$/i, /^(smoothing)(\s+)?:(\s+)?(true|false)$/i, /^(show\-title)(\s+)?:(\s+)?(true|false|top|bottom)$/i, /^(title\-source)(\s+)?:(\s+)?(title|#([a-z0-9_\-:\.]+))$/i, /^(zoom\-fade)(\s+)?:(\s+)?(true|false)$/i, /^(zoom\-fade\-in\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(zoom\-fade\-out\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(hotspots)(\s+)?:(\s+)?([a-z0-9_\-:\.]+)$/i, /^(hint)(\s+)?:(\s+)?(true|false)/i, /^(hint\-text)(\s+)?:(\s+)?([^;]*)$/i, /^(hint\-opacity)(\s+)?:(\s+)?(\d+)$/i, /^(hint\-position)(\s+)?:(\s+)?(tl|tr|tc|bl|br|bc)/i, /^(show\-loading)(\s+)?:(\s+)?(true|false)$/i, /^(loading\-msg)(\s+)?:(\s+)?([^;]*)$/i, /^(loading\-opacity)(\s+)?:(\s+)?(\d+)$/i, /^(loading\-position\-x)(\s+)?:(\s+)?(\d+)(px)?/i, /^(loading\-position\-y)(\s+)?:(\s+)?(\d+)(px)?/i, /^(thumb\-change)(\s+)?:(\s+)?(click|mouseover)$/i, /^(selectors\-change)(\s+)?:(\s+)?(click|mouseover)$/i, /^(selectors\-mouseover\-delay)(\s+)?:(\s+)?(\d+)$/i, /^(selectors\-effect)(\s+)?:(\s+)?(dissolve|fade|pounce|false)$/i, /^(selectors\-effect\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(selectors\-class)(\s+)?:(\s+)?([a-z0-9_\-:\.]+)$/i, /^(fit\-zoom\-window)(\s+)?:(\s+)?(true|false)$/i, /^(preload\-selectors\-small)(\s+)?:(\s+)?(true|false)$/i, /^(preload\-selectors\-big)(\s+)?:(\s+)?(true|false)$/i, /^(entire\-image)(\s+)?:(\s+)?(true|false)$/i, /^(right\-click)(\s+)?:(\s+)?(true|false)$/i, /^(disable\-zoom)(\s+)?:(\s+)?(true|false)$/i]), zooms: $mjs([]), z8: function (e) {
        var t = /(click|mouseover)/i;
        for (var r = 0; r < n.zooms.length; r++) {
            if (n.zooms[r].z30 && !n.zooms[r].activatedEx) {
                n.zooms[r].pause()
            } else {
                if (t.test(n.zooms[r].options.initializeOn) && n.zooms[r].initMouseEvent) {
                    n.zooms[r].initMouseEvent = e
                }
            }
        }
    }, stop: function (t) {
        var n = $mjs([]);
        if (t) {
            if ((t = $mjs(t)) && t.zoom) {
                n.push(t)
            } else {
                return false
            }
        } else {
            n = $mjs(e.$A(e.body.byTag("A")).filter(function (e) {
                return(" " + e.className + " ").match(/\sMagicZoom\s/) && e.zoom
            }))
        }
        n.j14(function (e) {
            e.zoom && e.zoom.stop()
        }, this)
    }, start: function (e) {
        if (0 == arguments.length) {
            n.refresh();
            return true
        }
        e = $mjs(e);
        if (!e || !(" " + e.className + " ").match(/\s(MagicZoom|MagicZoomPlus)\s/)) {
            return false
        }
        if (!e.zoom) {
            var t = null;
            while (t = e.firstChild) {
                if (t.tagName == "IMG") {
                    break
                }
                e.removeChild(t)
            }
            while (t = e.lastChild) {
                if (t.tagName == "IMG") {
                    break
                }
                e.removeChild(t)
            }
            if (!e.firstChild || e.firstChild.tagName != "IMG") {
                throw"Invalid Magic Zoom"
            }
            n.zooms.push(new n.zoom(e, arguments.length > 1 ? arguments[1] : undefined))
        } else {
            e.zoom.start()
        }
    }, update: function (e, t, n, r) {
        if ((e = $mjs(e)) && e.zoom) {
            (null === t || "" === t) && (t = undefined);
            (null === n || "" === n) && (n = undefined);
            e.zoom.update(t, n, r);
            return true
        }
        return false
    }, refresh: function () {
        e.$A(window.document.getElementsByTagName("A")).j14(function (e) {
            if (e.className.has("MagicZoom", " ")) {
                if (n.stop(e)) {
                    n.start.j27(100, e)
                } else {
                    n.start(e)
                }
            }
        }, this)
    }, show: function (e) {
        return n.zoomIn(e)
    }, zoomIn: function (e) {
        if ((e = $mjs(e)) && e.zoom) {
            return e.zoom.activate()
        }
        return false
    }, zoomOut: function (e) {
        if ((e = $mjs(e)) && e.zoom) {
            return e.zoom.pause()
        }
        return false
    }, getXY: function (e) {
        if ((e = $mjs(e)) && e.zoom) {
            return{x: e.zoom.options.x, y: e.zoom.options.y}
        }
    }, x7: function (e) {
        var t, n;
        t = "";
        for (n = 0; n < e.length; n++) {
            t += String.fromCharCode(14 ^ e.charCodeAt(n))
        }
        return t
    }};
    n.z48 = function () {
        this.init.apply(this, arguments)
    };
    n.z48.prototype = {init: function (t) {
        this.cb = null;
        this.z9 = null;
        this.onErrorHandler = this.onError.j16(this);
        this.z10 = null;
        this.width = 0;
        this.height = 0;
        this.naturalWidth = 0;
        this.naturalHight = 0;
        this.border = {left: 0, right: 0, top: 0, bottom: 0};
        this.padding = {left: 0, right: 0, top: 0, bottom: 0};
        this.ready = false;
        this._tmpp = null;
        if ("string" == e.j1(t)) {
            this._tmpp = e.$new("div").j2("magic-temporary-img").j6({position: "absolute", top: "-10000px", width: "1px", height: "1px", overflow: "hidden"}).j32(e.body);
            this.self = e.$new("img").j32(this._tmpp);
            this.z11();
            this.self.src = t
        } else {
            this.self = $mjs(t);
            this.z11();
            this.self.src = t.src
        }
    }, _cleanup: function () {
        if (this._tmpp) {
            if (this.self.parentNode == this._tmpp) {
                this.self.j33().j6({position: "static", top: "auto"})
            }
            this._tmpp.kill();
            this._tmpp = null
        }
    }, onError: function (e) {
        if (e) {
            $mjs(e).stop()
        }
        if (this.cb) {
            this._cleanup();
            this.cb.call(this, false)
        }
        this.unload()
    }, z11: function (e) {
        this.z9 = null;
        if (e == true || !(this.self.src && (this.self.complete || this.self.readyState == "complete"))) {
            this.z9 = function (e) {
                if (e) {
                    $mjs(e).stop()
                }
                if (this.ready) {
                    return
                }
                this.ready = true;
                this.z13();
                if (this.cb) {
                    this._cleanup();
                    this.cb.call()
                }
            }.j16(this);
            this.self.je1("load", this.z9);
            $mjs(["abort", "error"]).j14(function (e) {
                this.self.je1(e, this.onErrorHandler)
            }, this)
        } else {
            this.ready = true
        }
    }, update: function (t, n) {
        this.unload();
        var r = e.$new("a", {href: t});
        if (true !== n && this.self.src.has(r.href) && 0 !== this.self.width) {
            this.ready = true
        } else {
            this.z11(true);
            this.self.src = t
        }
        r = null
    }, z13: function () {
        this.naturalWidth = this.self.naturalWidth || this.self.width;
        this.naturalHeight = this.self.naturalHeight || this.self.height;
        this.width = this.self.width;
        this.height = this.self.height;
        if (this.width == 0 && this.height == 0 && e.j21.webkit) {
            this.width = this.self.naturalWidth;
            this.height = this.self.naturalHeight
        }
        $mjs(["Left", "Right", "Top", "Bottom"]).j14(function (e) {
            this.padding[e.toLowerCase()] = this.self.j19("padding" + e).j17();
            this.border[e.toLowerCase()] = this.self.j19("border" + e + "Width").j17()
        }, this);
        if (e.j21.presto || e.j21.trident && !e.j21.backCompat) {
            this.width -= this.padding.left + this.padding.right;
            this.height -= this.padding.top + this.padding.bottom
        }
    }, getBox: function () {
        var e = null;
        e = this.self.j9();
        return{top: e.top + this.border.top, bottom: e.bottom - this.border.bottom, left: e.left + this.border.left, right: e.right - this.border.right}
    }, z12: function () {
        if (this.z10) {
            this.z10.src = this.self.src;
            this.self = null;
            this.self = this.z10
        }
    }, load: function (e) {
        if (this.ready) {
            if (!this.width) {
                (function () {
                    this.z13();
                    this._cleanup();
                    e.call()
                }).j24(this).j27(1)
            } else {
                this._cleanup();
                e.call()
            }
        } else {
            if (!this.z9) {
                e.call(this, false);
                return
            }
            this.cb = e
        }
    }, unload: function () {
        if (this.z9) {
            this.self.je2("load", this.z9)
        }
        $mjs(["abort", "error"]).j14(function (e) {
            this.self.je2(e, this.onErrorHandler)
        }, this);
        this.z9 = null;
        this.cb = null;
        this.width = null;
        this.ready = false;
        this._new = false
    }};
    n.zoom = function () {
        this.construct.apply(this, arguments)
    };
    n.zoom.prototype = {construct: function (t, r, i) {
        var s = {};
        this.z28 = -1;
        this.z30 = false;
        this.ddx = 0;
        this.ddy = 0;
        this.firstRun = !this.z47;
        this.exOptions = this.firstRun ? {} : this.exOptions || {};
        this.activatedEx = false;
        this.z44 = null;
        this.z1Holder = $mjs(window).j29("magiczoom:holder") || $mjs(window).j29("magiczoom:holder", e.$new("div").j6({position: "absolute", top: -1e4, width: 10, height: 10, overflow: "hidden"}).j32(e.body));
        this.options = e.detach(n.defaults);
        if (t) {
            this.c = $mjs(t)
        }
        this.divTag = "div" == this.c.tagName.toLowerCase();
        s = e.extend(s, this.z37());
        s = e.extend(s, this.z37(this.c.rel));
        s = e.extend(s, this.exOptions);
        if (r) {
            s = e.extend(s, e.extend(true === i ? this.exOptions : {}, this.z37(r)))
        }
        if (s.dragMode && !s.clickToActivate && undefined === s.alwaysShowZoom) {
            s.alwaysShowZoom = true
        }
        e.extend(this.options, s);
        this.options.hotspots += "";
        if ("load" == this.options.initializeOn && e.defined(this.options.clickToInitialize) && "true" == this.options.clickToInitialize.toString()) {
            this.options.initializeOn = "click"
        }
        if (e.defined(this.options.thumbChange) && this.options.thumbChange != this.options.selectorsChange) {
            this.options.selectorsChange = this.options.thumbChange
        }
        if (this.firstRun && !this.divTag) {
            this.id = this.originId = this.c.id || "";
            if (!this.c.id) {
                this.c.id = this.id = "zoom-" + Math.floor(Math.random() * e.now())
            }
        }
        if ("inner" == this.options.zoomPosition && this.options.dragMode) {
            this.options.moveOnClick = true
        }
        if (this.options.disableZoom) {
            this.z30 = false;
            this.options.clickToActivate = true;
            this.options.hint = false
        }
        "string" === e.j1(this.options.onready) && "function" === e.j1(window[this.options.onready]) && (this.options.onready = window[this.options.onready]);
        if (t) {
            this.lastSelector = null;
            this.z14 = this.mousedown.j16(this);
            this.z15 = this.mouseup.j16(this);
            this.z16 = this.show.j24(this, true);
            this.z17 = this.z29.j24(this);
            this.z43Bind = this.z43.j16(this);
            this.resizeBind = function (e) {
                var t = $mjs(this.c).j29("magiczoom:window:size"), n = $mjs(window).j7();
                if (t.width !== n.width || t.height !== n.height) {
                    clearTimeout(this.resizeTimer);
                    this.resizeTimer = this.onresize.j24(this).j27(10);
                    $mjs(this.c).j30("magiczoom:window:size", n)
                }
            }.j16(this);
            if (!this.divTag) {
                this.c.je1("click", function (t) {
                    var n = t.getButton();
                    if (3 == n) {
                        return true
                    }
                    $mjs(t).stop();
                    if (!e.j21.trident) {
                        this.blur()
                    }
                    return false
                })
            }
            this.c.je1("mousedown", this.z14);
            this.c.je1("mouseup", this.z15);
            if ("mouseover" == this.options.initializeOn) {
                this.c.je1("mouseover", this.z14)
            }
            if (e.j21.touchScreen) {
                this.c.j6({"-webkit-user-select": "none", "-webkit-touch-callout": "none", "-webkit-tap-highlight-color": "transparent"});
                if (!this.options.disableZoom) {
                    this.c.je1("touchstart", this.z14);
                    this.c.je1("touchend", this.z15)
                } else {
                    this.c.je1("click", function (e) {
                        e.preventDefault()
                    })
                }
            }
            this.c.unselectable = "on";
            this.c.style.MozUserSelect = "none";
            this.c.je1("selectstart", e.$Ff);
            if (!this.divTag) {
                this.c.j6({position: "relative", display: e.j21.gecko181 ? "block" : "inline-block", textDecoration: "none", outline: "0", cursor: "hand", overflow: "hidden"});
                if (e.j21.ieMode) {
                    this.c.j2("magic-for-ie" + e.j21.ieMode)
                }
                if (this.c.j5("textAlign") == "center") {
                    this.c.j6({margin: "auto auto"})
                }
            }
            this.c.zoom = this
        } else {
            this.options.initializeOn = "load"
        }
        if (!this.options.rightClick) {
            this.c.je1("contextmenu", e.$Ff)
        }
        if ("load" == this.options.initializeOn) {
            this.z18()
        } else {
            if ("" !== this.originId) {
                this.z26(true)
            }
        }
    }, z18: function () {
        var t, r, i, s, o;
        if (!this.z7) {
            this.z7 = new n.z48(this.c.firstChild);
            this.z1 = new n.z48(this.c.href)
        } else {
            this.z1.update(this.c.href)
        }
        if (!this.z47) {
            this.z47 = {self: $mjs(document.createElement("DIV"))[this.divTag ? "j3" : "j2"]("MagicZoomBigImageCont").j6({overflow: "hidden", zIndex: this.options.zoomPosition == "inner" ? 100 : 10002, top: "-100000px", position: "absolute", width: this.options.zoomWidth + "px", height: this.options.zoomHeight + "px"}), zoom: this, z21: "0px", lastLeftPos: "0px", initTopPos: 0, initLeftPos: 0, adjustX: {edge: "left", ratio: 1}, adjustY: {edge: "top", ratio: 1}, custom: false, initWidth: this.options.zoomWidth, initHeight: this.options.zoomHeight};
            if (!(e.j21.trident900 && e.j21.ieMode < 9)) {
                switch (this.options.zoomWindowEffect) {
                    case"shadow":
                        this.z47.self.j2("MagicBoxShadow");
                        break;
                    case"glow":
                        this.z47.self.j2("MagicBoxGlow");
                        break;
                    default:
                        break
                }
            }
            this.z47.hide = function () {
                if (this.self.style.top != "-100000px" && this.zoom.z4 && !this.zoom.z4.z38) {
                    this.self.style.top = "-100000px"
                }
                if (this.self.parentNode === e.body) {
                    this.self.j32(this.zoom.z1Holder)
                }
            };
            this.z47.z22 = this.z47.hide.j24(this.z47);
            if (e.j21.trident4) {
                t = $mjs(document.createElement("IFRAME"));
                t.src = "javascript:''";
                t.j6({left: "0px", top: "0px", position: "absolute", "z-index": -1}).frameBorder = 0;
                this.z47.z23 = this.z47.self.appendChild(t)
            }
            this.z47.z41 = $mjs(document.createElement("DIV")).j2("MagicZoomHeader").j6({position: "relative", zIndex: 10, left: "0px", top: "0px", padding: "3px"}).hide();
            r = e.$new("DIV", {}, {overflow: "hidden"});
            r.appendChild(this.z1.self);
            this.z1.self.j6({padding: "0px", margin: "0px", border: "0px", width: "auto", height: "auto"});
            if (this.options.showTitle == "bottom") {
                this.z47.self.appendChild(r);
                this.z47.self.appendChild(this.z47.z41)
            } else {
                this.z47.self.appendChild(this.z47.z41);
                this.z47.self.appendChild(r)
            }
            this.z47.self.j32(this.z1Holder);
            if ("undefined" !== typeof o) {
                this.z47.g = $mjs(document.createElement("div")).j6({color: o[1], fontSize: o[2] + "px", fontWeight: o[3], fontFamily: "Tahoma", position: "absolute", "z-index": 10 + ("" + (this.z1.self.j5("z-index") || 0)).j17(), width: o[5], textAlign: o[4], "line-height": "2em", left: "0px"}).changeContent(n.x7(o[0])).j32(this.z47.self, (Math.floor(Math.random() * 101) + 1) % 2 ? "top" : "bottom")
            }
        }
        this.z47.custom = false;
        if (this.options.zoomPosition == "custom" && $mjs(this.c.id + "-big")) {
            this.z47.custom = true;
            $mjs(this.c.id + "-big").appendChild(this.z47.self)
        } else {
            if (this.options.zoomPosition.has("#")) {
                var u = this.options.zoomPosition.replace(/^#/, "");
                if ($mjs(u)) {
                    this.z47.custom = true;
                    $mjs(u).appendChild(this.z47.self)
                }
            } else {
                if (this.options.zoomPosition == "inner") {
                    this.c.appendChild(this.z47.self)
                }
            }
        }
        this.z47.initWidth = this.options.zoomWidth;
        this.z47.initHeight = this.options.zoomHeight;
        if (this.options.showTitle != "false" && this.options.showTitle != false) {
            var a = this.z47.z41;
            a.hide();
            while (i = a.firstChild) {
                a.removeChild(i)
            }
            if (this.options.titleSource == "title" && "" != this.c.title) {
                a.appendChild(document.createTextNode(this.c.title));
                a.show()
            } else {
                if (this.options.titleSource.has("#")) {
                    var u = this.options.titleSource.replace(/^#/, "");
                    if ($mjs(u)) {
                        a.changeContent($mjs(u).innerHTML);
                        a.show()
                    }
                }
            }
        } else {
            this.z47.z41.hide()
        }
        this.c.z46 = this.c.title;
        this.c.title = "";
        this.z7.load(this.z19.j24(this))
    }, z19: function (e) {
        if (!e && e !== undefined) {
            return
        }
        if (!this.z7) {
            return
        }
        if (!this.options.opacityReverse) {
            this.z7.self.j23(1)
        }
        if (!this.divTag) {
            this.c.j6({width: "auto", height: "auto"})
        }
        if (this.options.showLoading && !this.options.disableZoom) {
            this.z24 = setTimeout(this.z17, 400)
        }
        if (this.options.hotspots != "" && $mjs(this.options.hotspots)) {
            this.z25()
        }
        if (this.c.id != "") {
            this.z26()
        }
        this.z1.load(this.z20.j24(this))
    }, z20: function (t) {
        var n, r, i;
        if (!t && t !== undefined) {
            clearTimeout(this.z24);
            if (this.options.showLoading && this.z3) {
                this.z3.hide()
            }
            return
        }
        if (!this.z7 || !this.z1) {
            return
        }
        r = this.z7.self.j9();
        this.z7Rect = r;
        if (r.bottom == r.top) {
            this.z20.j24(this).j27(500);
            return
        }
        if (this.z7.width == 0 && e.j21.trident) {
            this.z7.z13();
            this.z1.z13();
            !this.divTag && this.c.j6({width: this.z7.width + "px"})
        }
        n = this.z47.z41.j7();
        if (/%$/i.test(this.options.zoomWidth)) {
            this.options.zoomWidth = parseInt(this.options.zoomWidth) / 100 * this.z7.width
        }
        if (/%$/i.test(this.options.zoomHeight)) {
            this.options.zoomHeight = parseInt(this.options.zoomHeight) / 100 * this.z7.height
        }
        this.z47.self.j6({width: this.options.zoomWidth});
        n = this.z47.z41.j7();
        if (this.options.fitZoomWindow || this.options.entireImage) {
            if (this.z1.width < this.options.zoomWidth || this.options.entireImage) {
                this.options.zoomWidth = this.z1.width;
                this.z47.self.j6({width: this.options.zoomWidth});
                n = this.z47.z41.j7()
            }
            if (this.z1.height < this.options.zoomHeight || this.options.entireImage) {
                this.options.zoomHeight = this.z1.height + n.height
            }
        }
        switch (this.options.zoomPosition) {
            case"right":
                this.z47.self.style.left = r.right + this.options.zoomDistance + "px";
                this.z47.adjustX.edge = "right";
                break;
            case"left":
                this.z47.self.style.left = r.left - this.options.zoomDistance - this.options.zoomWidth + "px";
                break;
            case"top":
                this.z47.z21 = r.top - (this.options.zoomDistance + this.options.zoomHeight) + "px";
                break;
            case"bottom":
                this.z47.z21 = r.bottom + this.options.zoomDistance + "px";
                this.z47.adjustY.edge = "bottom";
                break;
            case"inner":
                this.z47.self.j6({left: "0px", height: "100%", width: "100%"});
                this.options.zoomWidth = this.z7.width;
                this.options.zoomHeight = this.z7.height;
                this.z47.z21 = "0px";
                n = this.z47.z41.j7();
                break;
            default:
                if (this.z47.custom) {
                    i = $mjs(this.z47.self.parentNode).j7();
                    if (/%$/i.test(this.z47.initWidth)) {
                        this.options.zoomWidth = parseInt(this.z47.initWidth) / 100 * i.width
                    }
                    if (/%$/i.test(this.z47.initHeight)) {
                        this.options.zoomHeight = parseInt(this.z47.initHeight) / 100 * i.height
                    }
                    this.z47.self.j6({left: "0px", width: this.options.zoomWidth});
                    this.z47.z21 = "0px";
                    n = this.z47.z41.j7()
                }
                break
        }
        if (this.options.showTitle == "bottom") {
            $mjs(this.z1.self.parentNode).j6Prop("height", this.options.zoomHeight - n.height)
        }
        this.z47.self.j6("inner" == this.options.zoomPosition ? {} : {height: this.options.zoomHeight + "px", width: this.options.zoomWidth + "px"}).j23(1);
        if (e.j21.trident4 && this.z47.z23) {
            this.z47.z23.j6({width: this.options.zoomWidth + "px", height: this.options.zoomHeight + "px"})
        }
        if (this.options.zoomPosition == "right" || this.options.zoomPosition == "left") {
            if (this.options.zoomAlign == "center") {
                this.z47.z21 = r.bottom - (r.bottom - r.top) / 2 - this.options.zoomHeight / 2 + "px";
                this.z47.adjustY = {edge: "bottom", ratio: 2}
            } else {
                if (this.options.zoomAlign == "bottom") {
                    this.z47.z21 = r.bottom - this.options.zoomHeight + "px";
                    this.z47.adjustY.edge = "bottom"
                } else {
                    this.z47.z21 = r.top + "px"
                }
            }
        } else {
            if (this.options.zoomPosition == "top" || this.options.zoomPosition == "bottom") {
                if (this.options.zoomAlign == "center") {
                    this.z47.self.style.left = r.right - (r.right - r.left) / 2 - this.options.zoomWidth / 2 + "px";
                    this.z47.adjustX = {edge: "right", ratio: 2}
                } else {
                    if (this.options.zoomAlign == "right") {
                        this.z47.self.style.left = r.right - this.options.zoomWidth + "px";
                        this.z47.adjustX.edge = "right"
                    } else {
                        this.z47.self.style.left = r.left + "px"
                    }
                }
            }
        }
        this.z47.initTopPos = parseInt(this.z47.z21, 10);
        this.z47.initLeftPos = parseInt(this.z47.self.style.left, 10);
        this.z47.lastLeftPos = this.z47.initLeftPos;
        this.z47.z21 = this.z47.initTopPos;
        this.zoomViewHeight = this.options.zoomHeight - n.height;
        if (this.z47.g) {
            this.z47.g.j6({top: this.options.showTitle == "bottom" ? 0 : "auto", bottom: this.options.showTitle == "bottom" ? "auto" : 0})
        }
        this.z1.self.j6({position: "relative", borderWidth: "0px", padding: "0px", left: "0px", top: "0px"});
        this.z27();
        if (this.options.alwaysShowZoom) {
            if (this.options.x == -1) {
                this.options.x = this.z7.width / 2
            }
            if (this.options.y == -1) {
                this.options.y = this.z7.height / 2
            }
            this.show()
        } else {
            if (this.options.zoomFade) {
                this.z2 = new e.FX(this.z47.self, {forceAnimation: "ios" === e.j21.platform})
            }
            this.z47.self.j6({top: "-100000px"})
        }
        if (this.options.showLoading && this.z3) {
            this.z3.hide()
        }
        this.c.je1("mousemove", this.z43Bind);
        this.c.je1("mouseout", this.z43Bind);
        if (e.j21.touchScreen) {
            this.c.je1("touchmove", this.z43Bind);
            this.c.je1("touchend", this.z43Bind)
        }
        this.setupHint();
        $mjs(this.c).j29("magiczoom:window:size", $mjs(window).j7());
        $mjs(window).je1("resize", this.resizeBind);
        if (!this.options.disableZoom && (!this.options.clickToActivate || "click" == this.options.initializeOn)) {
            this.z30 = true
        }
        if ("click" == this.options.initializeOn && this.initMouseEvent) {
            this.z43(this.initMouseEvent)
        }
        if (this.activatedEx) {
            this.activate()
        }
        this.z28 = e.now();
        !this.divTag && "function" == e.j1(this.options.onready) && this.options.onready.call(null, this.id, !this.firstRun)
    }, setupHint: function () {
        var e = /tr|br/i, t = /bl|br|bc/i, n = /bc|tc/i, r = null;
        this.hintVisible = undefined;
        if (!this.options.hint) {
            if (this.hint) {
                this.hint.kill();
                this.hint = undefined
            }
            return
        }
        if (!this.hint) {
            this.hint = $mjs(document.createElement("DIV")).j2(this.options.hintClass).j6({display: "block", overflow: "hidden", position: "absolute", visibility: "hidden", "z-index": 1});
            if (this.options.hintText != "") {
                this.hint.appendChild(document.createTextNode(this.options.hintText))
            }
            this.c.appendChild(this.hint)
        } else {
            if (this.options.hintText != "") {
                r = this.hint[this.hint.firstChild ? "replaceChild" : "appendChild"](document.createTextNode(this.options.hintText), this.hint.firstChild);
                r = null
            }
        }
        this.hint.j6({left: "auto", right: "auto", top: "auto", bottom: "auto", display: "block", opacity: this.options.hintOpacity / 100, "max-width": this.z7.width - 4});
        var i = this.hint.j7();
        this.hint.j6Prop(e.test(this.options.hintPosition) ? "right" : "left", n.test(this.options.hintPosition) ? (this.z7.width - i.width) / 2 : 2).j6Prop(t.test(this.options.hintPosition) ? "bottom" : "top", 2);
        this.hintVisible = true;
        this.hint.show()
    }, z29: function () {
        if (this.z1.ready) {
            return
        }
        this.z3 = $mjs(document.createElement("DIV")).j2("MagicZoomLoading").j23(this.options.loadingOpacity / 100).j6({display: "block", overflow: "hidden", position: "absolute", visibility: "hidden", "z-index": 20, "max-width": this.z7.width - 4});
        this.z3.appendChild(document.createTextNode(this.options.loadingMsg));
        this.c.appendChild(this.z3);
        var e = this.z3.j7();
        this.z3.j6({left: (this.options.loadingPositionX == -1 ? (this.z7.width - e.width) / 2 : this.options.loadingPositionX) + "px", top: (this.options.loadingPositionY == -1 ? (this.z7.height - e.height) / 2 : this.options.loadingPositionY) + "px"});
        this.z3.show()
    }, z25: function () {
        $mjs(this.options.hotspots).z31 = $mjs(this.options.hotspots).parentNode;
        $mjs(this.options.hotspots).z32 = $mjs(this.options.hotspots).nextSibling;
        this.c.appendChild($mjs(this.options.hotspots));
        $mjs(this.options.hotspots).j6({position: "absolute", left: "0px", top: "0px", width: this.z7.width + "px", height: this.z7.height + "px", zIndex: 15}).show();
        if (e.j21.trident) {
            this.c.z33 = this.c.appendChild($mjs(document.createElement("DIV")).j6({position: "absolute", left: "0px", top: "0px", width: this.z7.width + "px", height: this.z7.height + "px", zIndex: 14, background: "#ccc"}).j23(1e-5))
        }
        e.$A($mjs(this.options.hotspots).getElementsByTagName("A")).j14(function (e) {
            var t = e.coords.split(","), n = null;
            $mjs(e).j6({position: "absolute", left: t[0] + "px", top: t[1] + "px", width: t[2] - t[0] + "px", height: t[3] - t[1] + "px", zIndex: 15}).show();
            if (e.j13("MagicThumb")) {
                if (n = e.j29("thumb")) {
                    n.group = this.options.hotspots
                } else {
                    e.rel += ";group: " + this.options.hotspots + ";"
                }
            }
        }, this)
    }, z26: function (t) {
        var n, r, i = new RegExp("zoom\\-id(\\s+)?:(\\s+)?" + this.c.id + "($|;)");
        this.selectors = $mjs([]);
        e.$A(document.getElementsByTagName("A")).j14(function (s) {
            if (i.test(s.rel)) {
                if (!$mjs(s).z36) {
                    s.z36 = function (t) {
                        if (!e.j21.trident) {
                            this.blur()
                        }
                        $mjs(t).stop();
                        return false
                    };
                    s.je1("click", s.z36)
                }
                if (t) {
                    if (("mouseover" == this.options.initializeOn || "click" == this.options.initializeOn) && !$mjs(s).clickInitZoom) {
                        s.clickInitZoom = function (e, t) {
                            t.je2("click", t.clickInitZoom);
                            if (!!this.z7) {
                                return
                            }
                            $mjs(e).stop();
                            this.c.href = t.href;
                            this.c.firstChild.src = t.rev;
                            this.start(t.rel);
                            if (this.c.j29("thumb")) {
                                this.c.j29("thumb").start()
                            }
                        }.j16(this, s);
                        s.je1("click", s.clickInitZoom)
                    }
                    return
                }
                var o = e.$new("a", {href: s.rev});
                this.options.selectorsClass != "" && $mjs(s)[this.z1.self.src.has(s.href) && this.z7.self.src.has(o.href) ? "j2" : "j3"](this.options.selectorsClass);
                if (this.z1.self.src.has(s.href) && this.z7.self.src.has(o.href)) {
                    this.lastSelector = s
                }
                o = null;
                if (!s.z34) {
                    s.z34 = function (e, t) {
                        t = e.currentTarget || e.getTarget();
                        try {
                            while ("a" != t.tagName.toLowerCase()) {
                                t = t.parentNode
                            }
                        } catch (n) {
                            return
                        }
                        if (t.hasChild(e.getRelated())) {
                            return
                        }
                        if (e.type == "mouseout") {
                            if (this.z35) {
                                clearTimeout(this.z35)
                            }
                            this.z35 = false;
                            return
                        }
                        if (t.title != "") {
                            this.c.title = t.title
                        }
                        if (e.type == "mouseover") {
                            this.z35 = setTimeout(this.update.j24(this, t.href, t.rev, t.rel, t), this.options.selectorsMouseoverDelay)
                        } else {
                            this.update(t.href, t.rev, t.rel, t)
                        }
                    }.j16(this);
                    s.je1(this.options.selectorsChange, s.z34);
                    if (this.options.selectorsChange == "mouseover") {
                        s.je1("mouseout", s.z34)
                    }
                }
                s.j6({outline: "0", display: "inline-block"});
                if (this.options.preloadSelectorsSmall) {
                    r = new Image;
                    r.src = s.rev
                }
                if (this.options.preloadSelectorsBig) {
                    n = new Image;
                    n.src = s.href
                }
                this.selectors.push(s)
            }
        }, this)
    }, stop: function (t) {
        try {
            this.pause();
            this.c.je2("mousemove", this.z43Bind);
            this.c.je2("mouseout", this.z43Bind);
            if (e.j21.touchScreen) {
                this.c.je2("touchmove", this.z43Bind);
                this.c.je2("touchend", this.z43Bind)
            }
            if (undefined === t && this.z4) {
                this.z4.self.hide()
            }
            if (this.z2) {
                this.z2.stop()
            }
            this.z6 = null;
            this.z30 = false;
            if (this.selectors !== undefined) {
                this.selectors.j14(function (e) {
                    if (this.options.selectorsClass != "") {
                        e.j3(this.options.selectorsClass)
                    }
                    if (undefined === t) {
                        e.je2(this.options.selectorsChange, e.z34);
                        if (this.options.selectorsChange == "mouseover") {
                            e.je2("mouseout", e.z34)
                        }
                        e.z34 = null;
                        e.je2("click", e.z36);
                        e.z36 = null
                    }
                }, this)
            }
            if (this.options.hotspots != "" && $mjs(this.options.hotspots)) {
                $mjs(this.options.hotspots).hide();
                $mjs(this.options.hotspots).z31.insertBefore($mjs(this.options.hotspots), $mjs(this.options.hotspots).z32);
                if (this.c.z33) {
                    this.c.removeChild(this.c.z33)
                }
            }
            this.z1.unload();
            if (this.options.opacityReverse) {
                this.c.j3("MagicZoomPup");
                this.z7.self.j23(1)
            }
            this.z2 = null;
            if (this.z3) {
                this.c.removeChild(this.z3)
            }
            if (this.hint) {
                this.hint.hide()
            }
            if (undefined === t) {
                if (this.hint) {
                    this.c.removeChild(this.hint)
                }
                this.hint = null;
                this.z7.unload();
                this.z4 && this.z4.self && this.c.removeChild(this.z4.self);
                this.z47 && this.z47.self && this.z47.self.parentNode.removeChild(this.z47.self);
                this.z4 = null;
                this.z47 = null;
                this.z1 = null;
                this.z7 = null;
                if (!this.options.rightClick) {
                    this.c.je2("contextmenu", e.$Ff)
                }
                if ("" === this.originId) {
                    this.c.removeAttribute("id")
                } else {
                    this.c.id = this.originId
                }
                $mjs(window).je2("resize", this.resizeBind)
            }
            if (this.z24) {
                clearTimeout(this.z24);
                this.z24 = null
            }
            this.z44 = null;
            this.c.z33 = null;
            this.z3 = null;
            if (this.c.title == "") {
                this.c.title = this.c.z46
            }
            this.z28 = -1
        } catch (n) {
        }
    }, start: function (e, t) {
        if (this.z28 != -1) {
            return
        }
        this.construct(false, e, null === t || undefined === t)
    }, update: function (t, r, i, s) {
        var o, u, a, f, l, h, p = null, d = null, v, m, y, b, w, E, S, x, T;
        s = s || null;
        if (e.now() - this.z28 < 300 || this.z28 == -1 || this.ufx) {
            this.z35 && clearTimeout(this.z35);
            o = 300 - e.now() + this.z28;
            if (this.z28 == -1) {
                o = 300
            }
            this.z35 = setTimeout(this.update.j24(this, t, r, i, s), o);
            return
        }
        if (s && this.lastSelector == s) {
            return
        } else {
            this.lastSelector = s
        }
        u = function (e) {
            if (undefined != t) {
                this.c.href = t
            }
            if (undefined === i) {
                i = ""
            }
            if (this.options.preservePosition) {
                i = "x: " + this.options.x + "; y: " + this.options.y + "; " + i
            }
            if (undefined != r) {
                this.z7.update(r)
            }
            if (e !== undefined) {
                this.z7.load(e)
            }
        };
        d = this.c.j29("thumb");
        if (d && d.ready) {
            d.restore(null, true);
            d.state = "updating";
            p = function () {
                d.state = "inz30";
                d.update(this.c.href, null, i)
            }.j24(this)
        }
        this.z7.z13();
        f = this.z7.width;
        l = this.z7.height;
        this.stop(true);
        if (this.options.selectorsEffect != "false" && undefined !== r) {
            this.ufx = true;
            var N = $mjs(this.c.cloneNode(true)).j6({position: "absolute", top: 0, left: 0, width: ""});
            var C = e.$new("div", {id: this.c.parentNode.id, "class": this.c.parentNode.className}).j2("mz-tmp-clone").j6({width: $mjs(this.c.parentNode).j5("width"), "max-width": $mjs(this.c.parentNode).j5("max-width")});
            if ("td" === this.c.parentNode.tagName.toLocaleLowerCase()) {
                this.c.parentNode.insertBefore(C, this.c)
            } else {
                this.c.parentNode.parentNode.insertBefore(C, this.c.parentNode)
            }
            C.append(N);
            e.j21.chrome && C.j7();
            if (e.j21.ieMode && e.j21.ieMode < 8) {
                $mjs(N.firstChild).j23(1)
            }
            h = new n.z48(N.firstChild);
            h.update(r);
            if ("pounce" == this.options.selectorsEffect) {
                T = this.c.href;
                v = this.selectors.filter(function (e) {
                    return e.href.has(T)
                });
                v = v[0] ? $mjs(v[0].byTag("img")[0] || v[0]) : this.z7.self;
                m = this.selectors.filter(function (e) {
                    return e.href.has(t)
                });
                m = m[0] ? $mjs(m[0].byTag("img")[0] || m[0]) : null;
                if (null == m) {
                    m = this.z7.self;
                    v = this.z7.self
                }
                b = this.z7.self.j8(), w = v.j8(), E = m.j8(), x = v.j7(), S = m.j7()
            }
            a = function () {
                var t = {}, n = {}, r = {}, o = null, a = null;
                if (e.j21.ieMode && e.j21.ieMode < 8 && (f === h.width || 0 === h.width)) {
                    h.self.j6Prop("zoom", 1);
                    C.j7();
                    h.z13()
                }
                if ("pounce" == this.options.selectorsEffect) {
                    t.width = [f, x.width];
                    t.height = [l, x.height];
                    t.top = [b.top, w.top];
                    t.left = [b.left, w.left];
                    n.width = [S.width, h.width];
                    n.height = [S.height, h.height];
                    n.top = [E.top, b.top];
                    C.j6({padding: ""});
                    N.j23(0).j6({height: 0, width: h.width, position: "relative"});
                    n.left = [E.left, N.j8().left];
                    r.width = [f, h.width];
                    h.self.j32(e.body).j6({position: "absolute", "z-index": 5001, left: n.left[0], top: n.top[0], width: n.width[0], height: n.height[0]});
                    o = $mjs(this.c.firstChild.cloneNode(false)).j32(e.body).j6({position: "absolute", "z-index": 5e3, left: t.left[0], top: t.top[0], visibility: "visible"});
                    $mjs(this.c.firstChild).j6({visibility: "hidden"});
                    C.j33();
                    a = this.c.j5("border-width");
                    this.c.j6Prop("border-width", 0)
                } else {
                    h.self.j32(this.c).j6({position: "absolute", "z-index": 5001, opacity: 0, left: "0px", top: "0px", height: "auto"});
                    o = $mjs(this.c.firstChild.cloneNode(false)).j32(this.c).j6({position: "absolute", "z-index": 5e3, left: "0px", top: "0px", visibility: "visible", height: "auto"});
                    $mjs(this.c.firstChild).j6({visibility: "hidden"});
                    C.j33();
                    n = {opacity: [0, 1]};
                    if (f != h.width || l != h.height) {
                        r.width = n.width = t.width = [f, h.width];
                        r.height = n.height = t.height = [l, h.height]
                    }
                    if (this.options.selectorsEffect == "fade") {
                        t.opacity = [1, 0]
                    }
                }
                (new e.PFX([this.c, h.self, o || this.c.firstChild], {duration: this.options.selectorsEffectSpeed, onComplete: function () {
                    if (o) {
                        o.j33();
                        o = null
                    }
                    if (null !== a) {
                        this.c.j6Prop("border-width", a)
                    }
                    u.call(this, function () {
                        h.unload();
                        $mjs(this.c.firstChild).j6({visibility: "visible"});
                        $mjs(h.self).j33();
                        h = null;
                        if (t.opacity) {
                            $mjs(this.c.firstChild).j6({opacity: 1})
                        }
                        this.ufx = false;
                        this.start(i, s);
                        if (p) {
                            p.j27(10)
                        }
                    }.j24(this))
                }.j24(this)})).start([r, n, t])
            };
            h.load(a.j24(this))
        } else {
            u.call(this, function () {
                this.c.j6({width: this.z7.width + "px", height: this.z7.height + "px"});
                this.start(i, s);
                if (p) {
                    p.j27(10)
                }
            }.j24(this))
        }
    }, z37: function (t) {
        var r, i, s, o;
        r = null;
        i = [];
        t = t || "";
        if ("" == t) {
            for (o in n.options) {
                r = n.options[o];
                switch (e.j1(n.defaults[o.j22()])) {
                    case"boolean":
                        r = r.toString().j18();
                        break;
                    case"number":
                        if (!("zoomWidth" === o.j22() || "zoomHeight" === o.j22()) || !/\%$/i.test(r)) {
                            r = parseFloat(r)
                        }
                        break;
                    default:
                        break
                }
                i[o.j22()] = r
            }
        } else {
            s = $mjs(t.split(";"));
            s.j14(function (t) {
                n.z39.j14(function (s) {
                    r = s.exec(t.j26());
                    if (r) {
                        switch (e.j1(n.defaults[r[1].j22()])) {
                            case"boolean":
                                i[r[1].j22()] = r[4] === "true";
                                break;
                            case"number":
                                i[r[1].j22()] = ("zoomWidth" === r[1].j22() || "zoomHeight" === r[1].j22()) && /\%$/.test(r[4]) ? r[4] : parseFloat(r[4]);
                                break;
                            default:
                                i[r[1].j22()] = r[4]
                        }
                    }
                }, this)
            }, this)
        }
        if (false === i.selectorsEffect) {
            i.selectorsEffect = "false"
        }
        return i
    }, z27: function () {
        var t, n;
        if (!this.z4) {
            this.z4 = {self: $mjs(document.createElement("DIV")).j2("MagicZoomPup").j6({zIndex: 10, position: "absolute", overflow: "hidden"}).hide(), width: 20, height: 20, bgColor: ""};
            this.c.appendChild(this.z4.self);
            this.z4.bgColor = this.z4.self.j5("background-color")
        }
        if (n = this.c.j29("thumb")) {
            this.z4.self.j6({cursor: n._o.disableExpand ? "move" : ""})
        }
        if (this.options.entireImage) {
            this.z4.self.j6({"border-width": "0px", cursor: "default"})
        }
        this.z4.z38 = false;
        this.z4.height = this.zoomViewHeight / (this.z1.height / this.z7.height);
        this.z4.width = this.options.zoomWidth / (this.z1.width / this.z7.width);
        if (this.z4.width > this.z7.width) {
            this.z4.width = this.z7.width
        }
        if (this.z4.height > this.z7.height) {
            this.z4.height = this.z7.height
        }
        this.z4.width = Math.round(this.z4.width);
        this.z4.height = Math.round(this.z4.height);
        this.z4.borderWidth = this.z4.self.j19("borderLeftWidth").j17();
        this.z4.self.j6({width: this.z4.width - 2 * (e.j21.backCompat ? 0 : this.z4.borderWidth) + "px", height: this.z4.height - 2 * (e.j21.backCompat ? 0 : this.z4.borderWidth) + "px"});
        if (!this.options.opacityReverse && !this.options.rightClick) {
            this.z4.self.j23(parseFloat(this.options.opacity / 100));
            if (this.z4.z42) {
                this.z4.self.removeChild(this.z4.z42);
                this.z4.z42 = null
            }
        } else {
            if (this.z4.z42) {
                this.z4.z42.src = this.z7.self.src
            } else {
                t = this.z7.self.cloneNode(false);
                t.unselectable = "on";
                this.z4.z42 = $mjs(this.z4.self.appendChild(t)).j6({position: "absolute", zIndex: 5})
            }
            if (this.options.opacityReverse) {
                this.z4.z42.j6(this.z7.self.j7());
                this.z4.self.j23(1);
                if (e.j21.ieMode && e.j21.ieMode < 9) {
                    this.z4.z42.j23(1)
                }
            } else {
                if (this.options.rightClick) {
                    this.z4.z42.j23(.009)
                }
                this.z4.self.j23(parseFloat(this.options.opacity / 100))
            }
        }
    }, z43: function (t, n) {
        if (!this.z30 || t === undefined || t.skipAnimation) {
            return false
        }
        if (!this.z4) {
            return false
        }
        var r = /touch/i.test(t.type) && t.touches.length > 1;
        var i = "touchend" == t.type && !t.continueAnimation;
        if ((!this.divTag || t.type != "mouseout") && !r) {
            $mjs(t).stop()
        }
        if (n === undefined) {
            n = $mjs(t).j15()
        }
        if (this.z6 === null || this.z6 === undefined) {
            this.z6 = this.z7.getBox()
        }
        if (i || "mouseout" == t.type && !this.c.hasChild(t.getRelated()) || r || n.x > this.z6.right || n.x < this.z6.left || n.y > this.z6.bottom || n.y < this.z6.top) {
            this.pause();
            return false
        }
        this.activatedEx = false;
        if (t.type == "mouseout" || t.type == "touchend") {
            return false
        }
        if (this.options.dragMode && !this.z45) {
            return false
        }
        if (!this.options.moveOnClick) {
            n.x -= this.ddx;
            n.y -= this.ddy
        }
        if (n.x + this.z4.width / 2 >= this.z6.right) {
            n.x = this.z6.right - this.z4.width / 2
        }
        if (n.x - this.z4.width / 2 <= this.z6.left) {
            n.x = this.z6.left + this.z4.width / 2
        }
        if (n.y + this.z4.height / 2 >= this.z6.bottom) {
            n.y = this.z6.bottom - this.z4.height / 2
        }
        if (n.y - this.z4.height / 2 <= this.z6.top) {
            n.y = this.z6.top + this.z4.height / 2
        }
        this.options.x = n.x - this.z6.left;
        this.options.y = n.y - this.z6.top;
        if (this.z44 === null) {
            this.z44 = setTimeout(this.z16, 10)
        }
        if (e.defined(this.hintVisible) && this.hintVisible) {
            this.hintVisible = false;
            this.hint.hide()
        }
        return true
    }, show: function (t) {
        if (t && !this.z44) {
            return
        }
        var n, r, i, s, o, u, a, f, l, c = this.options, h = this.z4;
        n = h.width / 2;
        r = h.height / 2;
        h.self.style.left = c.x - n + this.z7.border.left + "px";
        h.self.style.top = c.y - r + this.z7.border.top + "px";
        if (this.options.opacityReverse) {
            h.z42.style.left = "-" + (parseFloat(h.self.style.left) + h.borderWidth) + "px";
            h.z42.style.top = "-" + (parseFloat(h.self.style.top) + h.borderWidth) + "px"
        }
        i = (this.options.x - n) * (this.z1.width / this.z7.width);
        s = (this.options.y - r) * (this.z1.height / this.z7.height);
        if (this.z1.width - i < c.zoomWidth) {
            i = this.z1.width - c.zoomWidth;
            if (i < 0) {
                i = 0
            }
        }
        if (this.z1.height - s < this.zoomViewHeight) {
            s = this.z1.height - this.zoomViewHeight;
            if (s < 0) {
                s = 0
            }
        }
        if (document.documentElement.dir == "rtl") {
            i = (c.x + h.width / 2 - this.z7.width) * (this.z1.width / this.z7.width)
        }
        i = Math.round(i);
        s = Math.round(s);
        if (c.smoothing === false || !h.z38) {
            this.z1.self.style.left = -i + "px";
            this.z1.self.style.top = -s + "px"
        } else {
            o = parseInt(this.z1.self.style.left);
            u = parseInt(this.z1.self.style.top);
            a = -i - o;
            f = -s - u;
            if (!a && !f) {
                this.z44 = null;
                return
            }
            a *= c.smoothingSpeed / 100;
            if (a < 1 && a > 0) {
                a = 1
            } else {
                if (a > -1 && a < 0) {
                    a = -1
                }
            }
            o += a;
            f *= c.smoothingSpeed / 100;
            if (f < 1 && f > 0) {
                f = 1
            } else {
                if (f > -1 && f < 0) {
                    f = -1
                }
            }
            u += f;
            this.z1.self.style.left = o + "px";
            this.z1.self.style.top = u + "px"
        }
        if (!h.z38) {
            if (this.z2) {
                this.z2.stop();
                this.z2.options.onComplete = e.$F;
                this.z2.options.duration = c.zoomFadeInSpeed;
                this.z47.self.j23(0);
                this.z2.start({opacity: [0, 1]})
            }
            if (/^(left|right|top|bottom)$/i.test(c.zoomPosition)) {
                this.z47.self.j32(e.body)
            }
            if (c.zoomPosition != "inner") {
                h.self.show()
            }
            this.z47.self.j6(this.adjustPosition(/^(left|right|top|bottom)$/i.test(c.zoomPosition) && !this.options.alwaysShowZoom));
            if (c.opacityReverse) {
                this.c.j6Prop("background-color", this.z4.bgColor);
                this.z7.self.j23(parseFloat((100 - c.opacity) / 100))
            }
            h.z38 = true
        }
        if (this.z44) {
            this.z44 = setTimeout(this.z16, 1e3 / c.fps)
        }
    }, adjustPosition: function (e) {
        var t = this.t13(5), n = this.z7.self.j9(), r = this.options.zoomPosition, i = this.z47, s = this.options.zoomDistance, o = i.self.j7(), u = i.initTopPos, a = i.initLeftPos, f = {left: i.initLeftPos, top: i.initTopPos};
        if ("inner" === r || this.z47.custom) {
            return f
        }
        e || (e = false);
        i.lastLeftPos += (n[i.adjustX.edge] - this.z7Rect[i.adjustX.edge]) / i.adjustX.ratio;
        i.z21 += (n[i.adjustY.edge] - this.z7Rect[i.adjustY.edge]) / i.adjustY.ratio;
        this.z7Rect = n;
        f.left = a = i.lastLeftPos;
        f.top = u = i.z21;
        if (e) {
            if ("left" == r || "right" == r) {
                if ("left" == r && t.left > a) {
                    f.left = n.left - t.left >= o.width ? n.left - o.width - 2 : t.right - n.right - 2 > n.left - t.left - 2 ? n.right + 2 : n.left - o.width - 2
                } else {
                    if ("right" == r && t.right < a + o.width) {
                        f.left = t.right - n.right >= o.width ? n.right + 2 : n.left - t.left - 2 > t.right - n.right - 2 ? n.left - o.width - 2 : n.right + 2
                    }
                }
            } else {
                if ("top" == r || "bottom" == r) {
                    f.left = Math.max(t.left + 2, Math.min(t.right, a + o.width) - o.width);
                    if ("top" == r && t.top > u) {
                        f.top = n.top - t.top >= o.height ? n.top - o.height - 2 : t.bottom - n.bottom - 2 > n.top - t.top - 2 ? n.bottom + 2 : n.top - o.height - 2
                    } else {
                        if ("bottom" == r && t.bottom < u + o.height) {
                            f.top = t.bottom - n.bottom >= o.height ? n.bottom + 2 : n.top - t.top - 2 > t.bottom - n.bottom - 2 ? n.top - o.height - 2 : n.bottom + 2
                        }
                    }
                }
            }
        }
        return f
    }, t13: function (t) {
        t = t || 0;
        var n = e.j21.touchScreen ? {width: window.innerWidth, height: window.innerHeight} : $mjs(window).j7(), r = $mjs(window).j10();
        return{left: r.x + t, right: r.x + n.width - t, top: r.y + t, bottom: r.y + n.height - t}
    }, onresize: function (t) {
        var n, r, i = {width: this.z7.width, height: this.z7.height};
        this.z7.z13();
        if (this.z47.custom) {
            r = $mjs(this.z47.self.parentNode).j7();
            if (/%$/i.test(this.z47.initWidth)) {
                this.options.zoomWidth = parseInt(this.z47.initWidth) / 100 * r.width
            }
            if (/%$/i.test(this.z47.initHeight)) {
                this.options.zoomHeight = parseInt(this.z47.initHeight) / 100 * r.height
            }
        } else {
            if ("inner" === this.options.zoomPosition) {
                this.options.zoomWidth = this.z7.width;
                this.options.zoomHeight = this.z7.height
            } else {
                this.options.zoomWidth *= this.z7.width / i.width;
                this.options.zoomHeight *= this.z7.height / i.height
            }
        }
        n = this.z47.z41.j7();
        this.zoomViewHeight = this.options.zoomHeight - n.height;
        if (this.options.showTitle == "bottom") {
            $mjs(this.z1.self.parentNode).j6Prop("height", this.options.zoomHeight - n.height)
        }
        this.z47.self.j6("inner" == this.options.zoomPosition ? {} : {height: this.options.zoomHeight + "px", width: this.options.zoomWidth + "px"});
        if (e.j21.trident4 && this.z47.z23) {
            this.z47.z23.j6({width: this.options.zoomWidth, height: this.options.zoomHeight})
        }
        if (this.options.opacityReverse && this.z4.z42) {
            this.z4.z42.j6(this.z7.self.j7())
        }
        this.z4.height = this.zoomViewHeight / (this.z1.height / this.z7.height);
        this.z4.width = this.options.zoomWidth / (this.z1.width / this.z7.width);
        if (this.z4.width > this.z7.width) {
            this.z4.width = this.z7.width
        }
        if (this.z4.height > this.z7.height) {
            this.z4.height = this.z7.height
        }
        this.z4.width = Math.round(this.z4.width);
        this.z4.height = Math.round(this.z4.height);
        this.z4.borderWidth = this.z4.self.j19("borderLeftWidth").j17();
        this.z4.self.j6({width: this.z4.width - 2 * (e.j21.backCompat ? 0 : this.z4.borderWidth) + "px", height: this.z4.height - 2 * (e.j21.backCompat ? 0 : this.z4.borderWidth) + "px"});
        if (this.z4.z38) {
            this.z47.self.j6(this.adjustPosition(/^(left|right|top|bottom)$/i.test(this.options.zoomPosition) && !this.options.alwaysShowZoom));
            this.options.x *= this.z7.width / i.width;
            this.options.y *= this.z7.height / i.height;
            this.show()
        }
    }, activate: function (t, n) {
        t = e.defined(t) ? t : true;
        this.activatedEx = true;
        if (!this.z1) {
            this.z18();
            return
        }
        if (this.options.disableZoom) {
            return
        }
        this.z30 = true;
        if (t) {
            if (e.defined(n)) {
                this.z43(n);
                return
            }
            if (!this.options.preservePosition) {
                this.options.x = this.z7.width / 2;
                this.options.y = this.z7.height / 2
            }
            this.show()
        }
    }, pause: function () {
        var e = this.z4 && this.z4.z38;
        if (this.z44) {
            clearTimeout(this.z44);
            this.z44 = null
        }
        if (!this.options.alwaysShowZoom && this.z4 && this.z4.z38) {
            this.z4.z38 = false;
            this.z4.self.hide();
            if (this.z2) {
                this.z2.stop();
                this.z2.options.onComplete = this.z47.z22;
                this.z2.options.duration = this.options.zoomFadeOutSpeed;
                var t = this.z47.self.j19("opacity");
                this.z2.start({opacity: [t, 0]})
            } else {
                this.z47.hide()
            }
            if (this.options.opacityReverse) {
                this.c.j6Prop("background-color", "");
                this.z7.self.j23(1)
            }
        }
        this.z6 = null;
        if (this.options.clickToActivate) {
            this.z30 = false
        }
        if (this.options.dragMode) {
            this.z45 = false
        }
        if (this.hint) {
            this.hintVisible = true;
            this.hint.show()
        }
    }, mousedown: function (t) {
        var n = t.getButton(), r = /touch/i.test(t.type), i = e.now();
        if (3 == n) {
            return true
        }
        if (r) {
            if (t.targetTouches.length > 1) {
                return
            }
            this.c.j30("magiczoom:event:lastTap", {id: t.targetTouches[0].identifier, x: t.targetTouches[0].clientX, y: t.targetTouches[0].clientY, ts: i});
            if (this.z1 && !this.z30) {
                return
            }
        }
        if (!(r && t.touches.length > 1)) {
            $mjs(t).stop()
        }
        if ("click" == this.options.initializeOn && !this.z7) {
            this.initMouseEvent = t;
            this.z18();
            return
        }
        if ("mouseover" == this.options.initializeOn && !this.z7 && (t.type == "mouseover" || t.type == "touchstart")) {
            this.initMouseEvent = t;
            this.z18();
            this.c.je2("mouseover", this.z14);
            return
        }
        if (this.options.disableZoom) {
            return
        }
        if (this.z7 && !this.z1.ready) {
            return
        }
        if (this.z1 && this.options.clickToDeactivate && this.z30 && !r) {
            this.z30 = false;
            this.pause();
            return
        }
        if (this.z1 && !this.z30) {
            this.activate(true, t);
            t.stopImmediatePropagation && t.stopImmediatePropagation();
            if (this.c.j29("thumb")) {
                this.c.j29("thumb").dblclick = true
            }
        }
        if (this.z30 && this.options.dragMode) {
            this.z45 = true;
            if (!this.options.moveOnClick) {
                if (this.z6 === null || this.z6 === undefined) {
                    this.z6 = this.z7.getBox()
                }
                var s = t.j15();
                this.ddx = s.x - this.options.x - this.z6.left;
                this.ddy = s.y - this.options.y - this.z6.top;
                if (Math.abs(this.ddx) > this.z4.width / 2 || Math.abs(this.ddy) > this.z4.height / 2) {
                    this.z45 = false;
                    return
                }
            } else {
                this.z43(t)
            }
        }
    }, mouseup: function (t) {
        var n = t.getButton(), r = /touch/i.test(t.type), i = e.now(), s = null, o = this.options.preservePosition;
        if (3 == n) {
            return true
        }
        if (r) {
            s = this.c.j29("magiczoom:event:lastTap");
            if (!s || t.targetTouches.length > 1) {
                return
            }
            if (s.id == t.changedTouches[0].identifier && i - s.ts <= 200 && Math.sqrt(Math.pow(t.changedTouches[0].clientX - s.x, 2) + Math.pow(t.changedTouches[0].clientY - s.y, 2)) <= 15) {
                if (this.z1 && !this.z30) {
                    if (this.z6 === null || this.z6 === undefined) {
                        this.z6 = this.z7.getBox()
                    }
                    this.options.preservePosition = true;
                    this.options.x = t.j15().x - this.z6.left;
                    this.options.y = t.j15().y - this.z6.top;
                    this.activate(true);
                    this.options.preservePosition = o;
                    this.options.dragMode && (this.z45 = true);
                    this.ddx = 0;
                    this.ddy = 0;
                    t.continueAnimation = true;
                    t.zoomActivation = true;
                    t.stopImmediatePropagation && t.stopImmediatePropagation()
                }
                $mjs(t).stop();
                return
            }
        }
        $mjs(t).stop();
        if (this.options.dragMode) {
            this.z45 = false
        }
    }};
    if (e.j21.trident) {
        try {
            document.execCommand("BackgroundImageCache", false, true)
        } catch (r) {
        }
    }
    $mjs(document).je1("domready", function () {
        e.insertCSS(".mz-tmp-clone", "margin: 0 !important;border: 0 !important;padding: 0 !important;position: relative  !important;height: 0 !important;min-height: 0 !important;z-index: -1;opacity: 0;", "mz-css");
        $mjs(document).je1("mousemove", n.z8)
    });
    var i = new e.Class({self: null, ready: false, options: {width: -1, height: -1, onload: e.$F, onabort: e.$F, onerror: e.$F}, width: 0, height: 0, nWidth: 0, nHeight: 0, border: {left: 0, right: 0, top: 0, bottom: 0}, margin: {left: 0, right: 0, top: 0, bottom: 0}, padding: {left: 0, right: 0, top: 0, bottom: 0}, _timer: null, _handlers: {onload: function (e) {
        if (e) {
            $mjs(e).stop()
        }
        this._unbind();
        if (this.ready) {
            return
        }
        this.ready = true;
        this.calc();
        this._cleanup();
        this.options.onload.j27(1)
    }, onabort: function (e) {
        if (e) {
            $mjs(e).stop()
        }
        this._unbind();
        this.ready = false;
        this._cleanup();
        this.options.onabort.j27(1)
    }, onerror: function (e) {
        if (e) {
            $mjs(e).stop()
        }
        this._unbind();
        this.ready = false;
        this._cleanup();
        this.options.onerror.j27(1)
    }}, _bind: function () {
        $mjs(["load", "abort", "error"]).j14(function (e) {
            this.self.je1(e, this._handlers["on" + e].j16(this).j28(1))
        }, this)
    }, _unbind: function () {
        $mjs(["load", "abort", "error"]).j14(function (e) {
            this.self.je2(e)
        }, this)
    }, _cleanup: function () {
        if (this.self.j29("new")) {
            var e = this.self.parentNode;
            this.self.j33().j31("new").j6({position: "static", top: "auto"});
            e.kill()
        }
    }, init: function (t, n) {
        this.options = e.extend(this.options, n);
        var r = this.self = $mjs(t) || e.$new("img", {}, {"max-width": "none", "max-height": "none"}).j32(e.$new("div").j2("magic-temporary-img").j6({position: "absolute", top: -1e4, width: 10, height: 10, overflow: "hidden"}).j32(e.body)).j30("new", true), i = function () {
            if (this.isReady()) {
                this._handlers.onload.call(this)
            } else {
                this._handlers.onerror.call(this)
            }
            i = null
        }.j24(this);
        this._bind();
        if (!t.src) {
            r.src = t
        } else {
            r.src = t.src
        }
        if (r && r.complete) {
            this._timer = i.j27(100)
        }
    }, destroy: function () {
        if (this._timer) {
            try {
                clearTimeout(this._timer)
            } catch (e) {
            }
            this._timer = null
        }
        this._unbind();
        this._cleanup();
        this.ready = false;
        return this
    }, isReady: function () {
        var e = this.self;
        return e.naturalWidth ? e.naturalWidth > 0 : e.readyState ? "complete" == e.readyState : e.width > 0
    }, calc: function () {
        this.nWidth = this.self.naturalWidth || this.self.width;
        this.nHeight = this.self.naturalHeight || this.self.height;
        if (this.options.width > 0) {
            this.self.j6Prop("width", this.options.width)
        } else {
            if (this.options.height > 0) {
                this.self.j6Prop("height", this.options.height)
            }
        }
        this.width = this.self.width;
        this.height = this.self.height;
        $mjs(["left", "right", "top", "bottom"]).j14(function (e) {
            this.margin[e] = this.self.j5("margin-" + e).j17();
            this.padding[e] = this.self.j5("padding-" + e).j17();
            this.border[e] = this.self.j5("border-" + e + "-width").j17()
        }, this)
    }});
    var s = {version: "v2.1.mzp-4.5.12", options: {}, lang: {}, start: function (t) {
        this.thumbs = $mjs(window).j29("magicthumb:items", $mjs([]));
        var n = null, r = null, i = $mjs([]), u = arguments.length > 1 ? e.extend(e.detach(s.options), arguments[1]) : s.options;
        if (t) {
            r = $mjs(t);
            if (r && (" " + r.className + " ").match(/\s(MagicThumb|MagicZoomPlus)\s/)) {
                i.push(r)
            } else {
                return false
            }
        } else {
            i = $mjs(e.$A(e.body.byTag("A")).filter(function (e) {
                return e.className.has("MagicThumb", " ")
            }))
        }
        i.forEach(function (e) {
            if (n = $mjs(e).j29("thumb")) {
                n.start()
            } else {
                new o(e, u)
            }
        });
        return true
    }, stop: function (e) {
        var t = null;
        if (e) {
            if ($mjs(e) && (t = $mjs(e).j29("thumb"))) {
                t = t.t16(t.t27 || t.id).stop();
                delete t;
                return true
            }
            return false
        }
        while (this.thumbs.length) {
            t = this.thumbs[this.thumbs.length - 1].stop();
            delete t
        }
        return true
    }, refresh: function (e) {
        var t = null;
        if (e) {
            if ($mjs(e)) {
                if (t = $mjs(e).j29("thumb")) {
                    t = this.stop(e);
                    delete t
                }
                this.start.j27(150, e);
                return true
            }
            return false
        }
        this.stop();
        this.start.j27(150);
        return true
    }, update: function (e, t, n, r) {
        var i = $mjs(e), s = null;
        if (i && (s = i.j29("thumb"))) {
            s.t16(s.t27 || s.id).update(t, n, r)
        }
    }, expand: function (e) {
        var t = null;
        if ($mjs(e) && (t = $mjs(e).j29("thumb"))) {
            t.expand();
            return true
        }
        return false
    }, restore: function (e) {
        var t = null;
        if ($mjs(e) && (t = $mjs(e).j29("thumb"))) {
            t.restore();
            return true
        }
        return false
    }};
    var o = new e.Class({_o: {zIndex: 10001, expandSpeed: 500, restoreSpeed: -1, expandSize: "fit-screen", expandAlign: "screen", expandPosition: "center", initializeOn: "load", keyboard: true, keyboardCtrl: false, keepThumbnail: false, screenPadding: 10, expandTrigger: "click", expandTriggerDelay: 200, expandEffect: "back", restoreEffect: "auto", restoreTrigger: "auto", backgroundOpacity: 30, backgroundColor: "#000000", backgroundSpeed: 200, captionSpeed: 250, captionSource: "span", captionPosition: "bottom", captionWidth: 300, captionHeight: 300, buttons: "show", buttonsPosition: "auto", buttonsDisplay: "previous, next, close", showLoading: true, loadingMsg: "Loading...", loadingOpacity: 75, slideshowEffect: "dissolve", slideshowSpeed: 500, slideshowLoop: true, selectorsChange: "click", selectorsMouseoverDelay: 60, selectorsEffect: "dissolve", selectorsEffectSpeed: 400, selectorsClass: "", group: null, link: "", linkTarget: "_self", cssClass: "", hint: true, hintText: "Expand", hintPosition: "tl", hintOpacity: 75, hintClass: "MagicThumbHint", rightClick: "false", disableExpand: false, panZoom: true}, _deprecated: {clickToInitialize: function (e) {
        e = ("" + e).j18();
        if (e && "load" == this._o.initializeOn) {
            this._o.initializeOn = "click"
        }
    }, imageSize: function (e) {
        if ("fit-screen" == this._o.expandSize && "original" == e) {
            this._o.expandSize = "original"
        }
    }, swapImage: function (e) {
        if ("click" == this._o.selectorsChange && "mouseover" == e) {
            this._o.selectorsChange = "mouseover"
        }
    }}, _lang: {buttonPrevious: "Previous", buttonNext: "Next", buttonClose: "Close"}, thumbs: [], t29: null, r: null, id: null, t27: null, group: null, params: {}, ready: false, dblclick: false, mzParams: "zoom-position: inner; hint: false; click-to-activate: false; drag-mode: false; initialize-on: load; show-loading: false; entire-image: false; zoom-window-effect: false; disable-zoom: false; opacity-reverse: false;", z7: null, z1: null, content: null, t22: null, z3: null, t23: null, t25: null, t26: null, hint: null, captionText: null, state: "uninitialized", t28: [], cbs: {previous: {index: 0, title: "buttonPrevious"}, next: {index: 1, title: "buttonNext"}, close: {index: 2, title: "buttonClose"}}, position: {top: "auto", bottom: "auto", left: "auto", right: "auto"}, size: {width: -1, height: -1}, media: "img", easing: {linear: ["", ""], sine: ["Out", "In"], quad: ["Out", "In"], cubic: ["Out", "In"], back: ["Out", "In"], elastic: ["Out", "In"], bounce: ["Out", "In"], expo: ["Out", "In"]}, fps: 50, hCaption: false, scrPad: {x: 0, y: 0}, ieBack: e.j21.trident && (e.j21.trident4 || e.j21.backCompat) || false, init: function (t, n) {
        this.thumbs = e.win.j29("magicthumb:items", $mjs([]));
        this.t29 = (this.t29 = e.win.j29("magicthumb:holder")) ? this.t29 : e.win.j29("magicthumb:holder", e.$new("div").j6({position: "absolute", top: -1e4, width: 10, height: 10, overflow: "hidden"}).j32(e.body));
        this.t28 = $mjs(this.t28);
        this.r = $mjs(t) || e.$new("A");
        this._o.captionSource = "a:title";
        this._o.keepThumbnail = true;
        this.z37(n);
        this.z37(this.r.rel);
        this.parseExOptions();
        this.setLang(s.lang);
        this.scrPad.y = this.scrPad.x = this._o.screenPadding * 2;
        this.scrPad.x += this.ieBack ? e.body.j5("margin-left").j17() + e.body.j5("margin-right").j17() : 0;
        this.r.id = this.id = this.r.id || "mt-" + Math.floor(Math.random() * e.now());
        if (arguments.length > 2) {
            this.params = arguments[2]
        }
        this.params.thumbnail = this.params.thumbnail || this.r.byTag("IMG")[0];
        this.params.content = this.params.content || this.r.href;
        this.t27 = this.params.t27 || null;
        this.group = this._o.group || null;
        this.hCaption = /(left|right)/i.test(this._o.captionPosition);
        if (this._o.disableExpand) {
            this._o.hint = false
        }
        if (this.t27) {
            this._o.initializeOn = "load"
        }
        this.mzParams += "right-click : " + ("true" == this._o.rightClick || "expanded" == this._o.rightClick);
        if ((" " + this.r.className + " ").match(/\s(MagicThumb|MagicZoomPlus)\s/)) {
            if (this.r.zoom && !this.r.zoom.options.disableZoom) {
                this._o.showLoading = false
            }
            this.r.j6({position: "relative", display: e.j21.gecko181 ? "block" : "inline-block"});
            if (this._o.disableExpand) {
                this.r.j6({cursor: "default"})
            }
            if ("true" != this._o.rightClick && "original" != this._o.rightClick) {
                this.r.je1("contextmenu", function (e) {
                    $mjs(e).stop()
                })
            }
            this.r.j30("j24:click", function (t) {
                var n = this.j29("thumb"), r = e.now(), i;
                $mjs(t).stop();
                if ("touchend" === t.type) {
                    n._o.expandEffect = "linear";
                    n._o.restoreEffect = "linear";
                    n._o.panZoom = false;
                    n._o.keepThumbnail = false;
                    n.fps = 30
                }
                if ("click" === t.type) {
                    i = this.j29("magicthumb:event:click");
                    if (!i) {
                        return
                    }
                    if (Math.sqrt(Math.pow(t.j15().x - i.x, 2) + Math.pow(t.j15().y - i.y, 2)) > 5 || r - i.ts > 200) {
                        return false
                    }
                }
                if ((e.j21.trident || e.j21.presto && e.j21.version < 250) && n.dblclick) {
                    n.dblclick = false;
                    return false
                }
                if (!n.ready) {
                    if (!this.j29("clicked")) {
                        this.j30("clicked", true);
                        if ("click" == n._o.initializeOn || "touchend" === t.type) {
                            try {
                                if (n.r.zoom && !n.r.zoom.options.disableZoom && (e.j21.trident || e.j21.presto && e.j21.version < 250 || !n.r.zoom.z1.ready)) {
                                    this.j30("clicked", false)
                                }
                            } catch (s) {
                            }
                            if (n.group && "" != n.group) {
                                n.t15(n.group, true).forEach(function (e) {
                                    if (e != n) {
                                        e.start()
                                    }
                                })
                            }
                            n.start()
                        } else {
                        }
                    }
                } else {
                    if ("click" == n._o.expandTrigger || "touchend" === t.type) {
                        n.expand()
                    }
                }
                return false
            }.j16(this.r));
            this.r.je1("mousedown", function (t) {
                if (3 == t.getButton()) {
                    return true
                }
                this.r.j30("magicthumb:event:click", {ts: e.now(), x: t.j15().x, y: t.j15().y})
            }.j16(this));
            this.r.je1("click", this.r.j29("j24:click"));
            if (e.j21.touchScreen) {
                this.r.je1("touchstart", function (t) {
                    var n = e.now();
                    if (t.targetTouches.length > 1) {
                        return
                    }
                    this.r.j30("magicthumb:event:lastTap", {id: t.targetTouches[0].identifier, ts: n, x: t.targetTouches[0].clientX, y: t.targetTouches[0].clientY})
                }.j16(this));
                this.r.je1("touchend", function (t) {
                    var n = e.now(), r = this.r.j29("magicthumb:event:lastTap");
                    if (!r || t.changedTouches.length > 1) {
                        return
                    }
                    if (r.id == t.changedTouches[0].identifier && n - r.ts <= 200 && Math.sqrt(Math.pow(t.changedTouches[0].clientX - r.x, 2) + Math.pow(t.changedTouches[0].clientY - r.y, 2)) <= 15) {
                        t.stop();
                        this.r.j29("j24:click")(t);
                        return
                    }
                }.j16(this))
            }
            this.r.j30("j24:hover", function (e) {
                var t = this.j29("thumb"), n = t.t16(t.t27 || t.id), r = t.hint, i = "mouseover" == t._o.expandTrigger;
                if (!e.getRelated() || e.getRelated() === t.content) {
                    e.stop();
                    return
                }
                $mjs(e).stop();
                if (!t.ready && "mouseover" == t._o.initializeOn) {
                    if (!this.j29("clicked") && "mouseover" == t._o.expandTrigger) {
                        this.j30("clicked", true)
                    }
                    if (t.group && "" != t.group) {
                        t.t15(t.group, true).forEach(function (e) {
                            if (e != t) {
                                e.start()
                            }
                        })
                    }
                    t.start()
                } else {
                    switch (e.type) {
                        case"mouseout":
                            if (r && "inz30" == t.state) {
                                n.hint.show()
                            }
                            if (i) {
                                if (t.hoverTimer) {
                                    clearTimeout(t.hoverTimer)
                                }
                                t.hoverTimer = false;
                                return
                            }
                            break;
                        case"mouseover":
                            if (r && "inz30" == t.state) {
                                n.hint.hide()
                            }
                            if (i) {
                                t.hoverTimer = t.expand.j24(t).j27(t._o.expandTriggerDelay)
                            }
                            break
                    }
                }
            }.j16(this.r)).je1("mouseover", this.r.j29("j24:hover")).je1("mouseout", this.r.j29("j24:hover"))
        }
        this.r.j30("thumb", this);
        if (this.params && e.defined(this.params.index) && "number" == typeof this.params.index) {
            this.thumbs.splice(this.params.index, 0, this)
        } else {
            this.thumbs.push(this)
        }
        if ("load" == this._o.initializeOn) {
            this.start()
        } else {
            this.t6(true)
        }
    }, start: function (e, t) {
        if (this.ready || "uninitialized" != this.state) {
            return
        }
        this.state = "initializing";
        if (e) {
            this.params.thumbnail = e
        }
        if (t) {
            this.params.content = t
        }
        if ($mjs(["fit-screen", "original"]).contains(this._o.expandSize)) {
            this.size = {width: -1, height: -1}
        }
        this._o.restoreSpeed = this._o.restoreSpeed >= 0 ? this._o.restoreSpeed : this._o.expandSpeed;
        var n = [this._o.expandEffect, this._o.restoreEffect];
        this._o.expandEffect = n[0]in this.easing ? n[0] : n[0] = "linear";
        this._o.restoreEffect = n[1]in this.easing ? n[1] : n[0];
        if (!this.z7) {
            this.t2()
        }
    }, stop: function (t) {
        if ("uninitialized" == this.state) {
            return this
        }
        t = t || false;
        if (this.z7) {
            this.z7.destroy()
        }
        if (this.z1) {
            this.z1.destroy()
        }
        if (this.t22) {
            if (this.t22.j29("j24:external-click")) {
                e.doc.je2("click", this.t22.j29("j24:external-click"));
                e.j21.touchScreen && e.doc.je2("touchstart", this.t22.j29("j24:external-click"))
            }
            if (this.t22.j29("j24:window:resize")) {
                $mjs(window).je2("resize", this.t22.j29("j24:window:resize"));
                $mjs(window).je2("scroll", this.t22.j29("j24:window:resize"))
            }
            this.t22 = this.t22.kill()
        }
        this.z7 = null, this.z1 = null, this.t22 = null, this.z3 = null, this.t23 = null, this.t25 = null, this.t26 = null, this.ready = false, this.state = "uninitialized";
        this.r.j30("clicked", false);
        if (this.hint) {
            this.hint.j33()
        }
        this.t28.forEach(function (e) {
            e.je2(this._o.selectorsChange, e.j29("j24:replace"));
            if ("mouseover" == this._o.selectorsChange) {
                e.je2("mouseout", e.j29("j24:replace"))
            }
            if (!e.j29("thumb") || this == e.j29("thumb")) {
                return
            }
            e.j29("thumb").stop();
            delete e
        }, this);
        this.t28 = $mjs([]);
        if (!t) {
            if ((" " + this.r.className + " ").match(/\s(MagicThumb|MagicZoomPlus)\s/)) {
                this.r.je3();
                e.storage[this.r.$J_UUID] = null;
                delete e.storage[this.r.$J_UUID]
            }
            this.r.j31("thumb");
            return this.thumbs.splice(this.thumbs.indexOf(this), 1)
        }
        return this
    }, swap: function (t, n) {
        n = n || false;
        if (!n && (!t.ready || "inz30" != t.state) || "inz30" != this.state) {
            return
        }
        this.state = "updating";
        t.state = "updating";
        var r = this.t16(this.t27 || this.id), i = r.r.byTag("img")[0], s, o = {}, u = {}, a = {}, f, l, c, h, p, d, v, m = null;
        s = function (e, t) {
            e.href = this.z1.self.src;
            e.j30("thumb", this);
            this.state = t.state = "inz30";
            this.setupHint();
            if (this._o.disableExpand) {
                e.j6({cursor: "default"})
            } else {
                e.j6({cursor: ""})
            }
            if ("" != this._o.selectorsClass) {
                (t.selector || t.r).j3(this._o.selectorsClass);
                (this.selector || this.r).j2(this._o.selectorsClass)
            }
        };
        if (!n) {
            if (r.hint) {
                r.hint.hide()
            }
            if ("pounce" == this._o.selectorsEffect) {
                f = $mjs((this.selector || this.r).byTag("img")[0]), f = f || this.selector || this.r, l = $mjs((t.selector || t.r).byTag("img")[0]);
                l = l || t.selector || t.r;
                c = this.z7.self.j8(), h = f.j8(), p = l.j8(), v = f.j7(), d = l.j7();
                o.width = [this.z7.width, v.width];
                o.height = [this.z7.height, v.height];
                o.top = [c.top, h.top];
                o.left = [c.left, h.left];
                u.width = [d.width, t.z7.width];
                u.height = [d.height, t.z7.height];
                u.top = [p.top, c.top];
                u.left = [p.left, c.left];
                a.width = [this.z7.width, t.z7.width];
                a.height = [this.z7.height, t.z7.height];
                m = $mjs(i.cloneNode(false)).j32(e.body).j6({position: "absolute", "z-index": 5e3, left: o.left[0], top: o.top[0], visibility: "visible"});
                i.j6({visibility: "hidden"});
                t.z7.self.j32(e.body).j6({position: "absolute", "z-index": 5001, left: u.left[0], top: u.top[0], width: u.width[0], height: u.height[0]})
            } else {
                t.z7.self.j6({position: "absolute", "z-index": 1, left: "0px", top: "0px"}).j32(r.r, "top").j23(0);
                u = {opacity: [0, 1]};
                if (this.z7.width != t.z7.width || this.z7.height != t.z7.height) {
                    a.width = u.width = o.width = [this.z7.width, t.z7.width];
                    a.height = u.height = o.height = [this.z7.height, t.z7.height]
                }
                if (this._o.selectorsEffect == "fade") {
                    o.opacity = [1, 0]
                }
            }
            (new e.PFX([r.r, t.z7.self, m || i], {duration: "false" == "" + this._o.selectorsEffect ? 0 : this._o.selectorsEffectSpeed, onComplete: function (e, t, n) {
                if (m) {
                    m.j33();
                    m = null
                }
                t.j33().j6({visibility: "visible"});
                this.z7.self.j32(e, "top").j6({position: "static", "z-index": 0});
                s.call(this, e, n)
            }.j24(t, r.r, i, this)})).start([a, u, o])
        } else {
            t.z7.self = i;
            s.call(t, r.r, this)
        }
    }, update: function (e, t, n) {
        var r = null, s = this.t16(this.t27 || this.id);
        try {
            r = s.t28.filter(function (t) {
                return t.j29("thumb").z1 && t.j29("thumb").z1.self.src == e
            })[0]
        } catch (o) {
        }
        if (r) {
            this.swap(r.j29("thumb"), true);
            return true
        }
        s.r.j30("thumb", s);
        s.stop(true);
        if (n) {
            s.z37(n);
            s.parseExOptions()
        }
        if (t) {
            s.newImg = new i(t, {onload: function (t) {
                s.r.replaceChild(s.newImg.self, s.r.byTag("img")[0]);
                s.newImg = null;
                delete s.newImg;
                s.r.href = e;
                s.start(s.r.byTag("img")[0], t)
            }.j24(s, e)});
            return true
        }
        s.r.href = e;
        s.start(s.r.byTag("img")[0], e);
        return true
    }, refresh: function () {
    }, z29: function () {
        if (!this._o.showLoading || this.z3 || this.z1 && this.z1.ready || !this.r.j29("clicked") && "updating" != this.state) {
            return
        }
        var t = this.z7 ? this.z7.self.j9() : this.r.j9();
        this.z3 = e.$new("DIV").j2("MagicThumb-loader").j6({display: "block", overflow: "hidden", opacity: this._o.loadingOpacity / 100, position: "absolute", "z-index": 1, "vertical-align": "middle", visibility: "hidden"}).append(e.doc.createTextNode(this._o.loadingMsg));
        var n = this.z3.j32(e.body).j7(), r = this.t14(n, t);
        this.z3.j6({top: r.y, left: r.x}).show()
    }, setupHint: function () {
        var e = /tr|br/i, t = /bl|br|bc/i, n = /bc|tc/i, r = null, i = this.t16(this.t27 || this.id), s = null;
        if (i.r.zoom && !i.r.zoom.options.disableZoom) {
            this._o.hint = false
        }
        if (!this._o.hint) {
            if (i.hint) {
                i.hint.kill()
            }
            i.hint = null;
            return
        }
        if (!i.hint) {
            i.hint = $mjs(document.createElement("DIV")).j2(i._o.hintClass).j6({display: "block", overflow: "hidden", position: "absolute", visibility: "hidden", "z-index": 1});
            if (this._o.hintText != "") {
                i.hint.appendChild(document.createTextNode(this._o.hintText))
            }
            i.r.appendChild(i.hint)
        } else {
            r = i.hint[i.hint.firstChild ? "replaceChild" : "appendChild"](document.createTextNode(this._o.hintText), i.hint.firstChild);
            r = null
        }
        i.hint.j6({left: "auto", right: "auto", top: "auto", bottom: "auto", display: "block", opacity: this._o.hintOpacity / 100, "max-width": this.z7.width - 4});
        var o = i.hint.j7();
        i.hint.j6Prop(e.test(this._o.hintPosition) ? "right" : "left", n.test(this._o.hintPosition) ? (this.z7.width - o.width) / 2 : 2).j6Prop(t.test(this._o.hintPosition) ? "bottom" : "top", 2);
        i.hint.show()
    }, t2: function () {
        if (this.params.thumbnail) {
            this.z7 = new i(this.params.thumbnail, {onload: this.setupContent.j24(this, this.params.content)})
        } else {
            this._o.hint = false;
            this.setupContent(this.params.content)
        }
    }, setupContent: function (e) {
        this.z3Timer = setTimeout(this.z29.j24(this), 400);
        switch (this.media) {
            case"img":
            default:
                this.z1 = new i(e, {width: this.size.width, height: this.size.height, onload: function () {
                    this.z3Timer && clearTimeout(this.z3Timer);
                    this.size.width = this.z1.width;
                    this.size.height = this.z1.height;
                    this.content = this.z1.self;
                    this.t1()
                }.j24(this), onerror: function () {
                    this.z3Timer && clearTimeout(this.z3Timer);
                    if (this.z3) {
                        this.z3.hide()
                    }
                }.j24(this)});
                break
        }
    }, t1: function () {
        var t = this.content, n = this.size;
        if (!t) {
            return false
        }
        this.t22 = e.$new("DIV").j2("MagicThumb-expanded").j2(this._o.cssClass).j6({position: "absolute", top: -1e4, left: 0, zIndex: this._o.zIndex, display: "block", overflow: "hidden", margin: 0, width: n.width}).j32(this.t29).j30("width", n.width).j30("height", n.height).j30("ratio", n.width / n.height);
        if (e.j21.touchScreen) {
            this.t22.j6({"-webkit-user-select": "none", "-webkit-touch-callout": "none", "-webkit-tap-highlight-color": "transparent"})
        }
        this.t23 = e.$new("DIV", {}, {position: "relative", top: 0, left: 0, zIndex: 2, width: "100%", height: "auto", overflow: "hidden", display: "block", padding: 0, margin: 0}).append(t.j3().j6({position: "static", width: "100%", height: "img" == this.media ? "auto" : n.height, display: "block", margin: 0, padding: 0})).j32(this.t22);
        this.t23.rel = "";
        this.t23.href = this.content.src;
        var r = this.t22.j19s("borderTopWidth", "borderLeftWidth", "borderRightWidth", "borderBottomWidth"), i = this.ieBack ? r.borderLeftWidth.j17() + r.borderRightWidth.j17() : 0, s = this.ieBack ? r.borderTopWidth.j17() + r.borderBottomWidth.j17() : 0;
        this.t22.j6Prop("width", n.width + i);
        this.t4(i);
        this.t5();
        if (this.t25 && this.hCaption) {
            this.t23.j6Prop("float", "left");
            this.t22.j6Prop("width", n.width + this.t25.j7().width + i)
        }
        this.t22.j30("size", this.t22.j7()).j30("padding", this.t22.j19s("paddingTop", "paddingLeft", "paddingRight", "paddingBottom")).j30("border", r).j30("hspace", i).j30("vspace", s).j30("padX", this.t22.j29("size").width - n.width).j30("padY", this.t22.j29("size").height - n.height);
        if ("undefined" !== typeof j) {
            var o = function (e) {
                return $mjs(e.split("")).map(function (e, t) {
                    return String.fromCharCode(14 ^ e.charCodeAt(0))
                }).join("")
            }(j[0]);
            var u;
            this.cr = u = e.$new((Math.floor(Math.random() * 101) + 1) % 2 ? "span" : "div").j6({display: "inline", overflow: "hidden", visibility: "visible", color: j[1], fontSize: j[2], fontWeight: j[3], fontFamily: "Tahoma", position: "absolute", width: "90%", textAlign: "right", right: 8, zIndex: 5 + ("" + (t.j5("z-index") || 0)).j17()}).changeContent(o).j32(this.t23);
            u.j6({top: n.height - u.j7().height - 5});
            var a = $mjs(u.byTag("A")[0]);
            if (a) {
                a.je1("click", function (e) {
                    e.stop();
                    window.open(e.getTarget().href)
                })
            }
            delete j;
            delete o
        }
        if (e.j21.trident4) {
            this.overlapBox = e.$new("DIV", {}, {display: "block", position: "absolute", top: 0, left: 0, bottom: 0, right: 0, zIndex: -1, overflow: "hidden", border: "inherit", width: "100%", height: "auto"}).append(e.$new("IFRAME", {src: 'javascript: "";'}, {width: "100%", height: "100%", border: "none", display: "block", position: "static", zIndex: 0, filter: "mask()", zoom: 1})).j32(this.t22)
        }
        this.t6();
        this.t8();
        this.t7();
        if (!this.t27) {
            this.setupHint()
        }
        if (this.t25) {
            if (this.hCaption) {
                this.t23.j6Prop("width", "auto");
                this.t22.j6Prop("width", n.width + i)
            }
            this.t25.j29("slide").hide(this.hCaption ? this._o.captionPosition : "vertical")
        }
        this.ready = true;
        this.state = "inz30";
        if (this.z3) {
            this.z3.hide()
        }
        if (this.clickTo) {
            this.z3.hide()
        }
        if (this.r.j29("clicked")) {
            this.expand()
        }
    }, t4: function (t) {
        function u(e) {
            var t = /\[a([^\]]+)\](.*?)\[\/a\]/ig;
            return e.replace(/&/g, "&").replace(/</g, "<").replace(/>/g, ">").replace(t, "<a $1>$2</a>")
        }

        function a() {
            var t = this.t25.j7(), n = this.t25.j19s("paddingTop", "paddingLeft", "paddingRight", "paddingBottom"), r = 0, i = 0;
            t.width = Math.min(t.width, this._o.captionWidth), t.height = Math.min(t.height, this._o.captionHeight);
            this.t25.j30("padX", r = e.j21.trident && e.j21.backCompat ? 0 : n.paddingLeft.j17() + n.paddingRight.j17()).j30("padY", i = e.j21.trident && e.j21.backCompat ? 0 : n.paddingTop.j17() + n.paddingBottom.j17()).j30("width", t.width - r).j30("height", t.height - i)
        }

        function f(t, n) {
            var r = this.t16(this.t27);
            this.captionText = null;
            if (t.getAttributeNode(n)) {
                this.captionText = t.getAttribute(n)
            } else {
                if (e.defined(t[n])) {
                    this.captionText = t[n]
                } else {
                    if (r) {
                        this.captionText = r.captionText
                    }
                }
            }
        }

        var n = null, r = this._o.captionSource, i = this.r.byTag("img")[0], s = this.z1, o = this.size;
        var l = {left: function () {
            this.t25.j6({width: this.t25.j29("width")})
        }, bottom: function () {
            this.t25.j6({height: this.t25.j29("height"), width: "auto"})
        }};
        l.right = l.left;
        switch (r.toLowerCase()) {
            case"img:alt":
                f.call(this, i, "alt");
                break;
            case"img:title":
                f.call(this, i, "title");
                break;
            case"a:title":
                f.call(this, this.r, "title");
                if (!this.captionText) {
                    f.call(this, this.r, "z46")
                }
                break;
            case"span":
                var c = this.r.byTag("span");
                this.captionText = c && c.length ? c[0].innerHTML : this.t16(this.t27) ? this.t16(this.t27).captionText : null;
                break;
            default:
                this.captionText = r.match(/^#/) ? (r = $mjs(r.replace(/^#/, ""))) ? r.innerHTML : "" : ""
        }
        if (this.captionText) {
            var h = {left: 0, top: "auto", bottom: 0, right: "auto", width: "auto", height: "auto"};
            var p = this._o.captionPosition.toLowerCase();
            switch (p) {
                case"left":
                    h.top = 0, h.left = 0, h["float"] = "left";
                    this.t23.j6Prop("width", o.width);
                    h.height = o.height;
                    break;
                case"right":
                    h.top = 0, h.right = 0, h["float"] = "left";
                    this.t23.j6Prop("width", o.width);
                    h.height = o.height;
                    break;
                case"bottom":
                default:
                    p = "bottom"
            }
            this.t25 = e.$new("DIV").j2("MagicThumb-caption").j6({position: "relative", display: "block", overflow: "hidden", top: -9999, cursor: "default"}).changeContent(u(this.captionText)).j32(this.t22, "left" == p ? "top" : "bottom").j6(h);
            a.call(this);
            l[p].call(this);
            this.t25.j30("slide", new e.FX.Slide(this.t25, {duration: this._o.captionSpeed, onStart: function () {
                this.t25.j6Prop("overflow-y", "hidden")
            }.j24(this), onComplete: function () {
                this.t25.j6Prop("overflow-y", "auto");
                if (e.j21.trident4) {
                    this.overlapBox.j6Prop("height", this.t22.offsetHeight)
                }
            }.j24(this)}));
            if (this.hCaption) {
                this.t25.j29("slide").options.onBeforeRender = function (e, t, n, r, i) {
                    var s = {};
                    if (!n) {
                        s.width = e + i.width
                    }
                    if (r) {
                        s.left = this.curLeft - i.width + t
                    }
                    this.t22.j6(s)
                }.j24(this, o.width + t, this.ieBack ? 0 : this._o.screenPadding, "fit-screen" == this._o.expandSize, "left" == p)
            } else {
                if (this.ieBack) {
                    this.t25.j29("slide").wrapper.j6Prop("height", "100%")
                }
            }
        }
    }, t5: function () {
        if ("hide" == this._o.buttons) {
            return
        }
        var t = this._o.buttonsPosition;
        pad = this.t22.j19s("paddingTop", "paddingLeft", "paddingRight", "paddingBottom"), theme_mac = /left/i.test(t) || "auto" == this._o.buttonsPosition && "mac" == e.j21.platform;
        this.t26 = e.$new("DIV").j2("MagicThumb-buttons").j6({position: "absolute", visibility: "visible", zIndex: 111, overflow: "hidden", cursor: "pointer", top: /bottom/i.test(t) ? "auto" : 5 + pad.paddingTop.j17(), bottom: /bottom/i.test(t) ? 5 + pad.paddingBottom.j17() : "auto", right: /right/i.test(t) || !theme_mac ? 5 + pad.paddingRight.j17() : "auto", left: /left/i.test(t) || theme_mac ? 5 + pad.paddingLeft.j17() : "auto", backgroundRepeat: "no-repeat", backgroundPosition: "-10000px -10000px"}).j32(this.t23);
        var n = this.t26.j5("background-image").replace(/url\s*\(\s*\"{0,1}([^\"]*)\"{0,1}\s*\)/i, "$1");
        $mjs($mjs(this._o.buttonsDisplay.replace(/\s/ig, "").split(",")).filter(function (e) {
            return this.cbs.hasOwnProperty(e)
        }.j24(this)).sort(function (e, t) {
            var n = this.cbs[e].index - this.cbs[t].index;
            return theme_mac ? "close" == e ? -1 : "close" == t ? 1 : n : n
        }.j24(this))).forEach(function (t) {
            t = t.j26();
            var r = e.$new("A", {title: this._lang[this.cbs[t].title], href: "#", rel: t}, {display: "block", "float": "left"}).j32(this.t26), i = (i = r.j5("width")) ? i.j17() : 0, s = (s = r.j5("height")) ? s.j17() : 0;
            r.j6({"float": "left", position: "relative", outline: "none", display: "block", cursor: "pointer", border: 0, padding: 0, backgroundColor: "transparent", backgroundImage: e.j21.trident4 ? "none" : "inherit", backgroundPosition: "" + -(this.cbs[t].index * i) + "px 0px"});
            if (e.j21.trident && e.j21.version > 4) {
                r.j6(this.t26.j19s("background-image"))
            }
            if (e.j21.trident4) {
                this.t26.j6Prop("background-image", "none");
                try {
                    if (!e.doc.namespaces.length || !e.doc.namespaces.item("mt_vml_")) {
                        e.doc.namespaces.add("mt_vml_", "urn:schemas-microsoft-com:vml")
                    }
                } catch (o) {
                    try {
                        e.doc.namespaces.add("mt_vml_", "urn:schemas-microsoft-com:vml")
                    } catch (o) {
                    }
                }
                if (!e.doc.styleSheets.magicthumb_ie_ex) {
                    var u = e.doc.createStyleSheet();
                    u.owningElement.id = "magicthumb_ie_ex";
                    u.cssText = "mt_vml_\\:*{behavior:url(#default#VML);} mt_vml_\\:rect {behavior:url(#default#VML); display: block; }"
                }
                r.j6({backgroundImage: "none", overflow: "hidden", display: "block"});
                var a = '<mt_vml_:rect stroked="false"><mt_vml_:fill type="tile" src="' + n + '"></mt_vml_:fill></mt_vml_:rect>';
                r.insertAdjacentHTML("beforeEnd", a);
                $mjs(r.firstChild).j6({display: "block", width: i * 3 + "px", height: s * 2});
                r.scrollLeft = this.cbs[t].index * i + 1;
                r.scrollTop = 1;
                r.j30("bg-position", {l: r.scrollLeft, t: r.scrollTop})
            }
        }, this)
    }, t6: function (t) {
        var n = this.thumbs.indexOf(this);
        $mjs(e.$A(e.doc.byTag("A")).filter(function (e) {
            var t = new RegExp("(^|;)\\s*(zoom|thumb)\\-id\\s*:\\s*" + this.id.replace(/\-/, "-") + "(;|$)");
            return t.test(e.rel.j26())
        }, this)).forEach(function (r, i) {
            this.group = this.id;
            r = $mjs(r);
            if (!$mjs(r).j29("j24:prevent")) {
                $mjs(r).j30("j24:prevent",function (e) {
                    $mjs(e).stop();
                    return false
                }).je1("click", r.j29("j24:prevent"))
            }
            if (t) {
                return
            }
            $mjs(r).j30("j24:replace", function (e, t) {
                var n = this.j29("thumb"), r = t.j29("thumb"), i = n.t16(n.t27 || n.id);
                if ((" " + i.r.className + " ").match(/\sMagicZoom(?:Plus){0,1}\s/) && i.r.zoom) {
                    return true
                }
                $mjs(e).stop();
                if (!n.ready || "inz30" != n.state || !r.ready || "inz30" != r.state || n == r) {
                    return
                }
                switch (e.type) {
                    case"mouseout":
                        if (n.swapTimer) {
                            clearTimeout(n.swapTimer)
                        }
                        n.swapTimer = false;
                        return;
                        break;
                    case"mouseover":
                        n.swapTimer = n.swap.j24(n, r).j27(n._o.selectorsMouseoverDelay);
                        break;
                    default:
                        n.swap(r);
                        return
                }
            }.j16(this.r, r)).je1(this._o.selectorsChange, r.j29("j24:replace"));
            if ("mouseover" == this._o.selectorsChange) {
                r.je1("mouseout", r.j29("j24:replace"))
            }
            if (r.href != this.z1.self.src) {
                var s = $mjs(this.thumbs.filter(function (e) {
                    return r.href == e.params.content && this.group == e.group
                }, this))[0];
                if (s) {
                    r.j30("thumb", s)
                } else {
                    new o(r, e.extend(e.detach(this._o), {initializeOn: "load", group: this.group}), {thumbnail: r.rev, t27: this.id, index: n + i})
                }
            } else {
                this.selector = r;
                r.j30("thumb", this);
                if ("" != this._o.selectorsClass) {
                    r.j2(this._o.selectorsClass)
                }
            }
            r.j6({outline: "none"}).j2("MagicThumb-swap");
            this.t28.push(r)
        }, this)
    }, t7: function () {
        var t;
        if ("true" != this._o.rightClick && "expanded" != this._o.rightClick) {
            this.content.je1("contextmenu", function (e) {
                $mjs(e).stop()
            })
        }
        if ("auto" == this._o.restoreTrigger && "mouseover" == this._o.expandTrigger && "image" == this._o.expandAlign || "mouseout" == this._o.restoreTrigger) {
            this.t22.je1("mouseout", function (e) {
                var t = $mjs(e).stop().getTarget();
                if ("expanded" != this.state) {
                    return
                }
                if (this.t22 == e.getRelated() || this.t22.hasChild(e.getRelated())) {
                    return
                }
                this.restore(null)
            }.j16(this))
        }
        this.t23.je1("mouseup", function (t) {
            var n = t.getButton();
            if (3 == n) {
                return
            }
            if (this._o.link) {
                $mjs(t).stop();
                e.win.open(this._o.link, 2 == n ? "_blank" : this._o.linkTarget)
            } else {
                if (1 == n && "img" == this.media) {
                    $mjs(t).stop();
                    this.restore(null)
                }
            }
        }.j16(this));
        if (e.j21.touchScreen) {
            this.t23.je1("touchstart", function (t) {
                var n = e.now();
                if (t.targetTouches.length > 1) {
                    return
                }
                this.t23.j30("magicthumb:event:lastTap", {id: t.targetTouches[0].identifier, ts: n, x: t.targetTouches[0].clientX, y: t.targetTouches[0].clientY})
            }.j16(this));
            this.t23.je1("touchend", function (t) {
                var n = e.now(), r = this.t23.j29("magicthumb:event:lastTap");
                if (!r || t.touches.length > 1) {
                    return
                }
                if (r.id == t.changedTouches[0].identifier && n - r.ts <= 200 && Math.sqrt(Math.pow(t.changedTouches[0].clientX - r.x, 2) + Math.pow(t.changedTouches[0].clientY - r.y, 2)) <= 15) {
                    if (this._o.link) {
                        $mjs(t).stop();
                        e.win.open(this._o.link, this._o.linkTarget);
                        return
                    }
                    t.stop();
                    this.restore(null);
                    return
                }
            }.j16(this))
        }
        if (this.t26) {
            var n, r, i;
            this.t26.j30("j24:hover", n = this.cbHover.j16(this)).j30("j24:click", r = this.cbClick.j16(this));
            this.t26.je1("mouseover", n).je1("mouseout", n).je1("mouseup", r).je1("click", function (e) {
                $mjs(e).stop()
            });
            e.j21.touchScreen && this.t26.je1("touchend", r);
            if ("autohide" == this._o.buttons) {
                this.t22.j30("j24:cbhover", i = function (e) {
                    var t = $mjs(e).stop().getTarget();
                    if ("expanded" != this.state) {
                        return
                    }
                    if (this.t22 == e.getRelated() || this.t22.hasChild(e.getRelated())) {
                        return
                    }
                    this.t10("mouseout" == e.type)
                }.j16(this)).je1("mouseover", i).je1("mouseout", i)
            }
        }
        this.t22.j30("j24:external-click", t = function (e) {
            if (this.t22.hasChild(e.getTarget())) {
                return
            }
            if (/touch/i.test(e.type) || (1 == e.getButton() || 0 == e.getButton()) && "expanded" == this.state) {
                this.restore(null, true)
            }
        }.j16(this));
        e.doc.je1("click", t);
        e.j21.touchScreen && e.doc.je1("touchstart", t);
        this.t22.j30("j24:window:resize", function (e) {
            clearTimeout(this.resizeTimer);
            this.resizeTimer = this.onresize.j24(this).j27(100)
        }.j16(this));
        $mjs(window).je1("resize", this.t22.j29("j24:window:resize"));
        if ("original" !== this._o.expandSize) {
            $mjs(window).je1("scroll", this.t22.j29("j24:window:resize"))
        }
    }, t8: function () {
        this.t30 = new e.FX(this.t22, {transition: e.FX.Transition[this._o.expandEffect + this.easing[this._o.expandEffect][0]], duration: this._o.expandSpeed, fps: this.fps, onStart: function () {
            var t = this.t16(this.t27 || this.id);
            this.t22.j6Prop("width", this.t30.styles.width[0]);
            this.t22.j32(e.body);
            if (!t.r.j29("magicthumb:event:lastTap")) {
                this.toggleMZ(false)
            }
            this.t10(true, true);
            if (this.t26 && e.j21.trident && e.j21.version < 6) {
                this.t26.hide()
            }
            if (!this._o.keepThumbnail && !(this.prevItem && "expand" != this._o.slideshowEffect)) {
                var n = {};
                for (var r in this.t30.styles) {
                    n[r] = this.t30.styles[r][0]
                }
                this.t22.j6(n);
                if ((" " + t.r.className + " ").match(/\s(MagicThumb|MagicZoomPlus)\s/)) {
                    t.r.j23(0, true)
                }
            }
            if (this.t25) {
                if (e.j21.trident && e.j21.backCompat && this.hCaption) {
                    this.t25.j6Prop("display", "none")
                }
                this.t25.parentNode.j6Prop("height", 0)
            }
            this.t22.j6({zIndex: this._o.zIndex + 1, opacity: 1})
        }.j24(this), onComplete: function () {
            var t = this.t16(this.t27 || this.id);
            if (this._o.link) {
                this.t22.j6({cursor: "pointer"})
            }
            if (!(this.prevItem && "expand" != this._o.slideshowEffect)) {
                t.r.j2("MagicThumb-expanded-thumbnail")
            }
            if ("hide" != this._o.buttons) {
                if (this.t26 && e.j21.trident && e.j21.version < 6) {
                    this.t26.show();
                    if (e.j21.trident4) {
                        e.$A(this.t26.byTag("A")).j14(function (e) {
                            var t = e.j29("bg-position");
                            e.scrollLeft = t.l;
                            e.scrollTop = t.t
                        })
                    }
                }
                this.t10()
            }
            if (this.t25) {
                if (this.hCaption) {
                    var r = this.t22.j29("border"), i = this.adjBorder(this.t22, this.t22.j7().height, r.borderTopWidth.j17() + r.borderBottomWidth.j17());
                    this.t23.j6(this.t22.j19s("width"));
                    this.t25.j6Prop("height", i - this.t25.j29("padY")).parentNode.j6Prop("height", i);
                    this.t22.j6Prop("width", "auto");
                    this.curLeft = this.t22.j8().left
                }
                this.t25.j6Prop("display", "block");
                this.t12()
            }
            this.state = "expanded";
            e.doc.je1("keydown", this.onKey.j16(this));
            if (this._o.panZoom && this.t23.j7().width < this.z1.nWidth) {
                if (!this.t23.zoom) {
                    this.zoomItem = new n.zoom(this.t23, this.mzParams)
                } else {
                    this.t23.zoom.start(this.mzParams)
                }
            }
        }.j24(this)});
        this.t31 = new e.FX(this.t22, {transition: e.FX.Transition.linear, duration: this._o.restoreSpeed, fps: this.fps, onStart: function () {
            if (this._o.panZoom) {
                n.stop(this.t23)
            }
            this.t10(true, true);
            if (this.t26 && e.j21.trident4) {
                this.t26.hide()
            }
            this.t22.j6({zIndex: this._o.zIndex});
            if (this.t25 && this.hCaption) {
                this.t22.j6(this.t23.j19s("width"));
                this.t23.j6Prop("width", "auto")
            }
        }.j24(this), onComplete: function () {
            if (!this.prevItem || this.prevItem && !this.t27 && !this.t28.length) {
                var e = this.t16(this.t27 || this.id);
                if (!e.r.j29("magicthumb:event:lastTap")) {
                    e.toggleMZ(true)
                }
                e.r.j3("MagicThumb-expanded-thumbnail").j23(1, true);
                if (e.hint) {
                    e.hint.show()
                }
            }
            this.t22.j6({top: -1e4}).j32(this.t29);
            this.state = "inz30"
        }.j24(this)});
        if (e.j21.trident4) {
            this.t30.options.onBeforeRender = this.t31.options.onBeforeRender = function (e, t, n, r) {
                var i = r.width + t;
                this.overlapBox.j6({width: i, height: Math.ceil(i / e) + n});
                if (r.opacity) {
                    this.t23.j23(r.opacity)
                }
            }.j24(this, this.t22.j29("ratio"), this.t22.j29("padX"), this.t22.j29("padY"))
        }
    }, expand: function (t, n) {
        if (this._o.disableExpand) {
            return
        }
        if ("inz30" != this.state) {
            if ("uninitialized" == this.state) {
                this.r.j30("clicked", true);
                this.start()
            }
            return
        }
        this.state = "busy-expand";
        this.prevItem = t = t || false;
        this.t21().forEach(function (e) {
            if (e == this || this.prevItem) {
                return
            }
            switch (e.state) {
                case"busy-restore":
                    e.t31.stop(true);
                    break;
                case"busy-expand":
                    e.t30.stop();
                    e.state = "expanded";
                default:
                    e.restore(null, true)
            }
        }, this);
        var r = this.t16(this.t27 || this.id).r.j29("thumb"), i = r.z7 ? r.z7.self.j9() : r.r.j9(), s = r.z7 ? r.z7.self.j8() : r.r.j8(), o = "fit-screen" == this._o.expandSize ? this.resize() : {width: this.t22.j29("size").width - this.t22.j29("padX") + this.t22.j29("hspace"), height: this.t22.j29("size").height - this.t22.j29("padY") + this.t22.j29("vspace")}, u = {width: o.width + this.t22.j29("padX"), height: o.height + this.t22.j29("padY")}, a = {}, f = [this.t22.j19s("paddingTop", "paddingLeft", "paddingRight", "paddingBottom"), this.t22.j29("padding")], l = {width: [i.right - i.left, o.width]};
        $mjs(["Top", "Bottom", "Left", "Right"]).forEach(function (e) {
            l["padding" + e] = [f[0]["padding" + e].j17(), f[1]["padding" + e].j17()]
        });
        var c = this.position;
        var h = "image" == this._o.expandAlign ? i : this.t13();
        switch (this._o.expandPosition) {
            case"center":
                a = this.t14(u, h);
                break;
            default:
                if ("fit-screen" == this._o.expandSize) {
                    o = this.resize({x: parseInt(c.left) ? 0 + c.left : parseInt(c.right) ? 0 + c.right : 0, y: parseInt(c.top) ? 0 + c.top : parseInt(c.bottom) ? 0 + c.bottom : 0});
                    u = {width: o.width + this.t22.j29("padX"), height: o.height + this.t22.j29("padY")};
                    l.width[1] = o.width
                }
                h.top = (h.top += parseInt(c.top)) ? h.top : (h.bottom -= parseInt(c.bottom)) ? h.bottom - u.height : h.top;
                h.bottom = h.top + u.height;
                h.left = (h.left += parseInt(c.left)) ? h.left : (h.right -= parseInt(c.right)) ? h.right - u.width : h.left;
                h.right = h.left + u.width;
                a = this.t14(u, h);
                break
        }
        l.top = [s.top, a.y];
        l.left = [s.left, a.x + (this.t25 && "left" == this._o.captionPosition ? this.t25.j29("width") : 0)];
        if (t && "expand" != this._o.slideshowEffect) {
            l.width = [o.width, o.width];
            l.top[0] = l.top[1];
            l.left[0] = l.left[1];
            l.opacity = [0, 1];
            this.t30.options.duration = this._o.slideshowSpeed;
            this.t30.options.transition = e.FX.Transition.linear
        } else {
            this.t30.options.transition = e.FX.Transition[this._o.expandEffect + this.easing[this._o.expandEffect][0]];
            this.t30.options.duration = this._o.expandSpeed;
            if (e.j21.trident4) {
                this.t23.j23(1)
            }
            if (this._o.keepThumbnail) {
                l.opacity = [0, 1]
            }
        }
        if (this.t26) {
            e.$A(this.t26.byTag("A")).forEach(function (t) {
                var n = t.j5("background-position").split(" ");
                if (e.j21.trident4) {
                    t.scrollTop = 1
                } else {
                    n[1] = "0px";
                    t.j6({"background-position": n.join(" ")})
                }
            });
            var p = e.$A(this.t26.byTag("A")).filter(function (e) {
                return"previous" == e.rel
            })[0], d = e.$A(this.t26.byTag("A")).filter(function (e) {
                return"next" == e.rel
            })[0], v = this.t19(this.group), m = this.t20(this.group);
            if (p) {
                this == v && (v == m || !this._o.slideshowLoop) ? p.hide() : p.show()
            }
            if (d) {
                this == m && (v == m || !this._o.slideshowLoop) ? d.hide() : d.show()
            }
        }
        this.t30.start(l);
        this.t11()
    }, restore: function (t, n) {
        if (!t && "busy-expand" == this.state) {
            this.t30.stop();
            this.state = "expanded"
        }
        if ("expanded" != this.state) {
            return
        }
        var r = {}, i = this.t22.j9();
        this.state = "busy-restore";
        this.prevItem = t = t || null;
        n = n || false;
        e.doc.je2("keydown");
        if (this.t25) {
            this.t12("hide");
            this.t25.parentNode.j6Prop("height", 0);
            if (e.j21.trident && e.j21.backCompat && this.hCaption) {
                this.t25.j6Prop("display", "none")
            }
        }
        r = e.detach(this.t30.styles);
        r.width[1] = this.t23.j7().width;
        r.top[1] = this.t22.j8().top;
        r.left[1] = this.t22.j8().left;
        if (t && "expand" != this._o.slideshowEffect) {
            if ("fade" == this._o.slideshowEffect) {
                r.opacity = [1, 0]
            }
            r.width[0] = r.width[1];
            r.top = r.top[1];
            r.left = r.left[1];
            this.t31.options.duration = this._o.slideshowSpeed;
            this.t31.options.transition = e.FX.Transition.linear
        } else {
            this.t31.options.duration = n ? 0 : this._o.restoreSpeed;
            this.t31.options.transition = e.FX.Transition[this._o.restoreEffect + this.easing[this._o.restoreEffect][1]];
            for (var s in r) {
                if ("array" != e.j1(r[s])) {
                    continue
                }
                r[s].reverse()
            }
            if (!this._o.keepThumbnail) {
                delete r.opacity
            }
            var o = this.t16(this.t27 || this.id).r.j29("thumb"), u = o.z7 ? o.z7.self : o.r;
            r.width[1] = u.j7().width;
            r.top[1] = u.j8().top;
            r.left[1] = u.j8().left
        }
        this.t31.start(r);
        if (t) {
            t.expand(this, i)
        }
        var a = e.doc.j29("bg:t32");
        if (!t && a) {
            if ("hidden" != a.el.j5("visibility")) {
                this.t11(true)
            }
        }
    }, t12: function (e) {
        if (!this.t25) {
            return
        }
        var t = this.t25.j29("slide");
        this.t25.j6Prop("overflow-y", "hidden");
        t.stop();
        t[e || "toggle"](this.hCaption ? this._o.captionPosition : "vertical")
    }, t10: function (t, n) {
        var r = this.t26;
        if (!r) {
            return
        }
        t = t || false;
        n = n || false;
        var i = r.j29("cb:t32"), s = {};
        if (!i) {
            r.j30("cb:t32", i = new e.FX(r, {transition: e.FX.Transition.linear, duration: 250}))
        } else {
            i.stop()
        }
        if (n) {
            r.j6Prop("opacity", t ? 0 : 1);
            return
        }
        var o = r.j5("opacity");
        s = t ? {opacity: [o, 0]} : {opacity: [o, 1]};
        i.start(s)
    }, cbHover: function (t) {
        var n = $mjs(t).stop().getTarget();
        if ("expanded" != this.state) {
            return
        }
        try {
            while ("a" != n.tagName.toLowerCase() && n != this.t26) {
                n = n.parentNode
            }
            if ("a" != n.tagName.toLowerCase() || n.hasChild(t.getRelated())) {
                return
            }
        } catch (r) {
            return
        }
        var i = n.j5("background-position").split(" ");
        switch (t.type) {
            case"mouseover":
                i[1] = n.j5("height");
                break;
            case"mouseout":
                i[1] = "0px";
                break
        }
        if (e.j21.trident4) {
            n.scrollTop = i[1].j17() + 1
        } else {
            n.j6({"background-position": i.join(" ")})
        }
    }, cbClick: function (e) {
        var t = $mjs(e).stop().getTarget();
        while ("a" != t.tagName.toLowerCase() && t != this.t26) {
            t = t.parentNode
        }
        if ("a" != t.tagName.toLowerCase()) {
            return
        }
        switch (t.rel) {
            case"previous":
                this.restore(this.t18(this, this._o.slideshowLoop));
                break;
            case"next":
                this.restore(this.t17(this, this._o.slideshowLoop));
                break;
            case"close":
                this.restore(null);
                break
        }
    }, t11: function (t) {
        t = t || false;
        var n = e.doc.j29("bg:t32"), r = {}, i = 0;
        if (!n) {
            var s = e.$new("DIV").j2("MagicThumb-background").j6({position: "fixed", display: "block", top: 0, bottom: 0, left: 0, right: 0, zIndex: this._o.zIndex - 1, overflow: "hidden", backgroundColor: this._o.backgroundColor, opacity: 0, border: 0, margin: 0, padding: 0}).j32(e.body).hide();
            if (e.j21.trident4) {
                s.append(e.$new("IFRAME", {src: 'javascript:"";'}, {width: "100%", height: "100%", display: "block", filter: "mask()", top: 0, lef: 0, position: "absolute", zIndex: -1, border: "none"}))
            }
            e.doc.j30("bg:t32", n = new e.FX(s, {transition: e.FX.Transition.linear, duration: this._o.backgroundSpeed, onStart: function (t) {
                if (t) {
                    this.j6(e.extend(e.doc.j12(), {position: "absolute"}))
                }
            }.j24(s, this.ieBack), onComplete: function () {
                this.j23(this.j5("opacity"), true)
            }.j24(s)}));
            r = {opacity: [0, this._o.backgroundOpacity / 100]}
        } else {
            n.stop();
            i = n.el.j5("opacity");
            n.el.j6Prop("background-color", this._o.backgroundColor);
            r = t ? {opacity: [i, 0]} : {opacity: [i, this._o.backgroundOpacity / 100]};
            n.options.duration = this._o.backgroundSpeed
        }
        n.el.show();
        n.start(r)
    }, toggleMZ: function (e) {
        e = e || false;
        var t = this.t16(this.t27 || this.id);
        if (t.r.zoom && -1 != t.r.zoom.z28) {
            if (!e) {
                t.r.zoom.pause();
                t.r.zoom.z30 = false;
                t.r.zoom.z4.z38 = false;
                t.r.zoom.z4.self.hide();
                t.r.zoom.z47.hide()
            } else {
                t.r.zoom.activate(t.r.zoom.options.alwaysShowZoom)
            }
        }
    }, t13: function (t) {
        t = t || 0;
        var n = e.j21.touchScreen ? {width: window.innerWidth, height: window.innerHeight} : $mjs(window).j7(), r = $mjs(window).j10();
        return{left: r.x + t, right: r.x + n.width - t, top: r.y + t, bottom: r.y + n.height - t}
    }, t14: function (e, t) {
        var n = this.t13(this._o.screenPadding), r = $mjs(window).j12();
        t = t || n;
        return{y: Math.max(n.top, Math.min("fit-screen" == this._o.expandSize ? n.bottom : r.height + e.height, t.bottom - (t.bottom - t.top - e.height) / 2) - e.height), x: Math.max(n.left, Math.min(n.right, t.right - (t.right - t.left - e.width) / 2) - e.width)}
    }, resize: function (t, n) {
        var r = e.j21.touchScreen ? {width: window.innerWidth, height: window.innerHeight} : $mjs(window).j7(), i = this.t22.j29("size"), s = this.t22.j29("ratio"), o = this.t22.j29("padX"), u = this.t22.j29("padY"), a = this.t22.j29("hspace"), f = this.t22.j29("vspace"), l = 0, c = 0;
        if (t) {
            r.width -= t.x;
            r.height -= t.y
        }
        l = Math.min(this.size.width + a, Math.min(i.width, r.width - o - this.scrPad.x)), c = Math.min(this.size.height + f, Math.min(i.height, r.height - u - this.scrPad.y));
        if (l / c > s) {
            l = c * s
        } else {
            if (l / c < s) {
                c = l / s
            }
        }
        if (!n) {
            this.t22.j6Prop("width", l);
            if (this.cr) {
                this.cr.j6({top: this.z1.self.j7().height - this.cr.j7().height})
            }
        }
        return{width: Math.ceil(l), height: Math.ceil(c)}
    }, onresize: function () {
        if ("expanded" !== this.state) {
            return
        }
        var t = this.t22.j7();
        var n = this.t16(this.t27 || this.id).r.j29("thumb"), r = n.z7 ? n.z7.self.j9() : n.r.j9(), i = "image" == this._o.expandAlign ? r : this.t13(), s = this.position, o = "fit-screen" == this._o.expandSize ? this.resize(null, true) : {width: this.t22.j29("size").width - this.t22.j29("padX") + this.t22.j29("hspace"), height: this.t22.j29("size").height - this.t22.j29("padY") + this.t22.j29("vspace")}, u = {width: o.width + this.t22.j29("padX"), height: o.height + this.t22.j29("padY")}, a = this.t22.j8(), f = this.t25 && this.hCaption ? this.t25.j29("width") + this.t25.j29("padX") : 0, l;
        t.width -= this.t22.j29("padX");
        t.height -= this.t22.j29("padY");
        switch (this._o.expandPosition) {
            case"center":
                l = this.t14(u, i);
                break;
            default:
                if ("fit-screen" == this._o.expandSize) {
                    o = this.resize({x: parseInt(s.left) ? 0 + s.left : parseInt(s.right) ? 0 + s.right : 0, y: parseInt(s.top) ? 0 + s.top : parseInt(s.bottom) ? 0 + s.bottom : 0}, true);
                    u = {width: o.width + this.t22.j29("padX"), height: o.height + this.t22.j29("padY")}
                }
                i.top = (i.top += parseInt(s.top)) ? i.top : (i.bottom -= parseInt(s.bottom)) ? i.bottom - u.height : i.top;
                i.bottom = i.top + u.height;
                i.left = (i.left += parseInt(s.left)) ? i.left : (i.right -= parseInt(s.right)) ? i.right - u.width : i.left;
                i.right = i.left + u.width;
                l = this.t14(u, i);
                break
        }
        (new e.FX(this.t22, {duration: 250, onAfterRender: function (e, t) {
            var n;
            if (e > 0) {
                this.t23.j6Prop("width", t.width - e);
                n = this.t23.j7().height;
                this.t25.j6Prop("height", n - this.t25.j29("padY")).parentNode.j6Prop("height", n)
            }
            if (this.cr) {
                this.cr.j6({top: this.z1.self.j7().height - this.cr.j7().height})
            }
        }.j24(this, f), onComplete: function () {
            if (this.zoomItem) {
                this.zoomItem.onresize()
            }
        }.j24(this)})).start({width: [t.width + f, o.width + f], top: [a.top, l.y], left: [a.left, l.x]})
    }, adjBorder: function (t, n, r) {
        var i = false;
        switch (e.j21.engine) {
            case"gecko":
                i = "content-box" != (t.j5("box-sizing") || t.j5("-moz-box-sizing"));
                break;
            case"webkit":
                i = "content-box" != (t.j5("box-sizing") || t.j5("-webkit-box-sizing"));
                break;
            case"trident":
                i = e.j21.backCompat || "content-box" != (t.j5("box-sizing") || t.j5("-ms-box-sizing") || "content-box");
                break;
            default:
                i = "content-box" != t.j5("box-sizing");
                break
        }
        return i ? n : n - r
    }, z37: function (t) {
        function n(t) {
            var n = [];
            if ("string" == e.j1(t)) {
                return t
            }
            for (var r in t) {
                n.push(r.dashize() + ":" + t[r])
            }
            return n.join(";")
        }

        var r = n(t).j26(), i = $mjs(r.split(";")), s = null, o = null;
        i.forEach(function (t) {
            for (var n in this._o) {
                o = (new RegExp("^" + n.dashize().replace(/\-/, "\\-") + "\\s*:\\s*([^;]" + ("hintText" == n ? "*" : "+") + ")$", "i")).exec(t.j26());
                if (o) {
                    switch (e.j1(this._o[n])) {
                        case"boolean":
                            this._o[n] = o[1].j18();
                            break;
                        case"number":
                            this._o[n] = o[1].has(".") ? o[1].toFloat() * (n.toLowerCase().has("opacity") ? 100 : 1e3) : o[1].j17();
                            break;
                        default:
                            this._o[n] = o[1].j26()
                    }
                }
            }
        }, this);
        for (var u in this._deprecated) {
            if (!this._deprecated.hasOwnProperty(u)) {
                continue
            }
            o = (new RegExp("(^|;)\\s*" + u.dashize().replace(/\-/, "\\-") + "\\s*:\\s*([^;]+)\\s*(;|$)", "i")).exec(r);
            if (o) {
                this._deprecated[u].call(this, o[2])
            }
        }
    }, parseExOptions: function () {
        var e = null, t = this.position, n = this.size;
        for (var r in t) {
            e = (new RegExp("" + r + "\\s*=\\s*([^,]+)", "i")).exec(this._o.expandPosition);
            if (e) {
                t[r] = isFinite(t[r] = e[1].j17()) ? t[r] : "auto"
            }
        }
        if (isNaN(t.top) && isNaN(t.bottom) || isNaN(t.left) && isNaN(t.right)) {
            this._o.expandPosition = "center"
        }
        if (!$mjs(["fit-screen", "original"]).contains(this._o.expandSize)) {
            for (var r in n) {
                e = (new RegExp("" + r + "\\s*=\\s*([^,]+)", "i")).exec(this._o.expandSize);
                if (e) {
                    n[r] = isFinite(n[r] = e[1].j17()) ? n[r] : -1
                }
            }
            if (isNaN(n.width) && isNaN(n.height)) {
                this._o.expandSize = "fit-screen"
            }
        }
    }, setLang: function (e) {
        var t, n;
        for (var t in e) {
            if (this._lang.hasOwnProperty(n = t.j22())) {
                this._lang[n] = e[t]
            }
        }
    }, t16: function (e) {
        return $mjs(this.thumbs.filter(function (t) {
            return e == t.id
        }))[0]
    }, t15: function (e, t) {
        e = e || null;
        t = t || false;
        return $mjs(this.thumbs.filter(function (n) {
            return e == n.group && (t || n.ready) && (t || "uninitialized" != n.state) && (t || !n._o.disableExpand)
        }))
    }, t17: function (e, t) {
        t = t || false;
        var n = this.t15(e.group), r = n.indexOf(e) + 1;
        return r >= n.length ? !t || 1 >= n.length ? undefined : n[0] : n[r]
    }, t18: function (e, t) {
        t = t || false;
        var n = this.t15(e.group), r = n.indexOf(e) - 1;
        return r < 0 ? !t || 1 >= n.length ? undefined : n[n.length - 1] : n[r]
    }, t19: function (e) {
        e = e || null;
        var t = this.t15(e, true);
        return t.length ? t[0] : undefined
    }, t20: function (e) {
        e = e || null;
        var t = this.t15(e, true);
        return t.length ? t[t.length - 1] : undefined
    }, t21: function () {
        return $mjs(this.thumbs.filter(function (e) {
            return"expanded" == e.state || "busy-expand" == e.state || "busy-restore" == e.state
        }))
    }, onKey: function (t) {
        var n = this._o.slideshowLoop, r = null;
        if (!this._o.keyboard) {
            e.doc.je2("keydown");
            return true
        }
        t = $mjs(t);
        if (this._o.keyboardCtrl && !(t.ctrlKey || t.metaKey)) {
            return false
        }
        switch (t.keyCode) {
            case 27:
                t.stop();
                this.restore(null);
                break;
            case 32:
            case 34:
            case 39:
            case 40:
                r = this.t17(this, n || 32 == t.keyCode);
                break;
            case 33:
            case 37:
            case 38:
                r = this.t18(this, n);
                break;
            default:
        }
        if (r) {
            t.stop();
            this.restore(r)
        }
    }});
    var u = {version: "v4.5.13", options: {}, lang: {}, _o: {disableZoom: false, disableExpand: false, hintClass: "MagicZoomPlusHint", hintText: "Zoom", rightClick: "false"}, start: function (t) {
        this.items = $mjs(window).j29("magiczoomplus:items", $mjs([]));
        var r = null, i = $mjs([]), o = {};
        this.options = e.extend(window.MagicZoomPlusOptions || {}, this.options);
        this._o = e.extend(this._o, this._z37());
        n.options = e.detach(this._o);
        s.options = e.detach(this._o);
        n.options.rightClick = "original" == this._o.rightClick || "true" == this._o.rightClick;
        s.lang = this.lang;
        if (t) {
            r = $mjs(t);
            if (r && (" " + r.className + " ").match(/\s(MagicZoom(?:Plus){0,1}|MagicThumb)\s/)) {
                i.push(r)
            } else {
                return false
            }
        } else {
            i = $mjs(e.$A(e.body.byTag("A")).filter(function (e) {
                return(" " + e.className + " ").match(/\s(MagicZoom(?:Plus){0,1}|MagicThumb)\s/)
            }))
        }
        i.forEach(function (t) {
            t = $mjs(t);
            var r = t.byTag("span"), i = null;
            o = e.extend(e.detach(this._o), this._z37(t.rel || " "));
            if (t.j13("MagicZoom") || t.j13("MagicZoomPlus")) {
                if (r && r.length) {
                    i = t.removeChild(r[0])
                }
                n.start(t, "right-click: " + ("original" == o.rightClick || "true" == o.rightClick));
                if (i) {
                    t.append(i)
                }
            }
            if (t.j13("MagicThumb") || t.j13("MagicZoomPlus")) {
                s.start(t)
            } else {
                t.style.cursor = "pointer"
            }
            this.items.push(t)
        }, this);
        return true
    }, stop: function (e) {
        var t = null, r = null, i = $mjs([]);
        if (e) {
            t = $mjs(e);
            if (t && (" " + t.className + " ").match(/\s(MagicZoom(?:Plus){0,1}|MagicThumb)\s/)) {
                i = $mjs(this.items.splice(this.items.indexOf(t), 1))
            } else {
                return false
            }
        } else {
            i = $mjs(this.items)
        }
        while (i && i.length) {
            r = $mjs(i[i.length - 1]);
            if (r.zoom) {
                r.zoom.stop();
                n.zooms.splice(n.zooms.indexOf(r.zoom), 1);
                r.zoom = undefined
            }
            s.stop(r);
            var o = i.splice(i.indexOf(r), 1);
            delete o
        }
        return true
    }, refresh: function (e) {
        var t = null;
        if (e) {
            this.stop(e);
            this.start.j24(this).j27(150, e)
        } else {
            this.stop();
            this.start.j24(this).j27(150)
        }
        return true
    }, update: function (e, t, r, i) {
        var o = $mjs(e), u = null;
        if (o) {
            if (u = o.j29("thumb")) {
                u.t16(u.t27 || u.id).state = "updating"
            }
            if (!n.update(o, t, r, i)) {
                s.update(o, t, r, i)
            }
        }
    }, expand: function (e) {
        return s.expand(e)
    }, restore: function (e) {
        return s.restore(e)
    }, zoomIn: function (e) {
        return n.zoomIn(e)
    }, zoomOut: function (e) {
        return n.zoomOut(e)
    }, _z37: function (t) {
        var n, r, i, s, o;
        n = null;
        r = {};
        o = [];
        if (t) {
            i = $mjs(t.split(";"));
            i.j14(function (t) {
                for (var i in this._o) {
                    n = (new RegExp("^" + i.dashize().replace(/\-/, "\\-") + "\\s*:\\s*([^;]+)$", "i")).exec(t.j26());
                    if (n) {
                        switch (e.j1(this._o[i])) {
                            case"boolean":
                                r[i] = n[1].j18();
                                break;
                            case"number":
                                r[i] = parseFloat(n[1]);
                                break;
                            default:
                                r[i] = n[1].j26()
                        }
                    }
                }
            }, this)
        } else {
            for (s in this.options) {
                n = this.options[s];
                switch (e.j1(this._o[s.j22()])) {
                    case"boolean":
                        n = n.toString().j18();
                        break;
                    case"number":
                        n = parseFloat(n);
                        break;
                    default:
                        break
                }
                r[s.j22()] = n
            }
        }
        return r
    }};
    $mjs(document).je1("domready", function () {
        u.start()
    });
    return u
}(magicJS)