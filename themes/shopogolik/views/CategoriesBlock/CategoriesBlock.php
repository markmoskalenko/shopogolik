<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CategoriesBlock.php">
* </description>
**********************************************************************************************************************/?>
<div class="block" id="categories-block">
  <div class="block-content categories">
    <? $useVirtualMenu = DSConfig::getVal('search_useVirtualMenu') == 1; ?>
    <ul>
      <?php if (isset($mainMenu)) {
        foreach ($mainMenu as $id => $menu) {
          ?>
          <?php
          $type = 'category';
          if (isset($menu['children']) && (count($menu['children']) > 0) && !$useVirtualMenu) {
            $link = Yii::app()->createUrl('/category/page', array('page_id' => $menu['pkid']));
          }
          else {
            $link = Yii::app()->createUrl('/' . $type . '/index', array('name' => $menu['url']));
          }
          ?>
          <li class="parent">
            <? if (($menu['cid'] == 0) && ($menu['query'] == '')) { ?>
              <a href="javascript:void(0);" <?=($adminMode) ? "onclick=\"renderUpdateForm('" . $menu['pkid'] . "')\"" : ''; ?>><?= $menu['view_text'] ?></a>
            <? }
            else { ?>
              <a
                href="<?= ($adminMode) ? 'javascript:void(0);' : $link ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $menu['pkid'] . "')\"" : ''; ?> >
                <?= $menu['view_text'] ?>
              </a>
            <? } ?>
            <?php if (isset($menu['children']) && count($menu['children']) > 0) { ?>
              <ul>
                <? if ($useVirtualMenu) { ?>
                  <li>
                    <table class="categories-table">
                      <?php if (isset($menu['children']) && count($menu['children']) > 0) {
                        foreach ($menu['children'] as $id2 => $items2) {
                          ?>
                          <? if (isset($items2) && isset($items2['url'])) { ?>
                            <tr>
                              <td class="a2">
                                <? if (($items2['cid'] == 0) && ($items2['query'] == '')) { ?>
                                  <a class="a2" href="javascript:void(0);"><?= $items2['view_text'] ?></a>
                                <? }
                                else { ?>
                                  <a class="a2" href="<?=
                                  ($adminMode) ? 'javascript:void(0);' :
                                    Yii::app()
                                      ->createUrl('/' . $type . '/index', array('name' => $items2['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items2['pkid'] . "')\"" : ''; ?> >
                                    <?= $items2['view_text'] ?>
                                  </a>
                                <? } ?>
                              </td>
                            </tr>
                            <tr>
                              <td>

                                <?php if (isset($menu['children']) && count($menu['children']) > 0)
                                  //  foreach($menu['children'] as $id2=>$items2) {
                                  ?>
                                <?
                                if (isset($items2['children']) && count($menu['children']) > 0) {
                                  foreach ($items2['children'] as $id3 => $items3) {
                                    ?>

                                    <div class="a1" title="<? if (mb_strlen($items3['view_text'], 'UTF-8') > 20) {
                                      echo $items3['view_text'];
                                    } ?>">
                                      <a href="<?=
                                      ($adminMode) ? 'javascript:void(0);' :
                                        Yii::app()
                                          ->createUrl('/' . $type . '/index', array('name' => $items3['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items3['pkid'] . "')\"" : ''; ?> >
                                        <?= $items3['view_text'] ?>
                                      </a>&nbsp;
                                    </div>

                                  <?
                                  }
                                } ?>
                                <? // } ?>

                              </td>
                            </tr>
                          <? } ?>
                        <?php
                        }
                      } ?>
                    </table>
                  </li>

                <?
                }
                else {
                  ?>
                  <?php if (isset($menu['children']) && count($menu['children']) > 0) { ?>
                    <li>
                      <table class="categories-table">
                        <tr>
                          <td>
                            <? foreach ($menu['children'] as $id2 => $items2) { ?>

                              <? if (isset($items2) && isset($items2['url'])) { ?>
                                <a href="<?=
                                ($adminMode) ? 'javascript:void(0);' :
                                  Yii::app()
                                    ->createUrl('/' . $type . '/index', array('name' => $items2['url'])) ?>" <?= ($adminMode) ? "onclick=\"renderUpdateForm('" . $items2['pkid'] . "')\"" : ''; ?> >
                                  <?= $items2['view_text'] ?>
                                </a>&nbsp;
                              <? } ?>
                            <?php } ?>
                          </td>
                        </tr>
                      </table>
                    </li>
                  <? } ?>
                <? } ?>

              </ul>
            <? } else { ?>
<? //Эмуляция пустого меню ?>
              <ul>
                <li>
                  <table class="categories-table">
                    <tr>
                      <td class="a2"><a class="a2"/>
                      <div class="a1"><a class="a1"/></div></td>
                    </tr>
                  </table>
                </li>
              </ul>
            <? } ?>
          </li>
        <?php
        }
      } ?>
      <? if (!$adminMode) { ?>
        <li class="parent"><a href="<?= Yii::app()->createUrl('/category/list') ?>">
            <?= Yii::t('main', 'Все категории') ?>
          </a></li>
      <? } ?>
    </ul>
  </div>
</div>