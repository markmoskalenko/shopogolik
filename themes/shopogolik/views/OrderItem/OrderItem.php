<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrderItem.php">
* </description>
**********************************************************************************************************************/?>
<div class="cart-table">
    <?/* Status Block*/?>
    <?if (!is_a($item, 'Cart')) { ?>
        <div class="status-text"><?=$item->status_text;?></div>
        <? if ($item->status==4) { ?>
            <div class="status-text-cancell "><?=Yii::t('main','ОТМЕНЕНО');?></div>
        <? }
    }?>
  <div class="product-image">

    <ul class="hoverbox">
      <li>
        <a href="<?= Yii::app()->createUrl('/item/index', array('iid' => $item->iid)) ?>">
          <? if ($lazyLoad) { ?>
            <img class="lazy"
                 src="<?= Yii::app()->request->baseUrl ?>/themes/<?= Yii::app()->theme->name ?>/images/zoomloader.gif"
                 data-original="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt=""
                 title=""/>
            <noscript><img src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt=""/></noscript>
          <? }
          else { ?>

            <img src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
          <? } ?>
          <img class="preview" src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
        </a>
      </li>
    </ul>
  </div>
    <div class="cart-info">
      <?= (is_a($item, 'Cart') || (strpos($item->title, '<translation')===0)) ? $item->title : Yii::app()->DanVitTranslator->translateText($item->title, 'zh-CHS', Utils::TransLang()); ?>
        <?php if (isset($item->input_props_array)) {
        $input_props_array = $item->input_props_array;
      }
      else {
        $input_props_array = json_decode($item->input_props);
      }
      if (!$input_props_array || ($input_props_array == null)) {
        $input_props_array = array();
      }
      foreach ($input_props_array as $name => $value) {
        ?><br>
        <strong><?= (is_a($item, 'Cart') || (preg_match('/editTranslation|<translation/s',$value->name))) ? $value->name : Yii::app()->DanVitTranslator->translateText($value->name, 'zh-CHS', Utils::TransLang()); ?></strong>:
        <b>
          <?= (is_a($item, 'Cart') || (preg_match('/editTranslation|<translation/s',$value->value))) ? $value->value : Yii::app()->DanVitTranslator->translateText($value->value, 'zh-CHS', Utils::TransLang()); ?></b>
      <?php } ?>
      <? if (is_a($item, 'Cart')) { ?>
        <textarea placeholder="<?= Yii::t('main', 'Ваш комментарий к лоту') ?>"
                  name="description[<?= $item->id ?>]"><?= $item->desc ?></textarea>
      <? } ?>
      <input type="hidden" name="iid[<?= $item->id ?>]" value="<?= $item->iid ?>"/>
      <input type="hidden" name="params[<?= $item->id ?>]"
             value="<?= (isset($item->props)) ? $item->props : $item->input_props ?>"/>
    </div>
    <div class="param">
    <table border="0">
        <tbody>
        <tr>
            <td width="120"><label><?= Yii::t('main', 'Количество') ?>:</label></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="square">
            <td>
            <input <?= ($readOnly) ? 'readonly' : '' ?> type="text" name="num[<?= $item->id ?>]" id="num<?= $item->id ?>"
            value="<?= (isset($item->actual_num) && $item->actual_num)? $item->actual_num : $item->num ?>"/>
            </td>
            <td align="right">
                <? if (is_a($item, 'Cart')) {
                    $taobao_price = $item->price_no_discount;
                    $sum = $item->sum;
                    $sumResUserPrice=$item->sumResUserPrice;
                    $sum_no_discount = $item->sum_no_discount;
                    $sum_no_discountResUserPrice=$item->sum_no_discountResUserPrice;
                }
                else {
                    $actualPrice=(isset($item->calculated_actualPrice) && $item->calculated_actualPrice)? $item->calculated_actualPrice : $item->taobao_promotion_price;
                    $actualExpressFee=(isset($item->calculated_actualExpressFee) && $item->calculated_actualExpressFee)? $item->calculated_actualExpressFee :$item->express_fee;
                    $actualNum=(isset($item->actual_num) && $item->actual_num)? $item->actual_num : $item->num;
                    $resUserPrice = Formulas::getUserPrice(
                      array(
                        'price'=>$item->taobao_price,
                        'count'=>1,
                        'deliveryFee'=>$item->express_fee,
                        'postageId'=>0,
                        'sellerNick'=>FALSE,
                      ));
                    $taobao_price=$resUserPrice->price;
                    $resUserPrice = Formulas::getUserPrice(
                      array(
                        'price'=>$actualPrice,
                        'count'=>$actualNum,
                        'deliveryFee'=>$actualExpressFee,
                        'postageId'=>0,
                        'sellerNick'=>FALSE,
                      ));
                  $sum=$resUserPrice->price;
                  $sumResUserPrice=$resUserPrice;
                  $resUserPrice = Formulas::getUserPrice(
                      array(
                        'price'=>$item->taobao_price,
                        'count'=>$actualNum,
                        'deliveryFee'=>$item->express_fee,
                        'postageId'=>0,
                        'sellerNick'=>FALSE,
                      ));
                  $sum_no_discount=$resUserPrice->price;
                  $sum_no_discountResUserPrice=$resUserPrice;
                }?>

                <div class="cost">
                    &nbsp;&times;&nbsp;<?= Formulas::priceWrapper($taobao_price) ?>
                </div>
            </td>
            <td align="right">
                <div class="sum">
                    <s><span title="<?=(Yii::app()->user->role=='superAdmin')?$sum_no_discountResUserPrice->report():'';?>"><?= Formulas::priceWrapper((isset($item->status)&&(in_array($item->status,array(4,5)))) ? 0 : $sum_no_discount) ?></span></s>
                </div>
            </td>
        </tr>
        <tr>
            <td width="120"><label <?=(DSConfig::getVal('checkout_weight_needed')!=1) ? 'hidden="hidden"' : ''?>><?= Yii::t('main', 'Примерный вес, грамм') ?>:</label></td>
            <td>&nbsp;</td>
            <td align="right">
            <div class="sum">
                <? if ($sum != $sum_no_discount) { ?>
              <span title="<?=(Yii::app()->user->role=='superAdmin')?$sumResUserPrice->report():'';?>">
                    <?=Formulas::priceWrapper((isset($item->status)&&(in_array($item->status,array(4,5)))) ? 0 : $sum)?></span>
                <? } ?>
            </div>
            </td>
        </tr>
        <tr>
            <td>
            <input <?= ($readOnly) ? 'readonly' : '' ?> type="text" name="weight[<?= $item->id ?>]"
            id="weight<?= $item->id ?>"
            value="<?= (isset($item->calculated_actualWeight) && $item->calculated_actualWeight)? $item->calculated_actualWeight : $item->weight ?>"
             <?=(DSConfig::getVal('checkout_weight_needed')!=1) ? 'hidden="hidden"' : ''?>/>
            </td>
            <td>&nbsp;</td>
            <td>
                <? if ((!$readOnly) && ($allowDelete)) { ?>
                    <div class="remove">
                        <a href="<?= Yii::app()->createUrl('/cart/delete', array('id' => $item->id)) ?>">
                            <?= Yii::t('main', 'Удалить') ?><span class="ui-icon ui-icon-close" style="display:inline-block;"></span>
                        </a>
                    </div>
                <? } ?>
            </td>
        </tr>
        </tbody>
    </table>
    </div>

  <div class="clear"></div>
  <div>
    <? if (is_a($item, 'Cart')) { ?>
    <? }
    else { ?>
      <? $this->widget('application.components.blocks.OrderCommentsBlock', array(
        'orderId'       => FALSE,
        'orderItemId'   => $item->id,
        'showInternals' => $adminMode ? 1 : 0,
        'public'        => $publicComments,
        'pageSize'      => 5,
        'imageFormat'   => '_200x200.jpg',
      ));
      ?>
    <? } ?>
  </div>
</div>


<? if (!$readOnly || is_a($item, 'Cart')) { ?>
  <script>
    $("#num<?=$item->id?>").spinner({
      down: "ui-icon-triangle-1-s",
      up: "ui-icon-triangle-1-n",
      min: 1,
      max: 10000,
      step: 1
    });
    $("#weight<?=$item->id?>").spinner({
      down: "ui-icon-triangle-1-s",
      up: "ui-icon-triangle-1-n",
      min: 0,
      max: 100000,
      step: 100
    });
  </script>
<? } ?>

