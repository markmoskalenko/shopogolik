<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="sellerRelatedBlock.php">
* </description>
**********************************************************************************************************************/?>
        <? if(isset($sellerRelated->items) && is_array($sellerRelated->items) && (count($sellerRelated->items)>0)) { ?>
          <script async type="text/javascript">
           MagicScroll.extraOptions.sellerrelated = {
             'direction': 'right',
             'items': 5,
             'step' : 1,
             'duration' : 200,
             'speed': 3000,
             'height': 75,
             'width': 375,
             'arrows': 'outside',
             'arrows-opacity' : 20,
             'arrows-hover-opacity' : 100
            }
          </script>
                <div class="MagicScroll msborder sellerrelated" id="sellerrelated">
                <?  foreach($sellerRelated->items as $k=>$small_item) { ?>
                    <a href="<?=Yii::app()->createUrl('/item/index',array('iid'=>$small_item->num_iid))?>">
                    <img src="<?=Img::getImagePath($small_item->pic_url,'_180x180.jpg',false)?>" />
                    </a>
                <? } ?>
                <div>
               <script async type="text/javascript">
                 MagicScroll.init();
               </script>
        <? } ?>