<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="itemRelatedBlock.php">
* </description>
**********************************************************************************************************************/?>
<? if (isset($itemRelated->items) && is_array($itemRelated->items) && (count($itemRelated->items) > 0)) { ?>
  <script async type="text/javascript">
    MagicScroll.extraOptions.itemrelated = {
      'direction': 'right',
      'items': 8,
      'step': 1,
      'width': '960',
      'height': '125',
      'item-height': 125,
      'item-width': 125,
      'duration': 500,
      'speed': 3000,
      'arrows': 'outside',
      'arrows-opacity': 20,
      'arrows-hover-opacity': 100
    }
  </script>
  <div class="block" id="item-related-block">
    <div class="block-seller">
      <h4 class="title"><?= Yii::t('main', 'Похожие товары') ?></h4>

      <div class="MagicScroll msborder itemrelated" id="itemrelated">
        <? foreach ($itemRelated->items as $k => $small_item) { ?>
          <a href="<?= Yii::app()->createUrl('/item/index', array('iid' => $small_item->num_iid)) ?>">
            <img src="<?= Img::getImagePath($small_item->pic_url, '_180x180.jpg', FALSE) ?>"/>
          </a>
        <? } ?>
        <div>
          <script type="text/javascript">
            MagicScroll.init();
          </script>
        </div>
      </div>
    </div>
  </div>
<? } ?>