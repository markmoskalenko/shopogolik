<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="comment.php">
* </description>
**********************************************************************************************************************/?>
<?php if(!Yii::app()->user->isGuest):?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comment-comment-form',
	'enableAjaxValidation'=>false,
        'action'=>$this->createUrl('/item/comment'),
)); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text'); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>
        <input type="hidden" name="Comment[iid]" value="<?=$iid?>" />
	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main','Добавить'),array('class'=>'blue-btn bigger')); ?>
	</div>    
<?php $this->endWidget(); ?>
</div>
<?php else:?>
<p><?=Yii::t('main','Авторизуйтесь чтобы оставить комментарий')?></p>
<?php endif; ?>
