<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="BrandsBlock.php">
* </description>
**********************************************************************************************************************/?>
<? if ($brands) {?>
    <h4 class="page-title black"><?=Yii::t('main','Бренды')?></h4>

    <div class="MagicScroll msborder" id="brandsScroll">
        <? foreach ($brands as $i => $brand) { ?>
            <a href="<?=Yii::app()->createUrl('/brand/index',array('name'=>$brand->url))?>">
            <img src="<?=Yii::app()->request->baseUrl?>/images/brands/<?=$brand->img_src;?>" alt="<?= CHtml::encode($brand->name); ?>" />
            <?=$brand->name?></a>
        <? } ?>
    </div>
    <a href="/"><?=Yii::t('main','Все бренды')?></a>

<? } ?>