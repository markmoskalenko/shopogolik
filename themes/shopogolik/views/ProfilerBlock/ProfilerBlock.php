<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ProfilerBlock.php">
* </description>
**********************************************************************************************************************/?>
<? if (isset($profiling)&&is_array($profiling)) { ?>
<div class="block" id="search-terms">

    <h4><?=Yii::t('main','Время выполнения')?></h4>
    <div class="block-content">
      <span style="font-size: 12px;">
      <? foreach ($profiling as $rec=>$i) {
        if (strpos($i,'*')===0) { ?>
          <i><?=$i?></i><br/>
        <? } else {
        if (($rec<>'start') && ($rec<>'stop')) {
          $s=$rec;
          if ($rec=='duration') {
            $s='<u>'.'Total'.'</u>';
          }
        ?>
         <b><?=$s?>:</b>&nbsp;<?=$i?>s.<br/>
      <? }
       }
      } ?>
      </span>
    </div>

</div>
<? } ?>