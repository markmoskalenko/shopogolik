<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SearchParams.php">
* </description>
**********************************************************************************************************************/?>
<? $useDSG=DSConfig::getVal('search_useDSG')==1; ?>
<div class="block" id="search-terms">

    <div class="block-content">
        <form id="search-params" action="<?=Yii::app()->createUrl('/'.$type.'/index',$params)?>" method="post">
            <div class="form-item">
<? if ($priceRange) {
   if (count($priceRange)>0) { ?>
     <h4 class="title"><?=Yii::t('main','Распределение цен')?></h4>
       <?
       $priceArr=array();
       $percentArr = array();
       foreach ($priceRange as $range) {
         if ($range->start<=0) {
           continue;
         } else {
           $resUserPrice=Formulas::getUserPrice(
             array(
               'price'=>$range->start,
               'count'=>1,
               'deliveryFee'=>10,
               'postageId'=>FALSE,
               'sellerNick'=>FALSE,
             ));
           $price=$resUserPrice->price;
         }
         $priceArr[]=$price;
         $percentArr[]=(int)$range->percent;
        }
//--------------------------------------
     $this->widget(
     'chartjs.widgets.ChLine',
     array(
     'width' => 200,
     'height' => 150,
     'htmlOptions' => array(),
     'labels' => $priceArr,
     'datasets' => array(
     array(
     "fillColor" => "rgba(220,220,220,0.5)",
     "strokeColor" => "rgba(220,220,220,1)",
     "pointColor" => "rgba(220,220,220,1)",
     "pointStrokeColor" => "#ffffff",
     "data" => $percentArr,
     ),
     ),
     'options' => array(
       //Boolean - If we show the scale above the chart data
       'scaleOverlay'=>false,

	//Boolean - If we want to override with a hard coded scale
	'scaleOverride' => false,

	//** Required if scaleOverride is true **
	//Number - The number of steps in a hard coded scale
	'scaleSteps' => null,
	//Number - The value jump in the hard coded scale
	'scaleStepWidth' => null,
	//Number - The scale starting value
	'scaleStartValue' => null,

	//String - Colour of the scale line
	'scaleLineColor' => "rgba(0,0,0,.1)",

	//Number - Pixel width of the scale line
	'scaleLineWidth' => 2,

	//Boolean - Whether to show labels on the scale
	'scaleShowLabels' => true,

	//Interpolated JS string - can access value
	'scaleLabel' => "<%=value+'%'%>",

	//String - Scale label font declaration for the scale label
	'scaleFontFamily' => "'Arial'",

	//Number - Scale label font size in pixels
	'scaleFontSize' => 11,

	//String - Scale label font weight style
	'scaleFontStyle' => "normal",

	//String - Scale label font colour
	'scaleFontColor' => "#666",

	///Boolean - Whether grid lines are shown across the chart
	'scaleShowGridLines' => true,

	//String - Colour of the grid lines
	'scaleGridLineColor' => "rgba(0,0,0,.05)",

	//Number - Width of the grid lines
	'scaleGridLineWidth' => 1,

	//Boolean - Whether the line is curved between points
	'bezierCurve' => true,

	//Boolean - Whether to show a dot for each point
	'pointDot' => true,

	//Number - Radius of each point dot in pixels
	'pointDotRadius' => 4,

	//Number - Pixel width of point dot stroke
	'pointDotStrokeWidth' => 2,

	//Boolean - Whether to show a stroke for datasets
	'datasetStroke' => true,

	//Number - Pixel width of dataset stroke
	'datasetStrokeWidth' => 2,

	//Boolean - Whether to fill the dataset with a colour
	'datasetFill' => true,

	//Boolean - Whether to animate the chart
	'animation' => true,

	//Number - Number of animation steps
	'animationSteps' => 40,

	//String - Animation easing effect
	'animationEasing' => "easeOutQuart",

	//Function - Fires when the animation is complete
	'onAnimationComplete' => null,
     )
     )
     );
     //--------------------------------------
     ?>
  <? }
   } ?>
		<label for="price-min"><?=Yii::t('main','Цена (без доставки)')?>:</label>
		<span><?=Yii::t('main','от')?></span>
		<input type="text" size="4" class="small_input" name="price_min" id="price-min" 
                       value="<?=(isset($params['price_min']))?CHtml::encode($params['price_min']):0?>" />
		<span><?=Yii::t('main','до')?></span>
		<input type="text" size="4" class="small_input" name="price_max"
                       value="<?=(isset($params['price_max']))?CHtml::encode($params['price_max']):''?>" />
		<span><?=$symb?></span>
            </div>
<? if (!$useDSG) { ?>
            <div class="form-item">
                <label for="sales-min"><?=Yii::t('main','Количество продаж')?>:</label>
                <span><?=Yii::t('main','от')?></span>
                <input type="text" size="4" class="small_input" name="sales_min" id="sales-min"
                       value="<?=(isset($params['sales_min']))?CHtml::encode($params['sales_min']):''?>" />
		<span><?=Yii::t('main','до')?></span>
		<input type="text" size="4" class="small_input" name="sales_max" 
                       value="<?=(isset($params['sales_max']))?CHtml::encode($params['sales_max']):''?>" />
		<span><?=Yii::t('main','шт.')?></span>
            </div>
            <div class="form-item">
		<label for="rating"><?=Yii::t('main','Рейтинг продавца')?>:</label>
		<div id="rating_min">
                    <div class="rating-line">
                        <span><?=Yii::t('main','от')?></span>
                        <div class="rating-icons"></div>
                    </div>
                    <div id="rating_slider_min"></div>
                    <div class="rating_text">0</div>
                </div>
		<div id="rating_max">
                    <div class="rating-line">
                    	<span><?=Yii::t('main','до')?></span>
                    	<div class="rating-icons"></div>
                    </div>
                    <div id="rating_slider_max"></div>
                    <div class="rating_text">20</div>
		</div>						
		<input type="hidden" name="rating_min" id="rating_min_value" 
                       value="<?=(isset($params['rating_min']))?CHtml::encode($params['rating_min']):0?>" />
		<input type="hidden" name="rating_max" id="rating_max_value"
                       value="<?=(isset($params['rating_max']))?CHtml::encode($params['rating_max']):20?>" />
            </div>
  <? } ?>
            <!-- filling group filters -->
            <?php if(is_array($groups) and (count($groups)>0)) { ?>
            <h4 class="title"><?=Yii::t('main','Группы товаров')?></h4>
             <? foreach($groups as $prop){
                if (isset($prop->values[0])) {
                  $pidvid=explode(':',str_replace('%3A',':',$prop->values[0]->values_props));
                  $pid=$pidvid[0];
                  ?>
                  <div class="form-item param">
                    <label class="header"><?=$prop->title?></label>
                    <div class="param-childs<?php if(!isset($cur_props[$pid])) echo ' hidden';?>">
                      <?php if(isset($cur_props[$pid])) { ?>
                        <label>
                          <input type="radio" name="props[<?=$pid?>]" value="" />
                          <cite><?=Yii::t('main','Сбросить значения')?></cite>
                        </label>
                      <?php } ?>
                      <?php for($i=0;$i<4;$i++) { ?>
                        <?php if(isset($prop->values[$i])) {
                          $pidvid=explode(':',str_replace('%3A',':',$prop->values[$i]->values_props));
                          $vid=$pidvid[1];
                          ?>
                          <label>
                            <?php if(isset($cur_props[$pid]) && $cur_props[$pid] ==$vid ) {
                              $ch = ' checked';
                            } else {
                              $ch = '';
                            }?>
                            <input type="radio" name="props[<?=$pid?>]" value="<?=$vid?>"<?=$ch?> />
                            <?=$prop->values[$i]->values_title?>
                            <? if (isset($prop->values[$i]->values_count)&&($prop->values[$i]->values_count>0)) { ?>
                              (<?=$prop->values[$i]->values_count?>)
                            <?}?>
                          </label>
                        <?php } ?>
                      <?php } ?>
                      <?php if(count($prop->values)>4) { ?>
                        <a href="javascript: showMore(<?=$pid?>);"><?=Yii::t('main','Показать больше')?></a>
                      <?php } ?>
                    </div>
                    <?php if(count($prop->values)>4) { ?>
                      <div class="all-param-childs" id="param-<?=$pid?>">
                        <div class="param-header">
                          <?=$prop->title?>
                          <a href="javascript: closeMore(<?=$pid?>)" class="closer"></a>
                        </div>
                        <?php for($j=$i;$j<count($prop->values);$j++) { ?>
                          <?php if(isset($prop->values[$j])) {
                            $pidvid=explode(':',str_replace('%3A',':',$prop->values[$j]->values_props));
                            $vid=$pidvid[1];
                            ?>
                            <label>
                              <?php if(isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                $ch = ' checked';
                              } else {
                                $ch = '';
                              }?>
                              <input type="radio" name="props[<?=$pid?>]" value="<?=$vid?>"<?=$ch?> />
                              <?=$prop->values[$j]->values_title?>
                              <? if (isset($prop->values[$j]->values_count)&&($prop->values[$j]->values_count>0)) { ?>
                                (<?=$prop->values[$j]->values_count?>)
                              <?}?>
                            </label>
                          <?php } ?>
                        <?php } ?>
                      </div>
                    <?php } ?>
                  </div>
                <?php }
              }
            }?>
          <!-- filling filters -->
            <?php if(is_array($filters) and (count($filters)>0)) { ?>
            <h4 class="title"><?=Yii::t('main','Фильтры по свойствам')?></h4>
            <?  foreach($filters as $prop){
                if (isset($prop->values[0])) {
                $pidvid=explode(':',str_replace('%3A',':',$prop->values[0]->values_props));
                $pid=$pidvid[0];
                ?>
            <div class="form-item param">
                <label class="header"><?=$prop->title?></label>
                <div class="param-childs<?php if(!isset($cur_props[$pid])) echo ' hidden';?>">
                    <?php if(isset($cur_props[$pid])) { ?>
                    <label>                        
                        <input type="radio" name="props[<?=$pid?>]" value="" />
                        <cite><?=Yii::t('main','Сбросить значения')?></cite>
                    </label>
                    <?php } ?>
                    <?php for($i=0;$i<4;$i++) { ?>
                    <?php if(isset($prop->values[$i])) {
                        $pidvid=explode(':',str_replace('%3A',':',$prop->values[$i]->values_props));
                        $vid=$pidvid[1];
                        ?>
                    <label>
                        <?php if(isset($cur_props[$pid]) && $cur_props[$pid] ==$vid ) {
                            $ch = ' checked';
                        } else {
                          $ch = '';
                        }?>
                        <input type="radio" name="props[<?=$pid?>]" value="<?=$vid?>"<?=$ch?> />
                        <?=$prop->values[$i]->values_title?>
                      <? if (isset($prop->values[$i]->values_count)&&($prop->values[$i]->values_count>0)) { ?>
                    (<?=$prop->values[$i]->values_count?>)
                      <?}?>
                    </label>
                    <?php } ?>
                    <?php } ?>
                    <?php if(count($prop->values)>4) { ?>
                        <a href="javascript: showMore(<?=$pid?>);"><?=Yii::t('main','Показать больше')?></a>
                    <?php } ?>
                </div>
                <?php if(count($prop->values)>4) { ?>
                <div class="all-param-childs" id="param-<?=$pid?>">
                    <div class="param-header">
                        <?=$prop->title?>
                        <a href="javascript: closeMore(<?=$pid?>)" class="closer"></a>
                    </div>
                    <?php for($j=$i;$j<count($prop->values);$j++) { ?>
                    <?php if(isset($prop->values[$j])) {
                        $pidvid=explode(':',str_replace('%3A',':',$prop->values[$j]->values_props));
                        $vid=$pidvid[1];
                        ?>
                    <label>
                        <?php if(isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                            $ch = ' checked';
                        } else {
                          $ch = '';
                        }?>
                        <input type="radio" name="props[<?=$pid?>]" value="<?=$vid?>"<?=$ch?> />
                        <?=$prop->values[$j]->values_title?>
                        <? if (isset($prop->values[$j]->values_count)&&($prop->values[$j]->values_count>0)) { ?>
                          (<?=$prop->values[$j]->values_count?>)
                      <?}?>
                    </label>
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
            <?php }
              }
            }?>
          <!-- filling multiFilters -->
          <?php if(is_array($multiFilters) and (count($multiFilters)>0)) { ?>
            <? foreach($multiFilters as $prop){
              $pidvid=explode(':',str_replace('%3A',':',$prop->values[0]->values_props));
              $pid=$pidvid[0];
              ?>
              <div class="form-item param">
                <label class="header"><?=$prop->title?></label>
                <div class="param-childs<?php if(!isset($cur_props[$pid])) echo ' hidden';?>">
                  <?php if(isset($cur_props[$pid])) { ?>
                    <label>
                      <input type="radio" name="props[<?=$pid?>]" value="" />
                      <cite><?=Yii::t('main','Сбросить значения')?></cite>
                    </label>
                  <?php } ?>
                  <?php for($i=0;$i<4;$i++) { ?>
                    <?php if(isset($prop->values[$i])) {
                      $pidvid=explode(':',str_replace('%3A',':',$prop->values[$i]->values_props));
                      $vid=$pidvid[1];
                      ?>
                      <label>
                        <?php if(isset($cur_props[$pid]) && $cur_props[$pid] ==$vid ) {
                          $ch = ' checked';
                        } else {
                          $ch = '';
                        }?>
                        <input type="radio" name="props[<?=$pid?>]" value="<?=$vid?>"<?=$ch?> />
                        <?=$prop->values[$i]->values_title?>
                      <? if (isset($prop->values[$i]->values_count)&&($prop->values[$i]->values_count>0)) { ?>
                        (<?=$prop->values[$i]->values_count?>)
                        <?}?>
                      </label>
                    <?php } ?>
                  <?php } ?>
                  <?php if(count($prop->values)>4) { ?>
                    <a href="javascript: showMore(<?=$pid?>);"><?=Yii::t('main','Показать больше')?></a>
                  <?php } ?>
                </div>
                <?php if(count($prop->values)>4) { ?>
                  <div class="all-param-childs" id="param-<?=$pid?>">
                    <div class="param-header">
                      <?=$prop->title?>
                      <a href="javascript: closeMore(<?=$pid?>)" class="closer"></a>
                    </div>
                    <?php for($j=$i;$j<count($prop->values);$j++) { ?>
                      <?php if(isset($prop->values[$j])) {
                        $pidvid=explode(':',str_replace('%3A',':',$prop->values[$j]->values_props));
                        $vid=$pidvid[1];
                        ?>
                        <label>
                          <?php if(isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                            $ch = ' checked';
                          } else {
                            $ch = '';
                          }?>
                          <input type="radio" name="props[<?=$pid?>]" value="<?=$vid?>"<?=$ch?> />
                          <?=$prop->values[$j]->values_title?>
                      <? if (isset($prop->values[$j]->values_count)&&($prop->values[$j]->values_count>0)) { ?>
                          (<?=$prop->values[$j]->values_count?>)
                          <?}?>
                        </label>
                      <?php } ?>
                    <?php } ?>
                  </div>
                <?php } ?>
              </div>
            <?php }
          }?>
            <div class="form-item submit">
                <a href="javascript:void(0);" onclick="$('#search-params').submit(); return false;" class="search-terms-sbmt">
                    <?=Yii::t('main','Поиск')?>
                </a>
            </div>            
	</form>
    </div>

</div>	

<!-- search categories --> 
<?php if($cids) { ?>
<div class="block" id="categories-block">

    <h4 class="title"><?=Yii::t('main','Результаты по категориям')?></h4>
    <div class="block-content categories">        
	<ul id="search-categories">
            <?php if($type == 'search' || $type == 'brand') {
                foreach($cids as $cat) { ?>
            <?php $params['cid'] = $cat->cid; ?>
            <li><a rel="nofollow" href="<?=Yii::app()->createUrl('/'.$type.'/index',$params)?>">
                    <?=$cat->title?>
                <? if ($cat->count>0) { ?>
                    <span>(<?=$cat->count;?>)</span>
               <? } ?>
             </a></li>
            <?php } ?>
           <?php } else {
               foreach($cids as $cat) {
                 if (!isset($cat->url)) {
                   continue;
                 }
                 ?>
            <li><a rel="nofollow" href="<?=Yii::app()->createUrl('/category/index',array('name'=>$cat->url))?>">
                    <?=$cat->title?>
                <? if ($cat->count>0) { ?>
                  <span>(<?=$cat->count;?>)</span>
                <? } ?>
             </a></li>
            <?php }
             }
            ?>
               
	</ul>
    </div>

</div>
<?php } ?>
  <!-- search suggestions -->
<?php if($suggestions) { ?>
  <div class="block" id="categories-block">

    <h4 class="title"><?=Yii::t('main','Похожие результаты')?></h4>
    <div class="block-content categories">
      <ul id="search-categories">
        <?php if($type == 'search' || $type == 'brand') {
          foreach($suggestions as $sugg) { ?>
            <?php $paramsSugg=array();
            $paramsSugg['cid'] = $sugg->cid;
                  $paramsSugg['query'] = $sugg->q;
            ?>
            <li><a rel="nofollow" href="<?=Yii::app()->createUrl('/'.$type.'/index',$paramsSugg)?>">
                <?=$sugg->title?>
              </a></li>
          <?php } ?>
        <?php } else {
          foreach($suggestions as $sugg) { ?>
            <?php $paramsSugg=array();
            $paramsSugg['cid'] = $sugg->cid;
            $paramsSugg['query'] = $sugg->q;
            ?>
            <li><a rel="nofollow" href="<?=Yii::app()->createUrl('/'.'search'.'/index',$paramsSugg)?>">
                <?=$sugg->title?>
              </a></li>
          <?php } ?>
          <?php } ?>

      </ul>
    </div>

  </div>
<?php } ?>