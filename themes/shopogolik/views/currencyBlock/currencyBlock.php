<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="currencyBlock.php">
* </description>
**********************************************************************************************************************/?>
<div class="currency"><?=Yii::t('main','Валюта')?>:
  <select class="currency-sel" name="currency" onchange="location = this.options[this.selectedIndex].value;">
    <? $currency_array=explode(',',DSConfig::getVal('site_currency_block'));
    foreach ($currency_array as $val) { ?>
      <option value="/user/setcurrency/curr/<?=$val?>" <? if ($currency==$val) echo ' selected';?>><?=strtoupper($val)?></option>
    <? } ?>
  </select>
</div>