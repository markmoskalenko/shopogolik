<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="question.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title">
    <?=$this->pageTitle?>
</div>
<div class="content">
<div class="form"> 
<p style="text-align:center; color:red"><?=Yii::t('main','Символом * отмечены поля, обязательные для заполнения')?></p>
<h4><?=Yii::t('main','Задать вопрос в службу поддержки')?>:</h4>
<?php $form=$this->beginWidget('CActiveForm', array( 
    'id'=>'message-form',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableAjaxValidation'=>false, 
)); ?>
    <div class="row" style="width:900px;"> 
        <?php echo $form->labelEx($model,'theme',array('style'=>'width:180px;')); ?>
        <?php echo $form->textField($model,'theme',array('style'=>"width:450px;",'size'=>360,'maxlength'=>128,
            //'style'=>'width:550px;'
            )
                ); ?>
        <?php echo $form->error($model,'theme'); ?>
    </div> 
    
    <div class="row" style="width:900px;"> 
        <?php echo $form->labelEx($model,'question',array('style'=>'width:180px;')); ?>
        <?php echo $form->textArea($model,'question',array('style'=>"width:450px;",'rows'=>6, 'cols'=>50,
            //'style'=>'width:550px;'
            )); ?>
        <?php echo $form->error($model,'question'); ?>
    </div> 

    <div class="row" style="width:900px;"> 
        <?php echo $form->labelEx($model,'email',array('style'=>'width:180px;')); ?>
        <?php echo $form->textField($model,'email',array('style'=>"width:450px;",'size'=>60,'maxlength'=>128, 
            //'style'=>'width:550px;'
            )); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    
    <div class="row" style="width:900px;">
        <?php echo $form->labelEx($model,'category',array('style'=>'width:180px;')); ?>
        <?php echo $form->dropDownList($model,'category',$category_values); ?>
    </div>
    
    <div class="row" style="width:900px;">
        <?php echo $form->labelEx($model,'order_id',array('style'=>'width:180px;')); ?>
        <?if(count($orders) > 0){?>
            <?php echo $form->dropDownList($model,'order_id',$orders); ?>
        <?}else{?>
            <?php echo $form->textField($model,'order_id',array('style'=>"width:450px;",'size'=>360,'maxlength'=>128,
                //'style'=>'width:550px;'
                )); ?>
        <?}?>
    </div>
    <div class="row" style="width:900px;">
        <?php echo $form->labelEx($model,'file',array('style'=>'width:180px;')); ?>
        <?php echo $form->fileField($model,'file',array('style'=>"width:450px;",'id'=>'file_support'))?>
        <div class="hint"><p><?=Yii::t('main','Размер файла не должен превышать 1 Мб')?></p></div>
    </div>
     
    <div class="row buttons"> 
        <?php echo CHtml::submitButton(Yii::t('main','Отправить вопрос'),array('style'=>'width:130px;float:left;margin-left:120px;','class'=>'blue-btn bigger')); ?>
        <?php echo CHtml::htmlButton(Yii::t('main','Удалить файл'),array('style'=>"float:right;",'class'=>'blue-btn bigger','id'=>'remove_file_support')); ?>
    </div> 

<?php $this->endWidget(); ?>
      
</div>
</div>
<div style="margin-top:340px;" class="text"><?=cms::customContent('support')?></div>