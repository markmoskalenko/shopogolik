<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="calculator.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title">
    <?=$this->pageTitle?>
</div>
<div class="content">
    <div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'calc-form',	
        ));?>

    <div class="clear"></div>
    <div class="row">
                <?php echo $form->labelEx($model,'country'); ?>
		<?php
                    if($selected_country){
                        echo $form->dropDownList($model,'country',Deliveries::getCountries(),array('options' => array($selected_country=>array('selected'=>true))));
                    }else{
                        echo $form->dropDownList($model,'country',Deliveries::getCountries());
                    }
                ?>
		<?php echo $form->error($model,'country'); ?>
    </div>
    <div class="row">
                <?php echo $form->labelEx($model,'weight'); ?>
		<?php echo $form->textField($model,'weight'); ?>
		<?php echo $form->error($model,'weight'); ?>
    </div>
    <div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main','Посчитать'),array('class'=>'blue-btn bigger')); ?>
    </div>

    <?php $this->endWidget(); ?>
    </div>
    <?php if($res) { ?>
    <div class="clear"></div>
      <? if (count($delivery)>0) { ?>
    <table class="calculator">
      <? foreach ($delivery as $i=>$del) { ?>
        <tr>
            <th><?if ($del->summ>0) {
                echo Formulas::priceWrapper(Formulas::convertCurrency($del->summ,DSConfig::getSiteCurrency(),DSConfig::getCurrency()));
              } else {
                echo Yii::t('main','Рассчитывается при заказе');
              } ?></th>
            <td><b><?=Yii::t('main',$del->name)?></b><br />
            <?=Yii::t('main',$del->description)?>
            </td>
        </tr>
      <? } ?>
    </table>
        <? } else { ?>
        <p><b><?=Yii::t('main','К сожалению, доставка в выбранную Вами страну или регион с указанынми Вами параметрами временно не производится.')?></b></p>
        <? } ?>
    <? } ?>
  <div class="page-title">
    <?=Yii::t('main','Справочник веса товаров по категориям');?>
  </div>
  <div class="cabinet-table">
  <? $this->widget('zii.widgets.grid.CGridView',array(
    'id'=>'weights-grid',
    'dataProvider'=>$weights->search(),
    'filter'=>$weights,
    'pager'=>array(
      'header'         => '',
      'firstPageLabel' => '&lt;&lt;',
      'prevPageLabel'  => '&lt;',
      'nextPageLabel'  => '&gt;',
      'lastPageLabel'  => '&gt;&gt;',
    ),
    'template'=>'{summary}{items}{pager}',
    'summaryText'=>Yii::t('main','Значения').' {start}-{end} '.Yii::t('main','из').' {count}',
    'columns'=>array(
//      'id',
//      'cid',
//      'num_iid',
      array('name'=>'ru',
//            'filter'=>false,
      ),
      array('name'=>'en',
            'filter'=>false,
      ),
      array('name'=>'min_weight',
            'filter'=>false,
      ),
      array('name'=>'max_weight',
            'filter'=>false,
      ),
    ),
  ));
  ?>
</div>
</div>