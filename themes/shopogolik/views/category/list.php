<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="list.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title">
    <?=$this->pageTitle?>
</div>
<div class="content">
  <? $mainMenu=$categories;
  $useVirtualMenu=DSConfig::getVal('search_useVirtualMenu')==1; ?>
  <ul>
    <?php if (isset($mainMenu))
      foreach($mainMenu as $id=>$menu) { ?>
        <?php
        $type='category';
        if (isset($menu['children']) && (count($menu['children'])>0) && !$useVirtualMenu) {
          $link = Yii::app()->createUrl('/category/page',array('page_id'=>$menu['pkid']));
        } else {
          $link = Yii::app()->createUrl('/'.$type.'/index',array('name'=>$menu['url']));
        }
        ?>
        <li class="parent">
          <h3><a href="<?=$link?>">
            <?=$menu['view_text']?>
          </a></h3>
          <?php if (isset($menu['children']) && count($menu['children'])>0) { ?>
            <ul>
              <? if ($useVirtualMenu) { ?>
                <li>
                  <table class="categories-table">
                    <?php if (isset($menu['children']) && count($menu['children'])>0)
                      foreach($menu['children'] as $id2=>$items2) { ?>
                        <?if (isset($items2) && isset($items2['url'])){?>
                          <tr>
                            <td>
                              <a href="<?=Yii::app()->createUrl('/'.$type.'/index',array('name'=>$items2['url']))?>">
                                <b><?=$items2['view_text']?></b>
                              </a>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php if (isset($menu['children']) && count($menu['children'])>0)
                                //  foreach($menu['children'] as $id2=>$items2) { ?>
                              <?if (isset($items2['children']) && count($menu['children'])>0)
                                foreach($items2['children'] as $id3=>$items3) {?>
                                  <a href="<?=Yii::app()->createUrl('/'.$type.'/index',array('name'=>$items3['url']))?>">
                                    <?=$items3['view_text']?>
                                  </a>&nbsp;
                                <?}?>
                              <? // } ?>
                            </td>
                          </tr>
                        <?}?>
                      <?php } ?>
                  </table>
                </li>

              <? } else { ?>
                <?php if (isset($menu['children']) && count($menu['children'])>0) { ?>
                  <li>
                    <table class="categories-table">
                      <tr>
                        <td>
                          <?  foreach($menu['children'] as $id2=>$items2) { ?>

                            <?if (isset($items2) && isset($items2['url'])){?>
                              <a href="<?=Yii::app()->createUrl('/'.$type.'/index',array('name'=>$items2['url']))?>">
                                <?=$items2['view_text']?>
                              </a>&nbsp;
                            <?}?>
                          <?php } ?>
                        </td>
                      </tr>
                    </table>
                  </li>
                <? } ?>
              <? } ?>

            </ul>
          <? } ?>
        </li>
      <?php } ?>
  </ul>
</div>