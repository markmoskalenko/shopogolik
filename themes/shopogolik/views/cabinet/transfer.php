<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="transfer.php">
* </description>
**********************************************************************************************************************/?><div class="blue-tabs">
    <a href="<?=Yii::app()->createUrl('/cabinet/balance')?>">
	<span><?=Yii::t('main','Выписка по счету')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/payment')?>">
        <span><?=Yii::t('main','Пополнить счет')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/statement')?>">
        <span><?=Yii::t('main','Информация о счете')?></span>
    </a>
    <div class="active-tab">
	<span><?=$this->pageTitle?></span>
    </div>
</div>
<div class="cabinet-content">
    <p>Доступно для перевода: <b><?=Formulas::priceWrapper(Formulas::convertCurrency(Users::getBalance(Yii::app()->user->id),DSConfig::getVal('site_currency'),DSConfig::getCurrency()),DSConfig::getCurrency())?></b></p>
    <div class="form">
        <?=$form?>
    </div>
</div>
