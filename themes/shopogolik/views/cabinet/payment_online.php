<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="payment_online.php">
* </description>
**********************************************************************************************************************/?><div class="blue-tabs">
    <a href="<?=Yii::app()->createUrl('/cabinet/balance')?>">
	<span><?=Yii::t('main','Информация о счёте')?></span>
    </a>
    <div class="active-tab">
	<span><?=$this->pageTitle?></span>
    </div>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/statement')?>">
        <span><?=Yii::t('main','Информация о счете')?></span>
    </a>
    <a href="<?=Yii::app()->createUrl('/cabinet/balance/transfer')?>">
        <span><?=Yii::t('main','Перевод денег на другой счет')?></span>
    </a>
</div>
<div class="cabinet-content">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title><?=CHtml::encode(DSConfig::getVal('site_name').': '.Yii::t('main','Пополнение счёта'))?></title>
      <link href="//favicon.ico" type="image/x-icon" rel="icon" />
 <?=PaySystems::preRenderForm($data,$type); ?>
</div>
<!-- RoboKassa <script language='javascript' type='text/javascript'
 src='https://auth.robokassa.ru/Merchant/Handler/MrchSumPreview.ashx?MrchLogin=demo&OutSum=10.96&InvId=0&Desc=ROBOKASSA+Advanced+User+Guide&Shp_code=Asp&Shp_item=&SignatureValue=1c980c5b65d4e60b0f395ea2ddf6ed16&Culture=ru&Encoding=utf-8'>
</script>
-->