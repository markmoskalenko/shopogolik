<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="list.php">
* </description>
**********************************************************************************************************************/?>
<?php
$checkout_order_reconfirmation_needed = DSConfig::getVal('checkout_order_reconfirmation_needed') == 1;
?>
<div class="cabinet-content">
  <div>
    <? $status=OrdersStatuses::model()->find('value=:value',array(':value'=>$type)); ?>
    <h3><?= Yii::t('main', $status['name']) ?></h3>
  </div>
  <?
  $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'orders-grid',
    'dataProvider' => $orders,
    'enableSorting' => FALSE,
//    'filter'=>$model,
    'pager' => array(
      'header' => '',
      'firstPageLabel' => '&lt;&lt;',
      'prevPageLabel' => '&lt;',
      'nextPageLabel' => '&gt;',
      'lastPageLabel' => '&gt;&gt;',
    ),
//    'type'=>'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'summaryText' => Yii::t('main', 'Заказы') . ' {start}-{end} ' . Yii::t('main', 'из') . ' {count}',
    'columns' => array(
      array(
        'name' => 'id',
        'type' => 'raw', //$model->id
        'value' => 'CHtml::link(CHtml::encode($data->uid."-".$data->id),array("/cabinet/orders/view", "id"=>$data->id))',
      ),
      array(
          'header' => Yii::t('main', 'Товары'),
          'type' => 'raw',
          'value'=>'Order::getOrderItemsPreview($data->id,"_60x60.jpg")',
          'htmlOptions'=>array('style'=>'min-width:145px;height:60px;padding:3px;')
      ),
      array(
        'name' => 'date',
        'type' => 'raw',
        'value' => 'date("d.m.Y H:i",$data->date)'
      ),
      array(
        'name' => 'sum',
        'type' => 'raw',
        'value' => 'Formulas::priceWrapper(Formulas::convertCurrency($data->sum,DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
      ),
      array(
        'name' => 'weight',
        'type' => 'raw',
        'value' => '$data->weight'
      ),
      array(
        'name' => 'delivery_id',
        'type' => 'raw',
        'value' => '$data->delivery_id'
      ),
      array(
        'name' => 'delivery',
        'type' => 'raw',
        'value' => 'Formulas::priceWrapper(Formulas::convertCurrency($data->delivery,DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
      ),
      array(
        'header' => Yii::t('main', 'Итого'),
        'type' => 'raw',
        'value' => 'Formulas::priceWrapper(Formulas::convertCurrency($data->sum+$data->delivery,DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
      ),
      /*      array(

              'type'=>'raw',
              'value'=>'"
                    <a href=\'javascript:void(0);\' onclick=\'renderView(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
                    <a href=\'javascript:void(0);\' onclick=\'renderUpdateForm(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
                    <a href=\'javascript:void(0);\' onclick=\'delete_record(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-trash\'></i></a>
                   "',
              'htmlOptions'=>array('style'=>'width:150px;')
            ),
      */
    ),
  ));
  ?>
</div>