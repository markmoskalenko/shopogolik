<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CartBlock.php">
* </description>
**********************************************************************************************************************/?>
<div class="basket-text"><?=Yii::t('main','Товаров в корзине')?>:</div>
<a href="<?=Yii::app()->createUrl('/cart/index')?>" class="basket">
    <img src="<?=Yii::app()->request->baseUrl?>/themes/<?=Yii::app()->theme->name?>/images/basket<?=(count($cart->cartRecords)>0) ? '-full' : ''; ?>.png" alt="<?=Yii::t('main','Товаров в корзине')?>" />
</a>
<a href="<?=Yii::app()->createUrl('/cart/index')?>" class="basket-info">
  <? if (count($cart->cartRecords)>0) { ?>
  <span <?=(!$cart->allowOrder)? '' : '' ?>><?=count($cart->cartRecords).'&nbsp;'.Yii::t('main','на сумму')?><br><?=Formulas::priceWrapper($cart->total)?></span>
    <?=(!$cart->allowOrder)? '<div>'.Yii::t('main','Нужен дозаказ на сумму').': '.Formulas::priceWrapper($cart->summAddToAllowOrder).'</div>':''?>
  <? } else { ?>
    <br><?=Yii::t('main','Корзина пуста')?>
  <? } ?>


</a>