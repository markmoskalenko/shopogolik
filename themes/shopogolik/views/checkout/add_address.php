<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="add_address.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title"><h4><?=Yii::t('main','Оформление заказа')?></h4></div>
<div class="content">
    <h4><?=$this->pageTitle?></h4>
    <div class="form">
        <?=$form?>
    </div>
</div>