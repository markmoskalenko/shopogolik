<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
$seo_disable_items_index=DSConfig::getVal('seo_disable_items_index')==1;
$i = 0;
?>
<div class="blue-tabs">
  <? $base_params = $params;
  if (isset($base_params['recommend']))
  {
    unset($base_params['recommend']);
  }
  if (isset($base_params['users'])) {
    unset($base_params['users']);
  }
  ?>
    <a href="<?=Yii::app()->createUrl('/'.$this->id.'/index',$base_params)?>"
       <?php if(!isset($_GET['recommend']) || $_GET['recommend']==0)echo 'class="active"'?>
       ><span><?=Yii::t('main','Все товары')?></span></a>
    <?php if($this->id !== 'seller'):?>
    <a href="<?=Yii::app()->createUrl('/'.$this->id.'/index',  array_merge($params,array('recommend'=>1)))?>"
       <?php if(isset($_GET['recommend']) && $_GET['recommend']==1)echo 'class="active"'?>
       >
        <span><?=Yii::t('main','Рекомендованные товары')?></span></a>
    <?php endif; ?>
  <? if (isset($res->zh_query) && ($res->zh_query)) {?>
    <div style="float: right; padding-right: 8px; padding-top: 8px;">
      <?=Yii::t('main','Поисковый запрос').': '.$res->zh_query?>
    </div>
  <? } ?>
</div>
<?php if(!empty($category->{'page_desc_'.Utils::TransLang()})):?>
<div class="category-desc">
    <?=$category->{'page_desc_'.Utils::TransLang()}?>
</div>
<?php endif; ?>
<?php if(is_object($res)):?>
    <div style="padding-top: 50px;">
        <div class="search-sort">
            <?$fAction=Yii::app()->createUrl('/'.$this->id.'/index',$params);?>
            <form id="search-sort" action="<?=$fAction;?>" method="post">
                <?=Yii::t('main','Сортировка')?>:
                <select name="sort_by" id="sort_by" onchange="changeSortOrder('<?=$fAction;?>');">
                    <option value="popularity_desc"<? if ($sort_by=='popularity_desc') echo ' selected';?>>
                        <?=Yii::t('main','По популярности')?>
                    </option>
                    <option value="credit_desc"<? if ($sort_by == 'credit_desc') echo ' selected'; ?>>
                        <?= Yii::t('main', 'По рейтингу продавца') ?>
                    </option>
                    <option value="delistTime_desc"<? if ($sort_by == 'delistTime_desc') echo ' selected' ?>>
                        <?= Yii::t('main', 'Новинки') ?>
                    </option>

                    <option value="price_asc"<? if ($sort_by=='price_asc') echo ' selected';?>>
                        <?=Yii::t('main','Цена: по возрастанию')?>
                    </option>
                    <option value="price_desc"<? if ($sort_by=='price_desc') echo ' selected';?>>
                        <?=Yii::t('main','Цена: по убыванию')?>
                    </option>
                </select>
            </form>
        </div>
        <div class="search-original">
            <form id="search-original" action="<?=Yii::app()->createUrl('/'.$this->id.'/index',$orig_params)?>" method="get">
                <label>
                    <input type="checkbox" onchange="$('#search-original').submit();" name="original"
                        <?=(isset($params['original']))?' checked':''?> />
                    <?=Yii::t('main','Товары со скидкой')?>
                </label>
              <label>
                <input type="checkbox" onchange="$('#search-original').submit();" name="not_unique"
                  <?=(isset($params['not_unique']))?' checked':''?> />
                <?=Yii::t('main','Повторяющиеся товары')?>
              </label>
            </form>
        </div>
        <div class="search-count"><?=Yii::t('main','Всего предложений')?>:
            <span><?=(isset($res->total_results))?$res->total_results:0?></span></div>
    </div>
    <?php if($pages) ?>
    <?
        $this->widget('application.components.blocks.CSEOLinkPager', array(
            'pages' => $pages,
            'header' => '',
            'linkHtmlOptions' =>array('rel'=>'nofollow'),
            'prevPageLabel'=>'&lt;',
            'nextPageLabel'=>'&gt;',
        ))
    ?>
    <div class="products-list">

    <?php if(isset($res->items)) foreach($res->items as $item) { ?>
      <?php
      $first = ($i%4 == 0) ? ' first' : '';
      $i++;
      ?>
<? $this->widget('application.components.blocks.SearchItem', array(
   'searchResItem' => $item,
   'newLine' => $first,
//  'showControl' => NULL,
//  'disableItemForSeo' => true,
//  'imageFormat' => '_160x160.jpg',
      ));
      ?>
    <? } ?>
      </div>
    <?php endif ?>
<?php if($pages)
        $this->widget('application.components.blocks.CSEOLinkPager', array(
            'pages' => $pages,
            'header' => '',
          'linkHtmlOptions' =>array('rel'=>'nofollow'),
          'prevPageLabel'=>'&lt;',
          'nextPageLabel'=>'&gt;',
     ))
?>
  <div class="page-title"><?=Yii::t('main','Недавно просмотренные другими')?>:</div>
  <div class="products-list featured">
    <? $this->widget('application.components.blocks.SearchItemsList',array(
      'id'=>'recentAll-itemslist',
      'controlAddToFavorites' => true,
      'controlAddToFeatured' => true,
      'controlDeleteFromFavorites' => false,
      'lazyLoad' => true,
      'dataType'=>'itemsRecentAll',
      'pageSize' =>8,
//      'showControl' => null,
      'disableItemForSeo' => $seo_disable_items_index,
//      'imageFormat' => '_160x160.jpg',
    ));
    ?>
  </div>
<!-- Виджет кнопки вверх (Test-Templates) -->
<div class="search-sidebar">
  <div class="search-sidebar-top">▲</div>
  <div class="search-sidebar-next"><a href="#">▶</a></div>
  <div class="search-sidebar-prev"><a href="#">◀</a></div>
</div>
<? if (isset($res->debugMessages)) {?>
<!--  <script type="text/javascript" src="/themes/admin/js/jquery.zclip.js"></script> -->
  <br/>
  <br/>
  <br/>
  <br/>
  <hr/>
  <div>
  <?
$this->widget('bootstrap.widgets.TbGridView',array(
  'id'=>'dsg-debug-grid',
  'dataProvider'=>$res->debugMessages,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}<br/>{pager}{items}{pager}',
  'columns'=>array(
    'function',
    'param_name',
    array('name'=>'param_value',
          'type'=>'raw',
         'value'=> function($data) {
             //$res=preg_replace("/(.{128,256})\s.*/s","\1\.\.\.",$data["param_value"]);
             $res='<textarea rows="3" cols="20">'.htmlspecialchars($data['param_value']).'</textarea>';
             return $res;
          },
    ),
    array('name'=>'subject',
          'type'=>'raw',
          'value'=> function($data) {
              $res='<textarea rows="3" cols="20">'.htmlspecialchars($data['subject']).'</textarea>';
              return $res;
           },
    ),
    array('name'=>'result',
          'type'=>'raw',
          'value'=> function($data) {
              $res='<textarea rows="3" cols="20">'.htmlspecialchars($data['result']).'</textarea>';
              return $res;
            },
    ),
    array('name'=>'valid',
      'type'=>'raw',
      'value' => function($data) {
          $res=($data['valid']) ? 'true' : '<span style="color: red;"><strong>false</strong></span>';
          return $res;
        }
    ),

  ),
));
  ?>
  </div>
<?}?>