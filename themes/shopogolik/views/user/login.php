<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="login.php">
* </description>
**********************************************************************************************************************/?>
<div class="page-title">

    <?=$this->pageTitle?>

</div>
<div class="form">
    <?=$form->render()?>
    <a href="<?=$this->createUrl('/user/register')?>"><?=Yii::t('main','Зарегистрироваться')?></a>
    <br />
    <a href="<?=$this->createUrl('/user/password')?>"><?=Yii::t('main','Забыли пароль?')?></a>
</div>