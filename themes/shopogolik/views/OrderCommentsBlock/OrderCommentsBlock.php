<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrderCommentsBlock.php">
* </description>
**********************************************************************************************************************/?>
<div id="accordion-connemts-<?= $blockId ?>">
  <h3><i class="icon-file"></i><?=Yii::t('main','Комментарии')?>: <?=$dataProvider->totalItemCount?></h3>
  <div>
    <div class="comment-block">
      <?
        $this->widget('bootstrap.widgets.TbGridView', array(
          'id'           => 'grid-' . $blockId,
          'dataProvider' => $dataProvider,
          'type'         => 'striped bordered condensed',
          'template'     => '{items}{pager}', //{summary}{pager}
          'columns'      => array(
            array(
              'name'        => 'fromName',
              'htmlOptions' => array('style' => 'width:15%;font-size:0.9em;color:#00BCFF;'),
            ),
            array(
              'name'        => 'date',
              'htmlOptions' => array('style' => 'width:15%;font-size:0.9em;'),
            ),
            array(
              'name'        => 'message',
              'type'        => 'raw',
              'htmlOptions' => array('style' => 'width:70%;'),
              'value'       => function ($data) {
                  return Yii::app()->controller->widget("application.components.blocks.OrderCommentsViewBlock", array(
                    "message"     => $data->message,
                    "itemId"      => $data->id,
                    "isItem"      => isset($data->item_id),
                    "pageSize"    => 5,
                    "imageFormat" => "_200x200.jpg",
                  ), true);
                },
            ),
          ),
        ));
      ?>

      <div class="comment-btn-1">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
          'buttonType'  => 'submit',
          'type'        => 'success',
          'size'        => 'mini',
          'icon'        => 'ok white',
          'label'       => Yii::t('admin', 'Добавить комментарий'),
          'htmlOptions' => array('style'=>'','onclick' => '$("#new-message-' . $blockId . '").dialog("open");false;'),
        ));
        ?>
      </div>
    </div>
    <div id="new-message-<?= $blockId ?>" title="<?= Yii::t('main', 'Новое сообщение') ?>" style="display: none;">
      <form id="new-message-form-<?= $blockId ?>">
        <? if ($isItem) { ?>
          <label><b><?= Yii::t('main', 'Коментарий к лоту') ?>:</b></label><br/>
        <?
        }
        else {
          ?>
          <label><b><?= Yii::t('main', 'Коментарий к заказу') ?>:</b></label><br/>
        <? } ?>
        <input type="hidden" name="message[isItem]" value="<?= (int) $isItem ?>"/>
        <input type="hidden" name="message[parentId]" value="<?= $parentId ?>"/>
        <textarea cols="80" rows="3" name="message[message]" id="message-<?= $blockId ?>"></textarea>
        <? if ($public) { ?>
          <input type="hidden" name="message[public]" value="1"/>
        <? }
        else { ?>
          <br><input type="checkbox" name="message[public]" value="1"/><?= Yii::t('main', 'Виден заказчику') ?>
        <? } ?>
      </form>
      <br/>
      <button id="ok-<?= $blockId ?>" onclick="
        $('#new-message-<?= $blockId ?>').dialog('close');
        var msg = $('#new-message-form-<?= $blockId ?>').serialize();
                $.post('<?= Yii::app()->createUrl('/') ?>message/create',msg, function(){
        $('#message-<?= $blockId ?>').val('');
        $.fn.yiiGridView.update('grid-<?= $blockId ?>');
        },'text');
        return false;"><?= Yii::t('admin', 'Сохранить') ?></button>
      &nbsp;&nbsp;
      <button id="cancel" onclick="$('#new-message-<?= $blockId ?>').dialog('close');
        $('#message-<?= $blockId ?>').val('');
        return false;"><?= Yii::t('admin', 'Отмена') ?></button>
      <br><br>
    </div>
  </div>
</div>
<hr/>
<script type="text/javascript">
  $(function () {
  $("#new-message-<?=$blockId?>").dialog({
    autoOpen: false,
    width: "auto",
    modal: true,
    resizable: false
  });
  $("#accordion-connemts-<?=$blockId?>").accordion({
    collapsible: true,
    active: <?=($dataProvider->totalItemCount>0) ? '0' : 'false';?>
    //disabled: true
  });
    });
</script>

