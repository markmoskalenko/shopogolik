<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="list.php">
* </description>
**********************************************************************************************************************/?><div class="blue-tabs">
  <div class="active-tab">
    <span><?=$this->pageTitle?></span>
  </div>
</div>
<div class="cabinet-content">
  <?php
  $this->widget('zii.widgets.CMenu',array(
    'items'=>array(
//      array('label'=>Yii::t('main','Очистить'), 'url'=>Yii::app()->controller->createUrl('clear')),
//      array('label'=>Yii::t('main','Excel'),'url'=>Yii::app()->controller->createUrl('getExcel')),
      array('label'=>Yii::t('main','Экспорт в YML'), 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('getYML',array('uid'=>Yii::app()->user->id))),
//      array('label'=>Yii::t('main','Экспорт в CSV'), 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('getCSV',array('uid'=>Yii::app()->user->id))),
      array('label'=>Yii::t('admin','Очистить'), 'icon'=>'icon-refresh',
            'url'=>Yii::app()->controller->createUrl('delete', array('id' => -1))),
/*
        array('label'=>Yii::t('admin','Очистить кэш'), 'icon'=>'icon-refresh', 'url'=>'javascript:void(0);','linkOptions'=>array('onclick'=>'clearMenuCache()')),
		array('label'=>Yii::t('admin','Создать'), 'icon'=>'icon-plus', 'url'=>'javascript:void(0);','linkOptions'=>array('onclick'=>'renderCreateForm()')),
<a class="ui-icon ui-icon-heart" style="display:inline-block; cursor: pointer;"
                     title="<?= Yii::t('admin', 'Добавить в рекомендованное') ?>"
                     href="<?= Yii::app()->createUrl('/admin/featured/add/', array('id' => $item->top_item->num_iid)) ?>"
                     onclick="addFeatured(this,<?= $item->top_item->num_iid ?>); return false;"></a>
 */

    )));
  ?>
  <div>
  <?=Yii::t('main','Категорий для экспорта').':'.$categoriesCount;?>
  <?=Yii::t('main','Товаров для экспорта').':'.$itemsCount;?>
  </div>
  <div class="cabinet-table">
    <? $this->widget('application.components.blocks.SearchItemsList',array(
      'id'=>'favorite-itemslist',
  'showControl' => true,
  'controlAddToFavorites' => false,
  'controlAddToFeatured' => false,
      'controlDeleteFromFavorites' => true,
  'lazyLoad' => true,
  'dataType'=>'itemsFavorite',
  'pageSize' =>40,
  'dataProvider'=>$dataProvider,
    ));
    ?>
  </div>
</div>
