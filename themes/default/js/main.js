function dsAlert(text,title,uidlg) {
    if ((text!=undefined) && (text.length>0)) {
        if (!uidlg) {
            alert(text);
        } else {
            $('#ds-interface-alert').remove();
            $('<div id="ds-interface-alert" title="'+title+'">'+text+'</div>').dialog(
                {
                    closeOnEscape: true,
                    resizable: false,
                    modal: true,
                    buttons: {
                        Ok: function() {
                            $( this ).dialog( "close" );
                            $('#ds-interface-alert').remove();
                        }
                    }
                }
            )
        }
    }
}
function dsConfirm(text,title,uidlg) {
    if ((text!=undefined) && (text.length>0)) {
        if (!uidlg) {
            return confirm(text);
        } else {
            $('#ds-interface-confirm').remove();
            $('<div id="ds-interface-confirm" title="'+title+'">'+text+'</div>').dialog(
                {
                    closeOnEscape: true,
                    resizable: false,
                    modal: true,
                    buttons: {
                        Ok: function() {
                            $( this ).dialog( "close" );
                            $('#ds-interface-confirm').remove();
                            return true;
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                            $('#ds-interface-confirm').remove();
                            return false;
                        }
                    }
                }
            )
        }
    }
}

function clearCart() {
    if (confirm('Вы действительно хотите полностью очистить корзину?')) {
        var url = '/cart/deleteAll';
        //$.post(url,null,function(data){alert(data);},"text");
       var cartTable=document.getElementById("cart-table");
        if ((cartTable !== undefined) && (cartTable!=null)) {
            cartTable.style.visibility = "hidden";
        }
        var cartEmpty=document.getElementById("cart-empty");
        if (cartEmpty !== undefined && (cartEmpty!=null)) {
            cartEmpty.style.visibility = "visible";
        }
        $.post(url)
            .done(function( data ) {
                location.reload();
            });
    }
}
function triger_user(type, hide) {
    var btn=$("#"+type+"-btn");
	if( btn.hasClass("active") || hide) {
		btn.removeClass("active");
		$(".user-block").removeClass("shown");
		$("#"+type+"-block").hide();
	} else {
		$("#"+type+"-btn").addClass("active");
		$(".user-block").addClass("shown");
		$("#"+type+"-block").show();
	}	
}
/*
function reload_rating(id,val) {
	var width = 16;
	$("#rating_"+id+" .rating_text").text(val);
	if(0 == val) {
		$("#rating_"+id+" .rating-icons").css("width","16px").attr("level","0");
	}
	if(val < 6 && val > 0) {
		width = width * val;
		$("#rating_"+id+" .rating-icons").css("width",width+"px").attr("level","1");
	}
	if(val < 11 && val > 5) {
		width = width * (val-5);
		$("#rating_"+id+" .rating-icons").css("width",width+"px").attr("level","2");
	}
	if(val < 16 && val > 10) {
		width = width * (val-10);
		$("#rating_"+id+" .rating-icons").css("width",width+"px").attr("level","3");
	}
	if(val < 21 && val > 15) {
		width = width * (val-15);
		$("#rating_"+id+" .rating-icons").css("width",width+"px").attr("level","4");
	}
	$("#rating_"+id+"_value").val(val);
}
*/
function mycarousel_initCallback(carousel) {
    jQuery('.externalcontroll a').bind('click', function() {
        carousel.scroll(jQuery.jcarousel.intval(jQuery(this).attr("class")));
        return false;
    });
}

$(document).ready(function(){
    $(".chosen-grp").chosen({
        width: "210px",
        search_contains: true,
        no_results_text: 	"-",
        placeholder_text_multiple: 	"*",
        placeholder_text_single: 	"**"

    });
//================================
// famous fucking DIAPO - 4mel
//    try {
    var mainCameraSlider = $('#main_camera_slider');
    if (mainCameraSlider.length) {
        mainCameraSlider.camera({
        loader				: 'none',
        fx                  : 'simpleFade',
        height				: '145px',
        pagination			: true,
        navigation			: true,
        playPause			: false,
        time				: 100,
        transPeriod			: 1500,
        overlayer			: false
});
    }
    var BrandsCameraSlider=$('#brands_camera_slider');
    if (BrandsCameraSlider.length) {
        BrandsCameraSlider.camera({
        loader				: 'none',
        fx                  : 'simpleFade',
        height				: '50px',
        pagination			: false,
        navigation			: true,
        playPause			: false,
        time				: 500,
        transPeriod			: 500,
        overlayer			: false,
        rows				: 3,
        cols: 10
    });
    }
//    } catch (e) {

//    }
//== Suggestions ===============================
    $("#query").dvautocomplete("/suggestions", {
        autoFill:false,
        delay:500,
        minChars:3,
        matchSubset:1,
        matchContains:0,
        cacheLength:1,
        selectFirst:false,
        formatItem:liFormat,
        maxItemsToShow:-1,
        onItemSelect:selectItem
        /*
         autoFill – когда Вы начинаете вводить текст, в поле ввода будет подставлено (и выделено) первое подходящее значение из списка. Если Вы продолжаете вводить текст, в поле ввода и далее будет подставляться подходящее значение, но уже с учетом введенного Вами текста. (По умолчанию: false).
         inputClass – этот класс будет добавлен к элементу ввода. (По умолчанию: «ac_input»).
         resultsClass – класс для UL, который будет содержать элементы результата (элементы LI). (По умолчанию: «ac_results»).
         loadingClass – класс для элемента ввода, в то время, когда происходит обработка данных на сервере. (По умолчанию: «ac_loading»).
         lineSeparator – символ, который разделяет строки в данных, возвращаемых сервером. (По умолчанию: «\n»).
         cellSeparator – символ, который разделяет «ячейки» в строках данных, возвращаемых сервером. (По умолчанию: «|»).
         minChars – минимальное число символов, которое пользователь должен напечатать перед тем, как будет активизирван запрос. (По умолчанию: 1).
         delay – задержка в миллисекундах. Если в течение этого времени пользователь не нажимал клавиши, активизируется запрос. Если используется локальный запрос (к данным, находящимся непосредственно в файле), задержку можно сильно уменьшить. Например до 40ms. (По умолчанию: 400).
         cacheLength – число ответов от сервера, сохраняемых в кэше. Если установлено в 1 – кэширование данных отключено. Никогда не устанавливайте меньше единицы. (По умолчанию: 1).
         matchSubset – использовать ли кэш для уточнения запросов. Использование этой опции может сильно снизить нагрузку на сервер и увеличить производительность. Не забудьте при этом еще и установить для cacheLength значение побольше. Например 10. (По умолчанию: 1).
         matchCase – использовать ли сравнение чувствительное к регистру символов (только если Вы используете кэширование). (По умолчанию: 0).
         maxItemsToShow – ограничивает число результатов, которые будут показаны в выпадающем списке. Если набор данных содержит сотни элементов, может быть неудобно показывать весь список пользователю. Рекомендованное значение 10. (По умолчанию: -1).
         extraParams – дополнительные параметры, которые могут быть переданы на сервер. Если Вы напишете {page:4}, то строка запроса к обработчику на сервере будет сформирована следующим образом: my_handler.php?q=foo&page=4 (По умолчанию: {}).
         width – устанавливает ширину выпадающего списка. По умолчанию ширина выпадающего списка определена шириной элемента ввода. Однако, если ширина элемента ввода небольшая, а строки выпадающего списка содержат большое количество символов – эта опция вполне может пригодиться. (По умолчанию: 0).
         selectFirst – если установить в true, то по нажатию клавиши Tab или Enter будет выбрано то значение, которое в данный момент установлено в элементе ввода. Если же имеется выбранный вручную («подсвеченный») результат из выпадающего списка, то будет выбран именно он. (По умолчанию: false).
         selectOnly – если установить в true и в выпадающем списке только одно значение, оно будет выбрано по нажатию клавиши Tab или Enter, даже если этот пункт не был выбран пользователем с помощью клавиатуры или указателя мыши. Заметьте, что эта опция отменяет действие опции selectFirst. (По умолчанию: false).
         formatItem – JavaScript функция, которая поможет обеспечить дополнительную разметку элементов выпадающего списка. Функция будет вызываться для каждого элемента LI. Возвращаемые от сервера данные могут быть отображены в элементах LI выпадающего списка (см. второй пример). Принимает три параметра: строка результата, позиция строки в списке результатов, общее число элементов в списке результатов. (По умолчанию: none).
         onItemSelect – JavaScript функция, которая будет вызвана, когда элемент списка выбран. Принимает единственный параметр – выбранный элемент LI. Выбранный элемент будет иметь дополнительный атрибут «extra», значением которого будет являться массив всех ячеек строки, которая была получена в качестве ответа от сервера. (По умолчанию: none).
         */

    });
//== End suggestions ===========================
        if( $("div").is(".slider") )
        $(".slider").jcarousel({
//            easing: 'easeInElastic',
//            animation: 750,
/*
 visible: 0,
 auto: 0,
 wrap: 'last',
*/
                scroll: 1,
                auto: 3,
                visible: 1,      
                wrap: 'last',
                initCallback: mycarousel_initCallback,  
                itemLoadCallback: trigger,
                buttonNextHTML: null,
                buttonPrevHTML: null
        });
//================================
/*
        
        if( $("input").is(".weight_input") )
        {
            $(".weight_input").each(function(){
                $(this).keyup(function(){
                    var val = parseInt($(this).val());
                    var id = $(this).attr("id");
                    var count = parseInt($("#weight_"+id+"_count").html());                    
                    $("#weight_"+id).text(val*count);
                })
            });
        }
       
	$("#rating_max .rating-icons").css("width","80px").attr("level","4");
        $("#reg-btn").click(function(){
            triger_user("login",true);
            triger_user("reg");
            return false;
        });
*/
    if( $("div").is(".payment-systems") )
    {
        $(".payment-systems input").each(function(){
            $(this).change(function(){
                $(".desc").each(function(){
                    $(this).hide();
                });
                $(this).parent().find(".desc").show();
            });
        });
    }
	$("#login-btn").click(function(){
		triger_user("reg",true);
		triger_user("login");
		return false;
	});
        $(".tabs a").each(function(){
            $(this).click(function(){
                var id = $(this).attr("id");
                $(".active").removeClass("active");
                $(this).addClass("active");
                $(".tab").fadeOut('fast');
                $("#"+id+"-content").fadeIn('fast');
                return false;
            });
        });
        $("#message-close").click(function(){
            $("#message-block").fadeOut('slow');
            if($(this).parent().hasClass("notice")) {
                return true;
            } else {
            return false;
            }
        });
        
        $('#remove_file_support').click(function(){
            $('#file_support').val(''); 
        });
        
        $('.open_help').click(function(){
            //console.log($(this).parent('div').children('div'));
            
            $('.close').hide();
            $(this).parent('div').children('div').show();
            return false;
        });
        
//        $('#add_favorite_seller').click(function(){
//            $("#add_favorite_seller").toggle("slow");
////            document.add_favorite_seller_form.submit();
////            return false;
//        });
        
        $("#add_address").click(function(){
            var val = $('#hidden_address').val();
            window.location.assign(val);
        });
/*    $.ajaxSetup ({
        // Отменить кеширование AJAX запросов
        cache: false
    });
*/
    loadCatalogMenu(lang);
});

function loadCatalogMenu(lang) {
    var menuPlaceholder=$("#main-cats-menu-ajax");
    if (menuPlaceholder.length>0) {
//        $("#main-cats-menu-ajax").load("/category/menu");
        $.ajax({
            url: "/category/menu/lang/"+lang,
            cache: true,
            dataType: "html",
            success: function(data) {
                menuPlaceholder.html(data);
            }
        });
    }
};


function changeSortOrder(actn) {
    var selection = $("#sort_by :selected").val();
    var action=actn;
    if (action.match(/sort_by\/.+?(?=\/|$)/i)) {
        action= action.replace(/sort_by\/.+?(?=\/|$)/i,'sort_by/'+selection);
    } else {
        action=action+'/sort_by/'+selection;
    }
    $("#search-sort").attr("action", action);
    $('#search-sort').submit();
}

function editTranslation(evt,obj,type,id) {
//$("#sort_by").change(function() {
//    TranslateForm_cn
    var baseUrl = $(obj).attr('url');
    var url = baseUrl+'?type=' + type + '&id=' + id;
    if (url[0]!='/') {
        url='http://'+url;
    }
    $("#TranslateForm_type").val(type);
    $("#TranslateForm_id").val(id);
    $("#TranslateForm_from").val($(obj).attr('from'));
    $("#TranslateForm_to").val($(obj).attr('to'));
    $("#TranslateForm_uid").val($(obj).attr('uid'));
    $("#TranslateForm_url").val(url);
        $.getJSON(url, function (data) {
            if((data !== null) && (data !== undefined)&&(data.zh!='')) {
                $("#TranslateForm_zh").val(data.zh);
                $("#TranslateForm_en").val(data.en);
                $("#TranslateForm_ru").val(data.ru);
                var iframe = $('#translate-bkrs');
                if ( iframe.length ) {
                    iframe.attr('src','http://bkrs.info/slovo.php?ch='+data.zh);
                }
                $("#translationDialog").dialog("open");
                stopPropagation(evt);
            } else {
                dsAlert('Перевод не найден!','',true);
            }
            });
    return false;
}

function saveTranslation() {
    var spanBlock =$("#TranslateForm_type").val()+$("#TranslateForm_id").val();
    var lang=$("#TranslateForm_to").val();
    var t='';
    if (lang=='ru') {
        t=$("#TranslateForm_ru").val();
    } else if (lang=='en')  {
        t=$("#TranslateForm_en").val();
    } else {
        t=$("#TranslateForm_zh").val();
    }
    $('#'+spanBlock).text(t);
    var postform = $("#translation-form").serialize();
    var url = $("#TranslateForm_url").val();
    $.post(url, postform);
    $("#translationDialog").dialog("close");
    return false;
}

function stopPropagation(evt) {
    var ev;
    if (evt) {
    ev = evt;
    } else {
    ev=window.event;
    }
    if (ev.stopPropagation) {
        ev.stopPropagation();
    }
    if (ev.cancelBubble!=null) {
        ev.cancelBubble = true;
    }
}
//==== Suggestions processing ====
// --- Автозаполнение ---
function liFormat (row, i, num) {
    //$res.$row[$lang].'|'.$row['res_count'].'|'.$row['type'].'|'.$row['cid'].'|'.$row['query']
    var result = row[0] + '<p class=qnt>' + row[1] + '&nbsp;</p>';
    return result;
}
function selectItem(li) {
    var sValue = '';
    if( li == null ) {
        sValue = 'Ничего не выбрано!';
    }
    if( !!li.extra ) {
        sValue = JSON && JSON.parse(li.extra[1]) || $.parseJSON(li.extra[1]);
        //$('#query').val(sValue['query']);
        //$('#category').val(sValue['cid']);
        var action = $("#main-search-block").attr("action");
        action=action+'?cid='+sValue['cid'];
        if (sValue['query']!='') {
            action=action+'&query='+sValue['query'];
        }
        window.location.replace(action);
        //window.location.href = "http://stackoverflow.com";
       // $('#main-search-block').submit();
    }
    else {
        sValue = li.selectValue;
    }
}

function deleteFavorite(dellink,id){
    var url = $(dellink).attr('href');
    var elem = $("#item"+id);
    $.get(url,null,
        function(data){
            $(elem).remove();
            dsAlert(data,'',true);
        },
        "text");
}

function addFeatured(addlink,id){
    var url=$(addlink).attr('href');
    if (typeof url == 'undefined') {
        url= $(addlink).attr('formaction');
    }
    $.get(url,null,
        function(data){
            dsAlert(data,'',true);
        },
        "text");
}

function addFavorite(addlink,id){
    var url=$(addlink).attr('href');
    if (typeof url == 'undefined') {
        url= $(addlink).attr('formaction');
    }
    $.get(url,null,
        function(data){
            dsAlert(data,'',true);
        },
        "text");
}

function addCart(addlink,id){
    var url=$(addlink).attr('href');
    if (typeof url == 'undefined') {
        url= $(addlink).attr('formaction');
    }
    $.get(url,null,
        function(data){
            dsAlert(data,'',true);
        },
        "text");
}
//================================