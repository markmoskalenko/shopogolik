<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="list.php">
 * </description>
 * Рендеринг списка заказов в кабинете
 **********************************************************************************************************************/
?>
<div class="cabinet-content">
    <div>
        <? $status = OrdersStatuses::model()->find('value=:value', array(':value' => $type)); ?>
        <h3><?= Yii::t('main', $status['name']) ?></h3>
    </div>
    <?
    $this->widget(
      'zii.widgets.grid.CGridView',
      array(
        'id'            => 'orders-grid',
        'dataProvider'  => $orders,
        'enableSorting' => false,
        'pager'         => array(
          'header'         => '',
          'firstPageLabel' => '&lt;&lt;',
          'prevPageLabel'  => '&lt;',
          'nextPageLabel'  => '&gt;',
          'lastPageLabel'  => '&gt;&gt;',
        ),
        'template'      => '{summary}{items}{pager}',
        'summaryText'   => Yii::t('main', 'Заказы') . ' {start}-{end} ' . Yii::t('main', 'из') . ' {count}',
        'columns'       => array(
          array(
            'name'  => 'id',
            'type'  => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->uid."-".$data->id),array("/cabinet/orders/view", "id"=>$data->id))',
          ),
          array(
            'header'      => Yii::t('main', 'Товары'),
            'type'        => 'raw',
            'value'       => 'Order::getOrderItemsPreview($data->id,"_60x60.jpg")',
            'htmlOptions' => array('style' => 'min-width:145px;height:60px;padding:3px;')
          ),
          array(
            'name'  => 'date',
            'type'  => 'raw',
            'value' => 'date("d.m.Y H:i",$data->date)'
          ),
          array(
            'name'  => 'sum',
            'type'  => 'raw',
            'value' => 'Formulas::priceWrapper(Formulas::convertCurrency((($data->manual_sum)?$data->manual_sum:$data->sum),DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
          ),
          array(
            'name'  => 'weight',
            'type'  => 'raw',
            'value' => '$data->weight'
          ),
          array(
            'name'  => 'delivery_id',
            'type'  => 'raw',
            'value' => '$data->delivery_id'
          ),
          array(
            'name'  => 'delivery',
            'type'  => 'raw',
            'value' => 'Formulas::priceWrapper(Formulas::convertCurrency(
            (($data->manual_delivery || ((string)$data->manual_delivery===\'0\'))?$data->manual_delivery:$data->delivery),
            DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
          ),
          array(
            'header' => Yii::t('main', 'Итого'),
            'type'   => 'raw',
            'value'  => 'Formulas::priceWrapper(Formulas::convertCurrency(
            (($data->manual_sum)?$data->manual_sum:$data->sum)+
            (($data->manual_delivery || ((string)$data->manual_delivery===\'0\'))?$data->manual_delivery:$data->delivery),
            DSConfig::getSiteCurrency(),DSConfig::getCurrency()))'
          ),
        ),
      )
    );
    ?>
</div>