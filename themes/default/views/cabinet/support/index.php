<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 **********************************************************************************************************************/
?>
<div class="blue-tabs">
    <div class="active-tab">
        <span><?= $this->pageTitle ?></span>
    </div>
    <a href="<?= Yii::app()->createUrl('/cabinet/support/history') ?>">
        <span><?= Yii::t('main', 'История обращений') ?></span>
    </a>
</div>
<div class="cabinet-content">
    <h4><?= Yii::t('main', 'Задать вопрос в службу поддержки') ?>:</h4>

    <p style="text-align:center; color:red"><?=
        Yii::t(
          'main',
          'Символом * отмечены поля, обязательные для заполнения'
        ) ?>.</p>

    <div class="form">
        <?= $form ?>
    </div>
    <div class="clear"></div>
    <div class="content"><?= cms::customContent('support') ?></div>
</div>
