<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="BrandsBlock.php">
 * </description>
 * Виджет отображает карусель логотипов брендов на главной
 * $brands - массив моделей Brands
 * = Array of
 * Array(
 * [id] => 373
 * [name] => Fossil
 * [enabled] => 1
 * [img_src] => brand_logo_27.jpg
 * [vid] => 46470
 * [query] => Fossil
 * [url] => fossil
 * )
 **********************************************************************************************************************/
?>
<? if ($this->dataProvider && is_array($this->dataProvider)) {
        Yii::app()->clientScript->registerCssFile($this->frontThemePath . '/css/magicscroll.css', 'screen');
        Yii::app()->clientScript->registerScriptFile(
          $this->frontThemePath . '/js/' . (YII_DEBUG ? 'magicscroll.js' : 'magicscroll.js'),
          CClientScript::POS_BEGIN
        );
        ?>
    <script type="text/javascript">
        MagicScroll.extraOptions.<?=$this->id?> = {
            <?=$this->magicScrollExtraOptions ?>
        };
    </script>

        <div class="MagicScroll msborder" id="<?=$this->id?>">
            <? foreach ($this->dataProvider as $data) { ?>
                <? $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.widgets.SearchItemsList.SearchItem',
                  array('data'=>$data,
                        'showControl'=>$this->showControl,'disableItemForSeo'=>$this->disableItemForSeo,'imageFormat'=>$this->imageFormat,
                        'controlAddToFavorites'=>$this->controlAddToFavorites,'controlAddToFeatured'=>$this->controlAddToFeatured,
                        'controlDeleteFromFavorites'=>$this->controlDeleteFromFavorites,'lazyLoad'=>false),
                  false);
                ?>
             <? } ?>
        </div>
<? } ?>