<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="BrandsBlock.php">
 * </description>
 * Виджет отображает карусель логотипов брендов на главной
 * $brands - массив моделей Brands
 * = Array of
 * Array(
 * [id] => 373
 * [name] => Fossil
 * [enabled] => 1
 * [img_src] => brand_logo_27.jpg
 * [vid] => 46470
 * [query] => Fossil
 * [url] => fossil
 * )
 **********************************************************************************************************************/
?>
<? if ($brands) {
    if ($this->beginCache(
      'brandsBlock',
      array(
        'duration' => (YII_DEBUG ||
          (Yii::app()->user->inRole(array('contentManager', 'superAdmin')))
          ) ? 5 : 1200
      )
    )
    ) {
       
        Yii::app()->clientScript->registerCssFile($this->frontThemePath . '/css/magicscroll.css', 'screen');
        Yii::app()->clientScript->registerScriptFile(
          $this->frontThemePath . '/js/' . (YII_DEBUG ? 'magicscroll.js' : 'magicscroll.js'),
          CClientScript::POS_BEGIN
        );
        ?>
        <h4 class="page-title black"><?= Yii::t('main', 'Бренды') ?></h4>
        <script async type="text/javascript">
            MagicScroll.extraOptions.brandsScroll = {
                'items': 8,
                'width': 960,
                'speed': 2000,
                'height': 100,
                'arrows': 'outside',
                'arrows-opacity': 20,
                'arrows-hover-opacity': 100,
                'step': 4
            };
        </script>
        <div class="MagicScroll msborder" id="brandsScroll">
            <? foreach ($brands as $brand) {
                if (!$brand->img_src) continue;
                $imgSrc=Yii::app()->request->baseUrl.'/images/brands/'.$brand->img_src;

            if (DSConfig::getVal('seo_img_cache_enabled') == 1) {
                $imgsubdomains = explode(',', DSConfig::getVal('seo_img_cache_subdomains'));
                $domain = Yii::app()->getBaseUrl(true);
                if (is_array($imgsubdomains) && (count($imgsubdomains) > 0)) {
                    $n = $brand->id % count($imgsubdomains);
                    $s = $imgsubdomains[$n];
                    $subdomain = preg_replace('/(http[s]*:\/\/)/i', '$1' . $s . '.', $domain);
                } else {
                    $subdomain = $domain . '.';
                }
                $imgSrc = $subdomain . $imgSrc;
            }
                ?>
                <a href="<?= Yii::app()->createUrl('/brand/index', array('name' => $brand->url)) ?>">
                    <img src="<?= $imgSrc; ?>"
                         alt="<?= CHtml::encode($brand->name); ?>"/>
                    <?= $brand->name ?></a>
            <? } ?>
        </div>
        <a href="<?= Yii::app()->createUrl('/brand/list') ?>"><?= Yii::t('main', 'Все бренды') ?></a>
        <? $this->endCache();
    }
}