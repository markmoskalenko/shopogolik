<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 * Рендеринг списка товаров в поисковой выдаче посредством CListView
 *
 * var $showControl - показывать кнопки
 * var $disableItemForSeo
 * var $imageFormat
 * var $controlAddToFavorites - кнопка Добавить в избранное
 * var $controlAddToFeatured - кнопка Добавить в рекомендованное
 * var $controlDeleteFromFavorites - кнопка Удалить (из избранного, в частности)
 * var $lazyLoad
 **********************************************************************************************************************/
?>
<div class="product" id="item<?= $data['num_iid'] ?>">
    <? if ($showControl) { ?>
        <div class="control">
            <? if ($controlAddToFeatured) { ?>
                <a class="ui-icon ui-icon-heart" style="display:inline-block; cursor: pointer;"
                   title="<?= Yii::t('admin', 'Добавить в рекомендованное') ?>"
                   href="<?= Yii::app()->createUrl('/admin/featured/add/', array('id' => $data['num_iid'])) ?>"
                   onclick="addFeatured(this,<?= $data['num_iid'] ?>); return false;"></a>
            <? } ?>
            <? if ($controlAddToFavorites) { ?>
                <a class="ui-icon ui-icon-check" style="display:inline-block; cursor: pointer;"
                   title="<?= Yii::t('admin', 'Добавить в избранное') ?>"
                   href="<?= Yii::app()->createUrl('/cabinet/favorite/add', array('iid' => $data['num_iid'])) ?>"
                   onclick="addFavorite(this,<?= $data['num_iid'] ?>); return false;"></a>
            <? } ?>
            <? if ($controlDeleteFromFavorites) { ?>
                <a class="ui-icon ui-icon-closethick" style="display:inline-block; cursor: pointer;"
                   title="<?= Yii::t('admin', 'Удалить из избранного') ?>"
                   href="<?= Yii::app()->createUrl('/cabinet/favorite/delete', array('id' => $data['id'])) ?>"
                   onclick="deleteFavorite(this,<?= $data['num_iid'] ?>); return false;"></a>
            <? } ?>
            <a class="ui-icon ui-icon-cart" style="display:inline-block; cursor: pointer;"
               title="<?= Yii::t('main', 'Положить в корзину') ?>"
               href="<?= Yii::app()->createUrl('/cart/add', array('iid' => $data['num_iid'])) ?>"
               onclick="addCart(this,<?= $data['num_iid'] ?>); return false;"></a>
        </div>
    <? } ?>
    <div class="product-image">
        <!--<img class="lazy" src="img/grey.gif" data-original="img/example.jpg" width="640" height="480">-->
        <a <? if ($disableItemForSeo) {
            echo 'rel="nofollow"';
        } ?> href="<?= Yii::app()->createUrl('/item/index', array('iid' => $data['num_iid'])) ?>">
            <? if ($lazyLoad) { ?>
                <img class="lazy"
                     src="<?= Yii::app()->request->baseUrl ?>/themes/<?=
                     Yii::app()->theme->name ?>/images/zoomloader.gif"
                     data-original="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt=""
                     title=""/>
                <!--onload="this.width=160;this.height=160;" onmouseout="this.width=160;this.height=160;" onmouseover="this.width=195;this.height=195;"-->
                <noscript><img src="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt=""/></noscript>
            <?
            } else {
                ?>
                <img src="<?= Img::getImagePath($data['pic_url'], $imageFormat) ?>" alt="" title=""/>
            <? } ?>
        </a>
    </div>
    <div class="product-price">
        <?
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'       => $data['price'],
            'count'       => 1,
            'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
            'postageId'   => false,
            'sellerNick'  => false,
          )
        );
        $userPrice = $resUserPrice->price;
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'       => $data['promotion_price'],
            'count'       => 1,
            'deliveryFee' => ($data['express_fee']) ? $data['express_fee'] : 0,
            'postageId'   => false,
            'sellerNick'  => false,
          )
        );
        $userPromotionPrice = $resUserPrice->price;
        if ($data['price'] > $data['promotion_price']) {
            ?>
            <?= Formulas::priceWrapper($userPromotionPrice) ?>
            <div class="product-promotion">&nbsp;-&nbsp;<?= $userPrice - $userPromotionPrice; ?></div>
        <?
        } else {
            ?>
            <?= Formulas::priceWrapper($userPromotionPrice) ?>
        <? } ?>
    </div>

    <? if (isset($data['seller_rate']) && ($data['seller_rate'] > 0)) {
        ?>
        <div class="product-seller-rate">
            <span class="rating"><i class="i-rating rating_<?= DSGSeller::getCrownsFromSales($data['seller_rate'])-1?>"></i></span>
            <?//= $item->seller_rate ?>
        </div>
    <?
    } elseif (isset($data['tmall']) && $data['tmall']) { ?>
        <div class="product-seller-rate">
            <span class="rating"><i class="i-rating rating_21"></i></span>
            <?//= $item->seller_rate ?>
        </div>
    <?}?>
</div>