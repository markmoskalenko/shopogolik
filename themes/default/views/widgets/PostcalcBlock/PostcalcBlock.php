<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="PostcalcBlock.php">
 * </description>
 * Виджет отображает postcalc ;-)
 **********************************************************************************************************************/
?>
<iframe name="postcalc-light" id="postcalc-light" src="/tools/postcalc" width="100%" height="215px" scrolling="no"
        frameborder="0">

    <?= Yii::t('main', 'Ваш браузер не поддерживает фрэймы...') ?></iframe>

