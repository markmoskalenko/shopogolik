<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="FavoritesMenuBlock.php">
 * </description>
 * Виджет, реализующий раздел "Избранное" в менню категорий
 *
 * var $favoriteMenu = - собственно, массив масивов категорий главного меню.
 * array
 * (
 * 0 => array
 * (
 * 'pkid' => '2'
 * 'cid' => '0'
 * 'parent' => '1'
 * 'status' => '1'
 * 'url' => 'mainmenu-odezhda'
 * 'query' => '女装男装'
 * 'level' => '2'
 * 'order_in_level' => '200'
 * 'view_text' => 'Одежда'
 * 'children' => array
 * (
 * 3 => array(...)
 * 18 => array(...)
 * 31 => array(...)
 * 41 => array(...)
 * 49 => array(...)
 * 60 => array(...)
 * 70 => array(...)
 * 81 => array(...)
 * )
 * )
 * )
 * var $adminMode = false - рендерить ли в режиме Админки
 **********************************************************************************************************************/
?>
<div class="block" id="categories-block">
    <div class="block-content categories">
        <? $type = 'favorite';?>
        <ul>
            <? if (isset($favoriteMenu)) {
                foreach ($favoriteMenu as $id => $menu) {
                    $link = Yii::app()->createUrl('/' . $type . '/'.$menu['cid']);
                    ?>
                    <li class="parent">
                        <? if (($menu['cid'] == 0) && ($menu['query'] == '')) { ?>
                            <a
                              href="javascript:void(0);"><?= $menu['view_text'] ?></a>
                        <?
                        } else {
                            ?>
                            <a
                              href="<?= $link ?>">
                                <?= $menu['view_text'] ?>
                            </a>
                        <? } ?>
                        <? if (isset($menu['children']) && count($menu['children']) > 0) { ?>
                            <ul>
                                    <li>
                                        <table class="categories-table">
                                            <? if (isset($menu['children']) && count($menu['children']) > 0) {
                                                foreach ($menu['children'] as $id2 => $items2) {
                                                    if (isset($items2) && isset($items2['url'])) { ?>
                                                        <tr>
                                                            <td class="a2">
                                                                <? if (($items2['cid'] == 0) && ($items2['query'] == '')) { ?>
                                                                    <a class="a2"
                                                                       href="javascript:void(0);"><?= $items2['view_text'] ?></a>
                                                                <?
                                                                } else {
                                                                    ?>
                                                                    <a class="a2" href="<?=Yii::app()->createUrl('/' . $type . '/index',array('name' => $items2['url'])) ?>">
                                                                        <?= $items2['view_text'] ?>
                                                                    </a>
                                                                <? } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>

                                                                <? if (isset($menu['children']) && count(
                                                                    $menu['children']
                                                                  ) > 0
                                                                )
                                                                    //  foreach($menu['children'] as $id2=>$items2) {
                                                                    ?>
                                                                <?
                                                                if (isset($items2['children']) && count(
                                                                    $menu['children']
                                                                  ) > 0
                                                                ) {
                                                                    foreach ($items2['children'] as $id3 => $items3) { ?>

                                                                        <div class="a1" title="<? if (mb_strlen(
                                                                            $items3['view_text'],
                                                                            'UTF-8'
                                                                          ) > 20
                                                                        ) {
                                                                            echo $items3['view_text'];
                                                                        } ?>">
                                                                            <a href="<?=Yii::app()->createUrl('/' . $type . '/index',array('name' => $items3['url']))?>">
                                                                                <?= $items3['view_text'] ?>
                                                                            </a>&nbsp;
                                                                        </div>

                                                                    <?
                                                                    }
                                                                } ?>
                                                                <? // } ?>

                                                            </td>
                                                        </tr>
                                                    <? } ?>
                                                <?
                                                }
                                            } ?>
                                        </table>
                                    </li>
                            </ul>
                            <? }?>
                        <? } ?>
                    </li>
                <? } ?>
                <li class="parent"><a href="<?= Yii::app()->createUrl('/' . $type . '/other') ?>">
                        <?= Yii::t('main', 'Прочее...') ?>
                    </a></li>
        </ul>
    </div>
</div>