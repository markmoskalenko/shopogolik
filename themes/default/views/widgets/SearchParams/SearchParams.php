<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchParams.php">
 * </description>
 * Рендеринг параметров поиска
 *
 * var $type = 'category'|'search' - тип рендеринга
 * var $cids = array() - список рекомендованных категорий
 * var $groups = array - список групп уточняющего поиска
 * (
 * 0 => stdClass#1
 * (
 * [title] => '<translation editable=\"1\" translated=\"0\" url=\"/site/translate\" type=\"parseFilter\" from=\"zh-CHS\" to=\"ru\" uid=\"0\" id=\"plain0\" title=\"plain[0]: 电饭煲多功能\" >电饭煲多功能<translate  onclick=\"editTranslation(event,this.parentNode,\'plain\',\'0\'); stopPropagation(event); return false;\" ><span class=\"ui-icon ui-icon-pencil\"><span></translate></translation>'
 * [values] => array
 * (
 * 0 => stdClass(...)
 * ...
 * 7 => stdClass(...)
 * )))
 * var $filters = array() - список фильтров
 * var $multiFilters = array - список мультифильтров
 * (0 => stdClass#1
 * ([title] => '<translation editable=\"1\" translated=\"0\" url=\"/site/translate\" type=\"parseMultiFilter\" from=\"zh-CHS\" to=\"ru\" uid=\"0\" id=\"plain0\" title=\"plain[0]: 形状\" >形状<translate  onclick=\"editTranslation(event,this.parentNode,\'plain\',\'0\'); stopPropagation(event); return false;\" ><span class=\"ui-icon ui-icon-pencil\"><span></translate></translation>'
 * [values] => array
 * (0 => stdClass(...)
 * 1 => stdClass(...)
 * )))
 * var $suggestions = array() - список синонимов поиска, типа "так же рекомендуем посмотреть..."
 * var $priceRange = array - массив значений для построения графика цен
 * (0 => stdClass#1
 * ([start] => '0'
 * [end] => '20'
 * [percent] => '0'
 * ))
 *
 * var $params = параметры, переданные в запросе
 * array
 * ('name' => 'mainmenu-kuxonnye-elektropribory-elektricheskie-risovarki')
 * var $cur_props = array()
 **********************************************************************************************************************/
?>
<?  if ($this->beginCache(
  'SearchParams-' . implode('-', $params),
  array(
    'duration' => (YII_DEBUG ||
      (Yii::app()->user->inRole(array('contentManager', 'superAdmin')))
      ) ? 5 : 1200
  )
)
) {
    ?>
    <div class="block" id="search-terms">
    <div class="block-content">
    <form id="search-params" action="<?= Yii::app()->createUrl('/' . $type . '/index', $params) ?>" method="post">
    <div class="form-item">
        <? $this->render('themeBlocks.SearchParams.SearchParamsPriceGraph', array('priceRange' => $priceRange,)); ?>
        <label for="price-min"><?= Yii::t('main', 'Цена (без доставки)') ?>:</label>
        <span><?= Yii::t('main', 'от') ?></span>
        <input type="text" size="4" class="small_input" name="price_min" id="price-min"
               value="<?= (isset($params['price_min'])) ? CHtml::encode($params['price_min']) : 0 ?>"/>
        <span><?= Yii::t('main', 'до') ?></span>
        <input type="text" size="4" class="small_input" name="price_max"
               value="<?= (isset($params['price_max'])) ? CHtml::encode($params['price_max']) : '' ?>"/>
        <span><?= DSConfig::getCurrency(true) ?></span>
    </div>
    <!-- filling group filters -->
    <? if (is_array($groups) and (count($groups) > 0)) { ?>
        <h4 class="title"><?= Yii::t('main', 'Группы товаров') ?></h4>
        <? foreach ($groups as $prop) {
            if (isset($prop->values[0])) {
                $pidvid = explode(':', str_replace('%3A', ':', $prop->values[0]->values_props));
                $pid = $pidvid[0];
                ?>
                <div class="form-item param">
                    <label class="header"><?= $prop->title ?></label>

                    <div class="param-childs<? if (!isset($cur_props[$pid])) {
                        echo ' hidden';
                    } ?>">
                        <? if (isset($cur_props[$pid])) { ?>
                            <label>
                                <input type="radio" name="props[<?= $pid ?>]" value=""/>
                                <cite><?= Yii::t('main', 'Сбросить значения') ?></cite>
                            </label>
                        <? } ?>
                        <? for ($i = 0; $i < 4; $i++) { ?>
                            <? if (isset($prop->values[$i])) {
                                $pidvid = explode(':', str_replace('%3A', ':', $prop->values[$i]->values_props));
                                $pid = $pidvid[0];
                                $vid = $pidvid[1];
                                ?>
                                <label>
                                    <? if (isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                        $ch = ' checked';
                                    } else {
                                        $ch = '';
                                    }?>
                                    <input type="radio" name="props[<?= $pid ?>]" value="<?= $vid ?>"<?= $ch ?> />
                                    <?= $prop->values[$i]->values_title ?>
                                    <? if (isset($prop->values[$i]->values_count) && ($prop->values[$i]->values_count > 0)) { ?>
                                        (<?= $prop->values[$i]->values_count ?>)
                                    <? } ?>
                                </label>
                            <? } ?>
                        <? } ?>
                        <? if (count($prop->values) > 4) { ?>
                            <a href="javascript: showMore(<?= $pid ?>);"><?= Yii::t('main', 'Показать больше') ?></a>
                        <? } ?>
                    </div>
                    <? if (count($prop->values) > 4) { ?>
                        <div class="all-param-childs" id="param-<?= $pid ?>">
                            <div class="param-header">
                                <?= $prop->title ?>
                                <a href="javascript: closeMore(<?= $pid ?>)" class="closer"></a>
                            </div>
                            <? for ($j = $i; $j < count($prop->values); $j++) { ?>
                                <? if (isset($prop->values[$j])) {
                                    $pidvid = explode(':', str_replace('%3A', ':', $prop->values[$j]->values_props));
                                    $pid = $pidvid[0];
                                    $vid = $pidvid[1];
                                    ?>
                                    <label>
                                        <? if (isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                            $ch = ' checked';
                                        } else {
                                            $ch = '';
                                        }?>
                                        <input type="radio" name="props[<?= $pid ?>]" value="<?= $vid ?>"<?= $ch ?> />
                                        <?= $prop->values[$j]->values_title ?>
                                        <? if (isset($prop->values[$j]->values_count) && ($prop->values[$j]->values_count > 0)) { ?>
                                            (<?= $prop->values[$j]->values_count ?>)
                                        <? } ?>
                                    </label>
                                <? } ?>
                            <? } ?>
                        </div>
                    <? } ?>
                </div>
            <?
            }
        }
    }?>
    <!-- filling filters -->
    <? if (is_array($filters) and (count($filters) > 0)) { ?>
        <h4 class="title"><?= Yii::t('main', 'Фильтры по свойствам') ?></h4>
        <?  foreach ($filters as $prop) {
            if (isset($prop->values[0])) {
                $pidvid = explode(':', str_replace('%3A', ':', $prop->values[0]->values_props));
                $pid = $pidvid[0];
                ?>
                <div class="form-item param">
                    <label class="header"><?= $prop->title ?></label>

                    <div class="param-childs<? if (!isset($cur_props[$pid])) {
                        echo ' hidden';
                    } ?>">
                        <? if (isset($cur_props[$pid])) { ?>
                            <label>
                                <input type="radio" name="props[<?= $pid ?>]" value=""/>
                                <cite><?= Yii::t('main', 'Сбросить значения') ?></cite>
                            </label>
                        <? } ?>
                        <? for ($i = 0; $i < 4; $i++) { ?>
                            <? if (isset($prop->values[$i])) {
                                $pidvid = explode(':', str_replace('%3A', ':', $prop->values[$i]->values_props));
                                $pid = $pidvid[0];
                                $vid = $pidvid[1];
                                ?>
                                <label>
                                    <? if (isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                        $ch = ' checked';
                                    } else {
                                        $ch = '';
                                    }?>
                                    <input type="radio" name="props[<?= $pid ?>]" value="<?= $vid ?>"<?= $ch ?> />
                                    <?= $prop->values[$i]->values_title ?>
                                    <? if (isset($prop->values[$i]->values_count) && ($prop->values[$i]->values_count > 0)) { ?>
                                        (<?= $prop->values[$i]->values_count ?>)
                                    <? } ?>
                                </label>
                            <? } ?>
                        <? } ?>
                        <? if (count($prop->values) > 4) { ?>
                            <a href="javascript: showMore(<?= $pid ?>);"><?= Yii::t('main', 'Показать больше') ?></a>
                        <? } ?>
                    </div>
                    <? if (count($prop->values) > 4) { ?>
                        <div class="all-param-childs" id="param-<?= $pid ?>">
                            <div class="param-header">
                                <?= $prop->title ?>
                                <a href="javascript: closeMore(<?= $pid ?>)" class="closer"></a>
                            </div>
                            <? for ($j = $i; $j < count($prop->values); $j++) { ?>
                                <? if (isset($prop->values[$j])) {
                                    $pidvid = explode(':', str_replace('%3A', ':', $prop->values[$j]->values_props));
                                    $pid = $pidvid[0];
                                    $vid = $pidvid[1];
                                    ?>
                                    <label>
                                        <? if (isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                            $ch = ' checked';
                                        } else {
                                            $ch = '';
                                        }?>
                                        <input type="radio" name="props[<?= $pid ?>]" value="<?= $vid ?>"<?= $ch ?> />
                                        <?= $prop->values[$j]->values_title ?>
                                        <? if (isset($prop->values[$j]->values_count) && ($prop->values[$j]->values_count > 0)) { ?>
                                            (<?= $prop->values[$j]->values_count ?>)
                                        <? } ?>
                                    </label>
                                <? } ?>
                            <? } ?>
                        </div>
                    <? } ?>
                </div>
            <?
            }
        }
    }?>
    <!-- filling multiFilters -->
    <? if (is_array($multiFilters) and (count($multiFilters) > 0)) { ?>
        <? foreach ($multiFilters as $prop) {
            $pidvid = explode(':', str_replace('%3A', ':', $prop->values[0]->values_props));
            $pid = $pidvid[0];
            ?>
            <div class="form-item param">
                <label class="header"><?= $prop->title ?></label>

                <div class="param-childs<? if (!isset($cur_props[$pid])) {
                    echo ' hidden';
                } ?>">
                    <? if (isset($cur_props[$pid])) { ?>
                        <label>
                            <input type="radio" name="props[<?= $pid ?>]" value=""/>
                            <cite><?= Yii::t('main', 'Сбросить значения') ?></cite>
                        </label>
                    <? } ?>
                    <? for ($i = 0; $i < 4; $i++) { ?>
                        <? if (isset($prop->values[$i])) {
                            $pidvid = explode(':', str_replace('%3A', ':', $prop->values[$i]->values_props));
                            $pid = $pidvid[0];
                            $vid = $pidvid[1];
                            ?>
                            <label>
                                <? if (isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                    $ch = ' checked';
                                } else {
                                    $ch = '';
                                }?>
                                <input type="radio" name="props[<?= $pid ?>]" value="<?= $vid ?>"<?= $ch ?> />
                                <?= $prop->values[$i]->values_title ?>
                                <? if (isset($prop->values[$i]->values_count) && ($prop->values[$i]->values_count > 0)) { ?>
                                    (<?= $prop->values[$i]->values_count ?>)
                                <? } ?>
                            </label>
                        <? } ?>
                    <? } ?>
                    <? if (count($prop->values) > 4) { ?>
                        <a href="javascript: showMore(<?= $pid ?>);"><?= Yii::t('main', 'Показать больше') ?></a>
                    <? } ?>
                </div>
                <? if (count($prop->values) > 4) { ?>
                    <div class="all-param-childs" id="param-<?= $pid ?>">
                        <div class="param-header">
                            <?= $prop->title ?>
                            <a href="javascript: closeMore(<?= $pid ?>)" class="closer"></a>
                        </div>
                        <? for ($j = $i; $j < count($prop->values); $j++) { ?>
                            <? if (isset($prop->values[$j])) {
                                $pidvid = explode(':', str_replace('%3A', ':', $prop->values[$j]->values_props));
                                $pid = $pidvid[0];
                                $vid = $pidvid[1];
                                ?>
                                <label>
                                    <? if (isset($cur_props[$pid]) && $cur_props[$pid] == $vid) {
                                        $ch = ' checked';
                                    } else {
                                        $ch = '';
                                    }?>
                                    <input type="radio" name="props[<?= $pid ?>]" value="<?= $vid ?>"<?= $ch ?> />
                                    <?= $prop->values[$j]->values_title ?>
                                    <? if (isset($prop->values[$j]->values_count) && ($prop->values[$j]->values_count > 0)) { ?>
                                        (<?= $prop->values[$j]->values_count ?>)
                                    <? } ?>
                                </label>
                            <? } ?>
                        <? } ?>
                    </div>
                <? } ?>
            </div>
        <?
        }
    }?>
    <div class="form-item submit">
        <a href="javascript:void(0);" onclick="$('#search-params').submit(); return false;" class="search-terms-sbmt">
            <?= Yii::t('main', 'Поиск') ?>
        </a>
    </div>
    </form>
    </div>

    </div>

    <!-- search categories -->
    <? if ($cids) { ?>
        <div class="block" id="categories-block">

            <h4 class="title"><?= Yii::t('main', 'Результаты по категориям') ?></h4>

            <div class="block-content categories">
                <ul id="search-categories">
                    <? if ($type == 'search' || $type == 'brand') {
                        foreach ($cids as $cat) {
                            ?>
                            <? $params['cid'] = $cat->cid; ?>
                            <li><a rel="nofollow" href="<?= Yii::app()->createUrl('/' . $type . '/index', $params) ?>">
                                    <?= $cat->title ?>
                                    <? if ($cat->count > 0) { ?>
                                        <span>(<?= $cat->count; ?>)</span>
                                    <? } ?>
                                </a></li>
                        <? } ?>
                    <?
                    } else {
                        foreach ($cids as $cat) {
                            if (!isset($cat->url)) {
                                continue;
                            }
                            ?>
                            <li><a rel="nofollow"
                                   href="<?= Yii::app()->createUrl('/category/index', array('name' => $cat->url)) ?>">
                                    <?= $cat->title ?>
                                    <? if ($cat->count > 0) { ?>
                                        <span>(<?= $cat->count; ?>)</span>
                                    <? } ?>
                                </a></li>
                        <?
                        }
                    }
                    ?>

                </ul>
            </div>

        </div>
    <? } ?>
    <!-- search suggestions -->
    <? if ($suggestions) { ?>
        <div class="block" id="categories-block">

            <h4 class="title"><?= Yii::t('main', 'Похожие результаты') ?></h4>

            <div class="block-content categories">
                <ul id="search-categories">
                    <? if ($type == 'search' || $type == 'brand') {
                        foreach ($suggestions as $sugg) {
                            ?>
                            <? $paramsSugg = array();
                            $paramsSugg['cid'] = $sugg->cid;
                            $paramsSugg['query'] = $sugg->q;
                            ?>
                            <li><a rel="nofollow"
                                   href="<?= Yii::app()->createUrl('/' . $type . '/index', $paramsSugg) ?>">
                                    <?= $sugg->title ?>
                                </a></li>
                        <? } ?>
                    <?
                    } else {
                        foreach ($suggestions as $sugg) {
                            ?>
                            <? $paramsSugg = array();
                            $paramsSugg['cid'] = $sugg->cid;
                            $paramsSugg['query'] = $sugg->q;
                            ?>
                            <li><a rel="nofollow"
                                   href="<?= Yii::app()->createUrl('/' . 'search' . '/index', $paramsSugg) ?>">
                                    <?= $sugg->title ?>
                                </a></li>
                        <? } ?>
                    <? } ?>
                </ul>
            </div>
        </div>
    <? } ?>
    <script type="text/javascript">
        function showMore(pid) {
            $("#param-" + pid).toggle();
        }
        function closeMore(pid) {
            $("#param-" + pid).hide();
        }
        $(document).ready(function () {
            $("label.header").each(function () {
                $(this).click(function () {
                    $(this).parent().find('div.param-childs').slideToggle('slow');
                });
            });
        });
    </script>
    <? $this->endCache();
} ?>