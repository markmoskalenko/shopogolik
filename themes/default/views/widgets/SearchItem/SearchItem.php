<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItem.php">
 * </description>
 * Виджет, реализующий отображение товара в поисковой выдаче
 * var $item = stdClass#1
 * (
 * [num_iid] => '38289599003' - item id
 * [price] => 115 - цена в юанях
 * [promotion_price] => 115  - цена в юанях, со скидкой
 * [pic_url] => 'http://g.search.alicdn.com/img/bao/uploaded/i4/i2/T1GTIAFutbXXXXXXXX_!!0-item_pic.jpg_220x220.jpg'
 * [nick] => 'suelly2013' - ник продавца
 * [seller_rate] => 0 - рейтинг продавца
 * [post_fee] => 0 - стоимость доставки по Китаю в юанях, обычной почтой
 * [express_fee] => 0 - стоимость доставки по Китаю в юанях, экспресс
 * [ems_fee] => 0 - стоимость доставки по Китаю в юанях, EMS
 * [postage_id] => 0
 * [cid] => 0
 * [tmall] => false - товар с Tmall
 * [html_title] => 'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
 * [popup_title] => 'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
 * [html_alt] => 'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
 * [promotion_percent] => 0 - процент скидки
 * [userPrice] => 267 - конечная цена в текущей валюте
 * [userPromotionPrice] => 267 - конечная цена в текущей валюте, со скидкой
 * )
 * var $newLine = ' first' - разница в рендеринге для виджета товара, начинающего новую строку в выдаче
 * var $showControl = true - отображать ли всякие "Добавить в избранное", "Добавить в рекомендованное" и т.п.
 * var $disableItemForSeo =  true - запрещать индексировать карту товара
 * var $imageFormat = '_160x160.jpg'
 * var $lazyLoad = true - ленивая загрузка изображений
 **********************************************************************************************************************/
?>
<div class="product<?= $newLine ?>">
    <div class="control">
        <? if (isset($item->extUrlSame->count) && ($item->extUrlSame->count>0)) { ?>
        <a class="ui-icon ui-icon-extlink" style="display:inline-block; cursor: pointer;"
           title="<?= Yii::t('admin', 'У других продавцов') ?>"
           rel="nofollow" href="<?= $item->extUrlSame->url?>" target="_blank"</a>
        <? } ?>
        <? if (isset($item->extUrlSimilar->count) && ($item->extUrlSimilar->count>0)) { ?>
            <a class="ui-icon ui-icon-newwin" style="display:inline-block; cursor: pointer;"
            title="<?= Yii::t('admin', 'Похожие товары') ?>"
            rel="nofollow" href="<?= $item->extUrlSimilar->url?>" target="_blank"</a>
        <? } ?>
        <? if (isset($item->extUrlUser->count) && ($item->extUrlUser->count>0)) { ?>
            <a class="ui-icon ui-icon-person" style="display:inline-block; cursor: pointer;"
               title="<?= Yii::t('admin', 'Все товары продавца') ?>"
            rel="nofollow" href="<?= $item->extUrlUser->url?>" target="_blank"</a>
        <? } ?>
        <? if ($showControl) { ?>
            <a class="ui-icon ui-icon-heart" style="display:inline-block; cursor: pointer;"
               title="<?= Yii::t('admin', 'Добавить в рекомендованное') ?>"
               rel="nofollow" href="<?= Yii::app()->createUrl('/admin/featured/add/', array('id' => $item->num_iid)) ?>"
               onclick="addFeatured(this,<?= $item->num_iid ?>); return false;"></a>
        <? } ?>
        <a class="ui-icon ui-icon-check" style="display:inline-block; cursor: pointer;"
           title="<?= Yii::t('admin', 'Добавить в избранное') ?>"
           rel="nofollow" href="<?= Yii::app()->createUrl('/cabinet/favorite/add', array('iid' => $item->num_iid)) ?>"
           onclick="addFavorite(this,<?= $item->num_iid ?>); return false;"></a>
        <a class="ui-icon ui-icon-cart" style="display:inline-block; cursor: pointer;"
           title="<?= Yii::t('admin', 'Положить в корзину') ?>"
           rel="nofollow" href="<?= Yii::app()->createUrl('/cart/add', array('iid' => $item->num_iid)) ?>"
           onclick="addCart(this,<?= $item->num_iid ?>); return false;"></a>
    </div>
    <div class="product-image">
        <!--<img class="lazy" src="img/grey.gif" data-original="img/example.jpg" width="640" height="480">-->
        <a <? if ($disableItemForSeo) {
            echo 'rel="nofollow"';
        } ?> href="<?= Yii::app()->createUrl('/item/index', array('iid' => $item->num_iid)) ?>" target="_blank">
            <? if ($lazyLoad) { ?>
                <img class="lazy"
                     src="<?= Yii::app()->request->baseUrl ?>/themes/<?=
                     Yii::app()->theme->name ?>/images/zoomloader.gif"
                     data-original="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="<?= $item->html_alt ?>"
                     title="<?= $item->popup_title ?>"/>
                <!--onload="this.width=160;this.height=160;" onmouseout="this.width=160;this.height=160;" onmouseover="this.width=195;this.height=195;"-->
                <noscript><img src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt=""/></noscript>
            <?
            } else {
                ?>
                <img src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
            <? } ?>
        </a>
    </div>
    <div class="product-price">
        <? if ($item->price > $item->promotion_price) { ?>
            <?= Formulas::priceWrapper($item->userPromotionPrice) ?>
            <div class="product-promotion">&nbsp;-&nbsp;<?= $item->userPrice - $item->userPromotionPrice; ?></div>
        <? } else { ?>
            <?= Formulas::priceWrapper($item->userPromotionPrice) ?>
        <? } ?>
    </div>
    <? if (isset($item->seller_rate) && ($item->seller_rate > 0)) {
            ?>
            <div class="product-seller-rate">
                <span class="rating"><i class="i-rating rating_<?= DSGSeller::getCrownsFromSales($item->seller_rate)-1?>"></i></span>
                <?//= $item->seller_rate ?>
            </div>
        <?
    } elseif (isset($item->tmall) && $item->tmall) { ?>
        <div class="product-seller-rate">
            <span class="rating"><i class="i-rating rating_21"></i></span>
            <?//= $item->seller_rate ?>
        </div>
    <?}?>
</div>