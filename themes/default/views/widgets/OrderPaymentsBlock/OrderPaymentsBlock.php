<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="OrderPaymentsBlock.php">
 * </description>
 * Виджет отображает платежи по заказу
 * var $dataProvider = CActiveDataProvider#1
 * (
 * [modelClass] => 'OrdersPayments'
 * )
 * var $blockId = 'order-payments-1916'
 **********************************************************************************************************************/
?>
<div id="accordion-payments-<?= $blockId ?>">
    <h3><?= Yii::t('main', 'Платежи по заказу') ?>: <?= $dataProvider->totalItemCount ?></h3>

    <div class="payments-block">
        <? $this->widget(
          'bootstrap.widgets.TbGridView',
          array(
            'id'           => 'grid-' . $blockId,
            'dataProvider' => $dataProvider,
            'type'         => 'striped bordered condensed',
            'template'     => '{items}{pager}', //{summary}{pager}
            'columns'      => array(
              array(
                'name'        => 'fromName',
                'htmlOptions' => array('style' => 'width:50px;font-size:0.9em;'),
              ),
              array(
                'name' => 'summ',
              ),
              array(
                'name'        => 'date',
                'htmlOptions' => array('style' => 'width:45px;font-size:0.9em;'),
              ),
              array(
                'name' => 'descr',
              ),
            ),
          )
        );
        ?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#accordion-payments-<?=$blockId?>").accordion({
            collapsible: true,
            active: <?=($dataProvider->totalItemCount>1) ? '0' : 'false';?>
            //disabled: true
        });
    });
</script>



