<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Рендеринг страницы Under construction
 **********************************************************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
    <style type="text/css">
        p {
            font-size: 1.5em;
            font-weight: 700;
            text-align: center;
        }

        .red {
            color: red;
            font-size: 2em;
        }
    </style>
</head>

<body>

<div id="page">
    <img style="position: fixed;top: 45%;left: 21%;"
         src="<?= Yii::app()->request->baseUrl ?>/themes/<?= Yii::app()->theme->name ?>/images/under.jpg">
</div>
<div style="width:70%; margin:5% auto;">
    <p class="red">В связи со сложившейся ситуацией, интернет магазин временно прекращает<br> доставку заказов из Китая.
    </p>

    <p>Все оплаченные заказы будут доставлены заказчикам с оплатой за доставку при получении. Мы возобновим работу как
        только это будет возможно.</p>

    <p>Просим прощения за доставленные не удобства!
        С вопросами - обращайтесь.
        C уважением, администрация.</p>
</div>
</body>
</html>