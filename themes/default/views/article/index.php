<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Представление для отображения статических страниц (cms)
 * Нaпример, по ссылке http://<domain.ru>/ru/article/oplata
 *
 * var $article = stdClass#1
 * (
 * [title] => 'Способы оплаты'
 * [description] => 'Способы оплаты'
 * [keywords] => 'Способы оплаты'
 * [content] => HTML-контент страницы
 * )
 **********************************************************************************************************************/
?>
<div class="page-title">
    <?= isset($article->title) ? $article->title : '' ?>
</div>
<div class="content">
    <?= isset($article->content) ? $article->content : '' ?>
</div>