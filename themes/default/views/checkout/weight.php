<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="weight.php">
 * </description>
 * Форма ввода веса лотов заказа
 **********************************************************************************************************************/
?>
<div class="page-title"><h4><?= Yii::t('main', 'Оформление заказа') ?></h4></div>
<div class="content">
    <div class="weight-desc">
        <?= cms::customContent('checkout-weight-desc') ?>
    </div>
</div>
<div class="clear"></div>
<form action="<?= $this->createUrl('/checkout/weight') ?>" method="POST">
    <?php foreach ($cart->cartRecords as $k => $item) { ?>
        <? $this->widget(
          'application.components.widgets.OrderItem',
          array(
            'orderItem'   => $item,
            'readOnly'    => false,
            'allowDelete' => false,
            'imageFormat' => '_200x200.jpg',
          )
        );
        ?>
    <? } ?>
    <input type="hidden" name="step" value="2"/>

    <div class="next-btn">
        <?= CHtml::submitButton(Yii::t('main', 'Далее'), array('class' => 'blue-btn bigger')) ?>
    </div>
</form>
<hr/>
<div class="page-title">
    <?= Yii::t('main', 'Справочник веса товаров по категориям'); ?>
</div>
<div class="cabinet-table">
    <? $this->widget(
      'zii.widgets.grid.CGridView',
      array(
        'id'           => 'weights-grid',
        'dataProvider' => $weights->search(),
        'filter'       => $weights,
        'pager'        => array(
          'header'         => '',
          'firstPageLabel' => '&lt;&lt;',
          'prevPageLabel'  => '&lt;',
          'nextPageLabel'  => '&gt;',
          'lastPageLabel'  => '&gt;&gt;',
        ),
        'template'     => '{summary}{items}{pager}',
        'summaryText'  => Yii::t('main', 'Значения') . ' {start}-{end} ' . Yii::t('main', 'из') . ' {count}',
        'columns'      => array(
          array(
            'name' => 'ru',
          ),
          array(
            'name'   => 'en',
            'filter' => false,
          ),
          array(
            'name'   => 'min_weight',
            'filter' => false,
          ),
          array(
            'name'   => 'max_weight',
            'filter' => false,
          ),
        ),
      )
    );
    ?>
</div>