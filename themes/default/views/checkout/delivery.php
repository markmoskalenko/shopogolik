<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="delivery.php">
 * </description>
 * Выбор службы доставки при оформлении заказа
 **********************************************************************************************************************/
?>
<div class="page-title"><h4><?= Yii::t('main', 'Оформление заказа') ?></div>
<div class="content">
    <h4><?= $this->pageTitle ?></h4>
    <? if (count($delivery) > 0) { ?>
        <form id="delivery" class="delivery" action="<?= Yii::app()->createUrl('/checkout/delivery') ?>" method="post">
            <? foreach ($delivery as $i => $del) { ?>
                <label>
                    <input name="CheckoutForm[delivery]" value="<?= $del->id ?>" type="radio"/>
                    <?= Yii::t('main', $del->name) ?>
                    <? if ($del->summ > 0) {
                        echo ': ' . Formulas::priceWrapper(
                            Formulas::convertCurrency(
                              $del->summ,
                              DSConfig::getVal('site_currency'),
                              DSConfig::getCurrency()
                            )
                          );
                    }?>
                </label>
                <div class="hint">
                    <?= Yii::t('main', $del->description) ?>
                </div>
            <? } ?>
            <div class="clear"></div>
            <div class="next-btn">
                <input type="hidden" name="step" value="3"/>
                <?= CHtml::submitButton(Yii::t('main', 'Далее'), array('class' => 'blue-btn bigger', 'style' => 'margin-right:120px;')) ?>
            </div>
        </form>
    <?
    } else {
        ?>
        <p>
            <b><?=
                Yii::t(
                  'main',
                  'К сожалению, доставка в выбранную Вами страну или регион с указанными Вами параметрами временно не производится.'
                ) ?></b>
        </p>
        <div class="delivery-more">
            <a href="<?= Yii::app()->createUrl('/article/index', array('url' => 'dostavka')) ?>">
                <?= Yii::t('main', 'Подробнее об условиях доставки') ?>
            </a>
        </div>
    <? } ?>
</div>