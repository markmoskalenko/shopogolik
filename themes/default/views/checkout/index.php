<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Начало оформления заказа
 **********************************************************************************************************************/
?>
<div style="display: none;" class="page-title">
    <h4><?= Yii::t('main', 'Оформление заказа') ?><h4>
</div>
<div class="content">
    <?php if (count($addresses)): ?>
        <script>
            $(function () {
                $("#change_address").click(function () {
                    $("input[type=radio]").each(function (el) {
                        if ($(this).attr('checked')) {
                            var val = $(this).attr("value");
                            window.location.assign(val);
                        }
                    });
                    return false;
                });
                $("#href_address").click(function () {
                    var val = $('#href_address_hidden').attr("value");
                    window.location.assign(val);
                });
            });
        </script>

        <div class="page-title"><h4><?= Yii::t('main', 'Список адресов') ?></h4></div>
        <table class="cabinet-table">
            <tr>
                <th></th>
                <th><?= Yii::t('main', 'Адрес') ?></th>
                <th><?= Yii::t('main', 'Получатель') ?></th>
                <th><?= Yii::t('main', 'Телефон') ?></th>
            </tr>
            <?php
            $i = 1;
            ?>
            <?php foreach ($addresses as $address): ?>
                <tr>
                    <td style="padding-left: 25px;">
                        <input type="radio" name="address" <? if ($i == 1) {
                            echo "checked";
                        } ?> value="<?=
                        Yii::app()->createUrl(
                          'checkout/choose_address/',
                          array('id' => $address['id'])
                        ) ?>"/>
                    </td>
                    <td><?= $countries[$address['country']] ?>,&nbsp;<?= $address['index'] ?>
                        ,&nbsp;<?= $address['address'] ?></td>
                    <td><?= $address['lastname'] ?>&nbsp;<?= $address['firstname'] ?>
                        &nbsp;<?= isset($address['patroname']) ? $address['patroname'] : '' ?></td>
                    <td><?= $address['phone'] ?></td>

                </tr>
                <? $i++; ?>
            <? endforeach ?>
        </table>
        <div class="row buttons">
            <input type="hidden" id="href_address_hidden" value="<?= Yii::app()->createUrl('checkout/add_address') ?>"/>
            <input type="submit" id="href_address" style="margin-right: 25px;"
                   value=<?= Yii::t('main', "Hовый адрес") ?> name="Checkout" class="red-btn bigger"/>
            <input type="submit" id="change_address" value=<?= Yii::t('main', "Далее") ?> name="Checkout"
                   class="blue-btn bigger"/>

        </div>
    <? else: ?>
        <h3><?= Yii::t('main', 'Добавить новый адрес') ?></h3>
        <div class="form">
            <?= $form ?>
        </div>
    <?  endif; ?>
</div>