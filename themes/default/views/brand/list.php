<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="list.php">
 * </description>
 * Рендеринг страницы списка брэндов
 * http://<domain.ru>/ru/article/oplata
 * var $brands = array - массив описаний брэндов
 **********************************************************************************************************************/
?>
<div class="page-title"><?= $this->pageTitle ?></div>
<div>
    <? foreach ($brands as $alpha => $models) { ?>
        <a href="#<?= $alpha ?>" style="display:inline-block;"><?= $alpha ?></a>
    <? } ?>
</div>
<div class="content">
    <? $i = 0; ?>
    <? foreach ($brands as $alpha => $models) { ?>
        <? if ($i % 2 == 0) {
            $class = ' first';
        } else {
            $class = '';
        }
        $i++; ?>
        <div class="link-list<?= $class ?>">
            <h4><a name="<?= $alpha ?>"><?= $alpha ?></a></h4>
            <ul>
                <?php foreach ($models as $brand) {
                    if (!$brand->img_src) continue;
                    ?>
                    <li>
                        <a href="<?= $this->createUrl('/brand/index', array('name' => $brand->url)) ?>">
                            <?= CHtml::encode($brand->name); ?><br>
                            <img src="/images/brands/<?= $brand->img_src ?>" alt="<?= CHtml::encode($brand->name); ?>">
                        </a>
                    </li>
                <? } ?>
            </ul>
        </div>
    <? } ?>
</div>
    