<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="main.php">
 * </description>
 * Главная страница сайта (не путать с лэйаутом)
 *
 * var $itemsPopular = true
 * var $itemsRecommended = true
 * var $itemsRecentUser = false
 * var $itemsRecentAll = true
 **********************************************************************************************************************/
?>
    <!--  begin recomended  items -->
    <? if ($itemsRecommended) { ?>
        <div class="page-title"><?= Yii::t('main', 'Рекомендованные товары') ?>:</div>
        <div class="products-list featured">
            <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
            <? $this->widget(
              'application.components.widgets.SearchItemsList',
              array(
                'id'                         => 'recommended-itemslist',
                'controlAddToFavorites'      => true,
                'controlAddToFeatured'       => false,
                'controlDeleteFromFavorites' => false,
                'lazyLoad'                   => true,
                'dataType'                   => 'itemsRecommended',
                'pageSize'                   => 8,
                'disableItemForSeo'          => $seo_disable_items_index,
              )
            );
            ?>
        </div>
    <? } ?>
    <!--  begin recomended  items Gallery -->
<? /* ?>	
<? if ($itemsRecommended) { ?>
    <div class="page-title"><?= Yii::t('main', 'Рекомендованные товары') ?>:</div>
    <div class="products-list featured">
        <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
        <? $this->widget(
          'application.components.widgets.SearchItemsGallery',
          array(
            'id'                         => 'recommendedItemsGallery',
            'controlAddToFavorites'      => true,
            'controlAddToFeatured'       => false,
            'controlDeleteFromFavorites' => false,
            'dataType'                   => 'itemsRecommended',
            'disableItemForSeo'          => $seo_disable_items_index,
            'magicScrollExtraOptions'    => "'items': 4,'width': 720,'speed': 4000,'height': 180,'arrows': 'inside','arrows-opacity': 20,
                                             'arrows-hover-opacity': 100,'step': 4,'item-tag': 'div'",
          )
        );
        ?>
    </div>
<? } ?>
<? */ ?>
    <!--  begin popular items -->
    <? if ($itemsPopular) { ?>
        <div class="page-title"><?= Yii::t('main', 'Популярные товары') ?>:</div>
        <div class="products-list featured">
            <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
            <? $this->widget(
              'application.components.widgets.SearchItemsList',
              array(
                'id'                         => 'popular-itemslist',
                'controlAddToFavorites'      => true,
                'controlAddToFeatured'       => true,
                'controlDeleteFromFavorites' => false,
                'lazyLoad'                   => true,
                'dataType'                   => 'itemsPopular',
                'pageSize'                   => 8,
                'disableItemForSeo'          => $seo_disable_items_index,
              )
            );
            ?>
        </div>
    <? } ?>
    <!--  begin my items -->
    <? if ($itemsRecentUser) { ?>
        <div class="page-title"><?= Yii::t('main', 'Недавно просмотренные Вами') ?>:</div>
        <hr/>
        <div class="products-list featured">
            <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
            <? $this->widget(
              'application.components.widgets.SearchItemsList',
              array(
                'id'                         => 'recentUser-itemslist',
                'controlAddToFavorites'      => true,
                'controlAddToFeatured'       => true,
                'controlDeleteFromFavorites' => false,
                'lazyLoad'                   => true,
                'dataType'                   => 'itemsRecentUser',
                'pageSize'                   => 8,
                'disableItemForSeo'          => $seo_disable_items_index,
              )
            );
            ?>
        </div>
    <? } ?>
    <!--  begin all user items -->
    <? if ($itemsRecentAll) { ?>
        <div class="page-title"><?= Yii::t('main', 'Недавно просмотренные другими') ?>:</div>
        <div class="products-list featured">
            <? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1; ?>
            <? $this->widget(
              'application.components.widgets.SearchItemsList',
              array(
                'id'                         => 'recentAll-itemslist',
                'controlAddToFavorites'      => true,
                'controlAddToFeatured'       => true,
                'controlDeleteFromFavorites' => false,
                'lazyLoad'                   => true,
                'dataType'                   => 'itemsRecentAll',
                'pageSize'                   => 8,
                'disableItemForSeo'          => $seo_disable_items_index,
              )
            );
            ?>
        </div>
    <? } ?>