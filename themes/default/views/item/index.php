<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Рендеринг карты товара
 **********************************************************************************************************************/
?>
<?
Yii::app()->clientScript->registerCssFile($this->frontThemePath . '/css/magiczoomplus.css', 'screen');
Yii::app()->clientScript->registerCssFile($this->frontThemePath . '/css/magicscroll.css', 'screen');
Yii::app()->clientScript->registerScriptFile($this->frontThemePath . '/js/magicscroll.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile($this->frontThemePath . '/js/magiczoomplus.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(
  $this->frontThemePath . '/js/item.js?v=' . Search::$intversion,
  CClientScript::POS_HEAD
);
Yii::app()->clientScript->registerScriptFile(
  $this->frontThemePath . '/js/ui/' . (YII_DEBUG ? 'jquery.ui.selectable.js' : 'minified/jquery.ui.selectable.min.js'),
  CClientScript::POS_BEGIN
);
?>
<? $seo_disable_items_index = DSConfig::getVal('seo_disable_items_index') == 1;
$meta = Item::getSEOTags($item->top_item);
$this->pageTitle = $meta->title;
$this->meta_desc = $meta->description;
$this->meta_keyword = $meta->keywords;
?>

    <div class="item-title-container">
        <h3 class="item-title">
            <?= $item->top_item->title ?>
        </h3>
    </div>
    <div class="item-cat-path">
        <strong><?= Yii::t('main', 'Товар из категории') ?>:</strong>
        <?= $item->top_item->cat_path ?>
    </div>
    <div class="item-l">   <!-- item Cart left col-->
        <? Yii::app()->controller->renderPartial('item_images', array('item' => $item)); ?>
        <? Yii::app()->controller->renderPartial('seller_info', array('item' => $item, 'seller' => $seller)); ?>
    </div>
    <div class="item-r"> <!-- item cart right col -->
        <div class="item-params">
            <div class="form-item">
                <? Yii::app()->controller->renderPartial('item_price', array('item' => $item,)); ?>
            </div>
            <? Yii::app()->controller->renderPartial(
              'item_form',
              array('item' => $item, 'ajax' => $ajax, 'input_props' => $input_props)
            ); ?>
        </div>
    </div>

    <div class="clear"></div>
<? Yii::app()->controller->renderPartial('item_info', array('item' => $item)); ?>

<? if (isset($itemRelated->items)) {
    Yii::app()->controller->renderPartial('itemRelatedBlock', array('itemRelated' => $itemRelated));
} ?>

<? if (!Yii::app()->user->isGuest) { ?>
    <div class="page-title"><?= Yii::t('main', 'Недавно Вы смотрели') ?>:</div>
    <div class="products-list featured" style="width: 100%">
        <? $this->widget(
          'application.components.widgets.SearchItemsList',
          array(
            'id'                         => 'recentUser-itemslist',
            'controlAddToFavorites'      => true,
            'controlAddToFeatured'       => true,
            'controlDeleteFromFavorites' => false,
            'lazyLoad'                   => true,
            'dataType'                   => 'itemsRecentUser',
            'pageSize'                   => 10,
            'disableItemForSeo'          => $seo_disable_items_index,
          )
        );
        ?>
    </div>
<? } ?>
<? $this->widget('application.components.widgets.ProfilerBlock', array()); ?>
<? if (isset($item->top_item->debugMessages)) {
    $this->renderPartial(
      'debug',
      array(
        'debugMessages' => $item->top_item->debugMessages,
      )
    );
}?>