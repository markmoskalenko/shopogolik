<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_price.php">
 * </description>
 * Рендеринг цены товара
 **********************************************************************************************************************/
?>
<div class="item-price">
    <? if (!isset($item->sku)) {
        if ($item->top_item->price > $item->top_item->promotion_price) {
            ?>
            <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price">
                <? if (isset($item->top_item->userPromotionPriceNoDelivery)) { ?>
                    <span
                      title="<?=
                      (Yii::app()->user->inRole('superAdmin')) ? $item->top_item->userPromotionResUserPrice->report(
                      ) : '' ?>"><?= $item->top_item->userPromotionPriceNoDelivery; ?></span>
                <?
                } else {
                    ?>
                    <span
                      title="<?=
                      (Yii::app()->user->inRole('superAdmin')) ? $item->top_item->userPromotionResUserPrice1->report(
                      ) : '' ?>"><?= $item->top_item->userPromotionPrice1NoDelivery; ?></span>
                                -
                                <span
                      title="<?=
                      (Yii::app()->user->inRole('superAdmin')) ? $item->top_item->userPromotionResUserPrice2->report(
                      ) : '' ?>"><?= $item->top_item->userPromotionPrice2NoDelivery; ?></span>
                <? } ?><s>
                    <? if (isset($item->top_item->userPriceNoDelivery)) { ?>
                        <?= $item->top_item->userPriceNoDelivery; ?>
                    <?
                    } else {
                        if ($item->top_item->userPrice1NoDelivery == $item->top_item->userPrice2NoDelivery) {
                            ?>
                            <?= $item->top_item->userPrice1NoDelivery; ?>
                        <?
                        } else {
                            ?>
                            <?= $item->top_item->userPrice1NoDelivery; ?>-<?= $item->top_item->userPrice2NoDelivery; ?>
                        <?
                        }
                    }?></s></span>
        <?
        } else {
            ?>
            <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price"><?= $item->top_item->userPromotionPriceNoDelivery ?></span>
        <? } ?>
        <input type="hidden" name="price" value="<?= $item->top_item->promotion_price ?>" id="price_val"/>
    <?
    } else {
        if ($item->sku->price > $item->sku->promotion_price) {
            ?>
            <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price"><?= $item->sku->userPromotionPrice ?>
                &nbsp;<s><?= $item->sku->userPrice ?></s></span>
        <?
        } else {
            ?>
            <span><?= DSConfig::getCurrency(true) ?></span>&nbsp;<span id="price"><?= $item->sku->userPromotionPrice ?></span>
        <? } ?>
        <input type="hidden" name="price" value="<?= $item->sku->promotion_price ?>" id="price_val"/>
    <? } ?>
</div>
<? // Предварительная доставка ?>
<? if (isset($item->top_item->userPromotionDelivery) && ($item->top_item->userPromotionDelivery > 0)) {
    $chinaDelivery = $item->top_item->userDelivery;
} elseif (isset($item->top_item->userPromotionDelivery1) && ($item->top_item->userPromotionDelivery1 > 0)) {
    $chinaDelivery = $item->top_item->userPromotionDelivery1;
} elseif (isset($item->top_item->userDelivery) && ($item->top_item->userDelivery > 0)) {
    $chinaDelivery = $item->top_item->userDelivery;
} ?>
<? if (isset($chinaDelivery)) { ?>
  <div class="item-price-delivery">
      <?=Yii::t('main','Стоимость предварительной доставки')?>: <?= DSConfig::getCurrency(true) ?>&nbsp;<?=$chinaDelivery?>
  </div>
<? } ?>