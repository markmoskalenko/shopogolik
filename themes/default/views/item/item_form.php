<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="item_form.php">
 * </description>
 * Форма работы с картой товара
 **********************************************************************************************************************/
?>
<form action="<?= Yii::app()->createUrl('/cart/add') ?>" method="POST">
    <div class="form-item" style="border: none;">
        <input type="hidden" name="cid" value="<?= $item->top_item->cid ?>" id="cid"/>
        <input type="hidden" name="iid" value="<?= $item->top_item->num_iid ?>" id="iid"/>
        <input type="hidden" name="inputprops-processed" id="inputprops-processed" value="0"/>
        <?php
        if (!$ajax['input']) {
            echo Yii::app()->controller->renderPartial(
              'input_props',
              array(
                'totalCount'  => $item->top_item->num,
                'input_props' => $input_props
              )
            );
        } else {
            ?>
            <div id="item-input-props">
                <script async type="text/javascript">
                    loadInputProps();
                </script>
            </div>
        <?php } ?>
    </div>
    <div class="form-item">
        <label for="num"><?= Yii::t('main', 'Количество') ?>:</label>

        <div class="number">
            <nobr>
                <p class="minus">-</p>
                <input type="text" onchange="getUserPrice()" name="num" id="num" class="item-count" value="1"/>
                <input type="hidden" name="num-processed" id="num-processed" value="1"/>

                <p class="plus">+</p></nobr>
        </div>
                    <span id="item-count-price" class="item-info-param">x
                        <span id="count-price">
                            <?=
                            (isset($item->sku))
                              ? Formulas::priceWrapper($item->sku->userPriceFinal) :
                              Formulas::priceWrapper($item->top_item->userPriceFinal) ?>
                        </span> =
                        <span id="sum"><?=
                            (isset($item->sku)) ? $item->sku->userPromotionPrice
                              : (isset($item->top_item->userPromotionPrice)) ? $item->top_item->userPromotionPrice
                              : $item->top_item->userPromotionPrice1 . '-' . $item->top_item->userPromotionPrice2 ?></span>
                    </span>
    </div>
    <div class="form-item">
        <label><?= Yii::t('main', 'В наличии') ?>:</label>

        <div class="item-info-param">
            <span id="item_num"><?= $item->top_item->num ?></span>&nbsp;
        </div>
        <input type="hidden" name="item_totalcount" id="item_totalcount" value="<?= $item->top_item->num ?>">
        <? if (Yii::app()->user->checkAccess('@showTaobaoLinkInUserCard')) { ?>
            <div class="item-info-param" id="item_ontaobao">
                <a href="http://item.taobao.com/item.htm?id=<?= $item->top_item->num_iid ?>" target="_blank">
                    &nbsp;<?= Yii::t('main', 'на taobao.com') ?></a>
            </div>
        <? } ?>
    </div>
    <? if ($item->top_item->weight_calculated > 0) { ?>
        <div class="form-item">
            <label><?= Yii::t('main', 'Примерный вес 1 шт., грамм') ?>:</label>

            <div class="item-info-param">
                <span id="item_weight"><?= $item->top_item->weight_calculated ?></span>&nbsp;
            </div>
        </div>
    <? } ?>
    <div>
        <input type="submit" title="<?= Yii::t('admin', 'Сначала выберите характеристики товара') ?>"
               disabled="disabled" name="doGo" value="<?= Yii::t('main', 'Добавить в корзину') ?>"
               class="buy-btn bigger"/>
        <input type="button" name="dofav" value="<?= Yii::t('main', 'Добавить в избранное') ?>" class="fav-btn bigger"
               formaction="<?=
               Yii::app()->createUrl(
                 '/cabinet/favorite/add',
                 array(
                   'iid'      => $item->top_item->num_iid,
                   'download' => true
                 )
               ) ?>"
               onclick="addFavorite(this,<?= $item->top_item->num_iid ?>); return false;"/>
        <? if ((Yii::app()->user->notInRole(array('guest', 'user')))) { ?>
            <a class="ui-icon ui-icon-heart" style="display:inline-block; cursor: pointer;"
               title="<?= Yii::t('admin', 'Добавить в рекомендованное') ?>"
               href="<?= Yii::app()->createUrl('/admin/featured/add/', array('id' => $item->top_item->num_iid)) ?>"
               onclick="addFeatured(this,<?= $item->top_item->num_iid ?>); return false;"></a>
        <? } ?>
        <?/* if ((Yii::app()->user->notInRole(array('guest', 'user')))) { ?>
            <a class="ui-icon ui-icon-star" style="display:inline-block; cursor: pointer;"
               title="<?= Yii::t('admin', '"Экспорт в XML') ?>"
               href="<?= '/item/index/iid/' . $item->top_item->num_iid . '/exportType/CSV' ?>"></a>
        <? } */
        ?>
        <a href="javascript:void(0);" onclick="window.history.back();">
            <?= Yii::t('main', 'Продолжить покупки') ?>
        </a>

        <div style="margin: 0 0 10px 10px;">
            <!-- Vkontakte Script -->
            <script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
            <!-- Put this script tag to the place, where the Share button will be -->
            <script type="text/javascript">
                document.write(VK.Share.button(false, {
                    type: "round_nocount",
                    text: "<?=Yii::t('main','Сохранить')?>"
                }));
            </script>
        </div>
    </div>
</form>
