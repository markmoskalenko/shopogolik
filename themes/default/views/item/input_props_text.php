<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="input_props_text.php">
 * </description>
 * Рендеринг блока выбора свойств товара в карте товара - для свойств с текстом
 **********************************************************************************************************************/
?>
<div class="input-props-text">
    <table style="width: 555px;">
        <tr>
            <td style="vertical-align: top; padding-right: 10px;">
                <div style="display: block ;  text-align: right;">
                    <span style="font-weight: bold;"><?= $prop->name ?>:</span>
                    <input hidden="hidden" id="input-<?= $pid ?>" class="input_params" value="0">

                    <div>
                        <small>
                            <div class="select">
                                <span id="selres-<?= $pid ?>"><?= Yii::t('main', 'Не выбрано') ?></span>
                            </div>
                        </small>
            </td>
            <td style="width: 425px; vertical-align: top;">
                <div style="display: block;">
                    <ul class="selectable" id="<?= $pid ?>">
                        <?php foreach ($prop->childs as $i => $val) { ?>
                            <?php if (trim($val->name) !== '') { ?>
                                <li class="ui-widget-content prop"
                                    id="<?= $pid ?>:<?= $val->vid ?>"><?= $val->name ?></li>
                            <? } ?>
                        <? } ?>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
</div>
