<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="dropshop.global.js.php">
 * </description>
 * Набор jawaScript, использующегося во всех view
 **********************************************************************************************************************/
?>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.core.js' : 'minified/jquery.ui.core.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.widget.js' : 'minified/jquery.ui.widget.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.mouse.js' : 'minified/jquery.ui.mouse.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.button.js' : 'minified/jquery.ui.button.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.draggable.js' : 'minified/jquery.ui.draggable.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.position.js' : 'minified/jquery.ui.position.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.resizable.js' : 'minified/jquery.ui.resizable.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.dialog.js' : 'minified/jquery.ui.dialog.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.ui.accordion.js' : 'minified/jquery.ui.accordion.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/<?= YII_DEBUG ? 'jquery.dcjqaccordion.2.9.js' : 'minified/jquery.dcjqaccordion.2.9.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/<?= YII_DEBUG ? 'jquery.cookie.js' : 'jquery.cookie.min.js' ?>"></script>
<script type="text/javascript"
        src="<?= $this->frontThemePath ?>/js/ui/minified/jquery.hoverIntent.minified.js"></script>