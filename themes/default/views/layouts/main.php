<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="main.php">
 * </description>
 * Лэйаут фронта сайта
 **********************************************************************************************************************/
?>
<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $this->pageTitle ?></title>
    <link rel="icon" href="<?= $this->frontThemePath ?>/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?= $this->frontThemePath ?>/images/favicon.ico" type="image/x-icon"/>
    <? /* Подключение css и javaScript, которые используются во всех представлениях фронта,
          для сомнительного удобства вынесено в отдельные файлы.*/
    ?>
    <? $this->renderPartial('//layouts/dropshopGlobalCss', array()); ?>
    <? $this->renderPartial('//layouts/dropshopGlobalJs', array()); ?>
    <script type="text/javascript" src="<?= $this->frontThemePath ?>/js/main.js?v=<?= Search::$intversion ?>"></script>
</head>

<body
  class="<?= $this->columns ?> <?=
  Utils::TransLang() ?> <?= $this->id ?> <?= (!($this->id == $this->body_class)) ? $this->body_class : '' ?>">
<header>
    <div class="user-info">
        <div class="user-info-block">
            <table style="width:100%;">
                <tbody>
                <tr>
                    <td style="width:50%;">
                        <div style="text-align:left;">
                            <a class="space" href="<?= $this->createUrl('/article/about') ?>"><?=
                                Yii::t(
                                  'main',
                                  'О нас'
                                ) ?></a>
                            <a class="space"
                               href="<?= $this->createUrl('/tools/question') ?>"><?=
                                Yii::t(
                                  'main',
                                  'Задать вопрос'
                                ) ?></a>
                            <a class="space" href="<?= $this->createUrl('/article/about') ?>"><?=
                                Yii::t(
                                  'main',
                                  'Помощь'
                                ) ?></a>
                            <a class="space"
                               href="<?= $this->createUrl('/article', array('url' => 'contacts')) ?>"><?=
                                Yii::t(
                                  'main',
                                  'Контакты'
                                ) ?></a>
                        </div>
                        <div class="lang">
                            <? // Блок выбора языка отображения фронта?>
                            <? $this->widget('application.components.widgets.languageBlock'); ?>
                        </div>
                    </td>
                    <td style="text-align:right;">
                        <div class="currency">
                            <? // Блок выбора валюты отображения фронта?>
                            <? $this->widget('application.components.widgets.currencyBlock'); ?>
                        </div>
                        <div class="">
                            <? // Блок логина, входа в кабинет, остатка на счету?>
                            <? $this->widget('application.components.widgets.userBlock'); ?>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="user-info-block" style="height: 130px;">
            <div class="b-upperpart">
                <?= cms::customContent('main-header-logo') ?>
                <div class="upperpart-description">
                    <?= cms::customContent('main-header-slogan') ?>
                </div>
                <div class="upperpart-phone">
                    <div>
                        <img style="float:left;"
                             src="<?= $this->frontThemePath ?>/images/china.png">
                        <?= cms::customContent('phone-in-china') ?>
                    </div>
                    <div>
                        <img style="float:left;"
                             src="<?= $this->frontThemePath ?>/images/russia.png">
                        <?= cms::customContent('phone-in-russia') ?>
                    </div>
                </div>
                <div class="upperpart-skype">
                    <img src="<?= $this->frontThemePath ?>/images/skype.png">
                    <a href="skype:skype">Skype</a>
                </div>
                <div class="upperpart-email">
                    <img src="<?= $this->frontThemePath ?>/images/mail.png">
                    <a href="mailto:mail@mail.com">E-mail</a>
                </div>
                <div class="upperpart-trash">
                    <div class="basket-block">
                        <?php
                        $this->widget('application.components.widgets.CartBlock');
                        ?>
                    </div>
                </div>
                <div class="upperpart-call"></div>
                <div class="upperpart-calc"></div>
                <div class="upperpart-cabinet"></div>
                <div class="b-search-wrapper">
                    <? // Блок поискового запроса ?>
                    <?php $this->widget('application.components.widgets.SearchQueryBlock'); ?>
                </div>
            </div>
        </div>
        <div class="user-info-block">
            <div class="main-menu">
                <?= cms::menuContent('main-menu',true,true) ?>
           </div>
       </div>
</header>
<div class="wrapper">
    <?php if ($this->id == 'site' && $this->body_class == 'home') { ?>
        <? // Блок слайдера баннеров на главной ?>
        <? $this->widget('application.components.widgets.sliderBlock') ?>
    <? } ?>
    <div class="clear after-head"></div>
    <?php $this->widget(
      'zii.widgets.CBreadcrumbs',
      array(
        'links'    => $this->breadcrumbs,
        'homeLink' => CHtml::link(Yii::t('main', 'Главная'), array('/site/index'))
      )
    );
    ?>
    <? // Блок вывода всплывающих сообщений, если они есть?>
    <?php $this->widget('application.components.widgets.MessagesBlock') ?>
    <div class="left-col">
        <? // Вывод параметров поиска в поисковых же страницах?>
        <?  if (in_array(
            $this->id,
            array('search', 'category', 'favorite', 'brand', 'seller', 'site')
          ) && ($this->body_class != 'cabinet')
        ) {
            if (($this->id != 'site')) {
                $this->widget(
                  'application.components.widgets.SearchParams',
                  array(
                    'type'         => $this->id,
                    'params'       => $this->params['params'],
                    'cids'         => $this->params['cids'],
                    'bids'         => $this->params['bids'],
                    'groups'       => $this->params['groups'],
                    'filters'      => $this->params['filters'],
                    'multiFilters' => $this->params['multiFilters'],
                    'suggestions'  => $this->params['suggestions'],
                    'priceRange'   => $this->params['priceRange'],
                  )
                );
            }
            ?>
            <? // Вертикальный список категорий на главной и в поисковых страницах ?>
            <h4 class="page-title"><?= Yii::t('main', 'Категории товаров') ?></h4>
            <div id="main-cats-menu-ajax"><span style="text-align: center;"><img src="/images/ajax-loader.gif"></span>
            </div>
            <? if (Yii::app()->user->notInRole('guest')) { ?>
                <h4 class="page-title"><?= Yii::t('main', 'Избранное') ?></h4>
                <div id="favorites-menu">
                    <? // Блок категорий избранного, который выводится ниже меню категорий?>
                    <?
                    $this->widget(
                      'application.components.widgets.FavoritesMenuBlock',
                      array(
                        'adminMode' => false,
                      )
                    );
                    ?>
                </div>
            <? } ?>
            <? // Блок профайлера?>
            <? $this->widget('application.components.widgets.ProfilerBlock', array()); ?>
        <?
        } elseif ($this->body_class == 'cabinet') {
            // Блок меню кабинета
            $this->widget('application.components.widgets.cabinetMenuBlock');
        } elseif ($this->id == 'article') {
            ?>
            <?= cms::menuContent('help-vertical-menu') ?>
        <?
        } ?>
    </div>
    <?php if ($this->body_class == 'cabinet') { ?>
    <div class="main-col" id="cabinet">
        <? $this->widget('application.components.widgets.UserNoticeBlock'); ?>
        <? } elseif ($this->id == 'article') { ?>
        <div class="main-col" id="article">
            <? } else { ?>
            <div class="main-col" id="content">
                <? } ?>
                <? //Здесь выводится контент представления контроллера ?>
                <?= $content ?>
            </div>
            <div class="clear" style="height: 15px;"></div>
            <?php if ($this->id == 'site' && $this->body_class == 'home') { ?>
                <div>
                    <? //Рендеринг блока брендов?>
                    <? $this->widget('application.components.widgets.BrandsBlock'); ?>
                </div>
                <div class="clear"></div>
                <?= cms::customContent('main') ?>
            <? } ?>
            <div class="footer">
                <div class="menu">
                    <?= cms::menuContent('main-footer-menu') ?>
                </div>
                <div class="info">
                    <div class="paySystems">
                        <?= cms::customContent('main-paysystems') ?>
                    </div>
                    <div class="footerText"><?= cms::customContent('main-footertext') ?></div>
                    <div class="copyright">© 2014, <?= Yii::t('main', 'разработка и сопровождение') ?>: <a
                          href="http://dropshop.pro">dropshop:pro</a></div>
                </div>
                <div class="version"><?= Search::$version; ?></div>
            </div>
            <? //Скрипт кнопки вверх, назад-вперёд в поисковых выдачах ?>
            <? if (in_array(
                $this->id,
                array('search', 'category', 'favorite', 'brand', 'seller')
              ) && ($this->body_class != 'cabinet')
            ) {
                ?>
                <script src="<?= $this->frontThemePath ?>/js/up.js" type="text/javascript"></script>
            <? } ?>
            <? // Диалог корректировки переводов - используется повсеместно?>
            <div id="translationDialog" style="display: none;">
                <? if ((Yii::app()->user->notInRole(array('guest', 'user')))) { ?>
                    <?php $this->beginWidget(
                      'zii.widgets.jui.CJuiDialog',
                      array(
                        'id'      => 'translationDialog',
                        'options' => array(
                          'title'     => Yii::t('main', 'Редактирование перевода'),
                          'autoOpen'  => false,
                          'modal'     => true,
                          'resizable' => false,
                          'position'  => "[725,100]",
                          'style'     => "[725,100]",
                          'width'     => '70%',
                          'height'    => 'auto',
                        ),
                      )
                    );
                    echo $this->renderPartial('//site/translate', array());
                    $this->endWidget('zii.widgets.jui.CJuiDialog');
                    ?>
                <? } ?>
            </div>

            <? //Ленивая загрузка изображений. Используется в самых разных местах?>
            <? if (DSConfig::getVal('site_images_lazy_load') == 1) { ?>
                <script type="text/javascript"
                        src="<?= $this->frontThemePath ?>/js/<?= YII_DEBUG ? 'jquery.lazyload.js' : 'jquery.lazyload.min.js' ?>"></script>
                <script type="text/javascript">
                    $(function () {
                        $("img.lazy").show().lazyload({
                            effect: "fadeIn",
                            effect_speed: 500
//      skip_invisible : false
                        });
                    });
                </script>
            <? } ?>
        </div>
</body>
</html>