<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="dropshopGlobalJs.php">
 * </description>
 * Набор CSS, использующегося во всех view
 **********************************************************************************************************************/
?>
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/gridview/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/listview/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/pager/pager.css"/>
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/jquery-ui-1.9.2.custom.css"/>
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/font-awesome.min.css">
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/font-awesome-ie7.min.css">
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/taobaoratings.css"/>
<link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="<?= CHtml::asset($this->frontThemeAbsolutePath .'/css/ds.global.less') ?>"/>
<link rel="stylesheet" type="text/css" href="<?= CHtml::asset($this->frontThemeAbsolutePath .'/css/ds.layout.less') ?>"/>
