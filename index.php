<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 * Используйте $debug=true для включения отладки
 **********************************************************************************************************************/
?>
<?php
$debug = (($_SERVER['SERVER_NAME'] == '777.danvit.net') || FALSE);
$debug = true;
if ($debug) {
  error_reporting(E_ALL);
  define('YII_PROFILING', FALSE);
    defined('YII_DEBUG') or define('YII_DEBUG', TRUE);
    defined('YII_DEBUG_SHOW_PROFILER') or define('YII_DEBUG_SHOW_PROFILER', TRUE);
  //enable profiling
    defined('YII_DEBUG_PROFILING') or define('YII_DEBUG_PROFILING', TRUE);
  //trace level
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 0);
  //execution time
    defined('YII_DEBUG_DISPLAY_TIME') or define('YII_DEBUG_DISPLAY_TIME', TRUE);
  require_once(dirname(__FILE__) . '/../framework/yii.php');
}
else {
  define('YII_PROFILING', FALSE);
  require_once(dirname(__FILE__) . '/../framework/yiilite.php');
}

$configFile = dirname(__FILE__) . '/protected/config/main.php';

$app = Yii::createWebApplication($configFile);
if (!$debug) {
  Yii::app()->db->schemaCachingDuration = 3600;
}
if (DSConfig::getVal('site_under_construction') == 1) {
  $requestURL = Yii::app()->request->getRequestUri();
  $isAdmin = ((strpos($requestURL, '/admin')!==false)||(strpos($requestURL, '/message')!==false)||(strpos($requestURL, '/img/index')!==false));
  if ($isAdmin === FALSE) {
    if (($requestURL != '/user/login') && ($requestURL != '/cabinet')) {
//	echo $requestURL; die;
      $app->catchAllRequest = array('under/index');
    }
  };
}
error_reporting(1);
$app->run();
