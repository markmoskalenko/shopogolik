<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CabinetForm.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customCabinetForm extends CFormModel {

    public $sum;
    public $publicAccount;
    /*
     * Имя
     * @var string
     */
    public $firstname;
    /*
     * Фамилия
     * @var string
     */
    public $lastname;
    /*
     * Отчество
     * @var string
     */
    public $patroname;
    
    public $email;
    
    
    //псевдоним
    public $alias;
    //число рождения
    public $d_birth;
    public $m_birth;
    public $y_birth;
    public $birthday;
    
    //пол
    public $sex;
    
    public $skype;
    public $vk;
    public $promo_code;

    //password reset
    public $password;    
    public $new_password;    
    public $new_password2;
    
    
    //address
    public $country;    
    public $index;    
    public $city;    
    public $address;    
    public $phone;
    
    //support
    public $theme;    
    public $text;
    public $order_id;
    public $category;
    public $file;
    public $region;


  public function onUnsafeAttribute($name, $value) {
    //silently ignore unsafe attributes
  }


    function rules() {
        return array(            
            array('sum', 'required', 'on'=>'payment'),
            array('sum','numerical','on'=>'payment'),
          array('publicAccount', 'required', 'on'=>'payment'),

            array('firstname,patroname,lastname,password','required','on'=>'profile'),
            //array('password','exist', 'on'=>'profile','className'=>'Users'), //d_birth,m_birth,y_birth,
            array('d_birth,m_birth,y_birth,patroname,alias,birthday,skype,vk,promo_code','safe'),

            array('email,password','required','on'=>'email'),
            array('email','email'),

            array('password,new_password,new_password2','required','on'=>'password'),            
            array('new_password2','compare','compareAttribute'=>'new_password', 'on'=>'password'),

            array('country,index,city,address,phone,firstname,patroname,lastname','required','on'=>'address'),
            //array('patroname','exist','on'=>'address','className'=>'Users'),
            array('phone','length','min'=>11,'max'=>16),
            array('phone','numerical','integerOnly'=>true),
            array('phone', 'required', 'on'=>'payment'),
            array('theme,text,category','required','on'=>'support'),
            //array('file,order_id','exist','on'=>'support','className'=>'Users'),
            array('file','file','maxSize'=>1*1024*1024,'tooLarge'=>Yii::t('main','Файл должен быть меньше, чем 1MB'), 'allowEmpty'=>true),
            //array('order_id,file','exist','on'=>'support','className'=>'Users'),
            array('country,index,city,address,phone','required','on'=>'createaddress'),
        );
    }
    
    function attributeLabels() 
    {
        return array(
            'category'  => Yii::t('main','Категория'),
            'region'    => Yii::t('main','Регион'),
            'sum'       => Yii::t('main','Сумма платежа').' ('.strtoupper(DSConfig::getVal('site_currency')).')',
            'firstname' => Yii::t('main','Имя'),
            'lastname'  => Yii::t('main','Фамилия'),
            'patroname' => Yii::t('main','Отчество'),
            'alias'     => Yii::t('main','Псевдоним'),
            //'sex'       => Yii::t('main','Ваш пол'),
            'email'     => Yii::t('main','Новый E-mail'),
            'password'  => Yii::t('main','Текущий пароль'),
            'new_password'  => Yii::t('main','Новый пароль'),
            'new_password2'  => Yii::t('main','Повторите новый пароль'),
            'd_birth'      => Yii::t('main','Число рождения'),
            'm_birth'      => Yii::t('main','Месяц рождения'),
            'y_birth'      => Yii::t('main','Год рождения'),
            'country'   => Yii::t('main','Страна'),
            'city'      => Yii::t('main','Город'),
            'index'     => Yii::t('main','Индекс'),
            'address'   => Yii::t('main','Адрес'),
            'phone'     => Yii::t('main','Моб. тел. +XXXXXXXXXXX'),
            'theme'     => Yii::t('main','Тема'),
            'text'      => Yii::t('main','Текст сообщения'),
            'order_id'     => Yii::t('main','Номер заказа (если есть)'),
            'file'      => Yii::t('main','Прикрепить файл'),
            'skype'=>Yii::t('main','Ваш Skype'),
            'vk'=>Yii::t('main','Ваша страница ВКонтакте'),
            'promo_code'=>Yii::t('main','Промо-код'),
        );
    }
}