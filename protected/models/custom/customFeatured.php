<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Featured.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "featured".
 *
 * The followings are the available columns in table 'Featured':
 * @property string $num_iid
 * @property string $date
 * @property string $cid
 * @property double $express_fee
 * @property double $price
 * @property double $promotion_price
 * @property string $pic_url
 * @property string $title_zh
 * @property string $title_en
 * @property string $title_ru
 */
class customFeatured extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'featured';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('num_iid', 'required'),
			array('express_fee, price, promotion_price', 'numerical'),
			array('num_iid, cid', 'length', 'max'=>20),
			array('pic_url', 'length', 'max'=>512),
			array('title_zh, title_en, title_ru', 'length', 'max'=>4000),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('num_iid, date, cid, express_fee, price, promotion_price, pic_url, title_zh, title_en, title_ru', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'num_iid' => 'Num Iid',
			'date' => 'Date',
			'cid' => 'Cid',
			'express_fee' => 'Express Fee',
			'price' => 'Price',
			'promotion_price' => 'Promotion Price',
			'pic_url' => 'Pic Url',
			'title_zh' => 'Title Zh',
			'title_en' => 'Title En',
			'title_ru' => 'Title Ru',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

//		$criteria->compare('num_iid',$this->num_iid,true);
//		$criteria->compare('date',$this->date,true);
//		$criteria->compare('cid',$this->cid,true);
//		$criteria->compare('express_fee',$this->express_fee);
//		$criteria->compare('price',$this->price);
//		$criteria->compare('promotion_price',$this->promotion_price);
//		$criteria->compare('pic_url',$this->pic_url,true);
//		$criteria->compare('title_zh',$this->title_zh,true);
//		$criteria->compare('title_en',$this->title_en,true);
//		$criteria->compare('title_ru',$this->title_ru,true);
      $criteria->order='`date` DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
          'pagination'=>array(
            //Количество отзывов на страницу
            'pageSize'=>44,
          ),
          'sort'=>array(
            'defaultOrder'=>'`date` DESC',
          )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return featured the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}
}
