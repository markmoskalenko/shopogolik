<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersStatuses.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "orders_statuses".
 *
 * The followings are the available columns in table 'orders_statuses':
 * @property integer $id
 * @property string $value
 * @property string $name
 * @property string $descr
 * @property integer $manual
 * @property string $aplyment_criteria
 * @property string $auto_criteria
 * @property integer $order_in_process
 * @property integer $enabled
 *
 * The followings are the available model relations:
 * @property Orders[] $orders
 */
class customOrdersStatuses extends CActiveRecord
{
private static $_orderStatusesManual0=false;
private static $_orderStatusesManual1=false;
private static $_statusesEnabledDBQuery=false;
private static $_statusesForFilter=false;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders_statuses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('value, name', 'required'),
			array('manual, order_in_process, enabled', 'numerical', 'integerOnly'=>true),
			array('value', 'length', 'max'=>128),
			array('name', 'length', 'max'=>256),
			array('descr, aplyment_criteria, auto_criteria', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, value, name, descr, manual, aplyment_criteria, auto_criteria, order_in_process, enabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orders' => array(self::HAS_MANY, 'Orders', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main','id записи заказа. Внимание, в расчетах не используется!'),
			'value' => Yii::t('main','ID заказа для расчетов'),
			'name' => Yii::t('main','Название'),
			'descr' => Yii::t('main','Описание статуса заказа'),
			'manual' => Yii::t('main','Устанавливается ли статус вручную'),
			'aplyment_criteria' => Yii::t('main','Условие применения статуса к заказу'),
			'auto_criteria' => Yii::t('main','Условие вычисления статуса'),
			'order_in_process' => Yii::t('main','Порядок статуса в бизнес-процессе'),
			'enabled' => Yii::t('main','Включен ли статус'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('descr',$this->descr,true);
		$criteria->compare('manual',$this->manual);
		$criteria->compare('aplyment_criteria',$this->aplyment_criteria,true);
		$criteria->compare('auto_criteria',$this->auto_criteria,true);
		$criteria->compare('order_in_process',$this->order_in_process);
		$criteria->compare('enabled',$this->enabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
          'sort' => array(
            'defaultOrder' => 't.enabled DESC, t.order_in_process ASC',
          ),
          'pagination' => array(
            'pageSize' => 100,
          )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdersStatuses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}
//=================================================================
// Возвращает список статусов и кол-во заказов в них
  public static function getAllStatusesListAndOrderCount($uid=false,$manager=false){
    if ($uid===false) {
      $_uid=null;
    } else {
      $_uid=$uid;
    }
    if ($manager===false) {
      $_manager=null;
    } else {
      $_manager=$manager;
    }
    if (!self::$_statusesEnabledDBQuery) {
    $command = Yii::app()->db->createCommand("select * from orders_statuses os where os.enabled=1 order by order_in_process");
    self::$_statusesEnabledDBQuery=$command->queryAll();
    }
      $statuses=self::$_statusesEnabledDBQuery;
    if ($statuses) {
      foreach ($statuses as $i=>$status) {
        if ($status['manual']==1) {
          $cntAndDate=Yii::app()->db->createCommand("select count(0) as cnt, max(date) as lastdate, round(sum(delivery+`sum`),2) as totalsum,
          round(sum((select ifnull(sum(summ),0) as `sum` from orders_payments op
where op.oid = oo.id)),2) as totalpayed,
          round(sum((select ifnull(sum(summ),0) as `sum` from orders_payments op
where op.oid = oo.id))-sum(ifnull(manual_delivery,delivery)+ifnull(manual_sum,`sum`)),2) as totalnopayed
from orders oo where status=:value and (uid=:uid or :uid is null) and (manager=:manager or :manager is null)")
            ->bindParam(':value', $status['value'], PDO::PARAM_STR)
            ->bindParam(':uid', $_uid, PDO::PARAM_INT)
            ->bindParam(':manager', $_manager, PDO::PARAM_INT)
            ->queryRow();
        } else {
            $auto_criteria=trim($status['auto_criteria']," \t\n\r\0\\x0B;");
            if (!$auto_criteria) {
                $auto_criteria='select id from orders where 1=0';
            }
          $cntAndDate=Yii::app()->db->createCommand("select count(0) as cnt, max(date) as lastdate, round(sum(delivery+`sum`),2) as totalsum,
          round(sum((select ifnull(sum(summ),0) as `sum` from orders_payments op
where op.oid = tt.id)),2) as totalpayed,
round(sum((select ifnull(sum(summ),0) as `sum` from orders_payments op
where op.oid = tt.id))-sum(ifnull(manual_delivery,delivery)+ifnull(manual_sum,`sum`)),2) as totalnopayed from orders tt where
tt.id in ({$auto_criteria})")
            ->bindParam(':uid', $_uid, PDO::PARAM_INT)
            ->bindParam(':manager', $_manager, PDO::PARAM_INT)
            ->queryRow();
        }
        $statuses[$i]['count']=$cntAndDate['cnt'];
        $statuses[$i]['lastdate']=$cntAndDate['lastdate'];
        $statuses[$i]['totalsum']=$cntAndDate['totalsum'];
        $statuses[$i]['totalpayed']=$cntAndDate['totalpayed'];
        $statuses[$i]['totalnopayed']=$cntAndDate['totalnopayed'];
      }
      return $statuses;
    } else {
      return array();
    }
  }
//-------------------------
public static function getInQueryForOrderStatus($status){
  if ($status!=null) {
  $orderStatus=OrdersStatuses::model()->find('value=:value',array(':value'=>$status));
      if ($orderStatus['manual']==1) {
        $query="select oo.id from orders oo where oo.status='{$orderStatus['value']}'";
      } else {
        $query=trim($orderStatus['auto_criteria']," \t\n\r\0\\x0B;");
      }
  } else {
    $query="select oo.id from orders oo";
  }
    return $query;
}
//-----------------------------------------------------------------
  public static function getCaseQueryForOrderExtStatus(){
    if (!self::$_orderStatusesManual0) {
        self::$_orderStatusesManual0=OrdersStatuses::model()->findAll('enabled=1 and manual=0 order by order_in_process');
    }
    $orderStatuses=self::$_orderStatusesManual0;
    $caseQuery='';
    foreach ($orderStatuses as $orderStatus) {
        $auto_criteria=trim($orderStatus['auto_criteria']," \t\n\r\0\\x0B;");
      $caseQuery=$caseQuery." WHEN t.id IN (".$auto_criteria.") THEN '".$orderStatus['name']."'
      ";
    }
    $caseQuery=" CASE ".$caseQuery." ELSE '' END ";
    return $caseQuery;
  }

  public static function getAllowedStatusesForOrder($id,$uid,$manager){
      if (!self::$_orderStatusesManual1) {
          self::$_orderStatusesManual1=OrdersStatuses::model()->findAll('enabled=1 and manual=1 order by order_in_process');
      }
      $orderStatuses=self::$_orderStatusesManual1;
    $result=array();
    foreach ($orderStatuses as $orderStatus) {
      if (!$orderStatus['aplyment_criteria']) {
        continue;
      }
        $aplyment_criteria=trim($orderStatus['aplyment_criteria']," \t\n\r\0\\x0B;");
      $order=Order::model()->findBySql('select oo.id from orders oo where oo.id in ('.$aplyment_criteria.') and oo.id=:id',
        array(':id'=>$id,':uid'=>$uid,':manager'=>$manager));
      if ($order || Yii::app()->user->checkAccess('@allowChangeAnyOrderState')) {
        $result[$orderStatus['value']]=Yii::t('main',$orderStatus['name']);
      }
    }
    return $result;
  }

  public static function isAllowedStatusForOrder($status,$id,$uid,$manager) {
    $orderStatuses=self::getAllowedStatusesForOrder($id,$uid,$manager);
    return isset($orderStatuses[$status]);
  }

  public static function getStatusName($status) {
    $res=self::model()->find('value=:value',array(':value'=>$status));
    if ($res) {
      return Yii::t('main',$res->name);
    } else {
      return '-';
    }
  }

  public static function getStatusListForFilter(){
      if(!self::$_statusesForFilter) {
          self::$_statusesForFilter=self::model()->findAll('enabled=1 and manual=1 order by order_in_process');
      }
    $orderStatuses=self::$_statusesForFilter;
    $result=array();
    foreach ($orderStatuses as $orderStatus) {
      $result[$orderStatus['value']]=Yii::t('main',$orderStatus['name']);
    }
    return $result;
  }

}
