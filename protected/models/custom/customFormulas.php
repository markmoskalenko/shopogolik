<?php

/**
 * This is the model class for table "formulas".
 *
 * The followings are the available columns in table 'formulas':
 * @property integer $id
 * @property string $formula_id
 * @property string $formula
 * @property string $description
 */
class customFormulas extends CActiveRecord
{

    protected static $_cache = array();

    protected static $_UserK = false;

    protected static $_srcCurrRate = false;
    protected static $_srcCurr = false;
    protected static $_destCurrRate = false;
    protected static $_destCurr = false;

    protected static $_priceK_array = false;
    protected static $_skidkaK_array = false;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'formulas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('formula_id', 'length', 'max' => 256),
          array('formula, description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array('id, formula_id, formula, description', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id'          => 'PK',
          'formula_id'  => Yii::t('main', 'id формулы'),
          'formula'     => Yii::t('main', 'Формула расчета, php'),
          'description' => Yii::t('main', 'Описание формулы'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('formula_id', $this->formula_id, true);
        $criteria->compare('formula', $this->formula, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Formulas the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model(preg_replace('/^custom/', '', $className));
    }

    /* =====================================================================================================================

              Собственно, формулы.

       ===================================================================================================================*/
    public static function getUserPrice(array $params)
    {
        /*
          array(
            'price'=>'',
            'count'=>'',
            'deliveryFee'=>'',
            'postageId'=>'',
            'sellerNick'=>'',
            'asHtml' => FALSE,
            'currency' => FALSE,
            'user' => FALSE,
          )
          */
        //$price, $count, $deliveryFee, $postageId, $sellerNick, $asHtml = FALSE, $currency = FALSE, $user = FALSE
        //ini_set('precision', 20);
        $intParams = array(
          'price'       => new CException('getUserPrice: no price defined!'),
          'count'       => new CException('getUserPrice: no count defined!'),
          'deliveryFee' => new CException('getUserPrice: no deliveryFee defined!'),
          'postageId'   => new CException('getUserPrice: no postageId defined!'),
          'sellerNick'  => new CException('getUserPrice: no sellerNick defined!'),
          'currency'    => DSConfig::getCurrency(),
          'currencyDate'=>false,
          'user'        => Yii::app()->user->id,
        );
        //$price, $count, $deliveryFee, $postageId, $sellerNick, $asHtml = FALSE, $currency = FALSE, $user = FALSE
        foreach ($intParams as $key => $default) {
            $intParams[$key] = isset($params[$key]) ? $params[$key] : $default;
            if (is_a($intParams[$key], 'CException')) {
                throw $intParams[$key];
            }
        }
// === Variables =============
        $vars = array();
//-----------------------------------------------------------
        $vars['UserK'] = self::getUserK($intParams['user']);
        $vars['mainK'] = DSConfig::getVal('price_main_k');
//-- Postage calulations --
        $vars['postage'] = new stdClass();
        $vars['postage']->start_fee = $intParams['deliveryFee'];
        $vars['postage']->add_fee = (($intParams['deliveryFee']>50)?$intParams['deliveryFee']:0);
        $vars['postage']->add_standard = 1;
        if ((!$vars['postage']->start_fee) || ($vars['postage']->start_fee == 0)) {
            $vars['postage']->start_fee = 0; //DSConfig::getVal('delivery_default_chinese_fee_cny');
            $vars['postage']->add_fee = 0; //DSConfig::getVal('delivery_default_chinese_add_fee_cny');
            $vars['postage']->add_standard = 1;
        }
//----------------------
        if (eval(self::getFormula('getUserPrice[postFee]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[postFee]');
        }
        if (eval(self::getFormula('getUserPrice[priceWithPostageAndCount]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[priceWithPostageAndCount]');
        }
        $vars['discountPriceK'] = self::getPriceK($vars['priceWithPostageAndCount']);
        $vars['discountCountK'] = self::getCountK($intParams['count']);

        if (eval(self::getFormula('getUserPrice[discountCountSumm]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[discountCountSumm]');
        }
        if (eval(self::getFormula('getUserPrice[discountNoCountSumm]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[discountNoCountSumm]');
        }
        if (eval(self::getFormula('getUserPrice[userPriceInCNY]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[userPriceInCNY]');
        }
        if (eval(self::getFormula('getUserPrice[userOldPriceInCNY]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[userOldPriceInCNY]');
        }
        if (eval(self::getFormula('getUserPrice[discount]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[discount]');
        }
        if (eval(self::getFormula('getUserPrice[userDeliveryInCNY]')) === false) {
            throw new CException('Ошибки в формуле getUserPrice[userDeliveryInCNY]');
        }
        $vars['discountInCurr'] = self::convertCurrency($vars['discount'], 'cny', $intParams['currency'], false);
        $vars['userPriceInCurr'] = self::convertCurrency($vars['userPriceInCNY'], 'cny', $intParams['currency'], false,$intParams['currencyDate']);
        $vars['userDeliveryInCurr'] = self::convertCurrency(
          $vars['userDeliveryInCNY'],
          'cny',
          $intParams['currency'],
          false
        );
        $result = new FormulasGetUserPriceResult();
        $result->price = self::cRound($vars['userPriceInCurr'],$intParams['currency']);
        $result->discount = (float) $vars['discountInCurr'];
        $result->delivery = (float) $vars['userDeliveryInCurr'];
//-----------------------------------------------------------
        $result->params = $intParams;
        $result->vars = $vars;
        return $result;
    }

    public static function getUserPriceForArray($searchRes)
    {
        if (isset($searchRes->items)) {
            foreach ($searchRes->items as $k => $item) {
//===============================================
                if (DSConfig::getVal('taobao_EnabelDiscounts') == 0) {
                    foreach ($searchRes->items as $m => $ResItem) {
                        $searchRes->items[$m]->promotion_price = $searchRes->items[$m]->price;
                    }
                }
                if ($item->price != 0) {
                    $item->promotion_percent = ceil((100 - ($item->promotion_price / $item->price) * 100) / 5) * 5;
                } else {
                    $item->promotion_percent = 0;
                }
//===============================================
                $item->price = (float) $item->price;
                $item->promotion_price = (float) $item->promotion_price;
                $item->post_fee = (float) $item->post_fee;
                $item->ems_fee = (float) $item->ems_fee;
                $item->express_fee = (float) $item->express_fee;
                $resUserPrice = self::getUserPrice(
                  array(
                    'price'       => $item->price,
                    'count'       => 1,
                    'deliveryFee' => $item->express_fee,
                    'postageId'   => false,
                    'sellerNick'  => false,
                  )
                );
                $item->userPrice = $resUserPrice->price;
                $resUserPrice = self::getUserPrice(
                  array(
                    'price'       => $item->promotion_price,
                    'count'       => 1,
                    'deliveryFee' => $item->express_fee,
                    'postageId'   => false,
                    'sellerNick'  => false,
                  )
                );
                $item->userPromotionPrice = $resUserPrice->price;
                $searchRes->items[$k] = $item;
            }
        }
    }

    public static function getTaobaoPrice(array $params)
    {
        $intParams = array(
          'price' => new CException('getTaobaoPrice: no price defined!'),
        );
// Old parameters
        foreach ($intParams as $key => $default) {
            $intParams[$key] = isset($params[$key]) ? $params[$key] : $default;
            if (is_a($intParams[$key], 'CException')) {
                throw $intParams[$key];
            }
        }
// === Variables =============
        $vars = array();
        $vars['currency'] = DSConfig::getCurrency();
        $vars['mainK'] = DSConfig::getVal('price_main_k');
        $vars['postFee'] = 0; //DSConfig::getVal('delivery_default_chinese_fee_cny');
        $vars['priceWithPostageAndCount'] = Formulas::convertCurrency(
          $intParams['price'] - $vars['postFee'],
          $vars['currency'],
          'cny'
        );
        $vars['discountCountK'] = self::getPriceK($vars['priceWithPostageAndCount']);
        if (($vars['mainK'] * $vars['discountCountK']) != 0) {
            $vars['userPriceInCNY'] = Formulas::cRound(
              ($vars['priceWithPostageAndCount']) / ($vars['mainK'] * $vars['discountCountK']),
              'cny',
              0
            );
        } else {
            $vars['userPriceInCNY'] = Formulas::cRound($vars['priceWithPostageAndCount'], 'cny', 0);
        }
        if ($vars['userPriceInCNY'] < 0) {
            return 0;
        } else {
            return $vars['userPriceInCNY'];
        }
    }

    public static function GetPriceTable($price, $delivery, $postageId)
    {
        $prices = new stdClass();
        $prices->pricesX = array();
        $prices->pricesYprice = array();
        $prices->pricesYeconomy = array();
        for ($i = 1; $i < 21; $i++) {
            $prices->pricesX[] = $i;
            $resUserPrice = self::getUserPrice(
              array(
                'price'       => $price,
                'count'       => $i,
                'deliveryFee' => $delivery,
                'postageId'   => $postageId,
                'sellerNick'  => false,
              )
            );
            $p = $resUserPrice->price;
            $prices->pricesYeconomy[] = $resUserPrice->discount;
            $prices->pricesYprice[] = self::cRound($p / $i);
        }
        $arr = array(21, 51, 101);
        foreach ($arr as $i) {
            $prices->pricesX[] = $i;
            $resUserPrice = self::getUserPrice(
              array(
                'price'       => $price,
                'count'       => $i,
                'deliveryFee' => $delivery,
                'postageId'   => $postageId,
                'sellerNick'  => false,
              )
            );
            $p = $resUserPrice->price;
            $prices->pricesYeconomy[] = $resUserPrice->discount;
            $prices->pricesYprice[] = self::cRound($p / $i);
        }
        return $prices;
    }

    public static function priceWrapper($price, $currency = false, $digitsNum = false)
    {
        if (!$currency) {
            $currency = DSConfig::getCurrency();
        }
        if (!strrpos($price, '-')) {
            $html = '<span>' . self::cRound($price, $currency, $digitsNum) . '</span>';
        } else {
            $html = '<span>' . $price . '</span>';
        }
        $symb = DSConfig::getCurrency(true, true, $currency); //DSConfig::getVal('site_currency')
        $symb = str_replace('%d', $html, $symb);
        return $symb;
    }

    public static function getCurrencyRate($currency,$forDate) {
        if (!$forDate || (DSConfig::getVal('rates_use_currency_log')!=1)) {
            $result = (float) DSConfig::getVal('rate_' . $currency);
        } else {
            $iCurrency = $currency;
            $iForDate = $forDate;
            $res=Yii::app()->db->createCommand("select rate from currency_log ll
                                                where ll.currency=:currency and ll.date<=FROM_UNIXTIME(:forDate)
                                                order by ll.date desc limit 1")
              ->bindParam(':currency',$iCurrency,PDO::PARAM_STR)
              ->bindParam(':forDate',$iForDate,PDO::PARAM_INT)
              ->queryScalar();
            if (!$res) {
                $res=Yii::app()->db->createCommand("select rate from currency_log ll
                                                where ll.currency=:currency order by ll.date asc limit 1")
                  ->bindParam(':currency',$iCurrency,PDO::PARAM_STR)
                  ->queryScalar();
            }
            if (!$res) {
                $result=(float) DSConfig::getVal('rate_' . $currency);
            } else {
                $result=(float)$res;
            }

        }
        return $result;
    }

    public static function convertCurrency($srcVal, $srcCurr, $destCurr, $digitsNum = false, $forDate=false)
    {
        //ini_set('precision', 20);
        if (DSConfig::getVal('rates_auto_update') && ((time() - DSConfig::getVal(
                'rates_auto_update_last_time'
              )) > 60 * 60*4)
        ) {
            Utils::getCurrencyRatesFromBank();
        }

        if ($srcCurr == $destCurr) {
            $res = self::cRound($srcVal, $destCurr, $digitsNum);
            return $res;
        }
        if ((self::$_srcCurr != $srcCurr) && !$forDate) {
            $srcCurrRate = self::getCurrencyRate($srcCurr,$forDate);
                self::$_srcCurr = $srcCurr;
                self::$_srcCurrRate = $srcCurrRate;
        } else {
            $srcCurrRate = (($forDate)?self::getCurrencyRate($srcCurr,$forDate):self::$_srcCurrRate);
        }
        if ((self::$_destCurr != $destCurr) && !$forDate) {
            $destCurrRate = self::getCurrencyRate($destCurr,$forDate);
                self::$_destCurr = $destCurr;
                self::$_destCurrRate = $destCurrRate;
        } else {
            $destCurrRate = (($forDate)?self::getCurrencyRate($destCurr,$forDate):self::$_destCurrRate);
        }
        if ($destCurrRate != 0) {
            $res = self::cRound(($srcCurrRate / $destCurrRate) * $srcVal, $destCurr, $digitsNum);
        } else {
            $res = self::cRound(($srcCurrRate / 1) * $srcVal, $destCurr, $digitsNum);
        }
        return $res;
    }

    public static function cRound($val, $currency = false, $digitsNum = false)
    {
        if (!$currency) {
            $currency = DSConfig::getCurrency();
        }
        $res = $val;
        if (eval(self::getFormula('CRound')) === false) {
            throw new CException('Ошибки в формуле CRound');
        }
        return $res;
    }

    public static function calculateOrderItem($item)
    {

        // === Variables =============
        $vars = array();
        if (($item->actual_num <= 0) || ($item->actual_num == null)) {
            $item->actual_num = $item->num;
        }

        /* // == перерасчёт ручной доставки ==
        //actual_lot_express_fee
          if (($item->express_fee==0)) {
            $vars['postage']= new stdClass();
            $intParams = array('count'=>$item->num);
        //    $vars['postFee'] = $vars['postage']->start_fee + $vars['postage']->add_fee * ($intParams['count'] - 1);
            $vars['postage']->start_fee = DSConfig::getVal('delivery_default_chinese_fee_cny');
            $vars['postage']->add_fee = DSConfig::getVal('delivery_default_chinese_add_fee_cny');
            $vars['postage']->add_standard = 1;
        //----------------------
          if (eval(self::getFormula('getUserPrice[postFee]')) === FALSE) {
            throw new CException('Ошибки в формуле getUserPrice[postFee]');
          } else {
            $item->express_fee=$vars['postFee'];
          }
            unset($vars['postage']);
            unset($intParams);
            unset($vars['postFee']);
          }
        // ================================ */

        $vars['num'] = new FormulasVar($item->num, array(), Yii::t(
          'main',
          'Заказанное количество штук товаров в лоте'
        ));
        $vars['actual_num'] = new FormulasVar($item->actual_num, $vars, Yii::t(
          'main',
          'Фактическое количество штук товаров в лоте'
        ));
//???
        $vars['calculated_lotPrice'] = new FormulasVar(min(
            $item->taobao_promotion_price,
            $item->taobao_price
          ) * $vars['num']->val, $vars,
          Yii::t('main', 'Стоимость закупки лота в заказе, в юанях'));
        $vars['calculated_lotExpressFee'] = new FormulasVar($item->express_fee * $vars['num']->val, $vars,
          Yii::t('main', 'Стоимость доставки лота в заказе, в юанях'));
        $vars['calculated_lotWeight'] = new FormulasVar($item->weight * $vars['num']->val, $vars,
          Yii::t('main', 'Вес лота в заказе'));

        $vars['calculated_actualPrice'] = new FormulasVar(($vars['actual_num']->val > 0) ? Formulas::cRound(
            $item->actual_lot_price / $vars['actual_num']->val,
            false,
            2
          ) : 0, $vars,
          Yii::t('main', 'Расчетная стоимость одного товара лота'));
        $vars['calculated_actualWeight'] = new FormulasVar(($vars['actual_num']->val > 0) ? round(
            $item->actual_lot_weight / $item->actual_num
          ) : 0, $vars,
          Yii::t('main', 'Расчетный вес одного товара лота'));
        $vars['calculated_actualExpressFee'] = new FormulasVar(($vars['actual_num']->val > 0) ? Formulas::cRound(
            $item->actual_lot_express_fee / $vars['actual_num']->val,
            false,
            2
          ) : 0, $vars,
          Yii::t('main', 'Расчетная стоимость доставки одного товара лота'));

        $vars['billing_use_operator_account'] = new FormulasVar(DSConfig::getVal(
            'billing_use_operator_account'
          ) == 1, array(),
          Yii::t('main', 'Начислять ли бонусы оператору'));
        if ($vars['billing_use_operator_account']->val) {
            $vars['billing_operator_profit_model'] = new FormulasVar(DSConfig::getVal(
              'billing_operator_profit_model'
            ), array(), Yii::t('main', 'Параметры начисления бонусов'));
            parse_str($vars['billing_operator_profit_model']->val, $profitModel);
            //FROM_PURCHASE - от разницы на закупке лотов, FROM_TOTAL - от общей прибыли с заказа.
            if (isset($profitModel['FROM_PURCHASE']) && (!in_array(
                $item->status,
                OrdersItemsStatuses::getOrderItemExcludedStatusesArray()
              )) && ($item->actual_lot_price)
            ) {
                $vars['basePrice'] = new FormulasVar((min(
                      $item->taobao_promotion_price,
                      $item->taobao_price
                    ) * $vars['actual_num']->val) +
                  ($item->express_fee * $vars['actual_num']->val), $vars, Yii::t(
                    'main',
                    'Расчетная стоимость закупки лота с доставкой по Китаю'
                  ));
                $vars['realPrice'] = new FormulasVar($item->actual_lot_price + $item->actual_lot_express_fee, $vars, Yii::t(
                  'main',
                  'Фактическая стоимость закупки лота с доставкой по Китаю'
                ));
                $vars['delta'] = new FormulasVar($vars['basePrice']->val - $vars['realPrice']->val, $vars, Yii::t(
                  'main',
                  'Экономия на закупке'
                ));
                $vars['profit'] = new FormulasVar($vars['delta']->val * (($vars['basePrice']->val > $vars['realPrice']->val) ? (float) $profitModel['FROM_PURCHASE'] : 1), $vars,
                  Yii::t('main', 'Профит оператора в юанях'));
                $vars['calculated_operatorProfit'] = new FormulasVar(Formulas::convertCurrency(
                  $vars['profit']->val,
                  'cny',
                  DSConfig::getVal('site_currency')
                ), $vars, Yii::t('main', 'Профит оператора'));
            }
        }
        return $vars;
    }

    public static function calculateOrder($order)
    {
// === Variables =============
        try {
            $vars = array();
            $vars['orderId'] = new FormulasVar($order->id, array(), Yii::t('main', 'Идентификатор заказа (PK)'));
            $vars['items_count'] = new FormulasVar(
              Yii::app()->db->createCommand(
                'select ifnull(sum(oi.num),0)
                  from orders_items oi where oi.oid=:oid and oi.status not in (select ois.id from orders_items_statuses ois where ois.excluded=1)'
              )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t(
                'main',
                'Общее количество заказанных штук товаров во всех лотах заказа (кроме отмененных лотов)'
              )
            );
            $vars['actual_items_count'] = new FormulasVar(
              Yii::app()->db->createCommand(
                'select ifnull(sum(ifnull(oi.actual_num,oi.num)),0)
                                                                           from orders_items oi where oi.oid=:oid and oi.status not in (select ois.id from orders_items_statuses ois where ois.excluded=1)'
              )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t(
                'main',
                'Общее количество фактически штук товаров во всех лотах заказа (кроме отмененных лотов)'
              )
            );

            $vars['taobao_total'] = new FormulasVar(
              Yii::app()->db->createCommand(
                'select ifnull(
          sum(LEAST(oi.taobao_price, oi.taobao_promotion_price)*oi.num + oi.express_fee),0)
            from orders_items oi where oi.oid=:oid and oi.status not in (select ois.id from orders_items_statuses ois where ois.excluded=1)'
              )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t(
                'main',
                'Общая стоимость закупки всех лотов на таобао при заказе, в юанях (кроме отмененных лотов)'
              )
            );
            if ($vars['taobao_total']) {
                $vars['taobao_total']->val = self::cRound($vars['taobao_total']->val, false, 2);
            }
            $vars['taobao_total_curr'] = new FormulasVar(
              Formulas::convertCurrency(
                $vars['taobao_total']->val,
                'cny',
                DSConfig::getVal('site_currency'),false,$order->date
              ), array(), Yii::t(
                'main',
                'Общая стоимость закупки всех лотов на таобао при заказе, в валюте сайта (кроме отмененных лотов)'
              )
            );
            $vars['actual_taobao_total'] = new FormulasVar(
              Yii::app()->db->createCommand(
                'SELECT
           round(
                     sum(
          CASE
           when oi.actual_lot_price is null then (LEAST(oi.taobao_price, oi.taobao_promotion_price)* IFNULL(oi.actual_num, oi.num)
                                                   + IFNULL(oi.actual_lot_express_fee, oi.express_fee))

           else oi.actual_lot_price + IFNULL(oi.actual_lot_express_fee, oi.express_fee)
           END
                       )
                   ,2)
            FROM orders_items oi
           WHERE oi.oid = :oid AND oi.status NOT IN (select ois.id from orders_items_statuses ois where ois.excluded=1)'
              )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t(
                'main',
                'Фактическая стоимость закупки всех лотов на таобао, в юанях (кроме отмененных лотов)'
              )
            );

            $vars['actual_taobao_express_fee'] = new FormulasVar(
              Yii::app()->db->createCommand(
                'SELECT
           round(
                     sum(IFNULL(oi.actual_lot_express_fee, oi.express_fee))
                   ,2)
            FROM orders_items oi
           WHERE oi.oid = :oid AND oi.status NOT IN (select ois.id from orders_items_statuses ois where ois.excluded=1)'
              )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t(
                'main',
                'Фактическая стоимость доставки всех лотов по Китаю, в юанях (кроме отмененных лотов)'
              )
            );

            $vars['actual_taobao_price'] = new FormulasVar(
              Yii::app()->db->createCommand(
                'SELECT
           round(
                     sum(
          CASE
           when oi.actual_lot_price is null then (LEAST(oi.taobao_price, oi.taobao_promotion_price)* IFNULL(oi.actual_num, oi.num))

           else oi.actual_lot_price
           END
                       )
                   ,2)
            FROM orders_items oi
           WHERE oi.oid = :oid AND oi.status NOT IN (select ois.id from orders_items_statuses ois where ois.excluded=1)'
              )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t(
                'main',
                'Фактическая стоимость товаров всех лотов на таобао, в юанях (кроме отмененных лотов)'
              )
            );

            if ($vars['actual_taobao_total']) {
                $vars['actual_taobao_total']->val = self::cRound($vars['actual_taobao_total']->val, false, 2);
            }
            $vars['actual_taobao_total_curr'] = new FormulasVar(
              Formulas::convertCurrency(
                $vars['actual_taobao_total']->val,
                'cny',
                DSConfig::getVal('site_currency'),false,$order->date
              ), array(), Yii::t(
                'main',
                'Фактическая стоимость закупки всех лотов на таобао при заказе, в валюте сайта (кроме отмененных лотов)'
              )
            );

            $vars['actual_lots_weight'] = new FormulasVar(
              Yii::app()->db
                ->createCommand(
                  'SELECT
             round(
                       sum(
            CASE
             when oi.actual_lot_weight is null then oi.weight * IFNULL(oi.actual_num, oi.num)
             else oi.actual_lot_weight
             END
                         )
                     ,0)
              FROM orders_items oi
             WHERE oi.oid = :oid AND oi.status NOT IN (select ois.id from orders_items_statuses ois where ois.excluded=1)'
                )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t(
                'main',
                'Фактический вес всех лотов заказа, в граммах (кроме отмененных лотов)'
              )
            );

            $vars['sum'] = new FormulasVar($order->sum, $vars, Yii::t('main', 'Цена товаров заказа при оформлении'));
            $vars['delivery'] = new FormulasVar(
              $order->delivery, $vars, Yii::t(
                'main',
                'Цена доставки заказа при оформлении'
              )
            );
            $vars['order_total'] = new FormulasVar(
              $vars['sum']->val + $vars['delivery']->val, $vars, Yii::t(
                'main',
                'Общая цена заказа при оформлении'
              )
            );

            $vars['manual_weight'] = new FormulasVar(
              $order->manual_weight, $vars, Yii::t(
                'main',
                'Общий итоговый вес заказа, введенный вручную'
              )
            );

            $vars['usedWeight'] = new FormulasVar(
              ($vars['manual_weight']->val > 0) ? $vars['manual_weight']->val : $vars['actual_lots_weight']->val,
              $vars,
              Yii::t(
                'main',
                'Вес заказа, который будет использоваться при окончательных расчетах'
              )
            );
            $vars['delivery_country'] = new FormulasVar(
              $order->addresses['country'], array(), Yii::t(
                'main',
                'Страна доставки'
              )
            );

//    print_r($order->addresses['country']);
//     $vars['delivery_country']->val

            $vars['delivery_id'] = new FormulasVar($order->delivery_id, $vars, Yii::t('main', 'Служба доставки'));
            $delivery = Deliveries::getDelivery(
              $vars['usedWeight']->val,
              $vars['delivery_country']->val,
              $vars['delivery_id']->val,
              1
            ); //
            if (isset($delivery->summ)) {
                $vars['actual_lots_delivery'] = new FormulasVar($delivery->summ, $vars);
            } else {
                $vars['actual_lots_delivery'] = new FormulasVar('', $vars);
            }
            $vars['actual_lots_delivery']->description = Yii::t('main', 'Стоимость доставки по весу лотов');

            $orderItems = OrdersItems::model()->findAll(
              'oid=:oid and status not in (select ois.id from orders_items_statuses ois where ois.excluded=1)',
              array(':oid' => $vars['orderId']->val)
            );

            $vars['actual_lots_summ'] = new FormulasVar(0, $vars, Yii::t('main', 'Сумма цен лотов'));
            $vars['billing_use_operator_account'] = new FormulasVar(
              DSConfig::getVal(
                'billing_use_operator_account'
              ) == 1, array(), Yii::t('main', 'Начислять ли бонусы оператору')
            );
            $vars['operatorProfit'] = new FormulasVar(0, $vars, Yii::t('main', 'Профит оператора по лотам'));
// begin of items calculation
            if ($orderItems) {
                foreach ($orderItems as $orderItem) {
                    if ($vars['billing_use_operator_account']->val) {
                        $vars['operatorProfit']->val = $vars['operatorProfit']->val + $orderItem->calculated_operatorProfit;
                    }
                    /*        if ($orderItem->calculated_actualPrice) {
                              $price = $orderItem->calculated_actualPrice;
                            } else {
                              $price = 2;//min($orderItem->taobao_price, $orderItem->taobao_promotion_price);
                            }
                    */
                    $price = (($orderItem->calculated_actualPrice > 0) ? $orderItem->calculated_actualPrice : min(
                      $orderItem->taobao_price,
                      $orderItem->taobao_promotion_price
                    ));
                    $delivery = (($orderItem->calculated_actualExpressFee > 0) ? $orderItem->calculated_actualExpressFee : $orderItem->express_fee);
                    $num = (($orderItem->actual_num) ? $orderItem->actual_num : $orderItem->num);
                    $resUserPrice = self::getUserPrice(
                      array(
                        'price' => $price,
                        'count' => $num,
                        'deliveryFee' => $delivery,
                        'postageId' => 0,
                        'sellerNick' => $orderItem->seller_nick,
                        'currency' => DSConfig::getSiteCurrency(),
                        'currencyDate'=>$order->date,
                        'user' => $order->uid,
                      )
                    );
                    $vars['actual_lots_summ']->val = $vars['actual_lots_summ']->val + $resUserPrice->price;
                    $vars['actual_lots_summ']->description = $vars['actual_lots_summ']->description . '<br/>(' . $price . ' cny + ' . $delivery . ' cny) * ' . $num . ' pcs * ' . DSConfig::getVal(
                        'price_main_k'
                      ) . ' k = ' . $resUserPrice->price . ' ' . DSConfig::getSiteCurrency();
                    //.'<br/>'.print_r($orderItem,true);
//        $vars['calculated_actualPrice']+$vars['calculated_actualExpressFee']
                }
            }
// end of items calculation
/*            $vars['actual_lots_summ']->val = $vars['actual_lots_summ']->val + Formulas::cRound(
                $vars['operatorProfit']->val * DSConfig::getVal('price_main_k'),
                DSConfig::getSiteCurrency()
              );
*/
            $vars['actual_lots_total'] = new FormulasVar(
              $vars['actual_lots_summ']->val + (($vars['actual_lots_delivery']->val) ? $vars['actual_lots_delivery']->val : 0),
              $vars, Yii::t('main', 'Стоимость товаров и доставки по лотам')
            );

            $vars['manual_delivery'] = new FormulasVar(
              $order->manual_delivery, $vars, Yii::t(
                'main',
                'Стоимость доставки вручную'
              )
            );
            $vars['manual_sum'] = new FormulasVar(
              $order->manual_sum, $vars, Yii::t('main', 'Стоимость заказа вручную')
            );
            $vars['manual_total'] = new FormulasVar(
              (($vars['manual_delivery']->val || ($vars['manual_delivery']->val==='0')) ? $vars['manual_delivery']->val : $vars['delivery']->val)
              + (($vars['manual_sum']->val || ($vars['manual_sum']->val==='0')) ? $vars['manual_sum']->val : $vars['actual_lots_summ']->val),
              $vars, Yii::t('main', 'Стоимость заказа вручную')
            );
            $vars['payments_sum'] = new FormulasVar(
              Yii::app()->db->createCommand(
                'select round(ifnull(sum(opp.summ),0),2) from orders_payments opp where opp.oid=:oid'
              )
                ->bindParam(':oid', $vars['orderId']->val, PDO::PARAM_STR)
                ->queryScalar(), $vars, Yii::t('main', 'Сумма платежей по заказу')
            );

            $vars['payments_saldo'] = new FormulasVar(
              $vars['payments_sum']->val - $vars['manual_total']->val, $vars, Yii::t(
                'main',
                'Сальдо платежей по заказу'
              )
            );

            $vars['realCost'] = new FormulasVar(
              $vars['actual_taobao_total_curr']->val
              + (($vars['manual_delivery']->val) ? $vars['manual_delivery']->val : $vars['delivery']->val),
              $vars,
              Yii::t(
                'main',
                'Актуальная себестоимость заказа'
              )
            );

            $vars['showedCost'] = new FormulasVar(
              $vars['taobao_total_curr']->val
              + (($vars['manual_delivery']->val) ? $vars['manual_delivery']->val : $vars['delivery']->val),
              $vars,
              Yii::t(
                'main',
                'Отображаемая себестоимость заказа'
              )
            );
            $vars['realProfit'] = new FormulasVar(
              $vars['payments_sum']->val - $vars['realCost']->val - $vars['payments_saldo']->val, $vars,
              Yii::t('main', 'Реальное сальдо полученных платежей по заказу и его реальной себестоимости')
            );
            $vars['showedProfit'] = new FormulasVar(
              $vars['payments_sum']->val - $vars['showedCost']->val - $vars['payments_saldo']->val, $vars, Yii::t(
                'main',
                'Отображаемое сальдо полученных платежей по заказу и его реальной себестоимости'
              )
            );
            if ($vars['billing_use_operator_account']->val) {
                $vars['billing_operator_profit_model'] = new FormulasVar(
                  DSConfig::getVal(
                    'billing_operator_profit_model'
                  ), array(), Yii::t('main', 'Параметры начисления бонусов')
                );
                parse_str($vars['billing_operator_profit_model']->val, $profitModel);
                //FROM_PURCHASE - от разницы на закупке лотов, FROM_TOTAL - от общей прибыли с заказа.
                if (isset($profitModel['FROM_TOTAL'])) {
                    $vars['operatorProfit'] = new FormulasVar(
                      round(
                        $vars['operatorProfit']->val + (($vars['showedProfit']->val < 0) ? 0 : $vars['showedProfit']->val * $profitModel['FROM_TOTAL']),
                        2
                      ),
                      $vars, Yii::t('main', 'Профит оператора')
                    );
                }
            }
            $vars['siteProfit'] = new FormulasVar(
              round(
                $vars['realProfit']->val - $vars['operatorProfit']->val,
                2
              ), $vars, Yii::t('main', 'Профит сайта')
            );
        } catch (Exception $e) {
            print_r($e);die;
        }
        return $vars;
    }

    protected static function getUserK($uid)
    {
        if (!self::$_UserK) {
            $u = Users::model()->findByPk($uid);
            if (($u != false) && ($u != null)) {
                self::$_UserK = (float) $u['skidka'];
            } else {
                self::$_UserK = 1;
            }
        }
        return self::$_UserK;
    }

    protected static function getCountK($count)
    {
        $_count = (float) $count;
        $koef_discount = 1;
        if ($_count <= 1) {
            return $koef_discount;
        }
        if (self::$_skidkaK_array == false) {
            self::$_skidkaK_array = Yii::app()->db->createCommand(
              "select
              cast(SUBSTRING_INDEX(SUBSTRING_INDEX(counts.id,'_',2),'_',-1) as UNSIGNED INTEGER) as count_from,
              cast(SUBSTRING_INDEX(counts.id,'_',-1) as UNSIGNED INTEGER) as count_to,
              value
              from
              (select cc.id, cc.value from config cc
              where cc.id rlike 'skidka_[0-9]+') counts
              order by count_from, count_to"
            )
              ->queryAll();
        }
        if (is_array(self::$_skidkaK_array)) {
            foreach (self::$_skidkaK_array as $skidkaK) {
                if (($skidkaK['count_from'] < $_count) && ($_count <= $skidkaK['count_to'])) {
                    $koef_discount = $skidkaK['value'];
                    break;
                }
            }
        }
        return $koef_discount;
    }

    protected static function getPriceK($price)
    {
        $_price = (float) $price;
        $koef_price = 1;
        if (self::$_priceK_array == false) {
            self::$_priceK_array = Yii::app()->db->createCommand(
              "select
              cast(SUBSTRING_INDEX(SUBSTRING_INDEX(prices.id,'_',2),'_',-1) as UNSIGNED INTEGER) as price_from,
              cast(SUBSTRING_INDEX(prices.id,'_',-1) as UNSIGNED INTEGER) as price_to,
              value
              from
              (select cc.id, cc.value from config cc
              where cc.id rlike 'price_[0-9]+') prices
              order by price_from, price_to"
            )
              ->queryAll();
        }
        if (is_array(self::$_priceK_array)) {
            foreach (self::$_priceK_array as $priceK) {
                if (($priceK['price_from'] < $_price) && ($_price <= $priceK['price_to'])) {
                    $koef_price = $priceK['value'];
                    break;
                }
            }
        }
        return $koef_price;
    }

    protected static function getFormula($formulaId)
    {
        if (is_array(self::$_cache)) {
            if (isset(self::$_cache[$formulaId])) {
                return self::$_cache[$formulaId];
            }
        }
        $rec = self::model()
          ->findBySql('select * from formulas where formula_id=:formulaId', array(':formulaId' => $formulaId));
        if ($rec) {
            $res = $rec['formula'];
            if (!is_array(self::$_cache)) {
                self::$_cache = array();
            }
            self::$_cache[$formulaId] = $res;
            return $res;
        } else {
            throw new CException(Yii::t('main', 'Формула') . ' ' . $formulaId . ' ' . Yii::t('main', 'не найдена!'));
        }
    }
}
