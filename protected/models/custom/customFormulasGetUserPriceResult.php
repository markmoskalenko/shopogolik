<?php

/**
 * This is the model class for table "formulas".
 *
 * The followings are the available columns in table 'formulas':
 * @property integer $id
 * @property string $formula_id
 * @property string $formula
 * @property string $description
 */
class customFormulasGetUserPriceResult {
 public $price=0;
 public $discount=0;
 public $delivery=0;
 public $params=array();
 public $vars=array();
  public function report() {
  $res=print_r($this->params,true).'</br>'.print_r($this->vars,true);
    return $res;
}
}
