<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Item.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class customItem {

  public $taobao_item;
  public $top_item;
  protected $_iid;
  protected $_lang = 'ru';
  protected $fromCart = FALSE;
//  protected static $storedThis=array();

  /** @noinspection PhpInconsistentReturnPointsInspection */
  function __construct($iid = FALSE, $fromCart = FALSE) {
//      if (isset(self::$storedThis[($iid?$iid:'false').'-'.($fromCart?'true':'false')])) {
//          $cacheId=($iid?$iid:'false').'-'.($fromCart?'true':'false');
//          return unserialize(self::$storedThis[$cacheId]);
//      }
    $this->fromCart = $fromCart;
    //$debug = DSConfig::getVal('site_debug') == 1;
    if (!$fromCart) {
      Profiler::start('item->get', TRUE);
    }
    try {
      $this->_lang = Utils::TransLang();
      $this->_iid = $iid;
      //Проверяем нет ли у нас информаии по этому товару в базе ошибок
      $error = Yii::app()->db->createCommand()
        ->select('iid')
        ->from('top_errors_items')
        ->where('iid=:iid and date>:date', array(':iid' => $iid, ':date' => time() - 60 * 60 * 24))
        ->queryScalar();
      if ((int) $error > 0) {
        if (!$fromCart) {
          Profiler::stop('item->get');
        }
        return self::itemError($iid);
      }
      //Получаем данные из кэша о товаре НЕ с карты товара
      if (!$fromCart) {
        $cache = Yii::app()->fileCache->get('item-' . $this->_lang . '-' . $iid);
        if (($cache === FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
          //Данных в кэше не найдено
          $this->getItemData($iid);
          if (isset($this->top_item) && isset($this->top_item->cid)) {
            if ($this->top_item->cid > 0) {
              Yii::app()->fileCache->set('item-' . $this->_lang . '-' . $iid, array(
                $this->taobao_item,
                $this->top_item,
              ), 3600 * (int) DSConfig::getVal('search_cache_ttl_item'));
            }
          }
        }
        else {
          list($this->taobao_item, $this->top_item) = $cache;
        }
        if ((!isset($this->top_item->cid)) || ($this->top_item->cid <= 0)) {
          Yii::app()->fileCache->delete('item-' . $this->_lang . '-' . $iid);
        }
      }
      else {
        //Получаем данные из кэша о товаре с корзины

        $cache = Yii::app()->cache->get('item-cart-' . $this->_lang . '-' . $iid);
        if (($cache === FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
          //Данных в кэше не найдено
          $this->getItemData($iid);
          if (isset($this->top_item) && isset($this->top_item->cid)) {
            if ($this->top_item->cid > 0) {
              Yii::app()->cache->set('item-cart-' . $this->_lang . '-' . $iid, array(
                $this->taobao_item,
                $this->top_item,
                array()
              ), 3600 * (int) DSConfig::getVal('search_cache_ttl_item'));
            }
          }
          else {
            // если товар отстутствует - кэшить, но как-то иначе.
            /*            if ($this->top_item->cid > 0) {
                          Yii::app()->fileCache->set('item-card-' . $this->_lang . '-' . $iid, array(
                            $this->taobao_item,
                            $this->top_item,
                            array()
                          ), 3600 * 10 * (int) DSConfig::getVal('search_cache_ttl_item'));
                        }
                        */
          }
        }
        else {
          list($this->taobao_item, $this->top_item) = $cache;
          if ($this->top_item->cid <= 0) {
            Yii::app()->cache->delete('item-cart-' . $this->_lang . '-' . $iid);
          }
        }
      }
      if (!isset($this->taobao_item) || !isset($this->top_item)) {
        if (!$fromCart) {
          Profiler::stop('item->get');
        }
        return self::itemError($iid);
      }
//=============================== Postprocess express_fee
        if (DSConfig::getVal('delivery_ignore_delivery_cost_from_taobao') == 1) {
            if (isset($this->top_item->express_fee)) {
                $this->top_item->express_fee = DSConfig::getVal('delivery_default_chinese_fee_cny');
            }
            if (isset($this->taobao_item->express_fee)) {
                $this->taobao_item->express_fee = DSConfig::getVal('delivery_default_chinese_fee_cny');
            }
        }
        if (DSConfig::getVal('delivery_minimal_chinese_fee_cny')) {
            if (isset($this->top_item->express_fee)) {
                $this->top_item->express_fee = max($this->top_item->express_fee,DSConfig::getVal('delivery_minimal_chinese_fee_cny'));
            }
            if (isset($this->taobao_item->express_fee)) {
                $this->taobao_item->express_fee = max($this->taobao_item->express_fee,DSConfig::getVal('delivery_minimal_chinese_fee_cny'));
            }
        }

//===============================
// --- получение категории и пути категории товара
      if (!$fromCart) {
        $this->top_item->cat_path = Category::getPath($this->top_item->cid);
      }
//--- Перевод title, city, area - не должен кэшироваться
      if (!$this->fromCart) {
        Profiler::start('item->fillTop_item->translateTitle');
      }
      if (Utils::AppLang() != 'he') {
        $this->top_item->title = Yii::app()->DanVitTranslator->translateText($this->taobao_item->title, 'zh-CHS', $this->_lang, TRUE);
      }
      else {
        $this->top_item->title = Yii::app()->BingTranslator->translateText($this->taobao_item->title, 'zh-CHS', 'he', TRUE);
      }
      if (isset($this->top_item->state)) {
        $this->top_item->state = Yii::app()->DanVitTranslator->translateText($this->top_item->state, 'zh-CHS', $this->_lang);
      }
      $this->top_item->city = Yii::app()->DanVitTranslator->translateText($this->top_item->city, 'zh-CHS', $this->_lang);

      if (!$this->fromCart) {
        Profiler::stop('item->fillTop_item->translateTitle');
      }
//-- Перевод ТОРГОВЫХ свойств - не должен кэшироваться
      //=======================================================
      if (isset($this->top_item->props)) {
        if (!$this->fromCart) {
          Profiler::start('search->prepareProps->translate');
        }
        Yii::app()->DanVitTranslator->translateArray($this->top_item->props, 'prepareProps', 'zh-CHS', $this->_lang);

        foreach ($this->top_item->props as $k => $prop) {
          $prop->value = array();
          foreach ($prop->childs as $child) {
            if (isset($child->name)) {
              $prop->value[] = $child->name;
            }
          }
          if (isset($prop->value[0])) {
            $this->top_item->props[$k]->value = implode(', ', $prop->value);
          }
          else {
            unset($this->top_item->props[$k]);
          }
        }
//==== Сортировка ============================================
        if (!function_exists('orderItemProps')) {
          function orderItemProps($a, $b) {
            $a_val = 0;
            $b_val = 0;
            if (isset($a->childs)) {
              foreach ($a->childs as $child) {
                if (isset($child->url)) {
                  if (mb_strlen($child->url) > 0) {
                    $a_val = 1;
                    break;
                  }
                }
              }
            }

            if (isset($b->childs)) {
              foreach ($b->childs as $child) {
                if (isset($child->url)) {
                  if (mb_strlen($child->url) > 0) {
                    $b_val = 1;
                    break;
                  }
                }
              }
            }
            if ($a_val > $b_val) {
              return 1;
            }
            elseif ($a_val == $b_val) {
              return 0;
            }
            else {
              return -1;
            }
          }
        }
        if (!$this->fromCart) {
          if (isset($this->top_item->props)) {
            uasort($this->top_item->props, "orderItemProps");
          }
        }
//===========================================================
        if (!$this->fromCart) {
          Profiler::stop('search->prepareProps->translate');
        }
      }
//-- Перевод атрибутов - ОПИСАНИЯ ТОВАРА, ТОЛЬКО ДЛЯ DSG - не должен кэшироваться
      //=======================================================
      if (isset($this->taobao_item->fromDSG)) {
        if (!$this->fromCart) {
          Profiler::start('item->attrs->translate');
        }
        if (isset($this->top_item->item_attributes)) {
          Yii::app()->DanVitTranslator->translateArray($this->top_item->item_attributes, 'item_attributes', 'zh-CHS', $this->_lang);
//==== Сортировка ============================================
          /*           if (!function_exists('orderAttrs')) {
                      function orderAttrs($a, $b) {
                        $a_val = mb_strlen(Utils::removeOnlineTranslation($a->prop),'UTF-8')+mb_strlen(Utils::removeOnlineTranslation($a->val),'UTF-8');
                        $b_val = mb_strlen(Utils::removeOnlineTranslation($b->prop),'UTF-8')+mb_strlen(Utils::removeOnlineTranslation($b->val),'UTF-8');
                        if ($a_val < $b_val) {
                          return -1;
                        }
                        elseif ($a_val == $b_val) {
                          return 0;
                        }
                        else {
                          return 1;
                        }
                      }
                    }
                   if (!$this->fromCart) {
                    if (isset($this->top_item->item_attributes)) {
                      uasort($this->top_item->item_attributes, "orderAttrs");
                    }
                    }
          */
//===========================================================
        }
        if (!$this->fromCart) {
          Profiler::stop('item->attrs->translate');
        }
      }
//=================================================================================
//--- Расчёт цен - не должен кэшироваться!
//=================================================================================
//=================================================================================
// Собственно расчёт цен
//=================================================================================
      if (!$fromCart) {
        Profiler::start('item->calcPrice');
      }
      if ((DSConfig::getVal('taobao_EnabelDiscounts') == 1) &&
        ((DSConfig::getVal('search_use_safe_price_ranges') == 1)
        && ($this->top_item->promotion_price)
          && ($this->top_item->price)
          && ($this->top_item->promotion_price/$this->top_item->price)>(float)DSConfig::getVal('search_use_safe_price_minimal_promotion_percent'))
      ) {
        if (isset($this->top_item->promotion_price)) {
          $max_promotion_price = $min_promotion_price = $this->top_item->promotion_price;
        }
        else {
//        return false;
          $max_promotion_price = $min_promotion_price = $this->top_item->price;
        }

      }
      else {
        $max_promotion_price = $min_promotion_price = $this->top_item->price;
        $this->top_item->promotion_price = $this->top_item->price;
      }
      $max_price = $min_price = $this->top_item->price;
      if (isset($this->top_item->skus)) {
        $PriceArray = Array();
        $PromotionPriceArray = Array();
        foreach ($this->top_item->skus->sku as $sku) {
          if ((DSConfig::getVal('taobao_EnabelDiscounts') != 1) ||
            ((DSConfig::getVal('search_use_safe_price_ranges') == 1)
              && ($sku->promotion_price/$sku->price)<(float)DSConfig::getVal('search_use_safe_price_minimal_promotion_percent'))
          )
          {
            $sku->promotion_price = $sku->price;
          }
          if (isset($sku->promotion_price)) {
            $PromotionPriceArray[] = (float) $sku->promotion_price;
            $PriceArray[] = (float) $sku->price;
          }
        }
        if ((!empty($PromotionPriceArray)) && (!empty($PriceArray))) {
          $min_promotion_price = min($PromotionPriceArray);
          $max_promotion_price = max($PromotionPriceArray);
          $min_price = min($PriceArray);
          $max_price = max($PriceArray);
        }
      };
      $this->top_item->price=$max_price;
      $this->top_item->promotion_price=$min_promotion_price;
      if ($min_promotion_price == $max_promotion_price) {
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$min_promotion_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
            //'asHtml' => TRUE,
          ));
        $this->top_item->userPromotionPrice=$resUserPrice->price;
        $this->top_item->userPromotionPriceNoDelivery = $resUserPrice->price-$resUserPrice->delivery;
        $this->top_item->userPromotionDelivery = $resUserPrice->delivery;
        $this->top_item->userPromotionResUserPrice = $resUserPrice;
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$min_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
          ));
        $this->top_item->userPrice=$resUserPrice->price;
        $this->top_item->userPriceNoDelivery = $resUserPrice->price-$resUserPrice->delivery;
        $this->top_item->userDelivery = $resUserPrice->delivery;
        $this->top_item->userResUserPrice = $resUserPrice;
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$this->top_item->promotion_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
          ));
        $this->top_item->userPriceFinal=$resUserPrice->price;
      }
      else {
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$min_promotion_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
          ));
        $this->top_item->userPromotionPrice1=$resUserPrice->price;
        $this->top_item->userPromotionPrice1NoDelivery = $resUserPrice->price-$resUserPrice->delivery;
        $this->top_item->userPromotionDelivery1 = $resUserPrice->delivery;
        $this->top_item->userPromotionResUserPrice1 = $resUserPrice;
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$max_promotion_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
          ));
        $this->top_item->userPromotionPrice2=$resUserPrice->price;
        $this->top_item->userPromotionPrice2NoDelivery = $resUserPrice->price-$resUserPrice->delivery;
        $this->top_item->userPromotionDelivery2 = $resUserPrice->delivery;
        $this->top_item->userPromotionResUserPrice2 = $resUserPrice;
//        $this->top_item->userPromotionPrice = Formulas::priceWrapper($userPromotionPrice1 . '-' . $userPromotionPrice2);

        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$min_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
          ));
        $this->top_item->userPrice1=$resUserPrice->price;
        $this->top_item->userPrice1NoDelivery = $resUserPrice->price-$resUserPrice->delivery;
        $this->top_item->userDelivery1 = $resUserPrice->delivery;
        $this->top_item->userResUserPrice1 = $resUserPrice;
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$max_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
          ));
        $this->top_item->userPrice2=$resUserPrice->price;
        $this->top_item->userPrice2NoDelivery = $resUserPrice->price-$resUserPrice->delivery;
        $this->top_item->userDelivery2 = $resUserPrice->delivery;
        $this->top_item->userResUserPrice1 = $resUserPrice;
//        $this->top_item->userPrice = Formulas::priceWrapper($userPrice1 . '-' . $userPrice2); //$this->priceWrapper(
        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$this->top_item->promotion_price,
            'count'=>1,
            'deliveryFee'=>$this->taobao_item->express_fee,
            'postageId'=>$this->taobao_item->postage_id,
            'sellerNick'=>$this->taobao_item->nick,
          ));
        $this->top_item->userPriceFinal=$resUserPrice->price;
      }
      if (!$fromCart) {
        $this->top_item->priceTable = Formulas::GetPriceTable($this->taobao_item->promotion_price,
          (float) $this->taobao_item->express_fee,
          $this->taobao_item->postage_id);
      }
      if (!$fromCart) {
        Profiler::stop('item->calcPrice');
      }
    } catch (Exception $e) {
     //LogSiteErrors::logError($e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
       return null;
      //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
      //echo $e->getMessage();
    }
// ===== Расчёт примерного веса ===========================
    if (!$fromCart) {
      $this->top_item->weight_calculated = Weights::getItemWeight($this->top_item->cid, $this->top_item->num_iid);
    }
// ========================================================
    if (!$fromCart) {
      Profiler::stop('item->get', TRUE);
    }
//      $cacheId=($iid?$iid:'false').'-'.($fromCart?'true':'false');
//      self::$storedThis[$cacheId]=serialize($this);
  }

  public static function getPriceAndDeliveryFromCache($iid) {
    $lang = Utils::TransLang();
    try {
      //Получаем данные из кэша о товаре не с карты товара
      $cache = Yii::app()->fileCache->get('item-' . $lang . '-' . $iid);
      if (($cache === FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
        return FALSE;
        //Данных в кэше не найдено
      }
      else {
        list($taobao_item, $top_item) = $cache;
      }
      if ($top_item->cid <= 0) {
        Yii::app()->fileCache->delete('item-' . $lang . '-' . $iid);
      }

      if (!isset($taobao_item) || !isset($top_item)) {
        return FALSE;
      }
    } catch (Exception $e) {
      LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
      //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
      //echo $e->getMessage();
      return FALSE;
    }
    $res = new stdClass();
    $res->taobao_item = new stdClass();
    $res->taobao_item->price = $taobao_item->price;
    $res->taobao_item->express_fee = $taobao_item->express_fee;
    $res->taobao_item->postage_id = $taobao_item->postage_id;
    $res->taobao_item->nick = $taobao_item->nick;
    return $res;
  }

  protected function itemError($iid) {
    if (Yii::app()->controller->id == 'item') {
      return array(FALSE, FALSE);
//      throw new CHttpException(404, Yii::t('main','Данный товар не найден. Возможно он был удален продавцом'));
    }
    elseif (Yii::app()->controller->id == 'favorite') {
      Favorite::model()->deleteAll('num_iid=:num_iid', array(':num_iid' => $iid));
      return array(FALSE, FALSE);
    }
    else {
      return array(FALSE, FALSE);
    }
  }

  protected function getItemData($iid = FALSE) {
    //Основные данные о товаре
    $debug = DSConfig::getVal('site_debug') == 1;
    if (!$this->fromCart || $debug) {
      Profiler::start('item->getItemData');
    }
      $search_DropShop_grabbers_debug = DSConfig::getVal('search_DropShop_grabbers_debug') == 1;
      $req = new DSGItem($search_DropShop_grabbers_debug);
      $req->id = $iid;
      $req->fromCart = $this->fromCart;
      if (!$this->fromCart || $debug) {
        Profiler::start('item->getItemData->ItemGetRequest');
      }
      $item_resp = $req->execute('item.taobao.com');
//=================== Обрабатываем скидки ==============================
//=== пробуем получить скидки из лога поиска =====
      /*      $promotions = $this->getPromotionDataFromLog($iid, $this->fromCart || $item_resp->item->isTmall);
            if ($promotions) {
      //        $promotion_ratio=(float)$promotions['promotion_price']/(float)$promotions['price'];
              if ($item_resp->item->price != 0) {
                $promotion_ratio = (float) $promotions['promotion_price'] / $item_resp->item->price;
              }
              else {
                $promotion_ratio = 1;
              }
              $item_resp->item->price = (float) $item_resp->item->price;
              $item_resp->item->promotion_price = round($item_resp->item->price * $promotion_ratio, 2);

      //        $this->taobao_item->promotion_price = (float) $promotion->item_promo_price;

              if (isset($item_resp->item->skus->sku)) {
                foreach ($item_resp->item->skus->sku as $sku) {
                  $sku->price = (float) $sku->price;
                  if ((float) $sku->promotion_price >= $sku->price) {
                    $sku->promotion_price = round($sku->price * $promotion_ratio, 2);;
                  }
                }
              }
            }
            else */
      if (isset($item_resp->item->fromDSG)) {
        $item_resp->item->price = (float) $item_resp->item->price;
        if (isset($item_resp->item->promotion_price)) {
          $item_resp->item->promotion_price = (float) $item_resp->item->promotion_price;
        }
        else {
          $item_resp->item->promotion_price = (float) $item_resp->item->price;
        }
      }
      else {
        $this->itemError($iid);
        if (!$this->fromCart || $debug) {
          Profiler::stop('item->getItemData->ItemGetRequest');
        }
        return FALSE;
      }
      unset($promotion);
      unset($promotions);
// конец обработки скидок
//======================================================================
      if (!$this->fromCart || $debug) {
        Profiler::stop('item->getItemData->ItemGetRequest');
      }
    if (!isset($item_resp->item)) {
      $this->itemError($iid);
      if (!$this->fromCart || $debug) {
        Profiler::stop('item->getItemData');
      }
      return FALSE;
    }
    else {
      $this->taobao_item = clone $item_resp->item;
      if (!$this->fromCart || $debug) {
        Profiler::stop('item->getPromotionData->calc');
      }
      $this->fillTop_item();
    }
    if (!$this->fromCart || $debug) {
      Profiler::stop('item->getItemData');
    }
    if (isset($search_DropShop_grabbers_debug) && $search_DropShop_grabbers_debug) {
      $this->top_item->debugMessages = new CArrayDataProvider($item_resp->debugMessages, array(
        'id'         => 'id',
        'pagination' => array(
          'pageSize' => 150,
        ),
      ));
    }
    unset($item_resp);
    return array($this->taobao_item, $this->top_item);
  }

  protected function getPromotionDataFromLog($iid = FALSE, $fromCart = FALSE) {
    if (!$iid) {
      return FALSE;
    }
    $intIid = $iid;
    $secsAgo = -3600 * (int) DSConfig::getVal('search_cache_ttl_item');
    if ($fromCart) {
      $expired = 1;
    }
    else {
      $expired = 1;
    }
    $sql = "select t.express_fee,t.price,t.promotion_price
    from log_items_requests t
    where t.num_iid = :iid
    And ((t.`date`>CAST(DATE_ADD(NOW(), INTERVAL :SECS SECOND) AS DATETIME)) or (:expired=1))
    order by t.`date` DESC limit 1";
    $command = Yii::app()->db->createCommand($sql)
      ->bindParam(':iid', $intIid, PDO::PARAM_STR)
      ->bindParam(':SECS', $secsAgo, PDO::PARAM_INT)
      ->bindParam(':expired', $expired, PDO::PARAM_INT);

    $row = $command->queryRow();
    if ($row != FALSE) {
      $promotion = array();
      $promotion['express_fee'] = $row['express_fee'];
      $promotion['price'] = $row['price'];
      $promotion['promotion_price'] = $row['promotion_price'];
      return $promotion;
    }
    return FALSE;
  }

  protected function fillTop_item() {
    $debug = DSConfig::getVal('site_debug') == 1;
    if (!$this->fromCart || $debug) {
      Profiler::start('item->fillTop_item');
    }
    $this->top_item = new stdClass();
    $this->top_item->title = $this->taobao_item->title;
    if (isset($this->taobao_item->location->state)) {
      $this->top_item->state = $this->taobao_item->location->state;
      $this->top_item->city = $this->taobao_item->location->city;
    }
    else {
      if (isset($this->taobao_item->location) && ($this->taobao_item->location != '')) {
        $this->top_item->city = $this->taobao_item->location;
      }
      else {
        $this->top_item->city = FALSE;
      }
    }
    if (isset($this->taobao_item->cid)) {
      $this->top_item->cid = $this->taobao_item->cid;

      // Если из парсера - пока воздержимся
      if (isset($this->taobao_item->fromDSG)) {
        $this->top_item->fromDSG = TRUE;
        if (isset($this->taobao_item->desc)) {
          $this->top_item->desc = $this->taobao_item->desc;
        }
        if (isset($this->taobao_item->descUrl)) {
          $this->top_item->descUrl = $this->taobao_item->descUrl;
        }
        $this->top_item->props = unserialize(serialize($this->taobao_item->props));
        if (isset($this->taobao_item->item_attributes)) {
          $this->top_item->item_attributes = unserialize(serialize($this->taobao_item->item_attributes));
        }
        $this->top_item->seller = new stdClass();
        $this->top_item->seller->seller_nick = $this->taobao_item->nick;
        $this->top_item->seller->user_id = $this->taobao_item->seller_id;
        $this->top_item->seller->rating = new stdClass();
        $this->top_item->seller->rating = new stdClass();
        $this->top_item->seller->seller_credit = 0;
        $this->top_item->seller->rating->level = 0;
        $this->top_item->seller->paySuccess = 0;
        $this->top_item->seller->refundCount = 0;
        if (isset($this->taobao_item->valReviewsApi)) {
//=============================
          if ($this->taobao_item->isTmall) {
            if (isset($this->taobao_item->valReviewsApi->dsr)) {
              if (isset($this->taobao_item->valReviewsApi->dsr->rateTotal)) {
                $this->top_item->seller->seller_credit = $this->taobao_item->valReviewsApi->dsr->rateTotal;
              }
              if (isset($this->taobao_item->valReviewsApi->dsr->gradeAvg)) {
                $this->top_item->seller->rating->level = (float) $this->taobao_item->valReviewsApi->dsr->gradeAvg;
              }
//              if (isset($this->taobao_item->valReviewsApi->scoreInfo->width)) {
//                $this->top_item->seller->reviews = (float) $this->taobao_item->valReviewsApi->dsr->gradeAvg;
//              }
            }
          }
          else {
            if (isset($this->taobao_item->valReviewsApi->scoreInfo)) {
              if (isset($this->taobao_item->valReviewsApi->scoreInfo->merchandisTotal)) {
                $this->top_item->seller->seller_credit = $this->taobao_item->valReviewsApi->scoreInfo->merchandisTotal;
              }
              if (isset($this->taobao_item->valReviewsApi->scoreInfo->merchandisScore)) {
                $this->top_item->seller->rating->level = (float) $this->taobao_item->valReviewsApi->scoreInfo->merchandisScore;
              }
              if (isset($this->taobao_item->valReviewsApi->scoreInfo->width)) {
                $this->top_item->seller->reviews = $this->taobao_item->valReviewsApi->scoreInfo->width;
              }
            }
          }
//=============================
          if (isset($this->taobao_item->apiItemInfo->quantity)) {
            if (isset($this->taobao_item->apiItemInfo->quantity->paySuccess)) {
              $this->top_item->seller->paySuccess = $this->taobao_item->apiItemInfo->quantity->paySuccess;
            }
            else {
              $this->top_item->seller->paySuccess = 0;
            }
            if (isset($this->taobao_item->apiItemInfo->quantity->refundCount)) {
              $this->top_item->seller->refundCount = $this->taobao_item->apiItemInfo->quantity->refundCount;
            }
            else {
              $this->top_item->seller->refundCount = 0;
            }
          }

        }
      }
      else { //Из апи

        if (isset($this->taobao_item->props)) {
          //Получаем все характеристики товара
          $props_str = $this->taobao_item->props;
        }
        else {
          $props_str = '';
        }
        if (isset($this->taobao_item->props_name)) {
          $props_name_str = $this->taobao_item->props_name;
        }
        else {
          $props_name_str = '';
        }
        if (isset($this->taobao_item->property_alias)) {
          $property_alias_str = $this->taobao_item->property_alias;
        }
        else {
          $property_alias_str = '';
        }
//========= Вынимаем сразу же props из SKU
        if ($props_name_str != '') {
          $props_name_full_str = $props_name_str;
          if (isset($this->taobao_item->skus->sku)) {
            foreach ($this->taobao_item->skus->sku as $sku) {
              $props_name_full_str .= ';' . $sku->properties_name;
            }
          }
          $props_name_str_array = array_unique(explode(';', $props_name_full_str));
          sort($props_name_str_array);
          $props_name_str = implode(';', $props_name_str_array);
        }
//========================================
        if (!$this->fromCart || $debug) {
          Profiler::start('item->fillTop_item->prepareProps');
        }
        $props_array = $this->prepareProps($props_str, $props_name_str, $property_alias_str, $this->top_item->cid);
        if (!$this->fromCart || $debug) {
          Profiler::stop('item->fillTop_item->prepareProps');
        }
        $this->top_item->props = $props_array;
      }
    }
    else {
      $props_array = array();
      $this->top_item->props = $props_array;
    }

    //

    if (isset($this->taobao_item->skus)) {
      $this->top_item->skus = clone $this->taobao_item->skus;
    };
//==== Обработка SKU
    if ((DSConfig::getVal('taobao_EnabelDiscounts') == 1)
        && ((DSConfig::getVal('search_use_safe_price_ranges') == 1)
        && ($this->taobao_item->promotion_price)
        && ($this->taobao_item->price)
        && ($this->taobao_item->promotion_price/$this->taobao_item->price)>(float)DSConfig::getVal('search_use_safe_price_minimal_promotion_percent'))
    ) {
      $this->top_item->promotion_price = $this->taobao_item->promotion_price;
    }
    else {
      $this->top_item->promotion_price = $this->taobao_item->price;
    }
    $this->top_item->price = $this->taobao_item->price;
//============= Input props
    //Подготавливаем картинки характеристик, наличие на складе, цену
    if (isset($this->taobao_item->fromDSG)) {
      if (isset($this->taobao_item->props)) {
        $this->top_item->prop_imgs = new stdClass();
        $this->top_item->prop_imgs->prop_img = array();
        foreach ($this->top_item->props as $i => $prop) {
          $prop->haveImages = FALSE;
          if (isset($prop->childs)) {
            foreach ($prop->childs as $child) {
              if (isset($child->url)) {
                if ($child->url != '') {
                  $this->top_item->prop_imgs->prop_img[] = new stdClass();
                  end($this->top_item->prop_imgs->prop_img)->id = '0';
                  end($this->top_item->prop_imgs->prop_img)->position = '0';
                  end($this->top_item->prop_imgs->prop_img)->properties = $i . ':' . $child->vid;
                  end($this->top_item->prop_imgs->prop_img)->url = $child->url;
                  if (strlen($child->url) > 0) {
                    $prop->haveImages = TRUE;
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
      if (isset($this->taobao_item->prop_imgs->prop_img)) {
        $this->top_item->prop_imgs = clone $this->taobao_item->prop_imgs;
      }
    }

//
    $input_props = Array();
    if (isset($this->top_item->skus->sku)) {
      foreach ($this->top_item->skus->sku as $sku) {
        $sku_props = explode(';', $sku->properties);
        foreach ($sku_props as $sku_prop) {
          $skupropval = explode(':', $sku_prop);
          $pid = $skupropval[0];
          if (!isset($input_props[$pid]) && isset($this->top_item->props[$pid])) {
            $input_props[$pid] = $this->top_item->props[$pid];
          }
        }
      }
    }
//=============
    $this->top_item->input_props = $input_props;
//============= Заполняем простые свойства
    $this->top_item->num_iid = $this->taobao_item->num_iid;
    $this->top_item->num = $this->taobao_item->num;
    $this->top_item->nick = $this->taobao_item->nick;
    $this->top_item->seller_id = $this->taobao_item->seller_id;
//    $this->top_item->city=$this->taobao_item->location->city;
//    $this->top_item->state=$this->taobao_item->location->state;
    $this->top_item->pic_url = $this->taobao_item->pic_url;
    $this->top_item->item_imgs = clone $this->taobao_item->item_imgs;
    $this->top_item->express_fee = $this->taobao_item->express_fee;
    if (isset($this->taobao_item->postage_id)) {
      $this->top_item->postage_id = $this->taobao_item->postage_id;
    }
    else {
      $this->top_item->postage_id = 0;
    }
    if (!$this->fromCart || $debug) {
      Profiler::stop('item->fillTop_item');
    }
  }

  public function prepareProps($props, $props_name = '', $property_alias = '', $cid = 0) {
    $res = array();
    $props_array = Array();
    if ($props != '') {
      $props_array = explode(';', $props);
    }
    $name_array = Array();
    if ($props_name != '') {
      $name_array = explode(';', $props_name);
    }
    $alias_array = Array();
    if ($property_alias != '') {
      $alias_array = explode(';', $property_alias);
    }
//============= Заполняем массивы
    if (!empty($props_array)) {
      foreach ($props_array as $k => $prop) {
        $s = explode(':', $prop);
        $props_array[$k] = new StdClass();
        $props_array[$k]->pid = $s[0];
        $props_array[$k]->vid = $s[1];
      }
    }
    if (!empty($name_array)) {
      foreach ($name_array as $k => $prop) {
        $s = explode(':', $prop);
        $name_array[$k] = new StdClass();
        $name_array[$k]->pid = $s[0];
        $name_array[$k]->vid = $s[1];
        $name_array[$k]->pid_name = $s[2];
        $name_array[$k]->vid_name = $s[3];
      }
    }
    if (!empty($alias_array)) {
      foreach ($alias_array as $k => $prop) {
        $s = explode(':', $prop);
        $alias_array[$k] = new StdClass();
        $alias_array[$k]->pid = $s[0];
        $alias_array[$k]->vid = $s[1];
        $alias_array[$k]->vid_name = $s[2];
      }
    }
//=========== Подготавливаем результат
    if (!empty($props_array)) {
      foreach ($props_array as $k => $prop) {
        $res[] = new StdClass();
        $res[$k]->pid = $props_array[$k]->pid;
        $res[$k]->vid = $props_array[$k]->vid;
        if (!empty($name_array)) {
          $res[$k]->pid_name = $name_array[$k]->pid_name;
          $res[$k]->vid_name = $name_array[$k]->vid_name;
        }
        else {
          $res[$k]->pid_name = '';
          $res[$k]->vid_name = '';
        }
      }
    }
    else {
      foreach ($name_array as $k => $prop) {
        $res[] = new StdClass();
        $res[$k]->pid = $name_array[$k]->pid;
        $res[$k]->vid = $name_array[$k]->vid;
        $res[$k]->pid_name = $name_array[$k]->pid_name;
        $res[$k]->vid_name = $name_array[$k]->vid_name;
      }
    }

//=========== Обрабатывыаем алиасы
    if (!empty($alias_array)) {
      foreach ($alias_array as $alias_prop) {
        foreach ($res as $j => $prop) {
          if (($prop->pid == $alias_prop->pid) && ($prop->vid == $alias_prop->vid)) {
            $res[$j]->vid_alias = $alias_prop->vid_name;
            break;
          }
        }
      }
    }

    $result = Search::prepareProps($res, $cid, $this->fromCart);
    /*    Profiler::start('search->prepareProps->translate');
        Yii::app()->DanVitTranslator->translateArray($this->top_item->props, 'prepareProps','zh-CHS', $this->_lang);
        Profiler::stop('search->prepareProps->translate');
    */
    return $result;
  }

  public static function getDescriptionFromUrl($url, $removeHTML = FALSE) {
    Profiler::start('item->getDescription');
    $cacheTag = 'item-descr-' . $url . '-' . (string) $removeHTML;
    $cache = Yii::app()->fileCache->get($cacheTag);
    if (($cache === FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
      //Данных в кэше не найдено
      $dldata = DSGCurl::getHttpDocument($url);
      $descr = $dldata->data;
      unset ($dldata);
      if ($removeHTML) {
        $res = preg_match_all('/<\s*img\s+.*?\ssrc\s*=\s*"(http:\/\/[a-z0-9]+?\.taobaocdn.+?)"/is', $descr, $descImgs);
        if ($res) {
          $result = '<ul style="width: 900px;">';
          foreach ($descImgs[1] as $descImg) {
            $result = $result . '<li style="width: 450px; float:left;"><img style="width:450px; margin:-1px;" src="' . Img::getImagePath($descImg, '_360x360.jpg', FALSE) . '" alt=""></li>';
          }
          $descr = $result . '</ul>';
        }
      }
      Yii::app()->fileCache->set($cacheTag, $descr, 3600 * (int) DSConfig::getVal('search_cache_ttl_item'));
    }
    else {
      $descr = $cache;
    }
    Profiler::stop('item->getDescription');
    return $descr;
  }

  public function inputPropsReady() {
    if (isset($this->taobao_item->skus->sku) || isset($this->taobao_item->item_imgs->item_img)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public static function getSKU($iid, $params, $count=1) {
    $lang = Utils::TransLang();
    try {
      //Получаем данные из кэша о товаре не с карты товара
      $cache = Yii::app()->cache->get('item-cart-' . $lang . '-' . $iid);
      if ($cache === FALSE) {
        $cache = Yii::app()->fileCache->get('item-' . $lang . '-' . $iid);
      }
      if (($cache === FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
        $item = new Item($iid, TRUE);
        if (!$item) {
          return FALSE;
        }
        else {
          $taobao_item = $item->taobao_item;
          $top_item = $item->top_item;
        }
        //Данных в кэше не найдено
      }
      else {
        list($taobao_item, $top_item) = $cache;
      }
      if (!isset($taobao_item) || !isset($top_item)) {
        Yii::app()->cache->delete('item-cart-' . $lang . '-' . $iid);
        Yii::app()->fileCache->delete('item-' . $lang . '-' . $iid);
        return FALSE;
      }
    } catch (Exception $e) {
      LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
      return FALSE;
    }
    $result = new stdClass();
    $result->top_item = clone $top_item;
//=============================== Postprocess express_fee
      if (DSConfig::getVal('delivery_ignore_delivery_cost_from_taobao') == 1) {
          if (isset($result->top_item->express_fee)) {
              $result->top_item->express_fee = DSConfig::getVal('delivery_default_chinese_fee_cny');
          }
      }
      if (DSConfig::getVal('delivery_minimal_chinese_fee_cny')) {
          if (isset($result->top_item->express_fee)) {
              $result->top_item->express_fee = max($result->top_item->express_fee,DSConfig::getVal('delivery_minimal_chinese_fee_cny'));
          }
      }

//===============================
    $param_array = explode(';', $params);
//==================
    if (isset($top_item->skus->sku)) {
      $result->sku = new stdClass();
      $is_match = FALSE;
      foreach ($top_item->skus->sku as $sku) {
        $sku_param_array = explode(';', $sku->properties);
        $is_match = FALSE;
        foreach ($param_array as $i) {
          $is_match = in_array($i, $sku_param_array);
          if (!$is_match) {
            break;
          }
        }
        if ($is_match) {
          $result->sku = clone $sku;
          break;
        }
      }
      if (!$is_match) {
        unset($result->sku);
      }
    }
    if (isset($top_item->prop_imgs->prop_img)) {
      foreach ($top_item->prop_imgs->prop_img as $prop_img) {
        $img_param_array = explode(';', $prop_img->properties);
        $is_match = FALSE;
        foreach ($img_param_array as $i) {
          $is_match = in_array($i, $param_array);
          if ($is_match) {
            break;
          }
        }
        if ($is_match) {
          $result->prop_img = clone $prop_img;
          break;
        }
      }
    }
//===================
    if (isset($result->sku)) {
      $result->sku->price = (float) $result->sku->price;
      if (!isset($result->sku->promotion_price)) {
        $result->sku->promotion_price = $result->sku->price;
      }
        if ((DSConfig::getVal('taobao_EnabelDiscounts') != 1) ||
          ((DSConfig::getVal('search_use_safe_price_ranges') == 1)
            && ($result->sku->promotion_price/$result->sku->price)<(float)DSConfig::getVal('search_use_safe_price_minimal_promotion_percent'))
        )
        {
            $result->sku->promotion_price = $result->sku->price;
        }


      $resUserPrice = Formulas::getUserPrice(
        array(
          'price'=>$result->sku->promotion_price,
          'count'=>1,
          'deliveryFee'=>$result->top_item->express_fee,
          'postageId'=>$taobao_item->postage_id,
          'sellerNick'=>$taobao_item->nick,
          //'asHtml' => TRUE,
        ));
      $result->sku->userPromotionPrice=$resUserPrice->price;

        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$result->sku->promotion_price,
            'count'=>1,
            'deliveryFee'=>0,
            'postageId'=>$taobao_item->postage_id,
            'sellerNick'=>$taobao_item->nick,
              //'asHtml' => TRUE,
          ));
        $result->sku->userPromotionPriceNoDelivery=$resUserPrice->price;

      $resUserPrice = Formulas::getUserPrice(
        array(
          'price'=>$result->sku->price,
          'count'=>1,
          'deliveryFee'=>$result->top_item->express_fee,
          'postageId'=>$taobao_item->postage_id,
          'sellerNick'=>$taobao_item->nick,
        ));
      $result->sku->userPrice=$resUserPrice->price;

        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$result->sku->price,
            'count'=>1,
            'deliveryFee'=>0,
            'postageId'=>$taobao_item->postage_id,
            'sellerNick'=>$taobao_item->nick,
          ));
        $result->sku->userPriceNoDelivery=$resUserPrice->price;

      $resUserPrice = Formulas::getUserPrice(
        array(
          'price'=>$result->sku->promotion_price,
          'count'=>$count,
          'deliveryFee'=>$result->top_item->express_fee,
          'postageId'=>$taobao_item->postage_id,
          'sellerNick'=>$taobao_item->nick,
        ));
        $result->sku->userPriceFinal=$resUserPrice->price;

        $resUserPrice = Formulas::getUserPrice(
          array(
            'price'=>$result->sku->promotion_price,
            'count'=>$count,
            'deliveryFee'=>0,
            'postageId'=>$taobao_item->postage_id,
            'sellerNick'=>$taobao_item->nick,
          ));
        $result->sku->userPriceFinalNoDelivery=$resUserPrice->price;

      $result->sku->count = (($result->sku->quantity == 0)) ? 0 : $result->sku->quantity;
    }
    //       print_r($return);
    return $result;
  }

  public function save() {
    if (isset($this->newTitle)) {
      Yii::app()->db->createCommand()->update('featured',
        array('title' => $this->newTitle,),
        'iid=:iid', array(':iid' => $this->taobao_item->num_iid));
    }
  }

  public static function getSEOTags($topItem) {

    if (!function_exists('translit')) {
      function translit($s) {
        return Utils::translitURL($s);
      }
    }
    $meta = new stdClass();
    $meta->title = cms::customContent('default-meta-title');
    $meta->description = cms::customContent('default-meta-description');
    $meta->keywords = cms::customContent('default-meta-keywords');
    $src_title = Utils::translationAddClearTag($topItem->title);
    $src_cat_path = $topItem->cat_path;
    $src_item_id = $topItem->num_iid;
    $src_item_attributes = $topItem->item_attributes;
    $site_name = DSConfig::getVal('site_name');

    try {
      $xml = simplexml_load_string(DSConfig::getVal('seo_itemcard_rules'), NULL, LIBXML_NOCDATA);
//--------------------
      $evalCommand = (string) $xml->itemMetas;
      $meta = eval($evalCommand);
    } catch (Exception $e) {
      LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
      //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
      echo $e->getMessage();
      return $meta;
    }
    return $meta;
  }

}