<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersItemsComments.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "orders_items_comments".
 *
 * The followings are the available columns in table 'orders_items_comments':
 * @property string $id
 * @property integer $item_id
 * @property integer $uid
 * @property string $date
 * @property string $message
 * @property integer $internal
 *
 * The followings are the available model relations:
 * @property OrdersItems $item
 * @property Users $u
 * @property OrdersItemsCommentsAttaches[] $ordersItemsCommentsAttaches
 */
class customOrdersItemsComments extends DSEventableActiveRecord {
  public $attaches;
  public $fromName;

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'orders_items_comments';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('item_id, uid, date, message', 'required'),
      array('item_id, uid, internal', 'numerical', 'integerOnly' => TRUE),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array('id, item_id, uid, date, message, internal', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'item' => array(self::BELONGS_TO, 'OrdersItems', 'item_id'),
      'u' => array(self::BELONGS_TO, 'Users', 'uid'),
      'ordersItemsCommentsAttaches' => array(self::HAS_MANY, 'OrdersItemsCommentsAttaches', 'comment_id'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id' => Yii::t('main', 'id комментария к лоту'),
      'item_id' => Yii::t('main', 'Id лота заказа'),
      'uid' => Yii::t('main', 'id пользователя - автора сообщения'),
      'date' => Yii::t('main', 'Дата'),
      'message' => Yii::t('main', 'Текст сообщения'),
      'internal' => Yii::t('main', 'Внутреннее сообщение (не видно клиентам)'),
      'fromName' => Yii::t('main', 'Автор'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id, TRUE);
    $criteria->compare('item_id', $this->item_id);
    $criteria->compare('uid', $this->uid);
    $criteria->compare('date', $this->date, TRUE);
    $criteria->compare('message', $this->message, TRUE);
    $criteria->compare('internal', $this->internal);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return OrdersItemsComments the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public static function addOrderItemComment($orderItemId, $message, $internal) {
    if (Yii::app()->user->isGuest) {
      return FALSE;
    }
    $_uid = Yii::app()->user->id;
    if (($_uid == -1) && ($_uid == NULL)) {
      return FALSE;
    }
    $_mess = htmlspecialchars(trim($message));
    if (strlen($_mess) > 0) {
      $orderItemComment = new OrdersItemsComments();
      $orderItemComment->item_id = $orderItemId;
      $orderItemComment->uid = $_uid;
      $orderItemComment->date = date("Y-m-d H:i:s", time());
      $orderItemComment->message = $_mess;
      $orderItemComment->internal = $internal;
      return $orderItemComment->save();
    } else {
      return false;
    }
  }

  public static function getOrderItemComments($orderItemId, $showInternals = 0, $pageSize = 5,$asArray=false) {
    // Must to return dataProviders for comments and attaches
    $criteria = new CDbCriteria;
    $criteria->select = 't.*, (select uu.email from users uu where uu.uid=t.uid) as fromName';
    $criteria->condition = 't.item_id=:item_id and (t.internal=0 or :internal=1)';
    $criteria->params = array(':item_id' => $orderItemId, ':internal' => $showInternals);
    $criteria->order = 't.`date` DESC';
    if ($asArray) {
      $rows=self::model()->findAll($criteria);
      return $rows;
    } else {
    $orderItemComments = new CActiveDataProvider(self::model(), array(
      'criteria' => $criteria,
      'sort' => FALSE,
      'pagination' => array(
        'pageSize' => $pageSize,
//        'currentPage'=>'pageCount'-1,
      ),
    ));
    return $orderItemComments;
    }
  }
    public static function getAdminLink($id, $external = false)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/orders/view/id/' . $id . '&tabName=' .Yii::t('admin','Заказ '). $order->uid.'-'.$order->id;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/orders/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр заказа'
                ) . '" onclick="getContent(this,\'' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '\');return false;"><i class="icon-shopping-cart"></i>&nbsp;' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }

    public static function getUserLink($id)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            return 'http://' . DSConfig::getVal(
              'site_domain'
            ) . '/cabinet/orders/view/id/' . $id;
        } else {
            return '<a href="#">' . Yii::t('main', 'Ошибка') . '</a>';
        }
    }
}
