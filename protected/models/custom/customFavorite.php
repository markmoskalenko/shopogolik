<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Favorite.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "favorites".
 *
 * The followings are the available columns in table 'favorites':
 * @property integer $id
 * @property integer $uid
 * @property string $num_iid
 * @property string $date
 * @property string $cid
 * @property double $express_fee
 * @property double $price
 * @property double $promotion_price
 * @property string $pic_url
 * @property double $seller_rate
 */
class customFavorite extends CActiveRecord {
  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'favorites';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('num_iid', 'required'),
      array('uid', 'numerical', 'integerOnly' => TRUE),
      array('express_fee, price, promotion_price, seller_rate', 'numerical'),
      array('num_iid, cid', 'length', 'max' => 20),
      array('pic_url', 'length', 'max' => 512),
      array('date, dsg_item', 'safe'),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array(
        'id, uid, num_iid, date, cid, express_fee, price, promotion_price, pic_url, seller_rate, dsg_item',
        'safe',
        'on' => 'search'
      ),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'              => 'ID',
      'uid'             => 'Uid',
      'num_iid'         => 'Num Iid',
      'date'            => 'Date',
      'cid'             => 'Cid',
      'express_fee'     => 'Express Fee',
      'price'           => 'Price',
      'promotion_price' => 'Promotion Price',
      'pic_url'         => 'Pic Url',
      'seller_rate'     => 'Seller Rate',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('uid', $this->uid);
    $criteria->compare('num_iid', $this->num_iid, TRUE);
    $criteria->compare('date', $this->date, TRUE);
    $criteria->compare('cid', $this->cid, TRUE);
    $criteria->compare('express_fee', $this->express_fee);
    $criteria->compare('price', $this->price);
    $criteria->compare('promotion_price', $this->promotion_price);
    $criteria->compare('pic_url', $this->pic_url, TRUE);
    $criteria->compare('seller_rate', $this->seller_rate);

    return new CActiveDataProvider($this, array(
      'criteria'   => $criteria,
      'pagination' => array(
        'pageSize' => 44,
      )
    ));
  }

  public static function getCategoriesArrayForUser($uid) {
//      return array();
    $_uid = $uid;
    $recs = Yii::app()->db->createCommand(
      "select rr.id from
    (select ce1.id from categories_ext ce1
    where ce1.cid in (
    select ff.cid from favorites ff
     where ff.cid!=0 and ff.uid=:uid
     group by ff.cid
     ) and (ce1.query='' or ce1.query is null)
     union all
    select ce1.parent from categories_ext ce1
    where ce1.cid in (
    select ff.cid from favorites ff
     where ff.cid!=0  and ff.uid=:uid
     group by ff.cid
     ) and (ce1.query='' or ce1.query is null)
     union all
     select ce2.id from categories_ext ce2
     where ce2.id in (
    select ce1.parent from categories_ext ce1
    where ce1.cid in (
    select ff.cid from favorites ff
     where ff.cid!=0  and ff.uid=:uid
     group by ff.cid
     ) and (ce1.query='' or ce1.query is null)
     )
    ) rr
     group by rr.id
    ")->bindParam(':uid', $_uid, PDO::PARAM_STR)
      ->queryAll();
    $result = array();
    if ($recs) {
    foreach ($recs as $rec) {
      $result[]=$rec['id'];
    }
    }
    return $result;
  }

  protected static function getCategoriesArrayForExport($uid) {
    $_uid = $uid;
    $recs = Yii::app()->db->createCommand(
      "select eee.id,eee.parent,eee.cid,eee.ru as name from categories eee
where eee.id in
(select rr.id from
    (select ce1.id from categories ce1
    where ce1.cid in (
    select ff.cid from favorites ff
     where ff.cid!=0 and ff.uid=:uid and ff.dsg_item is not null
     group by ff.cid
     )
     union all
     select ce2.id from categories ce2
     where ce2.cid in (
    select ce1.parent from categories ce1
    where ce1.cid in (
    select ff.cid from favorites ff
     where ff.cid!=0  and ff.uid=:uid and ff.dsg_item is not null
     group by ff.cid
     )
     )
    ) rr
     group by rr.id)
     order by eee.parent, eee.cid
    ")->bindParam(':uid', $_uid, PDO::PARAM_STR)
      ->queryAll();
    $result = array();
    if ($recs) {
      foreach ($recs as $rec) {
        $result[$rec['cid']]=$rec['cid'];
      }
      foreach ($recs as $i=>$rec) {
        if(!in_array($rec['parent'],$result)) {
          $recs[$i]['parent']=0;
        }
      }
    }
    $res=new stdClass();
    $res->recs=$recs;
    $res->distinct=$result;
    return $res;
  }

public static function getFavoriteMenu($uid, $lang) {
        $result = array();
              $int_id_field = 'cid';
              $data = Yii::app()->db->createCommand("
        select
cid as pkid, cid as id, cid, ru,en,parent,status,meta_desc_ru,meta_keeword_ru,page_title_ru,meta_desc_en,meta_keeword_en,page_title_en,page_desc_ru,
          page_desc_en,url,zh,query,level from categories eee
where eee.cid in (
    select ff.cid from favorites ff where ff.cid!=0 and ff.uid=:uid -- group by ff.cid
     )
group by eee.cid
order by eee.cid")
                ->bindParam(":uid", $uid, PDO::PARAM_INT)
                ->queryAll();
        foreach ($data as $menucat) {
//------------------------------------------------------------
                    $menucat['view_text'] = $menucat[$lang];
                    unset($menucat['ru']);
                    unset($menucat['en']);
                    unset($menucat['zh']);
                    unset($menucat['meta_desc_ru']);
                    unset($menucat['meta_keeword_ru']);
                    unset($menucat['page_title_ru']);
                    unset($menucat['page_desc_ru']);
                    unset($menucat['meta_desc_en']);
                    unset($menucat['meta_keeword_en']);
                    unset($menucat['page_title_en']);
                    unset($menucat['page_desc_en']);
                    unset($menucat['manual']);
//============================================================
            $result[$menucat['pkid']] = $menucat;
        }
        return $result;
    }

public static function getExportData($uid) {
  $res=new stdClass();
  $cats=self::getCategoriesArrayForExport($uid);
  $res->categories=$cats->recs;
  $res->categoriesDistinct=$cats->distinct;
  $res->items=self::model()->findAll('uid=:uid and dsg_item is not null',array(':uid'=>$uid));
   return $res;
}

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return Favorites the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }
}