<?php

/**
 * This is the model class for table "deliveries".
 *
 * The followings are the available columns in table 'deliveries':
 * @property integer $id
 * @property string $delivery_id
 * @property integer $enabled
 * @property string $name
 * @property string $description
 * @property string $currency
 * @property double $min_weight
 * @property double $max_weight
 * @property string $fees
 */
class customDeliveries extends CActiveRecord {
  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'deliveries';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('enabled', 'numerical', 'integerOnly' => TRUE),
      array('min_weight, max_weight', 'numerical'),
      array('delivery_id', 'length', 'max' => 128),
      array('name', 'length', 'max' => 383),
      array('currency', 'length', 'max' => 32),
      array('description, fees', 'safe'),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array(
        'id, delivery_id, enabled, name, description, currency, min_weight, max_weight, fees',
        'safe',
        'on' => 'search'
      ),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'          => Yii::t('admin', 'Внутренний идентификатор доставки'),
      'delivery_id' => Yii::t('admin', 'id службы доставки'),
      'enabled'     => Yii::t('admin', 'Служба доставки активна'),
      'name'        => Yii::t('admin', 'Название службы доставки'),
      'description' => Yii::t('admin', 'Описание службы доставки'),
      'currency'    => Yii::t('admin', 'Валюта расчетов стоимости доставки'),
      'min_weight'  => Yii::t('admin', 'Минимальный вес в граммах'),
      'max_weight'  => Yii::t('admin', 'Максимальный вес в граммах'),
      'fees'        => Yii::t('admin', 'Стоимости доставки в разные страны'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('delivery_id', $this->delivery_id, TRUE);
    $criteria->compare('enabled', $this->enabled);
    $criteria->compare('name', $this->name, TRUE);
    $criteria->compare('description', $this->description, TRUE);
    $criteria->compare('currency', $this->currency, TRUE);
    $criteria->compare('min_weight', $this->min_weight);
    $criteria->compare('max_weight', $this->max_weight);
    $criteria->compare('fees', $this->fees, TRUE);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return Deliveries the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public static function getCountryName($code) {
    $filename = Yii::app()->basePath . '/config/countries.xml';
    if (file_exists($filename)) {
      $xml = (array) simplexml_load_file($filename, 'SimpleXMLElement', LIBXML_NOCDATA);
      $data = array();

      foreach ($xml['country'] as $country) {
        $country = (array) $country;
        if ($country['alpha3'] == $code) {
          return $country['name'];
        }
      }
      return $data;
    }
    else {
      return array();
    }
  }

  public static function getCountries($deliveryOnly = FALSE) {
    $countries = array();
    if ($deliveryOnly) {
      $data = Deliveries::model()->findAll('enabled =1');
      if (!$data) {
        return array();
      }

      foreach ($data as $row) {
        $xml = simplexml_load_string($row['fees']);
//======================================
        $a3 = array();
        $res = preg_match_all('/\<country\>(.*?)\</is', $xml, $a3);
        if ($res > 0) {
          foreach ($a3[1] as $country) {
            $countries[$country] = $country;
          }
        }
      }
    }
    $filename = Yii::app()->basePath . '/config/countries.xml';
    if (file_exists($filename)) {
      $xml = (array) simplexml_load_file($filename, 'SimpleXMLElement', LIBXML_NOCDATA);
      $data = array();

      foreach ($xml['country'] as $country) {
        $country = (array) $country;
        if ((in_array($country['alpha3'], $countries)) || ($deliveryOnly == FALSE)) {
          /** @noinspection PhpUndefinedClassInspection */
          if (Yii::app()->language == 'ru') {
            $data[$country['alpha3']] = $country['name'];
          }
          else {
            $data[$country['alpha3']] = $country['english'];
          }
        }
      }
      return $data;
    }
    else {
      return array();
    }
  }

  public static function getDelivery($weight, $country, $name = FALSE,$ignoreLimits=0) {
    if (!$name) {
      $resDelivery = array();
      $data = Deliveries::model()->findAll("enabled=1 and (:weight >= min_weight or min_weight=0 or :weight=0 or :ignoreLimits=1)
      and (:weight<=max_weight or max_weight=0 or :weight=0 or :ignoreLimits=1)",
        array(':weight' => $weight,':ignoreLimits'=>$ignoreLimits));
    }
    else {
      $resDelivery = new stdClass();
      $data = Deliveries::model()->findAll("enabled = 1 and delivery_id=:delivery_id and (:weight >= min_weight or min_weight=0 or :weight=0 or :ignoreLimits=1)
      and (:weight<=max_weight or max_weight=0 or :weight=0 or :ignoreLimits=1)",
        array(':delivery_id' => $name, ':weight' => $weight,':ignoreLimits'=>$ignoreLimits));
    }
    if (!$data) {
      return FALSE;
    }
    $selectedXML = FALSE;
    foreach ($data as $row) {
      $xml = simplexml_load_string($row['fees']);
//======================================
      if ($country) {
        $selectedXML = $xml->xpath("fees/fee/country[.='" . $country . "']/parent::*");
        if (!$selectedXML) {
          continue;
        }
      }
//======================================
      if (!$name) {
        $resDelivery[$row->delivery_id] = new stdClass();
        $currDelivery = end($resDelivery);
      }
      else {
        $currDelivery = $resDelivery;
      }
      $currDelivery->id = $row->delivery_id;
      $currDelivery->name = $row->name;
      $currDelivery->description = $row->description;
      $currDelivery->currency = $row->currency;
      if ($country) {
        $currDelivery->fee = $selectedXML[0];
        if (!isset($currDelivery->fee->profit)) {
          $currDelivery->fee->profit=1;
        }
//---------------
        if ($weight > 0) {
          if (($weight >= (float) $currDelivery->fee->initial_weight) && ((float) $currDelivery->fee->initial_weight > 0)
            && ((float) $currDelivery->fee->ext_weight > 0)
          ) {
            if ((float) $currDelivery->fee->ext_weight != 0) {
              $ext_weight_units = ceil(($weight - (float) $currDelivery->fee->initial_weight) / (float) $currDelivery->fee->ext_weight);
            }
            else {
              $ext_weight_units = ceil(($weight - (float) $currDelivery->fee->initial_weight) / 1);
            }
            $currDelivery->summ = Formulas::convertCurrency(
              ((float) $currDelivery->fee->initial_fee + $ext_weight_units * (float) $currDelivery->fee->ext_fee),
              $currDelivery->currency,
              DSConfig::getVal('site_currency')
            );
          }
          else {
            $currDelivery->summ = Formulas::convertCurrency(
              (float) $currDelivery->fee->initial_fee,
              $currDelivery->currency,
              DSConfig::getVal('site_currency')
            );
          }
        }
        else {
          $currDelivery->summ = 0.01;
         /* Formulas::convertCurrency(
            (float) $currDelivery->fee->initial_fee,
            $currDelivery->currency,
            DSConfig::getVal('site_currency'));
         */
        }
      }
//---------------
    }
    return $resDelivery;
  }

}
