<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrderCalculated.php">
* </description>
**********************************************************************************************************************/?>
<?php
class customOrderCalculated extends CFormModel {

  public $order_total=0; // Заявленная цена заказа во внутренней валюе + цена доставки в ней же
  public $taobao_total=0; // Закупка + доставка на таобао при заказе
  public $actual_taobao_total=0; // Закупка + доставка на таобао фактически
  public $actual_taobao_price=0; // Закупка на таобао фактически
  public $actual_taobao_express_fee=0; // доставка на таобао фактически
  public $taobao_total_curr=0; // Закупка + доставка на таобао при заказе
  public $actual_taobao_total_curr=0; // Закупка + доставка на таобао фактически
  public $actual_lots_weight=0; // Фактический вес по лотам
  public $actual_lots_delivery=0; // Фактическая доставка по лотам
  public $actual_lots_summ=0; // Фактическая доставка по лотам
  public $manual_total=0; //Расчётная справедливая стоимость заказа
  public $actual_lots_total=0; //Расчётная справедливая стоимость заказа по лотам
  public $actual_items_count=0; // Актуальное количество товаров в заказе
  public $items_count=0; // Актуальное количество товаров в заказе
  public $payments_sum=0; //Сумма платежей по заказу
  public $payments_saldo=0; //Сальдо платежей по заказу
  public $operatorProfit = 0;
  public $siteProfit = 0;
  public $realProfit = 0;

  public $vars=array();

  function rules() {
    return array();
  }

function __construct($order,$scenario='insert') {
  parent::__construct($scenario);
  $this->vars=Formulas::calculateOrder($order);
  foreach ($this->vars as $name => $val) {
    if (isset($this->$name)) {
      if (is_float($val->val)) { // && (!is_int($val->val))
      $this->$name=$val->cval;
      } else {
      $this->$name=$val->val;
      }
    }
  }
}

  function attributeLabels() {
    return array(
      'actual_taobao_total' => Yii::t('main', 'Фактическая закупка на таобао'),
      'operatorProfit' => Yii::t('admin', 'Возможный бонус менеджера').', '.DSConfig::getVal('site_currency'),
      'siteProfit' => Yii::t('admin', 'Возможный бонус сайта').', '.DSConfig::getVal('site_currency'),
      'realProfit' => Yii::t('admin', 'Возможная прибыль с заказа').', '.DSConfig::getVal('site_currency'),
    );
  }

public function varReport($varName) {
    if (DSConfig::getVal('formulas_show_titles_hints')!=1) {
        return '';
    }
  if (isset($this->vars) && isset($this->vars[$varName])) {
    $var=$this->vars[$varName];
    if (Yii::app()->user->inRole('superAdmin')) {
    $result= '$vars[\''.$varName.'\']'." </i>".$var->description."</i>";//.$var->report();
    $result=$result.$var->report();
    } else {
      $result=$var->description;
    }
  } else {
    $result= '$vars[\''.$varName.'\'] - <i>не определено!</i>';
  }
  return $result;
}

}


