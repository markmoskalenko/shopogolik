<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="TranslatorKeys.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "translator_keys".
 *
 * The followings are the available columns in table 'translator_keys':
 * @property integer $id
 * @property string $key
 * @property string $type
 * @property integer $enabled
 * @property integer $banned
 * @property string $banned_date
 * @property string $descr
 */
class customTranslatorKeys extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $keyusage='-';
	public function tableName()
	{
		return 'translator_keys';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enabled, banned', 'numerical', 'integerOnly'=>true),
			array('key, type', 'length', 'max'=>512),
			array('banned_date, descr', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, key, type, enabled, banned, banned_date, descr', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'key' => 'AppKey',
			'type' => Yii::t('admin','Тип'),
			'enabled' => Yii::t('admin','Вкл'),
			'banned' => Yii::t('admin','Бан'),
			'banned_date' => Yii::t('admin','Дата бана'),
			'descr' => Yii::t('admin','Описание'),
            'keyusage' => Yii::t('admin','Расход'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('banned',$this->banned);
		$criteria->compare('banned_date',$this->banned_date,true);
		$criteria->compare('descr',$this->descr,true);


      $criteria->select = "t.*,
      (select concat(sum(ll.chars),' (',round(100*sum(ll.chars)/2000000,1),'%)') from log_translator_keys ll where ll.keyid=t.id
      and ll.`date`>DATE_FORMAT(NOW() ,'%Y-%m-01')) as keyusage";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TranslatorKeys the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}
}
