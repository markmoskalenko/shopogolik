<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ItemSeller.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customItemSeller {
  protected $_iid;
  protected $_lang = 'ru';

  public static function getSellerInfoFromUrl($url) {
    if (!$url) {
        return false;
    }
      $cache = Yii::app()->fileCache->get('sellerInfo-' . $url);
      if (($cache == FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
              $seller=new DSGSeller();
              $seller->srcUrl=$url;
              $sellerInfo=$seller->execute();
        Yii::app()->fileCache->set('sellerInfo-' . $url, array($sellerInfo), 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other'));
      }
      else {
        list($sellerInfo) = $cache;
      }
      if (isset($sellerInfo->seller) && isset($sellerInfo->seller->nick) && isset($sellerInfo->seller->sales) && ($sellerInfo->seller->sales>0)) {
          Yii::app()->db->createCommand(
            'update log_item_requests set seller_rate=:sales where nick=:nick and (seller_rate is null or seller_rate!=:sales)'
          )
            ->bindParam(':sales', $sellerInfo->seller->sales, PDO::PARAM_STR)
            ->bindParam(':nick', $sellerInfo->seller->nick, PDO::PARAM_STR)
            ->execute();
      }
      return $sellerInfo;
  }

  /**
   * Возвращает user_id по нику пользователя
   **/
  public static function extNickToUserID($nick) {
    Profiler::start('item->extNickToUserID');
    $url = "http://shopsearch.taobao.com/search?" . http_build_query(array(
        "q" => $nick,
      ));
    try {
      $data = Utils::getHttpDocument($url,true);
      if (preg_match("/data-uid=\"(\d+)/", $data->data, $matches)) {
        $ret = $matches[1];
        Profiler::stop('item->extNickToUserID');
        return $ret;
      }
//      }
      Profiler::stop('item->extNickToUserID');
      return 0;
    } catch (Exception $e) {
      echo $e->getMessage();
      Profiler::stop('item->extNickToUserID');
      return 0;
    }
    //throw new Exception("user_id not found for nick «{$nick}»");
  }
}