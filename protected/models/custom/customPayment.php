<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Payment.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property integer $id
 * @property double $sum
 * @property string $description
 * @property integer $status
 */
class customPayment extends DSEventableActiveRecord {
  public $text_status;
  public $text_date;
  public $email;
  public $username;
  public $phone;

  /**
   * Returns the static model of the specified AR class.
   * @return Payment the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'payments';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('id, status, uid', 'numerical', 'integerOnly' => TRUE),
      array('sum', 'numerical'),
      array('description', 'length', 'max' => 256),
      // array('ruble', 'double'),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      // array('id, sum, description, status, ruble', 'safe', 'on'=>'search'),
      array('id, sum, description, status, date, uid, u.email', 'safe', 'on' => 'search'),
    );
  }

  public function primaryKey() {
    return 'id';
  }


  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'u' => array(self::BELONGS_TO, 'Users', 'uid'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id' => 'ID',
      'sum' => Yii::t('admin', 'Сумма'),
      'description' => Yii::t('admin', 'Описание'),
      'status' => Yii::t('admin', 'Статус'),
      'text_status' => Yii::t('admin', 'Статус'),
      'date' => Yii::t('admin', 'Дата'),
      'text_date' => Yii::t('admin', 'Дата'),
      'uid' => Yii::t('admin', 'ID пользователя'),
      'email' => Yii::t('admin', 'EMail'),
      'username' => Yii::t('admin', 'Имя'),
      'phone' => Yii::t('admin', 'Телефон'),
    );
  }


  public function save($refund_odrer = FALSE, $runValidation = TRUE, $attributes = NULL) {
    return parent::save();
  }


  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search($pageSize=100) {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.
      Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
    $criteria = new CDbCriteria;

    $criteria->compare('t.id', $this->id);
    $criteria->compare('sum', $this->sum);
    $criteria->compare('description', $this->description, TRUE);
    $criteria->compare('t.status', $this->status);
    $criteria->compare("FROM_UNIXTIME(t.date,'%Y-%m-%d %h:%i')", $this->date, TRUE);
    $criteria->compare("u.email", $this->email, TRUE);
    $criteria->compare("concat(u.firstname,' ',u.lastname)", $this->username, TRUE);
    $criteria->compare("u.phone", $this->phone, TRUE);
//    $criteria->condition="t.uid is not null and t.uid!=''";
    $criteria->condition="t.uid is not null and length(t.uid)>0";
    $criteria->compare('t.uid', $this->uid);
    /*
     *  1 - Зачисление или возврат средств
     *  2 - Снятие средств
     *  3 - Ожидание зачисления средств
     *  4 - Отмена ожидания зачисления средств
     *  5 - Отправка внутреннего перевода средств
     *  6 - Получение внутреннего перевода средств
     */
//      $criteria->select = "t.*,u.*";
    $criteria->select = "t.*, u.*,
      case when t.status=1 then '" . Yii::t('main', 'Зачисление или возврат средств') . "'
      when t.status=2 then '" . Yii::t('main', 'Снятие средств') . "'
      when t.status=3 then '" . Yii::t('main', 'Ожидание зачисления средств') . "'
      when t.status=4 then '" . Yii::t('main', 'Отмена ожидания зачисления средств') . "'
      when t.status=5 then '" . Yii::t('main', 'Отправка внутреннего перевода средств') . "'
      when t.status=6 then '" . Yii::t('main', 'Получение внутреннего перевода средств') . "'
      when t.status=7 then '" . Yii::t('main', 'Зачисление бонуса или прибыли') . "'
      when t.status=8 then '" . Yii::t('main', 'Вывод средств из системы') . "'
      else '" . Yii::t('main', 'Не определено (ошибка?)') . "' end as text_status";

    $criteria->with = array('u' => array('select' => 'email, lastname, firstname, phone'));
//      $criteria->with=array('u'=>array('select'=>'email, lastname, firstname, phone'));
//      $criteria->select=array('u.email');
    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
      'sort' => array(
        'defaultOrder' => 't.`date` DESC',
      ),
      'pagination' => array(
        'pageSize' => $pageSize,
      )
    ));
    //     var_dump($dataProvider->getData());
//      die;
  }
    public static function getAdminLink($id, $external = false)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/orders/view/id/' . $id . '&tabName=' .Yii::t('admin','Заказ '). $order->uid.'-'.$order->id;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/orders/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр заказа'
                ) . '" onclick="getContent(this,\'' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '\');return false;"><i class="icon-shopping-cart"></i>&nbsp;' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }

    public static function getUserLink($id)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            return 'http://' . DSConfig::getVal(
              'site_domain'
            ) . '/cabinet/orders/view/id/' . $id;
        } else {
            return '<a href="#">' . Yii::t('main', 'Ошибка') . '</a>';
        }
    }
}
