<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Utils.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class customUtils
{
    public static function getOriginalFromTranslation($s)
    {
        $t = preg_match('/plain\[\d+\]:\s*(.*?)".*?\>/s', $s, $m);
        if ($t > 0) {
            return $m[1];
        } else {
            return $s;
        }
    }

    public static function ARtoXMLnode($model, $attribute)
    {
        if (is_a($model, 'CActiveRecord')) {
            $labels = $model->attributeLabels();
            $label = isset($labels[$attribute]) ? $labels[$attribute] : '';
        } else {
            $label = '';
        }
        $begin = '<' . $attribute
          . ' title="'
          . $label
          . '"'
          . '>';
        $end = '</' . $attribute . '>';
        if (is_array($model) && isset($model[$attribute])) {
            $res = $model[$attribute];
        } elseif (isset($model->$attribute)) {
            $res = $model->$attribute;
        } else {
            $res = '';
        }
        if (preg_match("/[<>&\\\]/s", $res, $m) != 0) {
            $res = '<![CDATA[' . $res . ']]>';
        }
        return $begin . $res . $end . "\n";
    }

    public static function removeOnlineTranslation($s)
    {
        $res = preg_replace(
          '/\<translation.*?\>(.*?)(?:<translate.*?\>.*?<\/translate\>)*\<\/translation\>/is',
          '$1',
          $s
        );
        $res = preg_replace('/\<span.*\>(.*)\<\/span\>/is', '$1', $res);
        $res = trim($res);
        return $res;
    }

    public static function themePath()
    {
        return Yii::app()->request->baseUrl . '/themes/' . Yii::app()->theme->name;
    }

    public static function AppLang()
    {
        return Yii::app()->language;
    }

    public static function TransLang($lang=false)
    {
        if ($lang) {
            $l=$lang;
        } else {
        $l = Yii::app()->language;
        }
        if (in_array($l, array('ru', 'en', 'zh'))) {
            return $l;
        } else {
            return 'en';
        }
    }

    public static function prepareHtmlTemplate($templateHtml, $templateVars, $prefix = '{', $postfix = '}')
    {
        if (is_array($templateVars)) {
            foreach ($templateVars as $key => $value) {
                if (!is_object($value) && !is_array(
                    $value
                  )
                ) { //TODO: Вот эта проврка на is_array - а с чего бы здесь вобще быть array или object ?
                    $templateHtml = str_replace($prefix . $key . $postfix, (string) $value, $templateHtml);
                }
            }
        }
        return $templateHtml;
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     */
    public static function num2str($num, $lang)
    {
        $nul = Yii::t('main', 'ноль');
        $ten = array(
          array(
            '',
            Yii::t('main', 'один'),
            Yii::t('main', 'два'),
            Yii::t('main', 'три'),
            Yii::t('main', 'четыре'),
            Yii::t('main', 'пять'),
            Yii::t('main', 'шесть'),
            Yii::t('main', 'семь'),
            Yii::t('main', 'восемь'),
            Yii::t('main', 'девять')
          ),
          array(
            '',
            Yii::t('main', 'одна'),
            Yii::t('main', 'две'),
            Yii::t('main', 'три'),
            Yii::t('main', 'четыре'),
            Yii::t('main', 'пять'),
            Yii::t('main', 'шесть'),
            Yii::t('main', 'семь'),
            Yii::t('main', 'восемь'),
            Yii::t('main', 'девять')
          ),
        );
        $a20 = array(
          Yii::t('main', 'десять'),
          Yii::t('main', 'одиннадцать'),
          Yii::t('main', 'двенадцать'),
          Yii::t('main', 'тринадцать'),
          Yii::t('main', 'четырнадцать'),
          Yii::t('main', 'пятнадцать'),
          Yii::t('main', 'шестнадцать'),
          Yii::t('main', 'семнадцать'),
          Yii::t('main', 'восемнадцать'),
          Yii::t('main', 'девятнадцать')
        );
        $tens = array(
          2 => Yii::t('main', 'двадцать'),
          Yii::t('main', 'тридцать'),
          Yii::t('main', 'сорок'),
          Yii::t('main', 'пятьдесят'),
          Yii::t('main', 'шестьдесят'),
          Yii::t('main', 'семьдесят'),
          Yii::t('main', 'восемьдесят'),
          Yii::t('main', 'девяносто')
        );
        $hundred = array(
          '',
          Yii::t('main', 'сто'),
          Yii::t('main', 'двести'),
          Yii::t('main', 'триста'),
          Yii::t('main', 'четыреста'),
          Yii::t('main', 'пятьсот'),
          Yii::t('main', 'шестьсот'),
          Yii::t('main', 'семьсот'),
          Yii::t('main', 'восемьсот'),
          Yii::t('main', 'девятьсот')
        );
        if ($lang == 'ru') {
            $unit = array( // Units
              array(Yii::t('main', 'копейка'), Yii::t('main', 'копейки'), Yii::t('main', 'копеек'), 1),
              array(Yii::t('main', 'рубль'), Yii::t('main', 'рубля'), Yii::t('main', 'рублей'), 0),
              array(Yii::t('main', 'тысяча'), Yii::t('main', 'тысячи'), Yii::t('main', 'тысяч'), 1),
              array(Yii::t('main', 'миллион'), Yii::t('main', 'миллиона'), Yii::t('main', 'миллионов'), 0),
              array(Yii::t('main', 'миллиард'), Yii::t('main', 'милиарда'), Yii::t('main', 'миллиардов'), 0),
            );
        } elseif ($lang == 'ua') {
            $unit = array( // Units
              array(Yii::t('main', 'копейка'), Yii::t('main', 'копейки'), Yii::t('main', 'копеек'), 1),
              array(Yii::t('main', 'грн'), Yii::t('main', 'грн'), Yii::t('main', 'грн'), 0),
              array(Yii::t('main', 'тысяча'), Yii::t('main', 'тысячи'), Yii::t('main', 'тысяч'), 1),
              array(Yii::t('main', 'миллион'), Yii::t('main', 'миллиона'), Yii::t('main', 'миллионов'), 0),
              array(Yii::t('main', 'миллиард'), Yii::t('main', 'милиарда'), Yii::t('main', 'миллиардов'), 0),
            );
        } else {
            $unit = array( // Units
              array(Yii::t('main', 'копейка'), Yii::t('main', 'копейки'), Yii::t('main', 'копеек'), 1),
              array(Yii::t('main', 'р.'), Yii::t('main', 'р.'), Yii::t('main', 'р.'), 0),
              array(Yii::t('main', 'тысяча'), Yii::t('main', 'тысячи'), Yii::t('main', 'тысяч'), 1),
              array(Yii::t('main', 'миллион'), Yii::t('main', 'миллиона'), Yii::t('main', 'миллионов'), 0),
              array(Yii::t('main', 'миллиард'), Yii::t('main', 'милиарда'), Yii::t('main', 'миллиардов'), 0),
            );
        }
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                } # 20-99
                else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                } # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = self::morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            } //foreach
        } else {
            $out[] = $nul;
        }
        $out[] = self::morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . self::morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(Yii::t('main', ' '), $out)));
    }

    /**
     * Склоняем словоформу
     */
    protected static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        }
        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }
        if ($n == 1) {
            return $f1;
        }
        return $f5;
    }

//==================================================================
    public static function debugLog($val)
    {
        $command = Yii::app()->db->createCommand(
          "insert into debug_log (val,date)
                  values (:val,now())"
        )
          ->bindParam(':val', $val, PDO::PARAM_STR);
        $command->execute();
    }

//==================================================================
    public static function getCurrencyRatesFromBank($cur = false)
    {
    $autoUpdate=DSConfig::getVal('rates_auto_update') && ((time()-DSConfig::getVal('rates_auto_update_last_time'))>60*60*4);
//  http://www.cbr.ru/scripts/XML_daily_eng.asp?date_req='||TO_CHAR(sysdate,'dd/mm/yyyy')
        $cache = Yii::app()->fileCache->get('currency-rates-from-bank');
        if (($cache == false)) //Alexys cache
        {
            //http://www.cbr.ru/scripts/XML_daily_eng.asp?date_req=24/12/2013
            $resp = self::getHttpDocument('http://www.cbr.ru/scripts/XML_daily_eng.asp?date_req=' . date("d/m/Y"));
            $res = array();
            if (isset($resp->data) && ($resp->data != false)) {
                $resp_xml = simplexml_load_string($resp->data);
                if (isset($resp_xml->Valute)) {
                    foreach ($resp_xml->Valute as $val) {
                        $res[strtolower((string) $val->CharCode)] = (float) str_replace(
                            ",",
                            ".",
                            (string) $val->Value
                          ) / (float) $val->Nominal;
                    }
                }
                Yii::app()->fileCache->set('currency-rates-from-bank', $res, 60 * 60);
            }
        } else {
            $res = $cache;
        }
        if (is_array($res) && (count($res) > 0)) {
            if (!$cur) {
                if ($autoUpdate) {
                    foreach ($res as $currName=>$curr) {
                        $confCurr=DSConfig::model()->findByPk('rate_'.$currName);
                        if ($confCurr) {
                            $confCurr->value=$curr;
                            $confCurr->update();
                        }
                    }
                    $confLastUpd=DSConfig::model()->findByPk('rates_auto_update_last_time');
                    $confLastUpd->value=time();
                    $confLastUpd->update();
                }
                return $res;
            } else {
                if (isset($res[str_replace('rate_', '', $cur)])) {
                    return $res[str_replace('rate_', '', $cur)];
                } else {
                    return 1;
                }
            }
        } else {
            $confLastUpd=DSConfig::model()->findByPk('rates_auto_update_last_time');
            $confLastUpd->value=time();
            $confLastUpd->update();
            return 1;
        }
    }
    public static function translit($s) {
        return self::translitURL($s);
    }
    public static function translitURL($s)
    {
        $tr = array(
          'а' => 'a',
          'б' => 'b',
          'в' => 'v',
          'г' => 'g',
          'д' => 'd',
          'е' => 'e',
          'ё' => 'yo',
          'ж' => 'zh',
          'з' => 'z',
          'и' => 'i',
          'й' => 'j',
          'к' => 'k',
          'л' => 'l',
          'м' => 'm',
          'н' => 'n',
          'о' => 'o',
          'п' => 'p',
          'р' => 'r',
          'с' => 's',
          'т' => 't',
          'у' => 'u',
          'ф' => 'f',
          'х' => 'x',
          'ц' => 'c',
          'ч' => 'ch',
          'ш' => 'sh',
          'щ' => 'shh',
          'ь' => "",
          'ы' => "y",
          'ъ' => "",
          'э' => "e",
          'ю' => 'yu',
          'я' => 'ya',
          'А' => 'A',
          'Б' => 'B',
          'В' => 'V',
          'Г' => 'G',
          'Д' => 'D',
          'Е' => 'E',
          'Ё' => 'YO',
          'Ж' => 'Zh',
          'З' => 'Z',
          'И' => 'I',
          'Й' => 'J',
          'К' => 'K',
          'Л' => 'L',
          'М' => 'M',
          'Н' => 'N',
          'О' => 'O',
          'П' => 'P',
          'Р' => 'R',
          'С' => 'S',
          'Т' => 'T',
          'У' => 'U',
          'Ф' => 'F',
          'Х' => 'X',
          'Ц' => 'C',
          'Ч' => 'CH',
          'Ш' => 'SH',
          'Щ' => 'SHH',
          'Ь' => "",
          'Ы' => "Y",
          'Ъ' => "",
          'Э' => "E",
          'Ю' => 'YU',
          'Я' => 'YA',
        );

        return strtr($s, $tr);
    }
//===============================================================================
// CURL functions
//===============================================================================
    protected static $_curlObject = false;

    /**
     * Возвращает содержимое внешнего файла
     **/
    public static function getHttpDocument($path, $decode = false)
    {
        $ch = self::getCurl($path);
        $ret = self::download($ch, 20);
        $res = new stdClass();
        $res->info = curl_getinfo($ch);
        if ($res->info['http_code'] >= 400) {
            if (isset($ret)) {
                $res->data = $ret;
            } else {
                $res->data = false;
            }
        } else {
            try {
                $res->data = ($decode) ? @iconv('GBK', 'UTF-8//IGNORE', $ret) : $ret;
            } catch (Exception $e) {
                if (isset($ret)) {
                    $res->data = $ret;
                } else {
                    $res->data = false;
                }
            }
        }
        unset ($ret);
        return $res;
    }

    /**
     * Возвращает ресурс curl c базовыми настройкми
     **/
    public static function getCurl($path)
    {
        try {
            if (gettype(self::$_curlObject) != 'resource') {
                $ch = curl_init($path);
                Profiler::message('curl', 'new');
                self::$_curlObject = $ch;
            } else {
                $ch = self::$_curlObject;
                Profiler::message('curl', 'reuse');
                curl_setopt($ch, CURLOPT_URL, $path);
            }
            curl_setopt(
              $ch,
              CURLOPT_USERAGENT,
              'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0'
            );
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//      curl_setopt($ch, CURLOPT_HEADER, TRUE); // Ото ж выводить ли хэдер в контент
            curl_setopt($ch, CURLOPT_ENCODING, ''); //gzip,deflate
            //curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, false);
// Speed-up
//      curl_setopt($ch, CURLOPT_TCP_NODELAY, TRUE);
            curl_setopt($ch, CURLOPT_NOPROGRESS, true);
            curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 3600);
//---------
            $header = array();
            $header[] = "Connection: keep-alive";
            $header[] = "Keep-Alive: 30";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            return $ch;
        } catch (Exception $e) {
            throw new CHttpException (500, 'System error: curl not installed or inited.');
        }
    }

    /**
     * Служебная функция.
     * Выполняет запрос, обрабатывая редиректы.
     * Есть параметр CURLOPT_FOLLOWLOCATION, который нельзя применять в safe_mode
     * или когда задано open_basedir. Эта функция полвзояет обойти данное ограничение
     **/
    protected static function download( /*resource*/
      $ch, /*int*/
      $maxredirect = null
    ) {
        $mr = $maxredirect === null ? 20 : intval($maxredirect);
        $open_basedir = ini_get('open_basedir');
        $safe_mode = ini_get('safe_mode');
        if (($open_basedir == '') && (in_array($safe_mode, array('0', 'Off')))) {
            curl_setopt($ch, CURLOPT_MAXREDIRS, $mr);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);
        } else {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            if ($mr > 0) {
                $newurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                $originalUrl = $newurl;
                $rch = curl_copy_handle($ch);
                curl_setopt($rch, CURLOPT_HEADER, true);
                curl_setopt($rch, CURLOPT_NOBODY, true);
                curl_setopt($rch, CURLOPT_FORBID_REUSE, false);
                curl_setopt($rch, CURLOPT_RETURNTRANSFER, true);
                do {
                    curl_setopt($rch, CURLOPT_URL, $newurl);
                    $header = curl_exec($rch);
                    if (curl_errno($rch)) {
                        $code = 0;
                    } else {
                        $code = curl_getinfo($rch, CURLINFO_HTTP_CODE);
                        if ($code == 301 || $code == 302) {
                            preg_match('/Location:(.*?)[\n\r]/i', $header, $matches);
                            $url = trim(array_pop($matches));
                            if (preg_match('/http[s]*:\/\/.+?(?=\/)/i', $url)) {
                                $newurl = $url;
                            } else {
                                preg_match('/http[s]*:\/\/.+?(?=\/)/i', $newurl, $m);
                                $newurl = trim(array_pop($m)) . $url;
                            }
                        } else {
                            $code = 0;
                        }
                    }
                } while ($code && --$mr);
                curl_close($rch);
                if (!$mr) {
                    if ($maxredirect === null) {
                        trigger_error(
                          'Too many redirects. When following redirects, libcurl hit the maximum amount.',
                          E_USER_WARNING
                        );
                    }
                    return false;
                }
                $debug = DSConfig::getVal('site_debug') == 1;
                if ($debug) {
                    echo '<div>';
                    echo '<pre>' . $originalUrl . '</pre>';
                }
                if ($debug) {
                    echo '</div>';
                }
                curl_setopt($ch, CURLOPT_URL, $newurl);
            }
        }
        $execRes = curl_exec($ch);
        if (curl_errno($ch) > 0) {
            //throw new CHttpException(500, curl_error($ch));
            return $execRes;
        }
        return $execRes;
    }

    public static function closeCurl()
    {
        if (gettype(self::$_curlObject) == 'resource') {
            curl_close(self::$_curlObject);
            Profiler::message('curl', 'closed');
        }
    }

    public static function getHelp($id)
    {
        $url = preg_replace('/\?.*/', '', Yii::app()->request->getRequestUri()) . '/' . $id;
        echo "<div class='get-help'><a href='javascript:void(0);' onclick='helpGoTo(\"" . $url . "\",true); return false;' title='" . Yii::t(
            'main',
            'Справка'
          ) . "'><i class='icon-question-sign'></i></a></div>";
    }

    public static function safe($string)
    {
        return $string;
    }

    protected static function getTanslationArrayHash($translationsArray)
    {
        return md5(print_r($translationsArray, true));
    }

    public static function getRemoteTranslation($translationsArray)
    {
        // ==== post and get data to translator ===========================
        $tanslationArrayHash = self::getTanslationArrayHash($translationsArray);
        $cache = Yii::app()->cache->get('translationArray-' . $tanslationArrayHash);
        if (($cache == false) || Yii::app()->user->notInRole(array('guest', 'user'))) {
            $translator_block_mode_url = DSConfig::getVal('translator_block_mode_url');
            $url = $translator_block_mode_url;
            $post = urlencode(convert_uuencode(gzcompress(serialize($translationsArray), 9)));
            $l = strlen($post);
            //$post =urlencode(serialize($matches));
            $ch = Utils::getCurl($url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_POST, true);
            if (YII_DEBUG && 0) {
                curl_setopt($ch, CURLOPT_COOKIE, 'XDEBUG_SESSION=Alexy');
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('translationArray' => $post));
//===============================================
            $fHeader = fopen('php://temp', 'w+');
            $fBody = fopen('php://temp', 'w+');
            curl_setopt($ch, CURLOPT_WRITEHEADER, $fHeader);
            curl_setopt($ch, CURLOPT_FILE, $fBody);
//===============================================
            curl_exec($ch);
            if (curl_errno($ch) > 0) {
                throw new CHttpException(500, curl_error($ch));
            }
            fseek($fBody, 0);
            $fStat = fstat($fBody);
            $ret = false;
            if ($fStat['size'] > 0) {
                $ret = fread($fBody, $fStat['size']);
            }
//===============================================
            fseek($fHeader, 0);
            $fStat = fstat($fHeader);
            if ($fStat['size'] > 0) {
                $headers = fread($fHeader, $fStat['size']);
                if (preg_match('/Content\-Time:\s*(\d+)/i', $headers, $headerVal)) {
                    Profiler::message('Proxy Content-Time', $headerVal[1]);
                }
            }
            fclose($fHeader);
            fclose($fBody);
            unset ($fStat);
//===============================================
            $res = new stdClass();
            $res->info = curl_getinfo($ch);
            if (($res->info['http_code'] >= 400)) {
                if (isset($ret)) {
                    $res->data = $ret;
                } else {
                    $res->data = false;
                }
            } else {
                $res->data = $ret;
            }
            unset ($ret);
            Utils::closeCurl();
            Yii::app()->cache->set('translationArray-' . $tanslationArrayHash, $res, 3600);
        } else {
            $res = $cache;
        }
        return $res;
    }

    public static function getRemoteTranslationPlain($query, $from, $to) {
        $remoteQuery = array(
          array(
            0 => Yii::app()->DanVitTranslator->markupTranslation(
                $query,
                $from,
                $to,
                $query,
                'parseQuery',
                'plain',
                0,
                false
              ),
          ),
        );
        $res = self::getRemoteTranslation($remoteQuery);
// ==== end of post and get data to translator ====================
        if (($res->data) && ($res->info['http_code'] < 400)) {
            $translations = unserialize($res->data);
            if (isset($translations[0][1])) {
                $result = $translations[0][1];
            } else {
                $result = $remoteQuery[0][0];
            }
        } else {
            $result = $remoteQuery[0][0];
        }
        $res= self::removeOnlineTranslation($result);
        return $res;
    }

    public static function translationAddClearTag($translation)
    {
        if (DSConfig::getVal('translator_block_mode_enabled') == 1) {
            $res = preg_replace('/(editable\s*=\s*(?:"|&quot;)[01](?:"|&quot;))/is', '$1 clear="1"', $translation);
        } else {
            $res = Utils::removeOnlineTranslation($translation);
        }
        return $res;
    }
    public static function langToLangName($lang) {
        switch ($lang) {
            case 'ru': return 'RUS';break;
            case 'en': return 'ENG';break;
            case 'zh': return 'CHS';break;
            case 'he': return 'HEB';break;
            case 'de': return 'DEU';break;
            case 'fr': return 'FRA';break;
            case 'es': return 'ESP';break;
            default: return 'RUS';break;
        }
    }

    public static function translateFuzzyBrand($s) {
        $tryWord=strtolower(self::translit($s));
        $brands=Brands::model()->findAllBySql('select lower(name) as name, lower(query) as query from brands',array());

        $possibleWords=array();
        foreach($brands as $brand)
        {
            if(levenshtein(metaphone($tryWord), metaphone($brand->name)) < mb_strlen(metaphone($tryWord))/2)
            {
                if(levenshtein($tryWord, $brand->name) < mb_strlen($tryWord)/3)
                {
                    $possibleWords[] = $brand->name;
                }
            }
        }

        $similarity = 0;
        $meta_similarity = 0;
        $min_levenshtein = 1000;
        $meta_min_levenshtein = 1000;

        foreach($possibleWords as $possibleWord)
        {
            $min_levenshtein = min($min_levenshtein, levenshtein($possibleWord, $tryWord));
        }


        foreach($possibleWords as $possibleWord)
        {
            if(levenshtein($possibleWord, $tryWord) == $min_levenshtein)
            {
                $similarity = max($similarity, similar_text($possibleWord, $tryWord));
            }
        }

        $result=array();

        foreach($possibleWords as $possibleWord)
        {
            if(levenshtein($possibleWord, $tryWord) <= $min_levenshtein)
            {
                if(similar_text($possibleWord, $tryWord) >= $similarity)
                {
                    $result[] = $possibleWord;
                }
            }
        }


        foreach($result as $n)
        {
            $meta_min_levenshtein = min($meta_min_levenshtein, levenshtein(metaphone($n), metaphone($tryWord)));
        }

        foreach($result as $n)
        {
            if(levenshtein($n, $tryWord) == $meta_min_levenshtein)
            {
                $meta_similarity = max($meta_similarity, similar_text(metaphone($n), metaphone($tryWord)));
            }
        }

        $meta_result=array();
        foreach($result as $k)
        {
            if(levenshtein(metaphone($k), metaphone($tryWord)) <= $meta_min_levenshtein)
            {
                $keySimilarity=similar_text(metaphone($k), metaphone($tryWord));
                if($keySimilarity >= $meta_similarity)
                {
                    $meta_result[$k] = $keySimilarity;
                }
            }
        }
        if (count($meta_result)>0) {
        $res = array_search(max($meta_result), $meta_result);
        } else {
            $res=$s;
        }

        return $res;
    }
}