<?php

/**
 * This is the model class for table "cms_pages_content".
 *
 * The followings are the available columns in table 'cms_pages_content':
 * @property integer $id
 * @property string $page_id
 * @property string $lang
 * @property string $content_data
 * @property string $title
 * @property string $description
 * @property string $keywords
 *
 * The followings are the available model relations:
 * @property CmsPages $page
 */
class customCmsPagesContent extends CActiveRecord
{
    public $link;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_pages_content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id', 'required'),
			array('page_id', 'length', 'max'=>255),
			array('lang', 'length', 'max'=>8),
			array('title, description, keywords', 'length', 'max'=>1024),
			array('content_data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, page_id, lang, content_data, title, description, keywords', 'safe', 'on'=>'search,update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'page' => array(self::BELONGS_TO, 'CmsPages', 'page_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main','id контента страницы'),
			'page_id' => Yii::t('main','id страницы контента'),
			'lang' => Yii::t('main','Язык контента страницы (* - любой)'),
			'content_data' => Yii::t('main','php-код контента (синтаксис View)'),
			'title' => Yii::t('main','title страницы'),
			'description' => Yii::t('main','description страницы'),
			'keywords' => Yii::t('main','keywords страницы'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->select="t.*,concat(case when lang<>'*' then concat('/',lang) else '' end,'/article/',(select url from cms_pages where cms_pages.page_id=t.page_id LIMIT 1)) as link";
		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id,true);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('content_data',$this->content_data,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('keywords',$this->keywords,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmsPagesContent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}
  public function save($runValidation = TRUE, $attributes = NULL) {
//    Yii::app()->cache->set('cmsCustomContent-' . $id, $content, 600);
    return parent::save();
  }
}
