<?php

/**
 * This is the model class for table "categories_prices".
 *
 * The followings are the available columns in table 'categories_prices':
 * @property string $id
 * @property string $cid
 * @property string $query
 * @property double $begin0
 * @property double $end0
 * @property double $percent0
 * @property double $begin1
 * @property double $end1
 * @property double $percent1
 * @property double $begin2
 * @property double $end2
 * @property double $percent2
 * @property double $begin3
 * @property double $end3
 * @property double $percent3
 * @property double $begin4
 * @property double $end4
 * @property double $percent4
 * @property string $date
 */
class customCategoriesPrices extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categories_prices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('begin0, end0, percent0, begin1, end1, percent1, begin2, end2, percent2, begin3, end3, percent3, begin4, end4, percent4', 'numerical'),
			array('cid', 'length', 'max'=>20),
			array('query', 'length', 'max'=>220),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cid, query, begin0, end0, percent0, begin1, end1, percent1, begin2, end2, percent2, begin3, end3, percent3, begin4, end4, percent4, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cid' => 'Cid',
			'query' => 'Query',
			'begin0' => 'Begin0',
			'end0' => 'End0',
			'percent0' => 'Percent0',
			'begin1' => 'Begin1',
			'end1' => 'End1',
			'percent1' => 'Percent1',
			'begin2' => 'Begin2',
			'end2' => 'End2',
			'percent2' => 'Percent2',
			'begin3' => 'Begin3',
			'end3' => 'End3',
			'percent3' => 'Percent3',
			'begin4' => 'Begin4',
			'end4' => 'End4',
			'percent4' => 'Percent4',
			'date' => 'Дата последнего обновления',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('cid',$this->cid,true);
		$criteria->compare('query',$this->query,true);
		$criteria->compare('begin0',$this->begin0);
		$criteria->compare('end0',$this->end0);
		$criteria->compare('percent0',$this->percent0);
		$criteria->compare('begin1',$this->begin1);
		$criteria->compare('end1',$this->end1);
		$criteria->compare('percent1',$this->percent1);
		$criteria->compare('begin2',$this->begin2);
		$criteria->compare('end2',$this->end2);
		$criteria->compare('percent2',$this->percent2);
		$criteria->compare('begin3',$this->begin3);
		$criteria->compare('end3',$this->end3);
		$criteria->compare('percent3',$this->percent3);
		$criteria->compare('begin4',$this->begin4);
		$criteria->compare('end4',$this->end4);
		$criteria->compare('percent4',$this->percent4);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategoriesPrices the static model class
	 */
    public static function model($className = __CLASS__)
    {
        return parent::model(preg_replace('/^custom/', '', $className));
    }

    public static function getPricesRangeForSearch($cid,$query=false){
      $intCid=((!$cid)?0:$cid);
      $intQuery=((!$query)?'':$query);
      $minPrice=0;
      $maxPrice=999999;
      $res=Yii::app()->db->createCommand("select * from categories_prices cp where (cp.cid=:cid)
                                          and (cp.query=:query or  :query='') LIMIT 1")
        ->bindParam(':cid',$intCid,PDO::PARAM_INT)
        ->bindParam(':query',$intQuery,PDO::PARAM_STR)
        ->queryRow();
      if ($res) {
          $minPrice=round($res['end0']*0.6); // - половина минимальной цены
          $maxPrice=round($res['begin4']*1.4); // - в два раза больше максимальной цены
      }
        return array($minPrice,$maxPrice);
    }

    public static function getPricesRangeForArray($priceRange,$items=false){
        // Calculating min and max prices for search
        if (isset($priceRange[0]) && isset($priceRange[4])) {
            $minPrice=round($priceRange[0]->end/2); // - половина минимальной цены
            $maxPrice=round($priceRange[4]->start*2); // - в два раза больше максимальной цены
            $stepPrice=round(($maxPrice-$minPrice)/20);
        } elseif ($items && is_array($items)) {
            $avgPriceArr=array();
            foreach ($items as $item) {
                $avgPriceArr[]=min($item->userPrice,$item->userPromotionPrice);
            }
            $avgPrice=Weights::median($avgPriceArr);
            $minPrice=round($avgPrice/6);
            $maxPrice=round($avgPrice*10);
            $stepPrice=round(($maxPrice-$minPrice)/20);
        } else {
            $minPrice=0;
            $maxPrice=10000;
            $stepPrice=round(($maxPrice-$minPrice)/20);
        }
        return array($minPrice,$maxPrice,$stepPrice);
    }
}
