<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Category.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "categories".
 *
 * The followings are the available columns in table 'categories':
 * @property integer $cid
 * @property string $ru
 * @property string $en
 * @property integer $parent
 * @property integer $is_parent
 * @property integer $onmain
 * @property integer $status
 */
class customCategory extends CActiveRecord {
  /**
   * Returns the static model of the specified AR class.
   * @return Category the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'categories';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('cid, parent, is_parent', 'numerical', 'integerOnly' => TRUE),
      array(
        'meta_desc_ru,meta_keeword_ru,page_title_ru,meta_desc_en,
                                    meta_keeword_en,page_desc_en',
        'length',
        'max' => 300
      ),
      array('page_title_en,page_desc_ru,url,query', 'safe'),
      array('onmain, status', 'boolean'),
      array('ru, en', 'length', 'max' => 256),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('cid, ru, en, parent, is_parent, onmain, status', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'ww' => array(self::HAS_MANY, 'Weights', 'cid'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'cid' => Yii::t('main', 'Id категории'),
      'ru' => Yii::t('main', 'Русский перевод'),
      'en' => Yii::t('main', 'Англ. перевод'),
      'parent' => Yii::t('main', 'Родительская кат.'),
      'is_parent' => Yii::t('main', 'Является родителем'),
      'onmain' => Yii::t('main', 'На главной'),
      'status' => Yii::t('main', 'Включено'),
      'meta_desc_ru' => Yii::t('main', 'Meta description'),
      'meta_desc_en' => Yii::t('main', 'Meta description'),
      'meta_keeword_ru' => Yii::t('main', 'Meta keywords'),
      'meta_keeword_en' => Yii::t('main', 'Meta keywords'),
      'page_title_ru' => Yii::t('main', 'Title страницы'),
      'page_title_en' => Yii::t('main', 'Title страницы'),
      'page_desc_en' => Yii::t('main', 'Описание категории'),
      'page_desc_ru' => Yii::t('main', 'Описание категории'),
      'url' => Yii::t('main', 'Адрес страницы'),
      'query' => Yii::t('main', 'Запрос')
    );
  }

  public static function getUrl($model) {
    if (isset($model->url) && $model->url !== '') {
      return $model->url;
    }
    else {
      $url = trim(strtolower($model->en));
      $url = strtr($url, array(
        ' ' => '-',
        '/' => '-',
        '_' => '-',
        ',' => '',
        '\'' => '',
      ));
      $url = strtr($url, array(
        '---' => '-',
        '--' => '-',
      ));
      // Очень спорно!!!
        $model = MainMenu::model()->findByPk($model->id);
      $model->url = $url;
      $model->update();
      return $url;
    }
  }


  public static function getParents($cat, $result, $level) {
    $model = Category::model()->find('cid=:cid and cid<>0', array(':cid' => $cat->parent));
    if ($model) {
      $res = new StdClass();
      $res->cid = $model->cid;
      $res->url = self::getUrl($model);
      $res->parent = $model->parent;
      $res->name = $model->{Utils::TransLang()};
      $result[$level] = $res;

      if ($model->parent != 0) {
        $result = self::getParents($res, $result, $level + 1);
      }
    }
    return $result;
  }

  public static function getPath($cat, $result='') {
    $model = Category::model()->find('cid=:cid and cid<>0', array(':cid' => $cat));
    if ($model) {
      if ($result=='') {
        $result=$model[Utils::TransLang()];
      } else {
      $result=$model[Utils::TransLang()].', '.$result;
      }
      if ($model->parent != 0) {
        $result = self::getPath($model->parent, $result);
      }
    }
    return $result;
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('cid', $this->cid);
    $criteria->compare('ru', $this->ru, TRUE);
    $criteria->compare('en', $this->en, TRUE);
    $criteria->compare('parent', $this->parent);
    $criteria->compare('is_parent', $this->is_parent);
    $criteria->compare('onmain', $this->onmain);
    $criteria->compare('status', $this->status);
    $criteria->compare('url', $this->url);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
      'pagination' => array(
        'pageSize' => 100,
      ),
    ));
  }

  public static function getTree($compact, $lang, $fromId = 1, $status = 1, $depth = 3, $currlevel = 0) {
    $result = array();
    $currlevel = $currlevel + 1;
    $data = Yii::app()->db->createCommand("
select
 id as pkid,cid as id,cid,ru,en,parent,status,onmain,meta_desc_ru,meta_keeword_ru,page_title_ru,meta_desc_en,meta_keeword_en,page_title_en,page_desc_ru,
  page_desc_en,url,zh,query,level
from categories where (parent = case when :id=1 then 0 else :id end and cid<>:id) and (status=1 or :status=0) and (onmain=1 or :status=0)
order by cid")
      ->bindParam(":id", $fromId, PDO::PARAM_INT)
      ->bindParam(":status", $status, PDO::PARAM_INT)
      ->queryAll();
    foreach ($data as $menucat) {
      $result[$menucat['id']] = $menucat;
      //if ($menucat['level'] <= $depth) {
      if ($currlevel < $depth) {
        $result[$menucat['id']]['children'] = self::getTree($compact,$lang, $menucat['cid'], $status, $depth, $currlevel);
      }
      $result[$menucat['id']]['view_text']=$menucat[$lang];
      if ($compact) {
        unset($result[$menucat['id']]['ru']);
        unset($result[$menucat['id']]['en']);
        unset($result[$menucat['id']]['zh']);
        unset($result[$menucat['id']]['meta_desc_ru']);
        unset($result[$menucat['id']]['meta_keeword_ru']);
        unset($result[$menucat['id']]['page_title_ru']);
        unset($result[$menucat['id']]['page_desc_ru']);

        unset($result[$menucat['id']]['meta_desc_en']);
        unset($result[$menucat['id']]['meta_keeword_en']);
        unset($result[$menucat['id']]['page_title_en']);
        unset($result[$menucat['id']]['page_desc_en']);
      }
      if (!$compact) {
      if ($menucat['onmain'] == 1 and $menucat['status'] == 1) {
        $result[$menucat['id']]['text'] = CHtml::link('<b>' . $menucat[$lang] . '</b>', array(
          "category/update",
          "id" => $menucat['pkid']
        ), array("onclick" => "getContent(this,\"".Yii::t('main','Категория')." №" . $menucat['cid'] . "\");return false;","title"=>Yii::t('main',"Изменить параметры категории")));
      }
      elseif ($menucat['status'] == 0) {
        $result[$menucat['id']]['text'] = CHtml::link('<i>' . $menucat[$lang] . '</i>', array(
          "category/update",
          "id" => $menucat['pkid']
        ), array("onclick" => "getContent(this,\"".Yii::t('main','Категория')." №" . $menucat['cid'] . "\");return false;","title"=>Yii::t('main',"Изменить параметры категории")));
      }
      else {
        $result[$menucat['id']]['text'] = CHtml::link($menucat[$lang], array(
          "category/update",
          "id" => $menucat['pkid']
        ), array("onclick" => "getContent(this,\"".Yii::t('main','Категория')." №" . $menucat['cid'] . "\");return false;","title"=>Yii::t('main',"Изменить параметры категории")));
      }
    }
      $result[$menucat['id']]['hasChildren'] = isset($result[$menucat['id']]['children']);
      $result[$menucat['id']]['expanded'] = FALSE;
    }
    return $result;
  }
}