<?php

/**
 * This is the model class for table "cms_email_events".
 *
 * The followings are the available columns in table 'cms_email_events':
 * @property integer $id
 * @property string $mailevent_name
 * @property string $template
 * @property string $class
 * @property string $action
 * @property string $condition
 * @property string $recipients
 * @property integer $enabled
 * @property string $regular
 */
class customCmsEmailEvents extends CActiveRecord
{
    private static $testMode=false;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cms_email_events';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('class, action', 'required'),
          array('enabled', 'numerical', 'integerOnly' => true),
          array('class, action', 'length', 'max' => 255),
          array('regular', 'length', 'max' => 20),
          array('template, condition, recipients, tests', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'id, template, class, action, condition, recipients, tests, enabled, regular',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id'             => 'ID',
          'template'       => 'Шаблон сообщения (синтаксис View)',
          'class'          => 'Класс события',
          'action'         => 'Тип события',
          'condition'      => 'Условие',
          'recipients'     => 'Получатели',
          'tests'          => 'Тест',
          'enabled'        => 'Включено',
          'regular'        => 'Частота повторения, сек (0 - не повторять)',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('template', $this->template, true);
        $criteria->compare('class', $this->class, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('condition', $this->condition, true);
        $criteria->compare('recipients', $this->recipients, true);
        $criteria->compare('tests', $this->recipients, true);
        $criteria->compare('enabled', $this->enabled);
        $criteria->compare('regular', $this->regular, true);

        return new CActiveDataProvider(
          $this, array(
            'criteria'   => $criteria,
            'pagination' => array(
              'pageSize' => 25,
            ),
          )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmsEmailEvents the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model(preg_replace('/^custom/', '', $className));
    }

    private static function render($text, $_data_)
    {
        if (is_array($_data_)) {
            extract($_data_, EXTR_PREFIX_SAME, 'data');
        } else {
            $data = $_data_;
        }
        $open_basedir = ini_get('open_basedir');
        $safe_mode = ini_get('safe_mode');
        try {
            if ($open_basedir || $safe_mode) {
                $tFileName = tempnam(
                  YiiBase::getPathOfAlias('webroot') . '/upload/',
                  'php'
                ) or die('could not create file');
                $tempFile = fopen($tFileName, 'w+');
            } else {
                $tempFile = tmpfile();
            }
            $metaDatas = stream_get_meta_data($tempFile);
            $tmpFilename = $metaDatas['uri'];
            fwrite($tempFile, $text);
//      fseek($tempFile, 0);
            ob_start();
            ob_implicit_flush(false);
            $execRes=exec("php -l $tmpFilename");
            if (substr($execRes, 0, 28) == "No syntax errors detected in") {
                include $tmpFilename;
            } else {
                return false;
            }
            $content = ob_get_clean();
            fclose($tempFile); // this removes the file
            if (isset($tFileName)) {
                unlink($tFileName);
            }
            return $content;
        } catch (Exception $e) {
            return print_r($e, true);
        }
    }

    public static function emailEventTest($id)
    {
        $event = self::model()->findByPk($id);
        $result = false;
        if (!$event) {
            return false;
        }
        self::$testMode=true;
        $result = @eval($event->tests);
        return $result;
    }

    private static function isEmailEventCondition($new, $old, $condition)
    {
        $result = false;
        $result = @eval($condition);
        return $result;
    }

    private static function getEmailEventRecipients($new, $old, $condition)
    {
        $result = false;
        $inExcludeConditionArray = array();
        $inIncludeConditionArray = array();
        $inConditionArray = explode(',', $condition);
        if (!$inConditionArray || !count($inConditionArray)) {
            return false;
        }
        foreach ($inConditionArray as $i => $inCondition) {
            $inConditionArray[$i] = trim($inConditionArray[$i]);
            if (strpos($inConditionArray[$i], '$') === 0) {
                $tmp = @eval('return ' . $inConditionArray[$i] . ';');
                if (isset($tmp) && strlen($tmp)) {
                    $inIncludeConditionArray[] = "'" . $tmp . "'";
                }
            } elseif (strpos($inConditionArray[$i], '-') === 0) {
                $inExcludeConditionArray[] = "'" . preg_replace('/^\-/', '', $inConditionArray[$i]) . "'";
            } else {
                $inIncludeConditionArray[] = "'" . $inConditionArray[$i] . "'";
            }
        }
        $inExcludeConditionArray = array_unique($inExcludeConditionArray);
        $inIncludeConditionArray = array_unique($inIncludeConditionArray);
        $inInclude = implode(',', $inIncludeConditionArray);
        $inExclude = implode(',', $inExcludeConditionArray);
        if ($inInclude || $inExclude) {
            $sql = 'select distinct * from users where status =1 '
              . (($inInclude) ? ' and (uid in(' . $inInclude . ') or role in (' . $inInclude . '))' : '')
              . (($inExclude) ? ' and (uid not in(' . $inExclude . ') or role not in (' . $inExclude . '))' : '');
            $result = Users::model()->findAllBySql($sql);
        }
        return $result;
    }

    private static function sendEmailEventMessage($event, $new, $old, $recipients, $comparedFields,$queue)
    {
        $postingId=($queue?uniqid():null);
        foreach ($recipients as $recipient) {
            try {
                $from = 'support@' . DSConfig::getVal('site_domain');
                $fromName = Yii::t('main', 'Cлужба поддержки') . ' ' . DSConfig::getVal('site_name');
                $subj = Yii::t('main', 'Сообщение от службы поддержки') . ' ' . DSConfig::getVal('site_name');
                $body = @self::render(
                  $event->template,
                  array(
                    'event'          => $event,
                    'new'            => $new,
                    'old'            => $old,
                    'recipient'      => $recipient,
                    'comparedFields' => $comparedFields
                  )
                );
                if ($body) {
                    if (preg_match('/<from>(.*?)<\/from>/is', $body, $matches)) {
                        $from = $matches[1];
                    }
                    if (preg_match('/<fromName>(.*?)<\/fromName>/is', $body, $matches)) {
                        $fromName = $matches[1];
                    }
                    if (preg_match('/<subj>(.*?)<\/subj>/is', $body, $matches)) {
                        $subj = $matches[1];
                    }
                    $body = preg_replace('/\s*<mail>.*?<\/mail>\s*/is', '', $body);
                    Mail::sendMail($from, $fromName, $recipient->email, $subj, $body, $queue, $event->id, $postingId);
                }
            } catch (Exception $e) {
                continue;
            }
        }
        return true;
    }

    public static function emailProcessEvents($sender, $action, $queue=false)
    {
        try {
            $result = false;
            $class = get_class($sender);
            if ($action == 'beforeUpdate') {
                $old = $sender->findByPk($sender->primaryKey);
            } else {
                $old = false;
            }
            $events = self::model()->findAll(
              'class=:class and action=:action and enabled=1',
              array(':class' => $class, ':action' => $action)
            );
            if ($events) {
                foreach ($events as $i => $event) {
                    if (self::isEmailEventCondition($sender, $old, $event->condition)) {
                        $recipients = self::getEmailEventRecipients($sender, $old, $event->recipients);
                        if ($recipients) {
                            $comparedFields = self::compareObjects($sender, $old, $event->relevant_fields);
                            if ($comparedFields) {
                                $result = true;
                                self::sendEmailEventMessage(
                                  $event,
                                  $sender,
                                  $old,
                                  $recipients,
                                  $comparedFields,
                                  $queue
                                );
                            }
                        }
                    }
                }
            }
            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    private static function compareObjects($new, $old, $relevantFields)
    {
        $result = array();
        $relevantFieldsArray = preg_split('/\s*,\s*/', $relevantFields);
        if (is_array($relevantFieldsArray)) {
            foreach ($relevantFieldsArray as $field) {
                if ($old && $new) {
                      if (isset($old->$field) && isset($new->$field)) {
                          if (($old->$field!==$new->$field) || self::$testMode) {
                              $result[$field]=new stdClass();

                              $result[$field]->label=(method_exists($new,'getAttributeLabel')?$new->getAttributeLabel($field):$field);
                              $result[$field]->old=$old->$field;
                              $result[$field]->new=$new->$field;
                          }
                      }
                } elseif ($new) {
                    if (isset($new->$field)) {
                            $result[$field]=new stdClass();
                            $result[$field]->label=(method_exists($new,'getAttributeLabel')?$new->getAttributeLabel($field):$field);
                            $result[$field]->old=$new->$field;
                            $result[$field]->new=$new->$field;
                    }
                }
            }

        }
        if (count($result)) {
            return $result;
        } else {
            return false;
        }
    }
}
