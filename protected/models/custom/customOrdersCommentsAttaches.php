<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersCommentsAttaches.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "orders_comments_attaches".
 *
 * The followings are the available columns in table 'orders_comments_attaches':
 * @property string $id
 * @property string $comment_id
 * @property string $attach
 *
 * The followings are the available model relations:
 * @property OrdersComments $comment
 */
class customOrdersCommentsAttaches extends DSEventableActiveRecord
{
public $uploadedFile;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders_comments_attaches';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comment_id', 'required'),
			array('comment_id', 'length', 'max'=>20),
//			array('attach', 'safe'),
//            array('uploadedFile', 'safe'),
            array('attach', 'length', 'max'=>1024*1024),
          array('uploadedFile', 'file', 'types'=>'jpg, png, gif', 'allowEmpty'=>false, 'safe'=>false,
            //  'mimeTypes'=>'image/gif, image/jpeg, image/jpg, image/png',
            //  'wrongMimeType'=>Yii::t('main','Изображение должно быть в формате jpg или png'),
            'wrongType' =>Yii::t('main','Изображение должно быть в формате jpg, png или gif'),
            'tooLarge'=>Yii::t('main','Размер изображения не должен превышать 1МБ'),
            'tooSmall'=>Yii::t('main','Размер изображения не должен быть менее 512 байт'),
            'maxSize'=>1024*1024,
            'minSize'=>512,
              'on'=>'insert'), // this will allow empty field when page is update (remember here i create scenario update) 'on'=>'insert'
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, comment_id, attach', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comment' => array(self::BELONGS_TO, 'OrdersComments', 'comment_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main','ID приложения к сообщению'),
			'comment_id' => Yii::t('main','ID комментария'),
			'attach' => 'jpg, png',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('comment_id',$this->comment_id,true);
		$criteria->compare('attach',$this->attach,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdersCommentsAttaches the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}

  public static function getAttachesPreview($commentId) {
    $attaches = self::model()->findAll('comment_id=:comment_id', array(':comment_id' => $commentId));
    $res='';
    if ($attaches) {
      foreach ($attaches as $id=>$attach) {
        $res=$res.Yii::app()->controller->renderPartial('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.orders.commentattachpreview', array('isItem'=>0,'attach' => $attach), TRUE);
      }
    }
    return $res;
  }

    public static function getAdminLink($id, $external = false)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/orders/view/id/' . $id . '&tabName=' .Yii::t('admin','Заказ '). $order->uid.'-'.$order->id;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/orders/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр заказа'
                ) . '" onclick="getContent(this,\'' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '\');return false;"><i class="icon-shopping-cart"></i>&nbsp;' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }

    public static function getUserLink($id)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            return 'http://' . DSConfig::getVal(
              'site_domain'
            ) . '/cabinet/orders/view/id/' . $id;
        } else {
            return '<a href="#">' . Yii::t('main', 'Ошибка') . '</a>';
        }
    }

}
