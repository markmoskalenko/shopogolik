<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SiteLog.php">
* </description>
**********************************************************************************************************************/?>
<?php
class customSiteLog {
  public static function doHttpLog() {
    $uid = Yii::app()->user->id;
    if ($uid == NULL) {
      $uid = -1;
    }
    if (isset($GLOBALS['_COOKIE']['PHPSESSID'])) {
      $session = $GLOBALS['_COOKIE']['PHPSESSID'];
    }
    else {
      $session = '';
    }
    if (isset($GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI']) && ($GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI'] != '')) {
      $url = $GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI'];
    }
    elseif (isset($_SERVER['REQUEST_URI']) && ($_SERVER['REQUEST_URI'] != '')) {
      $url = $_SERVER['REQUEST_URI'];
    }
    else {
      $url = '';
    }

    if (isset($GLOBALS['HTTP_SERVER_VARS']['REMOTE_ADDR']) && ($GLOBALS['HTTP_SERVER_VARS']['REMOTE_ADDR'] != '')) {
      $ip = $GLOBALS['HTTP_SERVER_VARS']['REMOTE_ADDR'];
    }
    elseif (isset($_SERVER['REMOTE_ADDR']) && ($_SERVER['REMOTE_ADDR'] != '')) {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    else {
      $ip = getenv('HTTP_CLIENT_IP');
    }

    if (isset($GLOBALS['HTTP_SERVER_VARS']['HTTP_USER_AGENT']) && ($GLOBALS['HTTP_SERVER_VARS']['HTTP_USER_AGENT'] != '')) {
      $useragent = $GLOBALS['HTTP_SERVER_VARS']['HTTP_USER_AGENT'];
    }
    elseif (isset($_SERVER['HTTP_USER_AGENT']) && ($_SERVER['HTTP_USER_AGENT'] != '')) {
      $useragent = $_SERVER['HTTP_USER_AGENT'];
    }
    else {
      $useragent = '';
    }

    $command = Yii::app()->db->createCommand("INSERT INTO log_http_requests (session,url,ip,useragent,uid,`date`)
        VALUES (:session,:url,:ip,:useragent,:uid,now())")
      ->bindParam(':session', $session, PDO::PARAM_STR)
      ->bindParam(':url', $url, PDO::PARAM_STR)
      ->bindParam(':ip', $ip, PDO::PARAM_STR)
      ->bindParam(':useragent', $useragent, PDO::PARAM_STR)
      ->bindParam(':uid', $uid, PDO::PARAM_INT);
    $command->execute();
  }

  public static function doAPIKeyLog($type, $key) {
    $command = Yii::app()->db->createCommand("INSERT DELAYED INTO log_api_requests (`type`,`key`,`date`)
        VALUES (:type,:key,now())")
      ->bindParam(':type', $type, PDO::PARAM_STR)
      ->bindParam(':key', $key, PDO::PARAM_STR);
    $command->execute();
  }

  public static function doItemLog($item) {

Yii::app()->db->createCommand('delete QUICK from log_http_requests
                 WHERE log_http_requests.`date` < (CURDATE() - INTERVAL 30 DAY) LIMIT 500;')->execute();
Yii::app()->db->createCommand('delete QUICK from log_items_requests
                 WHERE log_items_requests.`date` < (CURDATE() - INTERVAL 14 DAY) LIMIT 500;')->execute();
Yii::app()->db->createCommand('delete QUICK from log_queries_requests
                       WHERE log_queries_requests.`date` < (CURDATE() - INTERVAL 30 DAY) LIMIT 20;')->execute();
Yii::app()->db->createCommand('delete QUICK from log_api_requests
                 WHERE log_api_requests.`date` < (CURDATE() - INTERVAL 30 DAY) LIMIT 500;')->execute();

      $uid = Yii::app()->user->id;
    if ($uid == NULL) {
      $uid = -1;
    }
    if (isset($GLOBALS['_COOKIE']['PHPSESSID'])) {
      $session = $GLOBALS['_COOKIE']['PHPSESSID'];
    }
    else {
      $session = '';
    }
    /*  `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
      `session` varchar(64) DEFAULT NULL,
      `uid` mediumint(9) DEFAULT NULL,
      `num_iid` bigint(20) NOT NULL,
      `cid` bigint(20) NOT NULL DEFAULT '0',
      `express_fee` float DEFAULT NULL,
      `price` float DEFAULT NULL,
      `promotion_price` float DEFAULT NULL,
      `nick` varchar(64) DEFAULT NULL,
      `pic_url` varchar(256) DEFAULT NULL,
      `title` varchar(1024) DEFAULT NULL,
      `prop_names` varchar(4000) DEFAULT NULL,
    */
    $title =Utils::removeOnlineTranslation($item->top_item->title);
    $prop_names = '';
    if (isset($item->top_item->props)) {
      foreach ($item->top_item->props as $pid => $prop) {
        $prop_name = preg_replace('/.*title.*?>(.+?)<.*/is', '$1', $prop->name) . ', ' . $pid;
        $prop_values = '';
        if (isset($prop->childs)) {
          foreach ($prop->childs as $value) {
            $prop_values = ', ' . preg_replace('/.*title.*?>(.+?)<.*/is', '$1', $value->name) . ', ' . $value->vid;
          }
        }
        $prop_names = $prop_names . ' ' . $prop_name . ' ' . $prop_values;
      }
    }
    $command = Yii::app()->db->createCommand("INSERT INTO log_item_requests (`date`,`session`,`uid`,`num_iid`,
  `cid`,`express_fee`,`price`,`promotion_price`,`nick`,`seller_rate`,
  `pic_url`,`title`,`prop_names`)
  VALUES (now(),:session,:uid,:num_iid,
  :cid,:express_fee,:price,:promotion_price,:nick,:seller_rate,
  :pic_url,:title,:prop_names)")
      ->bindParam(':session', $session, PDO::PARAM_STR)
      ->bindParam(':uid', $uid, PDO::PARAM_INT)
      ->bindParam(':num_iid', $item->top_item->num_iid, PDO::PARAM_STR)
      ->bindParam(':cid', $item->top_item->cid, PDO::PARAM_STR)
      ->bindParam(':express_fee', $item->taobao_item->express_price, PDO::PARAM_STR)
      ->bindParam(':price', $item->taobao_item->price, PDO::PARAM_STR)
      ->bindParam(':promotion_price', $item->taobao_item->promotion_price, PDO::PARAM_STR)
      ->bindParam(':nick', $item->taobao_item->nick, PDO::PARAM_STR)
      ->bindParam(':seller_rate', $item->taobao_item->seller_rate, PDO::PARAM_STR)
      ->bindParam(':pic_url', $item->taobao_item->pic_url, PDO::PARAM_STR)
      ->bindParam(':title', $title, PDO::PARAM_STR)
      ->bindParam(':prop_names', $prop_names, PDO::PARAM_STR);
    $command->execute();
    Yii::app()->db->createCommand('delete QUICK from log_item_requests
                 WHERE log_item_requests.`date` < (CURDATE() - INTERVAL 3 MONTH) LIMIT 10;')->execute();

  }

  public static function doItemsLog($searchRes) {
    if (!$searchRes) {
      return;
    }
    if (!isset($searchRes->items)) {
      return;
    }
    if (count($searchRes->items)<=0) {
          return;
    }
    Profiler::start('doItemsLog');
    $uid = Yii::app()->user->id;
    if ($uid == NULL) {
      $uid = -1;
    }
    if (isset($GLOBALS['_COOKIE']['PHPSESSID'])) {
      $session = $GLOBALS['_COOKIE']['PHPSESSID'];
    }
    else {
      $session = '';
    }

    $valuesToCheckExists= array();
      foreach ($searchRes->items as $item) {
          $valuesToCheckExists[]=$item->num_iid;
      }
    $notNeedToUpdateRes=Yii::app()->db->createCommand("select num_iid from log_items_requests where num_iid in (".implode(',',$valuesToCheckExists).")
    and `date`>(CURDATE()- INTERVAL 1 HOUR)")
    ->queryAll();
    $valuesToCheckExists= array();
    if ($notNeedToUpdateRes) {
        foreach ($notNeedToUpdateRes as $notNeedToUpdateItem) {
            $valuesToCheckExists[]=$notNeedToUpdateItem['num_iid'];
        }
    }
    $values = array();
    foreach ($searchRes->items as $item) {
      if ($item->cid <> 0) {
        $int_cid = $item->cid;
      }
      else {
        if ($searchRes->cid <> 0) {
          $int_cid = $searchRes->cid;
        }
        else {
          $int_cid = 0;
        }
      }
        if (!in_array($item->num_iid,$valuesToCheckExists)) {
            $values[] = "(now(),'" . $session . "'," . $uid . "," . $item->num_iid . "," . $int_cid . "," . $item->express_fee . "," . $item->price . ","
              . $item->promotion_price . ",'" . $item->pic_url . "'," . $item->seller_rate . ",'" . preg_replace('/\'/','\'',$searchRes->query) . "')";
        }
    }
    if (count($values) > 0) {
      try {
      //delayed ignore
        $sql = "insert DELAYED into log_items_requests (`date`,`session`,`uid`,`num_iid`,`cid`,`express_fee`,`price`,
  `promotion_price`,`pic_url`,`seller_rate`,`query`)
  values " . implode(',', $values)."
  on duplicate key update `date`=values(`date`), `express_fee`=values(express_fee), `price`=values(price),
  `promotion_price`=values(promotion_price),
  `seller_rate`=values(seller_rate)";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
      } catch (Exception $e) {
        Profiler::stop('doItemsLog');
      }
    }
    Profiler::stop('doItemsLog');
  }

  public static function doQueryLog($searchRes) {
    $uid = Yii::app()->user->id;
    if ($uid == NULL) {
      $uid = -1;
    }
    if (isset($GLOBALS['_COOKIE']['PHPSESSID'])) {
      $session = $GLOBALS['_COOKIE']['PHPSESSID'];
    }
    else {
      $session = '';
    }
    $command = Yii::app()->db->createCommand("INSERT DELAYED INTO log_queries_requests(`date`,`session`,`uid`,`res_count`,`cid`,`query`)
VALUES (now(),:session,:uid,:res_count,:cid,:query)")
      ->bindParam(':session', $session, PDO::PARAM_STR)
      ->bindParam(':uid', $uid, PDO::PARAM_INT)
      ->bindParam(':res_count', $searchRes->total_results, PDO::PARAM_INT)
      ->bindParam(':cid', $searchRes->cid, PDO::PARAM_INT)
      ->bindParam(':query', $searchRes->query, PDO::PARAM_STR);
    $command->execute();
  }

    public static function getTopSearchQueries($count=5) {
        $command = Yii::app()->db->createCommand("select ll.query, count(0) as cnt from log_queries_requests ll
where (ll.query is not null) and (ll.query !='') and ll.cid=0
group by ll.query
order by cnt desc limit ".$count);
        $result=$command->queryAll();
        return $result;
    }

}