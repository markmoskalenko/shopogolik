<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AccountForm.php">
* </description>
**********************************************************************************************************************/?>
<?php
class customAccountForm extends CFormModel {
    
    public $id_account;
    public $summ;
    public $password;
    
    function rules() {
        return array(
            array('id_account,summ,password','required','on'=>'transfer'),
            array('summ','numerical'),
        );    
    }
    
    function attributeLabels() 
    {
        return array(
            'id_account' => Yii::t('main','Номер счета в формате XXXX-XXXX').':',
            'summ'  => Yii::t('main','Сумма').'('.DSConfig::getVal('site_currency').'):',
            'password' => Yii::t('main','Ваш пароль').':',
        );    
    }
    
    
}


