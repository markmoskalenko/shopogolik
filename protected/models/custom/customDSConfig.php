<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DSConfig.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php
/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property string $id
 * @property string $label
 * @property string $value
 * @property string $default_value
 * @property integer $in_wizard
 */
class customDSConfig extends CActiveRecord {
  protected static $_cache = array();
  protected static $_currency = FALSE;
  protected static $_currs = FALSE;
  protected static $_currs_format = FALSE;

  /**
   * Returns the static model of the specified AR class.
   * @return DSConfig the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'config';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('id', 'required'),
      array('in_wizard', 'numerical', 'integerOnly' => TRUE),
      array('id,label', 'length', 'max' => 256),
      array('value, default_value', 'safe'),
      //array('value','numerical'),
      array('value', 'length', 'max' => 32767),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('id, label, value, default_value, in_wizard', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'            => Yii::t('main', 'Параметр'),
      'value'         => Yii::t('main', 'Значение'),
      'label'         => Yii::t('main', 'Описание'),
      'default_value' => Yii::t('main', 'По умолчанию'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->condition = "id LIKE '" . $this->id . "%'";
    $criteria->compare('value', $this->value, TRUE);
    $criteria->compare('label', $this->label, TRUE);
    $criteria->compare('default_value', $this->default_value, TRUE);
    $criteria->compare('in_wizard', $this->in_wizard);
    return new CActiveDataProvider($this, array(
      'criteria'   => $criteria,
      'sort'       => array(
        'defaultOrder' => 't.id ASC',
      ),
      'pagination' => array(
        'pageSize' => 200,
      ),
    ));
  }

  public static function getDays() {
    $days = array();
    for ($i = 1; $i <= 31; $i++) {
      if ($i < 10) {
        $days['0' . $i] = $i;
      }
      else {
        $days[$i] = $i;
      }
    }
    return $days;
  }

  public static function getYears() {
    $start_year = intval(date('Y') - 70);
    $end_year = intval(date('Y'));
    $years = array();
    for ($i = $start_year; $i <= $end_year; $i++) {
      $years[$i] = $i;
    }
    return $years;
  }

  public static function getMounthes() {
    return array(
      '01' => Yii::t('main', 'Январь'),
      '02' => Yii::t('main', 'Февраль'),
      '03' => Yii::t('main', 'Март'),
      '04' => Yii::t('main', 'Апрель'),
      '05' => Yii::t('main', 'Май'),
      '06' => Yii::t('main', 'Июнь'),
      '07' => Yii::t('main', 'Июль'),
      '08' => Yii::t('main', 'Август'),
      '09' => Yii::t('main', 'Сентябрь'),
      '10' => Yii::t('main', 'Октябрь'),
      '11' => Yii::t('main', 'Ноябрь'),
      '12' => Yii::t('main', 'Декабрь'),
    );
  }

  public static function formatXML($src) {
    $res = preg_replace('/&gt;(.+?)&lt;/i', '&gt;<b>$1</b>&lt;', $src);
    return $res;
  }

  public static function getVal($Pk) {
    if (is_array(self::$_cache)) {
      if (isset(self::$_cache[$Pk])) {
        return self::$_cache[$Pk];
      }
    }
    $res_object = self::model()->findByPk($Pk);
    if (!$res_object) {
      throw new CHttpException(500, Yii::t('main', 'Фатальная ошибка: нарушена целостность конфигурации системы. Обратитесь к разработчикам') . ' (' . $Pk . ')');
    }
    if (!is_object($res_object)) {
      throw new CHttpException(500, Yii::t('main', 'Фатальная ошибка: нарушена целостность конфигурации системы. Обратитесь к разработчикам') . ' (' . $Pk . ')');
    }
    $res = $res_object->value;
    if (!isset($res)) {
      throw new CHttpException(500, Yii::t('main', 'Фатальная ошибка: нарушена целостность конфигурации системы. Обратитесь к разработчикам') . ' (' . $Pk . ')');
    }
    if (strlen($res) <= 1024 * 16) {
      if (!is_array(self::$_cache)) {
        self::$_cache = array();
      }
        self::$_cache[$Pk] = $res;
    }
    return $res;
  }

  public static function getKeyTranslatorXML() {
    $xml = simplexml_load_string(DSConfig::getVal('keys_translator'), NULL, LIBXML_NOCDATA);
    $key = new stdClass();
    $keysCount = count($xml->translator_key);
    if ($keysCount > 1) {
      $keyIdx = rand(0, $keysCount - 1);
      $key->type = (string) $xml->translator_key[$keyIdx]->type;
      $key->appId = (string) $xml->translator_key[$keyIdx]->appId;
    }
    else {
      $key->type = (string) $xml->translator_key->type;
      $key->appId = (string) $xml->translator_key->appId;
    }
//    print_r($key); die;
    SiteLog::doAPIKeyLog($key->type, $key->appId);
    return $key;
  }

  public static function getSiteCurrency() {
    return self::getVal('site_currency');
  }

  public static function getCurrency($symbol = FALSE, $mask = FALSE, $currency = FALSE) {
    if ((!self::$_currency) && (!$currency)) {

      if (isset(Yii::app()->request->cookies['currency'])) {
        $currency=Yii::app()->request->cookies['currency']->value;
      } else {
        $currency=DSConfig::getVal('site_currency');
      }
//site_currency_format
      if (self::$_currs == FALSE) {
        self::$_currs = explode(',', DSConfig::getVal('site_currency_block'));
      }
      $currs = self::$_currs;
      if (!in_array($currency, $currs)) {
        $currency = DSConfig::getVal('site_currency');
      }
      self::$_currency = $currency;
    }
      if (!$currency) {
    $currency = self::$_currency;
      }
    if (!$symbol) {
      return $currency;
    }
    else {
      if (self::$_currs_format == FALSE) {
        self::$_currs_format = simplexml_load_string(DSConfig::getVal('site_currency_format'), NULL, LIBXML_NOCDATA);
      }
      $currxml = self::$_currs_format->xpath('/currencies/currency[@name="' . $currency . '"]');
      $symb = '$';
      $symbMask = '%s%d';

        if (is_array($currxml)) {
        if (isset($currxml[0])) {
          $symb = (string) $currxml[0]->symbol;
          $symbMask = (string) $currxml[0]->mask;
        }
      }

        if ($mask) {
        $symb = str_replace('%s', $symb, $symbMask);
      }
      return $symb;
    }
  }
}