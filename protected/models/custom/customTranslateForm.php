<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="TranslateForm.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customTranslateForm extends CFormModel
{
  public $type;
  public $id;
  public $from;
  public $to;
  public $uid;
  public $url;
  public $zh;
  public $en;
  public $ru;
  public $global;

  public function rules()
  {
    return array(
      array('type, id, from, to, uid, url, zh, en, ru, global', 'required'),
      //array('phone', 'safe'),
    );
  }


  public function attributeLabels()
  {
    return array(
      'type'=>Yii::t('main','Тип').':',
      'id'=>'ID:',
      'from'=>Yii::t('main','Язык').':',
      'to'=>Yii::t('main','Язык').':',
      'uid'=>Yii::t('main','ID оператора').':',
      'url'=>Yii::t('main','URL').':',
      'global'=>Yii::t('main','Изменить все вхождения перевода'),
      'zh'=>'zh:',
      'en'=>'en:',
      'ru'=>'ru:',
    );
  }
}