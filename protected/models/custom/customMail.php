<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Mail.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * This is the model class for table "mail_events".
 *
 * The followings are the available columns in table 'mail_events':
 * @property integer $id
 * @property string $mailevent_name
 * @property integer $enabled
 * @property string $mail_template
 */
class customMail
{
    /**
     * @return string the associated database table name
     */
    private static $mailFromQueryPerRun=10;
    public $user;
    public $message;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Mail the static model class
     */
    /*  public static function model($className = __CLASS__) {
        return parent::model(preg_replace('/^custom/','',$className));
      }
        */
    public static function sendMailToUser($uid, $message)
    {
        $mail = new Mail();
        $mail->user = Users::model()->findByPk($uid);
        $mail->message = $message;
        if ($mail->user) {
            return CmsEmailEvents::emailProcessEvents($mail, 'sendMailToUser');
        } else {
            return false;
        }
    }

    public static function sendMailToAll($message, $queue = true)
    {
        $mail = new Mail();
        $mail->user = 'all';
        $mail->message = $message;
        return CmsEmailEvents::emailProcessEvents($mail, 'sendMailToAll', $queue);
    }

    private static function internalSendMail($from, $fromName, $to, $subj, $body)
    {
        try {
            include_once Yii::app()->basePath . '/extensions/phpmailer/PHPMailer/class.phpmailer.php';
            $mailer = new PHPMailer();
            $mailer->CharSet = 'UTF-8';
            $mailer->From = $from;
            $mailer->FromName = $fromName;
            $mailer->Body = $body;
            $mailer->Subject = $subj;
            $mailer->isHTML(true);
            $mailer->AddAddress($to);
            $mailer->Send();
            $mailer->ClearAddresses();
            $mailer->ClearAttachments();
        } catch (Exception $e) {
            LogSiteErrors::logError($e);
            return false;
        }
        return true;
    }

    public static function sendMail($from, $fromName, $to, $subj, $body, $queue = false,$eventId=-1,$postingId=null)
    {
        $mailQueue=new MailQueue();
        $mailQueue->from=$from;
        $mailQueue->from_name=$fromName;
        $mailQueue->to=$to;
        $mailQueue->subj=$subj;
        $mailQueue->body=$body;
        $mailQueue->priority=0;
        $mailQueue->created=date('Y-m-d H:i:s', time());
        $mailQueue->processed=($queue?null:date('Y-m-d H:i:s', time()));
        $mailQueue->result=($queue?null:'OK');
        $mailQueue->event_id=$eventId;
        $mailQueue->posting_id=$postingId;
        if ($queue) {
            return $mailQueue->save();
        }
        if (self::internalSendMail($from, $fromName, $to, $subj, $body)) {
            return $mailQueue->save();
        } else {
            return false;
        }
    }
    public static function sendMailFromQueue() {
        try {
            $mailQueueToSend = MailQueue::model()
              ->findAllBySql(
                'select * from mail_queue mq
            where mq.processed is null order by priority desc, created LIMIT :cnt',
                array(':cnt' => self::$mailFromQueryPerRun)
              );
            if ($mailQueueToSend) {
                foreach ($mailQueueToSend as $mailQueue) {
                    $mailQueue->processed = date('Y-m-d H:i:s', time());
                    if (self::internalSendMail(
                      $mailQueue->from,
                      $mailQueue->from_name,
                      $mailQueue->to,
                      $mailQueue->subj,
                      $mailQueue->body
                    )
                    ) {
                        $mailQueue->result = 'OK';
                    } else {
                        $mailQueue->result = 'ERR';
                    }
                    $mailQueue->update();
                }
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
