<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="UserNotice.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "user_notice".
 *
 * The followings are the available columns in table 'user_notice':
 * @property integer $id
 * @property string $msg
 * @property double $sum
 * @property integer $uid
 */
class customUserNotice extends CActiveRecord
{

     /**
	 * Returns the static model of the specified AR class.
	 * @return UserNotice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_notice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(			
			array('id, uid', 'numerical', 'integerOnly'=>true),
			array('sum', 'numerical'),
			array('msg', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, msg, sum, uid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'msg' => 'Msg',
			'sum' => 'Sum',
			'uid' => 'Uid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('msg',$this->msg,true);
		$criteria->compare('sum',$this->sum);
		$criteria->compare('uid',$this->uid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}