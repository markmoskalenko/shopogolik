<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Profiler.php">
* </description>
**********************************************************************************************************************/?>
<?php
 class customProfiler {
   protected static $session_name = 'profiler';
public static function start ($name, $new=false) {
   if ($new) {
     $session = new CHttpSession;
     $session[self::$session_name]=array('start'=>microtime(true));
   }
  $session=Yii::app()->session;
  $arr=$session[self::$session_name];
  $arr[$name]=microtime(true);
  $session[self::$session_name]=$arr;
 }

   public static function stop ($name, $final=false) {
     $session=Yii::app()->session;
     $arr=$session[self::$session_name];
     if (isset($arr[$name])) {
     $arr[$name]=round(microtime(true)-$arr[$name],2);
     } else {
     $arr[$name]=0;
     }
     if ($final) {
       $arr['stop']=microtime(true);
     if (isset($arr['stop']) && (isset($arr['start']))) {
       $arr['duration']=round($arr['stop']-$arr['start'],2);
     } else {
       $arr['duration']=0;
     }
     }
     $session[self::$session_name]=$arr;
   }

   public static function message ($name,$text) {
     $session=Yii::app()->session;
     $arr=$session[self::$session_name];
//     if (isset($arr[$name])) {
//     $arr[$name]=$text;
//     } else {
       $arr[]='*'.$name.': '.$text;
//     }
     $session[self::$session_name]=$arr;
   }

   public static function get () {
     $session=Yii::app()->session;
     $arr=$session[self::$session_name];
     return $arr;
   }
}