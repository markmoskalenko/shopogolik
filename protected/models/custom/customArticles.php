<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Articles.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $title
 * @property string $meta_desc
 * @property string $meta_keeword
 * @property string $text
 * @property string $lang
 * @property string $url
 */
class customArticles extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, meta_desc, meta_keeword', 'length', 'max'=>256),
			array('lang', 'length', 'max'=>2),
			array('url', 'length', 'max'=>128),
			array('text', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, meta_desc, meta_keeword, text, lang, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => Yii::t('main','Заголовок'),
			'meta_desc' => 'Meta Description',
			'meta_keeword' => 'Meta Keyword',
			'text' => Yii::t('main','Содержимое'),
			'lang' => Yii::t('main','Язык'),
			'url' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('meta_desc',$this->meta_desc,true);
		$criteria->compare('meta_keeword',$this->meta_keeword,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
          'pagination' => array(
            'pageSize' => 50,
          ),
		));
	}
  public static function getVal($Pk) {
    return self::model()->findByPk($Pk)->text;
  }
}