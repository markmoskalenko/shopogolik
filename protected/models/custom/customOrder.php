<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Order.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $uid
 * @property string $status
 * @property integer $date
 * @property integer $manager
 * @property double $weight
 * @property double $delivery
 * @property double $sum
 * @property double $payed
 * @property string $code
 * @property integer $addresses_id
 * @property string $delivery_id
 * @property double $manual_weight
 * @property double $manual_delivery
 * @property double $manual_sum
 * @property string $store_id
 *
 * The followings are the available model relations:
 * @property Users $manager0
 * @property Users $u
 * @property Addresses $addresses
 * @property OrdersStatuses $status0
 * @property OrdersComments[] $ordersComments
 * @property OrdersItems[] $ordersItems
 * @property OrdersPayments[] $ordersPayments
 * @property OrdersPayments[] $ordersPayments1
 */
class customOrder extends DSEventableActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return Order the static model class
     */

    public $ordr_user_email;
    public $ordr_operator_email;
    public $goods_count;
    public $items_count;
    public $items_payment;
    public $items_legend;
    public $items_from_one_seller;
    public $managers;
    public $users;
    public $addresses;
    public $statuses;
    public $extstatuses;
    public $allowedStatuses;
    public $allowedDeliveries;
    public $ordersItems;
    public $ordersItemsLegend;
    public $ordersItemsFromOneSeller;
    public $topay;
    public $textdate;
    public $statusname;
    public $extstatusname;
    public $do_moneyback = 0;

    public $country;
    public $city;
    public $phone;
    public $address;
    public $firstname;
    public $lastname;
    public $fullAddress;
    public $userName;
    public $allowedManagers;

    public $orderCode;
    public $orderAge;
    public $managerBalance;
    public $userBalance;
    public $calculated;
    public $payments_sum;
    public $order_payments;

    public static function getOrderItemsLegend($orderId)
    {
        $intOrderId = $orderId;
        $itemsLegend = Yii::app()->db->createCommand(
          'select ois.name, count(0) as cnt, ois.excluded from orders_items oi, orders_items_statuses ois where ois.id=oi.status and oi.oid=:oid
                                 group by oi.status'
        )
          ->bindParam(':oid', $intOrderId, PDO::PARAM_STR)
          ->queryAll();
        if ($itemsLegend) {
            return $itemsLegend;
        } else {
            return array();
        }
    }

    public static function getItemsFromOneSeller($orderId)
    {
        $intOrderId = $orderId;
        $itemsFromOneSeller = Yii::app()->db->createCommand(
          'SELECT ifnull(sum(cnt),0) as cnt from (
          SELECT count(0) AS cnt
            FROM orders_items oi
           WHERE oi.oid = :oid
          GROUP BY oi.seller_id
           HAVING count(0)>1) ooi'
        )
          ->bindParam(':oid', $intOrderId, PDO::PARAM_STR)
          ->queryScalar();
        return $itemsFromOneSeller;
    }

    public static function getOrderItemsPreview($orderId, $format, $visibleCount = 3, $itemsLegend = false)
    {
        $orderItems = OrdersItems::model()->findAll('oid=:oid', array(':oid' => $orderId));
        /*
        .orderPreviewHidden{display:none;}
        .orderPreviewVisible{}
        #block_1:hover #block_2{display:block;}
        <div class="block_1" style="width:100px; height:100px; background:red; ">
        <img src="favicon.ico" width:20px;height:20px;>
        <div class="block_2" style="width:200px; height:200px; background:green; position: relative;top: -60px;left:10px;">
         */

        $resVisible = '';
        $resHidden = '';
        foreach ($orderItems as $id => $orderItem) {
            $imgPath = Img::getImagePath($orderItem->pic_url, $format, false);
            $res = Yii::app()->controller->renderPartial(
              'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.orders.preview',
              array('imgPath' => $imgPath),
              true,false,true
            );
            if ($id < $visibleCount) {
                $resVisible = $resVisible . $res;
            } else {
                $resHidden = $resHidden . $res;
            }
        }
        if (strlen($resHidden) > 0) {
            $res = '<div class="orderPreview">' . (($itemsLegend) ? $itemsLegend . '<br/>' : '') . '<div class="orderPreviewVisible">' . $resVisible . '<br/>' . Yii::t(
                'main',
                '...'
              ) . '<div class="orderPreviewHidden">' . $resVisible . $resHidden . '</div></div></div>';
        } else {
            $res = '<div class="orderPreview">' . (($itemsLegend) ? $itemsLegend . '<br/>' : '') . '<div class="orderPreviewVisible">' . $resVisible . '</div></div>';
        }
        return $res;
    }

    public static function getAdminOrdersList($status, $uid = false, $manager = false, $pageSize = 25, $asModel=false)
    {
        if ($uid === false) {
            $_uid = null;
        } else {
            $_uid = $uid;
        }
        if ($manager === false) {
            $_manager = null;
        } else {
            $_manager = $manager;
        }
        if ($status != null) {
            $orderStatus = OrdersStatuses::model()->find('value=:value', array(':value' => $status));
        } else {
            $orderStatus = new stdClass();
            $orderStatus->name = Yii::t('admin', 'Все');
            $orderStatus->value = null;
        }

        $criteria = new CDbCriteria;
        $criteria->select = "t.*, FROM_UNIXTIME(t.date,'%d.%m.%Y %h:%i') AS textdate,ss.name as statusname,
        " . OrdersStatuses::getCaseQueryForOrderExtStatus() . " as extstatusname,
        (select round(ifnull(sum(opp.summ),0),2) from orders_payments opp where opp.oid=t.id) as payments_sum,
--
  0-(round(round((ifnull(t.manual_delivery,t.delivery)+ifnull(t.manual_sum,t.sum)),2)
  - (select round(ifnull(sum(opp.summ),0),2) from orders_payments opp where opp.oid=t.id),2))
  as topay,
--
  (select count(0) from orders_items ii where ii.oid=t.id)
  as goods_count,
--
  (select sum(ii.num) from orders_items ii where ii.oid=t.id)
  as items_count,
--
  (select concat(round(sum(ifnull(oi.actual_lot_price,oi.taobao_promotion_price * ifnull(oi.actual_num,oi.num))),2),' / ',
  round(sum(ifnull(oi.actual_lot_express_fee,oi.express_fee)),2)) from orders_items oi where oi.oid = t.id
  and oi.status not in (select ois.id from orders_items_statuses ois where ois.excluded=1))
  as items_payment,
--
  (select group_concat(oil.val SEPARATOR ' ') as val
  from (select oi.oid,
  concat(ois.name,':',count(0)) as val from orders_items oi, orders_items_statuses ois where ois.id=oi.status
  group by oi.oid, oi.status) oil
  where oil.oid=t.id)
  as items_legend,
--
uu.email as ordr_user_email, mm.email as ordr_operator_email,
aa.country, aa.city,aa.phone,aa.address,aa.firstname,aa.lastname";
        $criteria->condition = '(t.uid=:uid or :uid is null) and (t.manager=:manager or :manager is null) and t.id IN (' . OrdersStatuses::getInQueryForOrderStatus(
            $orderStatus->value
          ) . ')';
        $criteria->join = "LEFT JOIN users uu ON uu.uid=t.uid
                       LEFT JOIN users mm ON mm.uid=t.manager
                       LEFT JOIN addresses aa ON aa.id = t.addresses_id
                       LEFT JOIN orders_statuses ss ON ss.value = t.status";
        $criteria->params = array(':uid' => $_uid, ':manager' => $_manager);
        if (isset($_GET['Order'])) {
            $criteria->compare('t.id', $_GET['Order']['id']);
            $criteria->compare("FROM_UNIXTIME(t.date,'%d.%m.%Y %h:%i')", $_GET['Order']['textdate'], true);
            $criteria->compare('t.delivery_id', $_GET['Order']['delivery_id']);
            $criteria->compare('t.status', $_GET['Order']['status']);
            $criteria->compare('uu.email', $_GET['Order']['ordr_user_email'], true);
            if (isset($_GET['Order']['ordr_operator_email'])) {
                $criteria->compare('mm.email', $_GET['Order']['ordr_operator_email'], true);
            }
            if (isset($_GET['Order']['address'])) {
                $criteria->compare(
                  "concat(aa.country,' ',aa.city,' ',aa.firstname,' ',aa.lastname,' ',aa.phone,' ',aa.address)",
                  $_GET['Order']['address'],
                  true
                );
            }
        }
        if (!$asModel) {
            $orders = new CActiveDataProvider(
              Order::model(), array(
                'criteria'   => $criteria,
                'sort'       => array(
                  'defaultOrder' => 't.`date` DESC',
                ),
                'pagination' => array(
                  'pageSize' => $pageSize,
                ),
              )
            );
            if (isset($_GET['Order'])) {
                if (is_array($_GET['Order'])) {
                    foreach ($_GET['Order'] as $orderAttrName => $orderAttr) {
                        $orders->model->{$orderAttrName} = $orderAttr;
                    }
                }
            }
            return $orders;
        } else {
            $orders=Order::model()->findAll($criteria);
            return $orders;
        }
    }

    public static function getOrdersList($status, $uid = false, $manager = false)
    {
        if ($uid === false) {
            $_uid = null;
        } else {
            $_uid = $uid;
        }
        if ($manager === false) {
            $_manager = null;
        } else {
            $_manager = $manager;
        }
        $orderStatus = OrdersStatuses::model()->find('value=:value', array(':value' => $status));
        $criteria = new CDbCriteria;
        $criteria->select = 't.*,
    (select round(ifnull(sum(opp.summ),0),2) from orders_payments opp where opp.oid=t.id) as payments_sum,
    (select count(0) from orders_items ii where ii.oid=t.id) as goods_count,
        (select sum(ii.num) from orders_items ii where ii.oid=t.id) as items_count,
        (select email from users uu where uu.uid=t.manager) as ordr_operator_email';
        $criteria->condition = '(uid=:uid or manager=:manager) and t.id IN (' . OrdersStatuses::getInQueryForOrderStatus(
            $orderStatus->value
          ) . ')';
        $criteria->params = array(':uid' => $_uid, ':manager' => $_manager);
        $criteria->order = 'date DESC';
        $orders = new CActiveDataProvider(Order::model(), array(
          'criteria'   => $criteria,
          'pagination' => array(
            'pageSize' => 25,
          ),
        ));
        return $orders;
    }

    public static function getOrdersListForReorder($uid)
    {
        $_uid = $uid;
        $criteria = new CDbCriteria;
        $criteria->select = 't.*,
    (select round(ifnull(sum(opp.summ),0),2) from orders_payments opp where opp.oid=t.id) as payments_sum,
    (select count(0) from orders_items ii where ii.oid=t.id) as goods_count,
        (select sum(ii.num) from orders_items ii where ii.oid=t.id) as items_count,
        null as ordr_operator_email';
        $criteria->condition = "(uid=:uid) and t.status IN ('IN_PROCESS','PAUSED') and
    (select count(0) from orders_items ii where ii.oid=t.id and ii.status<2)>0";
        $criteria->params = array(':uid' => $_uid);
        $criteria->order = 'date DESC';
        $orders = new CActiveDataProvider(Order::model(), array(
          'criteria'   => $criteria,
          'pagination' => array(
            'pageSize' => 10,
          ),
        ));
        return $orders;
    }

    public static function getOrder($id, $uid, $manager)
    {
        try {
            $order = self::model()
              ->find(
                'id=:id and ((uid=:uid or :uid is null) and (manager=:manager or :manager is null))',
                array(
                  ':id'      => $id,
                  ':uid'     => $uid,
                  ':manager' => $manager
                )
              );
            if ($order) {
                $order->managers = Users::model()->findByPk($order->manager);
                $order->users = Users::model()->findByPk($order->uid);
                $order->addresses = Addresses::model()->findByPk($order->addresses_id);
                $order->statuses = OrdersStatuses::model()->find('value=:value', array(':value' => $order->status));
                $order->extstatusname = self::getOrderExtStatus($order->id);
                $order->allowedStatuses = OrdersStatuses::getAllowedStatusesForOrder($id, $uid, $manager);
                $order->allowedManagers = Users::getAllowedManagersForOrder($id, $uid, $manager);
                $order->ordersItems = OrdersItems::model()->findAllBySql(
                  'select t.*,
                    (select os.name from orders_items_statuses os where os.id =t.status) as status_text from orders_items t where oid=:oid
                    order by t.seller_id',
                  array(':oid' => $order->id)
                );
                $order->ordersItemsLegend = self::getOrderItemsLegend($order->id);
                $order->ordersItemsFromOneSeller = self::getItemsFromOneSeller($order->id);

                $order->orderCode = $order->uid . '-' . $order->id;
                $order->textdate = date('d.m.Y H:i', $order->date);
                $order->orderAge = round((time() - $order->date) / (24 * 3600));
                $order->userName = $order->users['firstname'] . ' ' . $order->users['lastname'] . '  ' . $order->users['email'] . ' ' . Yii::t(
                    'admin',
                    'Телефон'
                  ) . ': ' . $order->users['phone'];
                $order->managerBalance = Users::getBalance($order->manager);
                $order->userBalance = Users::getBalance($order->uid);
                $order->fullAddress = $order->addresses['firstname'] . ' ' . $order->addresses['lastname'] . ', ' .
                  Deliveries::getCountryName($order->addresses['country']) . ', ' . $order->addresses['index'] . ', ' .
                  $order->addresses['city'] . ', ' . $order->addresses['address'] . ' ' .
                  Yii::t('admin', 'Телефон') . ': ' . $order->addresses['phone'];
                $deliveries = Deliveries::getDelivery($order->weight, $order->addresses['country'], false);
                $resDeliveries = array();
                if (is_array($deliveries)) {
                    foreach ($deliveries as $delivery) {
                        $resDeliveries[$delivery->id] = $delivery->id;
                    }
                }
                $order->allowedDeliveries = $resDeliveries;
                $order->calculated = new OrderCalculated($order);
            }
        } catch (Exception $e) {
            print_r($e);die;
        }

        return $order;
    }

    public static function getOrderExtStatus($id)
    {
        $extStatus = self::model()
          ->findBySql(
            "select " . OrdersStatuses::getCaseQueryForOrderExtStatus(
            ) . ' as extstatuses from orders t where t.id=:id',
            array(
              ':id'      => $id,
              ':uid'     => null,
              ':manager' => null
            )
          );
        if ($extStatus) {
            $res = $extStatus['extstatuses'];
            return Yii::t('main',$res);
        } else {
            return '';
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'orders';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('uid', 'required'),
          array('uid, date, manager, addresses_id, frozen', 'numerical', 'integerOnly' => true),
          array('weight, delivery, sum, manual_weight, manual_delivery, manual_sum', 'numerical'),
          array('status, code', 'length', 'max' => 128),
          array('delivery_id', 'length', 'max' => 64),
          array('store_id', 'length', 'max' => 4000),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'id, uid, status, date, manager, weight, delivery, sum, code, addresses_id, delivery_id, manual_weight, manual_delivery, manual_sum, store_id, frozen',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
          'managers'        => array(self::BELONGS_TO, 'Users', 'manager'),
          'users'           => array(self::BELONGS_TO, 'Users', 'uid'),
          'addresses'       => array(self::BELONGS_TO, 'Addresses', 'addresses_id'),
          'statuses'        => array(self::BELONGS_TO, 'OrdersStatuses', 'status'),
          'ordersComments'  => array(self::HAS_MANY, 'OrdersComments', 'oid'),
          'ordersItems'     => array(self::HAS_MANY, 'OrdersItems', 'oid'),
          'ordersPayments'  => array(self::HAS_MANY, 'OrdersPayments', 'uid'),
          'ordersPayments1' => array(self::HAS_MANY, 'OrdersPayments', 'oid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id'                  => Yii::t('main', 'Номер заказа'),
          'uid'                 => Yii::t('main', 'ID клиента'),
          'comment'             => Yii::t('main', 'Комментарий'),
          'status'              => Yii::t('main', 'Статус заказа'),
          'date'                => Yii::t('main', 'Дата'),
          'manager'             => Yii::t('main', 'ID менеджера'),
          'weight'              => Yii::t('main', 'Вес, грaмм'),
          'delivery'            => Yii::t('main', 'Доставка'),
          'sum'                 => Yii::t('main', 'Сумма заказа'),
          'payments_sum'        => Yii::t('main', 'Оплачено'),
          'code'                => Yii::t('main', 'Трек-код'),
          'delivery_id'         => Yii::t('main', 'Служба доставки'),
          'ordr_user_email'     => Yii::t('main', 'Клиент'),
          'ordr_operator_email' => Yii::t('main', 'Оператор'),
          'goods_count'         => Yii::t('main', 'Продуктов в заказе'),
          'items_count'         => Yii::t('main', 'Товаров в заказе'),
          'items_payment'         => Yii::t('main', 'Выкуп / доставка'),
          'items_legend'        => Yii::t('main', 'Лоты'),
          'items_from_one_seller'        => Yii::t('main', 'Несколько товаров от одного продавца'),
          'topay'               => Yii::t('main', 'Сальдо'),
          'textdate'            => Yii::t('main', 'Дата заказа'),
          'statusname'          => Yii::t('main', 'Статус'),
          'extstatusname'       => Yii::t('main', 'Доп. статус'),
          'country'             => Yii::t('main', 'Страна'),
          'city'                => Yii::t('main', 'Город'),
          'phone'               => Yii::t('main', 'Телефон'),
          'address'             => Yii::t('main', 'Адрес'),
          'firstname'           => Yii::t('main', 'Имя'),
          'lastname'            => Yii::t('main', 'Фамилия'),
          'orderCode'           => Yii::t('admin', 'ID заказа'),
          'orderAge'            => Yii::t('admin', 'В обработке, дней'),
          'fullAddress'         => Yii::t('admin', 'Получатель'),
          'store_id'            => Yii::t('admin', 'Складской ID'),
          'frozen'              => Yii::t('admin', 'Изменения в заказе заблокированы'),
          'userName'            => Yii::t('admin', 'Заказчик'),
          'managerName'         => Yii::t('admin', 'Оператор'),
          'managerBalance'      => Yii::t('admin', 'Баланс оператора'),
          'userBalance'         => Yii::t('admin', 'Баланс заказчика'),
          'do_moneyback'        => Yii::t('admin', 'Вернуть переплату по заказу'),
          'addresses_id'        => Yii::t('admin', 'ID адреса доставки'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('uid', $this->uid);
        $criteria->compare('status', $this->status);
        $criteria->compare('date', $this->date);
        $criteria->compare('manager', $this->manager);
        $criteria->compare('weight', $this->weight);
        $criteria->compare('delivery', $this->delivery);
        $criteria->compare('sum', $this->sum);
        $criteria->compare('code', $this->code);

        $criteria->select = "t.*,
    (select round(ifnull(sum(opp.summ),0),2) from orders_payments opp where opp.oid=t.id) as payments_sum,
    FROM_UNIXTIME(t.date,'%d.%m.%Y %h:%i') AS date, uu.email as ordr_user_email, mm.email as ordr_operator_email";
        $criteria->join = "LEFT JOIN users uu ON uu.uid=t.uid LEFT JOIN users mm ON mm.uid=t.manager";

        /*        $criteria->select = array(
                   'count(*) as total',
                'SUM(learnNew = 5) as best_learn',
                'SUM(serviceSiteMark = 5) as best_site'
              );
        */

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

//=================================================================
    protected function makeProfitPayments($orderId)
    {
        try {
            $order = self::getOrder($orderId, null, null);
            if ($order->calculated->operatorProfit != 0) {
                $paymentUser=Users::model()->findByPk($order->manager);
                if (!$paymentUser) {
                    $paymentUserId=Users::getFirstSuperAdminId();
                } else {
                    $paymentUserId=$order->manager;
                }
                $payment = new Payment();
                $payment->sum = $order->calculated->operatorProfit;
                $payment->description = Yii::t('admin', 'Зачисление бонусов за заказ') . ' №' . $orderId;
                $payment->uid = $paymentUserId;
                $payment->date = time();
                $payment->status = 7;
                $payment->save();
            }
            //===========
            if ($order->calculated->siteProfit != 0) {
                $paymentUser=Users::model()->findByPk(DSConfig::getVal('billing_site_profit_account'));
                if (!$paymentUser) {
                    $paymentUserId=Users::getFirstSuperAdminId();
                } else {
                    $paymentUserId=DSConfig::getVal('billing_site_profit_account');
                }
                $payment = new Payment();
                $payment->sum = $order->calculated->siteProfit;
                $payment->description = Yii::t('admin', 'Зачисление бонусов за заказ') . ' №' . $orderId;
                $payment->uid = $paymentUserId;
                $payment->date = time();
                $payment->status = 7;
                $payment->save();
            }
        } catch (Exception $e) {
//            CVarDumper::dump($e);die;
            return false;
        }
        return true;
    }

//-----------------------------------------------------------------

    public function save($runValidation = true, $attributes = null)
    {
        Yii::app()->db->autoCommit=false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
//-- Возврат переплаты --------------------
        if ($this->do_moneyback && !$this->isNewRecord) {
            $calculated = new OrderCalculated($this);
            /*      $oid = $this->id;
                  $manual_total = (($this->manual_delivery) ? $this->manual_delivery : $this->delivery) + (($this->manual_sum) ? $this->manual_sum : $this->sum);
                  $payments_sum = Yii::app()->db->createCommand('select round(ifnull(sum(opp.summ),0),2) from orders_payments opp where opp.oid=:oid')
                    ->bindParam(':oid', $oid, PDO::PARAM_STR)
                    ->queryScalar();
                  $payments_saldo = round($payments_sum - $manual_total, 2);
            */
            if ($calculated->payments_saldo > 0) {
                OrdersPayments::payForOrder(
                  $this->id,
                  -$calculated->payments_saldo,
                  Yii::t('admin', 'Возврат переплаты по заказу') . ' №' . $this->id
                );
            }
        }
//-----------------------------------------
            $oldRec = self::model()->findByPk($this->primaryKey);
            if ($oldRec) {
                if ($this->status != $oldRec->status) {
                    if ($this->status == 'SEND_TO_CUSTOMER') {
                        if (!$this->makeProfitPayments($this->id)) {
                            $transaction->rollback();
                            Yii::app()->db->autoCommit=true;
                            return false;
                        }
                    }
                }
                $result = parent::save();
            } else {
//      if (Yii::app()->user->id != $this->uid) {
                $result = parent::save();
            }
        } catch (Exception $e) {
            $transaction->rollback();
            Yii::app()->db->autoCommit=true;
//      print_r($e);die;
            return false;
        }
        if ($result) {
            $transaction->commit();
            Yii::app()->db->autoCommit=true;
            Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
            return true;
        } else {
            $transaction->rollback();
            Yii::app()->db->autoCommit=true;
            Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
            return false;
        }
    }

//-----------------------------------------------------------------

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Orders the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model(preg_replace('/^custom/', '', $className));
    }

    public static function getAdminSearchSnippet($id, $query)
    {
        if (!function_exists('markup')) {
            function markup($val, $query)
            {
                $result=@preg_replace('/'.$query.'/i','<strong>'.$query.'</strong>',$val);
                if (isset($result)&&$result) {
                    return $result;
                } else {
                    return $val;
                }
            }
        }
        $order = self::getOrder($id,null,null);
        $res = '';
        $preview='';
        $fields = array(
          'id',
          'uid',
          'code',
          'delivery_id',
          'ordr_user_email',
          'ordr_operator_email',
          'goods_count',
          'items_count',
          'items_payment',
          'items_legend',
          'topay',
          'textdate',
          'statusname',
          'extstatusname',
          'country',
          'city',
          'phone',
          'address',
          'firstname',
          'lastname',
          'orderCode',
          'orderAge',
          'fullAddress',
          'store_id',
          'userName',
//          'managerName',
        );
        if ($order) {
            $preview=self::getOrderItemsPreview($order->id,'_60x60.jpg');
            foreach ($fields as $field) {
                if (strlen($order->{$field}) > 0) {
                    $res = $res . '<small>' . $order->getAttributeLabel($field) . ':</small> ' . markup(
                        $order->{$field},
                        $query
                      ) . '&nbsp;';
                    if (in_array($field,array('textdate'))) {
                        $res = $res . '<br/>';
                    }
                }
            }
        }

        //return '<table><tr><td style="min-width:130px;height:60px;padding:1px;">'.$preview.'</td><td>'.$res.'</td></tr></table>';
        return '<div><div style="float:left;min-width:196px;height:76px;padding:2px;">'.$preview.'</div><div>'.$res.'</div></div>';
    }

    public static function getAdminLink($id, $external = false)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/orders/view/id/' . $id . '&tabName=' .Yii::t('admin','Заказ '). $order->uid.'-'.$order->id;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/orders/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр заказа'
                ) . '" onclick="getContent(this,\'' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '\');return false;"><i class="icon-shopping-cart"></i>&nbsp;' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }

    public static function getUserLink($id)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/cabinet/orders/view/id/' . $id;
            } else {
            return '<a href="#">' . Yii::t('main', 'Ошибка') . '</a>';
        }
    }

    public static function remindToPay() {
        $orders=Order::getAdminOrdersList('100', null, null, 100000,true);
        if ($orders) {
            foreach ($orders as $order) {
                CmsEmailEvents::emailProcessEvents($order,'remindToPay',true);
            }
        }
    }
}