<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Brands.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "brands".
 *
 * The followings are the available columns in table 'brands':
 * @property string $id
 * @property string $name
 * @property integer $enabled
 * @property string $img_src
 * @property string $vid
 * @property string $query
 * @property string $url
 * @property string $meta_desc_ru
 * @property string $meta_keeword_ru
 * @property string $page_title_ru
 * @property string $meta_desc_en
 * @property string $meta_keeword_en
 * @property string $page_title_en
 * @property string $page_desc_ru
 * @property string $page_desc_en
 */
class customBrands extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brands';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enabled', 'numerical', 'integerOnly'=>true),
			array('name, query, url', 'length', 'max'=>256),
            array('img_src, meta_desc_ru, meta_keeword_ru, page_title_ru, meta_desc_en, meta_keeword_en, page_title_en', 'length', 'max'=>512),
			array('vid', 'length', 'max'=>20),
            array('page_desc_ru, page_desc_en', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
          array('name, enabled, img_src, query, url', 'required'),
            array('id, name, enabled, img_src, vid, query, url, meta_desc_ru, meta_keeword_ru, page_title_ru, meta_desc_en, meta_keeword_en, page_title_en, page_desc_ru, page_desc_en', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main','ID бренда'),
			'vid' => Yii::t('main','Vid'),
			'name' => Yii::t('main','Наименование'),
			'img_src' => Yii::t('main','Логотип'),
			'query' => Yii::t('main','Запрос'),
            'url' => Yii::t('main','URL'),
			'enabled' => Yii::t('main','Включен'),
            'meta_desc_ru' => Yii::t('admin','meta description ru'),
            'meta_keeword_ru' => Yii::t('admin','meta keywords ru'),
            'page_title_ru' => Yii::t('admin','title ru'),
            'meta_desc_en' => Yii::t('admin','meta description en'),
            'meta_keeword_en' => Yii::t('admin','meta keywords en'),
            'page_title_en' => Yii::t('admin','meta title'),
            'page_desc_ru' => Yii::t('admin','Описание ru'),
            'page_desc_en' => Yii::t('admin','Описание en'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('img_src',$this->img_src,true);
		$criteria->compare('vid',$this->vid,true);
		$criteria->compare('query',$this->query,true);
		$criteria->compare('url',$this->url,true);
        $criteria->compare('meta_desc_ru',$this->meta_desc_ru,true);
        $criteria->compare('meta_keeword_ru',$this->meta_keeword_ru,true);
        $criteria->compare('page_title_ru',$this->page_title_ru,true);
        $criteria->compare('meta_desc_en',$this->meta_desc_en,true);
        $criteria->compare('meta_keeword_en',$this->meta_keeword_en,true);
        $criteria->compare('page_title_en',$this->page_title_en,true);
        $criteria->compare('page_desc_ru',$this->page_desc_ru,true);
        $criteria->compare('page_desc_en',$this->page_desc_en,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
          'pagination'=>array(
            'pageSize'=>100,
          ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Brands the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}

    public static function updateAllMeta() {

        if (!function_exists('translit')) {
            function translit($s) {
                return Utils::translitURL($s);
            }
        }
            try {
                $site_name = DSConfig::getVal('site_name');
                $xml = simplexml_load_string(DSConfig::getVal('seo_brand_rules'), NULL, LIBXML_NOCDATA);
                $brands=self::model()->findAll('enabled=1');
                foreach ($brands as $brand) {
                $brandname = $brand->name;
                    $brand->meta_desc_ru = eval((string) $xml->meta_desc_ru);
//--------------------
                    $brand->meta_desc_en = eval((string) $xml->meta_desc_en);
//--------------------
                    $brand->page_title_ru = eval((string) $xml->page_title_ru);
//--------------------
                    $brand->page_title_en = eval((string) $xml->page_title_en);
//--------------------
                    $brand->meta_keeword_ru = eval((string) $xml->meta_keeword_ru);
//--------------------
                    $brand->meta_keeword_en = eval((string) $xml->meta_keeword_en);
//--------------------
                    $brand->update();
                }
            } catch (Exception $e) {
                LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
                //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
                echo $e->getMessage();
                return FALSE;
            }
        return TRUE;
    }
}
