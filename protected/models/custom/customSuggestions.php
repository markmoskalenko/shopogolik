<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Suggestions.php">
* </description>
**********************************************************************************************************************/?>
<?php
class customSuggestions {
  public static function transkey($s) {
    /*Символы для замены на русские*/
    $arrReplace = array(
      'q' => 'й',
      'w' => 'ц',
      'e' => 'у',
      'r' => 'к',
      't' => 'е',
      'y' => 'н',
      'u' => 'г',
      'i' => 'ш',
      'o' => 'щ',
      'p' => 'з',
      '[' => 'х',
      ']' => 'ъ',
      'a' => 'ф',
      's' => 'ы',
      'd' => 'в',
      'f' => 'а',
      'g' => 'п',
      'h' => 'р',
      'j' => 'о',
      'k' => 'л',
      'l' => 'д',
      ';' => 'ж',
      "'" => 'э',
      'z' => 'я',
      'x' => 'ч',
      'c' => 'с',
      'v' => 'м',
      'b' => 'и',
      'n' => 'т',
      'm' => 'ь',
      ',' => 'б',
      '.' => 'ю',
      '/' => '.',
      '`' => 'ё',
      'Q' => 'Й',
      'W' => 'Ц',
      'E' => 'У',
      'R' => 'К',
      'T' => 'Е',
      'Y' => 'Н',
      'U' => 'Г',
      'I' => 'Ш',
      'O' => 'Щ',
      'P' => 'З',
      '{' => 'Х',
      '}' => 'Ъ',
      'A' => 'Ф',
      'S' => 'Ы',
      'D' => 'В',
      'F' => 'А',
      'G' => 'П',
      'H' => 'Р',
      'J' => 'О',
      'K' => 'Л',
      'L' => 'Д',
      ':' => 'Ж',
      '"' => 'Э',
      '|' => '/',
      'Z' => 'Я',
      'X' => 'Ч',
      'C' => 'С',
      'V' => 'М',
      'B' => 'И',
      'N' => 'Т',
      'M' => 'Ь',
      '<' => 'Б',
      '>' => 'Ю',
      '?' => ',',
      '~' => 'Ё',
      '@' => '"',
      '#' => '№',
      '$' => ';',
      '^' => ':',
      '&' => '?'
    );
    /*Меняем ключ со значением в массиве $arrReplace*/
    $arrReplace2 = array_flip($arrReplace);
    /*Удаляем некоторые символы*/
        unset($arrReplace2['.']);
        unset($arrReplace2[',']);
        unset($arrReplace2[';']);
        unset($arrReplace2['"']);
        unset($arrReplace2['?']);
        unset($arrReplace2['/']);
    /*И соединяем массивы в один*/
    $arrReplace = array_merge($arrReplace, $arrReplace2);
    return strtr($s, $arrReplace);
  }

  public static function getSuggestions($q, $transkey = FALSE) {
    $lang = Utils::TransLang();
    $command = Yii::app()->db->createCommand("SELECT * FROM (
SELECT * FROM (
SELECT 'query_fulltext' AS type, max(rr.query) AS ru, max(rr.query) AS en, max(rr.query) AS zh, rr.cid, rr.query,
max(rr.res_count) AS res_count
     FROM log_queries_requests rr
WHERE MATCH(rr.query) AGAINST (:q IN NATURAL LANGUAGE MODE)>0 -- IN BOOLEAN MODE)>0
GROUP BY rr.cid, rr.query
 ORDER BY MATCH(rr.query) AGAINST (:q IN NATURAL LANGUAGE MODE)>0 -- IN BOOLEAN MODE)>0
 DESC) ss2 LIMIT 20) s2
UNION ALL
SELECT * FROM (
SELECT * FROM (
SELECT 'cat_fulltext' AS type, cc.ru, cc.en, cc.zh, cc.cid, cc.query,
(SELECT qq.res_count FROM log_queries_requests qq WHERE qq.cid=cc.cid AND qq.query=cc.query ORDER BY qq.`date` DESC LIMIT 1)
AS res_count
     FROM categories_ext cc, categories_search ss
WHERE cc.id=ss.id
AND MATCH(ss.ru, ss.en, ss.zh) AGAINST (:q IN NATURAL LANGUAGE MODE)>0 -- IN BOOLEAN MODE)>0
 ORDER BY MATCH(ss.ru, ss.en, ss.zh) AGAINST (:q IN NATURAL LANGUAGE MODE)>0 -- IN BOOLEAN MODE)>0
 DESC) ss1 LIMIT 20) s1")
      ->bindParam(':q', $q, PDO::PARAM_STR);
    $rows = $command->queryAll();
    $res = '';
    if (count($rows) <= 0) {
      $command = Yii::app()->db->createCommand("SELECT * FROM (
SELECT * FROM (
SELECT 'query_fulltext' AS type, max(rr.query) AS ru, max(rr.query) AS en, max(rr.query) AS zh, rr.cid, rr.query,
max(rr.res_count) AS res_count
     FROM log_queries_requests rr
WHERE MATCH(rr.query) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
GROUP BY rr.cid, rr.query
 ORDER BY MATCH(rr.query) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
 DESC) ss2 LIMIT 20) s2
UNION ALL
SELECT * FROM (
SELECT * FROM (
SELECT 'cat_fulltext' AS type, cc.ru, cc.en, cc.zh, cc.cid, cc.query,
(SELECT qq.res_count FROM log_queries_requests qq WHERE qq.cid=cc.cid AND qq.query=cc.query ORDER BY qq.`date` DESC LIMIT 1)
AS res_count
     FROM categories_ext cc, categories_search ss
WHERE cc.id=ss.id
AND MATCH(ss.ru, ss.en, ss.zh) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
 ORDER BY MATCH(ss.ru, ss.en, ss.zh) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
 DESC) ss1 LIMIT 20) s1")
        ->bindParam(':q', $q, PDO::PARAM_STR);
      $rows = $command->queryAll();
    }
    if (count($rows) <= 0) {
      $transQ = self::transkey($q);
      $command = Yii::app()->db->createCommand("SELECT * FROM (
SELECT * FROM (
SELECT 'query_fulltext' AS type, max(rr.query) AS ru, max(rr.query) AS en, max(rr.query) AS zh, rr.cid, rr.query,
max(rr.res_count) AS res_count
     FROM log_queries_requests rr
WHERE MATCH(rr.query) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
GROUP BY rr.cid, rr.query
 ORDER BY MATCH(rr.query) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
 DESC) ss2 LIMIT 20) s2
UNION ALL
SELECT * FROM (
SELECT * FROM (
SELECT 'cat_fulltext' AS type, cc.ru, cc.en, cc.zh, cc.cid, cc.query,
(SELECT qq.res_count FROM log_queries_requests qq WHERE qq.cid=cc.cid AND qq.query=cc.query ORDER BY qq.`date` DESC LIMIT 1)
AS res_count
     FROM categories_ext cc, categories_search ss
WHERE cc.id=ss.id
AND MATCH(ss.ru, ss.en, ss.zh) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
 ORDER BY MATCH(ss.ru, ss.en, ss.zh) AGAINST (concat(:q,'*') IN BOOLEAN MODE)>0 -- IN BOOLEAN MODE)>0
 DESC) ss1 LIMIT 20) s1")
        ->bindParam(':q', $transQ, PDO::PARAM_STR);
      $rows = $command->queryAll();
    }
    foreach ($rows as $row) {
      $params = array();
      $params['type'] = $row['type'];
      $params['cid'] = $row['cid'];
      $params['query'] = $row['query'];
      $res = $res . $row[$lang] . '|' . $row['res_count'] . '|' . json_encode($params) . "\n";
    }
    return $res;
  }
}