<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Weights.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "weights".
 *
 * The followings are the available columns in table 'weights':
 * @property string $id
 * @property string $cid
 * @property string $num_iid
 * @property double $min_weight
 * @property double $max_weight
 */
class customWeights extends CActiveRecord {
  /**
   * @return string the associated database table name
   */
  public $ru;
  public $en;
  public function tableName() {
    return 'weights';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('max_weight', 'required'),
      array('min_weight, max_weight', 'numerical'),
      array('cid, num_iid', 'length', 'max' => 20),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array('id, cid, num_iid, min_weight, max_weight, checked', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'cc' => array(self::BELONGS_TO, 'Category', 'cid'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'         => 'ID',
      'cid'        => 'Cid',
      'num_iid'    => 'Num Iid',
      'min_weight' => Yii::t('main','Мин. вес'),
      'max_weight' => Yii::t('main','Макс. вес'),
      'checked'    => 'Checked',
      'ru' => Yii::t('main','Категория'),
      'en' => 'Category',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;
    $criteria->select = "t.cid, (select cc.ru from categories cc where cc.cid=t.cid limit 1) as ru,
    (select cc.en from categories cc where cc.cid=t.cid limit 1) as en,
    round(avg(t.min_weight)) as min_weight,
    round(avg(t.max_weight)) as max_weight";
//      $criteria->with='u';
//    $criteria->with = array('cc' => array('select' => "ru, en"));
    $criteria->group='t.cid';
    $criteria->order='(select count(0) from log_item_requests ii where ii.cid=t.cid) DESC';
    $criteria->condition="exists(select 'x' from categories cat where cat.cid=t.cid and (cat.ru like :query or cat.en like :query))";
    if ($this->ru) {
    $criteria->params=array(':query'=>'%'.$this->ru.'%');
    } else {
      $criteria->params=array(':query'=>'%');
    }

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
      'pagination' => array(
        'pageSize' => 50,
      ),
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return Weights the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public static function saveItemWeight($cid, $iid, $weight, $checked = 0) {
    if (($cid <= 0) || ($iid <= 0) || ($weight <= 0)) {
      return;
    }
    $rec = self::model()->find('(cid=:cid or :cid = 0) and num_iid=:iid', array(':cid' => $cid, ':iid' => $iid));
    if ((!$rec) && ($cid==0)) {
      return;
    }
    if (!$rec) {
      $rec = new Weights();
      $rec->cid = $cid;
      $rec->num_iid = $iid;
    }
    if ($rec->checked != 1) {
      $rec->min_weight = $weight;
      $rec->max_weight = $weight;
      $rec->checked = $checked;
    }
    $rec->save();
  }

  public static function median($arr) { //Медиана от массива $arr
    sort($arr);
    $count = count($arr);
    $middle = floor($count / 2);
    if ($count % 2) {
      return $arr[$middle];
    }
    else {
      return ($arr[$middle - 1] + $arr[$middle]) / 2;
    }
  }

  public static function getItemWeight($cid, $iid) {
    $checkout_weight_needed = DSConfig::getVal('checkout_weight_needed') == 1;
    if ($checkout_weight_needed) {
    //-- сначала проверяем на точное совпадение cid и iid - что вряд ли
    $rec = self::model()->find('cid=:cid and num_iid=:iid', array(':cid' => $cid, ':iid' => $iid));
    if ($rec) {
      return round(($rec->min_weight + $rec->max_weight) / 2);
    }
    //-- не нашли - берём медиану по категории для iid != 0 с учётом веса по checked
    $recs = self::model()->findAll('cid=:cid and num_iid!=0', array(':cid' => $cid));
    if ($recs) {
      $arr = array();
      foreach ($recs as $rec) {
        $arr[] = $rec->min_weight;
        $arr[] = $rec->max_weight;
      }
      return round(self::median($arr), -1);
    }
    //-- не нашли - берём медиану дефолтовых весов категории и её родителей включая собственно и товары
    $_cid = $cid;
    $recs = Yii::app()->db->createCommand("
SELECT ww.*
  FROM weights ww
       JOIN
       (SELECT T2.cid, T1.lvl
          FROM (SELECT @r AS _cid,
                       (SELECT @r := parent
                          FROM categories
                         WHERE cid = _cid)
                          AS parent,
                       @l := @l + 1 AS lvl
                  FROM (SELECT @r := :start_cid, @l := 0) vars, categories m
                 WHERE @r <> 0) T1
               JOIN categories T2 ON T1._cid = T2.cid) tt
          ON tt.cid = ww.cid                               -- and ww.num_iid=0
ORDER BY tt.lvl ASC")
      ->bindParam(':start_cid', $_cid, PDO::PARAM_STR)
      ->queryAll();
    if ($recs) {
      $arr = array();
      foreach ($recs as $rec) {
        $arr[] = $rec['min_weight'];
        $arr[] = $rec['max_weight'];
      }
      return round(self::median($arr), -1);
    }
    return 0;
    } else {
        return 0;
    }
  }
}
