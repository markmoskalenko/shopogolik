<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Users.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $uid
 * @property string $email
 * @property string $password
 * @property integer $status
 * @property string $lastname
 * @property string $firstname
 * @property string $patroname
 * @property string $alias
 * @property integer $created
 * @property string $role
 * @property string $phone
 * @property string $sex
 * @property string $country
 * @property string $city
 * @property string $index
 * @property string $address
 * @property string $birthday
 * @property double $skidka
 * @property string $skype
 * @property string $vk
 * @property integer $default_manager
 *
 * The followings are the available model relations:
 * @property Addresses[] $addresses
 * @property AdminTabsHistory[] $adminTabsHistories
 * @property Cart[] $carts
 * @property EventsLog[] $eventsLogs
 * @property Orders[] $orders
 * @property OrdersComments[] $ordersComments
 * @property OrdersItemsComments[] $ordersItemsComments
 * @property Payments[] $payments
 * @property Questions[] $questions
 * @property UserNotice[] $userNotices
 */
class customUsers extends DSEventableActiveRecord
{
    public $paymentsSUM;
    public $ordersCNT;
    public $ordersLast;
    public $ordr_user_stat;
    public $userBalance;
//============================
// public fields for OperatorsController
    public $ordr_operator_stat;
    public $ordr_operator_inprocess;
    public $ordr_operator_delay_buy;
    public $ordr_operator_send;
    public $ordr_operator_activity;
// birthday wrapper
    public $d_birth;
    public $m_birth;
    public $y_birth;
    public $orders_count;
//============================
    public $payments;
    public $addresses;
    public $manager;
    public $users;
    public $usersOrders;
    public $usersOrdersEvents;
    public $usersOrdersMessages;
    public $confirmationLink;

    /**
     * Returns the static model of the specified AR class.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model(preg_replace('/^custom/', '', $className));
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('status, created, default_manager', 'numerical', 'integerOnly' => true),
          array('skidka', 'numerical', 'integerOnly' => false),
          array('email,role', 'length', 'max' => 64),
          array('email','email','allowName'=>false,'pattern'=>'/.+@.+?\..+/i'),
          array('password, lastname, patroname, city', 'length', 'max' => 128),
          array('firstname, phone, index', 'length', 'max' => 32),
          array('alias', 'length', 'max' => 500),
          array('sex', 'length', 'max' => 50),
          array('country', 'length', 'max' => 3),
          array('address', 'length', 'max' => 256),
          array('skype', 'length', 'max' => 512),
          array('vk', 'length', 'max' => 2048),
          array('role,status,y_birth,m_birth,d_birth,alias,skidka', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'uid, email, status, lastname, firstname, patroname, alias, created, role, phone, country, city, index, address, birthday, skidka, default_manager, skype, vk',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
          'addresses'           => array(self::HAS_MANY, 'Addresses', 'uid'),
          'adminTabsHistories'  => array(self::HAS_MANY, 'AdminTabsHistory', 'uid'),
          'carts'               => array(self::HAS_MANY, 'Cart', 'uid'),
          'eventsLogs'          => array(self::HAS_MANY, 'EventsLog', 'uid'),
          'orders'              => array(self::HAS_MANY, 'Orders', 'manager'),
          'orders1'             => array(self::HAS_MANY, 'Orders', 'uid'),
          'ordersComments'      => array(self::HAS_MANY, 'OrdersComments', 'uid'),
          'ordersItemsComments' => array(self::HAS_MANY, 'OrdersItemsComments', 'uid'),
          'payments'            => array(self::HAS_MANY, 'Payments', 'uid'),
          'questions'           => array(self::HAS_MANY, 'Questions', 'uid'),
          'userNotices'         => array(self::HAS_MANY, 'UserNotice', 'uid'),
          'role0'               => array(self::BELONGS_TO, 'AccessRights', 'role'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'uid'                     => Yii::t('main', 'ID'),
          'firstname'               => Yii::t('main', 'Имя'),
          'alias'                   => Yii::t('main', 'Псевдоним'),
          'status'                  => Yii::t('main', 'Статус'),
          'password'                => Yii::t('main', 'Пароль'),
          'created'                 => Yii::t('main', 'Дата регистрации'),
          'patroname'               => Yii::t('main', 'Отчество'),
            //'sex' => Yii::t('main', 'Пол'),
          'lastname'                => Yii::t('main', 'Фамилия'),
          'email'                   => Yii::t('main', 'EMail'),
          'country'                 => Yii::t('main', 'Страна'),
          'city'                    => Yii::t('main', 'Город'),
          'index'                   => Yii::t('main', 'Индекс'),
          'address'                 => Yii::t('main', 'Адрес'),
          'phone'                   => Yii::t('main', 'Телефон'),
          'role'                    => Yii::t('main', 'Роль'),
          'paymentsSUM'             => Yii::t('main', 'Введено средств'),
          'userBalance'             => Yii::t('main', 'Баланс'),
          'skidka'                  => Yii::t('main', 'Скидка'),
          'ordersCNT'               => Yii::t('main', 'Заказов'),
          'ordersLast'              => Yii::t('main', 'Активность'),
//============================
// public fields for OperatorsController
          'ordr_user_stat'          => Yii::t('main', 'Статистика клиента'),
          'ordr_operator_stat'      => Yii::t('main', 'Статистика'),
          'ordr_operator_inprocess' => Yii::t('main', 'Заказов в обработке'),
          'ordr_operator_delay_buy' => Yii::t('main', 'Задержка закупки'),
          'ordr_operator_send'      => Yii::t('main', 'Отправлено получателю'),
          'ordr_operator_activity'  => Yii::t('main', 'Активность'),
//============================
          'skype'                   => 'Skype',
          'vk'                      => Yii::t('main', 'Адрес страницы ВКонтакте'),
          'default_manager'         => Yii::t('main', 'Менеджер'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
//    $criteria->select ="select SUM(payments.sum) as payments_sum";
        //where uid=". Topic.topicId, Topic.extension from Topic order by Topic.startDate desc limit 1,2) as Topic ON Topic.topicId = Rant.topicId LEFT JOIN User on User.userId = Rant.userId';
//    $criteria->join='LEFT JOIN payments pp ON pp.uid=t.uid';
        $criteria->select = "t.*, FROM_UNIXTIME(t.created,'%d.%m.%Y %h:%i') AS created,
    (select ROUND(SUM(pp.sum),2) from payments pp where pp.uid=t.uid and pp.status in (1,2,5,6,7,8)) as paymentsSUM,
    (select COUNT(0) from orders oo where oo.uid=t.uid) as ordersCNT,
    (select FROM_UNIXTIME(MAX(oo.date),'%d.%m.%Y %h:%i') from orders oo where oo.uid=t.uid) as ordersLast,
    (SELECT concat('<b>+</b>',sum(if((oo.status IN ('SEND_TO_CUSTOMER','RECEIVED_BY_CUSTOMER')), 1, 0)),'/',
       round(sum(if((oo.status IN ('SEND_TO_CUSTOMER','RECEIVED_BY_CUSTOMER')),
       (select ifnull(sum(summ),0) as `sum` from orders_payments op
where op.oid = oo.id)
       , 0)), 2),'<br/>',
       '<b>?</b>',sum(if((oo.status IN ('IN_PROCESS')), 1, 0)),'/',
       round(sum(if((oo.status IN ('IN_PROCESS')),
       (select ifnull(sum(summ),0) as `sum` from orders_payments op
where op.oid = oo.id), 0)), 2),'<br/>',
       '<b>~</b>',round(avg((select ifnull(sum(summ),0) as `sum` from orders_payments op
where op.oid = oo.id)), 2))
  FROM orders oo where oo.uid=t.uid) as ordr_user_stat";
//    $criteria->select = new CDbExpression("DATE_FORMAT(created, '%Y-%m-%d') AS created");

        $criteria->compare('uid', $this->uid);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('lastname', $this->lastname, true);
        $criteria->compare('firstname', $this->firstname, true);
        $criteria->compare('patroname', $this->patroname, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('created', $this->created);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('index', $this->index, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('skidka', $this->skidka, true);
        $criteria->compare('skype', $this->skype, true);
        $criteria->compare('vk', $this->vk, true);
        $criteria->compare('default_manager', $this->default_manager);

        return new CActiveDataProvider($this, array(
          'criteria'   => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          )
        ));
    }

    public function login($email, $password, $rememberMe = false)
    {
        $identity = new UserIdentity($email, $password);
        if ($identity->authenticate()) {
            if ($rememberMe) {
                Yii::app()->user->login($identity, 3600 * 24 * 30);
            } else {
                Yii::app()->user->login($identity);
            }
            if (isset(Yii::app()->user->returnUrl)) {
                Yii::app()->request->redirect(Yii::app()->user->returnUrl);
            } else {
                Yii::app()->controller->redirect('/cabinet');
            }
        } else {
            $msg = '';
            switch ($identity->errorCode) {
                case 1:
                    $msg = Yii::t('main', 'Введенный Email не найден');
                    break;
                case 2:
                    $msg = Yii::t('main', 'Пароль введен неправильно');
                    break;
                case 3:
                    $msg = Yii::t('main', 'Вы еще не активировали свой аккаунт');
                    break;
                case 4:
                    $msg = Yii::t('main', 'Ваш аккаунт заблокирован');
                    break;
            }
            Yii::app()->user->setFlash('loginError', $msg);
        }
    }

    public static function getBalance($uid)
    {
        $_uid = $uid;
        $res = Yii::app()->db->createCommand(
          "select (select SUM(`sum`) AS `sum` from payments
          where uid=:uid AND status in (1,2,5,6,7,8))
          -
          (select ifnull(sum(summ),0) as `sum` from orders_payments op
          where op.oid in (select oo.id from orders oo where oo.uid = :uid
          and oo.status not in ('CANCELED_BY_CUSTOMER','CANCELED_BY_SERVICE')
          )) as `sum`"
        )
          ->bindParam(':uid', $_uid, PDO::PARAM_INT)
          ->queryScalar();
        if ($res) {
            $result = Formulas::cRound($res, false, 2);
        } else {
            $result = 0;
        }
        return $result;
    }

    public function getOperatorOrders($aFilter)
    {

        $where_string = array();
        $where_array = array();

        if (isset($aFilter['manager'])) {
            $where_array[':manager'] = $aFilter['manager'];
            $where_string[] = 'manager=:manager';
        }

        if (isset($aFilter['uid'])) {
            $where_array[':uid'] = $aFilter['uid'];
            $where_string[] = 'uid=:uid';
        }

        if (isset($aFilter['status'])) {
            $where_array[':status'] = $aFilter['status'];
            $where_string[] = 'status=:status';
        }

        if (isset($aFilter['status_in'])) {
            $where_array[':status_in'] = $aFilter['status_in'];
            $where_string[] = 'status IN(:status_in)';
        }

        $where_string = implode(' AND ', $where_string);

        $res = Yii::app()->db->createCommand()
          ->select('COUNT(id) AS sum')
          ->from('orders')
          ->where($where_string, $where_array)
          ->queryRow();

        return $res['sum'];
    }

    public function getOperatorOrdersSumm($aFilter, $with_delivery = true)
    {
//            return 2000;
        $where_string = array();
        $where_array = array();

        if (isset($aFilter['manager'])) {
            $where_array[':manager'] = $aFilter['manager'];
            $where_string[] = 'manager=:manager';
        }

        if (isset ($aFilter['uid'])) {
            $where_array[':uid'] = $aFilter['uid'];
            $where_string[] = 'uid=:uid';
        }

        if (isset($aFilter['date_from'])) {
            $DATE = new DateTime($aFilter['date_from']);
            $where_array[':date_from'] = $DATE->format('U');
            $where_string[] = 'date >= :date_from';
            unset($DATE);
        }

        if (isset($aFilter['date_to'])) {
            $DATE = new DateTime($aFilter['date_to']);
            $where_array[':date_to'] = $DATE->format('U');
            $where_string[] = 'date <= :date_to';
            unset($DATE);
        }

        $where_string[] = "status='IN_PROCESS'";
        $where_string = implode(' AND ', $where_string);

        $return = 0;

        $res = Yii::app()->db->createCommand()
          //->select('SUM(`sum`)')
          ->select('*')
          ->from('orders')
          ->where($where_string, $where_array)
          ->queryAll();

        if (count($res) > 0) {
            foreach ($res as $data) {
                $return += $data['sum'];
                if ($with_delivery) {
                    $return += $data['delivery'];
                }
            }
        }

        return $return;
    }

    public function getOperatorLife($past)
    {
        $day = floor((time() - $past) / 86400);
        return $day;
    }

    public function getManagers()
    {
        $managers = array();
        $users = Yii::app()->db->createCommand()
          ->select('*')
          ->from('users')
          ->where("role not in ('user','translator')  and status=1")
          ->queryAll();

        foreach ($users as $v) {
            $managers[$v['uid']] = $v['firstname'] . " " . $v['lastname'];
        }
        return $managers;
    }

    public function save($runValidation = true, $attributes = null)
    {
        $res = parent::save();
        return $res;
    }

    public static function getAllowedManagersForOrder($id, $uid, $manager)
    {
        $alowedManagers = self::model()->findAllBySql(
          "select uu.uid, uu.firstname, uu.lastname, uu.email,
                            (select count(0) from orders oo where oo.manager = uu.uid and oo.status='IN_PROCESS') as orders_count
                             from users uu
                            where uu.status=1 and uu.role in (
                                    select distinct role from access_rights where
                                    role in ('orderManager', 'topManager', 'superAdmin') or
                                    allow rlike '%(order|top)manager|superadmin%'
                            )
                            order by orders_count desc"
        );
        $result = array();
        foreach ($alowedManagers as $alowedManager) {
            $result[$alowedManager['uid']] = $alowedManager['firstname'] . ' ' . $alowedManager['lastname'] . ' ' . $alowedManager['email'] . ' (' . $alowedManager['orders_count'] . ')';
        }
        return $result;
    }

    public static function getUser($uid)
    {
        $user = self::model()->findByPk($uid);
        if ($user) {
            $user->userBalance = self::getBalance($user->uid);
            $user->payments = new Payment('search');
            $user->payments->unsetAttributes(); // clear any default values
            $user->payments->uid = $uid;
            $user->addresses = new Addresses('search');
            $user->addresses->unsetAttributes(); // clear any default values
            $user->addresses->uid = $uid;
            $user->manager = self::model()
              ->find("uid=:manager and role not in ('user','guest')", array('manager' => $user->default_manager));
        }
        return $user;
    }

    public static function sendRegMail($user)
    {
        $hash = Yii::app()->getSecurityManager();
        $key = $hash->hashData($user->created);
        $user->confirmationLink=Yii::app()->createAbsoluteUrl('/user/check',array('email' => $user->email,'key' => $key));
        return CmsEmailEvents::emailProcessEvents($user, 'sendRegMail');
    }

    public static function sendPassMail($user) {
        $hash = Yii::app()->getSecurityManager();
        $key = $hash->hashData($user->email);
        $user->confirmationLink=Yii::app()->createAbsoluteUrl('/user/password_reset', array(
              'email' => $user->email,
              'key' => $key
            ));
        return CmsEmailEvents::emailProcessEvents($user, 'sendPassMail');
    }

    public static function getUidByPromo($promo)
    {
        $res = self::model()
          ->find('concat(CONV(round(12*created/(uid+1)), 10, 35),CONV(uid, 10, 27))=:promo', array(':promo' => $promo));
        if ($res === false || $res === null) {
            return DSConfig::getVal('checkout_default_manager_id');
        }
        return $res->uid;
    }

    public static function getPromoByUid($uid)
    {
        $_uid = $uid;
        $res = Yii::app()->db->createCommand(
          "select concat(CONV(round(12*created/(uid+1)), 10, 35),CONV(uid, 10, 27)) as
              res from users where uid=:uid"
        )
          ->bindParam(':uid', $_uid, PDO::PARAM_INT)->queryScalar();
        return $res;
    }

    public static function getUserAsManager($uid)
    {
        $user = self::getUser($uid);
        if ($user) {
            $user->users = new Users('search');
            $user->users->unsetAttributes(); // clear any default values
            $user->users->default_manager = $uid;
//---------------------------------
            $sqlForCount = "select count(0)
    from events_log ee
where (ee.event_name like 'Order.%' or ee.event_name like 'OrdersItems.%')
and ee.subject_id in (select oo.id from orders oo where oo.manager=:manager)";

            $sql = "select ee.*, (select uu.email from users uu where uu.uid=ee.uid) as fromName,
    (select el.event_descr from events el where el.event_name= ee.event_name) as eventName
    from events_log ee
where (ee.event_name like 'Order.%' or ee.event_name like 'OrdersItems.%')
and ee.subject_id in (select oo.id from orders oo where oo.manager=:manager)";

            $count = Yii::app()->db->createCommand($sqlForCount)
              ->bindParam(':manager', $uid, PDO::PARAM_INT)
              ->queryScalar();
            $user->usersOrdersEvents = new CSqlDataProvider($sql, array(
              'params'         => array(':manager' => $uid),
              'id'             => 'order_events_manager_related',
              'keyField'       => 'id',
              'sort'           => array(
                'defaultOrder' => 'ee.date DESC',
              ),
              'totalItemCount' => $count,
              'pagination'     => array(
                'pageSize' => 20,
              )
            ));
//---------------------------------
            $sqlForCount = "select count(0) from (
select oc.id
from orders_comments oc
where oc.oid in (select oo.id from orders oo where oo.manager=:manager)
union all
select ic.id
from orders_items_comments ic
join orders_items oi on oi.id=ic.item_id
where oi.oid in (select oo.id from orders oo where oo.manager=:manager)
) rr";

            $sql = "select @row:=@row+1 as rownum, rr.*, (select uu.email from users uu where uu.uid=rr.uid) as fromName from (
select oc.id, 'ORDER' as obj_type, oc.oid as obj_id, oc.uid, oc.date, oc.message, oc.internal
from orders_comments oc
where oc.oid in (select oo.id from orders oo where oo.manager=:manager)
union all
select ic.id, 'ITEM' as obj_type, oi.oid as obj_id, ic.uid, ic.`date`, ic.message, ic.internal
from orders_items_comments ic
join orders_items oi on oi.id=ic.item_id
where oi.oid in (select oo.id from orders oo where oo.manager=:manager)
order by `date` desc
) rr
INNER JOIN (SELECT @row := 0) row";

            $count = Yii::app()->db->createCommand($sqlForCount)
              ->bindParam(':manager', $uid, PDO::PARAM_INT)
              ->queryScalar();
            $user->usersOrdersMessages = new CSqlDataProvider($sql, array(
              'params'         => array(':manager' => $uid),
              'id'             => 'order_comments_manager_related',
              'keyField'       => 'rownum',
              'sort'           => array(
                'defaultOrder' => 'rr.date DESC',
              ),
              'totalItemCount' => $count,
              'pagination'     => array(
                'pageSize' => 10,
              )
            ));
//-----------------------------------
        }
        return $user;
    }

    public static function getFirstSuperAdminId()
    {
        $user = self::model()->findBySql("select * from users where status=1 and role='superAdmin'");
        if ($user) {
            return $user['uid'];
        } else {
            return Yii::app()->user->id;
        }
    }

    public static function getAdminSearchSnippet($id, $query)
    {
        if (!function_exists('markup')) {
            function markup($val, $query)
            {
                $result=@preg_replace('/'.$query.'/i','<strong>'.$query.'</strong>',$val);
                if (isset($result)&&$result) {
                    return $result;
                } else {
                    return $val;
                }
            }
        }
        $user = self::model()->findByPk($id);
        $res = '';
        $fields = array(
          'uid',
          'firstname',
          'patroname',
          'lastname',
          'email',
          'skype',
          'vk',
          'country',
          'city',
          'index',
          'address',
          'phone',
          'role',
          'default_manager',
        );
        if ($user) {
            foreach ($fields as $field) {
                if (strlen($user->{$field}) > 0) {
                    $res = $res . '<small>' . $user->getAttributeLabel($field) . ':</small> ' . markup(
                        $user->{$field},
                        $query
                      ) . '&nbsp;';
                }
            }
        }
        return $res;
    }


    public static function afterAuthenticate($user,$authenticated){

    }

    public static function getAdminLink($id, $external = false)
    {
        $user = self::model()->findByPk($id);
        if ($user) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/users/view/id/' . $id . '&tabName=' . $user->email;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/users/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр профиля пользователя'
                ) . '" onclick="getContent(this,\'' . $user->email . '\');return false;"><i class="icon-user"></i>&nbsp;' . $user->email . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }
}