<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Question.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "questions".
 *
 * The followings are the available columns in table 'questions':
 * @property integer $id
 * @property string $theme
 * @property integer $date
 * @property integer $uid
 * @property integer $category
 * @property integer $date_change
 * @property integer $order_id
 * @property string $file
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Users $u
 */
class customQuestion extends DSEventableActiveRecord {
  public $text_status;
  public $text_category;
  public $username;

  /**
   * Returns the static model of the specified AR class.
   * @return Question the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'questions';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('date, uid, category, date_change, order_id, status', 'numerical', 'integerOnly' => TRUE),
      array('theme', 'length', 'max' => 128),
      array('file', 'length', 'max' => 500),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array('id, theme, date, uid, email, category, date_change, order_id, status, text_status', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'u' => array(self::BELONGS_TO, 'Users', 'uid'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id' => 'ID',
      'theme' => Yii::t('main', 'Тема'),
      'date' => Yii::t('main', 'Дата'),
      'uid' => Yii::t('main', 'ID пользователя'),
      'email' => Yii::t('main', 'EMail'),
      'category' => Yii::t('main', 'Категория'),
      'text_category' => Yii::t('main', 'Категория'),
      'date_change' => Yii::t('main', 'Изменено'),
      'order_id' => Yii::t('main', 'ID заказа'),
      'file' => Yii::t('main', 'Файл'),
      'status' => Yii::t('main', 'Статус'),
      'text_status' => Yii::t('main', 'Статус'),
      'username' => Yii::t('admin', 'Имя'),
      'phone' => Yii::t('admin', 'Телефон'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.
      Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
    $criteria = new CDbCriteria;

    $criteria->compare('t.id', $this->id);
    $criteria->compare('theme', $this->theme, TRUE);
    $criteria->compare("FROM_UNIXTIME(date,'%d.%m.%Y %h:%i')", $this->date, TRUE);
    if ($this->uid != NULL) {
      $criteria->compare('t.uid', $this->uid);
    }
    $criteria->compare('category', $this->category);
    $criteria->compare("FROM_UNIXTIME(date_change,'%d.%m.%Y %h:%i')", $this->date_change, TRUE);
    $criteria->compare('order_id', $this->order_id);
    $criteria->compare('t.status', $this->status);

    $criteria->select = "t.*, u.*,
      case when t.status=1 then '" . Yii::t('main', 'На рассмотрении') . "'
      when t.status=2 then '" . Yii::t('main', 'Получен ответ') . "'
      else '" . Yii::t('main', 'Закрыто') . "' end as text_status,
      case when category=1 then '" . Yii::t('main', 'Общие вопросы') . "'
      when category=2 then '" . Yii::t('main', 'Вопросы по моему заказу') . "'
      when category=3 then '" . Yii::t('main', 'Рекламация') . "'
      when category=4 then '" . Yii::t('main', 'Возврат денег') . "'
      else '" . Yii::t('main', 'Оптовые заказы') . "' end as text_category";
//      $criteria->with='u';
    $criteria->with = array('u' => array('select' => "email, lastname, firstname, phone"));
    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
      'sort' => array(
        'defaultOrder' => 't.status ASC, t.`date` DESC',
      ),
      'pagination' => array(
        'pageSize' => 25,
      ),
    ));
  }
    public static function getAdminLink($id, $external = false)
    {
        $question = self::model()->findByPk($id);
        if ($question) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/questions/view/id/' . $id . '&tabName=' .Yii::t('admin','Вопрос '). $question->id;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/questions/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр вопроса'
                ) . '" onclick="getContent(this,\'' . Yii::t('admin','Вопрос '). $question->id . '\');return false;"><i class="icon-info"></i>&nbsp;' . Yii::t('admin','Вопрос '). $question->id . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }

    public static function getUserLink($id)
    {
        $question = self::model()->findByPk($id);
        if ($question) {
            return 'http://' . DSConfig::getVal(
              'site_domain'
            ) . '/cabinet/support/view/id/' . $id;
        } else {
            return '<a href="#">' . Yii::t('main', 'Ошибка') . '</a>';
        }
    }
}