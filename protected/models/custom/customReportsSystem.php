<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ReportsSystem.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "reports_system".
 *
 * The followings are the available columns in table 'reports_system':
 * @property integer $id
 * @property string $internal_name
 * @property string $name
 * @property string $description
 * @property string $query
 * @property string $data_props
 * @property string $view_props
 * @property string $type
 * @property string $group
 * @property integer $enabled
 */
class customReportsSystem extends CActiveRecord {
  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'reports_system';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('query', 'required'),
      array('enabled', 'numerical', 'integerOnly' => TRUE),
      array('internal_name, type, group', 'length', 'max' => 512),
      array('name', 'length', 'max' => 2048),
      array('description, data_props, view_props', 'safe'),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array(
        'id, internal_name, name, description, query, data_props, view_props, type, group, enabled',
        'safe',
        'on' => 'search'
      ),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'            => Yii::t('main','id отчета'),
      'internal_name' => Yii::t('main','Внутренее имя отчета'),
      'name'          => Yii::t('main','Отображаемое имя отчета'),
      'description'   => Yii::t('main','Описание отчета'),
      'query'         => Yii::t('main','Запрос'),
      'data_props'    => Yii::t('main','Свойства данных'),
      'view_props'    => Yii::t('main','Свойства отображения'),
      'type'          => Yii::t('main','Тип отчета'),
      'group'         => Yii::t('main','Группа отчетов'),
      'enabled'       => Yii::t('main','Включен ли отчет'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('internal_name', $this->internal_name, TRUE);
    $criteria->compare('name', $this->name, TRUE);
    $criteria->compare('description', $this->description, TRUE);
    $criteria->compare('query', $this->query, TRUE);
    $criteria->compare('data_props', $this->data_props, TRUE);
    $criteria->compare('view_props', $this->view_props, TRUE);
    $criteria->compare('type', $this->type, TRUE);
    $criteria->compare('group', $this->group, TRUE);
    $criteria->compare('enabled', $this->enabled);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return ReportsSystem the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public static function getReports($group) {
      @ini_set('max_execution_time', 300);
    $reports = self::model()->findAll('`group`=:group and enabled=1', array(':group' => $group));
    $result = array();
    if ($reports) {
      foreach ($reports as $report) {
        $result[$report->internal_name] = self::getReport($report->internal_name);
      }
      return $result;
    }
    return FALSE;
  }

  public static function getReport($name, $params = FALSE,$cacheTTL=0) {
      @ini_set('max_execution_time', 300);
    $report = self::model()->find('internal_name=:internal_name  and enabled=1', array(':internal_name' => $name));
    if (!$report) {
      return FALSE;
    }
    else {

      $cache=FALSE;
      if ($cacheTTL>0) {
        $cache = Yii::app()->fileCache->get($report->internal_name.'-' . ($params&&is_array($params)&&count($params))?@implode('-',$params):$params);
      }
      if (($cache == FALSE)) {
        $result = new stdClass();
        $result->internal_name = $report->internal_name;
        $result->name = $report->name;
        $result->description = $report->description;
        $result->type = $report->type;
        $result->group = $report->group;
        try {
          if (in_array($result->type, array('DEFAULT', 'TABLE'))) {
//-----------
            $dataProps = eval('return ' . $report->data_props);
            if ($params) {
              $dataProps['params'] = $params;
              $cmd = Yii::app()->db->createCommand('select count(0) from (' . $report->query . ') cnt_query');
              foreach ($params as $paramName => $paramValue) {
                $cmd->bindParam($paramName, $params[$paramValue], PDO::PARAM_STR);
              }
              $dataProps['totalItemCount'] = $cmd->queryScalar();
            }
            else {
              $dataProps['totalItemCount'] = Yii::app()->db->createCommand('select count(0) from (' . $report->query . ') cnt_query')
                ->queryScalar();
            }
            if ($report->view_props) {
              $result->viewProps = eval('return ' . $report->view_props);
            }
            else {
              $result->viewProps = FALSE;
            }
            $result->data = new CSqlDataProvider($report->query, $dataProps);
            return $result;
//-----------
          }
          elseif (in_array($result->type, array('CHART'))) {
            $cmd = Yii::app()->db->createCommand($report->query);
            if ($params) {
              $rawData = $cmd->queryAll(TRUE, $params);
            }
            else {
              $rawData = $cmd->queryAll(TRUE);
            }
            if ($rawData && isset($rawData[0])) {
              $data = array();
              foreach ($rawData[0] as $fieldName => $val) {
                $data[$fieldName] = array();
              }
              foreach ($rawData as $row) {
                foreach ($row as $fieldName => $val) {
                  $data[$fieldName][] = $val;
                }
              }
            } else {
              return false;
            }

            $result->dataProps = eval('return ' . $report->data_props);

            if ($report->view_props) {
              $result->viewProps = $report->view_props;
            }
            else {
              $result->viewProps = FALSE;
            }

            $result->data = $data;
            Yii::app()->fileCache->set($report->internal_name.'-' . ($params&&is_array($params)&&count($params))?@implode('-',$params):$params, array($result), $cacheTTL * 60 * 60);
            return $result;
          }
          else {
            return FALSE;
          }
        } catch (Exception $e) {
          return FALSE;
        }
      }
      else {
        list($result) = $cache;
        return $result;
      }

    }
  }
}
