<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AdminNews.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "admin_news".
 *
 * The followings are the available columns in table 'admin_news':
 * @property integer $id
 * @property integer $uid
 * @property string $date
 * @property string $message
 *
 * The followings are the available model relations:
 * @property Users $u
 */
class customAdminNews extends CActiveRecord
{
  public $username;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message', 'required'),
//			array('uid', 'numerical', 'integerOnly'=>true),
			array('message', 'length', 'max'=>4000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, uid, date, message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('admin','ID сообщения'),
			'uid' => Yii::t('admin','UID отправителя'),
			'date' => Yii::t('admin','Дата'),
			'message' => Yii::t('admin','Текст сообщения'),
            'username' => Yii::t('admin','Автор'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($pageSize=20)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
      $criteria->select = "t.*, (select uu.email from users uu where uu.uid=t.uid) as username";
//    $criteria->select = new CDbExpression("DATE_FORMAT(created, '%Y-%m-%d') AS created");

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('message',$this->message,true);
        $criteria->order='t.`date` DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
/*            'sort' => array(
              'defaultOrder' => 't.date DESC',
            ), */
            'sort' => FALSE,
            'pagination' => array(
              'pageSize' => $pageSize,
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminNews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}

  public function save($runValidation = TRUE, $attributes = NULL) {
    $this->uid=Yii::app()->user->id;
    $this->date=date("Y-m-d H:i:s", time());
    return parent::save();
  }

}
