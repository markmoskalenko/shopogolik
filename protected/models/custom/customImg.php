<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Img.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customImg {
  protected static $_curlObject = FALSE;

  public static function getImagePath($originalImagePath, $format, $useCache = TRUE) {
    if ($originalImagePath == '') {
      return '#';
    }
    $isPaginated = preg_match('/page[s]*[\/=](\d+)/i', Yii::app()->request->getRequestUri(), $matches);
    if ($isPaginated > 0) {
      if ($matches[1] == 1) {
        $isPaginated = 0;
      }
    }
      if (preg_match('/\/admin\//i', Yii::app()->request->getRequestUri())) {
          $isPaginated=1;
      }
    $newPath = preg_replace('/((.+?)\.(jpg|png|gif)).*/i', '$1', $originalImagePath);
    if (preg_match('/\_\d{2,3}x\d{2,3}(?=[\.|$])/im', $newPath)) {
      $newPath = preg_replace('/\_\d{2,3}x\d{2,3}(?=[\.|$])/im', '_160x160', $newPath);
    }
    else {
      $newPath = $newPath . $format;
    }
//===================================
    if ($useCache && ($isPaginated <= 0)) {
      if ((DSConfig::getVal('seo_img_cache_enabled') == 1) && (DSConfig::getVal('site_images_lazy_load') == 1)) {
        $clearHash = md5(Yii::app()->name . $newPath);
        $hash = $clearHash . '.jpg';
        $command = Yii::app()->db->createCommand("insert ignore into img_hashes (original_url,hash,created,last_access)
        values (:original_url,:hash,Now(),Now())")
          ->bindParam(':original_url', $newPath, PDO::PARAM_STR)
          ->bindParam(':hash', $hash, PDO::PARAM_STR);
        $command->execute();
        $imgsubdomains = explode(',', DSConfig::getVal('seo_img_cache_subdomains'));
        $domain = Yii::app()->getBaseUrl(TRUE);
        if (is_array($imgsubdomains) && (count($imgsubdomains) > 0)) {
          $d = 0;
          for ($i = 0; $i <= 31; $i++) {
            $d = $d ^ hexdec($clearHash{$i});
          }
          $n = (int) ((count($imgsubdomains) / 16) * $d);
          $s = $imgsubdomains[$n];
          $subdomain = preg_replace('/(http[s]*:\/\/)/i', '$1' . $s . '.', $domain);
        }
        else {
          $subdomain = $domain . '.';
        }
//TODO Не забыть убрать
//$subdomain=$domain; // REmove after debug !!!!
        $imgURL = $subdomain . Yii::app()->createUrl('/img/index', array('url' => $hash));
        return $imgURL;
      }
    }
//===================================
    return $newPath;
  }

  public static function getData($url) { //Get data from curl
    $result = new stdClass();
    $result->content = NULL;
    $result->info = NULL;

    if (gettype(self::$_curlObject) != 'resource') {
      $ch = curl_init($url);
      self::$_curlObject = $ch;
    }
    else {
      $ch = self::$_curlObject;
      curl_setopt($ch, CURLOPT_URL, $url);
    }

    $fp = fopen('php://temp', 'w+');
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, FALSE);
// Speed-up
    curl_setopt($ch, CURLOPT_NOPROGRESS, TRUE);
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 3600);

    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_exec($ch);
    if (!curl_errno($ch)) {
      $result->info = curl_getinfo($ch);
      fseek($fp, 0);
      $fs = fstat($fp);
      if ($fs['size'] > 0) {
        $result->content = fread($fp, $fs['size']);
      }
    };
    fclose($fp);
//    curl_close($ch);
    return $result;
  }

  public static function getDataToTempFile($url) { //Get data from curl
    if (gettype(self::$_curlObject) != 'resource') {
      $ch = curl_init($url);
      self::$_curlObject = $ch;
    }
    else {
      $ch = self::$_curlObject;
      curl_setopt($ch, CURLOPT_URL, $url);
    }

    $fp = tmpfile();
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, FALSE);
// Speed-up
    curl_setopt($ch, CURLOPT_NOPROGRESS, TRUE);
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 3600);

    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_exec($ch);
    if (!curl_errno($ch)) {
      fseek($fp, 0);
      $metaData = stream_get_meta_data($fp);
      $filename = $metaData["uri"];
      if ($filename) {
        return $filename;
      }
    };
//    curl_close($ch);
    return false;
  }

  public static function add_watermark($img, $wpath) {
    // создаём водяной знак
    //$watermark_path = Yii::app()->theme->basePath . DSConfig::getVal('seo_img_cache_watermark');
    $watermark = imagecreatefrompng($wpath);
    // получаем значения высоты и ширины водяного знака
    $watermark_width = imagesx($watermark);
    $watermark_height = imagesy($watermark);
    // помещаем водяной знак на изображение
    $dest_x = imagesx($img) - $watermark_width - 10;
    $dest_y = imagesy($img) - $watermark_height - 10;
    imagealphablending($img, TRUE);
    imagealphablending($watermark, TRUE);
    // создаём новое изображение
    imagecopy($img, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
    imagedestroy($watermark);
    return $img;
  }

}
