<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersPayments.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "orders_payments".
 *
 * The followings are the available columns in table 'orders_payments':
 * @property integer $id
 * @property integer $oid
 * @property integer $uid
 * @property double $summ
 * @property string $date
 * @property string $descr
 *
 * The followings are the available model relations:
 * @property Users $u
 * @property Orders $o
 */
class customOrdersPayments extends DSEventableActiveRecord {
  public $fromName;
  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'orders_payments';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('oid, uid, summ, date', 'required'),
      array('oid, uid', 'numerical', 'integerOnly' => TRUE),
      array('summ', 'numerical'),
      array('descr', 'safe'),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array('id, oid, uid, summ, date, descr', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'u' => array(self::BELONGS_TO, 'Users', 'uid'),
      'o' => array(self::BELONGS_TO, 'Orders', 'oid'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'    => Yii::t('main','ID платежа'),
      'oid'   => Yii::t('main','ID заказа'),
      'uid'   => Yii::t('main','ID пользователя или менеджера, проведшего платеж'),
      'summ'  => Yii::t('main','Сумма платежа во внутренней валюте'),
      'date'  => Yii::t('main','Дата проведения платежа'),
      'descr' => Yii::t('main','Описание платежа'),
      'fromName' => Yii::t('main','Инициатор'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search($pageSize=100) {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('oid', $this->oid);
    $criteria->compare('uid', $this->uid);
    $criteria->compare('summ', $this->summ);
    $criteria->compare('date', $this->date, TRUE);
    $criteria->compare('descr', $this->descr, TRUE);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
      'pagination' => array(
        'pageSize' => $pageSize,
      )
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return OrdersPayments the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public static function payForOrder($orderId, $summ, $description = '', $userId = FALSE) {
    $payment = new OrdersPayments();
    $payment->oid = $orderId;
    if ($userId === FALSE) {
      $payment->uid = Yii::app()->user->id;
    }
    else {
      $payment->uid = $userId;
    }
    $payment->summ = $summ;
    $payment->descr = $description;
    $payment->date = date('Y-m-d H:i:s', time());
    return $payment->save();
  }

  public static function getPaymentsSumForOrder($orderId) {
    $sum = self::model()->findBySql("select round(ifnull(sum(t.summ),0),2) as summ from orders_payments t where t.oid=:oid", array(
      ':oid' => $orderId,
    ));
    if ($sum) {
      return Formulas::cRound($sum['summ'], FALSE, 2);
    }
    else {
      return 0;
    }
  }

  public static function getOrderPayments($orderId, $pageSize = 5,$asArray=false,$userId=false) {
    // Must to return dataProviders for comments and attaches
    $criteria = new CDbCriteria;
    $criteria->select = 't.*, (select uu.email from users uu where uu.uid=t.uid) as fromName';
      if ($userId!==false) {
          $criteria->condition = 't.uid=:uid';
          $criteria->params = array(':uid' => $userId);
      } else {
          $criteria->condition = 't.oid=:oid';
          $criteria->params = array(':oid' => $orderId);
      }
    $criteria->order = 't.`date` DESC';
    if ($asArray) {
      $rows=self::model()->findAll($criteria);
      return $rows;
    } else {
    $orderPayments = new CActiveDataProvider(self::model(), array(
      'criteria' => $criteria,
      'sort' => FALSE,
      'pagination' => array(
        'pageSize' => $pageSize,
//        'currentPage'=>'pageCount'-1,
      ),
    ));
    return $orderPayments;
    }
  }
    public static function getAdminLink($id, $external = false)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/orders/view/id/' . $id . '&tabName=' .Yii::t('admin','Заказ '). $order->uid.'-'.$order->id;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/orders/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр заказа'
                ) . '" onclick="getContent(this,\'' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '\');return false;"><i class="icon-shopping-cart"></i>&nbsp;' . Yii::t('admin','Заказ '). $order->uid.'-'.$order->id . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }

    public static function getUserLink($id)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            return 'http://' . DSConfig::getVal(
              'site_domain'
            ) . '/cabinet/orders/view/id/' . $id;
        } else {
            return '<a href="#">' . Yii::t('main', 'Ошибка') . '</a>';
        }
    }
}
