<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AccessRights.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "access_rights".
 *
 * The followings are the available columns in table 'access_rights':
 * @property string $role
 * @property string $description
 * @property string $allow
 * @property string $deny
 *
 * The followings are the available model relations:
 * @property Users[] $users
 */
class customAccessRights extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_rights';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('role', 'required'),
			array('role', 'length', 'max'=>64),
			array('description, allow, deny', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('role, description, allow, deny', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'users' => array(self::HAS_MANY, 'Users', 'role'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'role' => Yii::t('admin','Роль'),
			'description' => Yii::t('admin','Описание'),
			'allow' => Yii::t('admin','Разрешено'),
			'deny' => Yii::t('admin','Запрещено'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('role',$this->role,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('allow',$this->allow,true);
		$criteria->compare('deny',$this->deny,true);
   //   $criteria->select="t.role as id,t.*";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessRights the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}

  public static function getRoles() {
    $res=self::model()->findAllBySql('select role, description from access_rights',array());
    $resArr = array();
    if (($res!= false) && ($res != null)) {
      foreach ($res as $r) {
        $resArr[$r['role']]=$r['description'];
      }
    }
    return $resArr;
  }

}
