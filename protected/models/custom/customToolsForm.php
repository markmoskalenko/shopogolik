<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ToolsForm.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customToolsForm extends CFormModel {

    public $weight;
    public $country;
    public $category;
    public $order_id;
    public $file;
    
    public $theme;
    public $question;
    public $email;
    
    function rules() {
        return array(            
            array('weight,country', 'required', 'on'=>'calc'),
            array('weight', 'numerical', 'on'=>'calc', 'min'=>0.1),
            array('theme,question,email','required','on'=>'question'),
            array('file','file','maxSize'=>1*1024*1024,'tooLarge'=>Yii::t('main','Файл должен быть меньше, чем 1MB'), 'allowEmpty'=>true),
            array('email','email'),
            //array('order_id,file','exist','on'=>'question','className'=>'Users')
        );
    }
    
    function attributeLabels() 
    {
        return array(
            'file'=>Yii::t('main','Прикрепить файл'),
            'order_id'=>Yii::t('main','Номер заказа (если есть)'),
            'category'=>Yii::t('main','Категория'),
            'weight'=>Yii::t('main','Вес, грамм'),
            'country'=>Yii::t('main','Страна'),
            'theme'=>Yii::t('main','Тема'),
            'question'=>Yii::t('main','Текст сообщения'),
            'email'=>'E-mail',
        );
    }
}