<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersItemsStatuses.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "orders_items_statuses".
 *
 * The followings are the available columns in table 'orders_items_statuses':
 * @property integer $id
 * @property string $name
 * @property string $desc
 *
 * The followings are the available model relations:
 * @property OrdersItems[] $ordersItems
 */
class customOrdersItemsStatuses extends CActiveRecord
{
private static $_getOrderItemExcludedStatusesArray = false;
private static $_getOrderItemStatusesList = false;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders_items_statuses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, excluded', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>512),
			array('desc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, desc, excluded', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ordersItems' => array(self::HAS_MANY, 'OrdersItems', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main','id статуса посылки (лота)'),
			'name' => Yii::t('main','Статус лота'),
			'desc' => Yii::t('main','Описание'),
            'excluded' => Yii::t('main','Не учитывать'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('desc',$this->desc,true);
        $criteria->compare('excluded',$this->excluded);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
          'pagination' => array(
            'pageSize' => 100,
          )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdersItemsStatuses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}

  public static function getOrderItemStatusesList(){
    if (!self::$_getOrderItemStatusesList) {
        self::$_getOrderItemStatusesList=OrdersItemsStatuses::model()->findAll();
    }
      $orderItemStatuses=self::$_getOrderItemStatusesList;
    $result=array();
    foreach ($orderItemStatuses as $orderItemStatus) {
        $result[$orderItemStatus['id']]=Yii::t('main',$orderItemStatus['name']);
    }
    return $result;
  }

    public static function getOrderItemExcludedStatusesArray(){
if (!self::$_getOrderItemExcludedStatusesArray) {
        $orderItemStatuses=OrdersItemsStatuses::model()->findAll('excluded=1');
        $result=array();
        foreach ($orderItemStatuses as $orderItemStatus) {
            $result[]=$orderItemStatus['id'];
        }
    self::$_getOrderItemExcludedStatusesArray=$result;
} else {
    $result=self::$_getOrderItemExcludedStatusesArray;
}
        return $result;
    }

  public static function getStatusName($status) {
    $res=self::model()->find('id=:id',array(':id'=>$status));
    if ($res) {
      return Yii::t('main',$res->name);
    } else {
      return '-';
    }
  }

//=================================================================
// Возвращает список статусов и кол-во заказов в них
    public static function getAllStatusesListAndItemsCount($uid=false,$manager=false){
        if ($uid===false) {
            $_uid=null;
        } else {
            $_uid=$uid;
        }
        if ($manager===false) {
            $_manager=null;
        } else {
            $_manager=$manager;
        }
        $command = Yii::app()->db->createCommand("select * from orders_items_statuses ois order by ois.excluded");
        $statuses=$command->queryAll();
        if ($statuses) {
            foreach ($statuses as $i=>$status) {
                    $cntAndDate=Yii::app()->db->createCommand("select count(0) as cnt, max(oo.date) as lastdate,
round(sum(oi.taobao_promotion_price),2) as totalsum
from orders_items oi, orders oo
where oi.oid=oo.id and oi.status=:value and (oo.uid=:uid or :uid is null)
and (oo.manager=:manager or :manager is null)")
                      ->bindParam(':value', $status['id'], PDO::PARAM_STR)
                      ->bindParam(':uid', $_uid, PDO::PARAM_INT)
                      ->bindParam(':manager', $_manager, PDO::PARAM_INT)
                      ->queryRow();
                $statuses[$i]['count']=$cntAndDate['cnt'];
                $statuses[$i]['lastdate']=$cntAndDate['lastdate'];
                $statuses[$i]['totalsum']=$cntAndDate['totalsum'];
            }
            return $statuses;
        } else {
            return array();
        }
    }

    public static function getInQueryForOrderItemStatus($status){
        if ($status!=null) {
                $query="select oi.id from orders_items oi where oi.status={$status}";
        } else {
            $query="select oi.id from orders_items oi";
        }
        return $query;
    }

    public static function getStatusListForFilter(){
        $orderItemStatuses=self::model()->findAll('1=1 order by excluded');
        $result=array();
        foreach ($orderItemStatuses as $orderItemStatus) {
            $result[$orderItemStatus['id']]=Yii::t('main',$orderItemStatus['name']);
        }
        return $result;
    }

}
