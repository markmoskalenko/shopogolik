<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="MainMenu.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "categories_ext".
 *
 * The followings are the available columns in table 'categories_ext':
 * @property integer $id
 * @property integer $cid
 * @property string $ru
 * @property string $en
 * @property integer $parent
 * @property integer $status
 * @property string $meta_desc_ru
 * @property string $meta_keeword_ru
 * @property string $page_title_ru
 * @property string $meta_desc_en
 * @property string $meta_keeword_en
 * @property string $page_title_en
 * @property string $page_desc_ru
 * @property string $page_desc_en
 * @property string $url
 * @property string $zh
 * @property string $query
 * @property integer $level
 * @property integer $order_in_level
 *
 * The followings are the available model relations:
 * @property CategoriesExt $parent0
 * @property CategoriesExt[] $categoriesExts
 */
class customMainMenu extends CActiveRecord {

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'categories_ext';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('cid, parent, status, url, ru, en', 'required'),
      array('cid, parent, status, level, order_in_level, manual', 'numerical', 'integerOnly' => TRUE),
      array(
        'ru, en, meta_desc_ru, meta_keeword_ru, page_title_ru, meta_desc_en, meta_keeword_en, page_title_en, url, zh, query',
        'length',
        'max' => 512
      ),
      array('page_desc_ru, page_desc_en, manual,decorate', 'safe'),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array(
        'id, cid, ru, en, parent, status, meta_desc_ru, meta_keeword_ru, page_title_ru, meta_desc_en, meta_keeword_en, page_title_en, page_desc_ru, page_desc_en, url, zh, query, level, order_in_level, manual, decorate',
        'safe',
        'on' => 'search'
      ),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'parent0'        => array(self::BELONGS_TO, 'CategoriesExt', 'parent'),
      'categoriesExts' => array(self::HAS_MANY, 'CategoriesExt', 'parent'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'              => Yii::t('main', 'Id категории'),
      'cid'             => Yii::t('main', 'cid'),
      'ru'              => Yii::t('main', 'Название ru'),
      'en'              => Yii::t('main', 'Название en'),
      'zh'              => Yii::t('main', 'Название zh'),
      'query'           => Yii::t('main', 'Запрос'),
      'parent'          => Yii::t('main', 'Родительская кат.'),
      'level'           => Yii::t('main', 'Уровень'),
      'status'          => Yii::t('main', '0- выкл, 1 - вкл в меню, 2 - вкл в списке, 3 - вкл везде'),
      'meta_desc_ru'    => Yii::t('main', 'Meta description ru'),
      'meta_desc_en'    => Yii::t('main', 'Meta description en'),
      'meta_keeword_ru' => Yii::t('main', 'Meta keywords ru'),
      'meta_keeword_en' => Yii::t('main', 'Meta keywords en'),
      'page_title_ru'   => Yii::t('main', 'Title страницы ru'),
      'page_title_en'   => Yii::t('main', 'Title страницы en'),
      'page_desc_en'    => Yii::t('main', 'Описание категории en'),
      'page_desc_ru'    => Yii::t('main', 'Описание категории ru'),
      'url'             => Yii::t('main', 'Адрес страницы'),
      'order_in_level'  => Yii::t('main', 'Порядок вывода'),
      'manual' =>Yii::t('main', 'Вручную'),
      'decorate' => Yii::t('main', 'Html-оформление'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search($pageSize = 100) {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;
    $criteria->compare('id', $this->id);
    $criteria->compare('cid', $this->cid);
    $criteria->compare('ru', $this->ru, TRUE);
    $criteria->compare('en', $this->en, TRUE);
    $criteria->compare('parent', $this->parent);
    $criteria->compare('status', $this->status);
    $criteria->compare('meta_desc_ru', $this->meta_desc_ru, TRUE);
    $criteria->compare('meta_keeword_ru', $this->meta_keeword_ru, TRUE);
    $criteria->compare('page_title_ru', $this->page_title_ru, TRUE);
    $criteria->compare('meta_desc_en', $this->meta_desc_en, TRUE);
    $criteria->compare('meta_keeword_en', $this->meta_keeword_en, TRUE);
    $criteria->compare('page_title_en', $this->page_title_en, TRUE);
    $criteria->compare('page_desc_ru', $this->page_desc_ru, TRUE);
    $criteria->compare('page_desc_en', $this->page_desc_en, TRUE);
    $criteria->compare('url', $this->url, TRUE);
    $criteria->compare('zh', $this->zh, TRUE);
    $criteria->compare('query', $this->query, TRUE);
    $criteria->compare('level', $this->level);
    $criteria->compare('order_in_level', $this->order_in_level);

    return new CActiveDataProvider($this, array(
      'criteria'   => $criteria,
      'pagination' => array(
        'pageSize' => $pageSize,
      ),
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return CategoriesExt the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public static function renderTreeNode($menucat, $lang) {
    $menucat['text'] = Yii::app()->controller->widget('application.modules.admin.components.widgets.CatTreeNodeBlock', array(
      'nodeData' => $menucat,
      'lang'     => $lang,
    ), TRUE);
    /*
        [pkid] => 2
        [cid] => 0
        [ru] => Одежда
        [en] => Female attire men's clothing
        [parent] => 1
        [status] => 1
        [meta_desc_ru] => Купить недорого Одежда, распродажи
        [meta_keeword_ru] => Женская верхняя одежда, Женские
        [page_title_ru] => Недорого , Одежда, поиск в магазине DropShop.pro
        [meta_desc_en] => Buy at a low price Female attire
        [meta_keeword_en] => Female -like upper garment
        [page_title_en] => Cheap , Female attire
        [page_desc_ru] =>
        [page_desc_en] =>
        [url] => mainmenu-odezhda
        [zh] => 女装男装
        [query] => 女装男装
        [level] => 2
        [text] => Одежда id:2 cid:0 q:女装男装 taobao:女装男装
        [hasChildren] =>
        [expanded] =>
    */
    unset($menucat['cid']);
    unset($menucat['ru']);
    unset($menucat['en']);
    unset($menucat['parent']);
    unset($menucat['status']);
    unset($menucat['meta_desc_ru']);
    unset($menucat['meta_keeword_ru']);
    unset($menucat['page_title_ru']);
    unset($menucat['meta_desc_en']);
    unset($menucat['meta_keeword_en']);
    unset($menucat['page_title_en']);
    unset($menucat['page_desc_ru']);
    unset($menucat['page_desc_en']);
    unset($menucat['url']);
    unset($menucat['zh']);
    unset($menucat['query']);
    return $menucat;
  }

  public static function getMainMenu($callbackType, $lang, $fromId = 1, $status = array(1,2,3), $depth = 3,$fromAPI=false) {
    $result = array();
      $int_id_field = 'pkid';
      $data = Yii::app()->db->createCommand("
select
 id as pkid,cid,ru,en,parent,status,meta_desc_ru,meta_keeword_ru,page_title_ru,meta_desc_en,meta_keeword_en,page_title_en,page_desc_ru,
  page_desc_en,url,zh,query,level,order_in_level, manual, decorate
from categories_ext where (id!=:id and parent = :id) and status in (".implode(',',$status).")
order by status DESC, order_in_level ASC")
        ->bindParam(":id", $fromId, PDO::PARAM_INT)
        ->queryAll();
/*    else {
      $int_id_field = 'cid';
      $data = Yii::app()->db->createCommand("
select
 id as pkid, cid as id, cid,ru,en,parent,status,meta_desc_ru,meta_keeword_ru,page_title_ru,meta_desc_en,meta_keeword_en,page_title_en,page_desc_ru,
  page_desc_en,url,zh,query,level
from categories where (parent = case when :id=1 then 0 else :id end and cid<>:id)
order by cid")
        ->bindParam(":id", $fromId, PDO::PARAM_INT)
        ->queryAll();
    }
*/
    foreach ($data as $menucat) {
//------------------------------------------------------------
      if ($callbackType == 0) {
          if ($fromAPI) {
              $menucat['view_text'] = $menucat[$lang];
              unset($menucat['ru']);
              unset($menucat['en']);
              unset($menucat['zh']);
              unset($menucat['meta_desc_ru']);
              unset($menucat['meta_keeword_ru']);
              unset($menucat['page_title_ru']);
              unset($menucat['page_desc_ru']);
              unset($menucat['meta_desc_en']);
              unset($menucat['meta_keeword_en']);
              unset($menucat['page_title_en']);
              unset($menucat['page_desc_en']);
              unset($menucat['manual']);
              unset($menucat['status']);
              unset($menucat['decorate']);
              unset($menucat['url']);
          } else {
        $menucat['view_text'] = $menucat[$lang];
        unset($menucat['ru']);
        unset($menucat['en']);
        unset($menucat['zh']);
        unset($menucat['meta_desc_ru']);
        unset($menucat['meta_keeword_ru']);
        unset($menucat['page_title_ru']);
        unset($menucat['page_desc_ru']);
        unset($menucat['meta_desc_en']);
        unset($menucat['meta_keeword_en']);
        unset($menucat['page_title_en']);
        unset($menucat['page_desc_en']);
        unset($menucat['manual']);
          }
      }
      elseif ($callbackType == 1) {
        $menucat = self::renderTreeNode($menucat, $lang);
        $menucat['hasChildren'] = isset($menucat['children']);
        $menucat['expanded'] = FALSE;
        $menucat['htmlOptions'] = array('class' => 'admin-category-tree-node', 'id'=>'admin-category-tree-node-'.$menucat['pkid']);
      }
//============================================================
      $result[$menucat['pkid']] = $menucat;
      if ($menucat['level'] <= $depth) {
        $result[$menucat['pkid']]['children'] = self::getMainMenu($callbackType, $lang, $menucat[$int_id_field], $status, $depth,$fromAPI);
      }
    }
    return $result;
  }

  public static function getMainMenuRecord($lang, $Id) {
    $result = array();
    $data = Yii::app()->db->createCommand("
select SQL_NO_CACHE
 id as pkid,cid,ru,en,parent,status,meta_desc_ru,meta_keeword_ru,page_title_ru,meta_desc_en,meta_keeword_en,page_title_en,page_desc_ru,
  page_desc_en,url,zh,query,level,order_in_level, manual, decorate
from categories_ext where (id = :id) LIMIT 1")
      ->bindParam(":id", $Id, PDO::PARAM_INT)
      ->queryRow();
    if ((count($data) > 0) && ($data != FALSE)) {
      $result[$data['pkid']] = $data;
      $result[$data['pkid']]['view_text'] = $data[$lang];
      if ($data['status'] == 0) {
        $result[$data['pkid']]['text'] = CHtml::link('<i>' . $data[$lang] . '</i>', array(
          "category/update",
          "id" => $data['pkid']
        ), array(
          "onclick" => "getContent(this,\"".Yii::t('main','Категория')." №" . $data['pkid'] . "\");return false;",
          "title"   => Yii::t('main', "Изменить параметры категории")
        ));
      }
      else {
        $result[$data['pkid']]['text'] = CHtml::link($data[$lang], array(
          "category/update",
          "id" => $data['pkid']
        ), array(
          "onclick" => "getContent(this,\"".Yii::t('main','Категория')." №" . $data['pkid'] . "\");return false;",
          "title"   => Yii::t('main', "Изменить параметры категории")
        ));
      }
      $result[$data['pkid']]['hasChildren'] = TRUE;
      $result[$data['pkid']]['expanded'] = TRUE;
    }
    return $result;
  }

  public static function updateMetaAndHFURLs($offset, $count, $internal = FALSE) {
    if ($internal == 'false') {
      $cats = MainMenu::model()->findAll('cid<>-1 order by parent limit ' . $offset . ',' . $count);
    }
    else {
      $cats = Category::model()->findAll('cid<>0 order by parent limit ' . $offset . ',' . $count);
    }
    if ($cats == NULL || $cats == FALSE) {
      MainMenu::clearMenuCache();
      return 'DONE';
    }
    foreach ($cats as $cat) {
      self::updateMetaAndHFURL($cat, $internal);
    }
    return $offset + $count;
  }

  protected static function updateMetaAndHFURL($cat, $internal) {

    if (!function_exists('translit')) {
      function translit($s) {
        return Utils::translitURL($s);
      }
    }
    if ($internal == 'false') {
      try {
        $site_name = DSConfig::getVal('site_name');
        $xml = simplexml_load_string(DSConfig::getVal('seo_category_rules'), NULL, LIBXML_NOCDATA);
        $parent_cat = MainMenu::model()->findByPk($cat['parent']);
        $ru = $cat['ru'];
        $en = $cat['en'];
        $zh = $cat['zh'];
        if ($cat['parent'] != 1) {
          $parent_ru = $parent_cat['ru'];
          $parent_en = $parent_cat['en'];
          $parent_zh = $parent_cat['zh'];
        }
        else {
          $parent_ru = '';
          $parent_en = '';
          $parent_zh = '';
        }
        $level = $cat['level'];
        $children_ru = array();
        $children_en = array();
        $children_zh = array();
        $child_cats = MainMenu::model()->findAll('parent=:parent', array(':parent' => $cat['id']));
        foreach ($child_cats as $child_cat) {
          $children_ru[$child_cat['id']] = $child_cat['ru'];
          $children_en[$child_cat['id']] = $child_cat['en'];
          $children_zh[$child_cat['id']] = $child_cat['zh'];
        }
        $related_ru = array();
        $related_en = array();
        $related_zh = array();
        $related_cats = MainMenu::model()->findAll('parent=:parent', array(':parent' => $cat['parent']));
        foreach ($related_cats as $related_cat) {
          $related_ru[$related_cat['id']] = $related_cat['ru'];
          $related_en[$related_cat['id']] = $related_cat['en'];
          $related_zh[$related_cat['id']] = $related_cat['zh'];
        }
//====================
//--------------------
        $evalCommand = (string) $xml->HFURL;
        $hfurl = eval($evalCommand);
        if (MainMenu::model()->count('url=:url and id<>:id', array(':url' => $hfurl, 'id' => $cat['id'])) > 0) {
          $hfurl = $hfurl . '-' . $cat['id'];
        }
        $cat['url'] = $hfurl;
//--------------------
        $meta_desc_ru = eval((string) $xml->meta_desc_ru);
        $cat['meta_desc_ru'] = $meta_desc_ru;
//--------------------
        $meta_desc_en = eval((string) $xml->meta_desc_en);
        $cat['meta_desc_en'] = $meta_desc_en;
//--------------------
        $page_title_ru = eval((string) $xml->page_title_ru);
        $cat['page_title_ru'] = $page_title_ru;
//--------------------
        $page_title_en = eval((string) $xml->page_title_en);
        $cat['page_title_en'] = $page_title_en;
//--------------------
        $meta_keeword_ru = eval((string) $xml->meta_keeword_ru);
        $cat['meta_keeword_ru'] = $meta_keeword_ru;
//--------------------
        $meta_keeword_en = eval((string) $xml->meta_keeword_en);
        $cat['meta_keeword_en'] = $meta_keeword_en;
//--------------------
        $cat->update();
      } catch (Exception $e) {
        LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
        //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
        echo $e->getMessage();
        return FALSE;
      }
    }
    else {
//=========================================================================================================
// старые категории
//=========================================================================================================
      try {
        $site_name = DSConfig::getVal('site_name');
        $xml = simplexml_load_string(DSConfig::getVal('seo_category_rules'), NULL, LIBXML_NOCDATA);
        $parent_cat = Category::model()->findAll('cid=:parent', array(':parent' => $cat['parent']));
        $ru = $cat['ru'];
        $en = $cat['en'];
        $zh = $cat['zh'];
        if (($cat['parent'] != 0) && (isset($parent_cat[0]))) {
          $parent_ru = $parent_cat[0]['ru'];
          $parent_en = $parent_cat[0]['en'];
          $parent_zh = $parent_cat[0]['zh'];
        }
        else {
          $parent_ru = '';
          $parent_en = '';
          $parent_zh = '';
        }
        $level = $cat['level'];
        $children_ru = array();
        $children_en = array();
        $children_zh = array();
        $child_cats = Category::model()->findAll('parent=:parent', array(':parent' => $cat['cid']));
        foreach ($child_cats as $child_cat) {
          $children_ru[$child_cat['id']] = $child_cat['ru'];
          $children_en[$child_cat['id']] = $child_cat['en'];
          $children_zh[$child_cat['id']] = $child_cat['zh'];
        }
        $related_ru = array();
        $related_en = array();
        $related_zh = array();
        $related_cats = Category::model()->findAll('parent=:parent', array(':parent' => $cat['parent']));
        foreach ($related_cats as $related_cat) {
          $related_ru[$related_cat['id']] = $related_cat['ru'];
          $related_en[$related_cat['id']] = $related_cat['en'];
          $related_zh[$related_cat['id']] = $related_cat['zh'];
        }
//====================
//--------------------
        $hfurl = eval((string) $xml->HFURL);
        $hfurl = 'cat-' . $hfurl;
        if (Category::model()->count('url=:url and id<>:id', array(':url' => $hfurl, 'id' => $cat['id'])) > 0) {
          $hfurl = $hfurl . '-' . $cat['id'];
        }
        $cat['url'] = $hfurl;
//--------------------
        $meta_desc_ru = eval((string) $xml->meta_desc_ru);
        $cat['meta_desc_ru'] = $meta_desc_ru;
//--------------------
        $meta_desc_en = eval((string) $xml->meta_desc_en);
        $cat['meta_desc_en'] = $meta_desc_en;
//--------------------
        $page_title_ru = eval((string) $xml->page_title_ru);
        $cat['page_title_ru'] = $page_title_ru;
//--------------------
        $page_title_en = eval((string) $xml->page_title_en);
        $cat['page_title_en'] = $page_title_en;
//--------------------
        $meta_keeword_ru = eval((string) $xml->meta_keeword_ru);
        $cat['meta_keeword_ru'] = $meta_keeword_ru;
//--------------------
        $meta_keeword_en = eval((string) $xml->meta_keeword_en);
        $cat['meta_keeword_en'] = $meta_keeword_en;
//--------------------
        $cat->update();
      } catch (Exception $e) {
        LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
        //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
        echo $e->getMessage();
        return FALSE;
      }
    }
//=========================================================================================================
    return TRUE;
  }

  protected function processMetaAndHFURL() {

    if (!function_exists('translit')) {
      function translit($s) {
        return Utils::translitURL($s);
      }
    }
    try {
      $site_name = DSConfig::getVal('site_name');
      $xml = simplexml_load_string(DSConfig::getVal('seo_category_rules'), NULL, LIBXML_NOCDATA);
      $parent_cat = MainMenu::model()->findByPk($this['parent']);
      $ru = $this['ru'];
      $en = $this['en'];
      $zh = $this['zh'];
      if ($this['parent'] != 1) {
        $parent_ru = $parent_cat['ru'];
        $parent_en = $parent_cat['en'];
        $parent_zh = $parent_cat['zh'];
      }
      else {
        $parent_ru = '';
        $parent_en = '';
        $parent_zh = '';
      }
      $level = $this['level'];
      $children_ru = array();
      $children_en = array();
      $children_zh = array();
      $child_cats = MainMenu::model()->findAll('parent=:parent', array(':parent' => $this['id']));
      foreach ($child_cats as $child_cat) {
        $children_ru[$child_cat['id']] = $child_cat['ru'];
        $children_en[$child_cat['id']] = $child_cat['en'];
        $children_zh[$child_cat['id']] = $child_cat['zh'];
      }
      $related_ru = array();
      $related_en = array();
      $related_zh = array();
      $related_cats = MainMenu::model()->findAll('parent=:parent', array(':parent' => $this['parent']));
      foreach ($related_cats as $related_cat) {
        $related_ru[$related_cat['id']] = $related_cat['ru'];
        $related_en[$related_cat['id']] = $related_cat['en'];
        $related_zh[$related_cat['id']] = $related_cat['zh'];
      }
//====================
//--------------------
      $evalCommand = (string) $xml->HFURL;
      $hfurl = eval($evalCommand);
      if (MainMenu::model()->count('url=:url and id<>:id', array(':url' => $hfurl, 'id' => $this['id'])) > 0) {
        $hfurl = $hfurl . '-' . $this['id'];
      }
      if ($this['url'] == '*') {
        $this['url'] = $hfurl;
      }
//--------------------
      $meta_desc_ru = eval((string) $xml->meta_desc_ru);
      if ($this['meta_desc_ru'] == '') {
        $this['meta_desc_ru'] = $meta_desc_ru;
      }
//--------------------
      $meta_desc_en = eval((string) $xml->meta_desc_en);
      if ($this['meta_desc_en'] == '')
        $this['meta_desc_en'] = $meta_desc_en;
//--------------------
      $page_title_ru = eval((string) $xml->page_title_ru);
      if ($this['page_title_ru'] == '')
        $this['page_title_ru'] = $page_title_ru;
//--------------------
      $page_title_en = eval((string) $xml->page_title_en);
      if ($this['page_title_en'] == '')
        $this['page_title_en'] = $page_title_en;
//--------------------
      $meta_keeword_ru = eval((string) $xml->meta_keeword_ru);
      if ($this['meta_keeword_ru'] == '')
        $this['meta_keeword_ru'] = $meta_keeword_ru;
//--------------------
      $meta_keeword_en = eval((string) $xml->meta_keeword_en);
      if ($this['meta_keeword_en'] == '')
        $this['meta_keeword_en'] = $meta_keeword_en;
//--------------------
    } catch (Exception $e) {
      LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
      //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
      echo $e->getMessage();
    }
//=========================================================================================================
  }

  public static function updateVirtualFromTaobao() {
    try {
//================================================================
/*
      $command = Yii::app()->db->createCommand("update LOW_PRIORITY cn_dic cc
 set cc.TEXT_RU=(select ee.ru from categories_ext ee where ee.zh=cc.TEXT_ZH LIMIT 1),
  cc.TEXT_EN=(select ee.en from categories_ext ee where ee.zh=cc.TEXT_ZH LIMIT 1),
  cc.TEXT_RU_LENGTH=(select char_length(ee.ru) from categories_ext ee where ee.zh=cc.TEXT_ZH LIMIT 1),
  cc.TEXT_EN_LENGTH=(select char_length(ee.en) from categories_ext ee where ee.zh=cc.TEXT_ZH LIMIT 1),
  cc.TRANSLATION_STATE=1
where exists (select 'x' from categories_ext ee where ee.zh=cc.TEXT_ZH LIMIT 1)");
      $command->execute();

      $command = Yii::app()->db->createCommand("insert delayed into cn_dic (SRC_LANG, FREQ, TRANSLATION_STATE, TEXT_ZH, TEXT_RU, TEXT_EN, TEXT_RU_LENGTH, TEXT_EN_LENGTH, TEXT_ZH_LENGTH)
select
'zh-CHS', 1,1, `zh`, `ru`, `en`,
 char_length(`ru`), char_length(`en`), char_length(`zh`)
from categories_ext ee
where not exists (select 'x' from cn_dic cc where ee.zh=cc.TEXT_ZH)");
      $command->execute();
*/
//================================================================
//      $data->data = iconv('GBK', 'UTF-8//IGNORE', $data->data);
      $xml = simplexml_load_string(DSConfig::getVal('search_CategoriesUpdate'), NULL, LIBXML_NOCDATA);
        $data = Utils::getHttpDocument((string)$xml->url,true);

      $result = array();
      $preg_data_level1 = $data->data;
      $preg_res_level1 = array();
      $res = preg_match_all((string) $xml->level1->block, $preg_data_level1, $preg_res_level1, PREG_SET_ORDER);
      $i = 1;
      $parent = 1;
      foreach ($preg_res_level1 as $level1 => $res_level1) {
        $i = $i + 1;
        $result[$i] = new stdClass();
        $result[$i]->parent = $parent;
        $result[$i]->id = $i;
        $result[$i]->level = 1;
        $res = preg_match((string) $xml->level1->name, $res_level1[0], $match);
        if (!$res) {
          array_pop($result);
          continue;
        }
        $result[$i]->zh = $match[1];
        if ((string) $xml->level1->cid != '') {
          $res = preg_match((string) $xml->level1->cid, $res_level1[0], $match);
          if (!$res) {
            $result[$i]->cid = 0;
          }
          else {
            $result[$i]->cid = $match[1];
          }
        }
        else {
          $result[$i]->cid = 0;
        }
        if ((string) $xml->level1->q != '') {
          $res = preg_match((string) $xml->level1->q, $res_level1[0], $match);
          if (!$res) {
            $result[$i]->q = '';
          }
          else {
            $result[$i]->q = iconv('GBK', 'UTF-8//IGNORE', urldecode($match[1]));
          }
        }
        else {
          $result[$i]->q = '';
        }
        //----- L2 -----------------------
        $preg_data_level2 = $res_level1[0];
        $preg_res_level2 = array();
        $parent_level2 = $i;
        $res = preg_match_all((string) $xml->level2->block, $preg_data_level2, $preg_res_level2, PREG_SET_ORDER);
        foreach ($preg_res_level2 as $level2 => $res_level2) {
          $i = $i + 1;
          $result[$i] = new stdClass();
          $result[$i]->parent = $parent_level2;
          $result[$i]->id = $i;
          $result[$i]->level = 2;
          $res = preg_match((string) $xml->level2->name, $res_level2[0], $match);
          if (!$res) {
            array_pop($result);
            continue;
          }
          $result[$i]->zh = $match[1];
          if ((string) $xml->level2->cid != '') {
            $res = preg_match((string) $xml->level2->cid, $res_level2[0], $match);
            if (!$res) {
              $result[$i]->cid = 0;
            }
            else {
              $result[$i]->cid = $match[1];
            }
          }
          else {
            $result[$i]->cid = 0;
          }
          if ((string) $xml->level2->q != '') {
            $res = preg_match((string) $xml->level2->q, $res_level2[0], $match);
            if (!$res) {
              $result[$i]->q = '';
            }
            else {
              $result[$i]->q = iconv('GBK', 'UTF-8//IGNORE', urldecode($match[1]));
            }
          }
          else {
            $result[$i]->q = '';
          }
          //----- L3 -----------------------
          $preg_data_level3 = $res_level2[0];
          $preg_res_level3 = array();
          $parent_level3 = $i;
          $res = preg_match_all((string) $xml->level3->block, $preg_data_level3, $preg_res_level3, PREG_SET_ORDER);
          foreach ($preg_res_level3 as $level3 => $res_level3) {
            $i = $i + 1;
            $result[$i] = new stdClass();
            $result[$i]->parent = $parent_level3;
            $result[$i]->id = $i;
            $result[$i]->level = 3;
            $res = preg_match((string) $xml->level3->name, $res_level3[0], $match);
            if (!$res) {
              array_pop($result);
              continue;
            }
            $result[$i]->zh = $match[1];
            if ((string) $xml->level3->cid != '') {
              $res = preg_match((string) $xml->level3->cid, $res_level3[0], $match);
              if (!$res) {
                $result[$i]->cid = 0;
              }
              else {
                $result[$i]->cid = $match[1];
              }
            }
            else {
              $result[$i]->cid = 0;
            }
            if ((string) $xml->level3->q != '') {
              $res = preg_match((string) $xml->level3->q, $res_level3[0], $match);
              if (!$res) {
                $result[$i]->q = '';
              }
              else {
                $result[$i]->q = iconv('GBK', 'UTF-8//IGNORE', urldecode($match[1]));
              }
            }
            else {
              $result[$i]->q = '';
            }
            if ((string) $xml->level3->ppath != '') {
              $res = preg_match((string) $xml->level3->ppath, $res_level3[0], $match);
              if ($res) {
                $result[$i]->q = $result[$i]->q.'&ppath='.urldecode($match[1]);
              }
            }
            //----- L4 --------------------
            //----- End L4 ----------------
          }
          //----- End L3 -------------------
        }
        //----- End L2 -------------------
      }
//      print_r($result);
      if (count($result) > 900) {
        $command = Yii::app()->db->createCommand("DELETE FROM categories_ext_taobao
          where cid>=0");
        $command->execute();

        foreach ($result as $catRec) {
          //translateLocal($query = '', $from = 'zh-CN', $to = 'ru', $strong = FALSE, $intOnly = TRUE) {
            $localTranslation=Yii::app()->DanVitTranslator->translateLocal($catRec->zh, 'zh-CN', 'ru', TRUE, FALSE);
          $catRec->ru = (($localTranslation!=$catRec->zh) ? $localTranslation: Utils::getRemoteTranslationPlain($catRec->zh, 'zh-CN', 'ru'));//
            $localTranslation=Yii::app()->DanVitTranslator->translateLocal($catRec->zh, 'zh-CN', 'en', TRUE, FALSE);
          $catRec->en = (($localTranslation!=$catRec->zh) ? $localTranslation: Utils::getRemoteTranslationPlain($catRec->zh, 'zh-CN', 'en'));//

          $catRec->url = '';
        }
        foreach ($result as $catRec) {
          $status = 3;
          $meta_desc_ru = '';
          $meta_keeword_ru = '';
          $page_title_ru = '';
          $meta_desc_en = '';
          $meta_keeword_en = '';
          $page_title_en = '';
          $page_desc_ru = '';
          $page_desc_en = '';
          $order_in_level = 0;
          $command = Yii::app()->db->createCommand("
          insert ignore into categories_ext_taobao (id,cid,ru,en,parent,status,meta_desc_ru,meta_keeword_ru,page_title_ru,meta_desc_en,meta_keeword_en,
            page_title_en,page_desc_ru,page_desc_en,url,zh,query,level,order_in_level)
  values(:id,:cid,:ru,:en,:parent,
  :status,:meta_desc_ru,:meta_keeword_ru,:page_title_ru,:meta_desc_en,:meta_keeword_en,
            :page_title_en,:page_desc_ru,:page_desc_en,:url,:zh,:query,:level,:order_in_level)")
            ->bindParam(':id', $catRec->id, PDO::PARAM_INT)
            ->bindParam(':cid', $catRec->cid, PDO::PARAM_INT)
            ->bindParam(':ru', $catRec->ru, PDO::PARAM_STR)
            ->bindParam(':en', $catRec->en, PDO::PARAM_STR)
            ->bindParam(':parent', $catRec->parent, PDO::PARAM_INT)
            ->bindParam(':status', $status, PDO::PARAM_INT)
            ->bindParam(':meta_desc_ru', $meta_desc_ru, PDO::PARAM_STR)
            ->bindParam(':meta_keeword_ru', $meta_keeword_ru, PDO::PARAM_STR)
            ->bindParam(':page_title_ru', $page_title_ru, PDO::PARAM_STR)
            ->bindParam(':meta_desc_en', $meta_desc_en, PDO::PARAM_STR)
            ->bindParam(':meta_keeword_en', $meta_keeword_en, PDO::PARAM_STR)
            ->bindParam(':page_title_en', $page_title_en, PDO::PARAM_STR)
            ->bindParam(':page_desc_ru', $page_desc_ru, PDO::PARAM_STR)
            ->bindParam(':page_desc_en', $page_desc_en, PDO::PARAM_STR)
            ->bindParam(':url', $catRec->url, PDO::PARAM_STR)
            ->bindParam(':zh', $catRec->zh, PDO::PARAM_STR)
            ->bindParam(':query', $catRec->q, PDO::PARAM_STR)
            ->bindParam(':level', $catRec->level, PDO::PARAM_INT)
            ->bindParam(':order_in_level', $order_in_level, PDO::PARAM_INT);
          $command->execute();
        }
//        self::updateMetaAndHFURL();
        $command = Yii::app()->db->createCommand("update ignore categories_ext_taobao tt, categories_ext tt1
set tt.`status`=tt1.`status`,
  tt.`level` = tt1.`level`,
  tt.`page_desc_en` = tt1.`page_desc_en`,
  tt.`page_desc_ru` = tt1.`page_desc_ru`,
  tt.`order_in_level` = tt1.`order_in_level`,
  tt.`decorate` = tt1.`decorate`
--  ,
--  tt.`parent` = if (tt1.manual=1, tt1.`parent`, tt.`parent`)
  where tt1.cid=tt.cid and (tt1.query=tt.query or (tt1.zh=tt.zh and tt1.query<>tt.query))");
        $command->execute();

        $command = Yii::app()->db->createCommand("DELETE from categories_ext
          where categories_ext.cid<>-1 and categories_ext.manual<>1");
        $command->execute();

        $command = Yii::app()->db->createCommand("DELETE from categories_search");
        $command->execute();

        $command = Yii::app()->db->createCommand("insert ignore into categories_ext
          select * from categories_ext_taobao where categories_ext_taobao.cid<>-1");
        $command->execute();
        $command = Yii::app()->db->createCommand("update ignore categories_ext tt1
          set tt1.query=tt1.zh where tt1.cid=0 and (tt1.query is null or tt1.query='')");
        $command->execute();
        $command = Yii::app()->db->createCommand("update categories_ext ce
          set ce.order_in_level = (select count(0)*100+100 from (select cce.parent, cce.id from categories_ext cce) ccee
          where ccee.parent = ce.parent and ccee.id<ce.id)
          where ce.manual=0");
        $command->execute();
        self::clearMenuCache();
        return TRUE;
      }
      else {
        self::clearMenuCache();
        echo Yii::t('main', 'Получено менее 900 категорий, обновление отменено.');
        return FALSE;
      }
    } catch (Exception $e) {
      echo $e->getMessage();
      self::clearMenuCache();
      return FALSE;
    }
  }

  public static function clearMenuCache() {
    Yii::app()->cache->delete('MainMenu-getTree-ru');
    Yii::app()->cache->delete('MainMenu-getTree-en');
    Yii::app()->cache->delete('MainMenu-getTree-zh');
      Yii::app()->cache->delete('MainMenu-getList-ru');
      Yii::app()->cache->delete('MainMenu-getList-en');
      Yii::app()->cache->delete('MainMenu-getList-zh');
      Yii::app()->cache->delete('MainMenu-getTree-rendering-ru');
      Yii::app()->cache->delete('MainMenu-getTree-rendering-en');
      Yii::app()->cache->delete('MainMenu-getTree-rendering-zh');
    Yii::app()->cache->delete('categories-getTree-ru');
    Yii::app()->cache->delete('categories-getTree-en');
    Yii::app()->cache->delete('categories-getTree-zh');
    Yii::app()->cache->delete('MainMenu-admin-getTree');
  }

  public static function getUrl($model) {
    if (isset($model->url) && $model->url !== '') {
      return $model->url;
    }
    else {
      $url = trim(strtolower($model->en));
      $url = strtr($url, array(
        ' '  => '-',
        '/'  => '-',
        '_'  => '-',
        ','  => '',
        '\'' => '',
      ));
      $url = strtr($url, array(
        '---' => '-',
        '--'  => '-',
      ));
        $model = MainMenu::model()->findByPk($model->id);
      $model->url = $url;
      $model->update();
      return $url;
    }
  }

  public static function getParents($cat, $result, $level) {
    $model = MainMenu::model()->find('id=:id', array(':id' => $cat->parent));
    if ($model) {
      $res = new StdClass();
      $res->id = $model->id;
      $res->cid = $model->cid;
      $res->url = self::getUrl($model);
      $res->parent = $model->parent;
      $res->name = $model->{Utils::TransLang()};
      $res->query = $model->query;
      $result[$level] = $res;

      if (($model->cid != -1)) {
        $result = self::getParents($res, $result, $level + 1);
      }
      else {
        array_pop($result);
      }
    }
    return $result;
  }

  public function save($runValidation = TRUE, $attributes = NULL) {
    if ($this->isNewRecord) {
    $this->manual=1;
    $this->processMetaAndHFURL();
    } else {
      $oldRec=MainMenu::findByPk($this->id);
      if ($oldRec) {
        if ($oldRec->parent!=$this->parent) {
          $this->manual=1;
        }
        if ($oldRec->order_in_level!=$this->order_in_level) {
          $this->manual=1;
        }
      }
    }
    self::clearMenuCache();
    $res=parent::save();
    if (!$res) {
      $err=$this->getErrors();
    }
    return $res;
  }

  public function delete() {
    self::clearMenuCache();
    return parent::delete();
  }

  public static function getCatList() {
    $recs = Yii::app()->db->createCommand("select cc2.id, cc4.ru as parent_ru,cc2.ru, cc4.en  as parent_en,cc2.en, cc4.zh  as parent_zh,cc2.zh, cc2.parent
FROM categories_ext cc2
left join categories_ext cc4 on cc4.id = cc2.parent
where cc2.parent in (select cc1.id FROM categories_ext cc1
where cc1.parent=1)
order by cc2.status DESC, cc2.order_in_level, cc4.ru, cc2.ru")->queryAll();
    $res = array();
    if ($recs) {
      foreach ($recs as $rec) {
        if ($rec[Utils::TransLang()] == '') {
          continue;
        }
          if ($rec['parent_' . Utils::TransLang()]=='Root') {
              $prefix='';
          } else {
              $prefix='   -- ';
          }
        $res[$rec['id']] = $prefix.'('.$rec['id'].') '.$rec['parent_' . Utils::TransLang()] . '\\' . $rec[Utils::TransLang()];
      }
    }
    return $res;
  }

public static function storageCommand($command,$name) {

  $_name = $name;
  if ($command == 'save') {
    Yii::app()->db->createCommand('delete quick from `categories_ext_storage` where `store_name` = :name;
insert into `categories_ext_storage` (`store_name`,`store_date`,`id`,`cid`,`ru`,`en`,`parent`,`status`,
 `meta_desc_ru`,`meta_keeword_ru`,`page_title_ru`,`meta_desc_en`,`meta_keeword_en`,`page_title_en`,
  `page_desc_ru`,`page_desc_en`,`url`,`zh`,`query`,`level`,`order_in_level`,`manual`,`decorate`)
  select :name,Now(), categories_ext.* from categories_ext;')
      ->bindParam(':name', $_name, PDO::PARAM_STR)
      ->execute();
    return $name . ': ' . Yii::t('admin', 'категории сохранены!');
  }
  elseif ($command == 'restore') {
    Yii::app()->db->createCommand('delete from categories_ext;
SET FOREIGN_KEY_CHECKS=0;
insert into `categories_ext` (`id`,`cid`,`ru`,`en`,`parent`,`status`,
 `meta_desc_ru`,`meta_keeword_ru`,`page_title_ru`,`meta_desc_en`,`meta_keeword_en`,`page_title_en`,
  `page_desc_ru`,`page_desc_en`,`url`,`zh`,`query`,`level`,`order_in_level`,`manual`,`decorate`)
select `id`,`cid`,`ru`,`en`,`parent`,`status`,
 `meta_desc_ru`,`meta_keeword_ru`,`page_title_ru`,`meta_desc_en`,`meta_keeword_en`,`page_title_en`,
  `page_desc_ru`,`page_desc_en`,`url`,`zh`,`query`,`level`,`order_in_level`,`manual`,`decorate` from categories_ext_storage
where `store_name`=:name;
SET FOREIGN_KEY_CHECKS=1;')
      ->bindParam(':name', $_name, PDO::PARAM_STR)
      ->execute();
    return $name . ': ' . Yii::t('admin', 'категории восстановлены!');
  }
  elseif ($command == 'delete') {
    Yii::app()->db->createCommand('delete quick from `categories_ext_storage` where `store_name` = :name;')
      ->bindParam(':name', $_name, PDO::PARAM_STR)
      ->execute();
    return $name . ': ' . Yii::t('admin', 'категории удалены!');
  }
}

}