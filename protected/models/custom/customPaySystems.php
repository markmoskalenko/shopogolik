<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="PaySystems.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "pay_systems".
 *
 * The followings are the available columns in table 'pay_systems':
 * @property integer $id
 * @property integer $enabled
 * @property string $logo_img
 * @property string $int_name
 * @property string $name
 * @property string $descr_ru
 * @property string $descr_en
 * @property string $parameters
 */
class customPaySystems extends CActiveRecord {
  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pay_systems';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('enabled', 'numerical', 'integerOnly' => TRUE),
      array('logo_img', 'length', 'max' => 512),
      array('int_name, name_ru,name_en', 'length', 'max' => 256),
      array('enabled, logo_img, int_name, name_ru, name_en, descr_ru, descr_en, parameters, form_ru, form_en', 'safe'),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array('id, enabled, logo_img, int_name, name_ru, name_en, descr_ru, descr_en, parameters', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id' => 'ID',
      'enabled' => Yii::t('admin', 'Включено'),
      'logo_img' => Yii::t('admin', 'Логотип'),
      'int_name' => Yii::t('admin', 'Внутр. имя'),
      'name_ru' => Yii::t('admin', 'Название Ru'),
      'name_en' => Yii::t('admin', 'Название En'),
      'descr_ru' => Yii::t('admin', 'Описание Ru'),
      'descr_en' => Yii::t('admin', 'Описание En'),
      'parameters' => Yii::t('admin', 'Параметры'),
      'form_ru' => Yii::t('admin', 'Форма Ru'),
      'form_en' => Yii::t('admin', 'Форма En'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('enabled', $this->enabled);
    $criteria->compare('logo_img', $this->logo_img, TRUE);
    $criteria->compare('int_name', $this->int_name, TRUE);
    $criteria->compare('name_ru', $this->name_ru, TRUE);
    $criteria->compare('name_en', $this->name_en, TRUE);
    $criteria->compare('descr_ru', $this->descr_ru, TRUE);
    $criteria->compare('descr_en', $this->descr_en, TRUE);
    $criteria->compare('parameters', $this->parameters, TRUE);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return Paysystems the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public static function preRenderForm($userPayment, $type, $value=false) {
    $paySystem = (self::model()->find('int_name=:type', array('type' => $type)));
    if (!$value) {
    $htmlForm = $paySystem['form_' . Utils::TransLang()];
    } else {
      $htmlForm = $value;
    }
    $htmlForm = Utils::prepareHtmlTemplate($htmlForm, (array) $userPayment);
    $parameters = $paySystem['parameters'];
    $params = (array) simplexml_load_string($parameters, NULL, LIBXML_NOCDATA);
    $htmlForm = Utils::prepareHtmlTemplate($htmlForm, $params, '{#', '}');
    if ($type == 'order_alt') {
      $sumInWords = Utils::num2str($userPayment['sum'], 'ua');
    }
    else {
      $sumInWords = Utils::num2str($userPayment['sum'], 'ru');
    }
    $serviceVars = array(
      'Now' => date('d/m/Y'),
      'dayMonthNow' => date('dm'),
      'sumInWords' => $sumInWords,
    );
    $htmlForm = Utils::prepareHtmlTemplate($htmlForm, $serviceVars);
    return $htmlForm;
  }
}
