<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CheckoutForm.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customCheckoutForm extends CFormModel {

    public $firstname;

    public $lastname;
    
    public $patroname;
    
    //address
    public $country;    
    public $index;    
    public $city;    
    public $address;    
    public $phone;
    public $step;
    public $region;
    
    public $delivery;

  public function onUnsafeAttribute($name, $value) {
    //silently ignore unsafe attributes
  }
    
    function rules() {
        return array(            
            array('firstname,patroname,lastname,country,index,city,address,phone','required','on'=>'address'),
            //array('patroname','exist','on'=>'address','className'=>'Users'),
            array('delivery','required','on'=>'delivery'),
            array('step','safe'),
        );
    }
    
    function attributeLabels() 
    {
        return array(
            'region'    => Yii::t('main','Регион'), 
            'patroname' => Yii::t('main','Отчество'),
            'firstname' => Yii::t('main','Имя'),
            'lastname'  => Yii::t('main','Фамилия'),            
            'country'   => Yii::t('main','Страна'),
            'city'      => Yii::t('main','Город'),
            'index'     => Yii::t('main','Индекс'),
            'address'   => Yii::t('main','Адрес'),
            'phone'     => Yii::t('main','Телефон'),            
        );
    }
}