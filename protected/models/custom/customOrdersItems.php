<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="OrdersItems.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * This is the model class for table "orders_items".
 *
 * The followings are the available columns in table 'orders_items':
 * @property integer $id
 * @property integer $oid
 * @property string $iid
 * @property string $pic_url
 * @property string $sku_id
 * @property string $props
 * @property string $title
 * @property string $seller_nick
 * @property integer $seller_id
 * @property integer $status
 * @property double $num
 * @property double $weight
 * @property double $express_fee
 * @property string $input_props
 * @property double $taobao_price
 * @property double $taobao_promotion_price
 * @property string $tid
 * @property string $track_code
 * @property double $actual_num
 * @property double $actual_lot_weight
 * @property double $actual_lot_express_fee
 * @property double $actual_lot_price
 *
 * The followings are the available model relations:
 * @property OrdersItemsStatuses $status0
 * @property Orders $o
 * @property OrdersItemsComments[] $ordersItemsComments
 * @property ParcelCode[] $parcelCodes
 */
class customOrdersItems extends DSEventableActiveRecord {
  public $status_text;
  public $calculated_lotPrice = 0;
  public $calculated_lotWeight = 0;
  public $calculated_lotExpressFee = 0;
  public $calculated_actualPrice = 0;
  public $calculated_actualWeight = 0;
  public $calculated_actualExpressFee = 0;
  public $calculated_operatorProfit = 0;
  public $vars=array();
//========================================
    public $ordr_user_email;
    public $ordr_operator_email;
    public $uid;
    public $mid;
    public $textdate;
    public $statusname;
    public $country;
    public $city;
    public $phone;
    public $address;
    public $firstname;
    public $lastname;
//========================================


  public function varReport($varName) {
      if (DSConfig::getVal('formulas_show_titles_hints')!=1) {
          return '';
      }
    if (isset($this->vars) && isset($this->vars[$varName])) {
      $var=$this->vars[$varName];
      $result= '$vars[\''.$varName.'\']'.'<i>'.$var->description.'</i>';//.$var->report();
      $result=$result.$var->report();
    } else {
      $result= '$vars[\''.$varName.'\'] - не определено!';
    }
    return $result;
  }

  function afterFind() {
    parent::afterFind();
    $this->vars=Formulas::calculateOrderItem($this);
    foreach ($this->vars as $name => $val) {
      if (isset($this->$name)) {
        if (is_float($val->val)) { // && (!is_int($val->val))
          $this->$name=$val->cval;
        } else {
          $this->$name=$val->val;
        }
      }
    }
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'orders_items';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('iid', 'required'),
      array('oid, seller_id, status', 'numerical', 'integerOnly' => TRUE),
      array(
        'num, weight, express_fee, taobao_price, taobao_promotion_price, actual_num, actual_lot_weight, actual_lot_express_fee, actual_lot_price',
        'numerical'
      ),
      array('iid', 'length', 'max' => 20),
      array('pic_url, seller_nick', 'length', 'max' => 1024),
      array('sku_id', 'length', 'max' => 256),
      array('props, tid, track_code', 'length', 'max' => 4000),
      array('title, input_props', 'safe'),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array(
        'id, oid, iid, pic_url, sku_id, props, title, seller_nick, seller_id, status, num, weight, express_fee, input_props, taobao_price, taobao_promotion_price, tid, track_code, actual_num, actual_lot_weight, actual_lot_express_fee, actual_lot_price',
        'safe',
        'on' => 'search'
      ),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'statuses'             => array(self::BELONGS_TO, 'OrdersItemsStatuses', 'status'),
      'order'                   => array(self::BELONGS_TO, 'Orders', 'oid'),
      'ordersItemsComments' => array(self::HAS_MANY, 'OrdersItemsComments', 'item_id'),
      'managers'        => array(self::BELONGS_TO, 'Users', 'manager'),
      'users'           => array(self::BELONGS_TO, 'Users', 'uid'),
      'addresses'       => array(self::BELONGS_TO, 'Addresses', 'addresses_id'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id'                     => Yii::t('admin', 'ID лота в заказе'),
      'oid'                    => Yii::t('admin', 'ID заказа'),
      'iid'                    => Yii::t('admin', 'IID товара лота'),
      'pic_url'                => Yii::t('admin', 'url картинки товара'),
      'props'                  => Yii::t('admin', 'PID:VID товара (SKU)'),
      'title'                  => Yii::t('admin', 'Название товара'),
      'seller_nick'            => Yii::t('admin', 'Ник продавца'),
      'seller_id'              => Yii::t('admin', 'ID продавца'),
      'status'                 => Yii::t('admin', 'Статус лота'),
      'num'                    => Yii::t('admin', 'Кол-во товаров в лоте'),
      'weight'                 => Yii::t('admin', 'Вес 1 шт. товара'),
      'express_fee'            => Yii::t('admin', 'Стоимость доставки по Китаю'),
      'input_props'            => Yii::t('admin', 'Свойства товара лота'),
      'sku_id'                 => Yii::t('admin', 'SKU'),
      'taobao_price'           => Yii::t('admin', 'Объявленная стоимость закупки'),
      'taobao_promotion_price' => Yii::t('admin', 'Объявленная стоимость закупки'),
      'tid'                    => Yii::t('admin', 'ID сделки'),
      'track_code'             => Yii::t('admin', 'Трэк-код'),
      'status_text'            => Yii::t('admin', 'Статус'),
      'actual_num'             => Yii::t('admin', 'Реально закупленное количество'),
      'actual_lot_weight'      => Yii::t('admin', 'Реальный вес лота'),
      'actual_lot_express_fee' => Yii::t('admin', 'Реальная цена доставки лота по Китаю'),
      'actual_lot_price'       => Yii::t('admin', 'Реальная цена лота'),
      'calculated_operatorProfit'=> Yii::t('admin', 'Возможный бонус менеджера').', '.DSConfig::getVal('site_currency'),
      'uid'                 => Yii::t('main', 'ID клиента'),
      'manager'             => Yii::t('main', 'ID менеджера'),
      'ordr_user_email'     => Yii::t('main', 'Клиент'),
      'ordr_operator_email' => Yii::t('main', 'Оператор'),
      'textdate'            => Yii::t('main', 'Дата заказа'),
      'statusname'          => Yii::t('main', 'Статус'),
      'country'             => Yii::t('main', 'Страна'),
      'city'                => Yii::t('main', 'Город'),
      'phone'               => Yii::t('main', 'Телефон'),
      'address'             => Yii::t('main', 'Адрес'),
      'firstname'           => Yii::t('main', 'Имя'),
      'lastname'            => Yii::t('main', 'Фамилия'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search() {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('oid', $this->oid);
    $criteria->compare('iid', $this->iid, TRUE);
    $criteria->compare('pic_url', $this->pic_url, TRUE);
    $criteria->compare('props', $this->props, TRUE);
    $criteria->compare('title', $this->title, TRUE);
    $criteria->compare('seller_nick', $this->seller_nick, TRUE);
    $criteria->compare('seller_id', $this->seller_id);
    $criteria->compare('status', $this->status);
    $criteria->compare('num', $this->num);
    $criteria->compare('weight', $this->weight);
    $criteria->compare('express_fee', $this->express_fee);
    $criteria->compare('input_props', $this->input_props, TRUE);
    $criteria->compare('sku_id', $this->sku_id);
    $criteria->compare('taobao_price', $this->taobao_price);
    $criteria->compare('taobao_promotion_price', $this->taobao_promotion_price);
    $criteria->compare('tid', $this->tid, TRUE);
    $criteria->compare('track_code', $this->track_code, TRUE);
    $criteria->compare('actual_num', $this->actual_num);
    $criteria->compare('actual_lot_weight', $this->actual_lot_weight);
    $criteria->compare('actual_lot_express_fee', $this->actual_lot_express_fee);
    $criteria->compare('actual_lot_price', $this->actual_lot_price);

    return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return OrdersItems the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model(preg_replace('/^custom/','',$className));
  }

  public function save($runValidation = TRUE, $attributes = NULL) {
    if (!$this->isNewRecord) {
      if ($this->actual_lot_weight) {
        $num = ($this->actual_num) ? $this->actual_num : $this->num;
        Weights::saveItemWeight(0, $this->iid, round($this->actual_lot_weight / $num), 1);
      }
    }
      Yii::app()->db->autoCommit=false;
      $transaction = Yii::app()->db->beginTransaction();
    try {
      $oldRec = self::model()->findByPk($this->primaryKey);
      if ($oldRec) {
        $result = parent::save();
      }
      else {
//      if (Yii::app()->user->id != $this->uid) {
        $result = parent::save();
      }
    } catch (Exception $e) {
      $transaction->rollback();
        Yii::app()->db->autoCommit=true;
      return FALSE;
    }
    if ($result) {
      $transaction->commit();
        Yii::app()->db->autoCommit=true;
      Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
      return TRUE;
    }
    else {
      $transaction->rollback();
        Yii::app()->db->autoCommit=true;
      Yii::app()->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();
      return FALSE;
    }
  }

    public static function getAdminOrdersItemsList($status, $uid = false, $manager = false, $pageSize = 25)
    {
        if ($uid === false) {
            $_uid = null;
        } else {
            $_uid = $uid;
        }
        if ($manager === false) {
            $_manager = null;
        } else {
            $_manager = $manager;
        }
        if ($status != null) {
            $orderItemStatus = OrdersItemsStatuses::model()->find('id=:value', array(':value' => $status));
        } else {
            $orderItemStatus = new stdClass();
            $orderItemStatus->name = Yii::t('admin', 'Все');
            $orderItemStatus->id = null;
        }

        $criteria = new CDbCriteria;
        $criteria->select = "t.*, FROM_UNIXTIME(oo.date,'%d.%m.%Y %h:%i') AS textdate,
ss.name as statusname,
uu.email as ordr_user_email, mm.email as ordr_operator_email,
uu.uid as uid, mm.uid as mid,
aa.country, aa.city,aa.phone,aa.address,aa.firstname,aa.lastname";
        $criteria->condition = '(oo.uid=:uid or :uid is null) and (oo.manager=:manager or :manager is null) and t.id IN (' . OrdersItemsStatuses::getInQueryForOrderItemStatus(
            $orderItemStatus->id
          ) . ')';
        $criteria->join = "LEFT JOIN orders oo ON oo.id=t.oid
                           LEFT JOIN users uu ON uu.uid=oo.uid
                           LEFT JOIN users mm ON mm.uid=oo.manager
                       LEFT JOIN addresses aa ON aa.id = oo.addresses_id
                       LEFT JOIN orders_items_statuses ss ON ss.id = t.status";
        $criteria->params = array(':uid' => $_uid, ':manager' => $_manager);
        if (isset($_GET['OrdersItems'])) {
            $criteria->compare('t.id', $_GET['OrdersItems']['id']);
            $criteria->compare("FROM_UNIXTIME(oo.date,'%d.%m.%Y %h:%i')", $_GET['OrdersItems']['textdate'], true);
            $criteria->compare('t.status', $_GET['OrdersItems']['status']);
            $criteria->compare('uu.email', $_GET['OrdersItems']['ordr_user_email'], true);
            if (isset($_GET['OrdersItems']['ordr_operator_email'])) {
                $criteria->compare('mm.email', $_GET['OrdersItems']['ordr_operator_email'], true);
            }
            if (isset($_GET['OrdersItems']['address'])) {
                $criteria->compare(
                  "concat(aa.country,' ',aa.city,' ',aa.firstname,' ',aa.lastname,' ',aa.phone,' ',aa.address)",
                  $_GET['OrdersItems']['address'],
                  true
                );
            }
        }
        $model = OrdersItems::model();
        $ordersItems = new CActiveDataProvider($model, array(
          'criteria'   => $criteria,
          'sort'       => array(
            'defaultOrder' => 'oo.`date` DESC',
          ),
          'pagination' => array(
            'pageSize' => $pageSize,
          ),
        ));
        if (isset($_GET['OrdersItems'])) {
            if (is_array($_GET['OrdersItems'])) {
                foreach ($_GET['OrdersItems'] as $orderItemAttrName => $orderItemAttr) {
                    $ordersItems->model->{$orderItemAttrName} = $orderItemAttr;
                }
            }
        }
        return $ordersItems;
    }

    public static function getOrderItemPreview($orderItemId, $format)
    {
        $orderItem = OrdersItems::model()->findByPk($orderItemId);
            $imgPath = Img::getImagePath($orderItem->pic_url, $format, false);
            $res = Yii::app()->controller->renderPartial(
              'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.orders.preview',
              array('imgPath' => $imgPath),
              true,false,true
            );
            $res = '<div class="orderPreview">'. '<div class="orderPreviewVisible"> <a href="'.Yii::app()->createUrl('/item/index',array('iid'=>$orderItem->iid)).'" target="_blank">' . $res . '</a></div></div>';
        return $res;
    }

    public static function getOrderItem($id, $uid, $manager)
    {
        $orderItem = self::model()->findByPk($id);
        $order = Order::model()
          ->find(
            'id=:id and ((uid=:uid or :uid is null) and (manager=:manager or :manager is null))',
            array(
              ':id'      => $orderItem->oid,
              ':uid'     => $uid,
              ':manager' => $manager
            )
          );
        if ($order) {
            $order->managers = Users::model()->findByPk($order->manager);
            $order->users = Users::model()->findByPk($order->uid);
            $order->addresses = Addresses::model()->findByPk($order->addresses_id);
            $order->statuses = OrdersStatuses::model()->find('value=:value', array(':value' => $order->status));
            $order->extstatusname = Order::getOrderExtStatus($order->id);
            $order->allowedStatuses = OrdersStatuses::getAllowedStatusesForOrder($id, $uid, $manager);
            $order->allowedManagers = Users::getAllowedManagersForOrder($id, $uid, $manager);
            $order->ordersItems = OrdersItems::model()->findAllBySql(
              'select t.*,
                    (select os.name from orders_items_statuses os where os.id =t.status) as status_text from orders_items t where oid=:oid
                    and id=:id
                    order by t.seller_id',
              array(':oid' => $order->id,
                     ':id' => $orderItem->id)
            );
            $order->ordersItemsLegend = Order::getOrderItemsLegend($order->id);
            $order->ordersItemsFromOneSeller = Order::getItemsFromOneSeller($order->id);

            $order->orderCode = $order->uid . '-' . $order->id;
            $order->textdate = date('d.m.Y H:i', $order->date);
            $order->orderAge = round((time() - $order->date) / (24 * 3600));
            $order->userName = $order->users['firstname'] . ' ' . $order->users['lastname'] . '  ' . $order->users['email'] . ' ' . Yii::t(
                'admin',
                'Телефон'
              ) . ': ' . $order->users['phone'];
            $order->managerBalance = Users::getBalance($order->manager);
            $order->userBalance = Users::getBalance($order->uid);
            $order->fullAddress = $order->addresses['firstname'] . ' ' . $order->addresses['lastname'] . ', ' .
              Deliveries::getCountryName($order->addresses['country']) . ', ' . $order->addresses['index'] . ', ' .
              $order->addresses['city'] . ', ' . $order->addresses['address'] . ' ' .
              Yii::t('admin', 'Телефон') . ': ' . $order->addresses['phone'];
            $deliveries = Deliveries::getDelivery($order->weight, $order->addresses['country'], false);
            $resDeliveries = array();
            if (is_array($deliveries)) {
                foreach ($deliveries as $delivery) {
                    $resDeliveries[$delivery->id] = $delivery->id;
                }
            }
            $order->allowedDeliveries = $resDeliveries;
            $order->calculated = new OrderCalculated($order);
        }
        return $order;
    }

    public static function getAdminSearchSnippet($id, $query)
    {
        if (!function_exists('markup')) {
            function markup($val, $query)
            {
                $result=@preg_replace('/'.$query.'/i','<strong>'.$query.'</strong>',$val);
                if (isset($result)&&$result) {
                    return $result;
                } else {
                    return $val;
                }
            }
        }
        $order=self::getOrderItem($id,null,null);
        if (isset($order->ordersItems[0])) {
        $orderItem=$order->ordersItems[0];
        } else {
            $orderItem=false;
        }
        $res = '';
        $preview='';
        $orderFields = array(
          'id',
          'uid',
          'code',
          'delivery_id',
          'ordr_user_email',
          'ordr_operator_email',
          'goods_count',
          'items_count',
          'items_legend',
          'topay',
          'textdate',
          'statusname',
          'extstatusname',
          'country',
          'city',
          'phone',
          'address',
          'firstname',
          'lastname',
          'orderCode',
          'orderAge',
          'fullAddress',
          'store_id',
          'userName',
//          'managerName',
        );
        $fields = array(
          'id',
          'iid',
          'pic_url',
          'title',
          'seller_nick',
          'seller_id',
          'tid',
          'track_code',
          'status_text',
        );
        if ($order) {
            foreach ($orderFields as $field) {
                if (strlen($order->{$field}) > 0) {
                    $res = $res . '<small>' . $order->getAttributeLabel($field) . ':</small> ' . markup(
                        $order->{$field},
                        $query
                      ) . '&nbsp;';
                    if (in_array($field,array('textdate'))) {
                        $res = $res . '<br/>';
                    }
                }
            }
        }
        $res = $res . '<br/>';
        if ($orderItem) {
            $preview=self::getOrderItemPreview($orderItem->id,'_60x60.jpg');
            foreach ($fields as $field) {
                if (strlen($orderItem->{$field}) > 0) {
                    $res = $res . '<small>' . $orderItem->getAttributeLabel($field) . ':</small> ' . markup(
                        $orderItem->{$field},
                        $query
                      ) . '&nbsp;';
                    if (in_array($field,array())) {
                        $res = $res . '<br/>';
                    }
                }
            }
        }
        return '<div><div style="float:left;min-width:68px;height:65px;padding:2px;">'.$preview.'</div><div>'.$res.'</div></div>';
    }

    public static function getAdminLink($id, $external = false)
    {
        $orderItem = self::model()->findByPk($id);
        if ($orderItem) {
            if ($external) {
                return 'http://' . DSConfig::getVal(
                  'site_domain'
                ) . '/admin/main/open?url=admin/ordersItems/view/id/' . $id . '&tabName=' .Yii::t('admin','Лот '). $orderItem->oid.'-'.$orderItem->id;
            } else {
                return '<a href="' . Yii::app()->createUrl(
                  '/admin/ordersItems/view',
                  array('id' => $id)
                ) . '" title="' . Yii::t(
                  'admin',
                  'Просмотр лота'
                ) . '" onclick="getContent(this,\'' . Yii::t('admin','Лот '). $orderItem->oid.'-'.$orderItem->id . '\');return false;"><i class="icon-inbox"></i>&nbsp;' . Yii::t('admin','Лот '). $orderItem->oid.'-'.$orderItem->id . '</a>';
            }
        } else {
            return '<a href="#">' . Yii::t('admin', 'Ошибка') . '</a>';
        }
    }

    public static function getUserLink($id)
    {
        $order = self::model()->findByPk($id);
        if ($order) {
            return 'http://' . DSConfig::getVal(
              'site_domain'
            ) . '/cabinet/orders/view/id/' . $id;
        } else {
            return '<a href="#">' . Yii::t('main', 'Ошибка') . '</a>';
        }
    }
}