<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="Addresses.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "addresses".
 *
 * The followings are the available columns in table 'addresses':
 * @property integer $id
 * @property integer $uid
 * @property string $country
 * @property string $city
 * @property string $index
 * @property string $phone
 * @property string $address
 * @property string $firstname
 * @property string $patroname
 * @property string $lastname
 * @property string $region
 *
 * The followings are the available model relations:
 * @property Users $u
 * @property Orders[] $orders
 */
class customAddresses extends CActiveRecord
{
  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'addresses';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('uid, country, city, index, phone, address', 'required'),
      array('uid', 'numerical', 'integerOnly'=>true),
      array('country', 'length', 'max'=>3),
      array('city, patroname, lastname', 'length', 'max'=>128),
      array('index, phone, firstname', 'length', 'max'=>32),
      array('address', 'length', 'max'=>256),
      array('region', 'length', 'max'=>500),
      // The following rule is used by search().
      // @todo Please remove those attributes that should not be searched.
      array('id, uid, country, city, index, phone, address, firstname, patroname, lastname, region', 'safe', 'on'=>'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'u' => array(self::BELONGS_TO, 'Users', 'uid'),
      'orders' => array(self::HAS_MANY, 'Orders', 'addresses_id'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id' => 'ID',
      'uid' => 'Uid',
      'country' => Yii::t('main','Страна'),
      'city' => Yii::t('main','Город'),
      'index' => Yii::t('main','Индекс'),
      'phone' => Yii::t('main','Телефон'),
      'address' => Yii::t('main','Адрес'),
      'firstname' => Yii::t('main','Имя'),
      'patroname' => Yii::t('main','Отчество'),
      'lastname' => Yii::t('main','Фамилия'),
      'region' => Yii::t('main','Регион'),
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   *
   * Typical usecase:
   * - Initialize the model fields with values from filter form.
   * - Execute this method to get CActiveDataProvider instance which will filter
   * models according to data in model fields.
   * - Pass data provider to CGridView, CListView or any similar widget.
   *
   * @return CActiveDataProvider the data provider that can return the models
   * based on the search/filter conditions.
   */
  public function search($pageSize=20)
  {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria=new CDbCriteria;

    $criteria->compare('id',$this->id);
    $criteria->compare('uid',$this->uid);
    $criteria->compare('country',$this->country,true);
    $criteria->compare('city',$this->city,true);
    $criteria->compare('index',$this->index,true);
    $criteria->compare('phone',$this->phone,true);
    $criteria->compare('address',$this->address,true);
    $criteria->compare('firstname',$this->firstname,true);
    $criteria->compare('patroname',$this->patroname,true);
    $criteria->compare('lastname',$this->lastname,true);
    $criteria->compare('region',$this->region,true);

    return new CActiveDataProvider($this, array(
      'criteria'=>$criteria,
      'pagination' => array(
        'pageSize' => $pageSize,
      )
    ));
  }

  /**
   * Returns the static model of the specified AR class.
   * Please note that you should have this exact method in all your CActiveRecord descendants!
   * @param string $className active record class name.
   * @return Addresses the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model(preg_replace('/^custom/','',$className));
  }
  public function getAddressesByUser($uid)
  {
    $res = Yii::app()->db->createCommand()
      //->select('SUM(`sum`)')
      ->select('*')
      ->from('addresses')
      ->where('uid=:uid',array(':uid'=>$uid))
      ->queryAll();

    return $res;
  }
}


    


