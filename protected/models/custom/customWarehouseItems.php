<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="WarehouseItems.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "warehouse_items".
 *
 * The followings are the available columns in table 'warehouse_items':
 * @property integer $id
 * @property string $tid
 * @property string $date_in
 * @property string $date_out
 * @property integer $uid_in
 * @property integer $uid_out
 * @property string $store_id
 */
class customWarehouseItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'warehouse_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tid', 'required'),
			array('uid_in, uid_out', 'numerical', 'integerOnly'=>true),
			array('tid, store_id', 'length', 'max'=>4000),
			array('date_in, date_out', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tid, date_in, date_out, uid_in, uid_out, store_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('main','id лота на складе'),
			'tid' => Yii::t('main','Трек-код товара'),
			'date_in' => Yii::t('main','Дата прихода на склад'),
			'date_out' => Yii::t('main','Дата расхода со склада'),
			'uid_in' => Yii::t('main','ID менеджера, получившего товар на складе'),
			'uid_out' => Yii::t('main','ID менеджера, отправившего товар со склада'),
			'store_id' => Yii::t('main','ID или описание размещения на складе'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tid',$this->tid,true);
		$criteria->compare('date_in',$this->date_in,true);
		$criteria->compare('date_out',$this->date_out,true);
		$criteria->compare('uid_in',$this->uid_in);
		$criteria->compare('uid_out',$this->uid_out);
		$criteria->compare('store_id',$this->store_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WarehouseItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(preg_replace('/^custom/','',$className));
	}
}
