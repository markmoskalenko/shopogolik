<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ItemRelated.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customItemRelated {
  protected $_iid;
  protected $_lang = 'ru';

  public static function getSellerRelated($seller_nick, $seller_id, $iid) {
    Profiler::start('item->getSellerRelated');
    $searchRes = new StdClass();
    $searchRes->items = array();
    $searchRes->cids = array();
    $searchRes->bids = array();
    $searchRes->groups = array();
    $searchRes->filters = array();
    $searchRes->multiFilters = array();
    $searchRes->suggestions = array();
    $searchRes->priceRange = array();
    $searchRes->query = $seller_nick;
    $searchRes->cid = 0;
    $searchRes->search_type = 'searchuser';
    $searchRes->total_results = 0;
    if (isset($seller_nick) && isset($seller_id) && isset($iid)) {
      $cache = Yii::app()->fileCache->get('item-sellerRelated-' . $seller_nick);
      if (($cache == FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
        //Получаем товары продавца
          if (!empty($seller_nick)) {
            $search = new Search();
            $searchRes = $search->searchByDSGSearch(Utils::TransLang(), $seller_id, TRUE);
            if ($searchRes->total_results < 0) {
              Yii::app()->fileCache->set('item-sellerRelated-' . $seller_nick, array($searchRes), 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other'));
            }
          }
          else {
            $searchRes->total_results = 0;
          }
        }
      else {
        list($searchRes) = $cache;
      }
      Formulas::getUserPriceForArray($searchRes);
      Profiler::stop('item->getSellerRelated');
      return $searchRes;
    }
    else {
      Formulas::getUserPriceForArray($searchRes);
      Profiler::stop('item->getSellerRelated');
      return $searchRes;
    }
  }

  public static function getItemRelated($item) {
    Profiler::start('item->getItemRelated');
    $searchRes = new StdClass();
    $searchRes->items = array();
    $searchRes->cids = array();
    $searchRes->bids = array();
    $searchRes->groups = array();
    $searchRes->filters = array();
    $searchRes->multiFilters = array();
    $searchRes->suggestions = array();
    $searchRes->priceRange = array();
    $searchRes->query = '';
    $searchRes->cid = 0;
    $searchRes->search_type = 'searchitemrelated';
    $searchRes->total_results = 0;
    if (isset($item->taobao_item->apiRelateMarket)) {
      if (isset($item->taobao_item->apiRelateMarket->result)) {
        $relatedList=$item->taobao_item->apiRelateMarket->result;
        $fromTmall=false;
      } elseif (isset($item->taobao_item->apiRelateMarket->list)) {
        $relatedList=$item->taobao_item->apiRelateMarket->list;
        $fromTmall=true;
      } else {
        $fromTmall=false;
        $relatedList=false;
      }
       if ($relatedList) {
      if (is_array($relatedList)) {
        if (count($relatedList)>0) {
          $searchRes->total_results = count($relatedList);
          if (is_array($relatedList)) {
          foreach ($relatedList as $resitem) {
            $searchRes->items[] = new StdClass();
            if ($fromTmall) {
              end($searchRes->items)->num_iid = $resitem->id;
              if (isset($resitem->marketPrice)) {
              end($searchRes->items)->price = (float) $resitem->marketPrice;
              } else {
                end($searchRes->items)->price = 0;
              }
              if (isset($resitem->price)) {
              end($searchRes->items)->promotion_price = (float) $resitem->price;
              } else {
                end($searchRes->items)->promotion_price=0;
              }
              if (end($searchRes->items)->promotion_price==0) {
                end($searchRes->items)->promotion_price=end($searchRes->items)->price;
              }
              end($searchRes->items)->pic_url = $resitem->img;
              end($searchRes->items)->nick = '';
              end($searchRes->items)->post_fee = 0;
              end($searchRes->items)->express_fee = 0;
              end($searchRes->items)->ems_fee = 0;
              end($searchRes->items)->postage_id = 0;
              end($searchRes->items)->cid = 0;
              if (preg_match('/http:\/\/detail\.(tmall)\.com\/item\.htm/',$resitem->url)>0) {
                end($searchRes->items)->tmall=true;
              } else {
                end($searchRes->items)->tmall=false;
              }
            } else {
            end($searchRes->items)->num_iid = $resitem->itemId;
            end($searchRes->items)->price = (float) $resitem->price;
            end($searchRes->items)->promotion_price = (float) $resitem->promotionPrice;
            if (end($searchRes->items)->promotion_price==0) {
              end($searchRes->items)->promotion_price=end($searchRes->items)->price;
            }
            end($searchRes->items)->pic_url = $resitem->pic;
            end($searchRes->items)->nick = '';
            end($searchRes->items)->post_fee = 0;
            end($searchRes->items)->express_fee = 0;
            end($searchRes->items)->ems_fee = 0;
            end($searchRes->items)->postage_id = 0;
            end($searchRes->items)->cid = $resitem->categoryId;
            if (preg_match('/http:\/\/detail\.(tmall)\.com\/item\.htm/',$resitem->url)>0) {
              end($searchRes->items)->tmall=true;
            } else {
              end($searchRes->items)->tmall=false;
            }
            }
          }
        }
        }
      }
       }
    }
    Formulas::getUserPriceForArray($searchRes);
      Profiler::stop('item->getItemRelated');
      return $searchRes;
  }


}
