<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Cart.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * This is the model class for table "cart".
 *
 * The followings are the available columns in table 'cart':
 * @property integer $id
 * @property integer $uid
 * @property string $iid
 * @property integer $num
 * @property string $input_props
 * @property string $desc
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Users $u
 */
class customCart extends CActiveRecord
{

    public $sum=0;
    public $sumResUserPrice=0;
    public $sumDiscount=0;
    public $sum_no_discount=0;
    public $sum_no_discountResUserPrice=0;
    public $TaobaoPromotion_price=0;
    public $TaobaoPrice=0;
    public $price=0;
    public $price_no_discount=0;
    public $title='';
    public $seller_nick='';
    public $seller_id=0;
    public $express_fee=0;
    public $pic_url='';
    public $input_props_array = array();
    public $sku_id=0;
    public $top_item=false;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cart';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//            array('guest_uid, uid, input_props', 'required'),
          array('uid, num, cid', 'numerical', 'integerOnly' => true),
          array('iid', 'length', 'max' => 20),
          array('input_props', 'length', 'max' => 512),
          array('guest_uid, uid, input_props, desc, num, date, weight, cid, store, order', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array('id, guest_uid, uid, iid, num, input_props, desc, date, weight, cid, store, order', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
          'u' => array(self::BELONGS_TO, 'Users', 'uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id'          => 'ID',
          'uid'         => 'Uid',
          'iid'         => 'Iid',
          'num'         => 'Num',
          'input_props' => 'InputProps',
          'desc'        => 'Desc',
          'date'        => 'Date',
          'weight'      => 'Weight',
          'cid'         => 'Cid',
          'sku_id'      => 'SKU id',
          'store'       => 'Store',
          'order'       => 'Order',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('uid', $this->uid);
        $criteria->compare('iid', $this->iid, true);
        $criteria->compare('num', $this->num);
        $criteria->compare('weight', $this->num);
        $criteria->compare('cid', $this->num);
        $criteria->compare('input_props', $this->input_props, true);
        $criteria->compare('desc', $this->desc, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    public static function getCartCacheId($userId,$forOrder) {
        if (Yii::app()->user->isGuest) {
            if (isset(Yii::app()->request->cookies['cart'])) {
                $cart_cookie = Yii::app()->request->cookies['cart'];
            } else {
                $cart_cookie = null;
            }
            if (($cart_cookie != null) && (isset($cart_cookie->value))) {
                $cartGuestUid = $cart_cookie->value;
            } else {
                $cartGuestUid = uniqid("guest", true);
            }
        } else {
            // Если юзер не гость, но у него всё же есть куковая корзина
            if ($userId === false) {
                if (isset(Yii::app()->request->cookies['cart'])) {
                    $cart_cookie = Yii::app()->request->cookies['cart'];
                } else {
                    $cart_cookie = null;
                }
                if (($cart_cookie != null) && (isset($cart_cookie->value))) {
                    $cartGuestUid = $cart_cookie->value;
                }
                $uid = Yii::app()->user->id;
            } else {
                $uid = $userId;
            }
        }
        $result = 'cart-' . ((isset($cartGuestUid) && $cartGuestUid) ? $cartGuestUid : 'null') . '-'
          . ((isset($uid) && ($uid >= 0)) ? $uid : '-1').'-'.(($forOrder)?'true':'false');
        return $result;
    }

    public static function getUserCart($userId = false,$forOrder=false)
    {
        $userCart = new stdClass();
        $userCart->cartRecords = array();
        $userCart->cartRecordsDataProvider = false;
        $userCart->totalDiscount = 0;
        $userCart->total = 0;
        $userCart->totalNoDiscount = 0;
        $userCart->totalWeight = 0;
        $userCart->allowOrder = true;
        $userCart->summAddToAllowOrder = 0;
        $userCart->cacheId=false;
        $newUser=false;
        //string uniqid ([ string $prefix = "" [, bool $more_entropy = false ]] )
        if (Yii::app()->user->isGuest) {
            if (isset(Yii::app()->request->cookies['cart'])) {
                $cart_cookie = Yii::app()->request->cookies['cart'];
            } else {
                $cart_cookie = null;
            }
            if (($cart_cookie != null) && (isset($cart_cookie->value))) {
                $cartGuestUid = $cart_cookie->value;
            } else {
                $newUser=true;
                $cartGuestUid = uniqid("guest", true);
            }
//---- Продлеваем куки
            $cookie = new CHttpCookie('cart', $cartGuestUid);
            $cookie->expire = time() + 60 * 60 * 24 * 15;
            Yii::app()->request->cookies['cart'] = $cookie;
        } else {
            // Если юзер не гость, но у него всё же есть куковая корзина
            if ($userId === false) {
                if (isset(Yii::app()->request->cookies['cart'])) {
                    $cart_cookie = Yii::app()->request->cookies['cart'];
                } else {
                    $cart_cookie = null;
                }
                if (($cart_cookie != null) && (isset($cart_cookie->value))) {
                    $cartGuestUid = $cart_cookie->value;
                    if ((bool) $cartGuestUid) {
                        unset(Yii::app()->request->cookies['cart']);
                    }
                }
                $uid = Yii::app()->user->id;
            } else {
                $uid = $userId;
            }

        }
//---- CARD CACHING SET -------------------------------------------------
        if (!$newUser) {
            $cache = self::getCartCache($userId,$forOrder);
            if (($cache == false) || (DSConfig::getVal('search_cache_enabled') != 1) || true) {
                if (isset($cartGuestUid) && $cartGuestUid) {
                    $cartRecords = self::model()->findAll(
                      "guest_uid=:guest_uid and uid=0 and (:for_order =0 or `order`=1) order by `date` desc",
                      array(':guest_uid' => $cartGuestUid,
                      ':for_order'=>($forOrder?1:0),
                      )
                    );
                    if ($userId === false && isset($uid) && ($uid >= 0)) {
                        foreach ($cartRecords as $cartRecord) {
                            $cartRecord->uid = $uid;
                            $cartRecord->guest_uid = '';
                            try {
                                $cartRecord->update();
                            } catch (Exception $e) {
                                continue;
                            }
                        }
                    }
                } else {
                    $cartRecords = self::model()->findAll("uid=:uid and guest_uid='' and (:for_order =0 or `order`=1) order by `date` desc",
                      array(':uid' => $uid,
                      ':for_order'=>($forOrder?1:0),
                      )
                      );
                }
                self::setCartCache(false, $forOrder, $cartRecords, 3600);
            } else {
                $cartRecords = $cache;
            }
            $userCart->cartRecords = $cartRecords;
        } else {
            $userCart->cartRecords=array();
        }
//============ Cart calculations ========================================

        foreach ($userCart->cartRecords as $i => $cart_item) {
            $item = new Item($cart_item['iid'], true);
            if (!isset($item) || is_null($item) || !$item->top_item || FALSE) {
//          $cart_item->delete();
                $userCart->cartRecords[$i]->num=0;
                $userCart->cartRecords[$i]->weight=0;
                $userCart->cartRecords[$i]->title = Yii::t('main','Этого товара временно нет в наличии');
                continue;
            } else {
                $userCart->cartRecords[$i]->top_item=$item->top_item;
// Если лот пришел с неполными данными из витрины
                $changed=false;
                if (!$userCart->cartRecords[$i]->cid) {
                    $userCart->cartRecords[$i]->cid=$item->top_item->cid;
                    $changed=true;
                }
                if ($userCart->cartRecords[$i]->weight<=0) {
                    $userCart->cartRecords[$i]->weight=Weights::getItemWeight($userCart->cartRecords[$i]->cid, $userCart->cartRecords[$i]->iid);
                    $changed=true;
                }
                if ($changed) {
                    $userCart->cartRecords[$i]->update();
                }
            }
            $itemRes = Item::getSKU($cart_item['iid'], $cart_item['input_props']);
            if (!$itemRes) {
                continue;
            }
            if (isset($itemRes->sku)) {
                $cart_item->sku_id = $itemRes->sku->sku_id;
                $resUserPrice =
                  Formulas::getUserPrice(
                    array(
                      'price'       => $itemRes->sku->promotion_price,
                      'count'       => $cart_item['num'],
                      'deliveryFee' => $itemRes->top_item->express_fee,
                      'postageId'   => $item->taobao_item->postage_id,
                      'sellerNick'  => $item->taobao_item->nick,
                    )
                  );

                $cart_item->sum = $resUserPrice->price;
                $cart_item->sumDiscount = $resUserPrice->discount;
                $cart_item->sumResUserPrice = $resUserPrice;
                $resUserPrice =
                  Formulas::getUserPrice(
                    array(
                      'price'       => $itemRes->sku->price,
                      'count'       => $cart_item['num'],
                      'deliveryFee' => $itemRes->top_item->express_fee,
                      'postageId'   => $item->taobao_item->postage_id,
                      'sellerNick'  => $item->taobao_item->nick,
                    )
                  );
                $cart_item->sum_no_discount = $resUserPrice->price;
                $cart_item->sum_no_discountResUserPrice = $resUserPrice;
                $cart_item->TaobaoPromotion_price = $itemRes->sku->promotion_price;
                $cart_item->TaobaoPrice = $itemRes->sku->price;
            } else {
                $cart_item->sku_id = 0;
                $resUserPrice = Formulas::getUserPrice(
                  array(
                    'price'       => $itemRes->top_item->promotion_price,
                    'count'       => $cart_item['num'],
                    'deliveryFee' => $itemRes->top_item->express_fee,
                    'postageId'   => $item->taobao_item->postage_id,
                    'sellerNick'  => $item->taobao_item->nick,
                  )
                );
                $cart_item->sum = $resUserPrice->price;
                $cart_item->sumDiscount = $resUserPrice->discount;
                $cart_item->sumResUserPrice = $resUserPrice;
                $resUserPrice = Formulas::getUserPrice(
                  array(
                    'price'       => $itemRes->top_item->price,
                    'count'       => $cart_item['num'],
                    'deliveryFee' => $itemRes->top_item->express_fee,
                    'postageId'   => $item->taobao_item->postage_id,
                    'sellerNick'  => $item->taobao_item->nick,
                  )
                );
                $cart_item->sum_no_discount = $resUserPrice->price;
                $cart_item->sum_no_discountResUserPrice = $resUserPrice;
                $cart_item->TaobaoPromotion_price = $itemRes->top_item->promotion_price;
                $cart_item->TaobaoPrice = $itemRes->top_item->promotion_price;
            }
            if ($cart_item['num'] != 0) {
                $cart_item->price = Formulas::cRound($cart_item->sum / $cart_item['num']);
            } else {
                $cart_item->price = 0;
            }
            if ($cart_item['num'] != 0) {
                $cart_item->price_no_discount = Formulas::cRound($cart_item->sum_no_discount / $cart_item['num']);
            } else {
                $cart_item->price_no_discount = 0;
            }
            if (!isset($userCart->totalDiscount)) $userCart->totalDiscount=0;
            if ($cart_item['order']) {
                $userCart->totalDiscount += $cart_item->sumDiscount + ($cart_item->sum_no_discount - $cart_item->sum);
            }
            if (!isset($userCart->total)) $userCart->total=0;
            if ($cart_item['order']) {
                $userCart->total += $cart_item->sum;
            }
            if (!isset($userCart->totalNoDiscount)) $userCart->totalNoDiscount=0;
            if ($cart_item['order']) {
                $userCart->totalNoDiscount += $cart_item->sum_no_discount;
            }
            if (!isset($userCart->priceUserFinal)) $userCart->priceUserFinal=0;
            if ($cart_item['order']) {
                $userCart->priceUserFinal += $cart_item->sum;
            }
            if (!isset($userCart->totalWeight)) $userCart->totalWeight=0;
            if ($cart_item['order']) {
                $userCart->totalWeight += round($cart_item->weight * $cart_item->num);
            }
            /*
                  if (isset($cart_item['weight'])) {
                    $cart->weight = $cart_item['weight'];
                  }
            */
//      $cart->sum = Formulas::priceWrapper($cart->sum);
//      $cart->price = Formulas::priceWrapper($cart->price);
            $cart_item->title = $item->top_item->title;
            $cart_item->seller_nick = $item->top_item->nick;
            $cart_item->seller_id = $item->top_item->seller_id;
            $cart_item->express_fee = $item->top_item->express_fee;

            if (isset($itemRes->prop_img)) {
                $cart_item->pic_url = $itemRes->prop_img->url;
            } else {
                $cart_item->pic_url = $item->top_item->pic_url;
            }
//--------------------
            if (isset($cart_item['input_props'])) {
                $pids = array();
                $vids = array();
                $inputPropsArray = explode(';', $cart_item['input_props']);
                if (count($inputPropsArray)) {
                    foreach ($inputPropsArray as $v) {
                        if ($v != '') {
                            list($pids[], $vids[]) = explode(':', str_replace('%3A', ':', $v));
                        }
                    }
                }
                foreach ($item->top_item->input_props as $pid => $prop) {
                    if (in_array($pid, $pids)) {
                        foreach ($prop->childs as $child) {
                            if (in_array($child->vid, $vids)) {
                                $cart_item->input_props_array[] = new stdClass();
                                end($cart_item->input_props_array)->name = $prop->name;
                                end($cart_item->input_props_array)->value = $child->name;
                                if (isset($prop->name_zh)) {
                                    end($cart_item->input_props_array)->name_zh = $prop->name_zh;
                                }
                                if (isset($child->name_zh)) {
                                    end($cart_item->input_props_array)->value_zh = $child->name_zh;
                                }
                            }
                        }
                    }
                }
            }
//---------------------
        }

        $neededSum = DSConfig::getVal('checkout_min_order_sum');
        if ($neededSum > 0 && isset($userCart->priceUserFinal)) {
            $neededSumInCurr = Formulas::convertCurrency(
              $neededSum,
              DSConfig::getVal('site_currency'),
              DSConfig::getCurrency()
            );
            $userCart->summAddToAllowOrder = $neededSumInCurr - $userCart->priceUserFinal;
            if ($userCart->summAddToAllowOrder > 0) {
                $userCart->allowOrder = false;
            }
        } else {
            $userCart->summAddToAllowOrder = 0;
            $userCart->allowOrder = true;
        }

//============ End of Cart calculations ===================================
        $userCart->cartRecordsDataProvider = new CArrayDataProvider($userCart->cartRecords, array(
          'id'         => 'userCart',
            /*      'sort'=>array(
                    'attributes'=>array(
                      'id', 'username', 'email',
                    ),
                  ),
            */
          'pagination' => array(
            'pageSize' => 1000,
          ),
        ));
// $dataProvider->getData() will return a list of arrays.
        return $userCart;
    }

    public static function addItemToCart($cid, $iid, $num, $input_props, $descr)
    {
        Yii::app()->db->autoCommit=false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $_cid = $cid;
            $_iid = $iid;
            $_num = $num;
            if (isset($input_props) && (is_array($input_props))) {
                natsort($input_props);
                $_input_props = implode(';', $input_props);
            } else {
                $_input_props = '';
            }
            $_descr = $descr;
            $weight = Weights::getItemWeight($cid, $iid);
            if (Yii::app()->user->isGuest) {
                if (isset(Yii::app()->request->cookies['cart'])) {
                    $cart_cookie = Yii::app()->request->cookies['cart'];
                } else {
                    $cart_cookie = null;
                }
                if (($cart_cookie != null) && (isset($cart_cookie->value))) {
                    $cartGuestUid = $cart_cookie->value;
                } else {
                    $cartGuestUid = uniqid("guest", true);
                }
//---- Продлеваем куки
                $cookie = new CHttpCookie('cart', $cartGuestUid);
                $cookie->expire = time() + 60 * 60 * 24 * 15;
                Yii::app()->request->cookies['cart'] = $cookie;
//----
//-----------
                $vars = array(
                  'id'           => 1,
                  ':guest_uid'   => $cartGuestUid,
                  ':cid'         => $_cid,
                  ':iid'         => $_iid,
                  ':input_props' => $_input_props,
                  ':num'         => $_num,
                  ':desc'        => $_descr,
                  ':weight'      => $weight
                );

                Yii::app()->db->createCommand(
                  "INSERT INTO cart (guest_uid,cid,iid,input_props,num,`desc`,`date`,weight)
                                          VALUES (:guest_uid,:cid,:iid,:input_props,:num,:desc,Now(),:weight)
                                          ON DUPLICATE KEY UPDATE
                                          num=num+:num"
                )
                  ->bindParam(':guest_uid', $cartGuestUid, PDO::PARAM_STR)
                  ->bindParam(':cid', $_cid, PDO::PARAM_INT)
                  ->bindParam(':iid', $_iid, PDO::PARAM_INT)
                  ->bindParam(':input_props', $_input_props, PDO::PARAM_STR)
                  ->bindParam(':num', $_num, PDO::PARAM_INT)
                  ->bindParam(':desc', $_descr, PDO::PARAM_STR)
                  ->bindParam(':weight', $weight, PDO::PARAM_STR)
                  ->execute();
//-----------
            } else {
                $uid = Yii::app()->user->id;
//-----------
                $vars = array(
                  'id'           => 2,
                  ':uid'         => $uid,
                  ':cid'         => $_cid,
                  ':iid'         => $_iid,
                  ':input_props' => $_input_props,
                  ':num'         => $_num,
                  ':desc'        => $_descr,
                  ':weight'      => $weight
                );
                Yii::app()->db->createCommand(
                  "INSERT INTO cart (uid,cid,iid,input_props,num,`desc`,`date`,weight)
                                          VALUES (:uid,:cid,:iid,:input_props,:num,:desc,Now(),:weight)
                                          ON DUPLICATE KEY UPDATE
                                          num=num+:num"
                )
                  ->bindParam(':uid', $uid, PDO::PARAM_INT)
                  ->bindParam(':cid', $_cid, PDO::PARAM_INT)
                  ->bindParam(':iid', $_iid, PDO::PARAM_INT)
                  ->bindParam(':input_props', $_input_props, PDO::PARAM_STR)
                  ->bindParam(':num', $_num, PDO::PARAM_INT)
                  ->bindParam(':desc', $_descr, PDO::PARAM_STR)
                  ->bindParam(':weight', $weight, PDO::PARAM_STR)
                  ->execute();
//-----------
            }
            $transaction->commit();
            Yii::app()->db->autoCommit=true;
            self::clearCartCache(false);
            return true;
        } catch (Exception $e) {
            $transaction->rollback();
            self::clearCartCache(false);
            Yii::app()->db->autoCommit=true;
            return false;
        }
    }

    public static function deleteUserCart($id = false,$deleteStored=false)
    {
        $userCart = self::getUserCart();
        if ($userCart->cartRecords) {
            Yii::app()->db->autoCommit=false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
            foreach ($userCart->cartRecords as $cartRecord) {
                if (($cartRecord->id == $id) || (($id == false) && ($cartRecord->store==0 || $deleteStored))) {
                    $cartRecord->delete();
                }
            }
                $transaction->commit();
                Yii::app()->db->autoCommit=true;
                unset(Yii::app()->request->cookies['cart']);
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->db->autoCommit=true;
                }

        }
        self::clearCartCache(false);
    }

    public static function cartIsEmpty($userId = false,$forOrder=false)
    {
        $userCart=self::getUserCart($userId,$forOrder);
        return count($userCart->cartRecords)<=0;
    }
    public static function clearCartCache($userId) {
        $cacheId=self::getCartCacheId($userId,true);
        if ($cacheId) {
            if (Yii::app()->cache->get($cacheId)!=false) {
                Yii::app()->cache->delete($cacheId);
            }
        }
        $cacheId=self::getCartCacheId($userId,false);
        if ($cacheId) {
            if (Yii::app()->cache->get($cacheId)!=false) {
                Yii::app()->cache->delete($cacheId);
            }
        }

        /*        else {
                    Yii::app()->cache->flush();
                }
        */
    }
    public static function getCartCache($userId, $forOrder) {
        $cacheId=self::getCartCacheId($userId,$forOrder);
        if ($cacheId) {
            $result=Yii::app()->cache->get($cacheId);
            return $result;
        } else {
            return false;
        }
    }

    public static function setCartCache($userId,$forOrder,$object,$ttl) {
        $cacheId=self::getCartCacheId($userId,$forOrder);
        if ($cacheId) {
            if (Yii::app()->cache->get($cacheId)!=false) {
                Yii::app()->cache->delete($cacheId);
            }
            Yii::app()->cache->set($cacheId, $object, $ttl);
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cart the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model(preg_replace('/^custom/', '', $className));
    }

    public function save($runValidation = true, $attributes = null)
    {
        if (!$this->isNewRecord) {
            Weights::saveItemWeight($this->cid, $this->iid, $this->weight);
        }
        $result=parent::save($runValidation,$attributes);
        return $result;
    }

    public function update($attributes = null)
    {
       if (!$this->isNewRecord) {
            Weights::saveItemWeight($this->cid, $this->iid, $this->weight);
        }
        $result=parent::update($attributes);
        return $result;
    }

}