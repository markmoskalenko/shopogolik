<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Search.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/*
 * Модель для общего поиска товаров. Уникальна и используется для поиска, категорий, продавцов
 */

class customSearch
{
    public static $version = 'ver.: <b>2.1</b>';
    public static $intversion = '2.1.26.11.2014';
    public $page = 1;
    public $breadcrumbs = array();
    public $cid_model = false;
    public $cid = false;
    public $params = array();
    public $search_ItemsPerPage = 100; //search_ItemsPerPageDefault parameter
    public $search_query = '';

    public function execute($type = 'global', $lang = false, $fromAPI = false)
    {
        Profiler::start('search->execute', true);
        //$curency = DSConfig::getCurrency();
        if (!$lang) {
            $lang = Utils::TransLang();
        }
        $this->params['page'] = $this->page;
        //Подготавливаем параметры
//        if (!isset($this->params['price_min']) || $this->params['price_min'] < 0.1) {
//            $this->params['price_min'] = 0.1;
//        }
        if (isset($this->params['price_min']) && (bool) ($this->params['price_min'])) {
            $this->params['price_min'] = Formulas::getTaobaoPrice(array('price' => $this->params['price_min']));
        }
        if (isset($this->params['price_max']) && (bool) ($this->params['price_max'])) {
            $this->params['price_max'] = Formulas::getTaobaoPrice(array('price' => $this->params['price_max']));
        }
        ksort($this->params);
        if ($type == 'global') {
//===================================================================================================
            if (!empty($this->params['nick'])) {
                $searchRes = $this->searchByUser($lang);
            } else {
                $searchRes = $this->searchByDSGSearch($lang, false, $fromAPI);
            }
//====================================================================================================
        } elseif ($type == 'favorite') {
            $searchRes = $this->searchByFavorite($lang);
        }
        Profiler::start('search->prepareItemTitle');
        $seo_catalog_enabled = DSConfig::getVal('seo_catalog_enabled') == 1;
        if (!$fromAPI) {
            foreach ($searchRes->items as $i => $searchResItem) {
                $this->prepareItemTitle($searchRes->items[$i], $lang, $seo_catalog_enabled);
            }
        }
        Profiler::stop('search->prepareItemTitle');
        if (!$fromAPI) {
            Formulas::getUserPriceForArray($searchRes);
        }
//====================================================================================================
        if (!function_exists('rand_compare')) {
            function rand_compare($a, $b)
            {
                return mt_rand(-1, 1);
            }
        }
        if (!function_exists('r_compare')) {
            function r_compare($a, $b)
            {
                $a_val = $a->promotion_price + $a->express_fee;
                $b_val = $b->promotion_price + $b->express_fee;

                if ($a_val < $b_val) {
                    return -1;
                } elseif ($a_val == $b_val) {
                    return 0;
                } else {
                    return 1;
                }
            }
        }
        if ((!empty($this->params['recommend'])) && ($this->params['recommend'] == 1)) {
            uasort($searchRes->items, "rand_compare");
        } else {
            // uasort($searchRes->items, "r_compare");
        }

        Profiler::stop('search->execute', true);
        SiteLog::doItemsLog($searchRes);
        SiteLog::doQueryLog($searchRes);
        if (isset($this->zh_query)) {
            $searchRes->zh_query = $this->zh_query;
        }
        return $searchRes;
    }

    public function searchByFavorite($lang)
    {
        $query = '';
        if (isset($this->params['query'])) {
            $query = $this->prepareSearchQuery($this->params['query'], true);
        }
        $req = new stdClass();
        if ($query == false) {
            $req->q = $query;
        }
        if (!empty($this->params['price_min'])) {
            $req->startPrice = $this->params['price_min'];
        } else {
            $req->startPrice = 0;
        }
        if (!empty($this->params['price_max']) && ($this->params['price_max'] != false)) {
            $req->endPrice = $this->params['price_max'];
        } else {
            $req->endPrice = 999999;
        }
        //set category id
        if (isset($this->params['cid'])) {
            if ($this->params['cid'] > 0) {
                $req->cat = $this->params['cid'];
            } else {
                $req->cat = 0;
            }
        }
        //set page size
        $req->pageSize = Search::getPageSize(); //12
        //set page number
        $in_page_no = $this->page;
        $req->pageNo = $in_page_no;
        $req->pageOffset = $req->pageSize * ($req->pageNo - 1);
        $req->uid = Yii::app()->user->id;
        if (!empty($this->params['sort_by'])) {
            $req->sort = $this->params['sort_by'];
        }
//======================================================================================
        $countTotal = Yii::app()->db->createCommand(
          "select count(0) as cnt from favorites ff
where
uid=:uid
and (ff.cid=:cid or :cid is null or :cid=0 or ff.cid in (
(select ce2.cid from categories_ext ce2 where ce2.parent in (select ce1.id from categories_ext ce1 where ce1.cid=:cid)
)))
and (least(price,promotion_price) between :price_min and :price_max)"
        )
          ->queryScalar(
            array(
              ':uid'       => $req->uid,
              ':cid'       => $req->cat,
              ':price_min' => $req->startPrice,
              ':price_max' => $req->endPrice
            )
          );

        $searchRecords = Yii::app()->db->createCommand(
          "select ff.num_iid,ff.cid,ff.express_fee,ff.price,ff.promotion_price,ff.pic_url,ff.seller_rate
  from favorites ff
where
uid=:uid
and (ff.cid=:cid or :cid is null or :cid=0 or ff.cid in (
(select ce2.cid from categories_ext ce2 where ce2.parent in (select ce1.id from categories_ext ce1 where ce1.cid=:cid)
)))
and (least(ff.price,ff.promotion_price) between :price_min and :price_max)
LIMIT :offset,:pagesize"
        )
          ->bindParam(':uid', $req->uid, PDO::PARAM_STR)
          ->bindParam(':cid', $req->cat, PDO::PARAM_STR)
          ->bindParam(':price_min', $req->startPrice, PDO::PARAM_STR)
          ->bindParam(':price_max', $req->endPrice, PDO::PARAM_STR)
          ->bindParam(':pagesize', $req->pageSize, PDO::PARAM_INT)
          ->bindParam(':offset', $req->pageOffset, PDO::PARAM_INT)
          ->queryAll();
//======================================================================================
        $searchRes = new StdClass();
        $searchRes->items = array();
        $searchRes->cids = array();
        $searchRes->bids = array();
        $searchRes->groups = array();
        $searchRes->filters = array();
        $searchRes->multiFilters = array();
        $searchRes->suggestions = array();
        $searchRes->priceRange = array();
        if (isset($this->params['query'])) {
            $searchRes->query = $this->params['query'];
        } else {
            $searchRes->query = '';
        }
        if (isset($this->params['cid'])) {
            $searchRes->cid = $this->params['cid'];
        } else {
            $searchRes->cid = 0;
        }
        $searchRes->search_type = 'searchDSG';
        if (($searchRecords) && ($countTotal > 0)) {
            $searchRes->total_results = $countTotal;
            if ($searchRes->total_results > 0) {
                foreach ($searchRecords as $item) {
                    $searchRes->items[] = new StdClass();
                    end($searchRes->items)->num_iid = $item['num_iid'];
                    end($searchRes->items)->price = $item['price'];
                    end($searchRes->items)->promotion_price = $item['promotion_price'];
                    end($searchRes->items)->pic_url = $item['pic_url'];
                    end($searchRes->items)->nick = '';
                    if (isset($item['seller_rate'])) {
                        end($searchRes->items)->seller_rate = $item['seller_rate'];
                    } else {
                        end($searchRes->items)->seller_rate = 0;
                    }
                    end($searchRes->items)->post_fee = $item['express_fee'];
                    end($searchRes->items)->express_fee = $item['express_fee'];
                    end($searchRes->items)->ems_fee = $item['express_fee'];
                    end($searchRes->items)->postage_id = 0;
                    end($searchRes->items)->cid = $item['cid'];
                    end($searchRes->items)->tmall = false;
                }
            }
        } else {
            $searchRes->total_results = 0;
        }
        return $searchRes;
    }

    public static function getFeatured($lang, $type = 'popular', $count = false,$asArray=false)
    { // array('popular','recommended','recentUser','recentAll')
        $dataProvider = false;
        try {
            if (!$count) {
                $featured_on_main_items_count = (int) DSConfig::getVal('featured_on_main_items_count');
            } else {
                $featured_on_main_items_count = $count;
            }
            $fromDateString = date("d.m.Y",time()-3600*24*14);
            if ($type == 'popular') {

                $countTotal = Yii::app()->db->createCommand(
                  "select count(0) as cnt from
(select count(0) as cnt -- , max(ii1.date) as last_date
from log_item_requests ii1
where ii1.date > STR_TO_DATE('" . $fromDateString . "','%d.%m.%Y') and
ii1.cid in (select cc.cid from categories cc where cc.cid>0) and
 ii1.uid!=-1
group by ii1.cid, ii1.num_iid
 having -- max(ii1.date) = last_date and
 count(0)>2) rr"
                )
                  ->queryScalar();
                $sql = "select count(0) as cnt, max(ii1.date) as last_date,
'' as cat_title_ru,'' as cat_title_en,'' as cat_title_zh, ii1.* from log_item_requests ii1
where ii1.date > STR_TO_DATE('" . $fromDateString . "','%d.%m.%Y') and
ii1.cid in (select cc.cid from categories cc where cc.cid>0) and
 ii1.uid!=-1
group by ii1.cid, ii1.num_iid
 having -- max(ii1.date) = last_date and
 count(0)>2
order by cnt desc, last_date desc";
            } elseif ($type == 'recentAll') {
                if (Yii::app()->user->inRole('guest')) {
                    $uid = -1;
                } else {
                    $uid = Yii::app()->user->id;
                }
                $countTotal = Yii::app()->db->createCommand(
                  "select count(0) as cnt from
(select count(0) as cnt -- , max(ii1.date) as last_date
from log_item_requests ii1
where ii1.date > STR_TO_DATE('" . $fromDateString . "','%d.%m.%Y') and
ii1.cid in (select cc.cid from categories cc where cc.cid>0) and (ii1.uid!=:uid) and (ii1.uid!=-1)
group by ii1.cid, ii1.num_iid
-- having max(ii1.date) = last_date
 ) gg"
                )
                  ->bindParam(':uid', $uid, PDO::PARAM_INT)
                  ->queryScalar();

                $sql = "select count(0) as cnt, max(ii1.date) as last_date,
'' as cat_title_ru,'' as cat_title_en,'' as cat_title_zh, ii1.* from log_item_requests ii1
where ii1.date > STR_TO_DATE('" . $fromDateString . "','%d.%m.%Y') and
ii1.cid in (select cc.cid from categories cc where cc.cid>0) and (ii1.uid!=:uid) and (ii1.uid!=-1)
group by ii1.cid, ii1.num_iid
-- having max(ii1.date) = last_date
order by last_date desc";
            } elseif ($type == 'recentUser') {
                $uid = Yii::app()->user->id;
                $countTotal = Yii::app()->db->createCommand(
                  "select count(0) as cnt from
(select count(0) as cnt -- , max(ii1.date) as last_date
from log_item_requests ii1
where ii1.date > STR_TO_DATE('" . $fromDateString . "','%d.%m.%Y') and
ii1.cid in (select cc.cid from categories cc where cc.cid>0) and
 :uid!='-1' and ii1.uid=:uid
group by ii1.cid, ii1.num_iid
-- having max(ii1.date) = last_date
 ) gg"
                )
                  ->bindParam(':uid', $uid, PDO::PARAM_INT)
                  ->queryScalar();

                $sql = "select count(0) as cnt, max(ii1.date) as last_date,
'' as cat_title_ru,'' as cat_title_en,'' as cat_title_zh, ii1.* from log_item_requests ii1
where ii1.date > STR_TO_DATE('" . $fromDateString . "','%d.%m.%Y') and
ii1.cid in (select cc.cid from categories cc where cc.cid>0) and
 :uid!=-1 and ii1.uid=:uid
group by ii1.cid, ii1.num_iid
-- having max(ii1.date) = last_date
order by last_date desc";

            } elseif ($type == 'recommended') {

                $countTotal = Yii::app()->db->createCommand("select count(0) as cnt from featured ii1")
//          ->bindParam(':uid', $uid, PDO::PARAM_INT)
                  ->queryScalar();

                $sql = "select ii1.num_iid as id, ii1.* from featured ii1 order by date desc";
            }
            if ($asArray) {
                $dataProvider=Yii::app()->db->createCommand($sql.' LIMIT '.$count)->queryAll();
            } else {
                $dataProvider = new CSqlDataProvider(
                  $sql, array(
                    'params' => ((in_array($type,array('recommended','popular')))?array():array(':uid' => $uid)),
                    'id'             => $type.'_dataProvider',
                    'keyField'       => 'id',
                    'totalItemCount' => $countTotal,
                    'pagination'     => array(
                      'pageSize' => $featured_on_main_items_count,
                    )
                  )
                );
            }
        } catch (Exception $e) {
            return false;
        }
        return $dataProvider;
    }

    protected function searchByUser($lang)
    {
        Profiler::start('search->searchByUser');
        $searchRes = new stdClass();
        $searchRes->search_type = 'SearchByUser';
        $searchRes->total_results = 0;
        $cache = Yii::app()->fileCache->get('user_DSG-' . implode('-', $this->params));
        if (($cache == false) || (DSConfig::getVal('search_cache_enabled') != 1)) {
            $searchRes = $this->searchByDSGSearch($lang, $this->params['nick']);
            if ($searchRes->total_results > 0) {
                Yii::app()->fileCache->set(
                  'user_DSG-' . implode('-', $this->params),
                  array($searchRes),
                  60 * 60 * (int) DSConfig::getVal('search_cache_ttl_search')
                );
            } else {
                $searchRes->total_results = 0;
            }
        } else {
            list($searchRes) = $cache;
        }
        Profiler::stop('search->searchByUser');
        return $searchRes;
    }

    public function prepareSearchQuery($query, $extOutput = false)
    {
        Profiler::start('search->prepareSearchQuery');
        $lang = Utils::TransLang();
        $this->search_query = $query;

        $query = trim(preg_replace(DSConfig::getVal('stoplist_search'), '', $query));
        if ((!(bool) $query) || (empty($query))) {
            $res = '';
//      return FALSE;
        }
        $query = Utils::translateFuzzyBrand($query);
        $useVirtualCatQueriesOnly = DSConfig::getVal('search_useVirtualCatQueriesOnly') == 1;
        $useVirtualCatQueriesForOldCatalog = DSConfig::getVal('search_useVirtualCatQueriesForOldCatalog') == 1;
        if (preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $query)) {
            $res = trim($query);
        } elseif ((!preg_match("/[а-я]{1}/ui", $query)) && ($lang == 'ru')) {
            $res = trim(strtoupper($query));
        } elseif ((!preg_match("/[a-z]{1}/ui", $query)) && ($lang == 'en')) {
            $res = trim(strtoupper($query));
        } elseif (preg_match('/\b' . str_replace('/','\/',$query) . '\b/i', DSConfig::getVal('stoplist_translator')) > 0) {
            $res = trim(strtoupper($query));
        } else {
            $zh_res = trim(
              Yii::app()->DanVitTranslator->translateQuery($query, $lang, 'zh-CN', !$extOutput)
            ); //TRUE, FALSE,
            $res = Utils::removeOnlineTranslation($zh_res);
            if (!$extOutput) {
                $zh_res = $res;
            }
        }
//============= Работаем с виртуальными категориями
        $isVirtual = (isset($this->params['virtual']) && ($this->params['virtual'] == 1));
        $haveCidQuery = (isset($this->params['cid_query']) && ($this->params['cid_query'] != ''));
        if ($isVirtual) {
                if ($haveCidQuery) {
                    $res = trim((trim($this->params['cid_query']) . ' ' . trim($res)));
                }
            }
//============= Работаем со свойствами
        $useContextualPropsSearch = DSConfig::getVal('search_useContextualPropsSearch') == 1;
        if ($useContextualPropsSearch) {
            if (!empty($this->params['props'])) {
                $pidsvids = explode(';', str_replace('%3B', ';', $this->params['props']));
                $pids = '';
                $vids = '';
                foreach ($pidsvids as $pidvid) {
                    if ($pidvid != '') {
                        $pidvidarray = explode(':', str_replace('%3A', ':', $pidvid));
                        $pids .= $pidvidarray[0] . ',';
                        $vids .= $pidvidarray[1] . ',';
                    }
                }
                $pids = substr($pids, 0, -1);
                $vids = substr($vids, 0, -1);
//======
                $command = Yii::app()->db->createCommand(
                  "
 select distinct vv.ZH from categories_props_vals vv
where vv.PID in (" . $pids . ") and vv.VID in (" . $vids . ") and vv.ZH is not null and vv.ZH<>''"
                );
                $rows = $command->queryAll();
                $props_zh = '';
                foreach ($rows as $row) {
                    $props_zh .= trim($row['ZH']) . ' ';
                }
                $props_zh = substr($props_zh, 0, -1);
                if ($props_zh != '') {
                    $res = trim($res) . ' ' . trim($props_zh);
                }
//======
            }
        }
//====================================
        Profiler::stop('search->prepareSearchQuery');
        if ($res == '') {
            return false;
        } else {
            if (isset($zh_res)) {
                $this->zh_query = $zh_res;
            }
            return trim($res);
        }
    }

    public static function getPageSize()
    {
        $pageSize = (int) DSConfig::getVal('search_ItemsPerPageDefault');
        return $pageSize;
    }

    public function prepareItemTitle($searchResItem, $lang, $seo_catalog_enabled)
    {
        if ((!$seo_catalog_enabled) || !isset($searchResItem->properties)) {
            $searchResItem->html_title = Yii::t(
              'main',
              'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
            );
            $searchResItem->popup_title = Yii::t(
              'main',
              'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
            );
            $searchResItem->html_alt = Yii::t(
              'main',
              'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
            );
        } else {
            $searchResItem->html_title = '';
            $searchResItem->popup_title = Yii::t(
              'main',
              'Подробно о товаре, выбор дополнительных свойств товара, оформление заказа...'
            );
            $searchResItem->html_alt = '';
            if (isset($searchResItem->properties)) {
                $pidvid = explode(';', str_replace('%3B', ';', $searchResItem->properties));
                $pid_vid = array();
                $pids = array();
                $vids = array();
                foreach ($pidvid as $i => $pv) {
                    $pid_vid[$i] = explode(':', str_replace('%3A', ':', $pidvid[$i]));
                    if (isset($pid_vid[$i][0]) && isset($pid_vid[$i][1])) {
                        $pids[] = $pid_vid[$i][0];
                        $vids[] = $pid_vid[$i][1];
                    } else {
                        return;
                    }
                }
                $pids_unique = array_unique($pids);
                $vids_unique = array_unique($vids);
                $command = Yii::app()->db->createCommand(
                  "
 select pp.pid, pp.zh as pid_zh, pp.en as pid_en, pp.ru as pid_ru, vv.vid, vv.zh as vid_zh, vv.en as vid_en, vv.ru as vid_ru, IS_SALE_PROP,IS_INPUT_PROP,IS_KEY_PROP,IS_COLOR_PROP,IS_ENUM_PROP,IS_ITEM_PROP,MULTI
 from categories_props pp
 left join categories_props_vals vv on pp.cid=vv.cid and pp.pid=vv.pid and vv.vid in (" . implode(',', $vids_unique) . ")
     where pp.cid=:cid and pp.HIDDEN=0 and pp.STATUS=1 and pp.pid in (" . implode(',', $pids_unique) . ")"
                )
                  ->bindParam(":cid", $searchResItem->cid, PDO::PARAM_INT);
                $rows = $command->queryAll();
                $res_tite = array();
                $res_alt = array();
                $res_brand = array();
                foreach ($rows as $row) {
                    if (($row['vid_' . $lang] != '') && (strlen($row['vid_' . $lang])) > 0) {
                        if (($row['IS_SALE_PROP'] == 0) && ($row['IS_KEY_PROP'] == 0)) {
                            $res_tite[] = $row['vid_' . $lang];
                        } elseif (($row['IS_KEY_PROP'] == 1) || ($row['pid'] == 20000)) {
                            $res_brand[] = $row['vid_' . $lang];
                        } elseif ($row['IS_SALE_PROP'] == 1) {
                            $res_alt[] = $row['vid_' . $lang];
                        }
                    }
                }
                if (count($res_brand) > 0) {
                    $searchResItem->html_title = implode(", ", $res_brand) . "\n" . implode(", ", $res_tite);
                } else {
                    $searchResItem->html_title = implode(", ", $res_tite);
                }
                $searchResItem->popup_title = implode(", ", $res_alt);
                $searchResItem->html_alt = $searchResItem->html_title;
            }
        }
    }

    public function searchByDSGSearch($lang, $nick = false, $light = false)
    {
        $search_DropShop_grabbers_debug = DSConfig::getVal('search_DropShop_grabbers_debug') == 1;
        $cacheParams = $this->params;
        $query = '';
        if (isset($this->params['query'])) {
            $query = $this->prepareSearchQuery($this->params['query'], true);
        }
        if ($nick != false) {
            if (preg_match('/^\d+$/', $nick)) {
                $query = trim($query . '&user_id=' . $nick);
            } else {
                $query = trim($nick . ' ' . $query);
            }
        }
        $cacheParams['query'] = $query;
        $cacheParams['light'] = $light;

        $cache = Yii::app()->fileCache->get('DSG_search-' . implode('-', $cacheParams));

        if (($cache == false) || (DSConfig::getVal(
              'search_cache_enabled'
            ) != 1) || ($search_DropShop_grabbers_debug == 1)
        ) //Alexys cache
        {
            $req = new DSGSearch($search_DropShop_grabbers_debug);
            $req->lang = $lang;
            if ($query != false) {
                $req->q = $query;
            }
            if (isset($this->params['original'])) {
                if ($this->params['original'] == 'on') {
                    $req->tianmao = true;
                }
            }
            if (isset($this->params['not_unique'])) {
                if ($this->params['not_unique'] == 'on') {
                    $req->not_unique = true;
                }
            }
            if ((!empty($this->params['recommend'])) && ($this->params['recommend'] == 1)) {
                $req->recommend = true;
            }

            //set category id
            if (isset($this->params['cid'])) {
                if ($this->params['cid'] > 0) {
                    $req->cat = $this->params['cid'];
                }
            }

//================================ Price safe mode ==========================================
            if (!empty($this->params['price_min'])) {
                $req->startPrice = $this->params['price_min'];
            } else {
                $req->startPrice = 0.1;
            }
            if (!empty($this->params['price_max'])) {
                $req->endPrice = $this->params['price_max'];
            } else {
                $req->endPrice = 999999;
            }
            if (DSConfig::getVal('search_use_safe_price_ranges')==1) {
                list($minPrice,$maxPrice)=CategoriesPrices::getPricesRangeForSearch($req->cat,$req->q);
                $req->startPrice = max($req->startPrice,$minPrice);
                $req->endPrice = min($req->endPrice,$maxPrice);
            }
//============================================================================================

            if (isset($this->params['props'])) {
                if (!empty($this->params['props'])) {
                    $req->props = $this->params['props'];
                }
            }
            //set page size
            $req->pageSize = Search::getPageSize(); //12
            //set page number
            $in_page_no = $this->page;
            /*      if ($in_page_no == 0) {
                    $in_page_no = 1;
                  }
                  $in_page_no = fmod($in_page_no, 10);
                  if ($in_page_no == 0) {
                    $in_page_no = 10;
                  }
            */
            $req->pageNo = $in_page_no;

//      $req->fields = 'num_iid,pic_url,nick,price,promotion_price,post_fee';

            if (!empty($this->params['sort_by'])) {
                $req->sort = $this->params['sort_by'];
            }
            $DSGSearchRes = $req->execute();
            unset($req);
            $searchRes = new StdClass();
            $searchRes->items = array();
            $searchRes->cids = array();
            $searchRes->bids = array();
            $searchRes->groups = array();
            $searchRes->filters = array();
            $searchRes->multiFilters = array();
            $searchRes->suggestions = array();
            $searchRes->priceRange = array();
            if (isset($this->params['query'])) {
                $searchRes->query = $this->params['query'];
            } else {
                $searchRes->query = '';
            }
            if (isset($this->params['cid'])) {
                $searchRes->cid = $this->params['cid'];
            } else {
                $searchRes->cid = 0;
            }
            $searchRes->search_type = 'searchDSG';
            if ((isset($DSGSearchRes->items)) && (isset($DSGSearchRes->total_results))) {
                $searchRes->total_results = $DSGSearchRes->total_results;
                if ($searchRes->total_results > 0) {
                    foreach ($DSGSearchRes->items as $item) {
                        $searchRes->items[] = new StdClass();
                        end($searchRes->items)->num_iid = $item->num_iid;
                        end($searchRes->items)->price = $item->price;
                        end($searchRes->items)->promotion_price = $item->promotion_price;
                        end($searchRes->items)->pic_url = $item->pic_url;
                        end($searchRes->items)->nick = $item->nick;
                        if (isset($item->seller_rate)) {
                            end($searchRes->items)->seller_rate = $item->seller_rate;
                        } else {
                            end($searchRes->items)->seller_rate = 0;
                        }
                        end($searchRes->items)->post_fee = $item->post_fee;
                        end($searchRes->items)->express_fee = $item->express_fee;
                        end($searchRes->items)->ems_fee = $item->ems_fee;
                        end($searchRes->items)->postage_id = $item->postage_id;
                        end($searchRes->items)->cid = $item->cid;
                        end($searchRes->items)->tmall = $item->tmall;
                    }
                    if (!$light) {
                        $searchRes->cids = $DSGSearchRes->intCategories;
                        //$searchRes->bids = array();
                        $searchRes->groups = $DSGSearchRes->intGroups;
                        $searchRes->filters = $DSGSearchRes->intFilters;
                        $searchRes->multiFilters = $DSGSearchRes->intMultiFilters;
                        $searchRes->suggestions = $DSGSearchRes->intSuggestions;
                        $searchRes->priceRange = $DSGSearchRes->intPriceRanges;
                    }
//-----------------
                    if (($this->page == 1) && isset($this->params['cid']) && isset($this->params['query']) && (!$light)) {
                        $this->savePriceRanges($this->params['cid'], $this->params['query'], $searchRes->priceRange);
                    }
//-----------------
                    if (count($DSGSearchRes->items) > 0) {
                        Yii::app()->fileCache->set(
                          'DSG_search-' . implode('-', $cacheParams),
                          array($searchRes),
                          60 * 60 * (int) DSConfig::getVal('search_cache_ttl_search')
                        );
                    }
                }
            } else {
                $searchRes->total_results = 0;
            }
        } else {
            list($searchRes) = $cache;
        }
//===== START Post-translations ========================================

        if (!$light) {
            /*      foreach ($searchRes->cids as $k => $value) {
                    if (isset($value->cid)) {
                      $searchRes->cids[$k]->title = Yii::app()->DanVitTranslator->translateCid($value->cid, $value->title, 'zh-CHS', $lang);
                    }
                  }
            */
            Profiler::start('Cids->translate');
            Yii::app()->DanVitTranslator->translateArray($searchRes->cids, 'parseCids', 'zh-CHS', $lang);
            Profiler::stop('Cids->translate');

            Profiler::start('Groups->translate');
            Yii::app()->DanVitTranslator->translateArray($searchRes->groups, 'parseFilter', 'zh-CHS', $lang);
            Profiler::stop('Groups->translate');

            Profiler::start('Filters->translate');
            Yii::app()->DanVitTranslator->translateArray($searchRes->filters, 'parseFilter', 'zh-CHS', $lang);
            Profiler::stop('Filters->translate');

            Profiler::start('multiFilters->translate');
            Yii::app()->DanVitTranslator->translateArray($searchRes->multiFilters, 'parseMultiFilter', 'zh-CHS', $lang);
            Profiler::stop('multiFilters->translate');
//===================================================
// Sort and truncate arrays
//---------------------------------------------------
            /*    if (!function_exists('compareByValueTitle')) {
                  function compareByValueTitle($a, $b) {
                    $a_val = $a->values_title;
                    $b_val = $b->values_title;
                    return strcmp($a_val, $b_val);
                  }
                }
                if (isset(end($this->DSGSearchRes->intMultiFilters)->values)) {
                  uasort(end($this->DSGSearchRes->intMultiFilters)->values, "compareByValueTitle");
                }
            */
//===================================================
            Profiler::start('Suggestions->translate');
            Yii::app()->DanVitTranslator->translateArray($searchRes->suggestions, 'parseSuggestions', 'zh-CHS', $lang);
            Profiler::stop('Suggestions->translate');
        }
//===== END Post-translations ==========================================
        if ($search_DropShop_grabbers_debug) {
            $searchRes->debugMessages = new CArrayDataProvider(
              $DSGSearchRes->debugMessages, array(
                'id'         => 'id',
                'pagination' => array(
                  'pageSize' => 150,
                ),
              )
            );
        }
        return $searchRes;
    }

    public static function prepareProps($props, $cid = 0, $fromCart = false)
    {
        if (!$fromCart) {
            Profiler::start('search->prepareProps');
        }
        $lang = Utils::TransLang();
        $res = array();
        if (!isset($props)) {
            if ($cid == 0) {
                if (!$fromCart) {
                    Profiler::stop('search->prepareProps');
                }
                return $res;
            } else {
//=============================
                $datas = Yii::app()->db->createCommand(
                  "
 select vv.pid, vv.vid, pp.zh as pid_name, vv.zh as vid_name from categories_props pp, categories_props_vals vv
where (vv.CID=" . $cid . ") and pp.pid=vv.pid and and pp.cid=vv.cid
         "
                )->queryAll();
                $props = Array();
                foreach ($datas as $data) {
                    $props[] = new stdClass();
                    end($props)->pid = $data['pid'];
                    end($props)->vid = $data['vid'];
                    end($props)->pid_name = $data['pid_name'];
                    end($props)->vid_name = $data['vid_name'];
                }
//=============================
            }
        }
// обрабатываем свойства из массива
        $pids = '';
        foreach ($props as $k => $prop) {
            if ($k == 0) {
                $pids = $prop->pid;
            } else {
                $pids .= ',' . $prop->pid;
            }
        }
        if ($pids != '') {
            $datas = Yii::app()->db->createCommand(
              "
 select pp.pid,pp.IS_SALE_PROP, pp.IS_ENUM_PROP,pp.IS_COLOR_PROP from categories_props pp
where (pp.CID=" . $cid . ") and pp.pid in (" . $pids . ")
         "
            )->queryAll();
            foreach ($datas as $data) {
                foreach ($props as $prop) {
                    if ($prop->pid == $data['pid']) {
                        $prop->is_sale_prop = $data['IS_SALE_PROP'];
                        $prop->is_enum_prop = $data['IS_ENUM_PROP'];
                        $prop->is_color_prop = $data['IS_COLOR_PROP'];
                    }
                }
            }
        }
        if (!(isset($props[0]->name))) {
            $p = 0;
            foreach ($props as $k => $prop) {
                //if(isset($prop->prop_values) && isset($ps[$j]) && !in_array($prop->pid,array(21541,11760343)))
                if (!in_array($prop->pid, array(21541, 11760343))) {
                    $new = false;
                    if ($k == 0) {
                        $res[$prop->pid] = new stdClass();
                        $p = 0;
                        $new = true;
                    } elseif ($props[$k - 1]->pid != $props[$k]->pid) {
                        $res[$prop->pid] = new stdClass();
                        $p = 0;
                        $new = true;
                    }
                    if ($new) {
                        $res[$prop->pid]->cid = $cid;
                        $res[$prop->pid]->name_zh = $prop->pid_name;
                        if (!$fromCart) {
                            $res[$prop->pid]->name = $prop->pid_name;
                        } else {
                            $res[$prop->pid]->name = $prop->pid_name; //$prop->pid_name;
                        }
//                echo $res[$prop->pid]->name.'<br>';
                        $res[$prop->pid]->childs = array();
                        if (isset($prop->is_sale_prop)) {
                            $res[$prop->pid]->is_sale_prop = $prop->is_sale_prop;
                        } else {
                            $res[$prop->pid]->is_sale_prop = 0;
                        }
                        if (isset($prop->is_enum_prop)) {
                            $res[$prop->pid]->is_enum_prop = $prop->is_enum_prop;
                        } else {
                            $res[$prop->pid]->is_enum_prop = 0;
                        }
                        if (isset($prop->is_enum_prop)) {
                            $res[$prop->pid]->is_color_prop = $prop->is_color_prop;
                        } else {
                            $res[$prop->pid]->is_color_prop = 0;
                        }
                    }
                    $res[$prop->pid]->childs[$p] = new stdClass();
                    if (isset($prop->is_color_prop)) {
                        if ($prop->is_color_prop == 1) {
                            if (isset($prop->vid_alias)) {
                                $prop->vid_name = $prop->vid_alias;
                            }
                        }
                    }
                    if (isset($prop->vid_alias)) {
                        $t_name = $prop->vid_alias;
                    } else {
                        $t_name = $prop->vid_name;
                    }
                    $res[$prop->pid]->childs[$p]->name_zh = $t_name;
                    if (!$fromCart) {
                        $res[$prop->pid]->childs[$p]->name = $t_name;
                    } else {
                        $res[$prop->pid]->childs[$p]->name = $t_name;
                    }
                    $res[$prop->pid]->childs[$p]->vid = $prop->vid;
                    $p++;
                }
                //$j++;
            }
        } // обрабатываем свойства из массива ItempropsGetRequest
        else {
            $pids = implode(',', array_keys($props));
            $datas = Yii::app()->db->createCommand(
              "
 select pp.pid,pp.IS_SALE_PROP, pp.IS_ENUM_PROP,pp.IS_COLOR_PROP from categories_props pp
where (pp.CID=" . $cid . ") and pp.pid in (" . $pids . ")
         "
            )->queryAll();
            foreach ($datas as $data) {
                $pids[$data['pid']]->is_sale_prop = $data['IS_SALE_PROP'];
                $pids[$data['pid']]->is_enum_prop = $data['IS_ENUM_PROP'];
                $pids[$data['pid']]->is_color_prop = $data['IS_COLOR_PROP'];
            }

            foreach ($props as $prop) {
                //if(isset($prop->prop_values) && isset($ps[$j]) && !in_array($prop->pid,array(21541,11760343)))
                if (isset($prop->prop_values) && !in_array($prop->pid, array(21541, 11760343))) {
                    $res[$prop->pid] = new stdClass();
                    $res[$prop->pid]->cid = $cid;
                    $res[$prop->pid]->name_zh = $prop->name;
                    if (!$fromCart) {
                        $res[$prop->pid]->name = $prop->name;
                    } else {
                        $res[$prop->pid]->name = $prop->name;
                    }
                    $res[$prop->pid]->childs = array();
                    $p = 0;
                    foreach ($prop->prop_values->prop_value as $value) {
                        //if(isset($vs[$i]))
                        //{
                        $res[$prop->pid]->childs[$p] = new stdClass();
                        $res[$prop->pid]->childs[$p]->name_zh = $value->name;
                        if (!$fromCart) {
                            $res[$prop->pid]->childs[$p]->name = $value->name;
                        } else {
                            $res[$prop->pid]->childs[$p]->name = $value->name;
                        }
                        $res[$prop->pid]->childs[$p]->vid = $value->vid;
                        //}
                        $p++;
                        //$i++;
                    }
                }
                //$j++;
            }
        }
        if (!$fromCart) {
            Profiler::stop('search->prepareProps');
        }
        //================ Сортировка ===========================
        if (!function_exists('prop_compare')) {
            function prop_compare($a, $b)
            {
                if (key($a) == 20000) {
                    return -1;
                }
                if (isset($a->is_sale_prop) && isset($b->is_sale_prop)) {
                    if ((int) $a->is_sale_prop > (int) $b->is_sale_prop) //($a->is_enum_prop>$b->is_enum_prop)||
                    {
                        return -1;
                    }
                } else {
                    return strcmp($a->name, $b->name);
                }
                return 0;
            }
        }
        uasort($res, "prop_compare");
        //=========================================================
        return $res;
    }

    public function breadcrumbs()
    {
        if ($this->cid_model) {
            $model = $this->cid_model;
        } else {
                $model = false; //MainMenu::model()->find('cid=:cid AND status=1', array(':cid' => $this->params['cid']));
        }
        if (!$model) {
            $model = Category::model()->find(
              'cid=:cid AND status!=0 and is_parent=0',
              array(':cid' => $this->params['cid'])
            );
        }
        if ($model) {
            //Генерируем хлебные крошки
            $cat = new StdClass();
            $cat->cid = $model->cid;
            $cat->url = Category::getUrl($model);
            $cat->parent = $model->parent;
            $cat->name = $model->{Utils::TransLang()};
                $parents = array_reverse(MainMenu::getParents($cat, array($cat), 1));
            foreach ($parents as $category) {
                $this->breadcrumbs[$category->name] = Yii::app()
                  ->createUrl('/category/index', array('name' => $category->url));
            }
        }
        return $this->breadcrumbs;
    }

    public function savePriceRanges($cid, $query, $priceRanges)
    {
        /*
         `categories_prices` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `cid` bigint(20) NOT NULL DEFAULT '0',
          `query` varchar(220) NOT NULL DEFAULT '',
          `begin0` float DEFAULT NULL,
          `end0` float DEFAULT NULL,
          `percent0` float DEFAULT NULL,
          `begin1` float DEFAULT NULL,
          `end1` float DEFAULT NULL,
          `percent1` float DEFAULT NULL,
          `begin2` float DEFAULT NULL,
          `end2` float DEFAULT NULL,
          `percent2` float DEFAULT NULL,
          `begin3` float DEFAULT NULL,
          `end3` float DEFAULT NULL,
          `percent3` float DEFAULT NULL,
          `begin4` float DEFAULT NULL,
          `end4` float DEFAULT NULL,
          `percent4` float DEFAULT NULL,
         */
        if (!isset($priceRanges)) {
            return false;
        }
        if (is_array($priceRanges) && (count($priceRanges) == 5)) {
            if (is_null($cid)) {
                return false;
            }
            $intCid = $cid;
            $intQuery = $query;
//      $fromDateString = date("d.m.Y");
// ii1.date > STR_TO_DATE('".$fromDateString."','%d.%m.%Y');

            try {
                $command = Yii::app()->db->createCommand(
                  "select id, `date` from categories_prices where cid=:cid and query=:query limit 1"
                )
                  ->bindParam(':cid', $intCid, PDO::PARAM_INT)
                  ->bindParam(':query', $intQuery, PDO::PARAM_STR);
                $row = $command->queryRow();
                if ($row != false) {
                    if (!is_null($row['date'])) {
                        $date = DateTime::createFromFormat('Y-m-d', $row['date'])->getTimestamp();
                        if ((time() - $date) < (3600 * 24 * 7)) {
                            return true;
                        }
                    }

                    $command = Yii::app()->db->createCommand(
                      "update categories_prices
set begin0=:begin0,
    end0=:end0,
    percent0=:percent0,
    begin1=:begin1,
    end1=:end1,
    percent1=:percent1,
    begin2=:begin2,
    end2=:end2,
    percent2=:percent2,
    begin3=:begin3,
    end3=:end3,
    percent3=:percent3,
    begin4=:begin4,
    end4=:end4,
    percent4=:percent4,
    `date`=Now()
    where cid=:cid and query=:query"
                    )
                      ->bindParam(':cid', $intCid, PDO::PARAM_INT)
                      ->bindParam(':query', $intQuery, PDO::PARAM_STR)
                      ->bindParam(':begin0', $priceRanges[0]->start, PDO::PARAM_STR)
                      ->bindParam(':end0', $priceRanges[0]->end, PDO::PARAM_STR)
                      ->bindParam(':percent0', $priceRanges[0]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin1', $priceRanges[1]->start, PDO::PARAM_STR)
                      ->bindParam(':end1', $priceRanges[1]->end, PDO::PARAM_STR)
                      ->bindParam(':percent1', $priceRanges[1]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin2', $priceRanges[2]->start, PDO::PARAM_STR)
                      ->bindParam(':end2', $priceRanges[2]->end, PDO::PARAM_STR)
                      ->bindParam(':percent2', $priceRanges[2]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin3', $priceRanges[3]->start, PDO::PARAM_STR)
                      ->bindParam(':end3', $priceRanges[3]->end, PDO::PARAM_STR)
                      ->bindParam(':percent3', $priceRanges[3]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin4', $priceRanges[4]->start, PDO::PARAM_STR)
                      ->bindParam(':end4', $priceRanges[4]->end, PDO::PARAM_STR)
                      ->bindParam(':percent4', $priceRanges[4]->percent, PDO::PARAM_STR);
                    $command->execute();
                } else {
                    $command = Yii::app()->db->createCommand(
                      "insert into categories_prices (
cid,query,begin0,end0,percent0,begin1,end1,percent1,begin2,end2,percent2,
    begin3,end3,percent3,begin4,end4,percent4, `date`)
    values(
    :cid,:query,:begin0,:end0,:percent0,:begin1,:end1,:percent1,:begin2,:end2,:percent2,
    :begin3,:end3,:percent3,:begin4,:end4,:percent4, Now())"
                    )
                      ->bindParam(':cid', $intCid, PDO::PARAM_INT)
                      ->bindParam(':query', $intQuery, PDO::PARAM_STR)
                      ->bindParam(':begin0', $priceRanges[0]->start, PDO::PARAM_STR)
                      ->bindParam(':end0', $priceRanges[0]->end, PDO::PARAM_STR)
                      ->bindParam(':percent0', $priceRanges[0]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin1', $priceRanges[1]->start, PDO::PARAM_STR)
                      ->bindParam(':end1', $priceRanges[1]->end, PDO::PARAM_STR)
                      ->bindParam(':percent1', $priceRanges[1]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin2', $priceRanges[2]->start, PDO::PARAM_STR)
                      ->bindParam(':end2', $priceRanges[2]->end, PDO::PARAM_STR)
                      ->bindParam(':percent2', $priceRanges[2]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin3', $priceRanges[3]->start, PDO::PARAM_STR)
                      ->bindParam(':end3', $priceRanges[3]->end, PDO::PARAM_STR)
                      ->bindParam(':percent3', $priceRanges[3]->percent, PDO::PARAM_STR)
                      ->bindParam(':begin4', $priceRanges[4]->start, PDO::PARAM_STR)
                      ->bindParam(':end4', $priceRanges[4]->end, PDO::PARAM_STR)
                      ->bindParam(':percent4', $priceRanges[4]->percent, PDO::PARAM_STR);
                    $command->execute();
                }
            } catch (Exception $e) {
                LogSiteErrors::logError(
                  $e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'],
                  $e['message'],
                  $e['trace']
                );
                //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
}