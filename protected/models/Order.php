<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="Order.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $uid
 * @property string $status
 * @property integer $date
 * @property integer $manager
 * @property double $weight
 * @property double $delivery
 * @property double $sum
 * @property double $payed
 * @property string $code
 * @property integer $addresses_id
 * @property string $delivery_id
 * @property double $manual_weight
 * @property double $manual_delivery
 * @property double $manual_sum
 * @property string $store_id
 *
 * The followings are the available model relations:
 * @property Users $manager0
 * @property Users $u
 * @property Addresses $addresses
 * @property OrdersStatuses $status0
 * @property OrdersComments[] $ordersComments
 * @property OrdersItems[] $ordersItems
 * @property OrdersPayments[] $ordersPayments
 * @property OrdersPayments[] $ordersPayments1
 */
class Order extends customOrder
{
    /*=================================================================================================
     * Please use OOP (add, hide, override or overload any method or property) to implement
     * any of your own functionality needed. This file never will be overwritten or changed
     * with updates to protect your custom functionality.
     * Warning: don't change anything in "custom" folder - all your changes will be lost after update!
     =================================================================================================*/
    /*
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmsCustomContent the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}