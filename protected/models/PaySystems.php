<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="PaySystems.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * This is the model class for table "pay_systems".
 *
 * The followings are the available columns in table 'pay_systems':
 * @property integer $id
 * @property integer $enabled
 * @property string $logo_img
 * @property string $int_name
 * @property string $name
 * @property string $descr_ru
 * @property string $descr_en
 * @property string $parameters
 */
class PaySystems extends customPaySystems
{
    /*=================================================================================================
     * Please use OOP (add, hide, override or overload any method or property) to implement
     * any of your own functionality needed. This file never will be overwritten or changed
     * with updates to protect your custom functionality.
     * Warning: don't change anything in "custom" folder - all your changes will be lost after update!
     =================================================================================================*/
    /*
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmsCustomContent the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
