<?php

/**
 * This is the model class for table "categories_prices".
 *
 * The followings are the available columns in table 'categories_prices':
 * @property string $id
 * @property string $cid
 * @property string $query
 * @property double $begin0
 * @property double $end0
 * @property double $percent0
 * @property double $begin1
 * @property double $end1
 * @property double $percent1
 * @property double $begin2
 * @property double $end2
 * @property double $percent2
 * @property double $begin3
 * @property double $end3
 * @property double $percent3
 * @property double $begin4
 * @property double $end4
 * @property double $percent4
 * @property string $date
 */
class CategoriesPrices extends customCategoriesPrices
{
    /*=================================================================================================
     * Please use OOP (add, hide, override or overload any method or property) to implement
     * any of your own functionality needed. This file never will be overwritten or changed
     * with updates to protect your custom functionality.
     * Warning: don't change anything in "custom" folder - all your changes will be lost after update!
     =================================================================================================*/
    /*
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmsCustomContent the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
