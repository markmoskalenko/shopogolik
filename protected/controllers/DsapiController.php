<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CategoryController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class DsapiController extends CController {
    public function filters()
    {
        return array(
          array('application.components.PostprocessFilter'),
//      array( 'COutputCache', 'duration'=> 60,),
        );
    }
  public function actionExec() {
      $error=false;
      if (isset($_GET['user'])) {
          $user = $_GET['user'];
      } else {
          $error=true;
          echo "'user' parameter is missing";
      }
      if (isset($_GET['password'])) {
          $password = $_GET['password'];
      } else {
          $error=true;
          echo "'password' parameter is missing";
      }
      if (isset($_GET['command'])) {
          $command = $_GET['command'];
      } else {
          $error=true;
          echo "'command' parameter is missing";
      }
      if (isset($_GET['params'])) {
          $params = $_GET['params'];
      } else {
          $params=false;
      }
      if (!$error) {
      header('Content-type: application/json');
      echo DSGDsAPI::Exec($user,$password,$command,$params);
      }
      Yii::app()->end();
  }
}