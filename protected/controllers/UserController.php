<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="UserController.php">
* </description>
**********************************************************************************************************************/?>
<?php
/*
 * Контроллер для управления пользовательскими действиями, такими как регистрация,
 * авторизация, восстановление пароля, логаут.
 * @package User
 */
class UserController extends customUserController {
    /*=================================================================================================
     * Please use OOP (add, hide, override or overload any method or property) to implement
     * any of your own functionality needed. This file never will be overwritten or changed
     * with updates to protect your custom functionality.
     * Warning: don't change anything in "custom" folder - all your changes will be lost after update!
     =================================================================================================*/
    public function actionRegister() {
        $this->layout = '//layouts/login';

        parent::actionRegister();
    }

    /*
  * Действие для отображения страницы авторизации
  */
    public function actionLogin() {
        $this->layout = '//layouts/login';
        $this->pageTitle = Yii::t('main', 'Авторизация');
        $this->breadcrumbs = array(
            $this->pageTitle
        );
        $model = new UserForm('login');
        $form = new CForm('webroot.themes.' . Yii::app()->theme->name . '.views.user.loginForm', $model);
        if (isset($_POST['UserForm'])) {
            $model->attributes = $_POST['UserForm'];
            if ($model->validate()) {
                Users::model()->login($model->email, $model->password, $model->rememberMe);
            }
        }
        $this->render('login', array('form' => $form));
    }
}