<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SearchController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customSearchController extends CustomFrontController {
  public $body_class = 'search';

  public function actionIndex($query = '', $cid = 0, $page = 1, $props = FALSE,
                              $price_min = FALSE, $price_max = FALSE, $sales_min = FALSE, $sales_max = FALSE,
                              $rating_min = FALSE, $rating_max = FALSE, $sort_by = FALSE, $original = FALSE, $recommend = FALSE, $not_unique = FALSE,
$sameItemId=false,$similarItemId=false) {
      if ($page > 100) {
          Yii::app()->user->setFlash('error', Yii::t('main', 'Уточните критерии поиска для получения качественного результата!'));
          $page = 1;
      }
//== Всякие поиски по id и тп ==============================================
    if (preg_match('/^\d{9,13}$/',trim($query))) {
    $this->redirect($this->createUrl('item/index', array('iid' => trim($query))));
    }
      $isNickQuery=strpos(trim($query),'@')===0;
      if ($isNickQuery||($cid==='seller')) {
          $this->redirect($this->createUrl('seller/index', array('nick' => (($cid!='seller')?preg_replace('/^@/u','',trim($query)):trim($query)))));
      }

    //$id=array();
    //%26id%3D
// Поиск товара по URL на таобао
    $res=preg_match('/(?:(?:item.*(?:[?&]|%26|&amp;)(?:id|num_id|iid)(?:=|%3D))|(?:\/item\/))(\d{9,13})/i',trim($query),$inId);
    if ($res>0) {
      $this->redirect($this->createUrl('item/index', array('iid' => (string)$inId[1])));
    }
//==========================================================================
    $orig_params=array();
    $params=array();
    $res = '';
    $model = MainMenu::model()->find('id=:cid AND status!=0', array(':cid' => $cid));
    if (trim($query) == '' && $cid == 0 && $similarItemId==false && $sameItemId==false) {
      $this->redirect('/site/index');
    }
    elseif ($model && (bool) $cid && $cid !== 'seller') { // && !(bool)$query
//==================================
        $params = array(
          'name' => $model->url,
          'query' => $query,
          'cid' => $cid,
          'page' => 1,
          'props' => $props,
          'price_min' => $price_min,
          'price_max' => $price_max,
          'sales_min' => $sales_min,
          'sales_max' => $sales_max,
          'rating_min' => $rating_min,
          'rating_max' => $rating_max,
          'sort_by' => $sort_by,
          'original' => $original,
          'recommend' => $recommend,
          'not_unique' => $not_unique,
          'sameItemId'=> $sameItemId,
          'similarItemId'=>$similarItemId,
        );
        foreach ($params as $k => $param) {
          if (isset($_POST[$k])) {
            $params[$k] = $_POST[$k];
          }
        }
//==================================
        $this->redirect($this->createUrl('/category/index', $params)); //array('name'=>$model->url,'query'=>$query)
//      else {
//        throw new CHttpException(400, Yii::t('main', 'Выбранная Вами категория товаров временно недоступна, повторите поиск позже.'));
//      }
    }
    else {
      // Поиск товара по URL на таобао
      //render all params
      /*
             'query' => FALSE,
            'name' => $name,
            'props' => $props,
            'cid' => $model->cid,
            'cid_query' => $search_query,
            'virtual' => $virtual,
            'price_min' => $price_min,
            'price_max' => $price_max,
            'sales_min' => $sales_min,
            'sales_max' => $sales_max,
            'rating_min' => $rating_min,
            'rating_max' => $rating_max,
            'sort_by' => $sort_by,
            'original' => $original,
            'recommend' => $recommend
        */
      if (isset($_POST['props'])) {
        $props = '';
      }
      $params = array(
        'query' => $query,
        'cid' => $cid,
        'props' => $props,
        'price_min' => $price_min,
        'price_max' => $price_max,
        'sales_min' => $sales_min,
        'sales_max' => $sales_max,
        'rating_min' => $rating_min,
        'rating_max' => $rating_max,
        'sort_by' => $sort_by,
        'original' => $original,
        'recommend' => $recommend,
        'not_unique' => $not_unique,
        'sameItemId'=> $sameItemId,
        'similarItemId'=>$similarItemId,
      );
      foreach ($params as $k => $param) {
        if (isset($_POST[$k])) {
          $params[$k] = $_POST[$k];
        }
      }

      if (is_array($params['props'])) {
        $propsCid=false;
        foreach ($params['props'] as $k => $v) {
          if ($v !== '') {
            if ($k!=-1) {
            $props .= $k . ':' . $v . ';';
            } else {
              $propsCid=$v;
            }
          }
        }
        $params['props'] = $props;
        if ($propsCid) {
          $params['cid']=$propsCid;
        }
      }

      $search = new Search;
      $search->params = $params;
      $search->page = $page;
      $res = $search->execute();
      $this->breadcrumbs = $search->breadcrumbs();

      foreach ($params as $k => $v) {
        if (!(bool) $v) {
          unset($params[$k]);
        }
      }
      $orig_params = $params;
      unset($orig_params['original']);
      unset($orig_params['not_unique']);
    }

    $this->breadcrumbs[] = Yii::t('main', 'Результаты поиска') . ' "' . CHtml::encode(isset($_GET['query'])?$_GET['query']:'*') . '"';

    //Pagination
    if (isset($res->total_results)) {
      $pages = new CPagination($res->total_results);
      $pages->pageSize = Search::getPageSize();
      $pages->currentPage = $page - 1;
      $pages->params = $params;
    }
    else {
      $pages = FALSE;
    }

    $this->pageTitle = Yii::t('main', 'Поиск').': '.$query;
    $this->params['params'] = $params;
    $this->params['cids'] = $res->cids;
    $this->params['bids'] = $res->bids;
    $this->params['groups'] = $res->groups;
    $this->params['filters'] = $res->filters;
    $this->params['multiFilters'] = $res->multiFilters;
    $this->params['suggestions'] = $res->suggestions;
    $this->params['priceRange'] = $res->priceRange;
    /*
                if($res->total_results == 0 && $cid == 0){
                    self::sellers($query);
                }
    */
    $this->render('index', array(
      'res' => $res,
      'params' => $params,
      'sort_by' => $sort_by,
      'pages' => $pages,
      'orig_params' => $orig_params,
    ));
  }

}