<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="ToolsController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class customToolsController extends CustomFrontController {

  public function actionCalculator() {
    $this->pageTitle = Yii::t('main', 'Расчет стоимости доставки');
    $this->breadcrumbs = array(
      $this->pageTitle
    );
    $selected_country = NULL;
    $addresses = Yii::app()->db->createCommand()
      ->select('*')
      ->from('addresses')
      ->where('uid=:uid', array(':uid' => Yii::app()->user->id))
      ->limit(1)
      ->order('id ASC')
      ->queryAll();
    if (is_array($addresses) && isset($addresses[0]['country'])) {
      $selected_country = $addresses[0]['country'];
    }
    $model = new ToolsForm('calc');
    $res = FALSE;
    if (isset($_POST['ToolsForm'])) {
      $_POST['ToolsForm']['weight'] = strtr($_POST['ToolsForm']['weight'], array(',' => '.'));
      $model->attributes = $_POST['ToolsForm'];
      $weight = $model->weight;
      if ($weight == 0) {
        $weight = 100;
      }
      if ($model->validate()) {
        $res = TRUE;
        $delivery = Deliveries::getDelivery($weight, $model->country);
      }
      else {
        $delivery = Deliveries::getDelivery($weight, $model->country);
      }
    }
    else {
      $delivery = Deliveries::getDelivery(100, $model->country);
    }
// Weights list =================================================
    $session = new CHttpSession;
    $session->open();
    $weights = new Weights('search');
    $weights->unsetAttributes(); // clear any default values
    $weights->ru = NULL;
    if (isset($_GET['Weights'])) {
      $weights->attributes = $_GET['Weights'];
      if (isset($_GET['Weights']['ru'])) {
        $weights->ru = $_GET['Weights']['ru'];
      }
      else {
        $weights->ru = NULL;
      }
    }
    $session['Weights_records'] = $weights->search();
//===============================================================
    $render = array(
      'model'            => $model,
      'res'              => $res,
      'delivery'         => $delivery,
      'selected_country' => $selected_country,
      'weights'          => $weights,
    );
    $this->render('calculator', $render);
  }

  function actionQuestion() {
    $file_name = '';
    $this->pageTitle = Yii::t('main', 'Задать вопрос');
    $this->breadcrumbs = array(
      $this->pageTitle
    );

    $category_values = array(
      1 => Yii::t('main', 'Общие вопросы'),
      2 => Yii::t('main', 'Вопросы по моему заказу'),
      3 => Yii::t('main', 'Рекламация'),
      4 => Yii::t('main', 'Возврат денег'),
      5 => Yii::t('main', 'Оптовые заказы'),
    );

    if (!Yii::app()->user->isGuest) {

      $orders_list = array();

      $uid = Yii::app()->user->id;
      $criteria = new CDbCriteria;
      $criteria->select = 'id,date';
      $criteria->condition = 'uid=:uid';
      $criteria->params = array(':uid' => $uid);
      $criteria->order = 'date DESC';
      $orders = Order::model()->findAll($criteria);

      if (count($orders) > 0) {

      }
    }
    else {
      $orders_list = array();
    }

    $model = new ToolsForm('question');
    $model->email = (!Yii::app()->user->isGuest) ? Yii::app()->user->email : '';
    if (isset($_POST['ToolsForm'])) {
      $model->attributes = $_POST['ToolsForm'];

      if ($model->validate()) {
        $question = new Question;
        $question->theme = $model->theme;
        $question->email = $model->email;
        $question->date = time();
        $question->category = $_POST['ToolsForm']['category'];
        if ($model->order_id) {
          $question->order_id = $model->order_id;
        }
        else {
          $question->order_id = NULL;
        }

        if (Yii::app()->user->isGuest) {
          $question->uid = 0;
        }
        else {
          $question->uid = Yii::app()->user->id;
        }

        $save_file = FALSE;
        if (!empty($_FILES['ToolsForm']['tmp_name']['file'])) {
          if ($_FILES['ToolsForm']['size']['file'] > 5 * 1024 * 1024) {
            Yii::app()->user->setFlash('success', Yii::t('main', 'Файл должен быть меньше, чем 5MB'));
            $this->redirect('/');
          }

          $file = CUploadedFile::getInstance($model, 'file');
          $extension = CFileHelper::getExtension($file);
          $question->file = $file;
          $file_name = uniqid('') . '.' . $extension;
          $save_file = TRUE;
        }
        else {
          $question->file = NULL;
        }

        $question->save(FALSE);

        if ($save_file) {
          $question->file->saveAs($_SERVER['DOCUMENT_ROOT'] . '/upload/' . $file_name);
          $question->file = $file_name;
          $question->save(FALSE);
        }

        $message = new Message;

        if (Yii::app()->user->isGuest) {
          $message->uid = 0;
        }
        else {
          $message->uid = Yii::app()->user->id;
        }

        $message->email = $model->email;
        $message->qid = $question->id;
        $message->question = $model->question;
        $message->date = time();
        $message->save();

        Yii::app()->user->setFlash('success', Yii::t('main', 'Ваше сообщение отправлено.'));
        $this->redirect('/');
      }
    }
    $render = array(
      'model'           => $model,
      'category_values' => $category_values,
      'orders'          => $orders_list,
    );

    $this->render('question', $render);
  }

    public function actionPostcalc() {
       $this->renderPartial( 'postcalc.postcalc_light', array(),false,true);
    }

}