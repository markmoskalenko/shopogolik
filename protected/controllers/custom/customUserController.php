<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="UserController.php">
* </description>
**********************************************************************************************************************/?>
<?php
/*
 * Контроллер для управления пользовательскими действиями, такими как регистрация,
 * авторизация, восстановление пароля, логаут.
 * @package User
 */
class customUserController extends CustomFrontController {

  public $columns = 'three-col';

  public function actions() {
    return array(
      // captcha action renders the CAPTCHA image displayed on the contact page
      'captcha' => array(
        'class' => 'CCaptchaAction',
        'backColor' => 0xFFFFFF,
      )
    );
  }

  /*
   * Действие для отображения страницы авторизации
   */
  public function actionLogin() {
    $this->pageTitle = Yii::t('main', 'Авторизация');
    $this->breadcrumbs = array(
      $this->pageTitle
    );
    $model = new UserForm('login');
    $form = new CForm('webroot.themes.' . Yii::app()->theme->name . '.views.user.loginForm', $model);
    if (isset($_POST['UserForm'])) {
      $model->attributes = $_POST['UserForm'];
      if ($model->validate()) {
        Users::model()->login($model->email, $model->password, $model->rememberMe);
      }
    }
    $this->render('login', array('form' => $form));
  }

  public function actionRegister() {
    $this->pageTitle = Yii::t('main', 'Регистрация');
    $this->breadcrumbs = array(
      $this->pageTitle
    );

    $model = new UserForm('register');

    if (isset($_POST['UserForm'])) {
      $model->attributes = $_POST['UserForm'];
      if ($model->validate()) {
        if (isset($_POST['UserForm']['privacy']) && $_POST['UserForm']['privacy'] == 1) {
          $record = Users::model()->findByAttributes(array('email' => $model->email));
          if ($record === NULL) {
            $user = new Users();
            $user->attributes=$model->attributes;
            $user->email = strtolower($model->email);
            $user->password = md5($model->password);
            $user->default_manager=Users::getUidByPromo($model->default_manager);
            if (!$user->default_manager) {
              $user->default_manager=users::getFirstSuperAdminId();
            }
            $user->status = 1;
            $user->created = time();

            if ($user->save()) {
              Users::sendRegMail($user);
              Yii::app()->user->setFlash('successEmail',
                Yii::t('main', 'Вы успешно зарегистрированы!').'<br>'.
                Yii::t('main', 'На почтовый ящик').' '.$user->email.' '.Yii::t('main', 'отправлено сообщение для подтверждения аккаунта').'.<br>'.
                Yii::t('main','Если письмо не приходит, попробуйте проверить папку "Спам"')
            );
              Users::model()->login($user->email, $model->password);
              $this->redirect($this->createUrl('/site/index'));
            }
            else {
              Yii::app()->user->setFlash('errorRegister',
                Yii::t('main', 'Произошла ошибка! Попробуйте еще раз.'));
            }
          }
          else {
            Yii::app()->user->setFlash('emailFinded',
              Yii::t('main', 'Данный Email уже зарегистрирован в базе'));
          }
        }
        else {
          Yii::app()->user->setFlash('emailFinded', Yii::t('main', 'Вы должны принять условия пользовательского соглашения'));
        }
      }
    }
    $this->render('register', array('model' => $model));
  }

  public function actionCheck($email, $key) {
    $record = Users::model()->findByAttributes(array('email' => $email));
    if ($record === NULL) {
      Yii::app()->user->setFlash('notFoundUser', Yii::t('main',
        'Пользователь с указаным EMail не найден!'));
      $this->redirect($this->createUrl('/site/index'));
    }
    else {
      $hash = Yii::app()->getSecurityManager();
      if ($key == $hash->hashData($record->created)) {
        $record->status = 1;
        $record->save();
        Yii::app()->user->setFlash('successCheck', Yii::t('main', 'Ваш Email успешно подтвержден!') .
          '<br>' . Yii::t('main', 'Теперь вы можете авторизоваться'));
        $this->redirect('/user/login');
      }
      else {
        throw new Exception (Yii::t('main', 'Код доступа не верный!'), 400);
      }
    }
  }

  public function actionLogout() {
    Yii::app()->user->logout();
    $this->redirect('/site/index');
  }

  public function actionSetLang($lang = 'ru') {
    $langs = array('en', 'ru', 'zh', 'he', 'fr', 'de', 'es');

    if (!in_array($lang, $langs)) {
      $lang = 'ru';
    }
      if (Yii::app()->getBaseUrl(true)== 'http://'.DSConfig::getVal('site_domain')) {
          $cookie = new CHttpCookie('lang', $lang);
          $cookie->expire = time() + 60 * 60 * 24 * 180;
          Yii::app()->request->cookies['lang'] = $cookie;
      }
    $url = Yii::app()->request->urlReferrer;
    $url = strtr($url, array(
      '/ru/' => '/' . $lang . '/',
      '/en/' => '/' . $lang . '/',
      '/zh/' => '/' . $lang . '/',
      '/he/' => '/' . $lang . '/',
      '/de/' => '/' . $lang . '/',
      '/fr/' => '/' . $lang . '/',
      '/es/' => '/' . $lang . '/'
    ));

    if (strpos($url, $lang) <= 0) {
      $url = strtr($url, array('/admin' => '/' . $lang . '/admin'));
    }
    $this->redirect($url);
    Yii::app()->end();
  }

  function actionSetCurrency($curr = FALSE) {
    $currs = explode(',', DSConfig::getVal('site_currency_block'));
    if (!in_array($curr, $currs)) {
      $curr = DSConfig::getVal('site_currency');
    }
    $cookie = new CHttpCookie('currency', $curr);
    $cookie->expire = time() + 60 * 60 * 24 * 180;
    Yii::app()->request->cookies['currency'] = $cookie;
    $this->redirect(Yii::app()->request->urlReferrer);
    Yii::app()->end();
  }

  function actionPassword() {
    $this->pageTitle = Yii::t('main', 'Восстановление пароля');
    $this->breadcrumbs = array(
      $this->pageTitle
    );
    $model = new UserForm('password');
    $form = new CForm('webroot.themes.' . Yii::app()->theme->name . '.views.user._password', $model);
    if (isset($_POST['UserForm'])) {
      $model->attributes = $_POST['UserForm'];
      if ($model->validate()) {
        $user = Users::model()->find('email=:email', array(':email' => $model->email));
        if ($user == FALSE) {
          Yii::app()->user->setFlash('non-found', Yii::t('main', 'Данный EMail в базе не найден'));
        }
        elseif (Users::sendPassMail($user)) {
          Yii::app()->user->setFlash('password_reset', Yii::t('main', 'Вам на почту отправлено сообщение с инструкциями о смене пароля'));
          $this->redirect('/user/login');
        }
      }
    }
    $this->render('password', array('form' => $form));
  }


  function actionPassword_reset($email, $key) {
    $record = Users::model()->findByAttributes(array('email' => $email));
    if ($record === NULL) {
      Yii::app()->user->setFlash('notFoundUser', Yii::t('main',
        'Данный Email в базе не найден'));
      $this->redirect($this->createUrl('/site/index'));
    }
    else {
      $hash = Yii::app()->getSecurityManager();
      if ($key == $hash->hashData($record->email)) {
        $this->pageTitle = Yii::t('main', 'Восстановление пароля');
        $this->breadcrumbs = array(
          $this->pageTitle
        );
        $model = new UserForm('password_reset');
        $form = new CForm('webroot.themes.' . Yii::app()->theme->name . '.views.user._password_reset', $model);
        if (isset($_POST['UserForm'])) {
          $model->attributes = $_POST['UserForm'];
          if ($model->validate()) {
            $record->password = md5($model->password);
            $record->save();
            Yii::app()->user->setFlash('pass', Yii::t('main', 'Ваш пароль успешно изменен.') . '<br>' .
              Yii::t('main', 'Теперь вы можете авторизоваться'));
            $this->redirect('/user/login');
          }
        }
        $this->render('password_reset', array('form' => $form));
      }
      else {
        throw new Exception (Yii::t('main', 'Код доступа не верный!'), 400);
      }
    }
  }

  public function actionNotice() {
    $notices = UserNotice::model()->findAll('uid=:uid', array(':uid' => Yii::app()->user->id));
    foreach ($notices as $notice) {
      $notice->delete();
    }
    if (Yii::app()->request->getRequestUri()!=Yii::app()->request->urlReferrer) {
    $this->redirect(Yii::app()->request->urlReferrer);
    }
  }

}