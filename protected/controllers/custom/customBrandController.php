<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="BrandController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customBrandController extends CustomFrontController {

  public function actionIndex($name = FALSE, $cid = 0, $page = 1, $props = FALSE,
                              $price_min = FALSE, $price_max = FALSE, $sales_min = FALSE, $sales_max = FALSE,
                              $rating_min = FALSE, $rating_max = FALSE, $sort_by = FALSE, $original = FALSE, $recommend = FALSE, $not_unique = FALSE) {
    if (!$name) {
      throw new CHttpException(400, Yii::t('main', 'Неверный запрос'));
    }

    if ($page > 99) {
      Yii::app()->user->setFlash('error', Yii::t('main', 'Уточните критерии поиска для получения качественного результата'));
      $page = 99;
    }

    $model = Brands::model()->find('url=:url AND enabled=1', array(':url' => $name));
    if ($model == FALSE) {
      throw new CHttpException (400, Yii::t('main', 'Неверный запрос'));
    }

    //render all params
    $props = '';
    /*
          'query' => FALSE,
          'name' => $name,
          'props' => $props,
          'cid' => $model->cid,
          'cid_query' => $search_query,
          'virtual' => $virtual,
          'price_min' => $price_min,
          'price_max' => $price_max,
          'sales_min' => $sales_min,
          'sales_max' => $sales_max,
          'rating_min' => $rating_min,
          'rating_max' => $rating_max,
          'sort_by' => $sort_by,
          'original' => $original,
          'recommend' => $recommend
     */
    $params = array(
      'query' => FALSE,
      'name' => $name,
      'props' => $props,
      'cid' => $cid,
      'price_min' => $price_min,
      'price_max' => $price_max,
      'sales_min' => $sales_min,
      'sales_max' => $sales_max,
      'rating_min' => $rating_min,
      'rating_max' => $rating_max,
      'sort_by' => $sort_by,
      'original' => $original,
      'recommend' => $recommend,
      'not_unique' => $not_unique,
    );
    foreach ($params as $k => $param) {
      if (isset($_POST[$k])) {
        $params[$k] = $_POST[$k];
      }
    }
    if ($model->vid != NULL && $model->vid != '' && $model->vid != 0) {
      $params['props'][20000] = $model->vid;
    }
    else {
      $params['query'] = $model->query;
    }

    if (is_array($params['props'])) {
      $propsCid=false;
      foreach ($params['props'] as $k => $v) {
        if ($v !== '') {
          if ($k!=-1) {
            $props .= $k . ':' . $v . ';';
          } else {
            $propsCid=$v;
          }
        }
      }
      $params['props'] = $props;
      if ($propsCid) {
      //  $params['query']=false;
        $params['cid']=$propsCid;
      }
    }

    $search = new Search;
    $search->params = $params;
    $search->page = $page;
    $search->cid_model = $model;

    $res = $search->execute();

    foreach ($params as $k => $v) {
      if (!(bool) $v) {
        unset($params[$k]);
      }
    }
    $orig_params = $params;
    unset($orig_params['original']);
//         unset($orig_params['recommend']);
    unset($orig_params['not_unique']);
    $this->breadcrumbs = array(
      Yii::t('main', 'Бренды'),
      $model->name,
    );
      $lang = Utils::TransLang();
      //СЕО настройки
      $this->pageTitle = (!empty($model->{'page_title_' . $lang})) ? $model->{'page_title_' . $lang} : $model->name;
      $this->meta_desc = (!empty($model->{'meta_desc_' . $lang})) ? $model->{'meta_desc_' . $lang}
        : Yii::t('main', 'Широкий ассортимент, {name} в нашем магазине', array('{name}' => $model->name));
      $this->meta_keyword = (!empty($model->{'meta_keeword_' . $lang})) ? $model->{'meta_keeword_' . $lang} : $model->name;
      //end

    //Pagination
    if (isset($res->total_results)) {
      $pages = new CPagination($res->total_results);
      $pages->pageSize = Search::getPageSize();
      $pages->currentPage = $page - 1;
      $pages->params = $params;
    }
    else {
      $pages = FALSE;
    }

    $this->params['params'] = $params;
    $this->params['cids'] = $res->cids;
    $this->params['bids'] = $res->bids;
    $this->params['groups'] = $res->groups;
    $this->params['filters'] = $res->filters;
    $this->params['multiFilters'] = $res->multiFilters;
    $this->params['suggestions'] = $res->suggestions;
    $this->params['priceRange'] = $res->priceRange;

    $this->render('/search/index', array(
      'res' => $res,
      'params' => $params,
      'sort_by' => $sort_by,
      'pages' => $pages,
      'orig_params' => $orig_params,
      'brand'    => $model,
    ));
  }

  function actionList() {
    $this->pageTitle = Yii::t('main', 'Все бренды');

    $this->breadcrumbs = array(
      $this->pageTitle
    );

    $this->params['params'] = FALSE;
    $this->params['cids'] = array();
    $this->params['bids'] = array();
    $this->params['groups'] = array();
    $this->params['filters'] = array();
    $this->params['multiFilters'] = array();
    $this->params['suggestions'] = array();
    $this->params['priceRange'] = array();

    $models = Brands::model()->findAll('enabled=1');
    $brands = array();
    foreach ($models as $model) {
      $alpha = substr($model->name, 0, 1);
      if (!isset($brands[$alpha])) {
        $brands[$alpha] = array();
      }
      $brands[$alpha][] = $model;
    }
    $this->render('list', array('brands' => $brands));
  }

}