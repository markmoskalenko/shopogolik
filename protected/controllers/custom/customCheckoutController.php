<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="CheckoutController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class customCheckoutController extends CustomFrontController
{
    public function filters()
    {
        return array(
          'Rights',
          array('application.components.PostprocessFilter'), // perform access control for CRUD operations
        );
    }

    /*
     * Первый шаг оформления заказа
     * 1. Если у пользователя имеется заказ который он недавно заказал, то он может сделать в него дозаказ
     * 2. Если такого заказа нет, то на данном этапе вводятся личные данные пользователя
     */

    public function actionIndex($user = false)
    {
        $this->checkCartIsEmpty(true);
        if ($user !== false) {
            $this->stepSelectAddress();
        } else {
            Cart::getUserCart(false,true);
            $checkout_reorder_needed = DSConfig::getVal('checkout_reorder_needed') == 1;
            if ($checkout_reorder_needed) {
                //Проверка на наличие таких заказов
                $orders = Order::getOrdersListForReorder(Yii::app()->user->id);
                if ($orders->getTotalItemCount() > 0) {
                    $this->stepSelectReorder($orders);
                } else {
                    $this->stepSelectAddress();
                }
            } else {
                $this->stepSelectAddress();
            }
        }
    }

    public function actionadd_address()
    {
        $this->checkCartIsEmpty(true);
        $session = new CHttpSession;
        $session->open();
        $model = new CheckoutForm('address');
        $form = new CForm('webroot.themes.' . Yii::app()->theme->name . '.views.checkout.addressForm', $model);
        $this->pageTitle = Yii::t('main', 'Адрес доставки');
        if (isset($_POST['CheckoutForm'])) {
            $model->attributes = $_POST['CheckoutForm'];
            $address = new Addresses;
            if ($model->validate() && $_POST['CheckoutForm']['step'] == '1') {
                $address->uid = Yii::app()->user->id;
                $address->country = $model->country;
                $address->city = $model->city;
                $address->index = $model->index;
                $address->phone = $model->phone;
                $address->address = $model->address;
                $address->firstname = $model->firstname;
                $address->patroname = $model->patroname;
                $address->lastname = $model->lastname;
                $address->region = $model->region;
                $address->save();
                unset($session['order_id']);
                $session['addresses_id'] = $address->id;
                $session['firstname'] = $model->firstname;
                $session['lastname'] = $model->lastname;
                $session['country'] = $model->country;
                $session['index'] = $model->index;
                $session['city'] = $model->city;
                $session['address'] = $model->address;
                $session['phone'] = $model->phone;
                $session['region'] = $model->region;
                $session['step'] = '2';
                $this->redirect('/checkout/weight');
            }
        }
        $this->render('add_address', array('form' => $form));
    }

    public function actionchoose_address($id)
    {
        $this->checkCartIsEmpty();
        $properties = array(
          'firstname',
          'lastname',
          'country',
          'index',
          'city',
          'address',
          'phone'
        );
        if (!$id) {
            throw new CHttpException (400, Yii::t('main', 'Адрес не найден или введен с ошибками.'));
        }
        $address = Addresses::model()->findByPk($id);
        if ($address) {
            foreach ($properties as $pr) {
                if (empty ($address->$pr)) {
                    Yii::app()->user->setFlash('payment', Yii::t('main', 'Не заполнены необходимые поля в адресе!'));
                    $this->redirect('/checkout');
                }
            }
            $session = new CHttpSession;
            $session->open();
            unset($session['order_id']);
            $session['addresses_id'] = $address->id;
            $session['firstname'] = $address->firstname;
            $session['lastname'] = $address->lastname;
            $session['country'] = $address->country;
            $session['index'] = $address->index;
            $session['city'] = $address->city;
            $session['address'] = $address->address;
            $session['phone'] = $address->phone;
            $session['region'] = $address->region;
            $session['step'] = '2';
            $this->redirect('/checkout/weight');
        }
        throw new CHttpException (400, Yii::t('main', 'Адрес не найден или введен с ошибками.'));
    }

    protected function stepSelectAddress()
    {
        $session = new CHttpSession;
        $session->open();
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $addresses = Yii::app()->db->createCommand()
          ->select('*')
          ->from('addresses')
          ->where('uid=:uid and enabled=1', array(':uid' => Yii::app()->user->id))
          ->order('id DESC')
          ->queryAll();
        $model = new CheckoutForm('address');
        $form = new CForm('webroot.themes.' . Yii::app()->theme->name . '.views.checkout.addressForm', $model);
        $this->pageTitle = Yii::t('main', 'Адрес доставки');
        $this->breadcrumbs = array(
          Yii::t('main', 'Оформление заказа') => '/checkout/index',
          $this->pageTitle
        );
        $data = array();
        if ($user) {
            foreach ($user as $k => $v) {
                $data[$k] = $v;
            }
        }
        $model->attributes = $data;
        if (isset($_POST['CheckoutForm'])) {
            $model->attributes = $_POST['CheckoutForm'];
            $address = new Addresses;
            if ($model->validate() && $_POST['CheckoutForm']['step'] == '1') {
                $address->uid = Yii::app()->user->id;
                $address->country = $model->country;
                $address->city = $model->city;
                $address->index = $model->index;
                $address->phone = $model->phone;
                $address->address = $model->address;
                $address->firstname = $model->firstname;
                $address->patroname = $model->patroname;
                $address->lastname = $model->lastname;
                $address->region = $model->region;
                $address->save();
                unset($session['order_id']);
                $session['addresses_id'] = $address->id;
                $session['firstname'] = $model->firstname;
                $session['lastname'] = $model->lastname;
                $session['country'] = $model->country;
                $session['index'] = $model->index;
                $session['city'] = $model->city;
                $session['address'] = $model->address;
                $session['phone'] = $model->phone;
                $session['region'] = $model->region;
                $session['step'] = '2';
                $this->redirect('/checkout/weight');
            }
        }
        $this->render(
          'index',
          array('countries' => Deliveries::getCountries(), 'form' => $form, 'addresses' => $addresses)
        );
        exit();
    }

    protected function stepSelectReorder($orders)
    {
        $this->pageTitle = Yii::t('main', 'Выбор заказа');
        $this->render('reorder', array('orders' => $orders));
    }

    /*
     * Выбор заказа для дозаказа или создание нового
     */
    public function actionReorder()
    {
        $this->checkCartIsEmpty();
        if (!(bool) $_POST['order']) {
            $this->redirect('/checkout/index/user/1');
        } else {
            $session = new CHttpSession;
            $session->open();
            $session['order_id'] = $_POST['order'];
            $session['step'] = '2';
            $this->redirect('/checkout/weight');
        }
    }

    public function actionWeight()
    {
        $this->checkCartIsEmpty(true);
        $session = new CHttpSession;
        $session->open();
        if ($session['step'] !== '2') {
            $this->redirect('/checkout');
        }
        $cart = Cart::getUserCart(false,true);
//------------------------------------
        $checkout_weight_needed = DSConfig::getVal('checkout_weight_needed') == 1;
        if ($checkout_weight_needed) {
            if (isset($_POST['weight']) && isset($_POST['num'])) {
                $description = $_POST['description'];
                $num = $_POST['num'];
                $weight = $_POST['weight'];
                foreach ($cart->cartRecords as $item) {
                    if (isset($num[$item->id]) && isset($description[$item->id])) {
                        if ((int) $num[$item->id] <= 0) {
                            $item->order = 0;
                        }
                        $item->num = round($num[$item->id]);
                        $item->desc = $description[$item->id];
                        $item->weight = abs($weight[$item->id]);
                        $item->update();
                    }
                }
                Cart::clearCartCache(false);
                $cart = Cart::getUserCart(false, true);
                $session['weight'] = $cart->totalWeight;
                if (isset($session['order_id'])) {
                    $this->reaorderCalc($session['order_id']);
                    $session['step'] = 4;
                    $this->redirect('/checkout/payment');
                } else {
                    $session['step'] = 3;
                    $this->redirect('/checkout/delivery');
                }
            }
//----------------------------------
        } else {
//----------------------------------
            $session['weight'] = 0;
            if (isset($session['order_id'])) {
                $this->reaorderCalc($session['order_id']);
                $session['step'] = 4;
                $this->redirect('/checkout/payment');
            } else {
                $session['step'] = 3;
                $this->redirect('/checkout/delivery');
            }
        }
        $this->pageTitle = Yii::t('main', 'Расчет веса');
        $this->breadcrumbs = array(
          Yii::t('main', 'Оформление заказа') => '/checkout/index',
          $this->pageTitle
        );
// Weights list =================================================
        $session = new CHttpSession;
        $session->open();
        $weights = new Weights('search');
        $weights->unsetAttributes(); // clear any default values
        $weights->ru = NULL;
        if (isset($_GET['Weights'])) {
            $weights->attributes = $_GET['Weights'];
            if (isset($_GET['Weights']['ru'])) {
                $weights->ru = $_GET['Weights']['ru'];
            }
            else {
                $weights->ru = NULL;
            }
        }
        $session['Weights_records'] = $weights->search();

        $this->render('weight', array('cart' => $cart,'weights'=>$weights));
    }
/*
// Weights list =================================================
        $session = new CHttpSession;
        $session->open();
        $weights = new Weights('search');
        $weights->unsetAttributes(); // clear any default values
        $weights->ru = NULL;
        if (isset($_GET['Weights'])) {
            $weights->attributes = $_GET['Weights'];
            if (isset($_GET['Weights']['ru'])) {
                $weights->ru = $_GET['Weights']['ru'];
            }
            else {
                $weights->ru = NULL;
            }
        }
        $session['Weights_records'] = $weights->search();

        $this->render('weight', array('cart' => $cart,'weights'=>$weights));
 */

    function actionDelivery()
    {
        $this->checkCartIsEmpty(true);
        $checkout_delivery_needed = DSConfig::getVal('checkout_delivery_needed') == 1;
        if ($checkout_delivery_needed) {
//------------------------------------
            $this->pageTitle = Yii::t('main', 'Выбор способа доставки');
            $this->breadcrumbs = array(
              Yii::t('main', 'Оформление заказа') => '/checkout/index',
              $this->pageTitle
            );

            $session = new CHttpSession;
            $session->open();

            if (!isset($session['country']) || $session['step'] !== 3) {
                $this->redirect('/checkout');
            }

            $data = array();
            foreach ($session as $k => $v) {
                $data[$k] = $v;
            }
//========================================================
            $delivery = Deliveries::getDelivery($session['weight'], $session['country']);
//=========================================================
            $model = new CheckoutForm('delivery');
            if (isset($_POST['CheckoutForm'])) {
                $model->attributes = $_POST['CheckoutForm'];
                if ($model->validate()) {
                    $session['delivery_id'] = $model->delivery;
                    $session['delivery'] = ((DSConfig::getVal('checkout_delivery_sum_needed')==1)?$delivery[$model->delivery]->summ:0);
                    $session['delivery_currency'] = $delivery[$model->delivery]->currency;
                    $session['step'] = 4;
                    $this->redirect('/checkout/payment');
                }
            }
            $this->render(
              'delivery',
              array(
                'delivery' => $delivery,
                'model'    => $model
              )
            );
        } else {
//==============================================================
            $session = new CHttpSession;
            $session->open();
            $data = array();
            foreach ($session as $k => $v) {
                $data[$k] = $v;
            }

            $session['delivery_id'] = '';
            $session['delivery'] = 0;
            $session['step'] = 4;
            $this->redirect('/checkout/payment');

//==============================================================
        }
    }

    function actionPayment()
    {
        $this->pageTitle = Yii::t('main', 'Оплата заказа');
        $this->breadcrumbs = array(
          Yii::t('main', 'Оформление заказа') => '/checkout/index',
          $this->pageTitle
        );
//----------------------
        $session = new CHttpSession;
        $session->open();
        $session['country_name'] = Deliveries::getCountryName($session['country']);
        $data = array();
        foreach ($session as $k => $v) {
            $data[$k] = $v;
        }

        if ($session['step'] !== 4) {
            $this->redirect('/checkout');
        }
        if (isset($session['order_id'])) {
            $session['reorder'] = true;
        } else {
            $session['reorder'] = false;
        }
        $cart = Cart::getUserCart(false,true);
        if (isset($_POST['doGo']) || isset($_POST['doGoNoPayment'])) {
            if (isset($_POST['doGoNoPayment'])) {
                $pay_in_office = true;
            } else {
                $pay_in_office = false;
            }
            if (isset($session['order_id'])) {
                $reorder = true;
                $this->save_order($data, $cart, $session['order_id']);
            } else {
                $reorder = false;

                $session['order_id'] = $this->save_order($data, $cart);
            }
            if ($session['order_id']) {
                $paymentSumm = (Formulas::convertCurrency(
                    $cart->total,
                    DSConfig::getCurrency(),
                    DSConfig::getVal('site_currency')
                  ) + $data['delivery']);
                if (!$pay_in_office) {
                    if (
                    OrdersPayments::payForOrder(
                      $session['order_id'],
                      $paymentSumm,
                      Yii::t('main', 'Оплата заказа') . ' №' . $session['order_id']
                    )
                    ) {
                        Cart::deleteUserCart();
                    }
                } else {
                    Cart::deleteUserCart();
                }
                if ($pay_in_office) {
                    Yii::app()->user->setFlash(
                      'checkout',
                      Yii::t(
                        'main',
                        'Ваш заказ успешно оформлен!  Вы можете оплатить его позже и отслеживать статус в личном кабинете.'
                      )
                    );
                } else {
                    Yii::app()->user->setFlash(
                      'checkout',
                      Yii::t('main', 'Ваш заказ успешно оформлен! Вы можете отслеживать его статус в кабинете.')
                    );
                }
                $this->redirect('/cabinet');
            }
        }
        $this->render(
          'payment',
          array(
            'data'     => $data,
            'cart'     => $cart,
            'order_id' => $session['order_id']
          )
        );
    }

    /*
     * Расчет доставки и прочих данных в случае дозаказа
     */
    protected function reaorderCalc($oid)
    {
        $session = new CHttpSession;
        $session->open();
        $order = Order::model()->findByPk($oid);
        $address = Addresses::model()->findByPk($order->addresses_id);
        $session['addresses_id'] = $order['addresses_id'];
        $session['delivery_id'] = $order['delivery_id'];
        $session['firstname'] = $address['firstname'];
        $session['lastname'] = $address['lastname'];
        $session['country'] = $address['country'];
        $session['index'] = $address['index'];
        $session['city'] = $address['city'];
        $session['address'] = $address['address'];
        $session['phone'] = $address['phone'];
        $weight = ($session['weight'] + $order->weight);

        /*    if ($weight > 2 && $model['delivery'] == 'airmall') {
              $model['delivery'] = 'china_post';
              $order->data = CJSON::encode($model);
              $order->save();
            }
        */
        $del = Deliveries::getDelivery($weight, $session['country'], $order['delivery_id']);
        if (isset($del->summ)) {
            $curr_delivery = $del->summ;
        } else {
            $curr_delivery = $order->delivery;
        }
        $session['delivery'] = $curr_delivery - $order->delivery;
    }

    protected function save_order($data, $cart, $oid = false)
    {
        Yii::app()->db->autoCommit=false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if ($oid) {
                $order = Order::model()->findByPk($oid);
            } else {
                $order = new Order;
                $order->uid = Yii::app()->user->id;
            }
            $order->addresses_id = $data['addresses_id'];
            $order->delivery_id = $data['delivery_id'];
            $order->weight = $order->weight + $data['weight'];
            $order->delivery = $order->delivery + $data['delivery'];
            if (isset($_POST['doGo']) || isset($_POST['doNoPayment'])) {
                $order->sum = $order->sum + (float) $cart->total;
            } else {
                $order->sum = (float) $cart->total;
            }
            if ($order->sum <= 0 && $order->delivery <= 0) {
                return false;
            } else {
                $order->sum = Formulas::convertCurrency(
                  (float) $order->sum,
                  DSConfig::getCurrency(),
                  DSConfig::getVal('site_currency')
                );
            }
            $checkout_order_reconfirmation_needed = DSConfig::getVal('checkout_order_reconfirmation_needed') == 1;
            if ($checkout_order_reconfirmation_needed) {
                $order->delivery = 0;
            }
            $order->date = time();
            $user = Users::model()->findByPk(Yii::app()->user->id);
            if ($user->default_manager) {
                $manager = Users::model()->find("uid=:default_manager and status=1 and role in (
                                    select distinct role from access_rights where
                                    role in ('orderManager', 'topManager', 'superAdmin') or
                                    allow rlike '%(order|top)manager|superadmin%'
                )",
                  array(':default_manager'=>$user->default_manager));
                if ($manager) {
                        $order->manager = $user->default_manager;
                } else {
                    $manager = Users::model()->findByPk(DSConfig::getVal('checkout_default_manager_id'));
                    if ($manager) {
                        $order->manager = $manager->uid;
                    } else {
                        $order->manager = 0;
                    }
                }
            } else {
                $manager = Users::model()->findByPk(DSConfig::getVal('checkout_default_manager_id'));
                if ($manager) {
                    $order->manager = $manager->uid;
                } else {
                    $order->manager = 0;
                }
            }
            if (!$order->manager) {
                $order->manager = users::getFirstSuperAdminId();
            }
            $orderSaved = $order->save();
            if ($orderSaved) {
//--------------------------------------
                if (isset($data['comment']) && ($order->id)) {
                    if (strlen($data['comment']) > 0) {
                        OrdersComments::addOrderComment($order->id, $data['comment'], 0);
                    }
                }
//====================================================
                foreach ($cart->cartRecords as $cartRecord) {
//TODO Здесь потом при некоторых условиях проверять на повторяемость товара и увеличивать кол-во вместо нового лота
                    $orderItem = new OrdersItems();
                    $orderItem->oid = $order->id;
                    $orderItem->iid = $cartRecord->iid;
                    $orderItem->pic_url = $cartRecord->pic_url;
                    $orderItem->props = $cartRecord->input_props;
                    if (isset($cartRecord->input_props_array) && ($cartRecord->input_props_array != false)) {
                        $orderItem->input_props = json_encode($cartRecord->input_props_array);
                    } else {
                        $orderItem->input_props = json_encode(array());
                    }
                    $orderItem->num = $cartRecord->num;
                    $orderItem->weight = $cartRecord->weight;

                    $orderItem->title = Utils::getOriginalFromTranslation($cartRecord->title);
                    $orderItem->seller_nick = $cartRecord->seller_nick;
                    $orderItem->seller_id = $cartRecord->seller_id;
                    // Цена в юанях со скидкой
                    $orderItem->taobao_promotion_price = $cartRecord->TaobaoPromotion_price;
                    // Цена в юанях без скидки
                    $orderItem->taobao_price = $cartRecord->TaobaoPrice;
                    //Доставка в юанях
                    $orderItem->express_fee = $cartRecord->express_fee;
                    $orderItem->sku_id = $cartRecord->sku_id;
                    //------------------------------
                    //Сумма стоимости лота с учётом скидок и проч. во внутренней валюте
//        $orderItem->sum = Formulas::convertCurrency($cartRecord->sum, DSConfig::getCurrency(), DSConfig::getVal('site_currency'));
//        //Сумма скидок на весь лот во внутренней валюте
//        $orderItem->sum_discount = Formulas::convertCurrency($cartRecord->sumDiscount, DSConfig::getCurrency(), DSConfig::getVal('site_currency'));
                    //Сумма стоимости лота без скидок во внутренней валюте
//        $orderItem->sum_no_discount = Formulas::convertCurrency($cartRecord->sum_no_discount, DSConfig::getCurrency(), DSConfig::getVal('site_currency'));
//        //Конечная цена единицы товара (с дисконтами и проч.) во внутренней валюте
//        $orderItem->_price = Formulas::convertCurrency($cartRecord->price, DSConfig::getCurrency(), DSConfig::getVal('site_currency'));
                    //??? Почему-то ещё одна общая стоимость лота
//        $orderItem->price = Formulas::convertCurrency($cartRecord->sum, DSConfig::getCurrency(), DSConfig::getVal('site_currency'));
//        //Цена единицы товара без дисконта во внутренней валюте
//        $orderItem->price_no_discount = Formulas::convertCurrency($cartRecord->price_no_discount, DSConfig::getCurrency(), DSConfig::getVal('site_currency'));
//        //??? Сумма стоимости лота с учётом скидок и проч. во внутренней валюте
//        $orderItem->price_user_final = Formulas::convertCurrency($cartRecord->sum, DSConfig::getCurrency(), DSConfig::getVal('site_currency'));
                    //------------------------------

                    $orderItem->status = ((!$oid)?1:5);
                    $orderItem->save(false);
                    if (isset($cartRecord->desc) && ($orderItem->id)) {
                        OrdersItemsComments::addOrderItemComment($orderItem->id, $cartRecord->desc, 0);
                    }
                }
            }
//=====================================================
            $transaction->commit();
            Yii::app()->db->autoCommit=true;
            return $order->id;
        } catch (Exception $e) {
            $transaction->rollback();
            Yii::app()->db->autoCommit=true;
            return false;
        }
    }

    protected function checkCartIsEmpty($forOrder=true) {
        if (Cart::cartIsEmpty(false,$forOrder)) {
            $this->redirect('/cart/index');
        }
    }

}