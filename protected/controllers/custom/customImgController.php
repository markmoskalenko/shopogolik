<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ImgController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customImgController extends CustomFrontController {
  public function actionBarcode($code) {
    $bc = new BarcodeGenerator;
    //$height=50, $thin=2, $thick=3, $fSize=2
    $bc->init('png',50,2,3,5); //$bc->init('png',50,2,3,5);
    //$text='', $showText=false, $fileName=null
    $bc->build($code,true);
  }

  public function actionIndex($url) {
    //echo $url;die;
    //=========================================
    $hash = $url;
    $start = microtime(TRUE);
    $watermark_path = Yii::app()->theme->basePath . DSConfig::getVal('seo_img_cache_watermark');
    $command = Yii::app()->db->createCommand("select original_url from img_hashes where hash=:hash LIMIT 1")
      ->bindParam(':hash', $hash, PDO::PARAM_STR);
    $row = $command->queryRow();
    if ($row != FALSE) {
      foreach ($_SERVER as $name => $value) {
        if ($name == 'HTTP_IF_NONE_MATCH') {
          header('HTTP/1.1 304 Not Modified');

          header('Cache-Control: max-age=864000');
          header("Expires: " . gmdate("D, d M Y H:i:s", time() + 864000) . " GMT");
          $time = microtime(TRUE) - $start;
          header('Connection: ' . $time * 1000);
          header('Pragma: public');
          return;
        }
      }
      $cache = Yii::app()->fileCache->get('img-cache-' . $hash);
      //$cache = FALSE;
      if (($cache == FALSE) || (DSConfig::getVal('seo_img_cache_file_enabled') != 1)) {
        $res = Img::getData($row['original_url']);
        if (isset($res->info)) {
          $timeget = microtime(TRUE) - $start;
          //=========================================
          $command = Yii::app()->db->createCommand("update img_hashes set last_access=Now(),size=:size where hash=:hash LIMIT 1")
            ->bindParam(':hash', $hash, PDO::PARAM_STR)
            ->bindParam(':size', $res->info['size_download'], PDO::PARAM_INT);
          $command->execute();
          //=========================================
          if (($res->info['http_code'] == 200) or ($res->info['http_code'] == 206)) {
            Yii::app()->fileCache->set('img-cache-' . $hash, $res->content, 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_search'));
            $image = imagecreatefromstring($res->content);
            if (is_file($watermark_path)) {
              $image = Img::add_watermark($image, $watermark_path);
            }
            header('Content-Type: image/jpeg');
            header('Cache-Control: public, max-age=864000');
            header("Expires: " . gmdate("D, d M Y H:i:s", time() + 864000) . " GMT");
            header('Etag: ' . $hash);
            $time = microtime(TRUE) - $start;
            header('Connection: ' . ($time * 1000) . ' , ' . ($timeget * 1000));
            header('Pragma: public');

            imagejpeg($image, NULL);
            imagedestroy($image);
            //echo $res-> content;
          }
          else {
            header('HTTP/1.1 404 Not found');
          }
        }
        else {
          header('HTTP/1.1 500 Internal Server Error');
        }
      }
      else {
        $image = imagecreatefromstring($cache);
        if (is_file($watermark_path)) {
          $image = Img::add_watermark($image, $watermark_path);
        }
        header('Content-Type: image/jpeg');
        header('Cache-Control: public, max-age=864000');
        header("Expires: " . gmdate("D, d M Y H:i:s", time() + 864000) . " GMT");
        header('Etag: ' . $hash);
        $time = microtime(TRUE) - $start;
        header('Connection: ' . $time * 1000);
        header('Pragma: public');
        imagejpeg($image, NULL, 90);
      }
    }
    else {
      header('HTTP/1.1 404 Not found');
    }
  }

  public function actionCommentAttach($isItem, $id) {
    if ($isItem === '0') {
      $attach = OrdersCommentsAttaches::model()->findByPk($id);
    }
    else {
      $attach = OrdersItemsCommentsAttaches::model()->findByPk($id);
    }
    if ($attach) {
      foreach ($_SERVER as $name => $value) {
        if ($name == 'HTTP_IF_NONE_MATCH') {
          header('HTTP/1.1 304 Not Modified');

          header('Cache-Control: max-age=864000');
          header("Expires: " . gmdate("D, d M Y H:i:s", time() + 864000) . " GMT");
          header('Pragma: public');
          return;
        }
      }
      header('Content-Type: image/jpeg');
      header('Cache-Control: public, max-age=864000');
      header("Expires: " . gmdate("D, d M Y H:i:s", time() + 864000) . " GMT");
      header('Etag: ' . md5($isItem . '-' . $id));
      header('Pragma: public');
      echo $attach->attach;
    }
    else {
      header('HTTP/1.1 404 Not found');
    }
    return;
  }
}