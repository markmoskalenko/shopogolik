<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ArticleController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customArticleController extends CustomFrontController {

  public function actionIndex($url = FALSE) {
    if (!$url) {
      throw new CHttpException(404, Yii::t('main', 'Not Found'));
    }
    try {
      $article = cms::pageContent($url);
      $this->breadcrumbs = array(
        $this->pageTitle
      );
      $this->render('index', array('article' => $article));
    } catch (Exception $e) {
      throw new CHttpException(404, Yii::t('main', 'Not Found'));
    }
  }

}