<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="MessageController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customMessageController extends CustomFrontController {
  public function filters() {
    return array(
      'Rights',array('application.components.PostprocessFilter'), // perform access control for CRUD operations
    );
  }

  public function actionCreate() {
    try {
      if (isset($_POST['message'])) {
        if (isset($_POST['message']['isItem']) && (isset($_POST['message']['parentId'])) && isset($_POST['message']['message'])) {
          if (((int) $_POST['message']['isItem'] >= 0) && ((int) $_POST['message']['parentId'] > 0) && (strlen($_POST['message']['message']) > 0)) {
            if (isset($_POST['message']['public'])) {
              if ($_POST['message']['public'] == 1) {
                $internal = 0;
              }
              else {
                $internal = 1;
              }
            }
            else {
              $internal = 1;
            }
            if ((int) $_POST['message']['isItem'] == 0) {
              OrdersComments::addOrderComment((int) $_POST['message']['parentId'], $_POST['message']['message'], $internal);
            }
            else {
              OrdersItemsComments::addOrderItemComment((int) $_POST['message']['parentId'], $_POST['message']['message'], $internal);
            }
          }
          else {
            return FALSE;
          }
        }
        else {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
      return TRUE;
    } catch (Exception $e) {
      return FALSE;
    }
  }

  public function actionAddImage() {
    $isOrder=null;
//    $itemId=0;
    if (!function_exists('processAddImageResult')) {
      function processAddImageResult($error,$blockId,$message=false) {
        if ($error) {
          if (!$message) {
          echo '<script>alert("' . Yii::t('main', 'Ошибка загрузки изображения') . '")</script>';
          } else {
            echo '<script>alert("' . Yii::t('main', 'Ошибка загрузки изображения') .'"+"\n"+"'.implode('"+"\n"+"',$message). '")</script>';
          }
          Yii::app()->end();
        }
        else {
          echo '<script>alert("' . Yii::t('main', 'Изображение загружено. Обновите страницу или вкладку, чтобы его увидеть') . '");</script>';
          Yii::app()->end();
        }
      }
    }
    if (isset($_POST['OrdersCommentsAttaches'])) {
      $isOrder=true;
      $model = new OrdersCommentsAttaches();
      $model->attributes = $_POST['OrdersCommentsAttaches'];
    } elseif (isset($_POST['OrdersItemsCommentsAttaches'])) {
      $isOrder=false;
      $model = new OrdersItemsCommentsAttaches();
      $model->attributes = $_POST['OrdersItemsCommentsAttaches'];
    }
    else {
      processAddImageResult(TRUE,null);
    }
    if ($model->validate()) {
    try {
      $model->scenario = 'insert';
      $itemId=$model->comment_id;
      $file = CUploadedFile::getInstance($model, 'uploadedFile');
      if ($file && ($file->error == 0)) {
        $model->attach = file_get_contents($file->tempName);
        $model->save();
        processAddImageResult(FALSE,$_POST['blockId']);
      }
      else {
        processAddImageResult(TRUE,null);
      }
    } catch (Exception $e) {
      processAddImageResult(TRUE,null);
    }
    } else {
      processAddImageResult(TRUE,null,$model->getErrors('uploadedFile'));
    }
  }


}