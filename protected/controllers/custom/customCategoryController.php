<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CategoryController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customCategoryController extends CustomFrontController {

  public $body_class = 'category';
  public $columns = 'two-col';

  public function actionIndex($name = FALSE, $query = FALSE, $cid = 0, $page = 1, $props = FALSE,
                              $price_min = FALSE, $price_max = FALSE, $sales_min = FALSE, $sales_max = FALSE,
                              $rating_min = FALSE, $rating_max = FALSE, $sort_by = FALSE, $original = FALSE, $recommend = FALSE, $not_unique = FALSE) {
    if (!$name && $cid == 0) {
      throw new CHttpException(400, Yii::t('main', 'Неверный запрос'));
    }
    $search_query = '';
    $useVirtualCatQueriesOnly = DSConfig::getVal('search_useVirtualCatQueriesOnly') == 1;
    $useVirtualCatQueriesForOldCatalog = DSConfig::getVal('search_useVirtualCatQueriesForOldCatalog') == 1;

    if (preg_match('/^mainmenu\-.*/', $name)) {
      $model = MainMenu::model()->find('url=:url AND status!=0', array(':url' => $name));
      $virtual = 1;
      if ($useVirtualCatQueriesOnly) {
        if (isset($model->query) && (($model->query == '') || ($model->query == NULL))) {
          if (isset($model->zh)) {
            $search_query = $model->zh;
          }
        }
        else {
          if (isset($model->query)) {
            $search_query = $model->query;
          }
        }
      }
      else {
        if (isset($model->query)) {
          $search_query = $model->query;
        }
      }
    }
    else {
      $model = Category::model()->find('url=:url AND status!=0', array(':url' => $name));
      $virtual = 0;
      if ($model != FALSE) {
        if ($useVirtualCatQueriesForOldCatalog) {
          $search_query = $model->query;
        }
        else {
          $search_query = '';
        }
      }
    }
    if ($model == FALSE) {
      throw new CHttpException (400, Yii::t('main', 'Неверный запрос'));
    }

    /*        $params = array(
                'cid' => $model->cid
            );
    */

    //render all params
    if (isset($_POST['props'])) {
      $props = '';
    }
    /*
          'query' => FALSE,
          'name' => $name,
          'props' => $props,
          'cid' => $model->cid,
          'cid_query' => $search_query,
          'virtual' => $virtual,
          'price_min' => $price_min,
          'price_max' => $price_max,
          'sales_min' => $sales_min,
          'sales_max' => $sales_max,
          'rating_min' => $rating_min,
          'rating_max' => $rating_max,
          'sort_by' => $sort_by,
          'original' => $original,
          'recommend' => $recommend
     */
    $params = array(
      'query'      => $query,
      'name'       => $name,
      'props'      => $props,
      'cid'        => $model->cid,
      'cid_query'  => str_replace('/', ' ', $search_query),
      'virtual'    => $virtual,
      'price_min'  => $price_min,
      'price_max'  => $price_max,
      'sales_min'  => $sales_min,
      'sales_max'  => $sales_max,
      'rating_min' => $rating_min,
      'rating_max' => $rating_max,
      'sort_by'    => $sort_by,
      'original'   => $original,
      'recommend'  => $recommend,
      'not_unique' => $not_unique,
    );

    foreach ($params as $k => $param) {
      if (isset($_POST[$k])) {
        $params[$k] = $_POST[$k];
      }
    }
    if (is_array($params['props'])) {
      foreach ($params['props'] as $k => $v) {
        if ($v !== '') {
          $props .= $k . ':' . $v . ';';
        }
      }
      $params['props'] = $props;
    }


    $search = new Search;
    $search->params = $params;
    $search->page = $page;
    $search->cid_model = $model;

    $res = $search->execute();

    $this->breadcrumbs = $search->breadcrumbs();

    foreach ($params as $k => $v) {
      if (!(bool) $v) {
        unset($params[$k]);
      }
    }
    $orig_params = $params;
    unset($orig_params['original']);
    unset($orig_params['not_unique']);

    $lang = Utils::TransLang();
    //СЕО настройки
    $this->pageTitle = (!empty($model->{'page_title_' . $lang})) ? $model->{'page_title_' . $lang} : $model->{$lang};

    $this->meta_desc = (!empty($model->{'meta_desc_' . $lang})) ? $model->{'meta_desc_' . $lang}
      : Yii::t('main', 'Широкий ассортимент, {category} в нашем магазине', array('{category}' => $model->{$lang}));

    $this->meta_keyword = (!empty($model->{'meta_keeword_' . $lang})) ? $model->{'meta_keeword_' . $lang} : $model->{$lang};
    //end

    //Pagination
    if (isset($res->total_results)) {
      $pages = new CPagination($res->total_results);
      $pages->pageSize = Search::getPageSize();
      $pages->currentPage = $page - 1;
      $pages->params = $params;
    }
    else {
      $pages = FALSE;
    }

    $this->params['params'] = $params;
    $this->params['cids'] = $res->cids;
    $this->params['bids'] = $res->bids;
    $this->params['groups'] = $res->groups;
    $this->params['filters'] = $res->filters;
    $this->params['multiFilters'] = $res->multiFilters;
    $this->params['suggestions'] = $res->suggestions;
    $this->params['priceRange'] = $res->priceRange;
//      var_dump($res);exit
    $this->render('/search/index', array(
      'res'         => $res,
      'params'      => $params,
      'sort_by'     => $sort_by,
      'pages'       => $pages,
      'orig_params' => $orig_params,
      'category'    => $model,
    ));
  }

  function actionPage($page_id) {
    $mainMenu = MainMenu::getMainMenu(0, Utils::TransLang(), (int) $page_id);
    $mainMenuRoot = MainMenu::getMainMenuRecord(Utils::TransLang(), (int) $page_id);
    if (count($mainMenuRoot) > 0) {
      $mainMenuRoot[(int) $page_id]['children'] = $mainMenu;

      $this->pageTitle = $mainMenuRoot[(int) $page_id]['view_text'];
      $this->breadcrumbs = array(
        $this->pageTitle
      );
    }
    $this->params['params'] = FALSE;
    $this->params['cids'] = array();
    $this->params['bids'] = array();
    $this->params['filters'] = array();
    $this->params['multiFilters'] = array();
    $this->params['suggestions'] = array();
    $this->params['priceRange'] = array();

    $this->render('page', array('links' => $mainMenuRoot, 'lang' => Utils::TransLang()));
  }

  function actionList() {
    $this->pageTitle = Yii::t('main', 'Все категории');
    $this->breadcrumbs = array(
      $this->pageTitle
    );

    $lang = Utils::TransLang();
    $cache = Yii::app()->cache->get('MainMenu-getList-' . $lang);
    if (($cache == FALSE) || (DSConfig::getVal('search_cache_enabled') != 1)) {
      $mainMenu = MainMenu::getMainMenu(0, $lang,1,array(2,3));
      Yii::app()->cache->set('MainMenu-getList-' . $lang, array($mainMenu), 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other'));
    }
    else {
      list($mainMenu) = $cache;
    }

    $this->params['params'] = FALSE;
    $this->params['cids'] = array();
    $this->params['bids'] = array();
    $this->params['groups'] = array();
    $this->params['filters'] = array();
    $this->params['multiFilters'] = array();
    $this->params['suggestions'] = array();
    $this->params['priceRange'] = array();
    $this->render('list', array('categories' => $mainMenu));
  }

  function actionMenu($lang='none') {
//        $this->renderPartial('menu', array(), FALSE, TRUE);
//      Yii::app()->end();
    if ($lang=='none') {
      $_lang=Utils::TransLang();
    } else {
      $_lang=$lang;
    }
    $ttl = 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other');

      $cache = false; //Yii::app()->cache->get('MainMenu-getTree-rendering-' . $_lang);
    if (!$cache || (count($cache)!=3)) {
      $res = $this->renderPartial('menu', array('lang'=>$_lang), TRUE, FALSE);
      $Etag = md5(serialize($res).$_lang);
      $last_modified_time=time();
        Yii::app()->cache->set('MainMenu-getTree-rendering-' . $_lang, array($res,$last_modified_time,$Etag),$ttl);
    } else {
        try {
        list($res,$last_modified_time,$Etag)=$cache;
            if (!is_long($last_modified_time)) {
            Yii::app()->cache->delete('MainMenu-getTree-rendering-' . $_lang);
            }
        } catch (Exception $e) {
            Yii::app()->cache->delete('MainMenu-getTree-rendering-' . $_lang);
        }
    }
      header('Content-Type: text/plain');
      header('Cache-Control: public, max-age=' . $ttl);
      header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
      header('Etag: ' . $Etag);
    if ((isset($_SERVER['HTTP_IF_NONE_MATCH'])&&(trim($_SERVER['HTTP_IF_NONE_MATCH'])==$Etag))&&
      (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])&&(@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time))) {
      header('HTTP/1.1 304 Not Modified');
      Yii::app()->end();
    }
      echo $res;
    Yii::app()->end();
  }
}