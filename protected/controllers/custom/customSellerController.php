<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SellerController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customSellerController extends CustomFrontController {

  function actionIndex($nick = FALSE, $cid = 0, $page = 1, $price_min = FALSE,
                       $price_max = FALSE, $sales_min = FALSE, $sales_max = FALSE, $sort_by = FALSE, $original = FALSE, $not_unique = FALSE) {
    if (!(bool) $nick) {
      throw new CHttpException(404, Yii::t('main', 'Неверный запрос'));
    }
    if ($page > 100) {
      Yii::app()->user->setFlash('error', Yii::t('main', 'Уточните критерии поиска для получения качественного результата!'));
      $page = 99;
    }
      if (DSConfig::getVal('search_use_fulltext_for_seller')!=1) {
          if (($nick) && (!preg_match('/^[@]*\d+$/', $nick))) {
              $user_id = ItemSeller::extNickToUserID($nick);
          } else {
              $user_id = preg_replace('/^[@]*/','',$nick);
          }
      } else {
          $user_id = $nick;
      }
      //
    $params = array(
      'query' => FALSE,
      'rating_min' => FALSE,
      'rating_max' => FALSE,
      'cid' => $cid,
      'price_min' => $price_min,
      'price_max' => $price_max,
      'sales_min' => $sales_min,
      'sales_max' => $sales_max,
      'sort_by' => $sort_by,
      'nick' => $user_id,
      'original' => $original,
      'not_unique' => $not_unique,
    );
    foreach ($params as $k => $param) {
      if (isset($_POST[$k])) {
        $params[$k] = $_POST[$k];
      }
    }

    $search = new Search;
    $search->params = $params;
    $search->page = $page;

    $res = $search->execute();

    foreach ($params as $k => $v) {
      if (!(bool) $v) {
        unset($params[$k]);
      }
    }
    $orig_params = $params;
    unset($orig_params['original']);
    unset($orig_params['not_unique']);

    $this->params['params'] = $params;
    $this->params['cids'] = $res->cids;
    $this->params['bids'] = $res->bids;
    $this->params['groups'] = $res->groups;
    $this->params['filters'] = $res->filters;
    $this->params['multiFilters'] = $res->multiFilters;
    $this->params['suggestions'] = $res->suggestions;
    $this->params['priceRange'] = $res->priceRange;
    //Pagination
    if (isset($res->total_results)) {
      $pages = new CPagination($res->total_results);
      $pages->pageSize = Search::getPageSize();
      $pages->currentPage = $page - 1;
      $pages->params = $params;
    }
    else {
      $pages = FALSE;
    }


    $this->pageTitle = Yii::t('main', 'Продавец {nick}', array('{nick}' => $nick));

    $this->render('/search/index', array(
      'res' => $res,
      'params' => $params,
      'sort_by' => $sort_by,
      'pages' => $pages,
      'orig_params' => $orig_params,
    ));
  }

}