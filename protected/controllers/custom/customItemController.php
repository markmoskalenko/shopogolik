<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="ItemController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/*
 * Контроллер для работы с товаром.
 */

class customItemController extends CustomFrontController {
  public $defaultAction = 'index';
  public $body_class = 'item';
  public $columns = 'two-col';
  public $item = FALSE;

  public function actionIndex($iid = FALSE, $exportType = FALSE) {
    //throw new CHttpException(500,'TEST');
    if (!$iid) {
      throw new CHttpException(400, Yii::t('main', 'Неверный запрос'));
    }
    elseif (preg_match('/[^\d]/', $iid)) {
      $this->render('item_not_found', array());
      return FALSE;
    }
    $this->item = new Item($iid, FALSE);
    if (!isset($this->item->taobao_item) || !isset($this->item->taobao_item->num_iid) || ($this->item->taobao_item->num_iid == 0)) {
      $this->render('item_not_found', array());
      return FALSE;
    }
    $lang = Utils::TransLang();
    $ajax = array(
      'input'         => !$this->item->inputPropsReady(),
      'seller'        => FALSE,
      'sellerRelated' => FALSE,
//      'itemRelated'=>FALSE,
    );
    if (isset($this->item->top_item->seller)) {
      $seller = $this->item->top_item->seller;
    }
    else {
      //Получаем данные о продавце
      $sellerCache = Yii::app()->fileCache->get('user-' . $this->item->top_item->nick);
      if ($sellerCache == FALSE) {
        $ajax['seller'] = TRUE;
        $seller = array();
      }
      else {
        list($seller) = $sellerCache;
      }
    }
    //Получаем данные о товарах продавца
    $sellerRelatedCache = Yii::app()->fileCache->get('item-sellerRelated-' . $this->item->top_item->nick);
    if ($sellerRelatedCache == FALSE) {
      $ajax['sellerRelated'] = TRUE;
      $sellerRelated = array();
    }
    else {
      list($sellerRelated) = $sellerRelatedCache;
    }

    //Получаем данные о похожих товарах
    if (isset($this->item->top_item->fromDSG)) {
      $itemRelated = ItemRelated::getItemRelated($this->item);
    }
    else {
      $itemRelated = array();
    }

    //---------comments
    /*    $comments = Comment::model()->findAll('iid=:iid', array(':iid' => $iid));
        if ($comments == FALSE) {
          $comments = array();
        }
        foreach ($comments as $k => $comment) {
          $author = Users::model()->findByPk($comment->uid);
          $comments[$k]->uid = $author->firstname . ' ' . $author->lastname;
        }
    */
    SiteLog::doItemLog($this->item);
    if (!$exportType) {
      $this->render('index',
        array(
          'item'          => $this->item,
          'input_props'   => $this->item->top_item->input_props,
          'props'         => $this->item->top_item->props,
          'seller'        => $seller,
          'sellerRelated' => $sellerRelated,
          'itemRelated'   => $itemRelated,
          'ajax'          => $ajax,
          'lang'          => $lang,
//        'comments' => $comments,
        ));
    }
    else {
      if ($exportType == 'XML') {
//====================================
        $item2xml = $this->item->taobao_item;
        header('Content-Type: application/xml');
        $filename = 'DSGItem-' . $iid . '.xml';
        header('Content-Disposition: attachment; charset=UTF-8; filename="' . $filename . '"');
        require_once 'XML/Serializer.php';
        $options = array(
          XML_SERIALIZER_OPTION_INDENT               => '  ',
          XML_SERIALIZER_OPTION_LINEBREAKS           => "\n",
//        XML_SERIALIZER_OPTION_DEFAULT_TAG          => 'unnamedItem',
          XML_SERIALIZER_OPTION_SCALAR_AS_ATTRIBUTES => FALSE,
          XML_SERIALIZER_OPTION_ATTRIBUTES_KEY       => '_attributes',
          XML_SERIALIZER_OPTION_CONTENT_KEY          => '_content',
          'addDecl'                                  => TRUE,
          'encoding'                                 => 'utf-8',
          'rootName'                                 => 'DSGItem',
          'mode'                                     => 'simplexml'
        );
        $serializer = new XML_Serializer($options);
        $result = $serializer->serialize($item2xml);
        if ($result === TRUE) {
          $xml = $serializer->getSerializedData();
          echo $xml;
        }
        else {
          print_r($result);
        }
        Yii::app()->end();
      }
      elseif ($exportType == 'CSV') {
//====================================
        $item2csv = $this->item->taobao_item;
        $result = array(
          'num_iid'                    => array(),
          'seller_id'                  => array(),
          'shop_id'                    => array(),
          'price'                      => array(),
          'promotion_price'            => array(),
          'delivery'                   => array(),
          'num'                        => array(),
          'title'                      => array(),
          'desc'                       => array(),
          'pic_url'                    => array(),
          'item_imgs'                  => array(), // список img
          'item_attributes'            => array(), //список ul-li
          'skus->sku->sku_id'          => array(),
          'skus->sku->quantity'        => array(),
          'skus->sku->price'           => array(),
          'skus->sku->promotion_price' => array(),
          'skus->sku->properties'      => array(),
        );
        $result['num_iid'][] = $item2csv->num_iid;
        $result['seller_id'][] = $item2csv->seller_id;
        $result['shop_id'][] = $item2csv->shop_id;
        $result['price'][] = $item2csv->price;
        $result['promotion_price'][] = $item2csv->promotion_price;
        $result['delivery'][] = $item2csv->express_fee;
        $result['num'][] = $item2csv->num;
        $result['title'][] = Yii::app()->DanVitTranslator->translateLocal($item2csv->title);
        $result['desc'][] = '';
        $result['pic_url'][] = $item2csv->pic_url;
        $item_imgs = '';
        if (is_array($item2csv->item_imgs->item_img) && (count($item2csv->item_imgs->item_img) > 0)) {
          foreach ($item2csv->item_imgs->item_img as $item_img) {
            $item_imgs = $item_imgs . '<img src="' . $item_img->url . '"/>';
          }
        }
        $result['item_imgs'][] = $item_imgs;
        $item_attributes = '<dl>';
        if (is_array($item2csv->item_attributes) && (count($item2csv->item_attributes) > 0)) {
          foreach ($item2csv->item_attributes as $attr) {
            $item_attributes = $item_attributes . '<dt>' . Yii::app()->DanVitTranslator->translateLocal($attr->prop) .
              '</dt><dd>' . Yii::app()->DanVitTranslator->translateLocal($attr->val) . '</dd>';
          }
        }
        $result['item_attributes'][] = $item_attributes . '</dl>';

        if (isset($item2csv->skus->sku[0])) {
          $result['skus->sku->sku_id'][] = $item2csv->skus->sku[0]->sku_id;
          $result['skus->sku->quantity'][] = $item2csv->skus->sku[0]->quantity;
          $result['skus->sku->price'][] = $item2csv->skus->sku[0]->price;
          $result['skus->sku->promotion_price'][] = $item2csv->skus->sku[0]->promotion_price;
//======================
          $properties = '<dl>';
          $propsArray = explode(';', $item2csv->skus->sku[0]->properties);
          if (is_array($propsArray) && (count($propsArray) > 0)) {
            foreach ($propsArray as $attr) {
              unset($pv);
              unset($prop);
              unset ($val);
              $pv = explode(':', $attr);
              if (isset($pv[0]) && isset($pv[1])) {
                if (isset($item2csv->props[$pv[0]])) {
                  if (isset($item2csv->props[$pv[0]]->childs)) {
                    foreach ($item2csv->props[$pv[0]]->childs as $child) {
                      if ($child->vid == $pv[1]) {
                        $prop = $item2csv->props[$pv[0]]->name;
                        $val = $child->name;
                      }
                    }
                  }

                }
                if (isset($prop) && isset($val)) {
                  $properties = $properties . '<dt id="' . $pv[0] . '">' . Yii::app()->DanVitTranslator->translateLocal($prop) .
                    '</dt><dd id="' . $pv[1] . '">' . Yii::app()->DanVitTranslator->translateLocal($val) . '</dd>';
                }
              }
            }
          }
          $result['skus->sku->properties'][] = $properties . '</dl>';
//======================
        }
        else {
          $result['skus->sku->sku_id'][] = '';
          $result['skus->sku->quantity'][] = '';
          $result['skus->sku->price'][] = '';
          $result['skus->sku->promotion_price'][] = '';
          $result['skus->sku->properties'][] = '';
        }
        foreach ($item2csv->skus->sku as $i_sku => $sku) {
          if ($i_sku == 0) {
            continue;
          }
          $result['num_iid'][] = $item2csv->num_iid;
          $result['seller_id'][] = '';
          $result['shop_id'][] = '';
          $result['price'][] = '';
          $result['promotion_price'][] = '';
          $result['delivery'][] = '';
          $result['num'][] = '';
          $result['title'][] = '';
          $result['desc'][] = '';
          $result['pic_url'][] = '';
          $result['item_imgs'][] = '';
          $result['item_attributes'][] = '';
          $result['skus->sku->sku_id'][] = $sku->sku_id;
          $result['skus->sku->quantity'][] = $sku->quantity;
          $result['skus->sku->price'][] = $sku->price;
          $result['skus->sku->promotion_price'][] = $sku->promotion_price;
//======================
          $properties = '<dl>';
          $propsArray = explode(';', $sku->properties);
          if (is_array($propsArray) && (count($propsArray) > 0)) {
            foreach ($propsArray as $attr) {
              unset($pv);
              unset($prop);
              unset ($val);
              $pv = explode(':', $attr);
              if (isset($pv[0]) && isset($pv[1])) {
                if (isset($item2csv->props[$pv[0]])) {
                  if (isset($item2csv->props[$pv[0]]->childs)) {
                    foreach ($item2csv->props[$pv[0]]->childs as $child) {
                      if ($child->vid == $pv[1]) {
                        $prop = $item2csv->props[$pv[0]]->name;
                        $val = $child->name;
                      }
                    }
                  }

                }
                if (isset($prop) && isset($val)) {
                  $properties = $properties . '<dt id="' . $pv[0] . '">' . Yii::app()->DanVitTranslator->translateLocal($prop) .
                    '</dt><dd id="' . $pv[1] . '">' . Yii::app()->DanVitTranslator->translateLocal($val) . '</dd>';
                }
              }
            }
          }
          $result['skus->sku->properties'][] = $properties . '</dl>';
//======================
        }
        $csvarray = array();
        foreach ($result as $i_res => $res) {
          if (isset($csvarray[0])) {
            $csvarray[0] = $csvarray[0] . '"' . $i_res . '";';
          }
          else {
            $csvarray[0] = '"' . $i_res . '";';
          }
          foreach ($res as $i_str => $str) {
            if (isset($csvarray[1 + $i_str])) {
              $csvarray[1 + $i_str] = $csvarray[1 + $i_str] . '"' . $str . '";';
            }
            else {
              $csvarray[1 + $i_str] = '"' . $str . '";';
            }
          }
        }
//=================
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        $filename = 'DSGItem-' . $iid . '.csv';
        header('Content-Disposition: attachment; filename="' . $filename . '"'); //charset=UTF-8;
        //$csv="\xEF\xBB\xBF";
        $csv = '';
        foreach ($csvarray as $csvstr) {
          $csv = $csv . $csvstr . "\r\n";
        }
        echo $csv; //chr(255).chr(254).
        //.mb_convert_encoding($csvstr, 'UTF-16LE', 'UTF-8')
        Yii::app()->end();
      }
//====================================
    }
    return;
  }

  function actionGetSKU($iid, $params = FALSE, $count=1) {
    //$this->actionGetInputProps($iid,false);
    try {
      if (!$params || !$iid) {
        Yii::app()->end();
      }
      $params = substr($params, 0, -1);
//    $item = new Item($iid,FALSE);
      $return = Item::getSKU($iid, $params,$count);
      echo CJSON::encode($return);
      Yii::app()->end();
    } catch (Exception $e) {
      print_r($e);
    }
  }

  function actionGetInputProps($iid = FALSE, $render = TRUE) {
    header('Access-Control-Allow-Origin: *');
    if (!$iid) {
      //$this->renderPartial('item_not_found', array(),FALSE, TRUE);
      return FALSE;
//      throw new CHttpException(400, Yii::t('main', 'Неверный запрос'));
    }
//        else
//            $iid = (int)$iid;

    $lang = Utils::TransLang();
    if ($this->item == FALSE) {
      $item = new Item($iid, TRUE);
    }
    else {
      $item = $this->item;
    }
    if (!isset($item->top_item)) {
      $this->render('item_not_found', array());
      return FALSE;
    }
    if ($render) {
      echo $this->renderPartial('input_props', array(
        'totalCount'  => $item->top_item->num,
        'input_props' => $item->top_item->input_props,
        'props'       => $item->top_item->props,
        'lang'        => $lang
      ));
      Yii::app()->end();
    }
    return TRUE;
  }

  function actionSellerRelatedBlock($nick, $userid, $iid) {
    header('Access-Control-Allow-Origin: *');
    if (empty($nick) || (strlen($nick) <= 0) || empty($userid)) {
      Yii::app()->end();
    }
    $searchRes = ItemRelated::getSellerRelated($nick, $userid, $iid);
    echo $this->renderPartial('sellerRelatedBlock', array('sellerRelated' => $searchRes));
    Yii::app()->end();
  }

  function actionGetUserPrice($iid, $price = FALSE, $count = FALSE, $delivery = FALSE) {
    header('Access-Control-Allow-Origin: *');
    if (!$price || !$delivery) {
      $item = Item::getPriceAndDeliveryFromCache($iid);
      if (!$item) {
        $item = new Item($iid, FALSE);
      }
      if (!$price) {
        $price = $item->taobao_item->price;
      }
      if (!$delivery) {
        $delivery = $item->taobao_item->express_fee;
      }
    }
    if ((!$delivery) || ($delivery == 0)) {
      $delivery = 0;// DSConfig::getVal('delivery_default_chinese_fee_cny');
    }

    if (!$count) {
      $count = 1;
    }
      //ini_set('precision', 20);
    $resUserPrice = Formulas::getUserPrice(
      array(
        'price'       => $price,
        'count'       => $count,
        'deliveryFee' => $delivery,
        'postageId'   => (isset($item)) ? $item->taobao_item->postage_id : 0,
        'sellerNick'  => (isset($item)) ? $item->taobao_item->nick : FALSE,
      ));
    $html = 'x&nbsp;<span id="count-price" title="'.Yii::t('main','Цена одного товара с предварительной доставкой').'">' .
      Formulas::priceWrapper(($resUserPrice->price+$resUserPrice->discount)/$count,false,2).'</span>'
      .(($resUserPrice->discount!=0)?'&dash; <span title="'.Yii::t('main','Экономия').'">'.
      Formulas::priceWrapper($resUserPrice->discount) . '</span>':'').' =' .
//          $item->priceWrapper(($userPrice-$item->convertCurrency($postFee,'cny',$item->getCurrency()))/$count) .'&nbsp;+&nbsp;'.
//          $item->priceWrapper($postFee,'cny').'<sup>*</sup></span> =
      '<span id="sum" title="'.Yii::t('main','Итоговая цена за указанное количество').'">' . Formulas::priceWrapper($resUserPrice->price) . '</span>';
    echo $html;
    Yii::app()->end();
  }

  public function actionDetail($iid = FALSE, $url = FALSE, $rawHTML = 0) {
    header('Access-Control-Allow-Origin: *');
    if ($url) {
        if (DSConfig::getVal('item_description_show_as_html')!=1) {
            $src = (string) Item::getDescriptionFromUrl($url);
            $src=preg_replace("/href\s*=\s*[\"'].*?[\"']/is",'href="#"',$src);
            if ($rawHTML != 0) {
                echo $src;
                return;
            } else {
                echo $this->renderPartial('item_details', array('src' => $src), true, false);
                return;
            }
        } else {
            $src = (string) Item::getDescriptionFromUrl($url);
            $src=preg_replace('/(^\s*var\s+desc\s*=\s*\')|(\'\s*;\s*$)/is','',$src);
            $src=preg_replace("/href\s*=\s*[\"'].*?[\"']/is",'href="javascript:void(0)"',$src);
            $src=preg_replace("/[\"']_blank[\"']/i",'"_self"',$src);
            echo $src;
            //echo '<iframe src="http://translate.google.com/translate?hl=bg&ie=UTF-8&u='.$url.'&sl=zh&tl=ru" "0"/>';
            return;
        }
    }
    else {
      echo FALSE;
      Yii::app()->end();
      return;
    }
    Yii::app()->end();
  }

  function actionGetUserPriceJson($iid, $price = FALSE, $count = FALSE, $delivery = FALSE) {
    header('Access-Control-Allow-Origin: *');
    if (!$price || !$delivery) {
      $item = Item::getPriceAndDeliveryFromCache($iid);
      if (!$item) {
        $item = new Item($iid, FALSE);
      }
      if (!$price) {
        $price = $item->taobao_item->price;
      }
      if (!$delivery) {
        $delivery = $item->taobao_item->express_fee;
      }
    }
    if ((!$delivery) || ($delivery == 0)) {
      $delivery = 0;// DSConfig::getVal('delivery_default_chinese_fee_cny');
    }

    if (!$count) {
      $count = 1;
    }
    $resUserPrice = Formulas::getUserPrice(
      array(
        'price'       => $price,
        'count'       => $count,
        'deliveryFee' => $delivery,
        'postageId'   => (isset($item)) ? $item->taobao_item->postage_id : 0,
        'sellerNick'  => (isset($item)) ? $item->taobao_item->nick : FALSE,
      ));
/*    $html = 'x&nbsp;<span id="count-price">' .
      Formulas::priceWrapper($result->price / $count) . '</span> =' .
      '<span id="sum">' . Formulas::priceWrapper($result->price) . '</span>';
*/
    $return = array(
      'price' => Formulas::priceWrapper($resUserPrice->price / $count),
      'sum'   => Formulas::priceWrapper($resUserPrice->price)
    );

    echo CJSON::encode($return);
  }
}