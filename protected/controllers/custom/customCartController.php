<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CartController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customCartController extends CustomFrontController {
  public function actionIndex() {
    $this->pageTitle = Yii::t('main', 'Корзина');
    $this->breadcrumbs = array(
      $this->pageTitle
    );

    $cart = Cart::getUserCart();
    $this->render('index', array(
      'userCart' => $cart,
    ));
  }

  public function actionAdd($iid=false) {
      if (Yii::app()->request->isAjaxRequest) {
          if ($iid==false) {
              echo Yii::t('main', 'Произошла ошибка! Попробуйте еще раз.');
              return false;
          } else {
//adding item to cart and optimize cart items
              $result = Cart::addItemToCart(0, $iid, 1, false, '');
              if ($result) {
                    echo Yii::t('main', 'Товар успешно добавлен в корзину');
                  return true;
              } else {
                   echo Yii::t('main', 'Произошла ошибка! Попробуйте еще раз.');
                  return false;
              }
          }

      } else {
          $iid = (isset($_POST['iid'])) ? $_POST['iid'] : false;
          $cid = (isset($_POST['cid'])) ? $_POST['cid'] : 0;
          $input_props = (isset($_POST['inputprops-processed'])) ? $_POST['inputprops-processed'] : false;
          if ($input_props != false) {
              $input_props_array = array();
              $iprops = explode(';', $input_props);
              foreach ($iprops as $iprop) {
                  if ($iprop != '') {
                      $ipropval = explode(':', $iprop);
                      $input_props_array[$ipropval[0]] = $ipropval[0] . ':' . $ipropval[1];
                  }
              }
          } else {
              $input_props_array = false;
          }
          $num = (isset($_POST['num'])) ? abs($_POST['num']) : 1;
          $descr = (isset($_POST['cart-description'])) ? $_POST['cart-description'] : '';

          if ($iid == false || $num == false) {
              throw new CHttpException(404, 'Not Found');
          }
          if (!($_POST['item_totalcount'] > 0)) {
              Yii::app()->user->setFlash(
                'cart-error',
                Yii::t('main', 'Товара нет в наличии или не выбраны свойства товара (цвет, размер)!')
              );
              $this->redirect($this->createUrl('item/index', array('iid' => $iid)));
          }
          if (isset($input_props_array) && (is_array($input_props_array))) {
              foreach ($input_props_array as $pid => $vid) {
                  if ($vid == '0') {
                      Yii::app()->user->setFlash(
                        'cart-error',
                        Yii::t('main', 'Не выбраны параметры товара!')
                      );
                      $this->redirect($this->createUrl('item/index', array('iid' => $iid)));
                  }
              }
          }
          //adding item to cart and optimize cart items
          $result = Cart::addItemToCart($cid, $iid, $num, $input_props_array, $descr);
          if ($result) {
              Yii::app()->user->setFlash(
                'cart',
                Yii::t('main', 'Товар успешно добавлен в корзину
            <br><br>
            <a href="/cart/index" class="btn btn-default btn-add-to-cart"/>Перейти в корзину</a>')
            );
          } else {
              Yii::app()->user->setFlash(
                'cart',
                Yii::t('main', 'Произошла ошибка! Попробуйте еще раз.')
              );
          }
          $this->redirect(Yii::app()->request->urlReferrer);
      }
  }

  public function actionDelete($id = FALSE) {
    if ($id === FALSE) {
      throw new CHttpException(404, 'Not Found');
    }
    Cart::deleteUserCart($id);
    $this->redirect('/cart/index');
  }

  public function actionDeleteAll() {
    Cart::deleteUserCart(false,true);
    $this->redirect('/cart/index');

  }

  public function actionSave() {
    $cart = Cart::getUserCart();
    if (!isset($_POST['weight']) || !isset($_POST['params']) || !isset($_POST['iid']) || !isset($_POST['num']) || !isset($_POST['description']) || (count($cart->cartRecords) <= 0)) {
      $this->redirect('/cart');
    }
    else {
      $description = $_POST['description'];
      $num = $_POST['num'];
      $weight = $_POST['weight'];
      $selectedProps=(isset($_POST['selectedProps'])?$_POST['selectedProps']:false);
      $store=(isset($_POST['store'])?$_POST['store']:false);
      $order=(isset($_POST['order'])?$_POST['order']:true);
        Yii::app()->db->autoCommit=false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($cart->cartRecords as $item) {
                if (isset($num[$item->id]) && $num[$item->id] && isset($description[$item->id])) {
                        $item->num = round($num[$item->id]);
                        $item->desc = $description[$item->id];
                        $item->weight = abs($weight[$item->id]);
                    if ($selectedProps && isset($selectedProps[$item->id])) {
                        $item->input_props=implode(';',$selectedProps[$item->id]);
                    }
                    if ($store && isset($store[$item->id])) {
                        $item->store=1;
                    } else {
                        $item->store=0;
                    }
                    if ($order && isset($order[$item->id])) {
                        $item->order=0;
                    }
                } else {
                    $item->store=1;
                    $item->order=0;
                }
                if (preg_match('/(?:^|;)\d+:0(?:$|;)/',$item->input_props)) {
                    $item->order=0;
                }
                $item->update();
            }
        } catch (Exception $e) {
            $transaction->rollback();
            Yii::app()->db->autoCommit=true;
            $this->redirect('/cart/index');			
        }
        $transaction->commit();
        Yii::app()->db->autoCommit=true;
    }
      Cart::clearCartCache(false);
      if (isset($_POST['Checkout'])) {
      $session = new CHttpSession;
      $session->open();
      if (isset($_POST['comment'])) {
        $session['comment'] = $_POST['comment'];
      }
      else {
        $session['comment'] = '';
      }
      $this->redirect('/checkout');
    }
    $this->redirect('/cart/index');
  }
}