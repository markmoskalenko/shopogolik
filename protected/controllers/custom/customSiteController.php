<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SiteController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class customSiteController extends CustomFrontController
{
    public function actionSitemap()
    {
//--------------------------------------
        header('Content-Type: application/xml');

        $lang = Utils::AppLang();
        $cache = Yii::app()->cache->get('sitemap-' . $lang);
        if (($cache == false) || (DSConfig::getVal('search_cache_enabled') != 1)) {
            $xml = '<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' . ' http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            $xml = $xml . "\n" . DSConfig::getVal('seo_sitemap_static');
            $seo_sitemap_patern = DSConfig::getVal('seo_sitemap_patern');
            /*
             <url>
               <loc>$URL</loc>
               <lastmod>$LASTMOD</lastmod>
               <changefreq>$FREQ</changefreq>
               <priority>$PRIORITY</priority>
            </url>
             */
//-- Обрабатываем статические страницы
            $command = Yii::app()->db->createCommand(
              "select distinct url from cms_pages where url is not null and enabled=1 and SEO=1"
            );
//        ->bindParam(':text', $textEn, PDO::PARAM_STR);
            $rows = $command->queryAll();
            foreach ($rows as $row) {
                $res = "\n" . $seo_sitemap_patern . "\n";
                $res = str_replace('$URL', Yii::app()->createAbsoluteUrl('/' . $row['url']), $res);
                $res = str_replace(
                  '$LASTMOD',
                  date(DATE_W3C, floor((time() - (3600 * 24 * 30)) / (3600 * 24 * 30)) * (3600 * 24 * 30)),
                  $res
                ); //),$res);
                $res = str_replace('$FREQ', 'monthly', $res);
                $res = str_replace('$PRIORITY', '0.8', $res);
                $xml = $xml . $res;
            }
//-- Обрабатываем меню
            $command = Yii::app()->db->createCommand("select distinct url from categories_ext where status=1");
//        ->bindParam(':text', $textEn, PDO::PARAM_STR);
            $rows = $command->queryAll();
            foreach ($rows as $row) {
                $res = "\n" . $seo_sitemap_patern . "\n";
                $res = str_replace('$URL', Yii::app()->createAbsoluteUrl('/category/' . $row['url']), $res);
                $res = str_replace(
                  '$LASTMOD',
                  date(DATE_W3C, floor((time() - (3600 * 24 * 7)) / (3600 * 24 * 7)) * (3600 * 24 * 7)),
                  $res
                ); //),$res);
                $res = str_replace('$FREQ', 'weekly', $res);
                $res = str_replace('$PRIORITY', '0.64', $res);
                $xml = $xml . $res;
            }
//-- Обрабатываем брэнды
            $command = Yii::app()->db->createCommand("select distinct url from brands cc where cc.enabled=1");
//        ->bindParam(':text', $textEn, PDO::PARAM_STR);
            $rows = $command->queryAll();
            foreach ($rows as $row) {
                $res = "\n" . $seo_sitemap_patern . "\n";
                $res = str_replace('$URL', Yii::app()->createAbsoluteUrl('/brand/' . $row['url']), $res);
                $res = str_replace(
                  '$LASTMOD',
                  date(DATE_W3C, floor((time() - (3600 * 24 * 15)) / (3600 * 24 * 15)) * (3600 * 24 * 15)),
                  $res
                ); //),$res);
                $res = str_replace('$FREQ', 'weekly', $res);
                $res = str_replace('$PRIORITY', '0.7', $res);
                $xml = $xml . $res;
            }
            $xml = $xml . "\n" . '</urlset>';
            Yii::app()->cache->set(
              'sitemap-' . $lang,
              array($xml),
              60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other')
            );
        } else {
            list($xml) = $cache;
        }
        echo $xml;
//--------------------------------------
    }

    public function actionTranslate($type = '', $id = 0)
    { //, $zh = FALSE, $en = FALSE, $ru = FALSE
        // plain cid cid_ext pid vid suggestion
        header('Access-Control-Allow-Origin: *');
        /*    if (!in_array(Yii::app()->user->getRole(),array('guest','user'))) {
              Yii::app()->end();
            }
        */
        $res = new stdClass();
        if (($type != '') && ($id != 0) && (count($_POST) <= 0)) {
            $res->type = $type;
            $res->id = $id;
            if ($res->type == 'plain') {
                $command = Yii::app()->db->createCommand(
                  "select dd.ID, dd.SRC_LANG,dd.FREQ,dd.TRANSLATION_STATE,dd.TEXT_ZH,
  dd.TEXT_RU,dd.TEXT_EN,dd.uid,dd.`date` from cn_dic dd
  -- left join users uu on uu.uid=dd.uid
  where dd.id=:id limit 1"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT);
                $row = $command->queryRow();
                if (isset($row['TEXT_ZH'])) {
                    $res->zh = $row['TEXT_ZH'];
                    $res->en = $row['TEXT_EN'];
                    $res->ru = $row['TEXT_RU'];
                } else {
                    $res->zh = '';
                    $res->en = '';
                    $res->ru = '';
                }
            } elseif ($res->type == 'cid') {
                $command = Yii::app()->db->createCommand(
                  "select id,ru,en,zh from categories
  where id=:id limit 1"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT);
                $row = $command->queryRow();
                if (isset($row['id'])) {
                    $res->zh = $row['zh'];
                    $res->en = $row['en'];
                    $res->ru = $row['ru'];
                } else {
                    $res->zh = '';
                    $res->en = '';
                    $res->ru = '';
                }
            } elseif ($res->type == 'cid_ext') {
                $command = Yii::app()->db->createCommand(
                  "select id,ru,en,zh from categories_ext
  where id=:id limit 1"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT);
                $row = $command->queryRow();
                if (isset($row['id'])) {
                    $res->zh = $row['zh'];
                    $res->en = $row['en'];
                    $res->ru = $row['ru'];
                } else {
                    $res->zh = '';
                    $res->en = '';
                    $res->ru = '';
                }
            } elseif ($res->type == 'pid') {
                $command = Yii::app()->db->createCommand(
                  "select ID,ZH,RU,EN from categories_props
  where ID=:id limit 1"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT);
                $row = $command->queryRow();
                if (isset($row['ID'])) {
                    $res->zh = $row['ZH'];
                    $res->en = $row['EN'];
                    $res->ru = $row['RU'];
                } else {
                    $res->zh = '';
                    $res->en = '';
                    $res->ru = '';
                }
            } elseif ($res->type == 'vid') {
                $command = Yii::app()->db->createCommand(
                  "select ID,ZH,RU,EN from categories_props_vals
  where ID=:id limit 1"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT);
                $row = $command->queryRow();
                if (isset($row['ID'])) {
                    $res->zh = $row['ZH'];
                    $res->en = $row['EN'];
                    $res->ru = $row['RU'];
                } else {
                    $res->zh = '';
                    $res->en = '';
                    $res->ru = '';
                }
            } elseif ($res->type == 'suggestion') {
                $res->zh = '';
                $res->en = '';
                $res->ru = '';
            } else {
                $res->zh = '';
                $res->en = '';
                $res->ru = '';
            }
            echo CJSON::encode($res);
            Yii::app()->end();
        } //==================================================================
        elseif (isset($_POST['TranslateForm'])) {
            $res->type = $_POST['TranslateForm']['type'];
            $res->id = $_POST['TranslateForm']['id'];
            $res->zh = $_POST['TranslateForm']['zh'];
            $res->en = $_POST['TranslateForm']['en'];
            $res->ru = $_POST['TranslateForm']['ru'];
            $res->global = $_POST['TranslateForm']['global'];
            $res->uid = $_POST['TranslateForm']['uid'];

            if ($res->type == 'plain') {
                $command = Yii::app()->db->createCommand(
                  "update cn_dic
        set TEXT_ZH=:zh,
        TEXT_RU=:ru,
        TEXT_EN=:en,
        uid=:uid,
        date=now()
        where id=:id or (:global=1 and TEXT_ZH=:zh)"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT)
                  ->bindParam(':zh', $res->zh, PDO::PARAM_STR)
                  ->bindParam(':en', $res->en, PDO::PARAM_STR)
                  ->bindParam(':ru', $res->ru, PDO::PARAM_STR)
                  ->bindParam(':uid', $res->uid, PDO::PARAM_INT)
                  ->bindParam(':global', $res->global, PDO::PARAM_INT);
                $command->execute();
            } elseif ($res->type == 'cid') {
                $command = Yii::app()->db->createCommand(
                  "update categories
        set ru= :ru,
        en=:en,
        zh=:zh
  where id=:id"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT)
                  ->bindParam(':zh', $res->zh, PDO::PARAM_STR)
                  ->bindParam(':en', $res->en, PDO::PARAM_STR)
                  ->bindParam(':ru', $res->ru, PDO::PARAM_STR);
                $command->execute();
            } elseif ($res->type == 'cid_ext') {
                $command = Yii::app()->db->createCommand(
                  "update categories_ext
        set ru= :ru,
        en=:en,
        zh=:zh
  where id=:id"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT)
                  ->bindParam(':zh', $res->zh, PDO::PARAM_STR)
                  ->bindParam(':en', $res->en, PDO::PARAM_STR)
                  ->bindParam(':ru', $res->ru, PDO::PARAM_STR);
                $command->execute();
            } elseif ($res->type == 'pid') {
                $command = Yii::app()->db->createCommand(
                  "update categories_props
        set ZH=:zh,
        RU=:ru,
        EN=:en
  where ID=:id  or (:global=1 and ZH=:zh)"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT)
                  ->bindParam(':zh', $res->zh, PDO::PARAM_STR)
                  ->bindParam(':en', $res->en, PDO::PARAM_STR)
                  ->bindParam(':ru', $res->ru, PDO::PARAM_STR)
                  ->bindParam(':global', $res->global, PDO::PARAM_INT);
                $command->execute();
            } elseif ($res->type == 'vid') {
                $command = Yii::app()->db->createCommand(
                  "update categories_props_vals
         set ZH=:zh,
         RU=:ru,
         EN=:en
  where ID=:id   or (:global=1 and ZH=:zh)"
                )
                  ->bindParam(':id', $res->id, PDO::PARAM_INT)
                  ->bindParam(':zh', $res->zh, PDO::PARAM_STR)
                  ->bindParam(':en', $res->en, PDO::PARAM_STR)
                  ->bindParam(':ru', $res->ru, PDO::PARAM_STR)
                  ->bindParam(':global', $res->global, PDO::PARAM_INT);
                $command->execute();
            } elseif ($res->type == 'suggestion') {
                $res->zh = '';
                $res->en = '';
                $res->ru = '';
            } else {
                $res->zh = '';
                $res->en = '';
                $res->ru = '';
            }
            Yii::app()->end();
        }
    }

    public function actionTranslateblock()
    {
        try {
            if (isset($_POST['translationArray'])) {
                $translations = unserialize(gzuncompress(convert_uudecode(urldecode($_POST['translationArray']))));
                //$translations=unserialize(urldecode($_POST['translationArray']));
                $translations = Yii::app()->DanVitTranslator->translateBlock($translations);
                echo serialize($translations);
            }
        } catch (Exception $e) {
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    public function actionIndex()
    {
        $this->body_class = 'home';
        $featured_items = DSConfig::getVal('featured_items');
        $featuredTypes = explode(',', $featured_items);
        $searchResPopular = (in_array('popular', $featuredTypes));
        $searchResRecommended = (in_array('recommended', $featuredTypes));
        $searchResRecentUser = (in_array('recentUser', $featuredTypes));
        $searchResRecentAll = (in_array('recentAll', $featuredTypes));
        $this->pageTitle = '';

        $referer = Yii::app()->request->urlReferrer;
        $extReferer = !preg_match('/^http[s]*:\/\/' . DSConfig::getVal('site_domain') . '/i', $referer);
        if ($extReferer) {
            if (Yii::app()->request->isAjaxRequest) {
                $this->renderPartial(
                  'main',
                  array(
                    'itemsPopular'     => $searchResPopular,
                    'itemsRecommended' => $searchResRecommended,
                    'itemsRecentUser'  => $searchResRecentUser,
                    'itemsRecentAll'   => $searchResRecentAll,
                  ),
                  false,
                  false
                );
            } else {
                $this->render(
                  'main',
                  array(
                    'itemsPopular'     => $searchResPopular,
                    'itemsRecommended' => $searchResRecommended,
                    'itemsRecentUser'  => $searchResRecentUser,
                    'itemsRecentAll'   => $searchResRecentAll,
                  )
                );
            }
        } else {
            if (Yii::app()->request->isAjaxRequest) {
                $this->renderPartial(
                  'main',
                  array(
                    'itemsPopular'     => $searchResPopular,
                    'itemsRecommended' => $searchResRecommended,
                    'itemsRecentUser'  => $searchResRecentUser,
                    'itemsRecentAll'   => $searchResRecentAll,
                  ),
                  false,
                  false
                );
            } else {
                $this->render(
                  'main',
                  array(
                    'itemsPopular'     => $searchResPopular,
                    'itemsRecommended' => $searchResRecommended,
                    'itemsRecentUser'  => $searchResRecentUser,
                    'itemsRecentAll'   => $searchResRecentAll,
                  )
                );
            }
        }
    }

    public function actionError()
    {
        $this->pageTitle = Yii::t('main', 'Ошибка');
        $error = Yii::app()->errorHandler->error;
        if ($error) {
            LogSiteErrors::logError(
              $error['code'] . ' ' . $error['type'] . ' ' . $error['file'] . ': ' . $error['line'],
              $error['message'],
              $error['trace']
            );
            //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
        } else {
            $error = new CHttpException(403, Yii::t('main', 'Что-то пошло не так.'));
        }
        if (Yii::app()->request->isAjaxRequest) {
            /*
                code - the HTTP status code (e.g. 403, 500)
                type - the error type (e.g. 'CHttpException', 'PHP Error')
                message - the error message
                file - the name of the PHP script file where the error occurs
                line - the line number of the code where the error occurs
                trace - the call stack of the error
                source - the context source code where the error occurs

             */
            echo 'Error: ' . $error['type'] . ': ' . $error['message'];
        } else {
            $this->render('error', Array('error' => $error));
        }
    }
}