<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="FavoriteController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class customFavoriteController extends CustomFrontController {

  public $body_class = 'category';
  public $columns = 'two-col';
/*
  function actionPage($page_id) {
    $mainMenu = MainMenu::getMainMenu(FALSE, 0, Utils::TransLang(), (int) $page_id);
    $mainMenuRoot = MainMenu::getMainMenuRecord(Utils::TransLang(), (int) $page_id);
    if (count($mainMenuRoot) > 0) {
      $mainMenuRoot[(int) $page_id]['children'] = $mainMenu;

      $this->pageTitle = $mainMenuRoot[(int) $page_id]['view_text'];
      $this->breadcrumbs = array(
        $this->pageTitle
      );
    }
    $this->params['params'] = FALSE;
    $this->params['cids'] = array();
    $this->params['bids'] = array();
    $this->params['filters'] = array();
    $this->params['multiFilters'] = array();
    $this->params['suggestions'] = array();
    $this->params['priceRange'] = array();

    $this->render('page', array('links' => $mainMenuRoot, 'lang' => Utils::TransLang()));
  }
*/
  public function actionIndex($name = FALSE,$page = 1,
                                 $price_min = FALSE, $price_max = FALSE, $sort_by = FALSE) {
    if (!$name) {
      throw new CHttpException(400, Yii::t('main', 'Неверный запрос'));
    }
    if ($name!='other') {
    $model = Category::model()->find('cid=:cid', array(':cid' => $name));

    if ($model == FALSE) {
      throw new CHttpException (400, Yii::t('main', 'Неверный запрос'));
    }
    }
    $params = array(
      'name'       => $name,
      'cid'        => isset($model) ? $model->cid : 0,
      'price_min'  => $price_min,
      'price_max'  => $price_max,
    );

    $search = new Search;
    $search->params = $params;
    $search->page = $page;
    $search->cid_model = isset($model) ? $model : false;

    $res = $search->execute('favorite');

    $this->breadcrumbs = $search->breadcrumbs();

    foreach ($params as $k => $v) {
      if (!(bool) $v) {
        unset($params[$k]);
      }
    }
    $orig_params = $params;
    unset($orig_params['original']);
    unset($orig_params['not_unique']);

    $lang = Utils::TransLang();
    if (isset($model) && ($model)) {
    //СЕО настройки
    $this->pageTitle = Yii::t('main','Избранное').' - '.(!empty($model->{'page_title_' . $lang})) ? $model->{'page_title_' . $lang} : $model->{$lang};

    $this->meta_desc = (!empty($model->{'meta_desc_' . $lang})) ? $model->{'meta_desc_' . $lang}
      : Yii::t('main', 'Широкий ассортимент, {category} в нашем магазине', array('{category}' => $model->{$lang}));

    $this->meta_keyword = (!empty($model->{'meta_keeword_' . $lang})) ? $model->{'meta_keeword_' . $lang} : $model->{$lang};
    } else {
      $model=false;
      $this->pageTitle = Yii::t('main','Избранное - прочее');
    }

    //Pagination
    if (isset($res->total_results)) {
      $pages = new CPagination($res->total_results);
      $pages->pageSize = Search::getPageSize();
      $pages->currentPage = $page - 1;
      $pages->params = $params;
    }
    else {
      $pages = FALSE;
    }

    $this->params['params'] = $params;
    $this->params['cids'] = $res->cids;
    $this->params['bids'] = $res->bids;
    $this->params['groups'] = $res->groups;
    $this->params['filters'] = $res->filters;
    $this->params['multiFilters'] = $res->multiFilters;
    $this->params['suggestions'] = $res->suggestions;
    $this->params['priceRange'] = $res->priceRange;
    $this->render('/search/index', array(
      'res'         => $res,
      'params'      => $params,
      'sort_by'     => $sort_by,
      'pages'       => $pages,
      'orig_params' => $orig_params,
      'category'    => $model,
    ));
  }

}