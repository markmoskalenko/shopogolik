<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="WebUser.php">
* </description>
**********************************************************************************************************************/?>
<?php

class WebUser extends CWebUser {
    private $_model = null;
    
    const ACC_PREFIX = '0000-0000';
    public $guestName='guest';

    /**
     * PHP magic method.
     * This method is overriden so that persistent states can be accessed like properties.
     * @param string $name property name
     * @return mixed property value
     */
    public function __get($name)
    {
     if ($name=='role') {
         return $this->getRole();
     } else {
            return parent::__get($name);
        }
    }

    public function getRole() {
        $user = $this->getModel();
        if($user){
            return $user->role;
        } else {
            return 'guest';
        }
    }

    public function inRole($roles,$recursive=true) {
        $userRole=$this->getRole();
        if (is_array($roles)) {
            if (in_array($userRole,$roles)) {
                return true;
            }
        } else {
            if ($userRole===$roles) {
                return true;
            }
        }
        if ($recursive && (!in_array($userRole,array('guest','user')))) {
            $ACL = Yii::app()->authManager->GetACLforUser(Yii::app()->user->id, true);
            if (is_array($roles)) {
            foreach ($roles as $role) {
                if (in_array($role,array('guest','user'))) {
                    continue;
                }
                if (in_array('$'.strtolower($role).'$',$ACL->allow)) {
                    return true;
                }
            }
            } else {
                if (!in_array($roles,array('guest','user'))) {
                    if (in_array('$' . strtolower($roles) . '$', $ACL->allow)) {
                        return true;
                    }
                }
            }
           }
        return false;
    }

    public function notInRole($roles,$recursive=true) {
        return !($this->inRole($roles,$recursive));
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = Users::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }
    

     public function getPersonalAccount(){
        if(!isset($this->id))
            return null;
        
        $len = strlen((string)$this->id);
        $prefix = explode('-',self::ACC_PREFIX);
        if($len > 4){
            return $prefix[0].'-'.(string)$this->id;
        }else{
            $data = substr($prefix[1], 0, -$len);
            return $prefix[0].'-'.$data.(string)$this->id;
        }
     }
     
     public function getIdByPersonalAccount($acc){
         $prefix = explode('-',$acc);
         if(count($prefix) != 2){
             return null;
         }
/*         $data = str_split(substr(implode('',$prefix),1));
         $data_real = $data;
         foreach ($data as $k=>$part){
             if($part != '0'){
                 break;
             }
             unset($data_real[$k]);
         }
*/
         return $prefix[1];
        
     }
     

    
}