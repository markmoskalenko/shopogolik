<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="CustomFrontController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class CustomFrontController extends CustomController
{

    public $columns = 'two-col';
	public $icon = '';

    public $body_class = '';

    public $breadcrumbs = array();

    public $params = array();

    public $meta_desc = '';

    public $meta_keyword = '';

    public $frontTheme = '';
    public $frontThemePath = '';
    public $frontThemeAbsolutePath = '';

//    public $layout =  '';
    public function init()
    {
      //  header('Access-Control-Allow-Origin: *');
        if (Yii::app()->theme->name == '') {
            Yii::app()->theme = DSConfig::getVal('site_front_theme');
        }
        $this->frontTheme = Yii::app()->theme->name;
        $this->frontThemePath = Yii::app()->request->baseUrl . '/themes/' . $this->frontTheme;
        $this->frontThemeAbsolutePath = Yii::getPathOfAlias('webroot') . '/themes/' . $this->frontTheme;
//=================================
//=================================

        $userId = Yii::app()->user->id;
        if ($userId != null) {
            $user = Users::model()->findByPk($userId);
            if (($user === false) || ($user == null)) {
                Yii::app()->user->logout();
                Yii::app()->user->clearStates();
            }
        }
        parent::init();
    }

    function __construct($id, $module = null)
    {
//    $cart = Yii::app()->request->cookies['cart'];
        $session = new CHttpSession;
        $session->open();
        if (isset(Yii::app()->request->cookies['lang'])) {
          Yii::app()->language=Yii::app()->request->cookies['lang']->value;
        } else {
            Yii::app()->language=substr(Yii::app()->request->getPreferredLanguage(),0,2);
        }
        if (!in_array(Yii::app()->language,explode(',',DSConfig::getVal('site_language_block')))) {
            Yii::app()->language=Yii::app()->sourceLanguage;
        }
        if (Yii::app()->getBaseUrl(true)== 'http://'.DSConfig::getVal('site_domain')) {
            $cookie = new CHttpCookie('lang', Yii::app()->language);
            $cookie->expire = time() + 60 * 60 * 24 * 180;
            Yii::app()->request->cookies['lang'] = $cookie;
        }
//=================================
        SiteLog::doHttpLog();
//=================================
        parent::__construct($id, $module);
    }

//=========================================================================
    public function filters()
    {
        return array(
          array('application.components.PostprocessFilter'),
//      array( 'COutputCache', 'duration'=> 60,),
        );
    }

    /**
     * This method is invoked at the beginning of {@link render()}.
     * You may override this method to do some preprocessing when rendering a view.
     * @param string $view the view to be rendered
     * @return boolean whether the view should be rendered.
     * @since 1.1.5
     */
    protected function beforeRender($view)
    {
// jquery publication
        $cs = Yii::app()->clientScript;
        $cs->scriptMap = array(
          'jquery-ui.js'      => false,
          'jquery-ui.min.js'  => false,
          'jquery-ui.css'     => false,
          'jquery-ui.min.css' => false,
        );
        Yii::app()->clientScript->registerScript(YII_DEBUG ? 'jquery.js' : 'jquery.min.js', CClientScript::POS_HEAD);
// define lang var for js
        Yii::app()->clientScript->registerScript(
          'defineLangVar',
          "
             var lang='" . Utils::AppLang() . "';
    ",
          CClientScript::POS_BEGIN
        );
// Поддержка rtl (например, иврит)
        if (Utils::AppLang() == 'he') {
            Yii::app()->clientScript->registerCss(
              'defineRTL',
              'html, body {
              height:100%;
              direction:rtl;
              }'
            );
        }
// meta tags processing
        if (!$this->pageTitle) {
            $this->pageTitle = cms::customContent('default-meta-title', true, true);
        }
        if ($this->meta_desc) {
            Yii::app()->clientScript->registerMetaTag(
              $this->meta_desc,
              'description',
              null,
              array('lang' => Utils::AppLang()),
              'page-description'
            );
        } else {
            if (!Yii::app()->clientScript->isMetaTagExists('page-description')) {
            Yii::app()->clientScript->registerMetaTag(
              cms::customContent('default-meta-description', true, true),
              'description',
              null,
              array('lang' => Utils::AppLang()),
              'page-description'
            );
            }
        }

        if ($this->meta_keyword) {
            Yii::app()->clientScript->registerMetaTag(
              $this->meta_keyword,
              'keywords',
              null,
              array('lang' => Utils::AppLang()),
              'page-keywords'
            );
        } else {
            if (!Yii::app()->clientScript->isMetaTagExists('page-keywords')) {
            Yii::app()->clientScript->registerMetaTag(
              cms::customContent('default-meta-keywords', true, true),
              'keywords',
              null,
              array('lang' => Utils::AppLang()),
              'page-keywords'
            );
            }
        }
        Yii::app()->clientScript->registerMetaTag(
          'DropShop team',
          'author',
          null,
          array('lang' => Utils::AppLang()),
          'page-author'
        );
        Yii::app()->clientScript->registerMetaTag(
          'DropShop team',
          'copyright',
          null,
          array('lang' => Utils::AppLang()),
          'page-copyright'
        );
        return true;
    }

    /**
     * Returns the directory containing view files for this controller.
     * The default implementation returns 'protected/views/ControllerID'.
     * Child classes may override this method to use customized view path.
     * If the controller belongs to a module, the default view path
     * is the {@link CWebModule::getViewPath module view path} appended with the controller ID.
     * @return string the directory containing the view files for this controller. Defaults to 'protected/views/ControllerID'.
     */
    public function getViewPath()
    {
        if (($module = $this->getModule()) === null) {
            $module = Yii::app();
        }
//        return $module->getViewPath() . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . $this->getId();
        return $module->getViewPath().DIRECTORY_SEPARATOR.$this->getId();
    }

}