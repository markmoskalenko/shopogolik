<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DsDbMessageSource.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class DsDbMessageSource extends CDbMessageSource
{
    public static function addTranslation($language, $category, $message, $translation = false)
    {
        $_language = $language;
        $_category = $category;
        $_message = trim($message);
        $_translation = $translation;
        $id = Yii::app()->db->createCommand(
          'select sm.id from t_source_message sm where sm.category=:category and sm.message=:message  limit 1'
        )
          ->bindParam(':category', $_category, PDO::PARAM_STR)
          ->bindParam(':message', $_message, PDO::PARAM_STR)
          ->queryScalar();
        if (!$id) {
            Yii::app()->db->createCommand(
              'insert ignore into  t_source_message (category,message)
                     values(:category,:message)'
            )
              ->bindParam(':category', $_category, PDO::PARAM_STR)
              ->bindParam(':message', $_message, PDO::PARAM_STR)
              ->execute();
            $id = Yii::app()->db->getLastInsertID();
        }
        if ($id && $translation) {
            Yii::app()->db->createCommand(
              'delete from t_message where id=:id and language=:language;
               insert ignore into  t_message (id,language,translation)
                     values(:id,:language,:translation);'
            )
              ->bindParam(':id', $id, PDO::PARAM_INT)
              ->bindParam(':language', $_language, PDO::PARAM_STR)
              ->bindParam(':translation', $_translation, PDO::PARAM_STR)
              ->execute();
        }
    }
}

class TranslationEventHandler
{
    private static function umlaute($text)
    {
        $returnvalue = "";
        for ($i = 0; $i < mb_strlen($text); $i++) {
            $teil = hexdec(rawurlencode(mb_substr($text, $i, 1)));
            if ($teil < 32 || $teil > 1114111) {
                $returnvalue .= mb_substr($text, $i, 1);
            } else {
                $returnvalue .= "&#" . str_pad($teil,8,'0',STR_PAD_LEFT) . ";";
            }
        }
        return $returnvalue;
    }

    public static function handleMissingTranslation($event)
    {
        if ($event->message=='...') {
            return;
        }
        // обрабатываем отсутствие перевода
        $translation = Yii::app()->DanVitTranslator->translateMessage(
          $event->message,
          Yii::app()->sourceLanguage,
          $event->language
        );
        $translation = trim($translation);
        if ($translation && ((strlen($translation)) > 0) && (strtoupper($translation) != 'FALSE')) {
            //$translation = htmlspecialchars($translation, ENT_COMPAT|ENT_IGNORE|, 'utf-8');
            $translation = htmlentities($translation, ENT_COMPAT|ENT_IGNORE, 'UTF-8');
/*            if (in_array($event->language, array('de', 'fr', 'es'))) {
                $translation = rawurlencode(utf8_encode($translation));
            }
*/
            if (ltrim($event->message) != $event->message) {
                $translation = ' ' . $translation;
            }
            if (rtrim($event->message) != $event->message) {
                $translation = $translation . ' ';
            }
        } else {
            $translation = false;
        }
        DsDbMessageSource::addTranslation($event->language, $event->category, $event->message, $translation);
        $event->message = (($translation) ? $translation : $event->message);
        $event->handled = true;
    }
}