<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="CustomControllerVars.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class CustomControllerVars implements ArrayAccess, Countable, Iterator, Serializable {

  private $_container = array();
  private $_position = 0;
  public $errors = array();
  public $report='';
  public $asText='';

  public function __construct(array $array = NULL) {
    if (!is_null($array)) {
      $this->_container = $array;
    }
  }

  public function offsetExists($offset) {
    return isset($this->_container[$offset]);
  }

  public function offsetGet($offset) {
    if (is_array($offset)) {
      $_offset=$offset[0];
      $_type=$offset[1];
    } else {
      $_offset=$offset;
      $_type=false;
    }
    if ($this->offsetExists($_offset)) {
      $result=$this->_container[$_offset];
    } else {
      $result=new CustomControllerVar(null,'Parameter '.$_offset.' not found!');
      $this->errors[$_offset]=$result->description;
    }
    if ($_type) {
      switch ($_type) {
        case $_type=='array':
          if (!is_array($result->val)) {
            $result->val=array();
            $this->errors[$_offset]='Type inconsistence: array needed!';
          };
          break;
        case ($_type=='numeric'):
          if (!is_numeric($result->val)) {
            $result->val=0;
            $this->errors[$_offset]='Type inconsistence: numeric needed!';
          };
        break;
        default:
          if (!is_a($result->val,$_type)) {
            $result->val=null;
            $this->errors[$_offset]='Type inconsistence: '.$_type.' needed!';
          };
          break;
      }
    }
    return $result;
  }

  public function offsetSet($offset, $value) {
    if (is_array($offset)) {
      $_offset=$offset[0];
      $_description=$offset[1];
    } else {
      $_offset=$offset;
      $_description='';
    }
    if (is_null($_offset)) {
      $this->_container[] = new CustomControllerVar($value,$_description);
    }
    else {
      $this->_container[$_offset] = new CustomControllerVar($value,$_description);
    }
  }

  public function offsetUnset($offset) {
    unset($this->_container[$offset]);
  }

  public function rewind() {
    $this->_position = 0;
  }

  public function current() {
    return $this->_container[$this->_position];
  }

  public function key() {
    return $this->_position;
  }

  public function next() {
    ++$this->_position;
  }

  public function valid() {
    return isset($this->_container[$this->_position]);
  }

  public function count() {
    return count($this->_container);
  }

  public function serialize() {
    return serialize($this->_container);
  }

  public function unserialize($data) {
    $this->_container = unserialize($data);
  }

  public function __invoke(array $data = NULL) {
    if (is_null($data)) {
      return $this->_container;
    }
    else {
      $this->_container = $data;
    }
  }
  public function prepareReport(){
    $this->report='none';
    if (is_array($this->_container)) {
      $this->report='';
      foreach ($this->_container as $name=>$var) {
        $varType=gettype($var->val);
        if ($varType=='object') {
          $varType=get_class($var->val);
        }
        $this->report=$this->report.$name.' ('.$varType.')'."\r\n";
        $varStruct=$var->val;
        if (is_array($varStruct)) {
          $varStruct=array_slice($varStruct,0,1);
        }
        try {
          $dump=CVarDumper::dumpAsString($varStruct,3);
        $this->asText=$this->asText.'var $'.$name." = \r\n".$dump."\r\n";
        } catch (Exception $e) {
          return;
        }
      }
    }
  }
}