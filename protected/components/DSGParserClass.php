<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DSGParserClass.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php
require_once('DSGCurl.php');
require_once('DSGUtils.php');

abstract class DSGParserClass {
  public $search_DSGRulesList = 'DSG_rulesList.xml';
  public $lang = 'ru';
  public $proxyProtocol = 'V1';
  public $forceProtocolV2 = false;
  public $proxyEnabled = TRUE;
  public $curlUseMulti = TRUE;
  private $XML;
  private static $forceDSGRulesFromFile = YII_DEBUG;
  public $debugExt = FALSE;
  protected $debugMessages = array();
  public $exitCode='UNCHECKED';
  public $referer='http://taobao.com/';

  function __construct($debugExt = FALSE) {
    if ($debugExt) {
      $this->debugExt = TRUE;
    }
    ini_set('pcre.backtrack_limit', 4*1024*1024);
    ini_set('pcre.recursion_limit', 1024*1024);
  }

  protected function debugMessage($function, $paramName, $paramValue, $subject, $result, $valid) {
    if ($this->debugExt) {
      $hash = md5((string) (is_object($paramName)) ? print_r($paramName, TRUE) : $paramName . ' ' .
        (string) (is_object($paramValue)) ? print_r($paramValue, TRUE) : $paramValue) . (string) $valid;
      foreach ($this->debugMessages as $mess) {
        if ($mess['hash'] == $hash) {
          return;
        }
      }
      $this->debugMessages[] = array(
        'id'          => count($this->debugMessages),
        'function'    => (string) $function,
        'param_name'  => (string) (is_object($paramName)) ? print_r($paramName, TRUE) : $paramName,
        'param_value' => (string) (is_object($paramValue)) ? print_r($paramValue, TRUE) : $paramValue,
        'subject'     => (string) (is_object($subject)) ? print_r($subject, TRUE) : $subject,
        'result'      => (string) (is_object($result)) ? print_r($result, TRUE) : $result,
        'valid'       => (string) (is_object($valid)) ? print_r($valid, TRUE) : $valid,
        'hash'        => $hash,
      );
    }
  }

  public function execute($name = '') {
//      throw new Exception('test test', 598);
    try {
      if (class_exists('DSConfig', FALSE)) {
        if (!$this->forceProtocolV2) {
          $this->proxyEnabled = (DSConfig::getVal('proxy_enabled') == 1);
        $this->proxyProtocol = DSConfig::getVal('proxy_protocol');
        }
      }
      if ($this->proxyEnabled && ($this->proxyProtocol == 'V2')) {
        if (class_exists('Profiler', FALSE)) {
          Profiler::start('proxyV2->download');
        }
        $res = DSGCurl::getFromProxyV2($this);
        if (class_exists('Profiler', FALSE)) {
          Profiler::stop('proxyV2->download');
        }
        if (isset($res->data)) {
//            throw new Exception('Good HTTP data', 597);
          return $res->data;
        }
        else {
            if (!class_exists('Profiler', FALSE)) {
                throw new Exception('No HTTP data', 597);
            }
          return FALSE;
        }
      }
      else {
          if (!class_exists('Profiler', FALSE)) {
//              throw new Exception('Error in protocol selection', 596);
          }
        return FALSE;
      }
    } catch (Exception $e) {
        if (class_exists('Profiler', FALSE)) {
            Profiler::stop('proxyV2->download');
        } else {
            throw new Exception(CVarDumper::dumpAsString($e), 599);
        }
      return FALSE;
    }
  }

  public function initRulesList() {
    if (class_exists('DSConfig', FALSE) && (!self::$forceDSGRulesFromFile)) {
      $this->search_DSGRulesList = DSConfig::getVal('search_DropShop_grabbers');
    }
    else {
      if (stream_resolve_include_path($this->search_DSGRulesList)!==false) {
      $this->search_DSGRulesList = file_get_contents($this->search_DSGRulesList, FILE_USE_INCLUDE_PATH);
      } else {
        $this->forceProtocolV2=true;
      }
    }
    if ($this->forceProtocolV2) {
       $this->proxyEnabled=true;
      $this->proxyProtocol = 'V2';
    }
  }

  protected function getDSGRules($urlType, $queryParams, $test = FALSE) {
    if (class_exists('Profiler', FALSE)) {
      Profiler::start('DSGParser->getDSGParams');
    }
    $xml = simplexml_load_string($this->search_DSGRulesList, NULL, LIBXML_NOCDATA);
    $search = $xml->xpath("/parser_sections/parser_section[name='" . $urlType . "']");
    if (isset($search[0])) {
      $this->XML = $search[0];
    }
    else {
      return FALSE;
    }

    $result = new stdClass();
    $result->debug = $this->getDSGRule('debug') == 'true';
    //=================
    $result->type = $this->getDSGRule('type');
    $result->urlPreservedPart = '';

    $res = $this->prepareQueryUrl($this->getDSGRule('url_query_params/param'), $queryParams, $test);
    if (isset($res->qParams)) {
      $result->url = $this->getDSGRule('base_url') . http_build_query($res->qParams);
    } else {
        $result->url=((isset($this->srcUrl))?$this->srcUrl:'');
    }
    if (isset($res->qParamsPreserved)) {
      $result->urlPreservedPart = http_build_query($res->qParamsPreserved);
    }
    if (class_exists('Profiler', FALSE)) {
      Profiler::stop('DSGParser->getDSGParams');
    }
    return $result;
  }

  public function regexMatch($ruleXPath, $subject, array &$matches = array(), $flags = 0, $offset = 0) {
    $rule = $this->getDSGRule($ruleXPath);
    if (($rule === FALSE) || is_array($rule) || ($rule instanceof SimpleXMLElement)) {
      $this->debugMessage('regexMatch', $ruleXPath, print_r($rule, TRUE), $subject, NULL, FALSE);
      return FALSE;
    }
//    $_subject =str_replace('   ',' ',$subject);
//    $_subject =str_replace('  ',' ',$_subject);
      ini_set('pcre.backtrack_limit', 4*1024*1024);
      ini_set('pcre.recursion_limit', 1024*1024);
    $res = preg_match($rule, $subject, $matches, $flags, $offset);
    $err=preg_last_error();
    if ($err!=0) {
        $this->debugMessage('regexMatch err: '.$err, $ruleXPath, $rule, $subject, print_r($matches, TRUE), $res > 0);
    } else {
        $this->debugMessage('regexMatch', $ruleXPath, $rule, $subject, print_r($matches, TRUE), $res > 0);
    }
    return $res;
  }

  public function regexMatchAll($ruleXPath, $subject, array &$matches = array(), $flags = 0, $offset = 0) {
    $rule = $this->getDSGRule($ruleXPath);
    if (($rule === FALSE) || is_array($rule) || ($rule instanceof SimpleXMLElement)) {
      $this->debugMessage('regexMatchAll', $ruleXPath, print_r($rule, TRUE), $subject, NULL, FALSE);
      return FALSE;
    }
      ini_set('pcre.backtrack_limit', 4*1024*1024);
      ini_set('pcre.recursion_limit', 1024*1024);
    $res = preg_match_all($rule, $subject, $matches, $flags, $offset);
    $err=preg_last_error();
    if ($err!=0) {
    $this->debugMessage('regexMatchAll err: '.$err, $ruleXPath, $rule, $subject, print_r($matches, TRUE), $res > 0);
    } else {
        $this->debugMessage('regexMatchAll', $ruleXPath, $rule, $subject, print_r($matches, TRUE), $res > 0);
    }
    return $res;
  }

  public function regexReplaceCallback($ruleXPath, $callback, $subject, $limit = -1, $count = NULL) {
    $rule = $this->getDSGRule($ruleXPath);
    if (($rule === FALSE) || is_array($rule) || ($rule instanceof SimpleXMLElement)) {
      $this->debugMessage('regexReplaceCallback', $ruleXPath, print_r($rule, TRUE), $subject, NULL, FALSE);
      return FALSE;
    }
      ini_set('pcre.backtrack_limit', 4*1024*1024);
      ini_set('pcre.recursion_limit', 1024*1024);
      $res = preg_replace_callback($rule, $callback, $subject, $limit, $count);
      $err=preg_last_error();
      if ($err!=0) {
          $this->debugMessage('regexReplaceCallback err: '.$err, $ruleXPath, $rule, $subject, NULL, $res > 0);
      } else {
          $this->debugMessage('regexReplaceCallback', $ruleXPath, $rule, $subject, NULL, $res > 0);
      }
    return $res;
  }

    public function jsonObjGet($ruleXPath, $subject) {
        $rule = $this->getDSGRule($ruleXPath);
        if (($rule === FALSE) || is_array($rule) || ($rule instanceof SimpleXMLElement)) {
            $this->debugMessage('jsonObjGet', $ruleXPath, print_r($rule, TRUE),  CVarDumper::dumpAsString($subject), NULL, FALSE);
            return FALSE;
        }
        $path=preg_split( '/\//',$rule,-1,PREG_SPLIT_NO_EMPTY);
        if (!$path ||(!is_array($path)||(count($path)<=0))) {
            $this->debugMessage( 'jsonObjGet err: incorrect path', $ruleXPath, $rule, CVarDumper::dumpAsString($subject), null, false);
            return false;
        }
        if (!is_object($subject) && !is_array($subject)) {
            $this->debugMessage( 'jsonObjGet err: incorrect subject', $ruleXPath, $rule, CVarDumper::dumpAsString($subject), null, false);
            return false;
        }
        $result=$subject;
        $modified=false;
        foreach ($path as $objName) {
            if (!$objName) continue;
            if (is_object($result) && property_exists($result,$objName)) {
                $result=$result->{$objName};
                $modified=true;
                continue;
            } elseif(is_array($result) && isset($result[$objName])) {
                $result=$result[$objName];
                $modified=true;
                continue;
            } else {
                $this->debugMessage( 'jsonObjGet path not found: '.$objName, $ruleXPath, $rule, CVarDumper::dumpAsString($subject), null, false);
                return false;
            }
        }
        if ($modified) {
            return $result;
        } else {
            return false;
        }
    }

  public function getDSGRule($xpath) {
    $xml = $this->XML->xpath($xpath);
    if ($xml) {
      if (count($xml) > 1 || count($xml[0]) > 1) {
        $res = $xml;
      }
      else {
        $res = trim((string) ($xml[0]));
      }
      return $res;
    }
    else {
      return FALSE;
    }
  }

  private function prepareQueryUrl($params, $queryParams, $test) {
    $qParams = array();
    $qParamsPreserved = array();
    $param_descr = array();
    $param_descr_default = array();
    if (is_array($params)) {
      foreach ($params as $param) {
        $default = FALSE;
        if (isset($param->name)) {
          if ((string) $param->name != 'default') {
            $param_descr[(string) $param->name] = new stdClass();
          }
          else {
            $default = TRUE;
            $param_descr_default[] = new stdClass();
          }
        }
        else {
          continue;
        }
        if (isset($param->preserve_after_redirect)) {
          if (!$default) {
            $param_descr[(string) $param->name]->preserve_after_redirect = (bool) (string) $param->preserve_after_redirect;
          }
          else {
            end($param_descr_default)->preserve_after_redirect = (bool) (string) $param->preserve_after_redirect;
          }
        }
        if (isset($param->url_part)) {
          if (!$default) {
            $param_descr[(string) $param->name]->url_part = (string) $param->url_part;
            if (isset($param->subparam)) {
              $param_descr[(string) $param->name]->subparam = (string) $param->subparam;
            }
          }
          else {
            end($param_descr_default)->url_part = (string) $param->url_part;
            if (isset($param->subparam)) {
              end($param_descr_default)->subparam = (string) $param->subparam;
            }
          }
        }
        if ((!$default) && (isset($param->values)) && (isset($param->values->value))) {
          end($param_descr)->values = array();
          foreach ($param->values->value as $val) {
            if ((isset($val->name)) && (isset($val->url_part))) {
              end($param_descr)->values[(string) $val->name] = (string) $val->url_part;
            }
            else {
              continue;
            }
          }
        }
        if (!$default) {
          if (isset($param->test_value)) {
            $param_descr[(string) $param->name]->test_value = (string) $param->test_value;
          }
        }
        else {
          if (isset($param->value)) {
            end($param_descr_default)->value = (string) $param->value;
          }
        }
      }
    }
    if (isset($queryParams)) {
      foreach ($queryParams as $k => $queryParam) {
        if (isset($param_descr[$k])) {
          if (!$test) {
            if (!isset($param_descr[$k]->values)) {
              $qParams[$param_descr[$k]->url_part] = $queryParams[$k];
              if (isset($param_descr[$k]->subparam)) {
                $qParams[$param_descr[$k]->url_part] = $param_descr[$k]->subparam . $qParams[$param_descr[$k]->url_part];
              }
            }
            else {
              $t = $queryParams[$k];
              $v = $param_descr[$k]->values;
              if (isset($v[$t])) {
                $qParams[$param_descr[$k]->url_part] = $param_descr[$k]->values[$t];
                if (isset($param_descr[$k]->subparem)) {
                  $qParams[$param_descr[$k]->url_part] = $param_descr[$k]->subparem . $qParams[$param_descr[$k]->url_part];
                }
              }
            }
          }
          else {
            if (isset($param_descr[$k]->test_value)) {
              $qParams[$param_descr[$k]->url_part] = $param_descr[$k]->test_value;
              if (isset($param_descr[$k]->subparem)) {
                $qParams[$param_descr[$k]->url_part] = $param_descr[$k]->subparem . $qParams[$param_descr[$k]->url_part];
              }
            }
          }
          if (isset($param_descr[$k]->preserve_after_redirect) && (isset($param_descr[$k]->preserve_after_redirect))) {
            $qParamsPreserved[$param_descr[$k]->url_part] = $qParams[$param_descr[$k]->url_part];
          }
        }
      }
    }
    if (isset($param_descr_default)) {
      foreach ($param_descr_default as $queryParam) {
        $qParams[$queryParam->url_part] = $queryParam->value;
        if (isset($queryParam->subparam)) {
          $qParams[$queryParam->url_part] = $queryParam->subparam . $qParams[$queryParam->url_part];
        }
        if (isset($queryParam->preserve_after_redirect) && (isset($queryParam->preserve_after_redirect))) {
          $qParamsPreserved[$queryParam->url_part] = $qParams[$queryParam->url_part];
        }
      }
    }
    $result = new stdClass();
    $result->qParams = $qParams;
    $result->qParamsPreserved = $qParamsPreserved;
//== Prepare query "q" extended params =========================================
    if (is_a($this, 'DSGSearch')) {
      if (is_array($result->qParams) && isset($result->qParams['q'])) {
        if (strpos($result->qParams['q'], '&') !== FALSE) {
          parse_str('q=' . $result->qParams['q'], $args);
          $result->qParams = array_merge($result->qParams, $args);
        }
      }
    }
//== Prepare search_filter params =========================================
    if (is_a($this, 'DSGSearch')) {
      if (isset($result->qParams['ppath'])) {
      if (preg_match('/\-1:(\d+)/',$result->qParams['ppath'],$res)) {
        unset($result->qParams['ppath']);
        $result->qParams['cat']=$res[1];
      }
      }
    }
//==============================================================================
//== Prepare search_filter params =========================================
    if (is_a($this, 'DSGSearch')) {
      if (class_exists('DSConfig', FALSE)) {
      $search_filter = DSConfig::getVal('search_filter');
      } else {
        $search_filter='';
      }
      if ($search_filter !== '') {
        parse_str($search_filter, $args);
        if (isset($args['q'])) {
        if (is_array($result->qParams) && isset($result->qParams['q'])) {
          $result->qParams['q']=$args['q'].' '.$result->qParams['q'];
          }
          unset($args['q']);
      }
        if (count($args)>0) {
          $result->qParams = array_merge($result->qParams, $args);
        }
      }
    }
//==============================================================================

    return $this->postprocessQueryUrl($result);
  }

private function postprocessQueryUrl($url) {
    if (is_a($this, 'DSGSearch')) {
       if ((string)$this->XML->type=='search_curl/list.json') {
           $this->referer = 'http://list.taobao.com/itemlist/default.htm?' . http_build_query($url->qParams);;
           if (isset($url->qParams) && is_array($url->qParams) && isset($url->qParams['s'])) {
//               $url->qParams['data-key'] = 's';
//               $url->qParams['data-value'] = $url->qParams['s'];
//               $url->qParams['ajax'] = 'true';
           }
       } else {
           $this->referer = 'http://s.taobao.com/search?' . http_build_query($url->qParams);;
           if (isset($url->qParams) && is_array($url->qParams) && isset($url->qParams['s'])) {
               $url->qParams['data-key'] = 's';
               $url->qParams['data-value'] = $url->qParams['s'];
               $url->qParams['ajax'] = 'true';
           }
       }
       if (isset($url->qParams['ppath'])) {
           if (preg_match('/(\d+):0[;$]/',$url->qParams['ppath'],$propsWithCat)) {
               $url->qParams['cat']=$propsWithCat[1];
           }
           $url->qParams['ppath']=preg_replace('/\d+:0[;$]/','',$url->qParams['ppath']);
           if (!$url->qParams['ppath']) {
               unset($url->qParams['ppath']);
           }
       }
    } else {
        $this->referer='http://item.taobao.com/';
    }
    return $url;
}

  public function JavaScriptToJSON($input, $asObject = FALSE,$strong=false) {
    if (($input == FALSE) || ($input == '')) {
      return new stdClass();
    }
    if (!$strong) {
        ini_set('pcre.backtrack_limit', 4 * 1024 * 1024);
        ini_set('pcre.recursion_limit', 1024 * 1024);
        if ($input{0} != '{') {
            $res = preg_replace('/^.*?(?={)/s', '', $input);
        } else {
            $res = $input;
        }
        $res = preg_replace('/[^}]*?$/s', '', $res);
        $res = preg_replace('/(?<=[\s,{])([a-z0-9\-_]+)\s*:(?!\d{2}:\d{2})/i', '"\1":', $res);
        $res = str_replace("'", '"', $res);
        $res = preg_replace('/("\s*:\s*)(?=[,}])/s', '\1""', $res);
        $cnt = preg_match_all('/(?::\s*")(.*?)(?:"\s*[,}])/', $res, $matches);
        if ($cnt > 0) {
            foreach ($matches[1] as $src) {
                if (strpos($src, '"') === false) {
                    continue;
                }
                $res = str_replace($src, str_replace('"', '\"', $src), $res);
            }
        }
        if (!$asObject) {
            return $res;
        } else {
            $resJSON = json_decode($res);
            $err = json_last_error();
            switch ($err) {
                case JSON_ERROR_NONE:
                    $this->debugMessage('JavaScriptToJSON', null, $input, null, $resJSON, true);
                    return $resJSON;
                    break;
                /*     case JSON_ERROR_DEPTH:
                       $jsonOk='Достигнута максимальная глубина стека';
                       break;
                     case JSON_ERROR_STATE_MISMATCH:
                       $jsonOk='Некорректные разряды или не совпадение режимов';
                       break;
                     case JSON_ERROR_CTRL_CHAR:
                       $jsonOk='Некорректный управляющий символ';
                       break;
                     case JSON_ERROR_SYNTAX:
                       $jsonOk='Синтаксическая ошибка, не корректный JSON';
                       break;
                     case JSON_ERROR_UTF8:
                       $jsonOk='Некорректные символы UTF-8, возможно неверная кодировка';
                       break;
                */
                default:
                    $this->debugMessage('JavaScriptToJSON', null, $input, null, $resJSON, false);
                    return new stdClass();
                    break;
            }
        }
    } else {
        $resJSON = json_decode($input);
        $err = json_last_error();
        switch ($err) {
            case JSON_ERROR_NONE:
                $this->debugMessage('JavaScriptToJSON', null, $input, null, $resJSON, true);
                return $resJSON;
                break;
            /*     case JSON_ERROR_DEPTH:
                   $jsonOk='Достигнута максимальная глубина стека';
                   break;
                 case JSON_ERROR_STATE_MISMATCH:
                   $jsonOk='Некорректные разряды или не совпадение режимов';
                   break;
                 case JSON_ERROR_CTRL_CHAR:
                   $jsonOk='Некорректный управляющий символ';
                   break;
                 case JSON_ERROR_SYNTAX:
                   $jsonOk='Синтаксическая ошибка, не корректный JSON';
                   break;
                 case JSON_ERROR_UTF8:
                   $jsonOk='Некорректные символы UTF-8, возможно неверная кодировка';
                   break;
            */
            default:
                $this->debugMessage('JavaScriptToJSON', null, $input, null, $resJSON, false);
                return new stdClass();
                break;
        }
    }
  }

  public function getObjPropValDef($stringPath, $defVal) {
    if (isset($stringPath)) {
      $this->debugMessage('getObjPropValDef', $stringPath, $stringPath, NULL, $stringPath, TRUE);
      return $stringPath;
    }
    else {
      $this->debugMessage('getObjPropValDef', $stringPath, $stringPath, NULL, $defVal, FALSE);
      return $defVal;
    }
  }
  public static function getParserVersion($date = TRUE) {
    if (class_exists('DSConfig', FALSE) && (!self::$forceDSGRulesFromFile)) {
      $rulesList = DSConfig::getVal('search_DropShop_grabbers');
    }
    else {
      if (stream_resolve_include_path('DSG_rulesList.xml')!==false) {
      $rulesList = file_get_contents('DSG_rulesList.xml', FILE_USE_INCLUDE_PATH);
      } else {
        $rulesList=false;
      }
    }
    if ($rulesList==false) {
      return 'V2';
    }
    $xml = simplexml_load_string($rulesList, NULL, LIBXML_NOCDATA);
    if (isset($xml->attributes()->ver)) {
      if ($date) {
        return $xml->attributes()->ver . ' ' . $xml->attributes()->date;
      }
      else {
        return $xml->attributes()->ver;
      }
    }
    return 'VX';
  }
    protected function verifyResult($res) {
            if (is_a($this, 'DSGSearch')) {
                if (!isset($res->items)) {
                    $this->exitCode = 'ERROR';
                    throw new Exception('ERROR in verifyResult 541',500);
                }
                if (is_array($res->items) && (count($res->items) <= 0)) {
                    $this->exitCode = 'ERROR';
                    throw new Exception('ERROR in verifyResult 545',500);
                }
                if ($res->total_results <= 0) {
                    $this->exitCode = 'ERROR';
                    throw new Exception('ERROR in verifyResult 549',500);
                }
            }
            if (is_a($this, 'DSGItem')) {
                if (!isset($res->item->num_iid)) {
                    $this->exitCode = 'ERROR';
                    throw new Exception('ERROR in verifyResult 555',500);
                }
                if (!$res->item->num_iid) {
                    $this->exitCode = 'ERROR';
                    throw new Exception('ERROR in verifyResult 559',500);
                }
            }
            if (is_a($this, 'DSGSeller')) {

                if (!isset($res->seller->nick)) {
                    $this->exitCode = 'ERROR';
                    throw new Exception('ERROR in verifyResult 566',500);
                }
                if (!$res->seller->nick) {
                    $this->exitCode = 'ERROR';
                    throw new Exception('ERROR in verifyResult 570',500);
                }
            }
       $this->exitCode='OK';
            return;
    }
}