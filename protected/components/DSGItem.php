<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DSGItem.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

require_once('DSGParserClass.php');

/**
 * Class DSGItem
 *
 * Request example
 *
 * <?php
 * $req = new DSGItem();
 * $req->id = $iid;
 * $req->fromCart = FALSE;
 * $item_resp = $req->execute('item.taobao.com');

 */
class DSGItem extends DSGParserClass
{
    public $id = false;
    public $postageArea='110100';
    public $fromCart = false;
    private $itemParams;
    private $DSGItemRes;

    function __construct($debugExt = false)
    {
        parent::__construct($debugExt);
    }

    public function execute($name = 'item.taobao.com')
    {
        $this->initRulesList();
        if (class_exists('DSConfig', FALSE)) {
            $this->postageArea = DSConfig::getVal('delivery_taobao_area_code');
        }
        $useDirect=false;
        $proxyV2result = parent::execute($name);
        if ($proxyV2result) {
            return $proxyV2result;
        }
        $query = array();
        if ($this->id != false) {
            $query['id'] = $this->id;
        }
        $this->itemParams = $this->getDSGRules($name, $query);
        if ($this->itemParams->debug) {
            echo '<div><pre>' . $this->itemParams->url . '</pre></div>';
        }
        if (class_exists('Profiler', false)) {
            Profiler::start('DSGItem->download');
        }
        $data = DSGCurl::getHttpDocument($this->itemParams->url, $this->itemParams->urlPreservedPart,$useDirect,$this->referer); //
        if (class_exists('Profiler', false)) {
            Profiler::stop('DSGItem->download');
        }
//    $this->debugMessage( 'getHttpDocument', $this->itemParams->url, NULL, NULL, $data, TRUE);
        if ($data->data === null) {
            $data->data = false;
        }
        $data->url = $this->itemParams->url;
        $data->isTmall = !(bool) $this->regexMatch('loaded_result_is_fake', $data->data);
        if (($name == 'item.taobao.com') && $data->isTmall) {
            $this->itemParams = $this->getDSGRules('detail.tmall.com', $query);
        }
        $this->DSGItemRes = new stdClass();
        $this->DSGItemRes->html = $data;
        if ($data->data == false) {
            return $this->DSGItemRes;
        }
        $this->DSGItemRes->item = new stdClass();
        $this->DSGItemRes->item->fromDSG = true;
        $this->DSGItemRes->item->isTmall = $data->isTmall;
        if ($data->isTmall) {
            $this->parseTmall($data);
        } else {
            $this->parseTaobao($data);
        }
        unset($this->DSGItemRes->html);
        unset($this->search_DSGRulesList);
        unset($this->itemParams);
        if (!function_exists('orderSKUs')) {
            function orderSKUs($a, $b)
            {
                $a_val = $a->url;
                $b_val = $b->url;
                if ($a_val > $b_val) {
                    return -1;
                } elseif ($a_val == $b_val) {
                    return 0;
                } else {
                    return 1;
                }
            }
        }
        if (isset($this->DSGItemRes->item->props)) {
            foreach ($this->DSGItemRes->item->props as $prop) {
                if (isset($prop->childs)) {
                    uasort($prop->childs, "orderSKUs");
                }
            }
        }
        $this->verifyResult($this->DSGItemRes);
        $this->DSGItemRes->debugMessages = $this->debugMessages;
        return $this->DSGItemRes;
    }

    private function parseTmall($data)
    {
        if (class_exists('Profiler', false)) {
            Profiler::start('DSGItem->parse-tm');
        }
        $resstr = array();
        $res = $this->regexMatch('parse_item_values/user_rate_url', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->userRateUrl = $resstr[0];
        }
        $resstr = array();
        $res = $this->regexMatch('parse_item_values/tshop', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->tshop = $this->JavaScriptToJSON($resstr[1], true);
        }
        $resstr = array();
        $this->DSGItemRes->item->cid = $this->getObjPropValDef(@$this->DSGItemRes->item->tshop->itemDO->categoryId, 0);
        $this->DSGItemRes->item->num_iid = $this->getObjPropValDef(@$this->DSGItemRes->item->tshop->itemDO->itemId, 0);
        $this->DSGItemRes->item->seller_id = $this->getObjPropValDef(
          @$this->DSGItemRes->item->tshop->itemDO->userId,
          0
        );
        $this->DSGItemRes->item->nick = urldecode(
          (string) $this->getObjPropValDef(@$this->DSGItemRes->item->tshop->itemDO->sellerNickName, '')
        );
        $this->DSGItemRes->item->price = (float) $this->getObjPropValDef(
          @$this->DSGItemRes->item->tshop->itemDO->reservePrice,
          0
        );
        $this->DSGItemRes->item->num = $this->getObjPropValDef(@$this->DSGItemRes->item->tshop->itemDO->quantity, 0);

        $res = $this->regexMatch('parse_item_values/shop_id', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->shop_id = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->shop_id = '0';
        }
        $res = $this->regexMatch('parse_item_values/title', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->title = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->title = '';
        }
        $res = $this->regexMatch('parse_item_values/pic_url', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->pic_url = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->pic_url = '';
        }
        $this->DSGItemRes->item->location = '';
        $this->DSGItemRes->item->post_fee = 0;
        $this->DSGItemRes->item->express_fee = 0;
        $this->DSGItemRes->item->ems_fee = 0;
        $this->DSGItemRes->item->item_imgs = new stdClass();
        $this->DSGItemRes->item->item_imgs->item_img = array();
        $res = $this->regexMatch('parse_item_values/item_imgs/block', $data->data, $resstr);
        if ($res > 0) {
            $item_imgs_data = $resstr[1];
            $res = $this->regexMatchAll('parse_item_values/item_imgs/url', $item_imgs_data, $resstr);
            unset($item_imgs_data);
            if ($res > 0) {
                foreach ($resstr[1] as $i => $item_img) {
                    $this->DSGItemRes->item->item_imgs->item_img[$i] = new stdClass();
                    $this->DSGItemRes->item->item_imgs->item_img[$i]->id = $i;
                    $this->DSGItemRes->item->item_imgs->item_img[$i]->position = $i;
                    $this->DSGItemRes->item->item_imgs->item_img[$i]->url = $item_img;
                }
                unset($item_img);
            }
        }

        $this->DSGItemRes->item->item_attributes = array();
        $res = $this->regexMatch('parse_item_values/item_attributes/block', $data->data, $resstr);
        if ($res > 0) {
            $attributes_data = $resstr[1];
            $res = $this->regexMatchAll('parse_item_values/item_attributes/propvallist', $attributes_data, $resstr);
            if ($res > 0) {
                foreach ($resstr[1] as $i => $item_attr) {
                    $this->DSGItemRes->item->item_attributes[$i] = new stdClass();
                    $this->DSGItemRes->item->item_attributes[$i]->prop = html_entity_decode(
                      $resstr[1][$i],
                      ENT_COMPAT,
                      'UTF-8'
                    );
                    $this->DSGItemRes->item->item_attributes[$i]->val = html_entity_decode(
                      $resstr[2][$i],
                      ENT_COMPAT,
                      'UTF-8'
                    );
                }
                unset($item_attr);
            }
            unset($attributes_data);
        }
        $this->DSGItemRes->item->props = array();
        $res = $this->regexMatch('parse_item_values/prop_imgs/block', $data->data, $resstr);
        if ($res > 0) {
            $propresstr = array();
            $propres = $this->regexMatchAll('parse_item_values/prop_imgs/prop/block', $resstr[1], $propresstr);
            if ($propres > 0) {
                // Обход блока
                foreach ($propresstr[1] as $propblock) {
                    // Свойство есть, создаём объект, который потом ниже добавим в массив, если всё хорошо
                    $propFinal = new stdClass();
                    $propblockresstr = array();
                    $propblockres = $this->regexMatch(
                      'parse_item_values/prop_imgs/prop/title',
                      $propblock,
                      $propblockresstr
                    );
                    if ($propblockres > 0) {
                        $propFinal->cid = $this->DSGItemRes->item->cid;
                        $propFinal->name_zh = $propblockresstr[1];
                        $propFinal->name = $propblockresstr[1];
                        unset($propblockresstr);
                    } else {
                        continue;
                    }
                    $propimgresstr = array();
                    $propimgres = $this->regexMatchAll(
                      'parse_item_values/prop_imgs/prop/prop_img/block',
                      $propblock,
                      $propimgresstr
                    );
                    if ($propimgres > 0) {
                        // Ага, есть значения - создаём для них массив в объекте
                        $propFinal->childs = array();
                        //$this->DSGItemRes->item->props->prop[$i]['prop_img']=array();
                        foreach ($propimgresstr[1] as $j => $propimgblock) {
                            $resstr0 = array();
                            $res0 = $this->regexMatch(
                              'parse_item_values/prop_imgs/prop/prop_img/properties',
                              $propimgblock,
                              $resstr0
                            );
                            if ($res0 > 0) {
                                // Ага, у значений есть нормальные параметры - создаём под них класс
                                $propFinal->childs[$j] = new stdClass();
                                //Получаем pid:vid
                                $pidvid = explode(':', $resstr0[1]);
                                if (!isset($pidvid[0]) || !isset($pidvid[1])) {
                                    // Нет pid-vid - пропускаем
                                    continue;
                                }
                                $propFinal->childs[$j]->vid = $pidvid[1];
                            } else {
                                continue;
                            }
                            $resstr0 = array();
                            $res0 = $this->regexMatch(
                              'parse_item_values/prop_imgs/prop/prop_img/title',
                              $propimgblock,
                              $resstr0
                            );
                            if ($res0 > 0) {
                                $propFinal->childs[$j]->name_zh = $resstr0[1];
                                $propFinal->childs[$j]->name = $resstr0[1];
                            } else {
                                $propFinal->childs[$j]->name_zh = '';
                                $propFinal->childs[$j]->name = '';
                            }
                            $resstr0 = array();
                            $res0 = $this->regexMatch(
                              'parse_item_values/prop_imgs/prop/prop_img/url',
                              $propimgblock,
                              $resstr0
                            );
                            if ($res0 > 0) {
                                $propFinal->childs[$j]->url = $resstr0[1];
                            } else {
                                $propFinal->childs[$j]->url = '';
                            }
                        }
                        unset($propimgblock);
                        unset($propimgresstr);
                    }
                    // Тут один стрёмный момент. Если всё хорошо или не очень - добавляем таки объект свойства в массив
                    if (isset($pidvid)) {
                        $this->DSGItemRes->item->props[$pidvid[0]] = clone $propFinal;
                        unset($pidvid);
                        unset($propFinal);
                    }
                }
                unset($propblock);
                unset($propresstr);
            }
        }
        // ===== apiItemDesc - получаем описание товара ========================================================================
        if (!$this->fromCart) {
            // $this->DSGItemRes->item->descUrl=$this->DSGItemRes->item->tshop->api;
            if (isset($this->DSGItemRes->item->tshop->api->descUrl)) {
                if (class_exists('Profiler', false)) {
                    Profiler::start('http:getDesc');
                }
//        $dldata = DSGCurl::getHttpDocument((string) $this->DSGItemRes->item->tshop->api->descUrl);
//        $this->debugMessage( 'getHttpDocument', (string) $this->DSGItemRes->item->tshop->api->descUrl, NULL, NULL, $dldata, TRUE);
                if (class_exists('Profiler', false)) {
                    Profiler::stop('http:getDesc');
                }
                $this->DSGItemRes->item->desc = false; //$dldata->data;
                $this->DSGItemRes->item->descUrl = (string) $this->DSGItemRes->item->tshop->api->descUrl;
                unset ($dldata);
            }
        }
        if ($this->curlUseMulti) {
            $urls = array();
            if (class_exists('Profiler', false)) {
                Profiler::start('http:getMulti');
            }
            if (!$this->fromCart) {
                if (isset($this->DSGItemRes->item->tshop->itemDO->itemId) && isset($this->DSGItemRes->item->tshop->itemDO->userId)) {
                    $_valReviewsApi = 'http://dsr.rate.tmall.com/list_dsr_info.htm?itemId='
                      . $this->DSGItemRes->item->tshop->itemDO->itemId . '&sellerId='
                      . $this->DSGItemRes->item->tshop->itemDO->userId;
                    $urls[] = $_valReviewsApi;
                    //$dldata = DSGCurl::getHttpDocument($_valReviewsApi);
                }
                // Some hacks to get more items
                $_apiRelateMarket = 'http://aldcdn.tmall.com/recommend.htm?itemId=' . $this->DSGItemRes->item->num_iid . '&refer=&rn=32&appId=03054';
                $urls[] = $_apiRelateMarket;
                //$dldata = DSGCurl::getHttpDocument($url);
            }
            $_truePostage='http://detailskip.taobao.com/json/postageFee.htm?itemId='.$this->DSGItemRes->item->num_iid.'&areaId='.$this->postageArea;
            $urls[] = $_truePostage;
            $downloads = DSGCurl::getHttpDocumentArray($urls,false,$this->referer);
            if (class_exists('Profiler', false)) {
                Profiler::stop('http:getMulti');
            }
            if (isset($this->DSGItemRes->item->tshop->itemDO->itemId) && isset($this->DSGItemRes->item->tshop->itemDO->userId)) {
                if (isset($_valReviewsApi) && isset($downloads[$_valReviewsApi])) {
                    $this->DSGItemRes->item->valReviewsApi = $this->JavaScriptToJSON(
                      $downloads[$_valReviewsApi]->data,
                      true
                    );
                }
            }
            if (isset($_apiRelateMarket) && isset($downloads[$_apiRelateMarket])) {
                $downloads[$_apiRelateMarket]->data = preg_replace(
                  '/,{"acurl".*/s',
                  '',
                  $downloads[$_apiRelateMarket]->data
                );
                $this->DSGItemRes->item->apiRelateMarket = $this->JavaScriptToJSON(
                  $downloads[$_apiRelateMarket]->data,
                  true
                );
            }
            if (isset($_truePostage) && isset($downloads[$_truePostage])) {
                $this->DSGItemRes->item->truePostage = $downloads[$_truePostage]->data;
            }

        } else {
            // ===== valReviewsApi - получаем РЕЙТИНГ ПРОДАВЦА ========================================================================
            //http://dsr.rate.tmall.com/list_dsr_info.htm?itemId=20008134785&sellerId=1714306514
            if (!$this->fromCart) {
                if (isset($this->DSGItemRes->item->tshop->itemDO->itemId) && isset($this->DSGItemRes->item->tshop->itemDO->userId)) {
                    if (class_exists('Profiler', false)) {
                        Profiler::start('http:getReviews');
                    }
                    $dldata = DSGCurl::getHttpDocument(
                      'http://dsr.rate.tmall.com/list_dsr_info.htm?itemId='
                      . $this->DSGItemRes->item->tshop->itemDO->itemId . '&sellerId='
                      . $this->DSGItemRes->item->tshop->itemDO->userId,false,
                      false,$this->referer
                    );
//        $this->debugMessage( 'getHttpDocument', 'http://dsr.rate.tmall.com/list_dsr_info.htm?itemId='
//          . $this->DSGItemRes->item->tshop->itemDO->itemId . '&sellerId='
//          . $this->DSGItemRes->item->tshop->itemDO->userId, NULL, NULL, $dldata, TRUE);
                    if (class_exists('Profiler', false)) {
                        Profiler::stop('http:getReviews');
                    }
                    $this->DSGItemRes->item->valReviewsApi = $this->JavaScriptToJSON($dldata->data, true);
                }
            }
            // ===== apiRelateMarket - получаем рекомендованные товары ========================================================================
            if (!$this->fromCart) {
                // Some hacks to get more items
                $url = 'http://aldcdn.tmall.com/recommend.htm?itemId=' . $this->DSGItemRes->item->num_iid . '&refer=&rn=32&appId=03054';
                if (class_exists('Profiler', false)) {
                    Profiler::start('http:getItemRelated');
                }
                $dldata = DSGCurl::getHttpDocument($url,false,false,$this->referer);
                //$this->debugMessage( 'getHttpDocument', $url, NULL, NULL, $dldata, TRUE);

                $dldata->data = preg_replace('/,{"acurl".*/s', '', $dldata->data);
                if (class_exists('Profiler', false)) {
                    Profiler::stop('http:getItemRelated');
                }
                $this->DSGItemRes->item->apiRelateMarket = $this->JavaScriptToJSON($dldata->data, true);
            }
            $url ='http://detailskip.taobao.com/json/postageFee.htm?itemId='.$this->DSGItemRes->item->num_iid.'&areaId='.$this->postageArea;
            if (class_exists('Profiler', false)) {
                Profiler::start('http:getTruePostage');
            }
            $dldata = DSGCurl::getHttpDocument($url,false,false,$this->referer);
            if (class_exists('Profiler', false)) {
                Profiler::stop('http:getTruePostage');
            }
            $this->DSGItemRes->item->truePostage = $dldata->data;
        }
//============================================================
        //http://aldcdn.tmall.com/recommend.htm?itemId=20008134785&refer=&rn=32&appId=03054
        //http://tui.taobao.com/recommend?appid=32&count=32&itemid=26733820522
        if (isset($this->DSGItemRes->item->tshop->initApi)) {
            $initApiData = DSGCurl::getHttpDocument(
              $this->DSGItemRes->item->tshop->initApi,
              false,
              false,
              'http://detail.tmall.com/item.htm?id=' . $this->DSGItemRes->item->num_iid
            );
            $initApi = $this->JavaScriptToJSON($initApiData->data, true);
            unset($initApiData);
        } else {
            $initApi = new stdClass();
        }
        //====== ДОСТАВКА ========================================
        if (isset($this->DSGItemRes->item->truePostage)){
            $res = $this->regexMatch('parse_item_values/block_sib/express_fee', $this->DSGItemRes->item->truePostage, $resstr);
            if ($res > 0) {
                if (!isset($resstr[2])) {
                    $this->DSGItemRes->item->express_fee = (float) $resstr[1];
                } else {
                    $this->DSGItemRes->item->express_fee = 0;
                }
            } else {
                $this->DSGItemRes->item->express_fee = 0;
            }
            $res = $this->regexMatch('parse_item_values/block_sib/ems_fee', $this->DSGItemRes->item->truePostage, $resstr);
            if ($res > 0) {
                if (!isset($resstr[2])) {
                    $this->DSGItemRes->item->ems_fee = (float) $resstr[1];
                } else {
                    $this->DSGItemRes->item->ems_fee = 0;
                }
            } else {
                $this->DSGItemRes->item->ems_fee = 0;
            }
            if ($this->DSGItemRes->item->express_fee == 0) {
                $this->DSGItemRes->item->express_fee = max(
                  array(
                    $this->DSGItemRes->item->post_fee,
                    $this->DSGItemRes->item->express_fee
                  )
                );
            }
    }
        else {
        $postage = 0;
        if (isset($initApi->defaultModel->deliveryDO->deliverySkuMap->default[0]->postage)) {
            $postage = $initApi->defaultModel->deliveryDO->deliverySkuMap->default[0]->postage;
            $res = preg_match('/([\d\.]+)/', $postage, $postages);
            if ($res > 0) {
                if (floatval($postages[1])) {
                    $postage = (float) $postages[1];
                } else {
                    $postage = 0;
                }
            } else {
                $postage = 0;
            }
        }
        unset($postages);
        $this->DSGItemRes->item->post_fee = $postage;
        $this->DSGItemRes->item->express_fee = $postage;
        $this->DSGItemRes->item->ems_fee = $postage;
    }
        // ===== SKUs - формируем ========================================================================
        $this->DSGItemRes->item->promotion_price = $this->DSGItemRes->item->price;
        if (isset($this->DSGItemRes->item->tshop->valItemInfo)) {
            if (isset($this->DSGItemRes->item->tshop->valItemInfo->skuMap)) {
                $this->DSGItemRes->item->skus = new stdClass();
                $srcSkuArray = (array) $this->DSGItemRes->item->tshop->valItemInfo->skuMap;
                $this->DSGItemRes->item->skus->sku = array();
                $totalQuantityNo99=0;
                $totalCountNo99=0;
                foreach ($srcSkuArray as $name => $val) {
                    $this->DSGItemRes->item->skus->sku[] = new stdClass();
                    end($this->DSGItemRes->item->skus->sku)->price = (float) $val->price;
                    end($this->DSGItemRes->item->skus->sku)->properties = trim($name, ';');
                    end($this->DSGItemRes->item->skus->sku)->properties_name = ''; // ???
                    end($this->DSGItemRes->item->skus->sku)->quantity = $val->stock;
                    if (end($this->DSGItemRes->item->skus->sku)->quantity != 99) {
                        $totalQuantityNo99=$totalQuantityNo99+end($this->DSGItemRes->item->skus->sku)->quantity;
                        $totalCountNo99=$totalCountNo99+1;
                    }
                    end($this->DSGItemRes->item->skus->sku)->sku_id = $val->skuId;

                    if (isset($initApi->defaultModel->itemPriceResultDO->priceInfo->{$val->skuId}->promotionList)) {
                        $promotions = $initApi->defaultModel->itemPriceResultDO->priceInfo->{$val->skuId}->promotionList;

                        $PromotionPriceArray = Array();
                        foreach ($promotions as $promotion) {
                            if (isset($promotion->price)) {
                                if ($promotion->price > 0) {
                                    $PromotionPriceArray[] = (float) $promotion->price;
                                }
                            }
                        }
                        if (count($PromotionPriceArray) > 0) {
                            $promotion_price = min($PromotionPriceArray);
                        } else {
                            $promotion_price = 0;
                        }
                        if ($promotion_price > 0) {
                            end($this->DSGItemRes->item->skus->sku)->promotion_price = $promotion_price;
                        } else {
                            end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->price;
                        }
                    } elseif (isset($val->specPrice)) {
                        if ($val->specPrice == '') {
                            end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->price;
                        } else {
                            end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->specPrice;
                        }
                    } else {
                        end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->price;
                    }
                }
// problem 99
                if ($totalCountNo99>0) {
                    $totalAvgQuantityNo99=$totalQuantityNo99/$totalCountNo99;
                    if ($totalAvgQuantityNo99>99*1.4 || $totalAvgQuantityNo99<99/1.4) {
                        foreach ($this->DSGItemRes->item->skus->sku as $i => $sku) {
                            $q=$this->DSGItemRes->item->skus->sku[$i]->quantity;
                            if ($this->DSGItemRes->item->skus->sku[$i]->quantity==99) {
                                $this->DSGItemRes->item->skus->sku[$i]->quantity=0;
                            }
                    }
                    }
                }
                $this->clearSKUs();
                unset($this->DSGItemRes->item->sku_promotions);
                $PromotionPriceArray = Array();
                foreach ($this->DSGItemRes->item->skus->sku as $sku) {
                    if (isset($sku->promotion_price)) {
                        if ($sku->promotion_price > 0) {
                            $PromotionPriceArray[] = $sku->promotion_price;
                        }
                    }

                }
                $this->DSGItemRes->item->promotion_price = min($PromotionPriceArray);
                unset($PromotionPriceArray);
                unset($sku);
                unset($srcSkuArray);
                unset($name);
                unset($val);
            } else {
                $tryToGetSku_promotions = true;
            }
        } else {
            $tryToGetSku_promotions = true;
        }
        if (isset($tryToGetSku_promotions) && $tryToGetSku_promotions && isset($initApi)) {
            if (isset($initApi->defaultModel->itemPriceResultDO->priceInfo->def->promotionList)
                && is_array($initApi->defaultModel->itemPriceResultDO->priceInfo->def->promotionList)) {
                    $promotions = array();
                    foreach ($initApi->defaultModel->itemPriceResultDO->priceInfo->def->promotionList as $defPromotions) {
                        if (isset($defPromotions->price)) {
                            $promotions[] = (float) $defPromotions->price;
                        }
                    }
                    if (count($promotions) > 0) {
                        $this->DSGItemRes->item->promotion_price = min($promotions);
                    }
                } elseif (isset($initApi->defaultModel->itemPriceResultDO->priceInfo->def->suggestivePromotionList)
              && is_array($initApi->defaultModel->itemPriceResultDO->priceInfo->def->suggestivePromotionList)) {
                $promotions = array();
                foreach ($initApi->defaultModel->itemPriceResultDO->priceInfo->def->suggestivePromotionList as $defPromotions) {
                    if (isset($defPromotions->price)) {
                        $promotions[] = (float) $defPromotions->price;
                    }
                }
                if (count($promotions) > 0) {
                    $this->DSGItemRes->item->promotion_price = min($promotions);
                }
            }
        }
        unset($skuFromSIB);
        unset($dldata);
        unset($this->DSGItemRes->item->tshop);
        $this->DSGItemRes->item->postage_id = '0';
        DSGCurl::closeCurl();
        if (class_exists('Profiler', false)) {
            Profiler::stop('DSGItem->parse-tm');
        }
    }

    private function parseTaobao($data)
    {
        if (class_exists('Profiler', false)) {
            Profiler::start('DSGItem->parse');
        }
        $resstr = array();
        $res = $this->regexMatch('parse_item_values/user_rate_url', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->userRateUrl = $resstr[0];
        }
        $resstr = array();
        $res = $this->regexMatch('parse_item_values/cid', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->cid = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->cid = '0';
        }
        $res = $this->regexMatch('parse_item_values/num_iid', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->num_iid = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->num_iid = '0';
        }
        $res = $this->regexMatch('parse_item_values/seller_id', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->seller_id = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->seller_id = '0';
        }
        $res = $this->regexMatch('parse_item_values/nick', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->nick = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->nick = '';
        }
        $res = $this->regexMatch('parse_item_values/shop_id', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->shop_id = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->shop_id = '0';
        }
        $res = $this->regexMatch('parse_item_values/title', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->title = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->title = '';
        }
        $res = $this->regexMatch('parse_item_values/pic_url', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->pic_url = (string) $resstr[1];
        } else {
            $this->DSGItemRes->item->pic_url = '';
        }
        $res = $this->regexMatch('parse_item_values/price', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGItemRes->item->price = (float) $resstr[1];
        } else {
            $this->DSGItemRes->item->price = 0;
        }
        $res = $this->regexMatch('parse_item_values/block_sib/_url', $data->data, $resstr);
        if ($res <= 0) {
            $this->DSGItemRes->item->num = 0;
            $this->DSGItemRes->item->location = '';
            $this->DSGItemRes->item->post_fee = 0;
            $this->DSGItemRes->item->express_fee = 0;
            $this->DSGItemRes->item->ems_fee = 0;
            $this->DSGItemRes->item->sku_data = new stdClass();
        } else {
            if (class_exists('Profiler', false)) {
                Profiler::start('http:getSIBdata');
            }
            $sib_data = DSGCurl::getHttpDocument(
              $resstr[1] . '&ref=' . urldecode(
                'http://item.taobao.com/item.htm?id=' . $this->DSGItemRes->item->num_iid
              ),
              false,
              false,
              'http://item.taobao.com/item.htm?id=' . $this->DSGItemRes->item->num_iid
            );
            //$this->debugMessage( 'getHttpDocument', $resstr[1], NULL, NULL, $sib_data, TRUE);
            if (class_exists('Profiler', false)) {
                Profiler::stop('http:getSIBdata');
            }
            $res = $this->regexMatch('parse_item_values/block_sib/num', $sib_data->data, $resstr);
            if ($res > 0) {
                $this->DSGItemRes->item->num = $resstr[1];
            } else {
                $this->DSGItemRes->item->num = 0;
            }
            $res = $this->regexMatch('parse_item_values/block_sib/location', $sib_data->data, $resstr);
            if ($res > 0) {
                $this->DSGItemRes->item->location = $resstr[1];
            } else {
                $this->DSGItemRes->item->location = '';
            }
            $res = $this->regexMatch('parse_item_values/block_sib/post_fee', $sib_data->data, $resstr);
            if ($res > 0) {
                if (!isset($resstr[2])) {
                    $this->DSGItemRes->item->post_fee = (float) $resstr[1];
                } else {
                    $this->DSGItemRes->item->post_fee = 0;
                }
            } else {
                $this->DSGItemRes->item->post_fee = 0;
            }
            $res = $this->regexMatch('parse_item_values/block_sib/express_fee', $sib_data->data, $resstr);
            if ($res > 0) {
                if (!isset($resstr[2])) {
                    $this->DSGItemRes->item->express_fee = (float) $resstr[1];
                } else {
                    $this->DSGItemRes->item->express_fee = 0;
                }
            } else {
                $this->DSGItemRes->item->express_fee = 0;
            }
            $res = $this->regexMatch('parse_item_values/block_sib/ems_fee', $sib_data->data, $resstr);
            if ($res > 0) {
                if (!isset($resstr[2])) {
                    $this->DSGItemRes->item->ems_fee = (float) $resstr[1];
                } else {
                    $this->DSGItemRes->item->ems_fee = 0;
                }
            } else {
                $this->DSGItemRes->item->ems_fee = 0;
            }
            if ($this->DSGItemRes->item->express_fee == 0) {
                $this->DSGItemRes->item->express_fee = max(
                  array(
                    $this->DSGItemRes->item->post_fee,
                    $this->DSGItemRes->item->express_fee
                  )
                );
            }
            $res = $this->regexMatch('parse_item_values/block_sib/sku_promotions', $sib_data->data, $resstr);
            if ($res > 0) {
                $this->DSGItemRes->item->sku_promotions = $this->JavaScriptToJSON('{' . $resstr[1], true);
            }
            $res = $this->regexMatch('parse_item_values/block_sib/sku_data', $sib_data->data, $resstr);
            if ($res > 0) {
                $this->DSGItemRes->item->sku_data = $this->JavaScriptToJSON('{' . $resstr[1], true);
            }
            unset($sib_data);
        }
        $this->DSGItemRes->item->item_imgs = new stdClass();
        $this->DSGItemRes->item->item_imgs->item_img = array();
        $res = $this->regexMatch('parse_item_values/item_imgs/block', $data->data, $resstr);
        if ($res > 0) {
            $item_imgs_data = $resstr[1];
            $res = $this->regexMatchAll('parse_item_values/item_imgs/url', $item_imgs_data, $resstr);
            unset($item_imgs_data);
            if ($res > 0) {
                foreach ($resstr[1] as $i => $item_img) {
                    $this->DSGItemRes->item->item_imgs->item_img[$i] = new stdClass();
                    $this->DSGItemRes->item->item_imgs->item_img[$i]->id = $i;
                    $this->DSGItemRes->item->item_imgs->item_img[$i]->position = $i;
                    $this->DSGItemRes->item->item_imgs->item_img[$i]->url = $item_img;
                }
                unset($item_img);
            }
        }
        $this->DSGItemRes->item->item_attributes = array();
        $res = $this->regexMatch('parse_item_values/item_attributes/block', $data->data, $resstr);
        if ($res > 0) {
            $attributes_data = $resstr[1];
            $res = $this->regexMatchAll('parse_item_values/item_attributes/propvallist', $attributes_data, $resstr);
            if ($res > 0) {
                foreach ($resstr[1] as $i => $item_attr) {
                    $this->DSGItemRes->item->item_attributes[$i] = new stdClass();
                    $this->DSGItemRes->item->item_attributes[$i]->prop = html_entity_decode(
                      $resstr[1][$i],
                      ENT_COMPAT,
                      'UTF-8'
                    );
                    $this->DSGItemRes->item->item_attributes[$i]->val = html_entity_decode(
                      $resstr[2][$i],
                      ENT_COMPAT,
                      'UTF-8'
                    );
                }
                unset($item_attr);
            }
            unset($attributes_data);
        }
        $this->DSGItemRes->item->props = array();
        $res = $this->regexMatch('parse_item_values/prop_imgs/block', $data->data, $resstr);
        if ($res > 0) {
            $propresstr = array();
            $propres = $this->regexMatchAll('parse_item_values/prop_imgs/prop/block', $resstr[1], $propresstr);
            if ($propres > 0) {
                // Обход блока
                foreach ($propresstr[1] as $propblock) {
                    // Свойство есть, создаём объект, который потом ниже добавим в массив, если всё хорошо
                    $propFinal = new stdClass();
                    $propblockresstr = array();
                    $propblockres = $this->regexMatch(
                      'parse_item_values/prop_imgs/prop/title',
                      $propblock,
                      $propblockresstr
                    );
                    if ($propblockres > 0) {
                        $propFinal->cid = $this->DSGItemRes->item->cid;
                        $propFinal->name_zh = $propblockresstr[1];
                        $propFinal->name = $propblockresstr[1];
//                $this->DSGItemRes->item->props->prop[$i]['title']=$propblockresstr[1];
                        unset($propblockresstr);
                    } else {
                        continue;
                    }
                    $propimgresstr = array();
                    $propimgres = $this->regexMatchAll(
                      'parse_item_values/prop_imgs/prop/prop_img/block',
                      $propblock,
                      $propimgresstr
                    );
                    if ($propimgres > 0) {
                        // Ага, есть значения - создаём для них массив в объекте
                        $propFinal->childs = array();
                        //$this->DSGItemRes->item->props->prop[$i]['prop_img']=array();
                        foreach ($propimgresstr[1] as $j => $propimgblock) {
                            $resstr0 = array();
                            $res0 = $this->regexMatch(
                              'parse_item_values/prop_imgs/prop/prop_img/properties',
                              $propimgblock,
                              $resstr0
                            );
                            if ($res0 > 0) {
                                // Ага, у значений есть нормальные параметры - создаём под них класс
                                $propFinal->childs[$j] = new stdClass();
//                 $this->DSGItemRes->item->props->prop[$i]['prop_img'][$j]=new stdClass();
                                //Получаем pid:vid
                                $pidvid = explode(':', $resstr0[1]);
                                if (!isset($pidvid[0]) || !isset($pidvid[1])) {
                                    // Нет pid-vid - пропускаем
                                    continue;
                                }
                                $propFinal->childs[$j]->vid = $pidvid[1];
                            } else {
                                continue;
                            }
                            $resstr0 = array();
                            $res0 = $this->regexMatch(
                              'parse_item_values/prop_imgs/prop/prop_img/title',
                              $propimgblock,
                              $resstr0
                            );
                            if ($res0 > 0) {
                                $propFinal->childs[$j]->name_zh = $resstr0[1];
                                $propFinal->childs[$j]->name = $resstr0[1];
                            } else {
                                $propFinal->childs[$j]->name_zh = '';
                                $propFinal->childs[$j]->name = '';
                            }
                            $resstr0 = array();
                            $res0 = $this->regexMatch(
                              'parse_item_values/prop_imgs/prop/prop_img/url',
                              $propimgblock,
                              $resstr0
                            );
                            if ($res0 > 0) {
                                $propFinal->childs[$j]->url = $resstr0[1];
                            } else {
                                $propFinal->childs[$j]->url = '';
                            }
                        }
                        unset($propimgblock);
                        unset($propimgresstr);
                    }
                    // Тут один стрёмный момент. Если всё хорошо или не очень - добавляем таки объект свойства в массив
                    if (isset($pidvid)) {
                        $this->DSGItemRes->item->props[$pidvid[0]] = clone $propFinal;
                        unset($pidvid);
                        unset($propFinal);
                    }
                }
                unset($propblock);
                unset($propresstr);
            }
        }
        $res = $this->regexMatch('parse_item_values/skus/block', $data->data, $resstr);
        if ($res > 0) {
            $skudatajson = $this->JavaScriptToJSON($resstr[1], true);
            unset($data);
// ===== apiItemDesc - получаем описание товара ========================================================================
            if (!$this->fromCart) {
                if (isset($skudatajson->apiItemDesc)) {
                    if (class_exists('Profiler', false)) {
                        Profiler::start('http:getDesc');
                    }
                    //$dldata = DSGCurl::getHttpDocument((string) $skudatajson->apiItemDesc);
                    //$this->debugMessage( 'getHttpDocument', (string) $skudatajson->apiItemDesc, NULL, NULL, $dldata, TRUE);
                    if (class_exists('Profiler', false)) {
                        Profiler::stop('http:getDesc');
                    }
                    //$this->DSGItemRes->item->desc = $dldata->data;
                    $this->DSGItemRes->item->desc = false;
                    $this->DSGItemRes->item->descUrl = (string) $skudatajson->apiItemDesc;
                    unset($dldata);
                }
            }
            if ($this->curlUseMulti) { //parallel donload
                if (class_exists('Profiler', false)) {
                    Profiler::start('http:getMulti');
                }
                $urls = array();
                if (!$this->fromCart) {
                    if (isset($skudatajson->valReviewsApi)) {
                        $urls[] = (string) $skudatajson->valReviewsApi;
                        //$dldata = DSGCurl::getHttpDocument((string) $skudatajson->valReviewsApi);
                    }
                    if (isset($skudatajson->apiItemInfo)) {
                        $urls[] = (string) $skudatajson->apiItemInfo;
                        //$dldata = DSGCurl::getHttpDocument((string) $skudatajson->apiItemInfo);
                    }
                    if (isset($skudatajson->apiRelateMarket)) {
                        // Some hacks to get more items
                        $url = preg_replace('/appid=\d+/', 'appid=32', (string) $skudatajson->apiRelateMarket);
                        $url = preg_replace('/count=\d+/', 'count=32', $url);
                        $urls[] = $url;
                        //$dldata = DSGCurl::getHttpDocument($url);
                    }
                }
                $_truePostage='http://detailskip.taobao.com/json/postageFee.htm?itemId='.$this->DSGItemRes->item->num_iid.'&areaId='.$this->postageArea;
                $urls[] = $_truePostage;
                $downloads = DSGCurl::getHttpDocumentArray($urls,false,$this->referer);
                if (class_exists('Profiler', false)) {
                    Profiler::stop('http:getMulti');
                }
                if (isset($skudatajson->valReviewsApi)) {
                    if (isset($downloads[(string) $skudatajson->valReviewsApi])) {
                        $this->DSGItemRes->item->valReviewsApi = $this->JavaScriptToJSON(
                          preg_replace(
                            '/(:?,"babyRateJsonList"|,"rateListInfo").*(?=})/s',
                            '',
                            $downloads[(string) $skudatajson->valReviewsApi]->data
                          ),
                          true
                        );
                    }
                }
                if (isset($skudatajson->apiItemInfo)) {
                    if (isset($downloads[(string) $skudatajson->apiItemInfo])) {
                        $this->DSGItemRes->item->apiItemInfo = $this->JavaScriptToJSON(
                          $downloads[(string) $skudatajson->apiItemInfo]->data,
                          true
                        );
                    }
                }
                if (isset($url)) {
                    if (isset($downloads[$url])) {
                        $this->DSGItemRes->item->apiRelateMarket = $this->JavaScriptToJSON(
                          $downloads[$url]->data,
                          true
                        );
                    }
                }
                if (isset($_truePostage) && isset($downloads[$_truePostage])) {
                    $this->DSGItemRes->item->truePostage = $downloads[$_truePostage]->data;
                }

            } else {
// ===== valReviewsApi - получаем РЕЙТИНГ ПРОДАВЦА ========================================================================
//http://rate.taobao.com/detail_rate.htm?userNumId=87110412&auctionNumId=15879813267&showContent=1&currentPage=1&ismore=0&siteID=7
                if (!$this->fromCart) {
                    if (isset($skudatajson->valReviewsApi)) {
                        if (class_exists('Profiler', false)) {
                            Profiler::start('http:getReviews');
                        }
                        $dldata = DSGCurl::getHttpDocument((string) $skudatajson->valReviewsApi,false,false,$this->referer);
                        //$this->debugMessage( 'getHttpDocument', (string) $skudatajson->valReviewsApi, NULL, NULL, $dldata, TRUE);
                        if (class_exists('Profiler', false)) {
                            Profiler::stop('http:getReviews');
                        }
                        $this->DSGItemRes->item->valReviewsApi = $this->JavaScriptToJSON(
                          preg_replace('/(:?,"babyRateJsonList"|,"rateListInfo").*(?=})/s', '', $dldata->data),
                          true
                        );
                    }
                }
// ===== apiItemInfo - получаем СТАТИСТИКУ ПРОДАЖ ========================================================================
                if (!$this->fromCart) {
                    if (isset($skudatajson->apiItemInfo)) {
                        if (class_exists('Profiler', false)) {
                            Profiler::start('http:getItemInfo');
                        }
                        $dldata = DSGCurl::getHttpDocument((string) $skudatajson->apiItemInfo,false,false,$this->referer);
                        //$this->debugMessage( 'getHttpDocument', (string) $skudatajson->apiItemInfo, NULL, NULL, $dldata, TRUE);
                        if (class_exists('Profiler', false)) {
                            Profiler::stop('http:getItemInfo');
                        }
                        $this->DSGItemRes->item->apiItemInfo = $this->JavaScriptToJSON($dldata->data, true);
                    }
                }
// ===== apiRelateMarket - получаем рекомендованные товары ========================================================================
                if (!$this->fromCart) {
                    if (isset($skudatajson->apiRelateMarket)) {
                        // Some hacks to get more items
                        $url = preg_replace('/appid=\d+/', 'appid=32', (string) $skudatajson->apiRelateMarket);
                        $url = preg_replace('/count=\d+/', 'count=32', $url);
                        if (class_exists('Profiler', false)) {
                            Profiler::start('http:getItemRelated');
                        }
                        $dldata = DSGCurl::getHttpDocument($url,false,false,$this->referer);
                        // $this->debugMessage( 'getHttpDocument',$url, NULL, NULL, $dldata, TRUE);
                        if (class_exists('Profiler', false)) {
                            Profiler::stop('http:getItemRelated');
                        }
                        $this->DSGItemRes->item->apiRelateMarket = $this->JavaScriptToJSON($dldata->data, true);
                    }
                }
//http://tui.taobao.com/recommend?appid=32&count=32&itemid=26733820522
                $url ='http://detailskip.taobao.com/json/postageFee.htm?itemId='.$this->DSGItemRes->item->num_iid.'&areaId='.$this->postageArea;
                if (class_exists('Profiler', false)) {
                    Profiler::start('http:getTruePostage');
                }
                $dldata = DSGCurl::getHttpDocument($url,false,false,$this->referer);
                if (class_exists('Profiler', false)) {
                    Profiler::stop('http:getTruePostage');
                }
                $this->DSGItemRes->item->truePostage = $dldata->data;
            }
            //====== ДОСТАВКА ========================================
            if (isset($this->DSGItemRes->item->truePostage)){
                $res = $this->regexMatch('parse_item_values/block_sib/express_fee', $this->DSGItemRes->item->truePostage, $resstr);
                if ($res > 0) {
                    if (!isset($resstr[2])) {
                        $this->DSGItemRes->item->express_fee = (float) $resstr[1];
                    } else {
                        $this->DSGItemRes->item->express_fee = 0;
                    }
                } else {
                    $this->DSGItemRes->item->express_fee = 0;
                }
                $res = $this->regexMatch('parse_item_values/block_sib/ems_fee', $this->DSGItemRes->item->truePostage, $resstr);
                if ($res > 0) {
                    if (!isset($resstr[2])) {
                        $this->DSGItemRes->item->ems_fee = (float) $resstr[1];
                    } else {
                        $this->DSGItemRes->item->ems_fee = 0;
                    }
                } else {
                    $this->DSGItemRes->item->ems_fee = 0;
                }
                if ($this->DSGItemRes->item->express_fee == 0) {
                    $this->DSGItemRes->item->express_fee = max(
                      array(
                        $this->DSGItemRes->item->post_fee,
                        $this->DSGItemRes->item->express_fee
                      )
                    );
                }
            }
// ===== SKUs - формируем ========================================================================
            if (isset($this->DSGItemRes->item->sku_data->sku)) {
                $skuFromSIB = $this->DSGItemRes->item->sku_data->sku;
                unset($this->DSGItemRes->item->sku_data);
            }
            $this->DSGItemRes->item->promotion_price = $this->DSGItemRes->item->price;
            if (isset($skudatajson->valItemInfo)) {
                if (isset($skudatajson->valItemInfo->skuMap)) {
                    $this->DSGItemRes->item->skus = new stdClass();
                    $srcSkuArray = (array) $skudatajson->valItemInfo->skuMap;
                    $this->DSGItemRes->item->skus->sku = array();
                    $totalQuantityNo99=0;
                    $totalCountNo99=0;
                    foreach ($srcSkuArray as $name => $val) {
                        $this->DSGItemRes->item->skus->sku[] = new stdClass();
                        end($this->DSGItemRes->item->skus->sku)->price = (float) $val->price;
                        end($this->DSGItemRes->item->skus->sku)->properties = trim($name, ';');
                        end($this->DSGItemRes->item->skus->sku)->properties_name = ''; // ???
                        if (isset($skuFromSIB)) {
                            if (isset($skuFromSIB->{$name})) {
                                end($this->DSGItemRes->item->skus->sku)->quantity = $skuFromSIB->{$name}->stock;
                            } else {
                                end($this->DSGItemRes->item->skus->sku)->quantity = $val->stock;
                            }
                        } else {
                            end($this->DSGItemRes->item->skus->sku)->quantity = $val->stock;
                        }
                        if (end($this->DSGItemRes->item->skus->sku)->quantity != 99) {
                            $totalQuantityNo99=$totalQuantityNo99+end($this->DSGItemRes->item->skus->sku)->quantity;
                            $totalCountNo99=$totalCountNo99+1;
                        }
                        end($this->DSGItemRes->item->skus->sku)->sku_id = $val->skuId;
                        if (isset($this->DSGItemRes->item->sku_promotions->{$name})) {
                            $promotions = $this->DSGItemRes->item->sku_promotions->{$name};

                            $PromotionPriceArray = Array();
                            foreach ($promotions as $promotion) {
                                if (isset($promotion->price)) {
                                    if ($promotion->price > 0) {
                                        $PromotionPriceArray[] = (float) $promotion->price;
                                    }
                                }
                            }
                            if (count($PromotionPriceArray) > 0) {
                                $promotion_price = min($PromotionPriceArray);
                            } else {
                                $promotion_price = 0;
                            }
                            if ($promotion_price > 0) {
                                end($this->DSGItemRes->item->skus->sku)->promotion_price = $promotion_price;
                            } else {
                                end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->price;
                            }
                        } elseif (isset($val->specPrice)) {
                            if ($val->specPrice == '' || !preg_match('/^[\d\.]+$/',$val->specPrice)) {
                                end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->price;
                            } else {
                                end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->specPrice;
                            }
                        } else {
                            end($this->DSGItemRes->item->skus->sku)->promotion_price = (float) $val->price;
                        }
                    }
// problem 99
                    if ($totalCountNo99>0) {
                        $totalAvgQuantityNo99=$totalQuantityNo99/$totalCountNo99;
                        if ($totalAvgQuantityNo99>99*1.4 || $totalAvgQuantityNo99<99/1.4) {
                            foreach ($this->DSGItemRes->item->skus->sku as $i => $sku) {
                                if ($this->DSGItemRes->item->skus->sku[$i]->quantity==99) {
                                    $this->DSGItemRes->item->skus->sku[$i]->quantity=0;
                                }
                            }
                        }
                    }
                    $this->clearSKUs();
                    unset($this->DSGItemRes->item->sku_promotions);
                    $PromotionPriceArray = Array();
                    foreach ($this->DSGItemRes->item->skus->sku as $sku) {
                        if (isset($sku->promotion_price)) {
                            if ($sku->promotion_price > 0) {
                                $PromotionPriceArray[] = $sku->promotion_price;
                            }
                        }
                    }
                    $this->DSGItemRes->item->promotion_price = min($PromotionPriceArray);
                    unset($PromotionPriceArray);
                    unset($sku);
                    unset($srcSkuArray);
                    unset($name);
                    unset($val);
                } else {
                    $tryToGetSku_promotions = true;
                }
            } else {
                $tryToGetSku_promotions = true;
            }
            if (isset($tryToGetSku_promotions) && $tryToGetSku_promotions) {
                if (isset($this->DSGItemRes->item->sku_promotions)) {
                    if (isset($this->DSGItemRes->item->sku_promotions->def)) {
                        if (is_array($this->DSGItemRes->item->sku_promotions->def)) {
                            $promotions = array();
                            foreach ($this->DSGItemRes->item->sku_promotions->def as $defPromotions) {
                                if (isset($defPromotions->price)) {
                                    $promotions[] = (float) $defPromotions->price;
                                }
                            }
                            if (count($promotions) > 0) {
                                $this->DSGItemRes->item->promotion_price = min($promotions);
                            }
                        }
                    }
                }
            }
            unset($skuFromSIB);
            unset($dldata);
//http://tui.taobao.com/recommend?appid=32&count=32&itemid=26733820522
// ==================================================================================
            unset($skudatajson);
        }
        $this->DSGItemRes->item->postage_id = '0';
        DSGCurl::closeCurl();
        if (class_exists('Profiler', false)) {
            Profiler::stop('DSGItem->parse');
        }
    }
    private function getPropCountInSKUs($pid,$vid) {
        $result=0;
        if (isset($this->DSGItemRes->item->skus->sku) && is_array($this->DSGItemRes->item->skus->sku)) {
            foreach ($this->DSGItemRes->item->skus->sku as $sku) {
                $regEx='/(^|;)'.$pid.':'.$vid.'(;|$)/';
                $props=$sku->properties;
                $isMatch=preg_match($regEx,$props);
                if ($isMatch){
                    $result=$result+$sku->quantity;
                }
            }
        }
        return $result;
    }
    private function clearSKUs () {
        if (isset($this->DSGItemRes->item->props) && is_array($this->DSGItemRes->item->props)) {
            foreach ($this->DSGItemRes->item->props as $i=>$prop) {
                if (isset($prop->childs) && is_array($prop->childs)) {
                    $newChilds=array();
                    $changed=false;
                    foreach ($prop->childs as $j=>$child) {
                       if (isset($child->vid) && $this->getPropCountInSKUs($i,$child->vid)>0) {
                           $newChilds[]=$this->DSGItemRes->item->props[$i]->childs[$j];
                       } else {
                           $changed=true;
                       }
                    }
                    if ($changed) {
                        $this->DSGItemRes->item->props[$i]->childs = $newChilds;
                    }
                }
            }
        }
        return;
    }
}
