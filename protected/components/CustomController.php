<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CustomFrontController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class CustomController extends CController {

  protected $vars;

  public function init() {
      if (!Yii::app()->request->isAjaxRequest && (is_a($this,'SearchController')||is_a($this,'SiteController')||is_a($this,'CategoryController'))) {
           ScheduledJobs::schedulerEvent();
      }
    $this->vars=new CustomControllerVars();
    parent::init();
  }
  private function renderVars($view,$data)
  {
    if (is_array($data)) {
      foreach ($data as $name=>$var) {
        $this->vars[$name]=$var;
      }
    }
    $this->vars->prepareReport();
  }

  /**
   * Renders a view with a layout.
   *
   * This method first calls {@link renderPartial} to render the view (called content view).
   * It then renders the layout view which may embed the content view at appropriate place.
   * In the layout view, the content view rendering result can be accessed via variable
   * <code>$content</code>. At the end, it calls {@link processOutput} to insert scripts
   * and dynamic contents if they are available.
   *
   * By default, the layout view script is "protected/views/layouts/main.php".
   * This may be customized by changing {@link layout}.
   *
   * @param string $view name of the view to be rendered. See {@link getViewFile} for details
   * about how the view script is resolved.
   * @param array $data data to be extracted into PHP variables and made available to the view script
   * @param boolean $return whether the rendering result should be returned instead of being displayed to end users.
   * @return string the rendering result. Null if the rendering result is not required.
   * @see renderPartial
   * @see getLayoutFile
   */
  public function render($view,$data=null,$return=false,$forceNoDebug=false)
  {
/*    if($this->beforeRender($view))
    {
      $output=$this->renderPartial($view,$data,true);
      if(($layoutFile=$this->getLayoutFile($this->layout))!==false)
        $output=$this->renderFile($layoutFile,array('content'=>$output),true);

      $this->afterRender($view,$output);

      $output=$this->processOutput($output);

      if($return)
        return $output;
      else
        echo $output;
    }
*/
      $footReport='';
      $headReport='';
    if(YII_DEBUG && !$forceNoDebug) {
      $this->renderVars($view,$data);
      $thisAction=get_class($this).'.'.$this->action->id;
      $reflector = new ReflectionClass(get_class($this));
      $thisFileName=$reflector->getFileName();
      $controllerReport="Action: ".$thisAction."\r\nFile: ".$thisFileName;
      $viewReport="View file: ".$this->getViewFile($view);
      $varsReport="View parameters:\r\n".$this->vars->report;
      $headReport = $this->markupReport($controllerReport."\r\n".$viewReport."\r\n".$varsReport);
      $footReport = $this->markupReport('End of action '.$thisAction);
    }
    if (isset($headReport) && isset($footReport)) {
      if ($return) {
        return $headReport.parent::render($view,$data,true).$footReport;
      } else {
      echo $headReport.parent::render($view,$data,true).$footReport;
      return;
      }
    }
    if($return) {
      return parent::render($view,$data,$return);
    } else {
      parent::render($view,$data,$return);
    }
  }

  /**
   * Renders a view.
   *
   * The named view refers to a PHP script (resolved via {@link getViewFile})
   * that is included by this method. If $data is an associative array,
   * it will be extracted as PHP variables and made available to the script.
   *
   * This method differs from {@link render()} in that it does not
   * apply a layout to the rendered result. It is thus mostly used
   * in rendering a partial view, or an AJAX response.
   *
   * @param string $view name of the view to be rendered. See {@link getViewFile} for details
   * about how the view script is resolved.
   * @param array $data data to be extracted into PHP variables and made available to the view script
   * @param boolean $return whether the rendering result should be returned instead of being displayed to end users
   * @param boolean $processOutput whether the rendering result should be postprocessed using {@link processOutput}.
   * @return string the rendering result. Null if the rendering result is not required.
   * @throws CException if the view does not exist
   * @see getViewFile
   * @see processOutput
   * @see render
   */
  public function renderPartial($view,$data=null,$return=false,$processOutput=false,$forceNoDebug=false)
  {
/*    if(($viewFile=$this->getViewFile($view))!==false)
    {
      $output=$this->renderFile($viewFile,$data,true);
      if($processOutput)
        $output=$this->processOutput($output);
      if($return)
        return $output;
      else
        echo $output;
    }
    else
      throw new CException(Yii::t('yii','{controller} cannot find the requested view "{view}".',
        array('{controller}'=>get_class($this), '{view}'=>$view)));
*/
      $footReport='';
      $headReport='';
      try {
    if(YII_DEBUG && !$forceNoDebug) {
      $this->renderVars($view,$data);
      $thisAction=get_class($this).'.'.$this->action->id;
      $reflector = new ReflectionClass(get_class($this));
      $thisFileName=$reflector->getFileName();
      $controllerReport="Action: ".$thisAction."\r\nFile: ".$thisFileName;
      $viewReport="View file: ".$this->getViewFile($view);
      $varsReport="View parameters:\r\n".$this->vars->report;
      $headReport = $this->markupReport($controllerReport."\r\n".$viewReport."\r\n".$varsReport);
      $footReport = $this->markupReport('End of action '.$thisAction);
    }
    if (isset($headReport) && isset($footReport)) {
      if ($return) {
        return $headReport.parent::renderPartial($view,$data,true,$processOutput).$footReport;
      } else {
        echo $headReport.parent::renderPartial($view,$data,true,$processOutput).$footReport;
        return;
      }
    }
    if($return) {
      return parent::renderPartial($view,$data,$return,$processOutput);
    } else {
      parent::renderPartial($view,$data,$return,$processOutput);
    }
      } catch (Exception $e) {
          print_r($e);
      }
  }

  private function markupReport($report) {
//    if (strpos($report,'<!DOCTYPE')!==false) {
      return "\r\n<!--\r\n".$report."-->\r\n";
//    } else {
//    return "\r\n<description hidden=\"true\">\r\n".$report."</description>\r\n";
//    }
  }

//=========================================================================
// Access rights management
//=========================================================================
  /**
   * The filter method for 'rights' access filter.
   * This filter is a wrapper of {@link CAccessControlFilter}.
   * @param CFilterChain $filterChain the filter chain that the filter is on.
   */
  public function filterRights($filterChain)
  {
    $filter = new RightsFilter;
    $filter->allowedActions = $this->allowedActions();
    $filter->filter($filterChain);
  }

  /**
   * @return string the actions that are always allowed separated by commas.
   */
  public function allowedActions()
  {
    return '';
  }

  /**
   * Denies the access of the user.
   * @param string $message the message to display to the user.
   * This method may be invoked when access check fails.
   * @throws CHttpException when called unless login is required.
   */
  public function accessDenied($message=null)
  {
    if( $message===null ) {
        $message = Yii::t('main', 'У Вас нет доступа к этой странице.');
    }

    $user = Yii::app()->user;
    if( $user->isGuest===true ) {
      $user->loginRequired();
    }
    else {
//    if (Yii::app()->request->isAjaxRequest) {
        throw new CHttpException(403, $message);
        return false;
    }
  }
}