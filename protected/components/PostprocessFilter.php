<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="PostprocessFilter.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class PostprocessFilter extends CFilter {
  protected $_allowedActions = array();

  /**
   * Performs the pre-action filtering.
   * @param CFilterChain $filterChain the filter chain that the filter is on.
   * @return boolean whether the filtering process should continue and the action
   * should be executed.
   */
  protected function preFilter($filterChain) {
    ob_start();
    return parent::preFilter($filterChain);
  }

  protected function postFilter($filterChain) {
    $content = ob_get_clean();
    $content = $this->filterTranslation($content);
    echo $content;
    return parent::preFilter($filterChain);
  }

  private function filterTranslation($content) {
    try {
      ini_set('pcre.backtrack_limit', 4*1024*1024);
      ini_set('pcre.recursion_limit', 1024*1024);
    if (DSConfig::getVal('translator_block_mode_enabled') == 1) {
      $result = $content;
//      unset($content);
      $res = preg_match_all('/(?:<|&lt;)translation.*?\stranslated=(?:"|&quot;)0(?:"|&quot;)\s.*?(?:<|&lt;)\/translation(?:>|&gt;)/is', $result, $matches, PREG_SET_ORDER);
      if ($res) {
        $res=Utils::getRemoteTranslation($matches);
// ==== end of post and get data to translator ====================
        if (($res->data) &&($res->info['http_code']<400)) {
        $translations=unserialize($res->data);
          if (($translations) && (is_array($translations))) {
            foreach ($translations as $translation) {
              if (isset($translation[1])) {
              $result=str_replace($translation[0],$translation[1],$result);
              }
            }
          }
        }
      }
      return $result;
    }
    else {
      return $content;
     }
    } catch (Exception $e) {
      return $content;
    }
  }

}
