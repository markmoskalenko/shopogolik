<?php
/**
 * Created by PhpStorm.
 * User: Alexys
 * Date: 26.09.14
 * Time: 11:23
 */

class DSEventableActiveRecord extends CActiveRecord {


    protected function beforeSave()
    {
        CmsEmailEvents::emailProcessEvents($this,($this->isNewRecord)?'beforeInsert':'beforeUpdate');
        Events::processEvents($this,get_class($this).'.'.(($this->isNewRecord)?'beforeInsert':'beforeUpdate'));
        if($this->hasEventHandler('onBeforeSave'))
        {
            $event=new CModelEvent($this);
            $this->onBeforeSave($event);
            return $event->isValid;
        }
        else
            return true;
    }

    /**
     * This method is invoked after saving a record successfully.
     * The default implementation raises the {@link onAfterSave} event.
     * You may override this method to do postprocessing after record saving.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterSave()
    {
        if($this->hasEventHandler('onAfterSave'))
            $this->onAfterSave(new CEvent($this));
        CmsEmailEvents::emailProcessEvents($this,($this->isNewRecord)?'afterInsert':'afterUpdate');
        Events::processEvents($this,get_class($this).'.'.(($this->isNewRecord)?'afterInsert':'afterUpdate'));
    }

    /**
     * This event is raised before the record is saved.
     * By setting {@link CModelEvent::isValid} to be false, the normal {@link save()} process will be stopped.
     * @param CModelEvent $event the event parameter
     */
    public function onBeforeSave($event)
    {
        $this->raiseEvent('onBeforeSave',$event);
    }

    /**
     * This event is raised after the record is saved.
     * @param CEvent $event the event parameter
     */
    public function onAfterSave($event)
    {
        $this->raiseEvent('onAfterSave',$event);
    }

}