<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DSGDsAPI.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

//require_once('.php');

/**
 * Class DSGItem
 *
 * Request example
 *
 * <?php
 * $req = new DSGItem();
 * $req->id = $iid;
 * $req->fromCart = FALSE;
 * $item_resp = $req->execute('item.taobao.com');
 */
class DSGDsAPI
{
    public static function Exec($user, $password, $command, $params)
    {
        if (method_exists('DSGDsAPI', $command)) {
            if ($command != 'checkAccess') {
                $access = self::checkAccess($user, $password, $params);
                if ($access !== 'OK') {
                    return $access;
                }
            }
            return self::$command($user, $password, $params);
        } else {
            return 'ERROR: command not found';
        }
    }

    /*
     http://777.danvit.net/dsapi/exec?user=apidemo@dropshop.pro&password=apidemo&command=checkAccess
     */
    private static function checkAccess($user, $password, $params)
    {
        $user = Users::model()->find(
          'email=:email and password=md5(:password)',
          array(':email' => $user, ':password' => $password)
        );
        if ($user && (Yii::app()->user->inRole(array('apiUser', 'superAdmin')))) {
            return 'OK';
        } else {
            return 'ERROR: access denied';
        }
    }

// http://777.danvit.net/dsapi/exec?user=apidemo@dropshop.pro&password=apidemo&command=getCatalogue
    private static function getCatalogue($user, $password, $params)
    {
        if (is_array($params)) {
            $intParams = $params;
        } else {
            $intParams = false;
        }
        if ($intParams && isset($intParams['lang'])) {
            $lang = Utils::TransLang($intParams['lang']);
        } else {
            $lang = Utils::TransLang('en');
        }
        $cache = Yii::app()->cache->get('API-MainMenu-getTree-' . $lang);
        if (($cache == false) || (DSConfig::getVal('search_cache_enabled') != 1)) {
            $mainMenu = MainMenu::getMainMenu(0, $lang, 1, array(0,1,2,3), 3, true);

            Yii::app()->cache->set(
              'API-MainMenu-getTree-' . $lang,
              array($mainMenu),
              60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other')
            );
        } else {
            list($mainMenu) = $cache;
        }
        return CJSON::encode($mainMenu);
    }

// http://777.danvit.net/dsapi/exec?user=apidemo@dropshop.pro&password=apidemo&command=getCatalogueList&params[lang]=ru
    private static function getCatalogueList($user, $password, $params)
    {
        if (!function_exists('createCatUrl')) {
            function createCatUrl($cat)
            {
                $urlArray = array(
                  'uniq'           => 'imgo',
                  'sort'           => 'popularity_desc',
                  'style'          => 'grid',
                  'mini'           => '1',
                  'filterFineness' => '2',
                  'atype'          => 'b',
                  'cph'            => 'yes',
                );
                if (isset($cat['query']) && $cat['query']) {
                    $urlArray['q'] = $cat['query'];
                }
                if (isset($cat['cid']) && $cat['cid']) {
                    $urlArray['cat'] = $cat['cid'];
                }
                if (!isset($urlArray['cat']) && !isset($urlArray['q'])) {
                    return '#';
                }
                $paramString = '';
                foreach ($urlArray as $name => $value) {
                    $paramString = $paramString . $name . '=' . $value . '&';
                }
                $urlString = 'http://s.taobao.com/search?' . trim($paramString, '&');
                return $urlString;
            }
        }
        if (is_array($params)) {
            $intParams = $params;
        } else {
            $intParams = false;
        }
        if ($intParams && isset($intParams['lang'])) {
            $lang = Utils::TransLang($intParams['lang']);
        } else {
            $lang = Utils::TransLang('en');
        }
        $cache = Yii::app()->fileCache->get('API-MainMenu-getList-' . $lang);
        if (($cache == false) || (DSConfig::getVal('search_cache_enabled') != 1)) { //|| TRUE

            $mainMenu = Yii::app()->db->createCommand(
              'select id as pkid,cid,parent,query,level,order_in_level,
                            ' . $lang . ' as view_text
             from categories_ext where status=1'
            )->queryAll();
            if ($mainMenu) {
                foreach ($mainMenu as $i => $cat) {
                    $mainMenu[$i]['url'] = createCatUrl($cat);
                }
            }
            Yii::app()->fileCache->set(
              'API-MainMenu-getList-' . $lang,
              array($mainMenu),
              60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other')
            );
        } else {
            list($mainMenu) = $cache;
        }

        return CJSON::encode($mainMenu);
    }

    /*
               'query'
              'props'
              'cid'
              'cid_query'
              'price_min'
              'price_max'
              'sort_by',
              'original',
              'recommend',
              'not_unique',
              'page'
    http://777.danvit.net/dsapi/exec?user=apidemo@dropshop.pro&password=apidemo&command=search
     */

    private static function search($user, $password, $params)
    {
        if (is_array($params)) {
            $intParams = $params;
        } else {
            $intParams = false;
        }
        if ($intParams && isset($intParams['lang'])) {
            $lang = Utils::TransLang($intParams['lang']);
        } else {
            $lang = Utils::TransLang('en');
        }
//======================================================================================================================
        if (!$intParams || ((!isset($intParams['query']) || !$intParams['query']) && (!isset($intParams['cid']) || !$intParams['cid']))) {
            return 'ERROR: uncorrect search query';
        }
        $search = new Search;
        $search->params = $intParams;
        $search->page = (isset($intParams['page']) && $intParams['page']) ? $intParams['page'] : 1;
        $res = $search->execute('global', $lang, true);
        $res->params = $intParams;
//======================================================================================================================
        return CJSON::encode($res);
    }
}
