<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CustomWidget.php">
* </description>
**********************************************************************************************************************/?>
<?php

class CustomWidget extends CWidget {

  protected $vars;
  public $frontTheme = '';
  public $frontThemePath = '';

  public function init() {
    $this->vars=new CustomControllerVars();
    $this->frontTheme = Yii::app()->theme->name;
      if ($this->frontTheme=='admin') {
          $this->frontTheme=DSConfig::getVal('site_front_theme');
      }
    $this->frontThemePath = Yii::app()->request->baseUrl . '/themes/' . $this->frontTheme;
      Yii::setPathOfAlias('themeBlocks',Yii::getPathOfAlias('webroot.themes').'/'.$this->frontTheme.'/views/widgets');
    parent::init();
  }

  private function renderVars($view,$data)
  {
    if (is_array($data)) {
      foreach ($data as $name=>$var) {
        $this->vars[$name]=$var;
      }
    }
    $this->vars->prepareReport();
  }

  /**
   * Renders a view.
   *
   * The named view refers to a PHP script (resolved via {@link getViewFile})
   * that is included by this method. If $data is an associative array,
   * it will be extracted as PHP variables and made available to the script.
   *
   * @param string $view name of the view to be rendered. See {@link getViewFile} for details
   * about how the view script is resolved.
   * @param array $data data to be extracted into PHP variables and made available to the view script
   * @param boolean $return whether the rendering result should be returned instead of being displayed to end users
   * @return string the rendering result. Null if the rendering result is not required.
   * @throws CException if the view does not exist
   * @see getViewFile
   */
  public function render($view,$data=null,$return=false,$forceNoDebug=false)
  {
/*    if(($viewFile=$this->getViewFile($view))!==false)
      return $this->renderFile($viewFile,$data,$return);
    else
      throw new CException(Yii::t('yii','{widget} cannot find the view "{view}".',
        array('{widget}'=>get_class($this), '{view}'=>$view)));
*/
   try {
   if(YII_DEBUG && !$forceNoDebug) {
     $this->renderVars($view,$data);
     $reflector = new ReflectionClass(get_class($this));
     $thisFileName=$reflector->getFileName();
     $controllerReport="Widget: ".get_class($this)."\r\nFile: ".$thisFileName;
     $viewReport="View file: ".$this->getViewFile($view);
     $varsReport="View parameters:\r\n".$this->vars->report;
     $headReport = $this->markupReport($controllerReport."\r\n".$viewReport."\r\n".$varsReport);
     $footReport = $this->markupReport('End of widget '.get_class($this));
   }
    if (isset($headReport) && isset($footReport)) {
        echo $headReport.parent::render($view,$data,true).$footReport;
//    $result=$headReport.parent::render($view,$data,true).$footReport;
//        if($processOutput)
//        $result=Yii::app()->controller->processOutput($result);
//        echo $result;
      return;
    }
    return parent::render($view,$data,$return);
   } catch (Exception $e) {
     echo 'Error in '.$this->getViewFile($view);
     return;
   }
  }

  private function markupReport($report) {
    return "\r\n<description hidden=\"true\">\r\n".$report."</description>\r\n";
  }

}