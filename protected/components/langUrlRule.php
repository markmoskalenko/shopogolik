<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="langUrlRule.php">
* </description>
**********************************************************************************************************************/?>
<?php

class langUrlRule extends CBaseUrlRule {

  public function createUrl($manager, $route, $params, $ampersand) {

    $lang = Utils::AppLang();
    if ($route == 'item/index') {
      $url = $lang . '/item/' . $params['iid'];
      if (isset($params['recentUser_dataProvider_page'])) {
        $url=$url.'/recentUser_dataProvider_page/'.$params['recentUser_dataProvider_page'];
      }
    }
/*    elseif (preg_match('/item\/(\d+)/i',$route, $matches)) {
      $url = $lang . '/item/' . $params['iid'];
    }
*/
    elseif ($route == 'category/index') {
      $url = $lang . '/category/' . $params['name'];
      unset($params['name'], $params['cid']);
      $p = '';
      foreach ($params as $k => $v) {
        if (($v!='')&&($v!=false)){
        $p .= '/' . $k . '/' . $v;
        }
      }
      $url .= $p;
    }
    elseif ($route == 'favorite/index') {
      $url = $lang . '/favorite/' . $params['name'];
      unset($params['name'], $params['cid']);
      $p = '';
      foreach ($params as $k => $v) {
        if (($v!='')&&($v!=false)){
          $p .= '/' . $k . '/' . $v;
        }
      }
      $url .= $p;
    }
    elseif ($route == 'category/page') {
      $url = $lang . '/category/page/' . $params['page_id'];
    }
    elseif ($route == 'favorite/page') {
      $url = $lang . '/favorite/page/' . $params['page_id'];
    }
    elseif ($route == 'article/index' || $route == 'article') {
      $url = $lang . '/article/' . $params['url'];
    }
    elseif ($route == 'brand/index') {
      if (isset($params['name'])) {
      $url = $lang . '/brand/' . $params['name'];
      unset($params['name']);
      } else {
        $url = $lang. '/search/index';
      }
      $p = '';
      foreach ($params as $k => $v) {
        $p .= '/' . $k . '/' . $v;
      }
      $url .= $p;
    }
    else {
      $p = '';
      foreach ($params as $k => $v) {
        $p .= '/' . $k . '/' . $v;
      }
      if (strpos($route . $p,$lang.'/')===0) {
      $url = $route . $p;
      } else {
        $url = $lang . '/' . $route . $p;
      }
    }
    $url = strtr($url, array('//' => '/'));
    return $url;
  }

  public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {
//
      if (preg_match('/^[\/]*dsapi\//is',$pathInfo)) {
          return $pathInfo;
      }
//-- process parse rules ------------------------------------------
      $banRule=Banrules::applyRules();
      if ($banRule!==false) {
          $pathInfo=$banRule;
      }
//-- end of process parse rules -----------------------------------
      $pathInfo=preg_replace('/((?:^|\/)(?:ru|en|zh|he|de|fr|es))_[a-z]{2}($|\/)/is','$1$2',$pathInfo);
    $pathInfo = explode('/', $pathInfo);
    if (isset($pathInfo[0])) {
      if (in_array($pathInfo[0],explode(',',DSConfig::getVal('site_language_block')))) {
        $this->checkLang($pathInfo[0]);
        unset($pathInfo[0]);
      }
    }
    if (isset($pathInfo[1])) {
      if (in_array($pathInfo[1],explode(',',DSConfig::getVal('site_language_block')))) {
        $this->checkLang($pathInfo[1]);
//        unset($pathInfo[1]);
      }
    }
    if (isset($pathInfo[2])) {
      if (in_array($pathInfo[2],explode(',',DSConfig::getVal('site_language_block')))) {
        $this->checkLang($pathInfo[2]);
//          unset($pathInfo[2]);
      }
    }
//======================================================================================================================
    if (isset($pathInfo[1]) && (($pathInfo[1] == 'category') || ($pathInfo[1] == 'favorite'))) {
      if (isset($pathInfo[2]) && $pathInfo[2] == 'article') {
        $pathInfo[2] .= '/page_id';
      }
      elseif (isset($pathInfo[2]) && $pathInfo[2] != 'list') {
        $pathInfo[1] .= '/index';
        if (gettype($pathInfo[2]) == 'string') {
          $pathInfo[2] = 'name/' . $pathInfo[2];
        }
        else {
          $pathInfo[2] = 'cid/' . $pathInfo[2];
        }
      }
    }
    if (isset($pathInfo[1]) && $pathInfo[1] == 'brand') {
      if (isset($pathInfo[2]) && $pathInfo[2] !== 'list') {
        $pathInfo[1] .= '/index';
        if (gettype($pathInfo[2]) == 'string') {
          $pathInfo[2] = 'name/' . $pathInfo[2];
        }
        else {
          $pathInfo[2] = 'cid/' . $pathInfo[2];
        }
      }
    }
    if (isset($pathInfo[1]) && $pathInfo[1] == 'item') {
      if (isset($pathInfo[2]) && (int) $pathInfo[2]) {
        $pathInfo[1] .= '/index';
        $pathInfo[2] = 'iid/' . $pathInfo[2];
      }
    }
    if (isset($pathInfo[1]) && $pathInfo[1] == 'article') {
      $pathInfo[1] .= '/index/url';
    }
      if (isset($pathInfo[0]) && $pathInfo[0] == 'article') {
          $pathInfo[0] .= '/index/url';
      }
    if ((isset($pathInfo[2]) && isset($pathInfo[0]) && $pathInfo[0] == 'user') && (isset($pathInfo[1]) && $pathInfo[1] == 'setlang')) {
      $pathInfo[2] = 'lang/' . $pathInfo[2];
    }
//======================================================================================================================
    $res=implode('/', $pathInfo);
    return $res;
  }

  function checkLang($lang) {
    if (!isset(Yii::app()->request->cookies['lang'])) {
        $lang=substr(Yii::app()->request->getPreferredLanguage(),0,2);
    }
    if (!in_array($lang, explode(',',DSConfig::getVal('site_language_block')))) {
      $lang = Yii::app()->sourceLanguage;
    }
//        $controllerId = Yii::app()->controller;
//      echo $controllerId;
    Yii::app()->language = $lang;
      if (Yii::app()->getBaseUrl(true)== 'http://'.DSConfig::getVal('site_domain')) {
          $cookie = new CHttpCookie('lang', $lang);
          $cookie->expire = 3600 * 24 * 180;
          Yii::app()->request->cookies['lang'] = $cookie;
      }
  }
}