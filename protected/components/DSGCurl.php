<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="DSGCurl.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* Класс, реализующий работу с библиотекой curl с некоторыми
   специфическими особенностями.                                               */
class DSGCurl {
//    private $debugLog=true;

  /* Использовать ли http-proxy для обращений к сайту-источнику
     или к DSG-прокси.
     
     0|1
     
     Работает в паре с параметром proxy_http_address                             */
   public static $proxy_http_enabled = FALSE;
  //public static $proxy_http_enabled = TRUE;
  /* Адрес и порт http-proxy для обращений к сайту-источнику или к
     DSG-прокси.
     
     Например, 'proxy.test.com:8080'
     
     Работает в паре с параметром proxy_http_enabled                                */
 // public static $proxy_http_address = '128.199.128.217:3128';
   public static $proxy_http_address = '10.0.0.1:3128';
  /* Использовать ли специализированный внешний DSG-прокси.
     
     0|1
     
     Работает в паре с параметром proxy_address                                  */
  public static $proxy_enabled = FALSE;
  /* Адрес специализированного внешнего DSG-прокси.
     
     Строковое значение. Например 'proxy2.dropshop.pro'
     
     Работает в паре с параметром proxy_enabled                   */
  public static $proxy_address = 'proxy2.dropshop.pro';
  /* Использовать ли режим отладки */
  public static $debug = FALSE;
  /* Переменная, в которой хранится экземпляр ресурса curl для его
     повторного использования в режиме keep-alive                                    */
  private static $_curlObject = FALSE;

  /* Summary
     Функция для скачивания контента заданного URL
     Description
     $path - URL, содержимое которого необходимо скачать
     
     $urlPreserved = FALSE - параметры (как часть URL), которые
     необходимо сохранять при редиректах.
     
     Например, необходимо сохранять номер страницы и порядок
     сортировки, тогда $urlPreserved может быть
     '&amp;page=10&amp;sord=desc'
     
     $direct = FALSE - принудительно скачивать без использования
     любых прокси.
     Returns
     Возвращает stdClass() $res, где
     
     $res-\>info = curl_getinfo($ch) - информация о закачке
     
     $res-\>data = - собственно, закачанные данные
                                   */
  public static function getHttpDocumentArray($urls, $direct = FALSE,$referer='http://taobao.com/') {
    set_time_limit(120);
    try {
      if (class_exists('Profiler',false)) {
        Profiler::message('curl multi', print_r($urls,true));
      }
//----------------------------------------
      if (class_exists('DSConfig',false)) {
        self::$proxy_enabled = DSConfig::getVal('proxy_enabled') == 1;
        self::$proxy_address = DSConfig::getVal('proxy_address');
        self::$debug = DSConfig::getVal('site_debug') == 1;
      }
      $proxy_enabled = (self::$proxy_enabled && !$direct);
//----------------------------------------
      $multi = curl_multi_init();
      $channels = array();
// Loop through the URLs, create curl-handles
// and attach the handles to our multi-request
      foreach ($urls as $url) {
        $ch = curl_init();

        if ($proxy_enabled) {
          $newurl = $url;
        if (class_exists('DSConfig', FALSE)) {
          self::$proxy_address =DSConfig::getVal('proxy_address');
        }
        $proxy_address = self::$proxy_address;
        $newurl = 'http://' . $proxy_address . '/index.php?url=' . urlencode($newurl) . '&pver=' . DSGParserClass::getParserVersion(FALSE);
        curl_setopt($ch, CURLOPT_URL, $newurl);
        } else {
          curl_setopt($ch, CURLOPT_URL, $url);
        }

//!!        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0');
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, ''); //gzip,deflate
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, FALSE);
// Speed-up
        //curl_setopt($ch, CURLOPT_TCP_NODELAY, TRUE);
        curl_setopt($ch, CURLOPT_NOPROGRESS, TRUE);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 3600);
//---------
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        if (class_exists('DSConfig',false)) {
          self::$proxy_http_enabled = DSConfig::getVal('proxy_http_enabled') == 1;
          self::$proxy_http_address = DSConfig::getVal('proxy_http_address');
        }
        if (self::$proxy_http_enabled) {
          curl_setopt($ch, CURLOPT_PROXY, self::$proxy_http_address);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//? может быть и нет
        curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_multi_add_handle($multi, $ch);
        $channels[$url] = $ch;
      }
    } catch (Exception $e) {
      throw new Exception (510, 'System error: curl not installed or inited.');
    }

// While we're still active, execute curl
    $active = null;
    do {
      $mrc = curl_multi_exec($multi, $active);
    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

    while ($active && $mrc == CURLM_OK) {
      // Wait for activity on any curl-connection
      if (curl_multi_select($multi) == -1) {
        continue;
      }

      // Continue to exec until curl is ready to
      // give us more data
      do {
        $mrc = curl_multi_exec($multi, $active);
      } while ($mrc == CURLM_CALL_MULTI_PERFORM);
    }

// Loop through the channels and retrieve the received
// content, then remove the handle from the multi-handle
    $results=array();
    foreach ($channels as $i=>$channel) {
      $results[$i]= new stdClass();
      $results[$i]->info=curl_getinfo($channel);
      $ret=curl_multi_getcontent($channel);
      curl_multi_remove_handle($multi, $channel);
      if ($direct || ($results[$i]->info['http_code'] >= 400)) {
        if (isset($ret)) {
          $results[$i]->data = $ret;
        }
        else {
          $results[$i]->data = FALSE;
        }
      }
      else {
        $results[$i]->data = @iconv('GBK', 'UTF-8//IGNORE', $ret);
      }
      unset ($ret);
    }
// Close the multi-handle and return our results
    curl_multi_close($multi);
    return $results;
  }

  public static function getHttpDocument($path, $urlPreserved = FALSE, $direct = FALSE,$referer='http://taobao.com/') {
    $ch = self::getCurl($path,$referer);
    $ret = self::download($ch, 20, $urlPreserved, $direct);
    $res = new stdClass();
    $res->info = curl_getinfo($ch);
    $res->data = $ret;
    if ($direct || ($res->info['http_code'] >= 400)) {
      if (isset($ret)) {
        $res->data = $ret;
      }
      else {
        $res->data = FALSE;
      }
    }
    if (isset($res->info['content_type']) && preg_match('/charset=GBK/i',$res->info['content_type'])) {
      $res->data = @iconv('GBK', 'UTF-8//IGNORE', $ret);
    }
    unset ($ret);
    return $res;
  }

  public static function getFromProxyV2($class) {
    if (class_exists('DSConfig', FALSE)) {
      self::$proxy_address =DSConfig::getVal('proxy_address');
    }
    $url = 'http://' . self::$proxy_address . '/index.php?url=' .get_class($class);
    if (isset($class->search_DSGRulesList)) {
        $tmpRulesList=$class->search_DSGRulesList;
      $class->search_DSGRulesList='DSG_rulesList.xml';
    }
    $class->forceProtocolV2=false;
    $post =urlencode(serialize($class));
//    $_class=unserialize(urldecode($post));
    $ch = self::getCurl($url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'data='.$post);
//    print_r($class);die;
//    print_r('data='.$post);die;
//    curl_setopt($ch, CURLOPT_VERBOSE, true);
//    curl_setopt($ch, CURLOPT_STDERR, fopen(dirname(__FILE__) . "/headers.txt", 'w+'));
    //curl_setopt($ch, CURLOPT_NOBODY, TRUE);
    //curl_setopt($ch, CURLOPT_HEADER, TRUE);
//===============================================
    $fHeader = fopen('php://temp', 'w+');
    $fBody = fopen('php://temp', 'w+');
    curl_setopt($ch,CURLOPT_WRITEHEADER,$fHeader);
    curl_setopt($ch,CURLOPT_FILE, $fBody);
//===============================================
//================== COOKIES =============================
      $cookieBuffer = array();
      if (isset(Yii::app()->request->cookies['cookie2'])) {
          $cookieBuffer['cookie2']=Yii::app()->request->cookies['cookie2']->value;
      }
      if (isset(Yii::app()->request->cookies['t'])) {
          $cookieBuffer['t']=Yii::app()->request->cookies['t']->value;
      }
      if (isset(Yii::app()->request->cookies['v'])) {
          $cookieBuffer['v']=Yii::app()->request->cookies['v']->value;
      }
      curl_setopt($ch, CURLOPT_COOKIE, implode("; ",$cookieBuffer) );
//========================================================
    curl_exec($ch);
    $code = curl_getinfo($ch);
    if (curl_errno($ch)>0) {
        if (isset($tmpRulesList)) {
      $class->search_DSGRulesList=$tmpRulesList;
        }
      $ret=CVarDumper::dumpAsString($code,10,false);
//        echo $ret; die;
      throw new CHttpException(575, curl_error($ch).' '.$ret);
    }
    fseek($fBody,0);
    $fStat=fstat($fBody);
    $ret = false;
    if ($fStat['size']>0) {
      $ret = fread($fBody,$fStat['size']);
    }
//===============================================
    fseek($fHeader,0);
    $fStat = fstat($fHeader);
    if ($fStat['size']>0) {
      $headers = fread($fHeader,$fStat['size']);
      if (preg_match('/Content\-Time:\s*(\d+)/i',$headers,$headerVal)) {
        if (class_exists('Profiler', FALSE)) {
      Profiler::message('Proxy Content-Time',$headerVal[1]);
        }
      }
//===============================================
        preg_match_all("/^(Set-cookie:\s*.*?)$/ims", $headers, $cookies);
        $domain=preg_replace('/^http[s]*:\/\//is','',Yii::app()->getBaseUrl(true));
        foreach( $cookies[1] as $cookie ){
            $localCockie=preg_replace('/\.taobao\.com/i',$domain,$cookie);
            header($localCockie);
//            $buffer_explode = strpos($cookie, "=");
//            self::$cookies[ substr($cookie,0,$buffer_explode) ] = substr($cookie,$buffer_explode+1);
        }
//===============================================

    }
    fclose($fHeader);
    fclose($fBody);
    unset ($fStat);
//===============================================
    $res = new stdClass();
    $res->info = curl_getinfo($ch);
    if (($res->info['http_code'] >= 400)) {
      if (isset($ret)) {
        $res->data = $ret;
      }
      else {
        $res->data = FALSE;
      }
    }
    else {
      $res->data = unserialize($ret);
    }
    unset ($ret);
    self::closeCurl();
    return $res;
  }

  /* Summary
     Возвращает ресурс curl для использования в функции download
     Description
     $path - строковый параметр, ссылка (URL) на ресурс, который
     необходимо скачать.
     Returns
     Возвращает ресурс curl с необходимыми установками.          */
  private static function getCurl($path,$referer='http://taobao.com/') {
    try {
      if (class_exists('Profiler',false)) {
        Profiler::message('curl path', $path);
      }
      if (gettype(self::$_curlObject) != 'resource') {
        $ch = curl_init($path);
        if (class_exists('Profiler',false)) {
          Profiler::message('curl', 'new');
        }
        self::$_curlObject = $ch;
      }
      else {
        $ch = self::$_curlObject;
        if (class_exists('Profiler',false)) {
          Profiler::message('curl', 'reuse');
        }
        curl_setopt($ch, CURLOPT_URL, $path);
      }
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0');
      curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
      curl_setopt($ch, CURLOPT_ENCODING, ''); //gzip,deflate
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);//120
      curl_setopt($ch, CURLOPT_FORBID_REUSE, FALSE);
// Speed-up
      //curl_setopt($ch, CURLOPT_TCP_NODELAY, TRUE);
      curl_setopt($ch, CURLOPT_NOPROGRESS, TRUE);
      curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 3600);
//      curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTP);
//---------
      curl_setopt($ch, CURLOPT_REFERER, $referer);
//      curl_setopt ($ch,CURLOPT_COOKIEFILE,'cookiefile.txt');
      curl_setopt ($ch,CURLOPT_COOKIEJAR,'/dev/null');

      if (class_exists('DSConfig',false)) {
        self::$proxy_http_enabled = DSConfig::getVal('proxy_http_enabled') == 1;
        self::$proxy_http_address = DSConfig::getVal('proxy_http_address');
      }
      if (self::$proxy_http_enabled) {
        curl_setopt($ch, CURLOPT_PROXY, self::$proxy_http_address);
      }

      $header = array();
      $header[] = "Connection: keep-alive";
      $header[] = "Keep-Alive: 30";
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

      return $ch;
    } catch (Exception $e) {
      throw new Exception (520, 'System error: curl not installed or inited.');
    }
  }

  /* Summary
     Внутренняя реализация загрузки документа при помощи curl с
     учетом авторизации, редиректов и других необходимых
     особенностей.
     Description
     $ch - ресурс curl
     
     $maxredirect = NULL - максимальное количество редиректов.
     Если NULL то 20 по умолчанию
     
     $urlPreserved = FALSE - параметры (как часть URL), которые
     необходимо сохранять при редиректах.
     
     Например, необходимо сохранять номер страницы и порядок
     сортировки, тогда $urlPreserved может быть
     '&amp;page=10&amp;sord=desc'
     
     $direct = FALSE - принудительно скачивать без использования
     любых прокси.
     Returns
     При успешном завершении будет возвращен результат, а при неудаче - FALSE. */
  private static function download($ch,$maxredirect = NULL, $urlPreserved = FALSE, $direct = FALSE) {
    set_time_limit(120);
    $mr = $maxredirect === NULL ? 20 : intval($maxredirect);
    $open_basedir = ini_get('open_basedir');
    $safe_mode = ini_get('safe_mode');
    if (class_exists('DSConfig',false)) {
      self::$proxy_enabled = DSConfig::getVal('proxy_enabled') == 1;
      self::$proxy_address = DSConfig::getVal('proxy_address');
      self::$debug = DSConfig::getVal('site_debug') == 1;
    }
    $proxy_enabled = (self::$proxy_enabled && !$direct);
    if (!$proxy_enabled) {
      if (($open_basedir == '') && (in_array($safe_mode, array('0', 'Off'))||!$safe_mode)) {
        curl_setopt($ch, CURLOPT_MAXREDIRS, $mr); //
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);//
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
      }
      else {
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        if ($mr > 0) {
          $newurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
          $originalUrl = $newurl;
          $rch = curl_copy_handle($ch);
          curl_setopt($rch, CURLOPT_HEADER, TRUE);
          curl_setopt($rch, CURLOPT_NOBODY, TRUE);
          curl_setopt($rch, CURLOPT_FORBID_REUSE, FALSE);
          curl_setopt($rch, CURLOPT_RETURNTRANSFER, TRUE);
          do {
            curl_setopt($rch, CURLOPT_URL, $newurl);
            $header = curl_exec($rch);
            if (curl_errno($rch)) {
              $code = 0;
            }
            else {
              $code = curl_getinfo($rch, CURLINFO_HTTP_CODE);
              if ($code == 301 || $code == 302) {
                preg_match('/Location:(.*?)[\n\r]/i', $header, $matches);
//                $oldurl=$newurl;
                $url = trim(array_pop($matches));
                if (preg_match('/http[s]*:\/\/.+?(?=\/)/i', $url)) {
                  $newurl = $url;
                }
                else {
                  preg_match('/http[s]*:\/\/.+?(?=\/)/i', $newurl, $m);
                  $newurl = trim(array_pop($m)) . $url;
                }
              }
              else {
                $code = 0;
              }
            }
          } while ($code && --$mr);
          curl_close($rch);
          if (!$mr) {
            if ($maxredirect === NULL) {
                throw new CHttpException(570, 'Too many redirects. When following redirects, libcurl hit the maximum amount.');
              //trigger_error('Too many redirects. When following redirects, libcurl hit the maximum amount.', E_USER_WARNING);
            }
            return FALSE;
          }
          $debug = self::$debug;
          if ($debug) {
            echo '<div>';
            echo '<pre>' . $originalUrl . '</pre>';
          }
          if (($urlPreserved) && ($newurl != $originalUrl)) {
            $newurl = $newurl . '&' . $urlPreserved;
            if ($debug) {
              echo '<pre>' . $newurl . '</pre>';
            }
          }
          if ($debug) {
            echo '</div>';
          }
          curl_setopt($ch, CURLOPT_URL, $newurl);
        }
      }
    }
    else {
      $newurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
      $originalUrl = $newurl;
      if (class_exists('DSConfig', FALSE)) {
        self::$proxy_address =DSConfig::getVal('proxy_address');
      }
      $proxy_address = self::$proxy_address;
      $debug = self::$debug;
      if ($debug) {
        echo '<pre>' . $originalUrl . '</pre>';
      }
      $newurl = 'http://' . $proxy_address . '/index.php?url=' . urlencode($newurl) . '&pver=' . DSGParserClass::getParserVersion(FALSE);
      if ($debug) {
        echo '<pre>' . $newurl . '</pre>';
      }
      curl_setopt($ch, CURLOPT_URL, $newurl);
    }
//================== COOKIES =============================
      $cookieBuffer = array();
      if (isset(Yii::app()->request->cookies['cookie2'])) {
          $cookieBuffer['cookie2']=Yii::app()->request->cookies['cookie2']->value;
      }
      if (isset(Yii::app()->request->cookies['t'])) {
          $cookieBuffer['t']=Yii::app()->request->cookies['t']->value;
      }
      if (isset(Yii::app()->request->cookies['v'])) {
          $cookieBuffer['v']=Yii::app()->request->cookies['v']->value;
      }
      curl_setopt($ch, CURLOPT_COOKIE, implode("; ",$cookieBuffer) );
//========================================================
/*      if( count(self::$cookies) > 0 ){
          $cookieBuffer = array();
          foreach(  self::$cookies as $k=>$c ) $cookieBuffer[] = "$k=$c";
          curl_setopt($ch, CURLOPT_COOKIE, implode("; ",$cookieBuffer) );
      }
*/
    $execRes=curl_exec($ch);
    if (curl_errno($ch)>0) {
      throw new CHttpException(530, curl_error($ch));
    } else {

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header      = substr($execRes, 0, $header_size);
        $execRes = substr($execRes, $header_size);
        preg_match_all("/^(Set-cookie:\s*.*?)$/ims", $header, $cookies);
        $domain=preg_replace('/^http[s]*:\/\//is','',Yii::app()->getBaseUrl(true));
        foreach( $cookies[1] as $cookie ){
            $localCockie=preg_replace('/\.taobao\.com/i',$domain,$cookie);
            header($localCockie);
//            $buffer_explode = strpos($cookie, "=");
//            self::$cookies[ substr($cookie,0,$buffer_explode) ] = substr($cookie,$buffer_explode+1);
        }
      return $execRes;
    }
  }

  /* Summary
     Функция безопасно закрывает общий ресурс curl в переменной
     _curlObject                                                                                               */
  public static function closeCurl() {
    if (gettype(self::$_curlObject) == 'resource') {
      curl_close(self::$_curlObject);
      if (class_exists('Profiler',false)) {
        Profiler::message('curl', 'closed');
      }
    }
  }
}