<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DSGSeller.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

require_once('DSGParserClass.php');

/**
 * Class DSGItem
 *
 * Request example
 *
 * <?php
 * $req = new DSGItem();
 * $req->id = $iid;
 * $req->fromCart = FALSE;
 * $item_resp = $req->execute('item.taobao.com');

 */
class DSGSeller extends DSGParserClass
{
    public $srcUrl = false;
    private $DSGSellerRes;

    function __construct($debugExt = false)
    {
        parent::__construct($debugExt);
    }

    public function execute($name = 'rate.taobao.com')
    {
//        try {
        $this->initRulesList();
        $proxyV2result = parent::execute($name);
        if ($proxyV2result) {
            return $proxyV2result;
        }
        $this->getDSGRules($name, array());
        if (class_exists('Profiler', false)) {
            Profiler::start('DSGSeller->download');
        }
        $data = DSGCurl::getHttpDocument($this->srcUrl,false,false,$this->referer); //
        if (class_exists('Profiler', false)) {
            Profiler::stop('DSGSeller->download');
        }
//    $this->debugMessage( 'getHttpDocument', $this->itemParams->url, NULL, NULL, $data, TRUE);
        if ($data->data === null) {
            $data->data = false;
        }
        $data->url = $this->srcUrl;
        
        $this->DSGSellerRes = new stdClass();
        $this->DSGSellerRes->html = $data;
        if ($data->data == false) {
            return $this->DSGSellerRes;
        }
        $this->DSGSellerRes->seller = new stdClass();
        $this->DSGSellerRes->seller->fromDSG = true;
        $this->parseSeller($data);
        unset($this->DSGSellerRes->html);
        $this->DSGSellerRes->debugMessages = $this->debugMessages;
        $this->verifyResult($this->DSGSellerRes);
        return $this->DSGSellerRes;
/*        } catch (Exception $e) {
            print_r($e);die;
        } */
    }

    private function parseSeller($data)
    {
       if (class_exists('Profiler', false)) {
            Profiler::start('DSGSeller->parse');
        }
        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/nick', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->nick = urldecode($resstr[1]);
        }
        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/wangwang', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->wangwang = urldecode($resstr[1]);
        }

        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/location', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->location = urldecode($resstr[1]);
        }
        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/date', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->date = $resstr[1];
        }
        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/sales', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->sales = $resstr[1];
            $this->DSGSellerRes->seller->crowns = self::getCrownsFromSales($this->DSGSellerRes->seller->sales);
        }
        $this->DSGSellerRes->seller->rates=array();
        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/rate_description_q', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->rates['description_q'] = $resstr[1];
        }

        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/rate_service_q', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->rates['service_q'] = $resstr[1];
        }

        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/rate_delivery_q', $data->data, $resstr);
        if ($res > 0) {
            $this->DSGSellerRes->seller->rates['delivery_q'] = $resstr[1];
        }
        $this->DSGSellerRes->seller->reviews=array();
        $resstr = array();
        $res = $this->regexMatch('parse_seller_values/review_block', $data->data, $resstr);
        if ($res > 0) {
            $review_block = $resstr[1];
            $resstr = array();
            $res = $this->regexMatch('parse_seller_values/review_positive', $review_block, $resstr);
            if ($res > 0) {
                $this->DSGSellerRes->seller->reviews['positive'] = $resstr[1];
            }
            $resstr = array();
            $res = $this->regexMatch('parse_seller_values/review_normal', $review_block, $resstr);
            if ($res > 0) {
                $this->DSGSellerRes->seller->reviews['normal'] = $resstr[1];
            }
            $resstr = array();
            $res = $this->regexMatch('parse_seller_values/review_negative', $review_block, $resstr);
            if ($res > 0) {
                $this->DSGSellerRes->seller->reviews['negative'] = $resstr[1];
            }
        }
        unset($review_block);
        unset($resstr);
        DSGCurl::closeCurl();
        if (class_exists('Profiler', false)) {
            Profiler::stop('DSGSeller->parse');
        }
    }
    public static function getCrownsFromSales($sales) {
        switch ($sales) {
            case $sales>100000000: $res=22; break;
            case $sales>10000000: $res=21; break;
            case $sales>5000000: $res=20; break;
            case $sales>2000000: $res=19; break;
            case $sales>1000000: $res=18; break;
            case $sales>500000: $res=17; break;
            case $sales>200000: $res=16; break;
            case $sales>100000: $res=15; break;
            case $sales>50000: $res=14; break;
            case $sales>20000: $res=13; break;
            case $sales>10000: $res=12; break;
            case $sales>5000: $res=11; break;
            case $sales>2000: $res=10; break;
            case $sales>1000: $res=9; break;
            case $sales>500: $res=8; break;
            case $sales>250: $res=7; break;
            case $sales>150: $res=6; break;
            case $sales>90: $res=5; break;
            case $sales>40: $res=4; break;
            case $sales>10: $res=3; break;
            case $sales>4: $res=2; break;
            default:$res=1; break;
        }
        return $res;
    }
    public static function getSalesFromCrowns($crowns) {
        switch ($crowns) {
            case $crowns>=22: $res=100000001; break;
            case $crowns>=21: $res=10000001; break;
            case $crowns>=20: $res=5000001; break;
            case $crowns>=19: $res=2000001; break;
            case $crowns>=18: $res=1000001; break;
            case $crowns>=17: $res=500001; break;
            case $crowns>=16: $res=200001; break;
            case $crowns>=15: $res=100001; break;
            case $crowns>=14: $res=50001; break;
            case $crowns>=13: $res=20001; break;
            case $crowns>=12: $res=10001; break;
            case $crowns>=11: $res=5001; break;
            case $crowns>=10: $res=2001; break;
            case $crowns>=9: $res=1001; break;
            case $crowns>=8: $res=501; break;
            case $crowns>=7: $res=251; break;
            case $crowns>=6: $res=151; break;
            case $crowns>=5: $res=91; break;
            case $crowns>=4: $res=41; break;
            case $crowns>=3: $res=11; break;
            case $crowns>=2: $res=5; break;
            default:$res=1; break;
        }
        return $res;
    }
}
