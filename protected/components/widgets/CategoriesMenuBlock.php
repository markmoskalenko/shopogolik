<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CategoriesMenuBlock.php">
* </description>
**********************************************************************************************************************/?>
<?
class CategoriesMenuBlock extends CustomWidget {
    public $adminMode=false;
    public $lang=false;
    function run()
    {
        if (!$this->lang) {
        $lang = Utils::TransLang();
        } else {
            $lang=$this->lang;
        }
      $cache = Yii::app()->cache->get('MainMenu-getTree-' . $lang);
      if (($cache == FALSE)) { //||(DSConfig::getVal('search_cache_enabled')!=1)
        $mainMenu = MainMenu::getMainMenu(0,$lang,1,array(1,3));
        Yii::app()->cache->set('MainMenu-getTree-' . $lang, array($mainMenu), 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other'));
      }
      else {
        list($mainMenu) = $cache;
      }
            $this->render('themeBlocks.CategoriesMenuBlock.CategoriesMenuBlock',
              array('mainMenu'=>$mainMenu,'adminMode'=>$this->adminMode));
    }
    
}