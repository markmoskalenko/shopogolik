<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SearchQueryBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class SearchQueryBlock extends CustomWidget {
  public function run() {
    $lang = Utils::TransLang();
      $cache = Yii::app()->cache->get('MainMenu-getTree-' . $lang);
      if (($cache == FALSE)) { //||(DSConfig::getVal('search_cache_enabled')!=1)
        $cats = MainMenu::getMainMenu(0,$lang,1,array(1,3));
        Yii::app()->cache->set('MainMenu-getTree-' . $lang, array($cats), 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other'));
      }
      else {
        list($cats) = $cache;
      }
    if (isset($_GET['name'])) {
        $c = MainMenu::model()->find(' url=:url', array(':url' => $_GET['name']));
      if (isset($c)) {
          $cid = $c->id;
      }
      else {
        $cid = '';
      }
    }
    else {
      $cid = '';
    }
    $query = (isset($_GET['query'])) ? addslashes(strip_tags($_GET['query'])) : '';
    //---------------------------------------------------------
    $this->render('themeBlocks.SearchQueryBlock.SearchQueryBlock', array('cats' => $cats,'query' => $query, 'cid' => $cid));
  }

}