<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrderPaymentsBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class OrderPaymentsBlock extends CustomWidget {

  public $orderId = FALSE;
  public $userId = FALSE;
  public $pageSize = 5;
  public function run() {
    if (($this->orderId===false) && ($this->userId===false)) {
      return;
    }
      $dataProvider =OrdersPayments::getOrderPayments($this->orderId, $this->pageSize,false,$this->userId);
      $blockId = 'order-payments-' . $this->orderId.'-'.$this->userId;
    $this->render('themeBlocks.OrderPaymentsBlock.OrderPaymentsBlock', array(
      'dataProvider' => $dataProvider,
      'blockId' => $blockId,
    ));
  }
}
