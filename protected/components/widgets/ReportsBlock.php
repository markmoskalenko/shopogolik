<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ReportsBlock.php">
* </description>
**********************************************************************************************************************/?>
<?
class ReportsBlock extends CustomWidget {
  public $title;
  public $group = FALSE;
  public $uid = FALSE;
  public function run() {
    if (!$this->group) {
      return;
    }
    $reports=ReportsSystem::getReports($this->group);
    if ($reports) {
    $this->render('application.modules.admin.views.widgets.ReportsBlock.ReportsBlock', array(
      'title'=>$this->title,
      'reports' => $reports,
    ));
  }
  }
}
?>