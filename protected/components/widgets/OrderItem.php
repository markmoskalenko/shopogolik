<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="OrderItem.php">
 * </description>
 **********************************************************************************************************************/
?>
<?

class OrderItem extends CustomWidget
{

    public $orderItem = false;
    public $readOnly = false;
    public $adminMode = false;
    public $allowDelete = true;
    public $publicComments = false;
    public $imageFormat = '_160x160.jpg';
    public $lazyLoad = null;
    public $order = false;
    public $light = false;
    public $dialog = false;
    public static $_order = false;

    public function run()
    {
        if (!$this->orderItem) {
            return;
        }
        if ($this->lazyLoad === null) {
            $this->lazyLoad = DSConfig::getVal('site_images_lazy_load') == 1;
        }
        if ($this->adminMode && (isset($this->orderItem->oid))) {
            if ($this->order) {
                self::$_order = $this->order;
            }
            if (!self::$_order) {
                self::$_order = Order::getOrder($this->orderItem->oid, null, null);
            }
            $this->readOnly = (self::$_order->frozen == 0) ? false : true;
            $viewPath = 'application.modules.admin.views.OrderItem.OrderItemAdmin';
        } else {
            self::$_order = false;
            $viewPath = 'themeBlocks.OrderItem.OrderItem';
        }
        $this->render(
          $viewPath,
          array(
            'item'           => $this->orderItem,
            'order'          => self::$_order,
            'readOnly'       => $this->readOnly,
            'allowDelete'    => $this->allowDelete,
            'adminMode'      => $this->adminMode,
            'imageFormat'    => $this->imageFormat,
            'publicComments' => $this->publicComments,
            'lazyLoad'       => $this->lazyLoad,
            'light' => $this->light,
            'dialog' => $this->dialog,
          )
        );
    }
}
