<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="MessagesBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class MessagesBlock extends CustomWidget {
    
    public function run()
    {
        $messages = Yii::app()->user->getFlashes();
        if(count($messages))
        $this->render('themeBlocks.MessagesBlock.messages',array('messages'=>$messages));
    }
}