<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SearchItem.php">
* </description>
**********************************************************************************************************************/?>
<?

class SearchItem extends CustomWidget {

  public $number = NULL;
  public $searchResItem = FALSE;
  public $newLine = NULL;
  public $showControl = NULL;
  public $disableItemForSeo = true;
  public $imageFormat = '_160x160.jpg';
  public function run()
    {
      if (!$this->searchResItem) {
        return;
      }
      if (is_null($this->newLine)) {
        $this->newLine = '';
      }
      if (is_null($this->showControl)) {
        $this->showControl = Yii::app()->user->checkAccess('admin/Featured/Add');
      }

//        echo Yii::getPathOfAlias('themeBlocks.SearchItem.SearchItem');exit;
        $this->render('themeBlocks.SearchItem.SearchItem',array(
          'number' => $this->number,
          'item'          => $this->searchResItem,
          'newLine' => $this->newLine,
          'showControl' => $this->showControl,
          'disableItemForSeo' => $this->disableItemForSeo,
          'imageFormat' => $this->imageFormat,
          'lazyLoad' => DSConfig::getVal('site_images_lazy_load')==1,
        ));
    }
}
