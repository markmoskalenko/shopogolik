<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="cabinetMenuBlock.php">
* </description>
**********************************************************************************************************************/?>
<?
class cabinetMenuBlock extends CustomWidget {

    function run()
    {
        $newAnswer = Message::model()->count('uid=:uid AND status = 2',array(':uid'=>Yii::app()->user->id));
        $render = array(
            'newAnswer'=>$newAnswer,
        );
        $this->render('themeBlocks.cabinetMenuBlock.cabinetMenuBlock',$render);
    }
    
}