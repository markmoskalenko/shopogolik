<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrderCommentsBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class OrderCommentsBlock extends CustomWidget {

  public $orderId = FALSE;
  public $orderItemId = FALSE;
  public $public = TRUE;
  public $showInternals = 0;
  public $pageSize = 5;
  public $imageFormat = '_160x160.jpg';

  public function run() {
    if ($this->orderId) {
      $dataProvider = OrdersComments::getOrderComments($this->orderId, $this->showInternals, $this->pageSize);
      $blockId = 'order-comments-' . $this->orderId;
      $parentId = $this->orderId;
      $isItem = FALSE;
    }
    elseif ($this->orderItemId) {
      $dataProvider = OrdersItemsComments::getOrderItemComments($this->orderItemId, $this->showInternals, $this->pageSize);
      $blockId = 'order-item-comments-' . $this->orderItemId;
      $parentId = $this->orderItemId;
      $isItem = TRUE;
    }
    else {
      return;
    }
    $this->render('themeBlocks.OrderCommentsBlock.OrderCommentsBlock', array(
      'dataProvider' => $dataProvider,
      'blockId' => $blockId,
      'isItem' => $isItem,
      'parentId' => $parentId,
      'public' => $this->public,
    ));
/*      echo '<script type="text/javascript">
$(function () {
        $("#new-message-'.$blockId.'").dialog({
            autoOpen: false,
            width: "auto",
            modal: true,
            resizable: false
        });
        $("#accordion-connemts-'.$blockId.'").accordion({
            collapsible: true,
            active: '.(($dataProvider->totalItemCount>0) ? '0' : 'false').'
});
});
</script>';
*/
  }
}
