<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchItemsList.php">
 * </description>
 **********************************************************************************************************************/
?>
<?

class SearchItemsGallery extends CustomWidget
{
    public $id = 'searchItemsGallery';
    public $dataProvider = false;

//  public $searchResItem = FALSE;
//  public $newLine = NULL;
    public $showControl = null;
    public $controlAddToFavorites = false;
    public $controlAddToFeatured = false;
    public $controlDeleteFromFavorites = false;
    public $disableItemForSeo = true;
    public $imageFormat = '_160x160.jpg';
    public $dataType = false;
    public $totalCount=100;
    public $magicScrollExtraOptions ="'items': 4,'width': 720,'speed': 4000,'height': 180,'arrows': 'outside','arrows-opacity': 20,
                                      'arrows-hover-opacity': 100,'step': 4,'item-tag': 'div'";

    public function run()
    {
        if (!$this->dataProvider && !$this->dataType) {
            return;
        }
        if (!$this->dataProvider) {
            switch ($this->dataType) {
                case 'itemsRecommended':
                    $this->dataProvider = Search::getFeatured(Utils::TransLang(), 'recommended', $this->totalCount, true);
                    break;
                case 'itemsPopular':
                    $this->dataProvider = Search::getFeatured(Utils::TransLang(), 'popular', $this->totalCount, true);
                    break;
                case 'itemsRecentUser':
                    $this->dataProvider = Search::getFeatured(Utils::TransLang(), 'recentUser', $this->totalCount, true);
                    break;
                case 'itemsRecentAll':
                    $this->dataProvider = Search::getFeatured(Utils::TransLang(), 'recentAll', $this->totalCount, true);
                    break;
                case 'itemsFavorite':
                    $this->dataProvider = Search::getFeatured(Utils::TransLang(), 'favorite', $this->totalCount, true);
                    break;
                default:
                    return;
                    break;
            }
        }
        if (!$this->dataProvider) {
            echo Yii::t('main', 'Нет данных');
            return;
        }
        if (is_null($this->showControl)) {
            $this->showControl = Yii::app()->user->checkAccess('Favorite/Add');
        }

        $this->render(
          'webroot.themes.' . DSConfig::getVal(
            'site_front_theme'
          ) . '.views.widgets.SearchItemsGallery.SearchItemsGallery',
          array(),
          false
        );
    }
}
