<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="BrandsBlock.php">
* </description>
**********************************************************************************************************************/?>
<?
class BrandsBlock extends CustomWidget {
    
    function run()
    {
      $brands=Brands::model()->findAll(array('order'=>'RAND()', 'condition'=>'enabled=1'));
      $this->render('themeBlocks.BrandsBlock.BrandsBlock',array('brands'=>$brands));
    }
    
}