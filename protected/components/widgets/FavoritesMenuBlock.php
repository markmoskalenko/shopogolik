<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="FavoritesMenuBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class FavoritesMenuBlock extends CustomWidget {
    public $adminMode=false;
    function run()
    {
        $lang = Utils::TransLang();
        $uid=Yii::app()->user->id;
      $cache = Yii::app()->cache->get('Favorites-menu-getTree-' . $lang.'-'.$uid);
      if (($cache == FALSE)) { //||(DSConfig::getVal('search_cache_enabled')!=1)
        $favoriteMenu = Favorite::getFavoriteMenu($uid,$lang);
        Yii::app()->cache->set('Favorites-menu-getTree-' . $lang.'-'.$uid, array($favoriteMenu), 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other'));
      }
      else {
        list($favoriteMenu) = $cache;
      }
            $this->render('themeBlocks.FavoritesMenuBlock.FavoritesMenuBlock',
              array('favoriteMenu'=>$favoriteMenu));
    }
    
}