<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="sliderBlock.php">
* </description>
 * Рендеринг слайдера баннеров на главной
**********************************************************************************************************************/?>
<?
class sliderBlock extends CustomWidget {
    
    function run()
    {
      $banners = Banners::model()->findAll('enabled=1');
      $this->render('themeBlocks.sliderBlock.sliderBlock',array('banners'=>$banners));
    }
    
}