<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2014
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="EventsBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class EventsBlock extends CWidget {

  public $subjectId = NULL;
  public $eventsType = NULL;
  public $showInternal = true;
  public $pageSize = 5;

  public function run() {
    $dataProvider = EventsLog::getEvents($this->subjectId, $this->eventsType, $this->pageSize,false,$this->showInternal);
    $blockId = 'events-' . $this->subjectId;
    $this->render('themeBlocks.EventsBlock.EventsBlock', array(
      'dataProvider' => $dataProvider,
      'blockId' => $blockId,
    ));
  }
}
