<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="userBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class userBlock extends CustomWidget {
    
    function run()
    {
        $isGuest = Yii::app()->user->isGuest;
        if(!$isGuest) {
          if (isset(Yii::app()->user->firstname)) {
            $user = Yii::app()->user->firstname;
          } else {
            $user = '';
          }
        } else {
          $user = '';
        }
        
        $this->render('themeBlocks.userBlock.userBlock',array('guest'=>$isGuest,'user'=>$user));
    }
    
}