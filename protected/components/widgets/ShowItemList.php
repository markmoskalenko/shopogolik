<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="ShowItemsList.php">
 * </description>
 **********************************************************************************************************************/?>
<?

class ShowItemList extends CustomWidget {
    public $id = 'search-items-list';
    public $dataProvider = false;

//  public $searchResItem = FALSE;
//  public $newLine = NULL;
    public $showControl = null;
    public $controlAddToFavorites = false;
    public $controlAddToFeatured = false;
    public $controlDeleteFromFavorites = false;
    public $disableItemForSeo = true;
    public $imageFormat = '_160x160.jpg';
    public $lazyLoad = true;
    public $template = '{pager}{items}';//'{pager}{items}{pager}'; //{sorter}{summary}
    public $dataType=false;
    public $pageSize =8;
    public $title='<span></span>';
    public $prevPageLabel='&lt;';//'<i class="fa fa-angle-left fa-fw"></i>',
    public $nextPageLabel='&gt;';//'<i class="fa fa-angle-right fa-fw"></i>',
    public $pagerCssClass ='pagination';// 'box-heading',
    public $viewName = 'ShowItemList';

    public function run()
    {
        if (!$this->dataProvider && !$this->dataType) {
            return;
        }
        if (!$this->dataProvider) {
            switch($this->dataType) {
                case 'itemsRecommended':$this->dataProvider=Search::getFeatured(Utils::TransLang(),'recommended',$this->pageSize);
                    break;
                case 'itemsPopular':$this->dataProvider=Search::getFeatured(Utils::TransLang(),'popular',$this->pageSize);
                    break;
                case 'itemsRecentUser': $this->dataProvider=Search::getFeatured(Utils::TransLang(),'recentUser',$this->pageSize);
                    break;
                case 'itemsRecentAll': $this->dataProvider=Search::getFeatured(Utils::TransLang(),'recentAll',$this->pageSize);
                    break;
                case 'itemsFavorite':$this->dataProvider=Search::getFeatured(Utils::TransLang(),'favorite',$this->pageSize);
                    break;
                default: return; break;
            }
        }
        if (!$this->dataProvider) {
            echo Yii::t('main','Нет данных');
            return;
        }
        if (is_null($this->showControl)) {
            $this->showControl = Yii::app()->user->checkAccess('Favorite/Add');
        }

        if (DSConfig::getVal('site_images_lazy_load')==0) {
            $this->lazyLoad=false;
        }

        if ($this->lazyLoad) {
                    $afterAjaxUpdate="js:function(id, data) {
            $('#".$this->id." img.lazy').show().lazyload({
              effect       : 'fadeIn',
              effect_speed : 500,
              skip_invisible : false
            });
            $(window).scroll();
            }";
        } else {
            $afterAjaxUpdate="js:function(id, data) {
        }";
        }

        $this->render('themeBlocks.ShowItemList.'.$this->viewName,array(
            'id'=>$this->id,
            'dataProvider'=>$this->dataProvider,
            'itemView'=>'webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.widgets.SearchItemsList.SearchItem', //themeBlocks.
            'viewData'=>array('showControl'=>$this->showControl,'disableItemForSeo'=>$this->disableItemForSeo,'imageFormat'=>$this->imageFormat,
            'controlAddToFavorites'=>$this->controlAddToFavorites,'controlAddToFeatured'=>$this->controlAddToFeatured,
            'controlDeleteFromFavorites'=>$this->controlDeleteFromFavorites,'lazyLoad'=>$this->lazyLoad),
            'imageFormat' => $this->imageFormat,
            'enableSorting'=>false,
            'itemsCssClass'=>'products-list',
            'template'=>$this->template,
            'pagerCssClass' =>$this->pagerCssClass,
            'pager' =>array(
                'header'=>$this->title,
                'firstPageLabel' => '',
                'lastPageLabel' => '',
                'prevPageLabel'=>$this->prevPageLabel,
                'nextPageLabel'=>$this->nextPageLabel,
            ),
            'afterAjaxUpdate' => $afterAjaxUpdate,
        ));
    }
}
