<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ReportBlock.php">
* </description>
**********************************************************************************************************************/?>
<?
class ReportBlock extends CustomWidget {
  public $report = FALSE;
  public function run() {
    if ($this->report) {
    $this->render('application.modules.admin.views.widgets.ReportBlock.ReportBlock', array(
      'report' => $this->report,
    ));
    }
  }
}
?>