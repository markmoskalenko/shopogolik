<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="currencyBlock.php">
* </description>
**********************************************************************************************************************/?>
<?
class languageBlock extends CustomWidget {
    
    function run()
    {
        $lang_array=explode(',',DSConfig::getVal('site_language_block'));
        $this->render('themeBlocks.languageBlock.languageBlock',array('lang_array'=>$lang_array));
    }
    
}