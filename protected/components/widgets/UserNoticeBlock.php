<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="UserNoticeBlock.php">
* </description>
**********************************************************************************************************************/?>
<?

class UserNoticeBlock extends CustomWidget {
    
    public function run()
    {
        $notices = UserNotice::model()->findAll('uid=:uid',array(':uid'=>Yii::app()->user->id));  
        if(count($notices))
        $this->render('themeBlocks.UserNoticeBlock.UserNotice',array('notices'=>$notices));
    }
    
}