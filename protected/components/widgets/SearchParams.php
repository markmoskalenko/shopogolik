<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="SearchParams.php">
 * </description>
 **********************************************************************************************************************/
?>
<?

class SearchParams extends CustomWidget {

  public $type = 'search';
  public $items = false;
  public $params = array();
  public $cids = array();
  public $bids = array();
  public $groups = array();
  public $filters = array();
  public $multiFilters = array();
  public $suggestions = array();
  public $priceRange = array();
  public $cid = 0;

  public function run() {
    if ((bool) $this->params === TRUE) {
      $cur_props = array();
      if (!empty($filters) && isset($this->params['props'])) {
        $ps = explode(';', str_replace('%3B', ';', $this->params['props']));
        foreach ($ps as $p) {
          if ($p !== '') {
            $pp = str_replace('%3A', ':', $p);
            list($k, $v) = explode(':', $pp);
            $cur_props[$k] = $v;
          }
        }
      }
        list($minPrice,$maxPrice,$stepPrice)=CategoriesPrices::getPricesRangeForArray($this->priceRange,$this->items);

      $this->render('themeBlocks.SearchParams.SearchParams', array(
        'type'         => $this->type,
        'cids'         => $this->cids,
        'groups'       => $this->groups,
        'filters'      => $this->filters,
        'multiFilters' => $this->multiFilters,
        'suggestions'  => $this->suggestions,
        'priceRange'   => $this->priceRange,
        'params'       => $this->params,
        'cur_props'    => $cur_props,
        'minPrice'=>$minPrice,
        'maxPrice'=>$maxPrice,
        'stepPrice'=>$stepPrice,
      ));
    }
  }
}
