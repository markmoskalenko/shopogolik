<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="cms.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

// Класс, реализующий cms-функции
class cms {
  public static function pageLink($id, $label = FALSE, $htmlOptions = array()) {
    $res = preg_split('/#/', $id);
    if (count($res) == 1) {
      $page_id = $res[0];
      $anchor = array();
    }
    elseif (count($res) == 2) {
      $page_id = $res[0];
      $anchor = array('#' => $res[1]);
    }
    else {
      $page_id = $id;
      $anchor = array();
    }
    $pageQuery = Yii::app()->db->createCommand("select pp.id, pp.page_id, pp.url, pp.SEO, pc.lang, pc.title
       -- pc.description,pc.keywords,pc.content_data
       from cms_pages_content pc, cms_pages pp
       where pp.page_id=:page_id and pp.page_id=pc.page_id and (pc.lang=:lang or pc.lang='*') and pp.enabled=1");
    $pageQuery->params = array(':page_id' => $page_id, ':lang' => Utils::AppLang());
    $page = $pageQuery->queryRow();
    if ($page) {
//      $intId=$page['id'];
      $href = Yii::app()->controller->createUrl('/article/' . $page['url'], $anchor);
      $labelText = $page['title'];
      if ($page['SEO'] != 1) {
        $htmlOptions['rel'] = 'nofollow';
      }
    }
    else {
//      $intId=$id;
      $href = '#';
      $labelText = '';
    }
    if (!isset($htmlOptions['href'])) {
      $htmlOptions['href'] = $href;
    }
    if ($label) {
      $labelText = $label;
    }
    $link = '<a ' . join(' ', array_map(function ($sKey) use ($htmlOptions) {
        if (is_bool($htmlOptions[$sKey])) {
          return $htmlOptions[$sKey] ? $sKey : '';
        }
        return $sKey . '="' . $htmlOptions[$sKey] . '"';
      }, array_keys($htmlOptions))) . '>' . $labelText . '</a>';
    return $link;
  }

  public static function menuContent($id, $SEO = TRUE, $plain = false) {
    $cache = Yii::app()->cache->get('cmsMenuContent-' . $id);
    $menuQuery = Yii::app()->db->createCommand('select mm.id, mm.menu_id, mm.menu_data, mm.enabled, mm.SEO from cms_menus mm
       where mm.menu_id=:menu_id and mm.enabled=1');
    $menuQuery->params = array(':menu_id' => $id);
    $menu = $menuQuery->queryRow();
    if ($menu) {
    $intId=$menu['id'];
    } else {
      $intId=$id;
    }
    if (($cache == FALSE) || (Yii::app()->user->notInRole(array('guest', 'user')))) {
      if ($menu) {
        try {
          $content = self::render($menu['menu_data']);
          if ((!$SEO) || ($menu['SEO'] != 1)) {
            $content = '<noindex>' . $content . '</noindex>';
          }
          Yii::app()->cache->set('cmsMenuContent-' . $id, $content, 600);
          return ($plain)?$content:self::wrap($content,'cmsMenus',$intId);
        } catch (Exception $e) {
          return self::wrap(print_r($e, TRUE),'cmsMenus',$intId);
        }
      }
      else {
        return self::wrap('<span>cms::menuContent('.$id.')</span>','cmsMenus',$intId);
      }
    }
    else {
      return self::wrap($cache,'cmsMenus',$intId);
    }
  }

  public static function pageContent($id, $SEO = TRUE) {
    $cache = Yii::app()->cache->get('cmsPageContent-' . $id);
    $pageQuery = Yii::app()->db->createCommand("select pc.id, pp.page_id, pp.url, pp.SEO, pc.lang, pc.title,
       pc.description,pc.keywords,pc.content_data
       from cms_pages_content pc, cms_pages pp
       where pp.page_id=:page_id and pp.page_id=pc.page_id and (pc.lang=:lang or pc.lang='*') and pp.enabled=1");
    $pageQuery->params = array(':page_id' => $id, ':lang' => Utils::AppLang());
    $page = $pageQuery->queryRow();
    if ($page) {
      $intId=$page['id'];
    } else {
      $intId=$id;
    }
    if (($cache == FALSE) || (Yii::app()->user->notInRole(array('guest', 'user')))) {
      if ($page) {
        try {
          $content = self::render($page['content_data']);
          if ((!$SEO) || ($page['SEO'] != 1)) {
            $content = '<noindex>' . $content . '</noindex>';
          }
          $result = new stdClass();
          $result->title = $page['title'];
          $result->description = $page['description'];
          $result->keywords = $page['keywords'];
          $result->content = $content;
          Yii::app()->cache->set('cmsPageContent-' . $id, $result, 600);
          return cms::wrap($result,'cmsPagesContent',$intId);
        } catch (Exception $e) {
          return self::wrap(print_r($e, TRUE),'cmsPagesContent',$intId);
        }
      }
      else {
        return self::wrap('<div>cms::pageContent('.$id.')</div>','cmsPagesContent',$intId);
      }
    }
    else {
      return self::wrap($cache,'cmsPagesContent',$intId);
    }
  }

  public static function customContent($id, $SEO = TRUE,$plain=false) {
    $cache = Yii::app()->cache->get('cmsCustomContent-' . $id);
    $customQuery = Yii::app()->db->createCommand("select cc.id, cc.content_data from cms_custom_content cc
       where cc.content_id=:content_id and (cc.lang=:lang or cc.lang='*') and cc.enabled=1");
    $customQuery->params = array(':content_id' => $id, ':lang' => Utils::AppLang());
    $custom = $customQuery->queryRow();
    if ($custom) {
      $intId=$custom['id'];
    } else {
      $intId=$id;
    }
    if (($cache == FALSE) || (Yii::app()->user->notInRole(array('guest', 'user')))) {
      if ($custom) {
        try {
          $content = self::render($custom['content_data']);
          if (!$SEO) {
            $content = '<noindex>' . $content . '</noindex>';
          }
          Yii::app()->cache->set('cmsCustomContent-' . $id, $content, 600);
          return ($plain)?$content:self::wrap($content,'cmsCustomContent',$intId);
        } catch (Exception $e) {
          return self::wrap(print_r($e, TRUE),'cmsCustomContent',$intId);
        }
      }
      else {
        if ($plain) {
          $result='cms::customContent(\''.$id.'\')';
        } else {
          $result=self::wrap('<div>cms::customContent('.$id.')</div>','cmsCustomContent',$intId);
        }
        return $result;
      }
    }
    else {
      return ($plain)?$cache:self::wrap($cache,'cmsCustomContent',$intId);
    }
  }

public static function getLangList(){
  $langs=explode(',',DSConfig::getVal('site_language_block'));
  $res=array('*'=>'*');
  foreach ($langs as $lang) {
    $res[$lang]=$lang;
  }
  return $res;
}

  private static function render($text) {
    $open_basedir = ini_get('open_basedir');
    $safe_mode = ini_get('safe_mode');
    try {
      if ($open_basedir || $safe_mode) {
        $tFileName = tempnam(YiiBase::getPathOfAlias('webroot').'/upload/', 'php') or die('could not create file');
        $tempFile = fopen($tFileName,'w+');
      } else {
      $tempFile = tmpfile();
      }
      $metaDatas = stream_get_meta_data($tempFile);
      $tmpFilename = $metaDatas['uri'];
      fwrite($tempFile, $text);
//      fseek($tempFile, 0);
      ob_start();
      ob_implicit_flush(FALSE);
      include $tmpFilename;
      $content = ob_get_clean();
      fclose($tempFile); // this removes the file
      if (isset($tFileName)) {
        unlink($tFileName);
      }
      return $content;
    } catch (Exception $e) {
      return print_r($e, TRUE);
    }
  }
  private static function wrap($content,$type,$intId) {
    //http://777.danvit.net/admin/main/open?url=admin/users/view/id/2136&tabName=test@mail.ru

//====== SEO ===============
      if (isset($content->keywords) && $content->keywords) {
          Yii::app()->clientScript->registerMetaTag($content->keywords, 'keywords', NULL,array('lang' => Utils::AppLang()),'page-keywords');
      }
      if (isset($content->description) && $content->description) {
          Yii::app()->clientScript->registerMetaTag($content->description, 'description', NULL,array('lang' => Utils::AppLang()),'page-description');
      }
      if (isset($content->title) && $content->title) {
          Yii::app()->controller->pageTitle = $content->title;
      }
//==========================

    if (Yii::app()->user->checkAccess('admin/'.$type.'/index')) {
      if (isset($content->content)){
        $res=$content;
        $res->content='<span class="editable" title="'.Yii::t('main','Двойной клик для редактирования').'" ondblclick="window.open(\'/admin/main/open?url=admin/'.$type.'/update/id/'.$intId.'&tabName='.$type.' '.$intId.'\')">'.$res->content.'</span>';
      } else {
      $res='<span class="editable" title="'.Yii::t('main','Двойной клик для редактирования').'" ondblclick="window.open(\'/admin/main/open?url=admin/'.$type.'/update/id/'.$intId.'&tabName='.$type.' '.$intId.'\')">'.$content.'</span>';
      }
      return $res;
    } else {
      return $content;
    }
  }
}