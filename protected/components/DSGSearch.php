<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DSGSearch.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php
require_once('DSGParserClass.php');

/**
 * Class DSGSearch
 *
 * <?php
 * $req = new DSGSearch();
 * $req->lang = $lang;
 * if ($query != FALSE) {
 * $req->q = $query;
 * }
 * if (!empty($this->params['price_min'])) {
 * $req->startPrice = $this->params['price_min'];
 * }
 * else {
 * $req->startPrice = 0;
 * }
 * if (!empty($this->params['price_max'])) {
 * $req->endPrice = $this->params['price_max'];
 * }
 * else {
 * $req->endPrice = 999999;
 * }
 * if (isset($this->params['original'])) {
 * if ($this->params['original'] == 'on') {
 * $req->tianmao = TRUE;
 * }
 * }
 * if (isset($this->params['not_unique'])) {
 * if ($this->params['not_unique'] == 'on') {
 * $req->not_unique = TRUE;
 * }
 * }
 * if ((!empty($this->params['recommend'])) && ($this->params['recommend'] == 1)) {
 * $req->recommend = TRUE;
 * }
 * if (isset($this->params['cid'])) {
 * if ($this->params['cid'] > 0) {
 * $req->cat = $this->params['cid'];
 * }
 * }
 * if (isset($this->params['props'])) {
 * if (!empty($this->params['props'])) {
 * $req->props = $this->params['props'];
 * }
 * }
 * $req->pageSize = Search::getPageSize(); //12
 * $in_page_no = $this->page;
 * $req->pageNo = $in_page_no;
 * if (!empty($this->params['sort_by'])) {
 * $req->sort = $this->params['sort_by'];
 * }
 * $DSGSearchRes = $req->execute();

 */
class DSGSearch extends DSGParserClass
{
    public $q = ''; /* Поисковый запрос на китайском языке. */
    public $ex_q = ''; /* Исключить данный текст из результатов поиска. Значение - на
                        китайском.                                                                                          */
    public $nick = '';
    public $startPrice = '';
    public $endPrice = '';
    public $startCredit = '';
    public $endCredit = '';
    public $tianmao = false; /* Искать только товары со скидкой. Если да, передаем значение 1 */
    public $not_unique = false; /* Группировать ли одинаковые, повторяющиеся товары. Если да -
                                 передаем значение 1.                                                                         */
    public $recommend = false; /* Поиск только по рекомендованным товарам (с высоким рейтингом
                                и сервисом продавца). Если да - значение 1                                        */
    public $sort = '';
    public $cat = '';
    public $props = '';
    public $pageSize = 44;
    public $pageNo = 1;
    public $search_use_related_queries = 1;
    public $search_use_related_cats = 1;
    public $search_use_parsed_filters = 1;
    public $translator_long_list_count_limit = 20;
    private $searchParams;
    private $DSGSearchRes;

    function __construct($debugExt = false)
    {
        parent::__construct($debugExt);
        if (class_exists('DSConfig', false)) {
            $this->search_use_related_queries = DSConfig::getVal('search_use_related_queries') == 1;
            $this->search_use_related_cats = DSConfig::getVal('search_use_related_cats') == 1;
            $this->search_use_parsed_filters = DSConfig::getVal('search_use_parsed_filters') == 1;
            $this->translator_long_list_count_limit = DSConfig::getVal('translator_long_list_count_limit');
        }
    }

    public function execute($name = 's.taobao.com/json')
    {
//------------------------------------------
        $this->initRulesList();
//      return false;
        $forceNoProxy=false;
            if ($this->pageNo <= 10) {
                $proxyV2result = parent::execute($name);
                if ($proxyV2result) {
                    return $proxyV2result;
                }
            } else {
                $forceNoProxy = true;
            }
        $query = array();
        if (isset($this->q) && ($this->q != '')) {
            $query['q'] = $this->q;
        }
        if (isset($this->cat) && ($this->cat > 0)) {
            $query['cat'] = $this->cat;
        } else {
            $query['cat'] = 0;
        }
        if (isset($this->props) && ($this->props != '') && ($this->props)) {
            $query['props'] = $this->props;
        }
        if (isset($this->pageNo) && ($this->pageNo > 1)) {
            $query['start_item_num'] = ($this->pageNo - 1) * $this->pageSize;
        }
        if (isset($this->sort) && ($this->sort != '')) {
            $query['sort_order'] = $this->sort;
        } else {
            $query['sort_order'] = 'popularity_desc';
        }
        if (isset($this->tianmao) && ($this->tianmao)) {
            $query['tianmao'] = 1;
        }
        if ((!(isset($this->not_unique) && ($this->not_unique)))) { // && (!(isset($this->tianmao) && ($this->tianmao)))
            $query['uniq'] = 'imgo';
        }

        if (isset($this->startPrice) && (isset($this->endPrice))) {
            $query['filter'] = '[' . $this->startPrice . ',' . $this->endPrice . ']';
        }
        if (isset($this->recommend) && ($this->recommend)) {
            $query['isprepay'] = 1;
            $query['user_type'] = 1;
            $query['tianmao'] = 1;
            $query['support_xcard'] = 1;
        }
        $this->searchParams = $this->getDSGRules($name, $query);
        if ($this->searchParams->debug) {
            echo '<div><pre>' . $this->searchParams->url . '</pre></div>';
        }
        //$data = $this->phantom_execute('taobao.js', $this->searchParams->url);
        if (class_exists('Profiler', false)) {
            Profiler::start('DSGSearch->download');
        }
        $data = DSGCurl::getHttpDocument($this->searchParams->url, $this->searchParams->urlPreservedPart,$forceNoProxy,$this->referer);
        DSGCurl::closeCurl();
        if (class_exists('Profiler', false)) {
            Profiler::stop('DSGSearch->download');
        }

        if ($data->data === null) {
            $data->data = 'TIMEOUT OR ALERT';
        }
        $items_count = $this->regexMatchAll('loaded_items_count_check', $data->data);
        $data->url = $this->searchParams->url;
        if (!$items_count) {
            $data->items_count = 0;
        } else {
            $data->items_count = $items_count;
        }
        $this->DSGSearchRes = new stdClass();
        $this->DSGSearchRes->html = $data;
        $this->DSGSearchRes->items = array();
        if ($data->data == false) {
            return $this->DSGSearchRes;
        }
        if (class_exists('Profiler', false)) {
            Profiler::start('DSGSearch->parse');
        }
        if ($name == 's.taobao.com') {
            $resstr = array();
            $res = $this->regexMatch('parse_items_count', $data->data, $resstr);
            if ($res > 0) {
                $resnum = DSGUtils::parseChineseNumber($resstr[1]);
            } else {
                $resnum = 0;
            }
            $this->DSGSearchRes->total_results = $resnum;
            $this->regexReplaceCallback(
              'parse_items_block',
              array(
                $this,
                'parseItem'
              ),
              $this->DSGSearchRes->html->data,
              $this->pageSize
            );

            $this->DSGSearchRes->intCategories = array();
            $this->DSGSearchRes->intGroups = array();
            $this->DSGSearchRes->intFilters = array();
            $this->DSGSearchRes->intMultiFilters = array();
            $this->DSGSearchRes->intSuggestions = array();
            $this->DSGSearchRes->intPriceRanges = array();

            if ($this->search_use_related_cats) {
                $this->parseCategories($data->data);
            }
            if ($this->search_use_parsed_filters) {
                $this->parseGroups($data->data);
                $this->parseFilters($data->data);
                $this->parseMultiFilters($data->data);
            }
            if ($this->search_use_related_queries) {
                $this->parseSuggestions($data->data);
            }
            $this->parsePriceRanges($data->data);
            unset($data);
            unset($resstr);
            unset($this->searchParams);
            unset($this->DSGSearchRes->html);
            unset($this->search_DSGRulesList);
        } elseif ($name == 's.taobao.com/json') {
            $resstr = array();
            $res = $this->regexMatch('loaded_data_section', $data->data, $resstr);
            if ($res > 0) {
                $jsonData = $this->JavaScriptToJSON($resstr[1],true,true);
            } else {
                $this->DSGSearchRes->total_results=0;
                if (class_exists('Profiler', false)) {
                    Profiler::stop('DSGSearch->parse');
                }
                $this->DSGSearchRes->debugMessages = $this->debugMessages;
                return $this->DSGSearchRes;
            }
            $res=$this->jsonObjGet('parse_items_count',$jsonData);
            if (!$res) {
                $this->DSGSearchRes->total_results=0;
                if (class_exists('Profiler', false)) {
                    Profiler::stop('DSGSearch->parse');
                }
                $this->DSGSearchRes->debugMessages = $this->debugMessages;
                return $this->DSGSearchRes;
            } else {
                $this->DSGSearchRes->total_results = $res;
            }
            $itemsList= $res=$this->jsonObjGet('parse_items_block',$jsonData);
            if (is_array($itemsList)) {
                foreach ($itemsList as $item) {
                    $this->parseItemFromJson($item);
                }
            }
            unset($itemsList);
            $this->DSGSearchRes->intCategories = array();
            $this->DSGSearchRes->intGroups = array();
            $this->DSGSearchRes->intFilters = array();
            $this->DSGSearchRes->intMultiFilters = array();
            $this->DSGSearchRes->intSuggestions = array();
            $this->DSGSearchRes->intPriceRanges = array();

            if ($this->search_use_related_cats) {
              //  $this->parseCategoriesFromJson($jsonData);
            }
            if ($this->search_use_parsed_filters) {
              $this->parseGroupsFromJson($jsonData);
              $this->parseFiltersFromJson($jsonData);
              //  $this->parseMultiFiltersFromJson($jsonData);
            }
            if ($this->search_use_related_queries) {
                $this->parseSuggestionsFromJson($jsonData);
            }
            $this->parsePriceRangesFromJson($jsonData);
            unset($data);
            unset($resstr);
            unset($this->searchParams);
            unset($this->DSGSearchRes->html);
            unset($this->search_DSGRulesList);
            unset($jsonData);
        }
        if (class_exists('Profiler', false)) {
            Profiler::stop('DSGSearch->parse');
        }
        $this->DSGSearchRes->debugMessages = $this->debugMessages;
        if (is_array($this->DSGSearchRes->items) && (count($this->DSGSearchRes->items) <= 0)) {
            $this->DSGSearchRes->total_results = 0;
        }
        $this->verifyResult($this->DSGSearchRes);
        return $this->DSGSearchRes;
    }

    private function parseCategories($data)
    {
        return; //TODO Потом посмотреть и починить
        $sections = array();
        $this->DSGSearchRes->intCategories = array();
        $block = array();
        $res = $this->regexMatch('parse_categories_values/block', $data, $block);
        if ($res > 0) {
            $res = $this->regexMatchAll('parse_categories_values/section', $block[1], $sections);
            if ($res > 0) {
                foreach ($sections[1] as $section) {
                    $this->DSGSearchRes->intCategories[] = new stdClass();
                    $resstr = array();
                    $res = $this->regexMatch('parse_categories_values/count', $section, $resstr);
                    if ($res > 0) {
                        $resnum = DSGUtils::parseChineseNumber($resstr[1]);
                        end($this->DSGSearchRes->intCategories)->count = $resnum;
                    } else {
                        array_pop($this->DSGSearchRes->intCategories);
                        continue;
                    }

                    $res = $this->regexMatch('parse_categories_values/cid', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intCategories)->cid = $resstr[1];
                    } else {
                        end($this->DSGSearchRes->intCategories)->cid = 0;
                    }

                    $res = $this->regexMatch('parse_categories_values/title', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intCategories)->title = $resstr[1];
                    } else {
                        array_pop($this->DSGSearchRes->intCategories);
                        continue;
                    }

                    $res = $this->regexMatch('parse_categories_values/q', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intCategories)->q = @iconv(
                          'GBK',
                          'UTF-8//IGNORE',
                          urldecode($resstr[1])
                        );
                    } else {
                        end($this->DSGSearchRes->intCategories)->q = '';
                    }
                }
                unset($resstr);
                unset($sections);
                unset($section);
                //===================================================
                // Sort and truncate arrays
                //---------------------------------------------------
                if (!function_exists('compareByCount')) {
                    function compareByCount($a, $b)
                    {
                        $a_val = $a->count;
                        $b_val = $b->count;
                        if ($a_val > $b_val) {
                            return -1;
                        } elseif ($a_val == $b_val) {
                            return 0;
                        } else {
                            return 1;
                        }
                    }
                }
                if (isset($this->DSGSearchRes->intCategories)) {
                    uasort($this->DSGSearchRes->intCategories, "compareByCount");
                }

                if (isset($this->DSGSearchRes->intCategories)) {
                    $this->DSGSearchRes->intCategories = $this->preprocessCats($this->DSGSearchRes->intCategories);
                }
                if (isset($this->DSGSearchRes->intCategories)) {
                    $this->DSGSearchRes->intCategories = array_slice(
                      $this->DSGSearchRes->intCategories,
                      0,
                      $this->translator_long_list_count_limit
                    );
                }
            }
        }
        return;
    }

    private function parseCategoriesFromJson($data)
    {
        return; //TODO Потом посмотреть и починить
        $sections = array();
        $this->DSGSearchRes->intCategories = array();
        $block = array();
        $res = $this->regexMatch('parse_categories_values/block', $data, $block);
        if ($res > 0) {
            $res = $this->regexMatchAll('parse_categories_values/section', $block[1], $sections);
            if ($res > 0) {
                foreach ($sections[1] as $section) {
                    $this->DSGSearchRes->intCategories[] = new stdClass();
                    $resstr = array();
                    $res = $this->regexMatch('parse_categories_values/count', $section, $resstr);
                    if ($res > 0) {
                        $resnum = DSGUtils::parseChineseNumber($resstr[1]);
                        end($this->DSGSearchRes->intCategories)->count = $resnum;
                    } else {
                        array_pop($this->DSGSearchRes->intCategories);
                        continue;
                    }

                    $res = $this->regexMatch('parse_categories_values/cid', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intCategories)->cid = $resstr[1];
                    } else {
                        end($this->DSGSearchRes->intCategories)->cid = 0;
                    }

                    $res = $this->regexMatch('parse_categories_values/title', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intCategories)->title = $resstr[1];
                    } else {
                        array_pop($this->DSGSearchRes->intCategories);
                        continue;
                    }

                    $res = $this->regexMatch('parse_categories_values/q', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intCategories)->q = @iconv(
                          'GBK',
                          'UTF-8//IGNORE',
                          urldecode($resstr[1])
                        );
                    } else {
                        end($this->DSGSearchRes->intCategories)->q = '';
                    }
                }
                unset($resstr);
                unset($sections);
                unset($section);
                //===================================================
                // Sort and truncate arrays
                //---------------------------------------------------
                if (!function_exists('compareByCount')) {
                    function compareByCount($a, $b)
                    {
                        $a_val = $a->count;
                        $b_val = $b->count;
                        if ($a_val > $b_val) {
                            return -1;
                        } elseif ($a_val == $b_val) {
                            return 0;
                        } else {
                            return 1;
                        }
                    }
                }
                if (isset($this->DSGSearchRes->intCategories)) {
                    uasort($this->DSGSearchRes->intCategories, "compareByCount");
                }

                if (isset($this->DSGSearchRes->intCategories)) {
                    $this->DSGSearchRes->intCategories = $this->preprocessCats($this->DSGSearchRes->intCategories);
                }
                if (isset($this->DSGSearchRes->intCategories)) {
                    $this->DSGSearchRes->intCategories = array_slice(
                      $this->DSGSearchRes->intCategories,
                      0,
                      $this->translator_long_list_count_limit
                    );
                }
            }
        }
        return;
    }

    private function preprocessCats($inArray)
    {
        //===================================================
        // 1. Remove unexisting categories
        // 2. Remove disabled categories
        // 3. Fill hfurl
        //===================================================
        if (!class_exists('Yii')) {
            return array();
        }
        $cids = array();
        if (isset($inArray)) {
            foreach ($inArray as $value) {
                if (isset($value->cid)) {
                    if ($value->cid != 0) {
                        $cids[] = $value->cid;
                    }
                } elseif (isset($value->values_cid)) {
                    if ($value->values_cid != 0) {
                        $cids[] = $value->values_cid;
                    }
                }
            }
        }
        $cids = array_slice($cids, 0, 100);
        $cidsParam = implode(',', $cids);
        if ($cidsParam != '') {
            $command = Yii::app()->db->createCommand(
              "
SELECT rr.cid, max(rr.url) AS url
  FROM (SELECT cc.cid, cc.url
          FROM categories cc
         WHERE cc.status = 1 AND cc.cid <> 0 AND cc.cid IN (" . Utils::safe($cidsParam) . ")
        UNION ALL
        SELECT ee.cid, ee.url
          FROM categories_ext ee
         WHERE ee.status = 1 AND ee.cid <> 0 AND ee.cid IN (" . Utils::safe($cidsParam) . ")) rr
GROUP BY rr.cid
   "
            );
            $rows = $command->queryAll();
        } else {
            $rows = array();
        }
        $foundCids = array();
        foreach ($rows as $row) {
            if (strlen($row['url']) > 0) {
                $foundCids[$row['cid']] = $row['url'];
            }
        }
        $deleteCids = array();
        if (isset($inArray)) {
            foreach ($inArray as $k => $value) {
                if (isset($value->cid)) {
                    if (isset($foundCids[$value->cid])) {
                        $inArray[$k]->url = $foundCids[$value->cid];
                    } else {
                        $deleteCids[] = $k;
                    }
                } elseif (isset($value->values_cid)) {
                    if (isset($foundCids[$value->values_cid])) {
                        $inArray[$k]->url = $foundCids[$value->values_cid];
                    } else {
                        $deleteCids[] = $k;
                    }
                }
            }
            foreach ($deleteCids as $k) {
                unset($inArray[$k]);
            }
            return $inArray;
        }
        return array();
    }

    private function parseGroups($data)
    {
        $sections = array();
        $this->DSGSearchRes->intGroups = array();
        $block = array();
        $res = $this->regexMatch('parse_group_values/block', $data, $block);
        if ($res > 0) {
            $res = $this->regexMatchAll('parse_group_values/section', $block[1], $sections);
            unset($block);
            if ($res > 0) {
                foreach ($sections[1] as $section) {
                    $this->DSGSearchRes->intGroups[] = new stdClass();
                    $resstr = array();
                    $res = $this->regexMatch('parse_group_values/title', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intGroups)->title = $resstr[1];
                    } else {
                        array_pop($this->DSGSearchRes->intGroups);
                        continue;
                    }
//======================================================================
                    end($this->DSGSearchRes->intGroups)->values = array();
                    $values_sections = array();
                    $res = $this->regexMatchAll('parse_group_values/values_section', $section, $values_sections);
                    if ($res > 0) {
                        foreach ($values_sections[1] as $values_section) {
                            end($this->DSGSearchRes->intGroups)->values[] = new stdClass();
                            $res = $this->regexMatch('parse_group_values/values_count', $values_section, $resstr);
                            if ($res > 0) {
                                $resnum = DSGUtils::parseChineseNumber($resstr[1]);
                                end(end($this->DSGSearchRes->intGroups)->values)->values_count = $resnum;
                            } else {
                                array_pop(end($this->DSGSearchRes->intGroups)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_group_values/values_title', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_title = $resstr[1];
                            } else {
                                array_pop(end($this->DSGSearchRes->intGroups)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_group_values/values_props', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_props = urldecode($resstr[1]);
                            } else {
                                array_pop(end($this->DSGSearchRes->intGroups)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_group_values/values_q', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_q = @iconv(
                                  'GBK',
                                  'UTF-8//IGNORE',
                                  urldecode($resstr[1])
                                );
                            } else {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_q = '';
                            }
                            $res = $this->regexMatch('parse_group_values/values_cid', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_cid = $resstr[1];
                            } else {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_cid = 0;
                            }
                        }
                        /*            foreach (end($this->DSGSearchRes->intGroups)->values as $val) {
                                      if (preg_match('/\-1:(.+)/',$val->values_props,$res)) {
                                        $val->values_props='';
                                        $val->values_cid=$res[1];
                                      }
                                    }
                        */
                        unset($values_section);
                        unset($values_sections);
                    } else {
                        array_pop($this->DSGSearchRes->intGroups);
                        continue;
                    }
                    //===================================================
                    // Sort and truncate arrays
                    //---------------------------------------------------
                    if (!function_exists('compareByValueCount')) {
                        function compareByValueCount($a, $b)
                        {
                            $a_val = $a->values_count;
                            $b_val = $b->values_count;
                            if ($a_val > $b_val) {
                                return -1;
                            } elseif ($a_val == $b_val) {
                                return 0;
                            } else {
                                return 1;
                            }
                        }
                    }
                    if ((isset(end($this->DSGSearchRes->intGroups)->values)) && (isset(end(
                          end($this->DSGSearchRes->intGroups)->values
                        )->values_props))
                    ) {
                        $props = explode(':', end(end($this->DSGSearchRes->intGroups)->values)->values_props);
                        if ((isset($props[0])) && (($props[0] == 30000) || ($props[0] == 403))) {
                            array_pop($this->DSGSearchRes->intGroups);
                            unset($props);
                            continue;
                        }
                        unset($props);
                    }
                    if (isset(end($this->DSGSearchRes->intGroups)->values)) {
                        uasort(end($this->DSGSearchRes->intGroups)->values, "compareByValueCount");
                        end($this->DSGSearchRes->intGroups)->values = array_slice(
                          end($this->DSGSearchRes->intGroups)->values,
                          0,
                          $this->translator_long_list_count_limit
                        );
                    }
                }
                unset($sections);
                unset($section);
                unset($resstr);
            }
        }
        return;
    }

    private function parseGroupsFromJson($data)
    {
        $this->DSGSearchRes->intGroups = array();
        $groups = $this->jsonObjGet('parse_group_values', $data);
        if ($groups!==false && is_array($groups)) {
                foreach ($groups as $group) {
                    $this->DSGSearchRes->intGroups[] = new stdClass();
                    if (isset($group->text)) {
                        end($this->DSGSearchRes->intGroups)->title = $group->text;
                    } else {
                        array_pop($this->DSGSearchRes->intGroups);
                        continue;
                    }
                    end($this->DSGSearchRes->intGroups)->key=(isset($group->key)?$group->key:false);
                    end($this->DSGSearchRes->intGroups)->value=(isset($group->value)?$group->value:false);
//======================================================================
                    end($this->DSGSearchRes->intGroups)->values = array();
                    if (isset($group->sub) && (is_array($group->sub))) {
                        foreach ($group->sub as $value) {
                            if (!in_array($value->key, array('cat','ppath'))) {
                                continue;
                            }
                            end($this->DSGSearchRes->intGroups)->values[] = new stdClass();
                            end(end($this->DSGSearchRes->intGroups)->values)->values_count = $value->count;
                            end(end($this->DSGSearchRes->intGroups)->values)->values_title = $value->text;
                            end(end($this->DSGSearchRes->intGroups)->values)->values_props = (($value->key=='ppath')?$value->value:'');
                            end(end($this->DSGSearchRes->intGroups)->values)->values_q = (($value->key=='q')?$value->value:'');
                            if ($value->key=='cat') {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_cid = $value->value;
                                end(end($this->DSGSearchRes->intGroups)->values)->values_props =$value->value.':0';
                                } else {
                                end(end($this->DSGSearchRes->intGroups)->values)->values_cid = 0;
                            }
                        }
                    } else {
                        array_pop($this->DSGSearchRes->intGroups);
                        continue;
                    }
                    //===================================================
                    // Sort and truncate arrays
                    //---------------------------------------------------
                    if (!function_exists('compareByValueCount')) {
                        function compareByValueCount($a, $b)
                        {
                            $a_val = $a->values_count;
                            $b_val = $b->values_count;
                            if ($a_val > $b_val) {
                                return -1;
                            } elseif ($a_val == $b_val) {
                                return 0;
                            } else {
                                return 1;
                            }
                        }
                    }
                    if ((isset(end($this->DSGSearchRes->intGroups)->values)) && (isset(end(
                          end($this->DSGSearchRes->intGroups)->values
                        )->values_props))
                    ) {
                        $props = explode(':', end(end($this->DSGSearchRes->intGroups)->values)->values_props);
                        if ((isset($props[0])) && (($props[0] == 30000) || ($props[0] == 403))) {
                            array_pop($this->DSGSearchRes->intGroups);
                            unset($props);
                            continue;
                        }
                        unset($props);
                    }
                    if (isset(end($this->DSGSearchRes->intGroups)->values)) {
                        uasort(end($this->DSGSearchRes->intGroups)->values, "compareByValueCount");
                        end($this->DSGSearchRes->intGroups)->values = array_slice(
                          end($this->DSGSearchRes->intGroups)->values,
                          0,
                          $this->translator_long_list_count_limit
                        );
                    }
                }
        }
        return;
    }


    private function parseFilters($data)
    {
        $sections = array();
        $this->DSGSearchRes->intFilters = array();
        $block = array();
        $res = $this->regexMatch('parse_filter_values/block', $data, $block);
        if ($res > 0) {
            $res = $this->regexMatchAll('parse_filter_values/section', $block[1], $sections);
            unset($block);
            if ($res > 0) {
                foreach ($sections[1] as $section) {
                    $this->DSGSearchRes->intFilters[] = new stdClass();
                    $resstr = array();
                    $res = $this->regexMatch('parse_filter_values/title', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intFilters)->title = $resstr[1];
                    } else {
                        array_pop($this->DSGSearchRes->intFilters);
                        continue;
                    }
//======================================================================
                    end($this->DSGSearchRes->intFilters)->values = array();
                    $values_sections = array();
                    $res = $this->regexMatchAll('parse_filter_values/values_section', $section, $values_sections);
                    if ($res > 0) {
                        foreach ($values_sections[1] as $values_section) {
                            end($this->DSGSearchRes->intFilters)->values[] = new stdClass();
                            $res = $this->regexMatch('parse_filter_values/values_count', $values_section, $resstr);
                            if ($res > 0) {
                                $resnum = DSGUtils::parseChineseNumber($resstr[1]);
                                end(end($this->DSGSearchRes->intFilters)->values)->values_count = $resnum;
                            } else {
                                array_pop(end($this->DSGSearchRes->intFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_filter_values/values_title', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_title = $resstr[1];
                            } else {
                                array_pop(end($this->DSGSearchRes->intFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_filter_values/values_props', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_props = urldecode($resstr[1]);
                            } else {
                                array_pop(end($this->DSGSearchRes->intFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_filter_values/values_q', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_q = @iconv(
                                  'GBK',
                                  'UTF-8//IGNORE',
                                  urldecode($resstr[1])
                                );
                            } else {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_q = '';
                            }
                            $res = $this->regexMatch('parse_filter_values/values_cid', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_cid = $resstr[1];
                            } else {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_cid = 0;
                            }
                        }
                        unset($values_section);
                        unset($values_sections);
                    } else {
                        array_pop($this->DSGSearchRes->intFilters);
                        continue;
                    }
                    //===================================================
                    // Sort and truncate arrays
                    //---------------------------------------------------
                    if (!function_exists('compareByValueCount')) {
                        function compareByValueCount($a, $b)
                        {
                            $a_val = $a->values_count;
                            $b_val = $b->values_count;
                            if ($a_val > $b_val) {
                                return -1;
                            } elseif ($a_val == $b_val) {
                                return 0;
                            } else {
                                return 1;
                            }
                        }
                    }
                    if ((isset(end($this->DSGSearchRes->intFilters)->values)) && (isset(end(
                          end($this->DSGSearchRes->intFilters)->values
                        )->values_props))
                    ) {
                        $props = explode(':', end(end($this->DSGSearchRes->intFilters)->values)->values_props);
                        if ((isset($props[0])) && (($props[0] == 30000) || ($props[0] == 403))) {
                            array_pop($this->DSGSearchRes->intFilters);
                            unset($props);
                            continue;
                        }
                        unset($props);
                    }
                    if (isset(end($this->DSGSearchRes->intFilters)->values)) {
                        uasort(end($this->DSGSearchRes->intFilters)->values, "compareByValueCount");
                        end($this->DSGSearchRes->intFilters)->values = array_slice(
                          end($this->DSGSearchRes->intFilters)->values,
                          0,
                          $this->translator_long_list_count_limit
                        );
                    }
                }
                unset($sections);
                unset($section);
                unset($resstr);
            }
        }
        return;
    }

    private function parseFiltersFromJson($data)
    {
        $sections = array();
        $this->DSGSearchRes->intFilters = array();
        $block = array();
        $filters = $this->jsonObjGet('parse_filter_values', $data);
        if ($filters!==false && is_array($filters)) {
                foreach ($filters as $filter) {
                    $this->DSGSearchRes->intFilters[] = new stdClass();
                    if (isset($filter->text)) {
                        end($this->DSGSearchRes->intFilters)->title = $filter->text;
                    } else {
                        array_pop($this->DSGSearchRes->intFilters);
                        continue;
                    }
                    end($this->DSGSearchRes->intFilters)->key=(isset($filter->key)?$filter->key:false);
                    end($this->DSGSearchRes->intFilters)->value=(isset($filter->value)?$filter->value:false);
//======================================================================
                    end($this->DSGSearchRes->intFilters)->values = array();
                    if (isset($filter->sub) && is_array($filter->sub)) {
                        foreach ($filter->sub as $value) {
                            if (!in_array($value->key, array('cat','ppath'))) {
                                continue;
                            }
                            end($this->DSGSearchRes->intFilters)->values[] = new stdClass();
                            end(end($this->DSGSearchRes->intFilters)->values)->values_count = $value->count;
                            end(end($this->DSGSearchRes->intFilters)->values)->values_title = $value->text;
                                end(end($this->DSGSearchRes->intFilters)->values)->values_props = (($value->key=='ppath')?$value->value:'');
                                end(end($this->DSGSearchRes->intFilters)->values)->values_q = (($value->key=='q')?$value->value:'');
                            if ($value->key=='cat') {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_cid = $value->value;
                                end(end($this->DSGSearchRes->intFilters)->values)->values_props =$value->value.':0';
                            } else {
                                end(end($this->DSGSearchRes->intFilters)->values)->values_cid = 0;
                            }


                        }
                    } else {
                        array_pop($this->DSGSearchRes->intFilters);
                        continue;
                    }
                    //===================================================
                    // Sort and truncate arrays
                    //---------------------------------------------------
                    if (!function_exists('compareByValueCount')) {
                        function compareByValueCount($a, $b)
                        {
                            $a_val = $a->values_count;
                            $b_val = $b->values_count;
                            if ($a_val > $b_val) {
                                return -1;
                            } elseif ($a_val == $b_val) {
                                return 0;
                            } else {
                                return 1;
                            }
                        }
                    }
                    if ((isset(end($this->DSGSearchRes->intFilters)->values)) && (isset(end(
                          end($this->DSGSearchRes->intFilters)->values
                        )->values_props))
                    ) {
                        $props = explode(':', end(end($this->DSGSearchRes->intFilters)->values)->values_props);
                        if ((isset($props[0])) && (($props[0] == 30000) || ($props[0] == 403))) {
                            array_pop($this->DSGSearchRes->intFilters);
                            unset($props);
                            continue;
                        }
                        unset($props);
                    }
                    if (isset(end($this->DSGSearchRes->intFilters)->values)) {
                        uasort(end($this->DSGSearchRes->intFilters)->values, "compareByValueCount");
                        end($this->DSGSearchRes->intFilters)->values = array_slice(
                          end($this->DSGSearchRes->intFilters)->values,
                          0,
                          $this->translator_long_list_count_limit
                        );
                    }
                }
                unset($sections);
                unset($section);
                unset($resstr);
        }
        return;
    }


    private function parseMultiFilters($data)
    {
        $sections = array();
        $this->DSGSearchRes->intMultiFilters = array();
        $block = array();
        $res = $this->regexMatch('parse_multifilter_values/block', $data, $block);
        if ($res > 0) {
            $res = $this->regexMatchAll('parse_multifilter_values/section', $block[1], $sections);
            if ($res > 0) {
                foreach ($sections[1] as $section) {
                    $this->DSGSearchRes->intMultiFilters[] = new stdClass();
                    $resstr = array();
                    $res = $this->regexMatch('parse_multifilter_values/title', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intMultiFilters)->title = $resstr[1];
                    } else {
                        array_pop($this->DSGSearchRes->intMultiFilters);
                        continue;
                    }
//======================================================================
                    end($this->DSGSearchRes->intMultiFilters)->values = array();
                    $values_sections = array();
                    $res = $this->regexMatchAll('parse_multifilter_values/values_section', $section, $values_sections);
                    if ($res > 0) {
                        foreach ($values_sections[1] as $values_section) {
                            end($this->DSGSearchRes->intMultiFilters)->values[] = new stdClass();
                            $res = $this->regexMatch('parse_multifilter_values/values_count', $values_section, $resstr);
                            if ($res > 0) {
                                $resnum = DSGUtils::parseChineseNumber($resstr[1]);
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_count = $resnum;
                            } else {
                                array_pop(end($this->DSGSearchRes->intMultiFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_title', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_title = $resstr[1];
                            } else {
                                array_pop(end($this->DSGSearchRes->intMultiFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_props', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_props = urldecode(
                                  $resstr[1]
                                );
                            } else {
                                array_pop(end($this->DSGSearchRes->intMultiFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_q', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_q = @iconv(
                                  'GBK',
                                  'UTF-8//IGNORE',
                                  urldecode($resstr[1])
                                );
                            } else {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_q = '';
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_cid', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_cid = $resstr[1];
                            } else {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_cid = 0;
                            }
                        }
                    } else {
                        array_pop($this->DSGSearchRes->intMultiFilters);
                        continue;
                    }

                    //== Remove unneaded props ================================
                    if ((isset(end($this->DSGSearchRes->intMultiFilters)->values)) && (isset(end(
                          end($this->DSGSearchRes->intMultiFilters)->values
                        )->values_props))
                    ) {
                        $mprops = explode(';', end(end($this->DSGSearchRes->intMultiFilters)->values)->values_props);
                        if (isset($mprops[0])) {
                            $props = explode(':', $mprops[0]);
                        } else {
                            $props = explode(':', end(end($this->DSGSearchRes->intMultiFilters)->values)->values_props);
                        }
                        if ((isset($props[0])) && (($props[0] == 30000) || ($props[0] == 403))) {
                            array_pop($this->DSGSearchRes->intMultiFilters);
                            continue;
                        }
                    }
                }
            }
        }
        return;
    }

    private function parseMultiFiltersFromJson($data)
    {
        $sections = array();
        $this->DSGSearchRes->intMultiFilters = array();
        $block = array();
        $res = $this->regexMatch('parse_multifilter_values/block', $data, $block);
        if ($res > 0) {
            $res = $this->regexMatchAll('parse_multifilter_values/section', $block[1], $sections);
            if ($res > 0) {
                foreach ($sections[1] as $section) {
                    $this->DSGSearchRes->intMultiFilters[] = new stdClass();
                    $resstr = array();
                    $res = $this->regexMatch('parse_multifilter_values/title', $section, $resstr);
                    if ($res > 0) {
                        end($this->DSGSearchRes->intMultiFilters)->title = $resstr[1];
                    } else {
                        array_pop($this->DSGSearchRes->intMultiFilters);
                        continue;
                    }
//======================================================================
                    end($this->DSGSearchRes->intMultiFilters)->values = array();
                    $values_sections = array();
                    $res = $this->regexMatchAll('parse_multifilter_values/values_section', $section, $values_sections);
                    if ($res > 0) {
                        foreach ($values_sections[1] as $values_section) {
                            end($this->DSGSearchRes->intMultiFilters)->values[] = new stdClass();
                            $res = $this->regexMatch('parse_multifilter_values/values_count', $values_section, $resstr);
                            if ($res > 0) {
                                $resnum = DSGUtils::parseChineseNumber($resstr[1]);
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_count = $resnum;
                            } else {
                                array_pop(end($this->DSGSearchRes->intMultiFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_title', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_title = $resstr[1];
                            } else {
                                array_pop(end($this->DSGSearchRes->intMultiFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_props', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_props = urldecode(
                                  $resstr[1]
                                );
                            } else {
                                array_pop(end($this->DSGSearchRes->intMultiFilters)->values);
                                continue;
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_q', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_q = @iconv(
                                  'GBK',
                                  'UTF-8//IGNORE',
                                  urldecode($resstr[1])
                                );
                            } else {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_q = '';
                            }
                            $res = $this->regexMatch('parse_multifilter_values/values_cid', $values_section, $resstr);
                            if ($res > 0) {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_cid = $resstr[1];
                            } else {
                                end(end($this->DSGSearchRes->intMultiFilters)->values)->values_cid = 0;
                            }
                        }
                    } else {
                        array_pop($this->DSGSearchRes->intMultiFilters);
                        continue;
                    }

                    //== Remove unneaded props ================================
                    if ((isset(end($this->DSGSearchRes->intMultiFilters)->values)) && (isset(end(
                          end($this->DSGSearchRes->intMultiFilters)->values
                        )->values_props))
                    ) {
                        $mprops = explode(';', end(end($this->DSGSearchRes->intMultiFilters)->values)->values_props);
                        if (isset($mprops[0])) {
                            $props = explode(':', $mprops[0]);
                        } else {
                            $props = explode(':', end(end($this->DSGSearchRes->intMultiFilters)->values)->values_props);
                        }
                        if ((isset($props[0])) && (($props[0] == 30000) || ($props[0] == 403))) {
                            array_pop($this->DSGSearchRes->intMultiFilters);
                            continue;
                        }
                    }
                }
            }
        }
        return;
    }

    private function parseSuggestions($data)
    {
        $sections = array();
        $resstr = array();
        $this->DSGSearchRes->intSuggestions = array();
        $res = $this->regexMatchAll('parse_suggestions_values/section', $data, $sections);
        if ($res > 0) {
            foreach ($sections[1] as $section) {
                $this->DSGSearchRes->intSuggestions[] = new stdClass();
                $res = $this->regexMatch('parse_suggestions_values/title', $section, $resstr);
                if ($res > 0) {
                    end($this->DSGSearchRes->intSuggestions)->title = $resstr[1];
                } else {
                    array_pop($this->DSGSearchRes->intSuggestions);
                    continue;
                }
                $res = $this->regexMatch('parse_suggestions_values/q', $section, $resstr);
                if ($res > 0) {
                    end($this->DSGSearchRes->intSuggestions)->q = @iconv('GBK', 'UTF-8//IGNORE', urldecode($resstr[1]));
                } else {
                    end($this->DSGSearchRes->intSuggestions)->q = '';
                }
                $res = $this->regexMatch('parse_suggestions_values/cid', $section, $resstr);
                if ($res > 0) {
                    end($this->DSGSearchRes->intSuggestions)->cid = $resstr[1];
                } else {
                    end($this->DSGSearchRes->intSuggestions)->cid = 0;
                }
            }
            unset($resstr);
            unset($sections);
            unset($section);
        }
        return;
    }

    private function parseSuggestionsFromJson($data)
    {
        $this->DSGSearchRes->intSuggestions = array();
        $words = $this->jsonObjGet('parse_suggestions_values', $data);
        if (($words!==false) && (is_array($words))) {
            foreach ($words as $word) {
                $this->DSGSearchRes->intSuggestions[] = new stdClass();
                    end($this->DSGSearchRes->intSuggestions)->title = $word->text;
                    end($this->DSGSearchRes->intSuggestions)->q = $word->query; // @iconv('GBK', 'UTF-8//IGNORE', urldecode($word->query));
                    end($this->DSGSearchRes->intSuggestions)->cid=0;
            }
        }
        return;
    }

    private function parsePriceRanges($data)
    {
        $sections = array();
        $resstr = array();
        $this->DSGSearchRes->intPriceRanges = array();
        $res = $this->regexMatchAll('parse_price_ranges_values/section', $data, $sections);
        if ($res > 0) {
            foreach ($sections[1] as $section) {
                $this->DSGSearchRes->intPriceRanges[] = new stdClass();
                $res = $this->regexMatch('parse_price_ranges_values/start', $section, $resstr);
                if ($res > 0) {
                    end($this->DSGSearchRes->intPriceRanges)->start = round((float) $resstr[1]);
                } else {
                    array_pop($this->DSGSearchRes->intPriceRanges);
                    continue;
                }
                $res = $this->regexMatch('parse_price_ranges_values/end', $section, $resstr);
                if ($res > 0) {
                    end($this->DSGSearchRes->intPriceRanges)->end = round((float) $resstr[1]);
                } else {
                    array_pop($this->DSGSearchRes->intPriceRanges);
                    continue;
                }
                $res = $this->regexMatch('parse_price_ranges_values/percent', $section, $resstr);
                if ($res > 0) {
                    end($this->DSGSearchRes->intPriceRanges)->percent = $resstr[1];
                } else {
                    array_pop($this->DSGSearchRes->intPriceRanges);
                    continue;
                }
            }
        }
        return;
    }

    private function parsePriceRangesFromJson($data)
    {
        $this->DSGSearchRes->intPriceRanges = array();
        $ranks = $this->jsonObjGet('parse_price_ranges_values', $data);
        if (($ranks !== false) && (is_array($ranks))) {
            foreach ($ranks as $rank) {
                $this->DSGSearchRes->intPriceRanges[] = new stdClass();
                end($this->DSGSearchRes->intPriceRanges)->start = round((float) $rank->start);
                end($this->DSGSearchRes->intPriceRanges)->end = round((float) $rank->end);
                end($this->DSGSearchRes->intPriceRanges)->percent = $rank->percent;
            }
        }
        return;
    }

    protected function parseItem($matches)
    {
        $this->DSGSearchRes->items[] = new StdClass();
        $resstr = array();
        $res = $this->regexMatch('parse_item_values/num_iid', $matches[0], $resstr);
        if ($res > 0) {
            end($this->DSGSearchRes->items)->num_iid = $resstr[1]; //$item->num_iid;
        } else {
            array_pop($this->DSGSearchRes->items);
            return '';
        }
        $res = $this->regexMatch('parse_item_values/price', $matches[0], $resstr);
        if ($res > 0) {
            $price = (float) $resstr[1];
        } else {
            array_pop($this->DSGSearchRes->items);
            return '';
        }
        $res = $this->regexMatch('parse_item_values/price_original', $matches[0], $resstr); /// !!!
        if ($res > 0) {
            if ((float) $resstr[1] > 0) {
                $price_original = (float) $resstr[1];
            } else {
                $price_original = $price;
            }
        } else {
            $price_original = $price;
        }
        end($this->DSGSearchRes->items)->price = max($price, $price_original); //$item->price;
        end($this->DSGSearchRes->items)->promotion_price = min($price, $price_original); //$item->promotion_price;
        $res = $this->regexMatch('parse_item_values/pic_url', $matches[0], $resstr);
        if ($res > 0) {
            end($this->DSGSearchRes->items)->pic_url = $resstr[1]; //$item->pic_url;
        } else {
            array_pop($this->DSGSearchRes->items);
            return '';
        }
        $res = $this->regexMatch('parse_item_values/nick', $matches[0], $resstr);
        if ($res > 0) {
            end($this->DSGSearchRes->items)->nick = $resstr[1]; //$item->nick;
        } else {
            end($this->DSGSearchRes->items)->nick = '';
        }
        $res = $this->regexMatch('parse_item_values/seller_rate', $matches[0], $resstr);
        if ($res > 0) {
            end($this->DSGSearchRes->items)->seller_rate = $resstr[1];
        } else {
            end($this->DSGSearchRes->items)->seller_rate = 0;
        }
        $res = $this->regexMatch('parse_item_values/post_fee', $matches[0], $resstr);
        if ($res > 0) {
            end($this->DSGSearchRes->items)->post_fee = (float) $resstr[1]; //$item->post_fee;
        } else {
            end($this->DSGSearchRes->items)->post_fee = 0;
        }
        end($this->DSGSearchRes->items)->express_fee = end($this->DSGSearchRes->items)->post_fee;
        end($this->DSGSearchRes->items)->ems_fee = end($this->DSGSearchRes->items)->post_fee;
        end($this->DSGSearchRes->items)->postage_id = 0;
        $res = $this->regexMatch('parse_item_values/cid', $matches[0], $resstr);
        if ($res > 0) {
            end($this->DSGSearchRes->items)->cid = $resstr[1];
        } else {
            end($this->DSGSearchRes->items)->cid = 0;
        }
        $res = $this->regexMatch('parse_item_values/tmall', $matches[0], $resstr);
        if ($res > 0) {
            end($this->DSGSearchRes->items)->tmall = true;
        } else {
            end($this->DSGSearchRes->items)->tmall = false;
        }
        return '';
    }

    protected function parseItemFromJson($rawJsonItem)
    {
        $this->DSGSearchRes->items[] = new StdClass();
        $res = $this->jsonObjGet('parse_item_values/num_iid', $rawJsonItem);
        if ($res!==false) {
            end($this->DSGSearchRes->items)->num_iid = $res; //$item->num_iid;
        } else {
            array_pop($this->DSGSearchRes->items);
            return;
        }

        $res = $this->jsonObjGet('parse_item_values/price', $rawJsonItem);
        if ($res !== false) {
            $price = (float) $res;
        } else {
            array_pop($this->DSGSearchRes->items);
            return;
        }

        $res = $this->jsonObjGet('parse_item_values/price_original', $rawJsonItem); /// !!!
        if ($res !== false) {
            if ((float) $res > 0) {
                $price_original = (float) $res;
            } else {
                $price_original = $price;
            }
        } else {
            $price_original = $price;
        }
        end($this->DSGSearchRes->items)->price = max($price, $price_original); //$item->price;
        end($this->DSGSearchRes->items)->promotion_price = min($price, $price_original); //$item->promotion_price;

        $res = $this->jsonObjGet('parse_item_values/pic_url', $rawJsonItem);
        if ($res !== false) {
            end($this->DSGSearchRes->items)->pic_url = $res; //$item->pic_url;
        } else {
            array_pop($this->DSGSearchRes->items);
            return;
        }
        $res = $this->jsonObjGet('parse_item_values/nick', $rawJsonItem);
        if ($res !== false) {
            end($this->DSGSearchRes->items)->nick = $res; //$item->nick;
        } else {
            end($this->DSGSearchRes->items)->nick = '';
        }
        $res = $this->jsonObjGet('parse_item_values/seller_rate', $rawJsonItem);
        if ($res !==false) {
            end($this->DSGSearchRes->items)->seller_rate = DSGSeller::getSalesFromCrowns((float)$res+1);
        } else {
            end($this->DSGSearchRes->items)->seller_rate = 0;
        }
        $res = $this->jsonObjGet('parse_item_values/post_fee', $rawJsonItem);
        if ($res !== false) {
            end($this->DSGSearchRes->items)->post_fee = (float) $res; //$item->post_fee;
        } else {
            end($this->DSGSearchRes->items)->post_fee = 0;
        }

        end($this->DSGSearchRes->items)->express_fee = end($this->DSGSearchRes->items)->post_fee;
        end($this->DSGSearchRes->items)->ems_fee = end($this->DSGSearchRes->items)->post_fee;
        end($this->DSGSearchRes->items)->postage_id = 0;

        $res = $this->jsonObjGet('parse_item_values/cid', $rawJsonItem);
        if ($res !==false) {
            end($this->DSGSearchRes->items)->cid = $res;
        } else {
            end($this->DSGSearchRes->items)->cid = 0;
        }

        $res = $this->jsonObjGet('parse_item_values/tmall', $rawJsonItem);
        if ($res !== false && ($res == '1')) {
            end($this->DSGSearchRes->items)->tmall = true;
        } else {
            end($this->DSGSearchRes->items)->tmall = false;
        }
        return;
    }
}