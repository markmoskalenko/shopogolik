<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="DSGUtils.php">
* </description>
**********************************************************************************************************************/?>
<?php
class DSGUtils {
  public static function parseChineseNumber($n) {
    $resstr = array();
    if ($n > 0) {
      $zhnum = $n;
      $res = preg_match('/([\d\.]+)/i', $zhnum, $resstr);
      if ($res > 0) {
        if (($zhnum != $resstr[1]) || (strpos($resstr[0],'.')>0)) {
          $resnum = round((float) $resstr[1] * 10000);
        }
        else {
          $resnum = $zhnum;
        }
      }
      else {
        $resnum = 0;
      }
    }
    else {
      $resnum = 0;
    }
    return $resnum;
  }
}