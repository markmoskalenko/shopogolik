<?php
echo "<div class='printableArea'>";
echo "<h1>".Yii::t('admin','Служба доставки')." #".$model->delivery_id."</h1>";
 $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'info',
            'icon'=>'plus white',
			'label'=>Yii::t('plus','Создать'),
			'htmlOptions'=>array('onclick'=>'renderCreateForm();'),
		));
		echo " ";
             $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'success',
            'icon'=>'edit white',
			'label'=>Yii::t('admin','Правка'),
			'htmlOptions'=>array('onclick'=>'renderUpdateForm('.$model->id.');'),
		));
		echo " ";
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'danger',
            'icon'=>'trash white',
			'label'=>Yii::t('admin','Удалить'),
			'htmlOptions'=>array('onclick'=>'delete_record('.$model->id.');'),
		));
		echo " ";
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'warning',
            'icon'=>'print white',
			'label'=>Yii::t('admin','Печать'),
			'htmlOptions'=>array('onclick'=>'print();'),
		));
echo "<hr/>";
	         $this->widget('bootstrap.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
						'id',
                		'delivery_id',
		                'enabled',
		                'name',
		                'description',
		                'currency',
		                'min_weight',
		                'max_weight',
		                'fees',
			),
		));
	         echo "</div>";