<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      delivery_id		</th>
 		<th width="80px">
		      enabled		</th>
 		<th width="80px">
		      name		</th>
 		<th width="80px">
		      description		</th>
 		<th width="80px">
		      currency		</th>
 		<th width="80px">
		      min_weight		</th>
 		<th width="80px">
		      max_weight		</th>
 		<th width="80px">
		      fees		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->delivery_id; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       		<td>
			<?php echo $row->name; ?>
		</td>
       		<td>
			<?php echo $row->description; ?>
		</td>
       		<td>
			<?php echo $row->currency; ?>
		</td>
       		<td>
			<?php echo $row->min_weight; ?>
		</td>
       		<td>
			<?php echo $row->max_weight; ?>
		</td>
       		<td>
			<?php echo $row->fees; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
