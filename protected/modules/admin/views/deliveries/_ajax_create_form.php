    <div id='deliveries-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Создать службу доставки')?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
   <?php
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'deliveries-create-form',
	'enableAjaxValidation'=>false,
     'enableClientValidation'=>false,
     'method'=>'post',
     'action'=>array("deliveries/create"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
            'clientOptions'=>array(
            'validateOnType'=>true,
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form, data, hasError) {
                if (!hasError)
                    {
                        create();
                            }
                    }'
            ),
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
   <div class="control-group">
       <div class="row-fluid">
           <div class="span6">
               <?php echo $form->labelEx($model,'delivery_id'); ?>
               <?php echo $form->textField($model,'delivery_id',array('class'=>'span3','maxlength'=>128)); ?>
               <?php echo $form->error($model,'delivery_id'); ?>

               <?php echo $form->labelEx($model,'enabled'); ?>
               <?php echo $form->textField($model,'enabled',array('class'=>'span3')); ?>
               <?php echo $form->error($model,'enabled'); ?>

               <?php echo $form->labelEx($model,'name'); ?>
               <?php echo $form->textField($model,'name',array('class'=>'span12','maxlength'=>383)); ?>
               <?php echo $form->error($model,'name'); ?>

               <?php echo $form->labelEx($model,'description'); ?>
               <?php echo $form->textArea($model,'description',array('rows'=>3, 'class'=>'span12')); ?>
               <?php echo $form->error($model,'description'); ?>
           </div>
           <div class="span6">
               <?php echo $form->labelEx($model,'currency'); ?>
               <?php echo $form->textField($model,'currency',array('size'=>32,'class'=>'span12')); ?>
               <?php echo $form->error($model,'currency'); ?>

               <?php echo $form->labelEx($model,'min_weight'); ?>
               <?php echo $form->textField($model,'min_weight',array('class'=>'span6')); ?>
               <?php echo $form->error($model,'min_weight'); ?>

               <?php echo $form->labelEx($model,'max_weight'); ?>
               <?php echo $form->textField($model,'max_weight',array('class'=>'span6')); ?>
               <?php echo $form->error($model,'max_weight'); ?>

               <?php echo $form->labelEx($model,'fees'); ?>
               <?php echo $form->textArea($model,'fees',array('rows'=>3, 'class'=>'span12')); ?>
               <?php echo $form->error($model,'fees'); ?>
           </div>
  </div>

</div><!--end modal body-->
  

<?php $this->endWidget(); ?>

</div>

    </div><!--end modal body-->
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove white',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->

</div>

<script type="text/javascript">
function create()
 {
   var data=$("#deliveries-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/deliveries/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#deliveries-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('deliveries-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#deliveries-create-form').each (function(){
  this.reset();
   });
  $('#deliveries-view-modal').modal('hide');
  $('#deliveries-create-modal').modal({
   show:true
  });
}
</script>
