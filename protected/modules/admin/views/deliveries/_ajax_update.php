<div id="deliveries-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#deliveries-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/deliveries/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#deliveries-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('deliveries-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#deliveries-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/deliveries/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#deliveries-update-modal-container').html(data); 
                 $('#deliveries-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
