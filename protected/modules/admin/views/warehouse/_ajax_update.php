<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update.php">
* </description>
**********************************************************************************************************************/?><div id="warehouse-items-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#warehouse-items-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/warehouse-items/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#warehouse-items-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('warehouse-items-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#warehouse-items-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/warehouse-items/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#warehouse-items-update-modal-container').html(data); 
                 $('#warehouse-items-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
