<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_search.php">
* </description>
**********************************************************************************************************************/?>
<?php  $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'search-warehouse-items-form',
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
));  ?>


	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tid',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'date_in',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'date_out',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'uid_in',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'uid_out',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'store_id',array('class'=>'span5','maxlength'=>4000)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'search white', 'label'=>Yii::t('admin','Поиск'))); ?>
               <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'danger','icon'=>'icon-remove-sign white', 'label'=>Yii::t('admin','Сброс'))); ?>
	</div>

<?php $this->endWidget(); ?>


<?php /* Alexys $cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile(Yii::app()->request->baseUrl.'/themes/admin/css/jquery-ui.css');
*/
?>	
   <script>
	$(".btnreset").click(function(){
		$(":input","#search-warehouse-items-form").each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		if (type == "text" || type == "password" || tag == "textarea") this.value = "";
		else if (type == "checkbox" || type == "radio") this.checked = false;
		else if (tag == "select") this.selectedIndex = "";
	  });
	});
   </script>

