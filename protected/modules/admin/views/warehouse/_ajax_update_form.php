<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update_form.php">
* </description>
**********************************************************************************************************************/?>    <div id='warehouse-items-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
   
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Update warehouse-items #<?php echo $model->id; ?></h3>
    </div>
    
    <div class="modal-body">
 
    
    
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'warehouse-items-update-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("warehouse-items/update"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
	
)); ?>
     	<fieldset>
		<legend>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		</legend>

	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">
			
			<?php echo $form->hiddenField($model,'id',array()); ?>
			
	               				  <div class="row">
					  <?php echo $form->labelEx($model,'tid'); ?>
					  <?php echo $form->textField($model,'tid',array('size'=>60,'maxlength'=>4000)); ?>
					  <?php echo $form->error($model,'tid'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'date_in'); ?>
					  <?php echo $form->textField($model,'date_in'); ?>
					  <?php echo $form->error($model,'date_in'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'date_out'); ?>
					  <?php echo $form->textField($model,'date_out'); ?>
					  <?php echo $form->error($model,'date_out'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'uid_in'); ?>
					  <?php echo $form->textField($model,'uid_in'); ?>
					  <?php echo $form->error($model,'uid_in'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'uid_out'); ?>
					  <?php echo $form->textField($model,'uid_out'); ?>
					  <?php echo $form->error($model,'uid_out'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'store_id'); ?>
					  <?php echo $form->textField($model,'store_id',array('size'=>60,'maxlength'=>4000)); ?>
					  <?php echo $form->error($model,'store_id'); ?>
				  </div>

			  
                        </div>   
  </div>

  </div><!--end modal body-->
  
  <div class="modal-footer">
	<div class="form-actions">

	                
		<?php		
		 $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			//'id'=>'sub2',
			'type'=>'primary',
                        'icon'=>'ok white', 
			'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
			'htmlOptions'=>array('onclick'=>'update();'),
		));
		
		?>
             
	</div> 
   </div><!--end modal footer-->	
</fieldset>

<?php $this->endWidget(); ?>

</div>


</div><!--end modal-->



