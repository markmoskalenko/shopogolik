<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_create_form.php">
* </description>
**********************************************************************************************************************/?>
    <div id='events-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новое событие')?></h3>
    </div>
    
    <div class="modal-body">
    
    <div class="form">

   <?php
   
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	    'id'=>'events-create-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("events/create"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
          'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>

    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model,'event_name'); ?>
                <?php echo $form->textField($model,'event_name',array('size'=>60,'maxlength'=>128)); ?>
                <?php echo $form->error($model,'event_name'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled',array('size'=>60,'maxlength'=>128)); ?>
                <?php echo $form->error($model,'enabled'); ?>
            </div>
            <div class="span4"></div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model,'event_descr'); ?>
                <?php echo $form->textArea($model,'event_descr',array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'event_descr'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'event_rules'); ?>
                <?php echo $form->textArea($model,'event_rules',array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'event_rules'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'event_action'); ?>
                <?php echo $form->textArea($model,'event_action',array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'event_action'); ?>
            </div>
        </div>
  </div>
  
</div><!--end modal body-->
        <div class="modal-footer">
            <?php

            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove white',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

<!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#events-create-form").serialize();
  jQuery.ajax({
    type: 'POST',
    url: '<?php
    echo Yii::app()->createAbsoluteUrl("admin/events/create"); ?>',
    data:data,
    success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#events-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('events-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#events-create-form').each (function(){
  this.reset();
   });
  $('#events-view-modal').modal('hide');
  $('#events-create-modal').modal({
   show:true
  });
}
</script>
