<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="excelReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      uid		</th>
      <th width="80px">
        lastname		</th>
 		<th width="80px">
		      firstname		</th>
      <th width="80px">
        patroname		</th>
 		<th width="80px">
		      email		</th>
 		<th width="80px">
		      status		</th>
 		<th width="80px">
		      created		</th>
 		<th width="80px">
		      role		</th>
 		<th width="80px">
		      country		</th>
 		<th width="80px">
		      city		</th>
 		<th width="80px">
		      index		</th>
 		<th width="80px">
		      address		</th>
 		<th width="80px">
		      phone		</th>
 		<th width="80px">
		      alias		</th>
 		<th width="80px">
		      birthday		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->uid; ?>
		</td>
      <td>
        <?php echo $row->lastname; ?>
      </td>
       		<td>
			<?php echo $row->firstname; ?>
		</td>
      <td>
        <?php echo $row->patroname; ?>
      </td>
       		<td>
			<?php echo $row->email; ?>
		</td>
       		<td>
			<?php echo $row->status; ?>
		</td>
       		<td>
			<?php echo $row->created; ?>
		</td>
       		<td>
			<?php echo $row->role; ?>
		</td>
       		<td>
			<?php echo $row->country; ?>
		</td>
       		<td>
			<?php echo $row->city; ?>
		</td>
       		<td>
			<?php echo $row->index; ?>
		</td>
       		<td>
			<?php echo $row->address; ?>
		</td>
       		<td>
			<?php echo $row->phone; ?>
		</td>
       		<td>
			<?php echo $row->alias; ?>
		</td>
       		<td>
			<?php echo $row->birthday; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
