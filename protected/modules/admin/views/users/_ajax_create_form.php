<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_ajax_create_form.php">
 * </description>
 **********************************************************************************************************************/
?>
<div id='users-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?= Yii::t('admin', 'Создание пользователя') ?></h3>
  </div>

  <div class="modal-body">

    <div class="form">

      <?php

      $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'users-create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'                 => 'post',
        'action'                 => array("users/create"),
        'type'                   => 'horizontal',
        'htmlOptions'            => array(
          'onsubmit' => "return false;", /* Disable normal form submit */
        ),
        'clientOptions'          => array(
          'validateOnType'   => TRUE,
          'validateOnSubmit' => TRUE,
          'afterValidate'    => 'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'

        ),

      )); ?>
        <h6>
          <?= Yii::t('admin', 'Поля') ?>
            <span class="required">*</span> <?= Yii::t('admin', 'обязательны') ?>
        </h6>

        <?php echo $form->errorSummary($model, 'Opps!!!', NULL, array('class' => 'alert alert-error span12')); ?>
          <div class="control-group">
          <div class="row-fluid">
              <div class="span4">
                  <?php echo $form->labelEx($model, 'lastname'); ?>
                  <?php echo $form->textField($model, 'lastname', array('size' => 60, 'maxlength' => 128)); ?>
                  <?php echo $form->error($model, 'lastname'); ?>

                  <?php echo $form->labelEx($model, 'email'); ?>
                  <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 64)); ?>
                  <?php echo $form->error($model, 'email'); ?>

                  <?php echo $form->labelEx($model, 'created'); ?>
                  <?php echo $form->textField($model, 'created'); ?>
                  <?php echo $form->error($model, 'created'); ?>

                  <?php echo $form->labelEx($model, 'city'); ?>
                  <?php echo $form->textField($model, 'city', array('size' => 60, 'maxlength' => 128)); ?>
                  <?php echo $form->error($model, 'city'); ?>

                  <?php echo $form->labelEx($model, 'phone'); ?>
                  <?php echo $form->textField($model, 'phone', array('size' => 32, 'maxlength' => 32)); ?>
                  <?php echo $form->error($model, 'phone'); ?>

                  <?php echo $form->labelEx($model, 'skype'); ?>
                  <?php echo $form->textField($model, 'skype', array('size' => 60, 'maxlength' => 512)); ?>
                  <?php echo $form->error($model, 'skype'); ?>
              </div>
              <div class="span4">
                  <?php echo $form->labelEx($model, 'firstname'); ?>
                  <?php echo $form->textField($model, 'firstname', array('size' => 32, 'maxlength' => 32)); ?>
                  <?php echo $form->error($model, 'firstname'); ?>

                  <?php echo $form->labelEx($model, 'password'); ?>
                  <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 128)); ?>
                  <?php echo $form->error($model, 'password'); ?>

                  <?php echo $form->labelEx($model, 'role'); ?>
                  <?php echo $form->textField($model, 'role', array('size' => 32, 'maxlength' => 32)); ?>
                  <?php echo $form->error($model, 'role'); ?>

                  <?php echo $form->labelEx($model, 'index'); ?>
                  <?php echo $form->textField($model, 'index', array('size' => 32, 'maxlength' => 32)); ?>
                  <?php echo $form->error($model, 'index'); ?>

                  <?php echo $form->labelEx($model, 'alias'); ?>
                  <?php echo $form->textField($model, 'alias', array('size' => 60, 'maxlength' => 500)); ?>
                  <?php echo $form->error($model, 'alias'); ?>

                  <?php echo $form->labelEx($model, 'vk'); ?>
                  <?php echo $form->textField($model, 'vk', array('size' => 60, 'maxlength' => 2048)); ?>
                  <?php echo $form->error($model, 'vk'); ?>
              </div>
              <div class="span4">
                  <?php echo $form->labelEx($model, 'patroname'); ?>
                  <?php echo $form->textField($model, 'patroname', array('size' => 60, 'maxlength' => 128)); ?>
                  <?php echo $form->error($model, 'patroname'); ?>

                  <?php echo $form->labelEx($model, 'status'); ?>
                  <?php echo $form->textField($model, 'status'); ?>
                  <?php echo $form->error($model, 'status'); ?>

                  <?php echo $form->labelEx($model, 'country'); ?>
                  <?php echo $form->textField($model, 'country', array('size' => 3, 'maxlength' => 3)); ?>
                  <?php echo $form->error($model, 'country'); ?>

                  <?php echo $form->labelEx($model, 'address'); ?>
                  <?php echo $form->textField($model, 'address', array('size' => 60, 'maxlength' => 256)); ?>
                  <?php echo $form->error($model, 'address'); ?>

                  <?php echo $form->labelEx($model, 'skidka'); ?>
                  <?php echo $form->textField($model, 'skidka'); ?>
                  <?php echo $form->error($model, 'skidka'); ?>

                  <?php echo $form->labelEx($model, 'default_manager'); ?>
                  <?php echo $form->textField($model, 'default_manager'); ?>
                  <?php echo $form->error($model, 'default_manager'); ?>
              </div>
          </div>


        </div>

    </div>
    <!--end modal body-->
  </div>
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'  => 'submit',
                'type'        => 'info',
                //'size'        => 'mini',
                'icon'        => 'ok white',
                'label'       => $model->isNewRecord ? Yii::t('admin', 'Добавить') : Yii::t('admin', 'Сохранить'),
                'htmlOptions' => array('onclick' => 'create();'),
            )
        );
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'reset',
            'type'       => 'danger',
            //'size'       => 'mini',
            'icon'       => 'remove white',
            'label'      => Yii::t('admin', 'Сброс'),
        )); ?>
    </div><!--end modal footer-->
    <?php $this->endWidget(); ?>

</div><!--end modal-->

<script type="text/javascript">
  function create() {
    var data = $("#users-create-form").serialize();
    jQuery.ajax({
        type: 'POST',
        url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/users/create"); ?>',
        data: data,
        success: function (data) {
          //alert("succes:"+data);
          if (data != "false") {
            $('#users-create-modal').modal('hide');
            $.fn.yiiGridView.update('users-grid', {
              //renderView(data);
              //alert(data);
          });

        }

      },
      error
  :
    function (data) { // if error occured
      alert("Error occured, please try again");
      alert(data);
    },

    dataType:'html'
  }
  )
  ;

  }

  function renderCreateForm() {
    $('#users-create-form').each(function () {
      this.reset();
    });
    $('#users-view-modal').modal('hide');
    $('#users-create-modal').modal({
      show: true
    });
  }

</script>
