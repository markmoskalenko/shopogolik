<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="update.php">
* </description>
**********************************************************************************************************************/?>
<h1><?=Yii::t('admin','Профиль пользователя')?> <?= $model->email.' (ID:'.$model->uid.')';?></h1>
<!-- =============================================================== -->
<div class="row-fluid">
<div class="span5">
  <?
  echo $this->renderPartial('_form', array('model'=>$model));
  ?>
</div>
<div class="span7">
<!-- ==================  Платежи ==================== -->
  <div class="ordr-header-block" id="headerblock-payments-view-<?=$model->uid?>">
    <h3><i class="icon-barcode"></i><?= Yii::t('admin', 'Пополнения счёта')?></h3>
    <?
    $this->widget('bootstrap.widgets.TbGridView', array(
      'id'           => 'payment-grid-'.$model->uid,
      'dataProvider' => $model->payments->search(15),
//      'filter'       => $model,
      'type'         => 'striped bordered condensed',
      'template'     => '{summary}{items}{pager}',
      'columns'      => array(
        array('name'=>'sum',
            'value'=>'sprintf(\'%01.2f\',$data->sum)',
        ),
        'description',
        array(
          'name'   => 'status',
          'filter' => array(
            1 => Yii::t('admin', 'Зачисление или возврат средств') . '(1)',
            2 => Yii::t('admin', 'Снятие средств') . '(2)',
            3 => Yii::t('admin', 'Ожидание зачисления средств') . '(3)',
            4 => Yii::t('admin', 'Отмена ожидания зачисления средств') . '(4)',
            5 => Yii::t('admin', 'Отправка внутреннего перевода средств') . '(5)',
            6 => Yii::t('admin', 'Получение внутреннего перевода средств') . '(6)',
          ),
          'value'  => '$data->text_status." (".$data->status.")"',
        ),
        array(
          'type'  => 'raw',
          'name'  => 'date',
          'value' => 'date("Y-m-d H:i:s", $data->date)',
        ),
      ),
    ));
 ?>
  </div>
    <div class="ordr-header-block" id="headerblock-order-payments-view-<?=$model->uid?>">
        <h3><i class="icon-barcode"></i><?= Yii::t('admin', 'Платежи по заказам')?></h3>
    <? $this->widget('application.components.widgets.OrderPaymentsBlock', array(
        'orderId'  => false,
        'userId'  => $model->uid,
        'pageSize' => 15,
      ));
    ?>
    </div>
 <!-- ==================  Адреса ==================== -->
  <div class="ordr-header-block" id="headerblock-addresses-<?=$model->uid?>">
    <h3><i class="icon-barcode"></i><?=Yii::t('admin','Адреса')?></h3>
    <div class="form">
        <div class="control-group">
          <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'addresses-grid-'.$model->uid,
            'dataProvider'=>$model->addresses->search(5),
            'type'=>'striped bordered condensed',
            'template'=>'{summary}{items}{pager}',
            'columns'=>array(
            'country',
            'city',
            'index',
            'phone',
            'address',
            'firstname',
            'patroname',
            'lastname',
          ),
          ));
          ?>
        </div>
    </div>
  </div>
    <!-- ==================  Корзина пользователя ==================== -->
    <div class="ordr-header-block" id="headerblock-userCart-<?=$model->uid?>">
        <h3><i class="icon-shopping-cart"></i><?=Yii::t('admin','Корзина пользователя')?></h3>
        <div class="form">
            <div class="control-group">
             <?//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ?>
                <? if (count($userCart->cartRecords) > 0) { ?>
                    <!-- ================================ -->
                    <div id="cart-table" style="visibility:visible">
                        <?php foreach ($userCart->cartRecords as $k => $item) { ?>
                            <? $this->widget(
                              'application.components.widgets.OrderItem',
                              array(
                                'orderItem'   => $item,
                                'readOnly'    => true,
                                'imageFormat' => '_200x200.jpg',
                                'allowDelete' => false,
//                                'adminMode' =>TRUE,
//                                'publicComments'=>FALSE,
                                'lazyLoad' => FALSE,
                              )
                            );
                            ?>
                        <?php } ?>
                        <div class="clear"></div>
                        <div class="cart-footer">
                            <div style="float: right; text-align: right;">
                                <div class="cart-total">
                                    <strong><?= Yii::t('main', 'Итого') ?>:</strong><?= Formulas::priceWrapper($userCart->total) ?>
                                </div>
                                <div class="cart-discont">
                                    <? if ($userCart->totalDiscount > 0) {
                                        echo Yii::t('main', 'Экономия') . ': ' . Formulas::priceWrapper(
                                            $userCart->totalDiscount
                                          ) . '';
                                    } ?>
                                </div>
                                <br>
                            </div>

                        </div>
                    </div>
                <?
                } else {
                    ?>
                    <div class="warning"><?= Yii::t('main', 'Ваша корзина пуста!') ?></div>
                <? } ?>
             <?//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ?>
            </div>
        </div>
    </div>
</div>
</div>
<!-- ========== Заказы пользователя ============================== -->
<div>
  <?php
  //======================================
  $this->widget('application.modules.admin.components.widgets.OrdersListBlock', array(
    'type' => NULL,
    'name' => Yii::t('main','Заказы пользователя'),
    'uid'=>$model->uid,
    'filter'=>true,
    'idPrefix' =>'user-orders-'.$model->uid,
    'pageSize' =>10,
  ));
  ?>
</div>

<?//- Internal message ----------------------------------------------?>
<div id="new-intermal-message-to-<?=$model->uid?>" title="<?=Yii::t('main','Сообщение пользователю').' '.$model->email?>" style="display: none;">
  <form id="new-intermal-message-form-to-<?=$model->uid?>">
      <label><b><?=Yii::t('Admin','Текст сообщения')?>:</b></label><br/>
    <input type="hidden" name="message[uid]" value="<?=$model->uid?>" />
    <textarea cols="80" rows="3" name="message[message]" id="message-<?=$model->uid?>"></textarea>
  </form><br/>
  <button id="change" onclick="
    $('#new-intermal-message-to-<?=$model->uid?>').dialog('close');
    var msg = $('#new-intermal-message-form-to-<?=$model->uid?>').serialize();
    $.post('/admin/message/sendNote',msg, function(){
    $('#message-<?=$model->uid?>').val('');
    },'text');
    return false;"><?=Yii::t('admin','Отправить')?></button>
  &nbsp;&nbsp;
  <button id="cancel" onclick="$('#new-intermal-message-to-<?=$model->uid?>').dialog('close');
    $('#message-<?=$model->uid?>').val('');
    return false;"><?=Yii::t('admin','Отмена')?></button>
  <br><br>
</div>
<script type="text/javascript">
  $("#new-intermal-message-to-<?=$model->uid?>").dialog({
    autoOpen: false,
    width: "auto",
    modal: true,
    resizable: false
  });
</script>
<?//- Internal email ----------------------------------------------?>
<div id="new-intermal-email-to-<?=$model->uid?>" title="<?=Yii::t('main','EMail пользователю').' '.$model->email?>" style="display: none;">
  <form id="new-intermal-email-form-to-<?=$model->uid?>">
    <label><b><?=Yii::t('Admin','Текст сообщения')?>:</b></label><br/>
    <input type="hidden" name="message[uid]" value="<?=$model->uid?>" />
    <textarea cols="80" rows="3" name="message[message]" id="message-<?=$model->uid?>"></textarea>
  </form><br/>
  <button id="change" onclick="
    $('#new-intermal-email-to-<?=$model->uid?>').dialog('close');
    var msg = $('#new-intermal-email-form-to-<?=$model->uid?>').serialize();
    $.post('/admin/message/sendMail',msg, function(){
    $('#message-<?=$model->uid?>').val('');
    },'text');
    return false;"><?=Yii::t('admin','Отправить')?></button>
  &nbsp;&nbsp;
  <button id="cancel" onclick="$('#new-intermal-email-to-<?=$model->uid?>').dialog('close');
    $('#message-<?=$model->uid?>').val('');
    return false;"><?=Yii::t('admin','Отмена')?></button>
  <br><br>
</div>
<script type="text/javascript">
  $("#new-intermal-email-to-<?=$model->uid?>").dialog({
    autoOpen: false,
    width: "auto",
    modal: true,
    resizable: false
  });
</script>
<script>
  $(function() {
    $( "#headerblock-payments-view-<?=$model->uid?>" ).accordion({
      collapsible: true,
      active: 0
      //disabled: true
    });
    $( "#headerblock-addresses-<?=$model->uid?>" ).accordion({
      collapsible: true,
      active: 0
      //disabled: true
    });
      $( "#headerblock-userCart-<?=$model->uid?>" ).accordion({
          collapsible: true,
          active: 0
          //disabled: true
      });
  });
  //  ordr-header-block
</script>
