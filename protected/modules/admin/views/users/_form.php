<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_form.php">
* </description>
**********************************************************************************************************************/?>
<? ///////////////////// Профиль пользователя (данные) ///////////////////////////// ?>
<div class="ordr-header-block" id="headerblock-personal-<?=$model->uid?>">
  <h3><i class="icon-barcode"></i><?=Yii::t('admin','Данные')?></h3>
  <div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
      'id'=>'user-update-form-id-'.$model->uid,
      'enableAjaxValidation'=>false,
      'enableClientValidation'=>false,
      'method'=>'post',
      'action'=>array("/admin/orders/update"),
      'type'=>'horizontal',
    )); ?>
      <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>




      <div class="control-group">
          <? echo $form->uneditableRow($model,'uid',array('style'=>'width:50px;')); ?>
          <? echo $form->uneditableRow($model, 'lastname'); ?>
          <? echo $form->uneditableRow($model, 'firstname'); ?>
          <? echo $form->uneditableRow($model, 'patroname'); ?>
          <? echo $form->uneditableRow($model, 'email'); ?>
          <? echo $form->uneditableRow($model, 'phone'); ?>
          <div style="margin-left: 20px;background: #ffa8ac;width: 45px; padding: 3px;text-align: center;color: #ffffff;">
              <? echo $model->country.' '.$model->city.' '.$model->index.' '.$model->address; ?>
          </div>
        <h4>
        <?
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'size'=>'mini',
            'type'=>'success',
            'icon'=>'envelope white',
            'label'=>Yii::t('admin','E-Mail'),
            'htmlOptions'=>array('onclick'=>'$("#new-intermal-email-to-'.$model->uid.'").dialog("open");return false;'),
        ));
        ?>
        <?
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'info',
            'size'=>'mini',
            'icon'=>'ok white',
            'label'=>Yii::t('admin','Сообщение'),
            'htmlOptions'=>array('onclick'=>'$("#new-intermal-message-to-'.$model->uid.'").dialog("open");return false;'),
        ));
        ?>
        </h4>
      </div>
  </div>
  <?php $this->endWidget(); ?>
</div>
<? // ///////////////////////////////// Статутс /////////////////////////////////// ?>
<div class="ordr-header-block" id="headerblock-status-<?=$model->uid?>">
  <h3><i class="icon-circle"></i><?=Yii::t('main','Статус')?></h3>
  <div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
      'id'=>'user-update-form-status-'.$model->uid,
      'enableAjaxValidation'=>false,
      'enableClientValidation'=>false,
      'method'=>'post',
      'action'=>array('/admin/users/update/id/'.$model->uid),
      'type'=>'horizontal',
      'htmlOptions'=>array(
      'enctype'=>'multipart/form-data',
      )
    )); ?>
      <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
      <div class="control-group">
        <? echo $form->hiddenField($model,'uid',array());
        if(Yii::app()->user->notInRole(array('superAdmin','topManager'))) {
          $htmlOptions=array('disabled'=>'disabled');
        } else {
          $htmlOptions=false;
        }
        echo $form->dropDownListRow($model, 'status', array(
            '0' => Yii::t('admin', 'Не активирован'),
            '1' => Yii::t('admin', 'Активирован'),
            '-1' => Yii::t('admin', 'Заблокирован')),$htmlOptions
        );
        ?>
        <?
        echo $form->textFieldRow($model, 'skidka',$htmlOptions);
        echo $form->dropDownListRow($model, 'role', AccessRights::getRoles(),$htmlOptions);
        ?>
          <h4>
              <?php
              $this->widget('bootstrap.widgets.TbButton', array(
                  'buttonType'=>'ajaxSubmit',
                  'type'=>'info',
                  'size'=>'mini',
                  'icon'=>'ok white',
                  'htmlOptions'=>array('title'=>Yii::t('admin','Сохранить')),
                  'label'=>Yii::t('admin',''),
                  'url'=>'/admin/users/update/id/'.$model->uid,
                  'ajaxOptions'=> array(
                  'complete'=>'js:function(){reloadSelectedTab();}',
                  ),
              ));
              ?>
              <?php
              $this->widget('bootstrap.widgets.TbButton', array(
                  'buttonType'=>'reset',
                  'type'=>'danger',
                  'size'=>'mini',
                  'icon'=>'remove white',
                  'htmlOptions'=>array('title'=>Yii::t('admin','Отмена')),
                  'label'=>Yii::t('admin',''),
              ));
              ?>
          </h4>
      </div>
  </div>
  <?php $this->endWidget(); ?>
</div>
<? // ///////////////////////////////// Статистика //////////////////////////////// ?>
<div class="ordr-header-block" id="headerblock-statistic-<?=$model->uid?>">
  <h3><i class="icon-list"></i><?=Yii::t('admin','Статистика')?></h3>
 <div class="form">
      <div class="control-group">
        <?$this->widget('application.components.widgets.ReportBlock', array(
          'report' => ReportsSystem::getReport('STAT_USER_REVIEW', array(':uid'=>$model->uid)),
        ));
        ?>
      </div>
  </div>
</div>
<? /////////////////////// Управление балансом пользоваьтеля ////////////////////// ?>
<div class="ordr-header-block" id="headerblock-payment-<?=$model->uid?>">
  <h3><i class="icon-money icon-spin"></i><?=Yii::t('main','Управление балансом пользователя')?></h3>
  <div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
      'id'=>'user-update-form-payment-'.$model->uid,
      'enableAjaxValidation'=>false,
      'enableClientValidation'=>false,
      'method'=>'post',
      'action'=>array('/admin/users/balance'),
      'type'=>'horizontal',
      'htmlOptions'=>array('enctype'=>'multipart/form-data',),
    )); ?>
      <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
      <div class="control-group">
        <?   echo $form->hiddenField($model,'uid');
             echo $form->uneditableRow($model,'userBalance');
        if(Yii::app()->user->inRole(array('superAdmin','topManager')) && Yii::app()->user->checkAccess('admin/users/balance/')) { ?>
        <div class="control-group ">
          <label class="control-label" for="sum"><?= Yii::t('admin', 'Сумма') ?></label>
            <div class="controls">
                <input type="text" name="sum" id="sum" value="0.00"/>
            </div>
        </div>
        <div class="control-group ">
          <label class="control-label" for="operation"><?= Yii::t('admin', 'Операция') ?></label>
            <div class="controls">
              <select name="operation" id="operation">
                <option value="1"><?= Yii::t('admin', 'Пополнение счёта') ?></option>
                <option value="2"><?= Yii::t('admin', 'Снятие средств') ?></option>
              </select>
            </div>
        </div>
        <div class="control-group ">
          <label class="control-label" for="desc"><?= Yii::t('admin', 'Описание') ?></label>
            <div class="controls">
                <textarea style="border-radius:0;width:90%;height: 70px;font-size:12px;resize:none;" cols="60" rows="2" name="desc" id="desc"><?= Yii::t('admin', 'Операция со счётом вручную') ?></textarea>
            </div>
        </div>
       <? } ?>
          <h4>
              <?php
              $this->widget('bootstrap.widgets.TbButton', array(
                  'buttonType'=>'ajaxSubmit',
                  'type'=>'info',
                  'size'=>'mini',
                  'icon'=>'ok white',
                  'htmlOptions'=>array('title'=>Yii::t('admin','Сохранить')),
                  'label'=>Yii::t('admin',''),
                  'url'=>'/admin/users/balance',
                  'ajaxOptions' => array(
                    'success' => 'js:function(){reloadSelectedTab(); dsAlert(\''.Yii::t('admin','Сохранено').'\',\''.Yii::t('admin','Подтверждение').'\',true);}',
                    'error' => 'js:function(){dsAlert(\''.Yii::t('admin','Ошибка').'\',\''.Yii::t('admin','Ошибка').'\',true);}',
                  ),
              ));
              ?>
              <?php
              $this->widget('bootstrap.widgets.TbButton', array(
                  'buttonType'=>'reset',
                  'type'=>'danger',
                  'size'=>'mini',
                  'icon'=>'remove white',
                  'htmlOptions'=>array('title'=>Yii::t('admin','Отмена')),
                  'label'=>Yii::t('admin',''),
              ));
              ?>
          </h4>
      </div>
  </div>
  <?php $this->endWidget(); ?>
</div>

<script>
  $(function() {
    $( "#headerblock-personal-<?=$model->uid?>" ).accordion({
      collapsible: true,
      active: 0
    });
    $( "#headerblock-status-<?=$model->uid?>" ).accordion({
      collapsible: true,
      active: 0
    });
    $( "#headerblock-statistic-<?=$model->uid?>" ).accordion({
      collapsible: true,
      active: 0
    });
    $( "#headerblock-payment-<?=$model->uid?>" ).accordion({
      collapsible: true,
      active: 0
    });
  });
</script>

