<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	'Users',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').slideToggle('fast');
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('users-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

?>
<div class="row-fluid">
    <div class="span4"><h1><?=Yii::t('admin','Пользователи')?></h1></div>
    <div class="span8">
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
	'htmlOptions'=>array(
		'class'=>''
	)
));
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
		array('label'=>Yii::t('admin','Создать'), 'icon'=>'icon-plus', 'url'=>'javascript:void(0);','linkOptions'=>array('onclick'=>'renderCreateForm()')),
   //   array('label'=>Yii::t('admin','Список'), 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('index'),'active'=>true, 'linkOptions'=>array()),
		array('label'=>Yii::t('admin','Поиск'), 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
		array('label'=>Yii::t('admin','PDF'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GeneratePdf'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
		array('label'=>Yii::t('admin','Excel'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateExcel'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
        array('label'=>Yii::t('admin','Список клиентов'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateMailList',
        array('criteria'=>'role in (\'user\') and status=1')), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
        array('label'=>Yii::t('admin','Список менеджеров'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateMailList',
        array('criteria'=>'role not in (\'user\') and status=1')), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
	),
));
$this->endWidget();
?>
    </div>
</div>



<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
    'filter'=>$model,
        'type'=>'striped bordered condensed',
        'template'=>'{summary}{pager}{items}{pager}',
	'columns'=>array(
    array(
      'type' => 'raw',
      'filter'=>false,
      'name' => 'uid',
      'value' => 'CHtml::link($data->uid, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->email."\");return false;"))',
    ),
        array(

            'type'=>'raw',
            'value'=>'"
            <nobr>
		      <a href=\'javascript:void(0);\' onclick=\'renderView(".$data->uid.")\'   class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
		      <a href=\'javascript:void(0);\' onclick=\'renderUpdateForm(".$data->uid.")\'   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		      <a href=\'javascript:void(0);\' onclick=\'delete_record(".$data->uid.")\'   class=\'btn btn-small view\'  ><i class=\'icon-trash\'></i></a>
		    </nobr>
		     "',
            'htmlOptions'=>array('style'=>'width:150px;')
        ),
    array(
      'type' => 'raw',
      'name' => 'role',
      'value' => function ($data, $row) {
        $res = '<span style="color:'.(($data->role=='user')?'black':'green').'">'.$data->role. '</span>';
        return $res;
      },
    ),
		'firstname',
    'lastname',
    array(
      'type' => 'raw',
      'name' => 'email',
      'value' => 'CHtml::link($data->email, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->email."\");return false;"))',
    ),
        'default_manager',
/*    'phone',
    'country',
    'city',
    'address', */
      array(
        'name'=>'status',
        'class' => 'CCheckBoxColumn',
        'checked'=>'$data->status==1',
        'header'=>Yii::t('admin','Вкл.'),
        //'disabled'=>'true',
        'selectableRows'=>0,
      ),
    'created',
    'paymentsSUM',
    array(
      'name'=>'userBalance',
      'value'=>'Users::getBalance($data->uid)',
    ),
    'ordersCNT',
    'ordersLast',
    array (
      'type'=>'raw',
      'name'=>'ordr_user_stat',
    ),
      'skidka',
		/*
		'created',
		'patroname',
		'role',
		'lastname',
		'country',
		'city',
		'index',
		'address',
		'phone',
		'alias',
		'birthday',
		*/

        
	),
)); 


 $this->renderPartial("_ajax_update");
 $this->renderPartial("_ajax_create_form",array("model"=>$model));
 $this->renderPartial("_ajax_view");

 ?>


<script type="text/javascript">
function delete_record(id)
{

  if(!confirm("Are you sure you want delete this?"))
   return;

 //  $('#ajaxtest-view-modal').modal('hide');

 var data="id="+id;


  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/users/delete"); ?>',
   data:data,
success:function(data){
                 if(data=="true")
                  {
                     $('#users-view-modal').modal('hide');
                     $.fn.yiiGridView.update('users-grid', {

                         });

                  }
                 else
                   alert("deletion failed");
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data));
         alert("Error occured, please try again");
       //  alert(data);
    },

  dataType:'html'
  });

}
</script>

<style type="text/css" media="print">
body {visibility:hidden;}
.printableArea{visibility:visible;}
</style>
<script type="text/javascript">
function printDiv()
{

window.print();

}
</script>


