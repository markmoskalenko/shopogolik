<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update.php">
* </description>
**********************************************************************************************************************/?>
<div id="users-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#users-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/users/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#users-update-modal').modal('hide');
                  $.fn.yiiGridView.update('users-grid', {
                     
                         });
                   //renderView(data);
                   alert(data);
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#users-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/users/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#users-update-modal-container').html(data); 
                 $('#users-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
