<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update_form.php">
* </description>
**********************************************************************************************************************/?>
<div id='users-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
   
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Редактирование пользователя')?> #<?php echo $model->uid; ?></h3>
    </div>

    <div class="modal-body">

    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'users-update-form',
	'enableAjaxValidation'=>false,
    'enableClientValidation'=>false,
    'method'=>'post',
    'action'=>array("users/update"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
    'onsubmit'=>"return false;",/* Disable normal form submit */
  //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
    ),
)); ?>
		<h6><?=Yii::t('admin','Поля')?><span class="required">*</span> <?=Yii::t('admin','обязательны')?></h6>
    	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        <?php echo $form->textFieldRow($model,'uid',array('class'=>'input-mini','readonly'=>true)); ?>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->uneditableRow($model,'lastname',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->uneditableRow($model,'firstname',array('size'=>32,'maxlength'=>32)); ?>
        <?php echo $form->uneditableRow($model,'patroname',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->textFieldRow($model,'alias',array('size'=>60,'maxlength'=>500)); ?>
        <?php echo $form->uneditableRow($model,'email',array('size'=>60,'maxlength'=>64)); ?>
        <?php echo $form->passwordFieldRow($model,'password',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->uneditableRow($model,'created'); ?>

        <? if(Yii::app()->user->notInRole(array('superAdmin','topManager'))) {
            $htmlOptions=array('disabled'=>'disabled');
        } else {
            $htmlOptions=false;
        }
        echo $form->dropDownListRow($model, 'status', array(
                '0' => Yii::t('admin', 'Не активирован'),
                '1' => Yii::t('admin', 'Активирован'),
                '-1' => Yii::t('admin', 'Заблокирован')),$htmlOptions
        );
        echo $form->dropDownListRow($model, 'role', AccessRights::getRoles(),$htmlOptions); ?>
        <?php echo $form->textFieldRow($model,'country',array('size'=>3,'maxlength'=>3)); ?>
    </div>
    <div class="span6"><?php echo $form->textFieldRow($model,'city',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->textFieldRow($model,'index',array('size'=>32,'maxlength'=>32)); ?>
        <?php echo $form->textFieldRow($model,'address',array('size'=>60,'maxlength'=>256)); ?>
        <?php echo $form->textFieldRow($model,'phone',array('size'=>32,'maxlength'=>32)); ?>
        <?php echo $form->textFieldRow($model,'skype',array('size'=>60,'maxlength'=>512)); ?>
        <?php echo $form->textFieldRow($model,'vk',array('size'=>60,'maxlength'=>2048)); ?>
        <?php echo $form->textFieldRow($model,'skidka',$htmlOptions); ?>
        <?php echo $form->textFieldRow($model,'default_manager'); ?>
    </div>
</div>

</div><!--end modal body-->
  
    </div>
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            //'id'=>'sub2',
            'type'=>'info',
            'icon'=>'ok white',
            'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'update();'),
        ));
        ?>
    </div><!--end modal footer-->
    <?php $this->endWidget(); ?>

</div>
<!--end modal-->



