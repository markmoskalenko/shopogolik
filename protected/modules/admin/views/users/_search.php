<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_search.php">
* </description>
**********************************************************************************************************************/?>
<?php  $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'search-users-form',
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
));  ?>

        <?php echo $form->textFieldRow($model,'uid',array('class'=>'input-mini')); ?>
<div class="row-fluid">
    <div class="span3">
        <?php echo $form->textFieldRow($model,'firstname',array('class'=>'','maxlength'=>32)); ?>
        <?php echo $form->textFieldRow($model,'email',array('class'=>'','maxlength'=>64)); ?>
        <?php echo $form->textFieldRow($model,'status',array('class'=>'')); ?>
        <?php echo $form->textFieldRow($model,'created',array('class'=>'')); ?>
    </div>
    <div class="span3">
        <?php echo $form->textFieldRow($model,'patroname',array('class'=>'','maxlength'=>128)); ?>
        <?php echo $form->textFieldRow($model,'role',array('class'=>'','maxlength'=>32)); ?>
        <?php echo $form->textFieldRow($model,'lastname',array('class'=>'','maxlength'=>128)); ?>
        <?php echo $form->textFieldRow($model,'country',array('class'=>'','maxlength'=>3)); ?>
    </div>
    <div class="span3">
        <?php echo $form->textFieldRow($model,'city',array('class'=>'','maxlength'=>128)); ?>
        <?php echo $form->textFieldRow($model,'index',array('class'=>'','maxlength'=>32)); ?>
        <?php echo $form->textFieldRow($model,'address',array('class'=>'','maxlength'=>256)); ?>
        <?php echo $form->textFieldRow($model,'phone',array('class'=>'','maxlength'=>32)); ?>
    </div>
    <div class="span3">
        <?php echo $form->textFieldRow($model,'alias',array('class'=>'','maxlength'=>500)); ?>
        <?php echo $form->textFieldRow($model,'skype',array('class'=>'','maxlength'=>512)); ?>
        <?php echo $form->textFieldRow($model,'vk',array('class'=>'','maxlength'=>2048)); ?>
        <?php echo $form->textFieldRow($model,'default_manager',array('class'=>'')); ?>
        <?// echo $form->textFieldRow($model,'birthday',array('class'=>'span5')); ?>
    </div>
</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'info','icon'=>'search white','label'=>Yii::t('admin','Поиск'))); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button','type'=>'danger','icon'=>'remove white','label'=>Yii::t('admin','Сброс'), 'htmlOptions'=>array('class'=>''))); ?>
	</div>

<?php $this->endWidget(); ?>


<?php /* Alexys $cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile(Yii::app()->request->baseUrl.'/themes/admin/css/jquery-ui-1.9.2.custom.css');
*/
?>	
   <script>
	$(".btnreset").click(function(){
		$(":input","#search-users-form").each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		if (type == "text" || type == "password" || tag == "textarea") this.value = "";
		else if (type == "checkbox" || type == "radio") this.checked = false;
		else if (tag == "select") this.selectedIndex = "";
	  });
	});
   </script>

