<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="list.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
  'id'=>'operators-grid',
  'dataProvider'=>$dataProvider,
  //'filter'=>$dataProvider->model,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{pager}{items}{pager}',
  'columns'=>array(
    array(
      'type' => 'raw',
      'name' => 'uid',
      'value' => 'CHtml::link($data->uid, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->email."\");return false;"));',
    ),
    array(
      'type' => 'raw',
      'name' => 'email',
      'value' => 'CHtml::link($data->email, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->email."\");return false;"))
      ." (".$data->firstname." ".$data->lastname.")"',
    ),
    'phone',
    array(
      'name'=>'status',
      'class' => 'CCheckBoxColumn',
      'checked'=>'$data->status==1',
      'header'=>Yii::t('admin','Вкл.'),
      //'disabled'=>'true',
      'selectableRows'=>0,
    ),
    'created',
 'ordr_operator_inprocess',
 'ordr_operator_delay_buy',
 'ordr_operator_send',
 'ordr_operator_activity',
    array (
      'type'=>'raw',
      'name'=>'ordr_operator_stat',
    ),
  ),
));

