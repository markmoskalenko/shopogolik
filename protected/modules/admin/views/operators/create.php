<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="create.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Пользователи')=>array('index'),
  Yii::t('admin','Добавление'),
);

$this->menu=array(
	array('label'=>Yii::t('admin','Список пользователей'), 'url'=>array('index')),
	array('label'=>Yii::t('admin','Управление пользователями'), 'url'=>array('admin')),
);
?>

<h1><?=Yii::t('admin','Добавление нового пользователя')?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>