<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>Yii::t('admin','Весь список'), 'url'=>array('index')),
	array('label'=>Yii::t('admin','Добавить'), 'url'=>array('create')),
	array('label'=>Yii::t('admin','Изменить данные'), 'url'=>array('update', 'id'=>$model->uid)),
	array('label'=>Yii::t('admin','Удалить пользователя'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>Yii::t('admin','Are you sure you want to delete this item?'))),
        array('label'=>Yii::t('admin','Управление'), 'url'=>array('admin')),
);
?>

<h1><?=Yii::t('admin','Просмотр данных пользователя')?> #<?php echo $model->uid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'firstname',
		'email',		
		'status',
		'created',
		'patroname',
		'role',
		'lastname',
		'country',
		'city',
		'index',
		'address',
		'phone',
	),
)); ?>
