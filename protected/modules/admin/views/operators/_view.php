<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_view.php">
* </description>
**********************************************************************************************************************/?>
<?php
$aFillter = array(
    'manager' => $data->uid,
    'status_in'=>'1',//необработанные заказы
    
);
$aFillter_z = array(
    'manager' => $data->uid,
    'status'=>'2',//Задержка закупа
    
);
$aFillter_p = array(
    'manager' => $data->uid,
    'status'=>'8',//Задержка поставки
    
);

$aFillter_s_o = array(
    'manager' => $data->uid,
    'status_in'=>'7,9',//обработанные
);

?>


<div class="view">
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid), array('view', 'id'=>$data->uid)); ?>
	<br />

	<b>ФИО оператора:</b>
	<?php echo CHtml::encode($data->firstname).'&nbsp;'.CHtml::encode($data->patroname).'&nbsp;'.CHtml::encode($data->lastname); ?>
	<br />
        
        <b>Необработанные заказы:</b>
        <?php echo $data->getOperatorOrders($aFillter) ?>
        <br />
        <b>Задержка закупа:</b>
        <?php echo $data->getOperatorOrders($aFillter_z) ?>
        <br />
        <b>Задержка поставки:</b>
        <?php echo $data->getOperatorOrders($aFillter_p) ?>
        <br />
        <b>Сумма обработанных заказов:</b>
        <?php echo $data->getOperatorOrdersSumm($aFillter_s_o) ?>
        <br />
        
     

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br />
	

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('index')); ?>:</b>
	<?php echo CHtml::encode($data->index); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	*/ ?>

</div>