<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="admin.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Пользователи')=>array('index'),
  Yii::t('admin','Управление'),
);

$this->menu=array(
    array('label'=>Yii::t('admin','Список пользователей'), 'url'=>array('index')),
    array('label'=>Yii::t('admin','Добавить пользователя'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('users-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1><?=Yii::t('admin','Управление пользователями')?></h1>

<?php echo CHtml::link(Yii::t('admin','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'users-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{pager}{items}{pager}',
    'columns'=>array(
        'uid',
        'firstname',
                'lastname',
        'email',
      array(
        'name'=>'status',
        'class' => 'CCheckBoxColumn',
        'checked'=>'$data->status==1',
        'header'=>Yii::t('admin','Вкл.'),
        //'disabled'=>'true',
        'selectableRows'=>0,
      ),
        'created',
                'role',        
        /*
        'patroname',        
        'country',
        'city',
        'index',
        'address',
        'phone',
        */
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
