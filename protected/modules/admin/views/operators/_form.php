<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_form.php">
* </description>
**********************************************************************************************************************/?>
<?php


$aFilterSummOrder = array(
    'manager'=>$model->uid,
    'date_from'=>date('Y').'-'.date('m').'-01',
    'date_to'=>date('Y-m-d')
);

//echo date("d.m.Y H:i",$model->created);

?>
<script>
    $(function() {
        $( "#date_from" ).datepicker({dateFormat: "yy-mm-dd"});
        $( "#date_to" ).datepicker({dateFormat: "yy-mm-dd"});
    });
</script>

<div class="form">
    <b>ФИО:&nbsp;<?php echo $model->firstname.' '.$model->lastname.' '.$model->patroname   ?></b><br/>
    <b>Телефон:&nbsp;<?php echo $model->phone ?></b><br/>
    <b>Адрес:&nbsp;<?php echo $model->address ?></b><br/>
    <b>Дата регистрации:&nbsp;<?php echo date("d.m.Y H:i",$model->created) ?></b><br/>
    <b>Срок работы:&nbsp;<?php echo $model->getOperatorLife($model->created )?>&nbsp;дня</b><br/>
    <form id="manager-order-sum-<?=$model->uid?>" action="/admin/operators/ordersum">
    <b>Показать потраченную сумму за период с
    <input type="text" id="date_from" name="date_from"/>
     по
    <input type="text" id="date_to" name="date_to"/>
    <a href="javascript:void(0);" onclick="seeOrderSum(<?=$model->uid?>);return false;"><?=Yii::t('admin','Показать')?></a>
    </b>
    </form><br/>
    <p id="ordersSum<?=$model->uid?>">
    <b><?=Yii::t('admin','Сумма всех заказов за период c ')?>01.<?=date('m.Y')?> по <?=date('d.m.Y')?> - <?=$model->getOperatorOrdersSumm($aFilterSummOrder,true)?></b>
    </p>
    <br/>


    <?php
    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'action'=>'/admin/operators/update',
        'id'=>'operator-form-'.$model->uid,
        'enableAjaxValidation'=>false,
    ));
    ?>
    <?php echo $form->errorSummary($model); ?>
    <?=$form->hiddenField($model,'uid')?>

    <div class="row">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->hiddenField($model,'email'); ?>
            <?=$model->email?>
    </div>
    <?php echo $form->hiddenField($model,'password'); ?>

    <div class="row">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status',array(
                '0'=>Yii::t('admin','Не активирован'),
                '1'=>Yii::t('admin','Активирован'),
                '-1'=>Yii::t('admin','Заблокирован')
            )); ?>
            <?php echo $form->error($model,'status'); ?>
    </div>



    <div class="row">
            <?php echo $form->labelEx($model,'role'); ?>
            <?php echo $form->dropDownList($model,'role',AccessRights::getRoles()); ?>
            <?php echo $form->error($model,'role'); ?>
    </div>

<!--    <div class="row">
            <?php echo $form->labelEx($model,'country'); ?>
            <?php echo $form->hiddenField($model,'country'); ?>
            <?=$model->country?>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model,'city'); ?>
            <?php echo $form->hiddenField($model,'city'); ?>
            <?=$model->city?>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model,'index'); ?>
            <?php echo $form->hiddenField($model,'index'); ?>
            <?=$model->index?>
    </div>-->

    <div class="row buttons">
      <?    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        //'id'=>'sub2',
        'type'=>'primary',
        'icon'=>'ok white',
        'label'=>Yii::t('admin','Сохранить'),
        'htmlOptions'=>array('onclick'=>'saveOperator('.$model->id.'); return false;'),
      ));
      ?>
    </div>
    <?php $this->endWidget(); ?>

</div>
