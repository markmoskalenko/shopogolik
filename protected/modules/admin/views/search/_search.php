<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_search.php">
 * </description>
 **********************************************************************************************************************/
?>
    <div class="form">
        <? $form = $this->beginWidget(
          'bootstrap.widgets.TbActiveForm',
          array(
            'id'          => 'int-search-form',
            'type'        => 'search',
            'method'      => 'get',
            'htmlOptions' => array('class' => 'well'),
          )
        ); ?>
        <i class="icon-search"></i><input type="text"
                                          value="<?= (isset($model->query) && ($model->query)) ? $model->query : '' ?>"
                                          placeholder="<?= Yii::t('main', 'Введите строку для поиска') ?>" id="query"
                                          name="query"
                                          class="input-medium" style="width: 70%; margin: 0.2em 1em !important; padding: 5px !important;">
        <?
        $this->widget(
          'bootstrap.widgets.TbButton',
          array(
            'buttonType'  => 'ajaxSubmit',
            'type'        => 'info',
            'icon'        => 'ok white',
            'label'       => Yii::t('admin', 'Поиск'),
//            'loadingText'=>Yii::t('admin','Выполняется...'),
//             'completeText'=>Yii::t('admin','Выполнено'),
            'url'         => Yii::app()->createURL('/admin/search/index', array()),
            'htmlOptions' => array('onclick'=>'$(\'#search-content\').html(\''.Yii::t('admin','Осуществляется поиск, подождите несколько секунд...').'\')'),
            'ajaxOptions' => array('type'=>'GET','update' => '#search-content'),
          )
        );
        $this->endWidget();
        ?>

    </div>
<? if (isset($model->query) && ($model->query)) { ?>
    <div><?= Yii::t('admin', 'Вы искали') ?>: <?= $model->query ?></div>
<? } ?>
<?php
//    'onsubmit'=>"alert('test');$.fn.yiiGridView.update('search-grid', {data: $(this).serialize()});return false;",),
?>
