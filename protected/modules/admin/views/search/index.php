<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="index.php">
 * </description>
 **********************************************************************************************************************/
?>
<? if (!$isAjax) { ?>
<div id="search-content" class="well form-search">
  <? } ?>
  <?php
  $this->breadcrumbs = array(
    Yii::t('admin', 'Поиск'),
  );
  ?>



  <div class="search-form">
    <?php $this->renderPartial('_search', array('model' => $model)); ?>
  </div>
  <!-- search-form -->

  <?php
  if ($model->query) {
    $this->widget('bootstrap.widgets.TbGridView', array(
      'id'           => 'search-grid',
      'dataProvider' => $model->search(),
      //'filter'       => FALSE,
      'type'         => 'striped bordered condensed',
      'template'     => '{summary}{pager}{items}{pager}',
      'columns'      => array(
/*        array(
          'header' =>Yii::t('admin','Тип записи'),
          'type'  => 'raw',
          'name'  => 'type',
          'value' => '$data["type"]',
        ),
*/
        array(
          'header' =>Yii::t('admin','Ссылка'),
          'type'  => 'raw',
          'value' => function ($data) {
              return $data['type']::getAdminLink($data['pk']);
            }
        ),
        array(
          'header' =>Yii::t('admin','Описание'),
          'type'  => 'raw',
          'value' => function ($data) {
                return $data['type']::getAdminSearchSnippet($data['pk'],$data['query']);
            }
        ),
        /*    'id',
            'sum',
            'description',
            array(
              'name'   => 'status',
              'filter' => array(
                1 => Yii::t('admin', 'Зачисление или возврат средств') . '(1)',
                2 => Yii::t('admin', 'Снятие средств') . '(2)',
                3 => Yii::t('admin', 'Ожидание зачисления средств') . '(3)',
                4 => Yii::t('admin', 'Отмена ожидания зачисления средств') . '(4)',
                5 => Yii::t('admin', 'Отправка внутреннего перевода средств') . '(5)',
                6 => Yii::t('admin', 'Получение внутреннего перевода средств') . '(6)',
              ),
              'value'  => '$data->text_status." (".$data->status.")"',
            ),
            array(
              'type'  => 'raw',
              'name'  => 'date',
              'value' => 'date("Y-m-d H:i:s", $data->date)',
            ),
            array(
              'type'  => 'raw',
              'name'  => 'uid',
              'value' => 'CHtml::link($data->uid, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->u->email."\");return false;"))',
            ),
            array(
              'type'  => 'raw',
              'name'  => 'email',
              'value' => 'CHtml::link($data->u->email, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->u->email."\");return false;"))',
            ),
            array(
              'type'  => 'raw',
              'name'  => 'username',
              'value' => 'trim($data->u->lastname." ".$data->u->firstname)',
            ),
            array(
              'type'  => 'raw',
              'name'  => 'phone',
              'value' => '$data->u->phone',
            ),
            /*
            'check_summ',
            */
        /*    array(

              'type'        => 'raw',
              'value'       => '"
                      <a href=\'javascript:void(0);\' onclick=\'renderView(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
                      <a href=\'javascript:void(0);\' onclick=\'renderUpdateForm(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
                     "',
        //		      <a href=\'javascript:void(0);\' onclick=\'delete_record(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-trash\'></i></a>
              'htmlOptions' => array('style' => 'width:100px;')
            ),
        */
      ),
    ));
  }
  else {
    ?>
    <div><?=($isAjax) ? Yii::t('admin', 'Увы, ничего не найдено. Уточните запрос и повторите поиск...'):
          Yii::t('admin', 'Введите номер заказа, лота, имя или id клиента, трек-код, статус пользователя, заказа, лота и т.п.'); ?></div>
  <? } ?>
  <? if (!$isAjax) { ?>
</div>
<? } ?>
