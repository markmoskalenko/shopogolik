
    <div id='formulas-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Создать формулу')?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
   <?php
   
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	        'id'=>'formulas-create-form',
	        'enableAjaxValidation'=>false,
            'enableClientValidation'=>false,
            'method'=>'post',
            'action'=>array("formulas/create"),
	        'type'=>'horizontal',
	        'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
                    'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>

    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

            <?php echo $form->labelEx($model,'formula_id'); ?>
			<?php echo $form->textField($model,'formula_id',array('class'=>'span10','maxlength'=>256)); ?>
			<?php echo $form->error($model,'formula_id'); ?>

        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->labelEx($model,'formula'); ?>
                <?php echo $form->textArea($model,'formula',array('rows'=>8, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'formula'); ?>
            </div>
            <div class="span6">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textArea($model,'description',array('rows'=>8, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
        </div>


  </div>
  
  </div><!--end modal body-->
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

<!--Script section-->

<script type="text/javascript">
function create()
 {
   var data=$("#formulas-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/formulas/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#formulas-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('formulas-grid', {
                     
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#formulas-create-form').each (function(){
  this.reset();
   });
  $('#formulas-view-modal').modal('hide');
  $('#formulas-create-modal').modal({
   show:true
  });
}
</script>
