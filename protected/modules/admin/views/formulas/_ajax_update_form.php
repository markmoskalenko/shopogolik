    <div id='formulas-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Редактировать формула <strong style="color:#0093f5;">#<?php echo $model->id; ?></strong></h3>
    </div>
    
    <div class="modal-body">
 
    
    
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	    'id'=>'formulas-update-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("formulas/update"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
)); ?>
        <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
        <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        <?php echo $form->hiddenField($model,'id',array()); ?>

        <?php echo $form->labelEx($model,'formula_id'); ?>
        <?php echo $form->textField($model,'formula_id',array('class'=>'span10','maxlength'=>256)); ?>
        <?php echo $form->error($model,'formula_id'); ?>

        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->labelEx($model,'formula'); ?>
                <?php echo $form->textArea($model,'formula',array('rows'=>10, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'formula'); ?>
            </div>
            <div class="span6">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textArea($model,'description',array('rows'=>10, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
		</div>

  </div><!--end modal body-->
  
</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                //'id'=>'sub2',
                'type'=>'info',
                'icon'=>'ok white',
                'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
                'htmlOptions'=>array('onclick'=>'update();'),
            ));
            ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->



