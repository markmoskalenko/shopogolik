<?php  $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'search-formulas-form',
    	'action'=>Yii::app()->createUrl($this->route),
    	'method'=>'get',
));  ?>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
        <?php echo $form->textAreaRow($model,'formula',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
    </div>
    <div class="span6">
        <?php echo $form->textFieldRow($model,'formula_id',array('class'=>'span12','maxlength'=>256)); ?>
        <?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
    </div>
</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'info', 'icon'=>'search white', 'label'=>Yii::t('admin','Поиск'))); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'danger', 'icon'=>'icon-remove-sign white', 'label'=>Yii::t('admin','Сброс'))); ?>
	</div>

<?php $this->endWidget(); ?>


<?php /* Alexys $cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile(Yii::app()->request->baseUrl.'/themes/admin/css/jquery-ui.css');
*/
?>	
   <script>
	$(".btnreset").click(function(){
		$(":input","#search-formulas-form").each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		if (type == "text" || type == "password" || tag == "textarea") this.value = "";
		else if (type == "checkbox" || type == "radio") this.checked = false;
		else if (tag == "select") this.selectedIndex = "";
	  });
	});
   </script>

