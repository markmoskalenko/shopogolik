<div id="formulas-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#formulas-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/formulas/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#formulas-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('formulas-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#formulas-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/formulas/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#formulas-update-modal-container').html(data); 
                 $('#formulas-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
