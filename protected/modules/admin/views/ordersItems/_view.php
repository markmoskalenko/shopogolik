<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oid')); ?>:</b>
	<?php echo CHtml::encode($data->oid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iid')); ?>:</b>
	<?php echo CHtml::encode($data->iid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pic_url')); ?>:</b>
	<?php echo CHtml::encode($data->pic_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sku_id')); ?>:</b>
	<?php echo CHtml::encode($data->sku_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('props')); ?>:</b>
	<?php echo CHtml::encode($data->props); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('seller_nick')); ?>:</b>
	<?php echo CHtml::encode($data->seller_nick); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seller_id')); ?>:</b>
	<?php echo CHtml::encode($data->seller_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num')); ?>:</b>
	<?php echo CHtml::encode($data->num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weight')); ?>:</b>
	<?php echo CHtml::encode($data->weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('express_fee')); ?>:</b>
	<?php echo CHtml::encode($data->express_fee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('input_props')); ?>:</b>
	<?php echo CHtml::encode($data->input_props); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taobao_price')); ?>:</b>
	<?php echo CHtml::encode($data->taobao_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taobao_promotion_price')); ?>:</b>
	<?php echo CHtml::encode($data->taobao_promotion_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tid')); ?>:</b>
	<?php echo CHtml::encode($data->tid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('track_code')); ?>:</b>
	<?php echo CHtml::encode($data->track_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_num')); ?>:</b>
	<?php echo CHtml::encode($data->actual_num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_lot_weight')); ?>:</b>
	<?php echo CHtml::encode($data->actual_lot_weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_lot_express_fee')); ?>:</b>
	<?php echo CHtml::encode($data->actual_lot_express_fee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_lot_price')); ?>:</b>
	<?php echo CHtml::encode($data->actual_lot_price); ?>
	<br />

	*/ ?>

</div>