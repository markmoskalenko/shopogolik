
    <div id='orders-items-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Create orders-items</h3>
    </div>
    
    <div class="modal-body">
    
    <div class="form">

   <?php
   
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'orders-items-create-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("orders-items/create"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
          'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
                                    

            ),                  
  
)); ?>
     	<fieldset>
		<legend>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		</legend>

	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">
			
							  <div class="row">
					  <?php echo $form->labelEx($model,'oid'); ?>
					  <?php echo $form->textField($model,'oid'); ?>
					  <?php echo $form->error($model,'oid'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'iid'); ?>
					  <?php echo $form->textField($model,'iid',array('size'=>20,'maxlength'=>20)); ?>
					  <?php echo $form->error($model,'iid'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'pic_url'); ?>
					  <?php echo $form->textField($model,'pic_url',array('size'=>60,'maxlength'=>1024)); ?>
					  <?php echo $form->error($model,'pic_url'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'sku_id'); ?>
					  <?php echo $form->textField($model,'sku_id',array('size'=>60,'maxlength'=>256)); ?>
					  <?php echo $form->error($model,'sku_id'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'props'); ?>
					  <?php echo $form->textField($model,'props',array('size'=>60,'maxlength'=>4000)); ?>
					  <?php echo $form->error($model,'props'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'title'); ?>
					  <?php echo $form->textArea($model,'title',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'title'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'seller_nick'); ?>
					  <?php echo $form->textField($model,'seller_nick',array('size'=>60,'maxlength'=>1024)); ?>
					  <?php echo $form->error($model,'seller_nick'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'seller_id'); ?>
					  <?php echo $form->textField($model,'seller_id'); ?>
					  <?php echo $form->error($model,'seller_id'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'status'); ?>
					  <?php echo $form->textField($model,'status'); ?>
					  <?php echo $form->error($model,'status'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'num'); ?>
					  <?php echo $form->textField($model,'num'); ?>
					  <?php echo $form->error($model,'num'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'weight'); ?>
					  <?php echo $form->textField($model,'weight'); ?>
					  <?php echo $form->error($model,'weight'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'express_fee'); ?>
					  <?php echo $form->textField($model,'express_fee'); ?>
					  <?php echo $form->error($model,'express_fee'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'input_props'); ?>
					  <?php echo $form->textArea($model,'input_props',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'input_props'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'taobao_price'); ?>
					  <?php echo $form->textField($model,'taobao_price'); ?>
					  <?php echo $form->error($model,'taobao_price'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'taobao_promotion_price'); ?>
					  <?php echo $form->textField($model,'taobao_promotion_price'); ?>
					  <?php echo $form->error($model,'taobao_promotion_price'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'tid'); ?>
					  <?php echo $form->textField($model,'tid',array('size'=>60,'maxlength'=>4000)); ?>
					  <?php echo $form->error($model,'tid'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'track_code'); ?>
					  <?php echo $form->textField($model,'track_code',array('size'=>60,'maxlength'=>4000)); ?>
					  <?php echo $form->error($model,'track_code'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'actual_num'); ?>
					  <?php echo $form->textField($model,'actual_num'); ?>
					  <?php echo $form->error($model,'actual_num'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'actual_lot_weight'); ?>
					  <?php echo $form->textField($model,'actual_lot_weight'); ?>
					  <?php echo $form->error($model,'actual_lot_weight'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'actual_lot_express_fee'); ?>
					  <?php echo $form->textField($model,'actual_lot_express_fee'); ?>
					  <?php echo $form->error($model,'actual_lot_express_fee'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'actual_lot_price'); ?>
					  <?php echo $form->textField($model,'actual_lot_price'); ?>
					  <?php echo $form->error($model,'actual_lot_price'); ?>
				  </div>

			  
                        </div>   
  </div>

  </div><!--end modal body-->
  
  <div class="modal-footer">
	<div class="form-actions">

		<?php
		
		 $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
                        'icon'=>'ok white', 
			'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'create();'),
			)
			
		);
		
		?>
              <?php
 $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'reset',
                        'icon'=>'remove',  
			'label'=>Yii::t('admin','Сброс'),
		)); ?>
	</div> 
   </div><!--end modal footer-->	
</fieldset>

<?php
 $this->endWidget(); ?>

</div>

</div><!--end modal-->

<script type="text/javascript">
function create()
 {
 
   var data=$("#orders-items-create-form").serialize();
     


  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/orders-items/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#orders-items-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('orders-items-grid', {
                     
                         });
                   
                 }
                 
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },

  dataType:'html'
  });

}

function renderCreateForm()
{
  $('#orders-items-create-form').each (function(){
  this.reset();
   });

  
  $('#orders-items-view-modal').modal('hide');
  
  $('#orders-items-create-modal').modal({
   show:true
   
  });
}

</script>
