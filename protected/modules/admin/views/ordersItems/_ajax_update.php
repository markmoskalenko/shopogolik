<div id="orders-items-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#orders-items-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/orders-items/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#orders-items-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('orders-items-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#orders-items-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/orders-items/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#orders-items-update-modal-container').html(data); 
                 $('#orders-items-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
