<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="excelReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      oid		</th>
 		<th width="80px">
		      iid		</th>
 		<th width="80px">
		      pic_url		</th>
 		<th width="80px">
		      sku_id		</th>
 		<th width="80px">
		      props		</th>
 		<th width="80px">
		      title		</th>
 		<th width="80px">
		      seller_nick		</th>
 		<th width="80px">
		      seller_id		</th>
 		<th width="80px">
		      status		</th>
 		<th width="80px">
		      num		</th>
 		<th width="80px">
		      weight		</th>
 		<th width="80px">
		      express_fee		</th>
 		<th width="80px">
		      input_props		</th>
 		<th width="80px">
		      taobao_price		</th>
 		<th width="80px">
		      taobao_promotion_price		</th>
 		<th width="80px">
		      tid		</th>
 		<th width="80px">
		      track_code		</th>
 		<th width="80px">
		      actual_num		</th>
 		<th width="80px">
		      actual_lot_weight		</th>
 		<th width="80px">
		      actual_lot_express_fee		</th>
 		<th width="80px">
		      actual_lot_price		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->oid; ?>
		</td>
       		<td>
			<?php echo $row->iid; ?>
		</td>
       		<td>
			<?php echo $row->pic_url; ?>
		</td>
       		<td>
			<?php echo $row->sku_id; ?>
		</td>
       		<td>
			<?php echo $row->props; ?>
		</td>
       		<td>
			<?php echo $row->title; ?>
		</td>
       		<td>
			<?php echo $row->seller_nick; ?>
		</td>
       		<td>
			<?php echo $row->seller_id; ?>
		</td>
       		<td>
			<?php echo $row->status; ?>
		</td>
       		<td>
			<?php echo $row->num; ?>
		</td>
       		<td>
			<?php echo $row->weight; ?>
		</td>
       		<td>
			<?php echo $row->express_fee; ?>
		</td>
       		<td>
			<?php echo $row->input_props; ?>
		</td>
       		<td>
			<?php echo $row->taobao_price; ?>
		</td>
       		<td>
			<?php echo $row->taobao_promotion_price; ?>
		</td>
       		<td>
			<?php echo $row->tid; ?>
		</td>
       		<td>
			<?php echo $row->track_code; ?>
		</td>
       		<td>
			<?php echo $row->actual_num; ?>
		</td>
       		<td>
			<?php echo $row->actual_lot_weight; ?>
		</td>
       		<td>
			<?php echo $row->actual_lot_express_fee; ?>
		</td>
       		<td>
			<?php echo $row->actual_lot_price; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
