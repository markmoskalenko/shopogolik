<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<? $debug=false;
   $checkout_order_reconfirmation_needed=DSConfig::getVal('checkout_order_reconfirmation_needed')==1;
   $billing_use_operator_account=DSConfig::getVal('billing_use_operator_account')==1;?>
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
  'htmlOptions'=>array(
    'class'=>''
  )
));
$this->widget('bootstrap.widgets.TbMenu', array(
  'type'=>'pills',
  /* type	string		Menu type. Valid values are tabs, pills and list.
  scrollspy	string|array		Scrollspy target or configuration.
  stacked	boolean	false	Whether the menu should appear vertically stacked.
  dropup	boolean	false	Whether dropdown menus should drop up instead. */
  'items'=>array(
/*
   Товары
   Заказчик
   Оператор
   Экспорт
   XML
   Excel
   Печать
   PDF
   Накладная
   Удалить
   array("users/view", "id"=>$order->uid),
   array("title"=>Yii::t('admin',"Профиль клиента"),"onclick"=>"getContent(this,\"Пользователь №$order->uid\");return false;")
*/
    array('label'=>Yii::t('admin','Заказ'), 'icon'=>'icon-user', 'url'=>array("orders/view", "id"=>$order->id),
          'linkOptions'=>array('onclick'=>"getContent(this,\"".Yii::t('main','Заказ')." $order->uid-$order->id\");return false;"),),
    array('label'=>Yii::t('admin','Оператор'), 'icon'=>'icon-user-md', 'url'=>array("users/view", "id"=>$order->manager),
          'linkOptions'=>array('onclick'=>"getContent(this,\"".Yii::t('main','Оператор')." №$order->manager\");return false;"),),
    //array('label'=>Yii::t('admin','Удалить'), 'icon'=>'icon-remove', 'url'=>'#order-items'),
  ),
));
$this->endWidget();
?>
<!-- new ordr header -->
<div class="row-fluid">
<div class="span5">
<? // ////////////////////// Заказ ////////////////////////////////////////////////// // ?>
<div class="ordr-header-block" id="headerblockItem-id-<?=$order->id?>">
  <h3><i class="icon-shopping-cart"></i><?=Yii::t('main','Заказ')?></h3>
    <div class="form" style="margin: 3px 0;">
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
          'id'=>'order-update-form-id',
          'enableAjaxValidation'=>false,
          'enableClientValidation'=>false,
          'method'=>'post',
          'action'=>array("/admin/orders/update"),
          'type'=>'horizontal',
        )); ?>
            <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
            <div class="control-group">
                <?
                echo $form->hiddenField($order,'id',array());
                echo $form->uneditableRow($order, 'orderCode');
                echo $form->uneditableRow($order, 'textdate');
                echo $form->uneditableRow($order, 'orderAge');
                ?>
            </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<? // ////////////////////// Оператор /////////////////////////////////////////////// // ?>
<div class="ordr-header-block" id="headerblockItem-operator-<?=$order->id?>">
  <h3><i class="icon-user"></i><?=Yii::t('main','Заказчик и оператор')?></h3>
        <div class="form"  style="margin: 3px 0;">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'id'=>'order-update-form-operator',
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>false,
                'method'=>'post',
                'action'=>array("/admin/orders/update"),
                'type'=>'horizontal',
            )); ?>
                <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
                <div class="control-group">
                    <?  echo $form->hiddenField($order,'id',array());
                    echo $form->uneditableRow($order,'manager');
                    echo $form->uneditableRow($order,'managerBalance');
                    echo $form->uneditableRow($order,'userName');
                    echo CHtml::link("<span class=\"icon-user\" style=\"cursor: pointer;position: relative;top: -22px;left:90px;margin:0;\"></span>",
                        array("users/view", "id"=>$order->uid),
                        array("title"=>Yii::t('admin',"Профиль клиента"),"onclick"=>"getContent(this,\"Пользователь №$order->uid\");return false;"));
                        echo $form->uneditableRow($order,'userBalance');
                    ?>
                </div>
        </div>
        <?php $this->endWidget(); ?>

    </div>
</div>
<div class="span7">
    <? // ///////////////////// Статус /////////////////////////////////////////////////// // ?>
    <div class="ordr-header-block" id="headerblockItem-status-<?=$order->id?>">
        <h3><i class="icon-time"></i><?=Yii::t('main','Статус')?></h3>
        <div class="form" style="margin: 3px 0;">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'id'=>'order-update-form-status',
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>false,
                'method'=>'post',
                'action'=>array("/admin/orders/update"),
                'type'=>'horizontal',
              )); ?>
            <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
            <div class="control-group">
                <?  echo $form->hiddenField($order,'id',array());
                echo $form->uneditableRow($order->statuses,'name',array('size'=>60,'maxlength'=>128));
                echo $form->uneditableRow($order,'extstatusname');
                echo $form->checkboxRow($order, 'frozen',array('disabled'=>true));
                ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <? // //////////////////// Логистика ///////////////////////////////////////////////// // ?>
    <div class="ordr-header-block" id="headerblockItem-logistic-<?=$order->id?>">
        <h3><i class="icon-truck"></i><?=Yii::t('main','Логистика')?></h3>
        <div class="form"  style="margin: 3px 0;">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'id'=>'order-update-form-logistic',
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>false,
                'method'=>'post',
                'action'=>array("/admin/orders/update"),
                'type'=>'horizontal',
              )); ?>
            <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
            <div class="control-group">
                <!--            <div class="span4"> -->
                <?  echo $form->hiddenField($order,'id',array()); ?>

                <table border="1" cellspacing="1" cellpadding="1" style="width:95%;margin:10px 3%;">
                    <tr>
                        <th width="25%" scope="col">&nbsp;</th>
                        <th width="25%" scope="col"><?=Yii::t('admin','В заказе')?></th>
                        <th width="25%" scope="col"><?=Yii::t('admin','По факту')?></th>
                        <th width="25%" scope="col"><?=Yii::t('admin','По лотам')?></th>
                    </tr>
                    <tr>
                        <td scope="row"><?=Yii::t('admin','Вес заказа, грамм')?></td>
                        <td><?=$form->textField($order,'weight',array('size'=>8,'readonly'=>true))?></td>
                        <td><?=$form->textField($order,'manual_weight',array('size'=>8,'readonly'=>true))?>
                        <td><?=$form->textField($order->calculated,'actual_lots_weight',array('size'=>8,'readonly'=>true))?>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="table-select"><?=($order->manual_weight>0)?$order->weight-$order->manual_weight:0?></td>
                        <td class="table-select"><?=$order->weight-$order->calculated->actual_lots_weight?></td>
                    </tr>
                </table>
                <?
                echo $form->textAreaRow($order,'fullAddress',array('readonly'=>true,'style'=>'border-radius:0;width:90%;height: 70px;font-size:12px;resize:none;'));
                echo $form->dropDownListRow($order,'delivery_id',$order->allowedDeliveries,array('disabled'=>true));
                echo $form->textFieldRow($order,'store_id',array('readonly'=>true));
                echo $form->textFieldRow($order,'code',array('readonly'=>true));
                ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
</div>

<!-- Items block -->
<div class="ordr-header-block" id="headerblock-item-<?=$order->id?>" name="order-item-<?=$order->id?>">
  <div class="form">
  <fieldset>
<?php foreach($order->ordersItems as $item) {?>
  <? $this->widget('application.components.widgets.OrderItem', array(
    'orderItem' => $item,
    'readOnly' => $order->frozen,
    'allowDelete' => FALSE,
    'adminMode' =>TRUE,
    'publicComments'=>FALSE,
    'imageFormat' => '_200x200.jpg',
    'lazyLoad' => FALSE,
  ));
  ?>
     <? } ?>
</fieldset>
  </div>
</div>
<!-- end of Items block -->
<script>
  $(function() {
    $( "#headerblockItem-id-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: 0
      //disabled: true
    });
    $( "#headerblockItem-operator-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: false
      //disabled: true
    });
    $( "#headerblockItem-status-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: 0
      //disabled: true
    });
    $( "#headerblockItem-logistic-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: false
      //disabled: true
    });
  });
//  ordr-header-block
</script>
