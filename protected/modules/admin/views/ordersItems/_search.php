<?php  $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'search-orders-items-form',
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
));  ?>


	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'oid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'iid',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'pic_url',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textFieldRow($model,'sku_id',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'props',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textAreaRow($model,'title',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'seller_nick',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textFieldRow($model,'seller_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'num',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'weight',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'express_fee',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'input_props',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'taobao_price',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'taobao_promotion_price',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tid',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'track_code',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'actual_num',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'actual_lot_weight',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'actual_lot_express_fee',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'actual_lot_price',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'search white', 'label'=>Yii::t('admin','Поиск'))); ?>
               <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'icon'=>'icon-remove-sign white', 'label'=>Yii::t('admin','Сброс'), 'htmlOptions'=>array('class'=>'btnreset btn-small'))); ?>
	</div>

<?php $this->endWidget(); ?>


<?php /* Alexys $cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile(Yii::app()->request->baseUrl.'/themes/admin/css/jquery-ui.css');
*/
?>	
   <script>
	$(".btnreset").click(function(){
		$(":input","#search-orders-items-form").each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		if (type == "text" || type == "password" || tag == "textarea") this.value = "";
		else if (type == "checkbox" || type == "radio") this.checked = false;
		else if (tag == "select") this.selectedIndex = "";
	  });
	});
   </script>

