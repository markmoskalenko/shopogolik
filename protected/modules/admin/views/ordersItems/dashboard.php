<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="dashboard.php">
* </description>
**********************************************************************************************************************/?>
<h3><?=Yii::t('admin','Лоты по статусам')?>:</h3>
<?
$this->widget('bootstrap.widgets.TbGridView', array(
  'id'=>'ordersItemsByStatuses-grid',
  'dataProvider'=>$ordersItemsByStatusesDataProvider,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{items}{pager}',
  'columns'=>array(
    array(
      'type'=>'raw',
      'value'=>'"
		      <a href=\'#dashboard".$data[\'id\']."\' class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
		      <a href=\'/admin/ordersItems/index/type/".$data[\'id\']."\' onclick=\'getContent(this,\"".Yii::t(\'admin\',\'Лоты\').\': \'.Yii::t(\'admin\', $data[\'name\'])."\"); return false;\' class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
      'htmlOptions'=>array('style'=>'width:80px;')
    ),
    array('header'=>Yii::t('admin','Статус лота'),
      'name'=>'name',
      'type'=>'raw',
      'value'=>'"
        <a href=\'/admin/ordersItems/index/type/".$data[\'id\']."\' onclick=\'getContent(this,\"".Yii::t(\'admin\',\'Лоты\').\': \'.Yii::t(\'admin\', Yii::t(\'admin\',$data[\'name\']))."\"); return false;\'>".Yii::t(\'admin\',$data[\'name\'])."</a>
    "',
    ),
    array('header'=>Yii::t('admin','Лотов'),
      'name'=>'count'),
    array('header'=>Yii::t('admin','На сумму'),
      'name'=>'totalsum'),
    array('header'=>Yii::t('admin','Изменено'),
      'type'=>'raw',
      'value'=>'($data["lastdate"]) ? date("d.m.Y H:i",$data["lastdate"]) : ""'),
    array('header'=>Yii::t('admin','Описание'),
      'name'=>'desc'),
  ),
));
?>
<?foreach ($ordersItemsByStatusesArray as $ordersItemsByStatuses) {
//    continue;
//  if ($ordersByStatuses['count']>0) {
$this->widget('application.modules.admin.components.widgets.OrdersItemsListBlock', array(
  'type' => $ordersItemsByStatuses['id'],
  'name' => $ordersItemsByStatuses['name'],
  'idPrefix' =>'dashboard',
  'pageSize' =>25,
));
//  }
}
?>