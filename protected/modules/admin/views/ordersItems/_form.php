<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'orders-items-form',
	'enableAjaxValidation'=>false,
        'method'=>'post',
	'type'=>'horizontal',
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data'
	)
)); ?>
     	<fieldset>
		<legend>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		</legend>

	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">

	<?php echo $form->textFieldRow($model,'oid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'iid',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'pic_url',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textFieldRow($model,'sku_id',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'props',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textAreaRow($model,'title',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'seller_nick',array('class'=>'span5','maxlength'=>1024)); ?>

	<?php echo $form->textFieldRow($model,'seller_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'num',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'weight',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'express_fee',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'input_props',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'taobao_price',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'taobao_promotion_price',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tid',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'track_code',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'actual_num',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'actual_lot_weight',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'actual_lot_express_fee',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'actual_lot_price',array('class'=>'span5')); ?>

                        </div>   
  </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
                        'icon'=>'ok white',  
			'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
		)); ?>
              <?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'reset',
                        'icon'=>'remove',  
			'label'=>Yii::t('admin','Сброс'),
		)); ?>
	</div>
</fieldset>

<?php $this->endWidget(); ?>

</div>
