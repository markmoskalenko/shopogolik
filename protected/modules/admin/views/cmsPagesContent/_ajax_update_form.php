    <div id='cms-pages-content-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Изменение контента страницы')?> <strong style="color:#0093f5;">#<?php echo $model->page_id; ?></strong></h3>
    </div>
    <div class="modal-body">
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    	'id'=>'cms-pages-content-update-form',
    	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("cmsPagesContent/update"),
    	'type'=>'horizontal',
    	'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
)); ?>
        <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
        <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
		<?php echo $form->hiddenField($model,'id',array()); ?>
        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model,'page_id'); ?>
                <?php echo $form->textField($model,'page_id',array('class'=>'span12','maxlength'=>255)); ?>
                <?php echo $form->error($model,'page_id'); ?>

                <?php echo $form->labelEx($model,'content_data'); ?>
                <?php echo $form->textArea($model,'content_data',array('rows'=>10, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'content_data'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'lang'); ?>
                <?php echo $form->textField($model,'lang',array('class'=>'span12','maxlength'=>8)); ?>
                <?php echo $form->error($model,'lang'); ?>

                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textArea($model,'description',array('rows'=>10,'class'=>'span12')); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'title'); ?>
                <?php echo $form->textField($model,'title',array('class'=>'span12','maxlength'=>1024)); ?>
                <?php echo $form->error($model,'title'); ?>

                <?php echo $form->labelEx($model,'keywords'); ?>
                <?php echo $form->textArea($model,'keywords',array('rows'=>10,'class'=>'span12')); ?>
                <?php echo $form->error($model,'keywords'); ?>
            </div>
        </div>

  </div><!--end modal body-->

</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                //'id'=>'sub2',
                'type'=>'info',
                'icon'=>'ok white',
                'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
                'htmlOptions'=>array('onclick'=>'update();'),
            ));
            ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->