<div class="row-fluid">
    <div class="span10" style="border:solid 1px #c9e0ed;">
        <div class="form" style="margin-bottom:0 !important;">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'id'=>'cms-pages-content-form-'.$model->id,
                'enableAjaxValidation'=>false,
                'method'=>'post',
                'type'=>'horizontal',
                'htmlOptions'=>array(
                    'enctype'=>'multipart/form-data'
                )
            )); ?>

            <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

                    <?php echo $form->textFieldRow($model,'page_id',array('class'=>'span10','maxlength'=>255)); ?>
                    <?php echo $form->dropDownListRow($model,'lang',cms::getLangList(),array('class'=>'span1','maxlength'=>8)); ?>
                    <?php echo $form->textfIELDRow($model,'title',array('class'=>'span10','maxlength'=>1024)); ?>
                    <?php echo $form->textaREARow($model,'description',array('class'=>'span10','rows'=>'6','maxlength'=>1024)); ?>
                    <?php echo $form->textaREARow($model,'keywords',array('class'=>'span10','rows'=>'6','maxlength'=>1024)); ?>

                    <div class="row" style="margin:10px;">
                        <?php echo $form->labelEx($model,'content_data'); ?>
                        <?php
                        $editor=new SRichTextarea();
                        $editor->init();
                        $editor->model=$model;
                        $editor->attribute='content_data';
                        $editor->htmlOptions=array('rows'=>20, 'cols'=>280);
                        $editor->run(true);
                        ?>
                        <br/>
                        <?php echo $form->error($model,'content_data'); ?>
                    </div>
                    <?// echo $form->textAreaRow($model,'content_data',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

<div class="modal-footer" style="margin:0 !important;">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'info',
                'icon'=>'ok white',
                'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
                'htmlOptions'=>array('onclick'=>"saveForm('cms-pages-content-form-".$model->id."'); return false;",),
            )); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
</div>
<?php $this->endWidget(); ?>

        </div>
    </div>
    <div class="span2">
        <div id="cms-pages-content-history-<?=$model->id?>">
            <?  $this->widget('application.modules.admin.components.widgets.CmsHistoryBlock', array(
                'id' =>'cms_pages_content',
                'tableName' => 'cms_pages_content',
                'contentId' => $model->page_id,
                'contentLang' =>$model->lang,
                'pageSize' =>50,
            ));
            ?>
        </div>
    </div>
</div>
<br/>
<div id="cms-pages-content-filemanager-<?=$model->id?>"></div>
<script type="text/javascript" charset="utf-8">
    // Documentation for client options:
    // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
    $(document).ready(function() {
        $('#cms-pages-content-filemanager-<?=$model->id?>').elfinder({
            url : '/admin/fileman/index'  // connector URL (REQUIRED)
            // , lang: 'ru'                    // language (OPTIONAL)
            ,resizable: false
            ,height: '600px'
            ,width: '83%'
        });
    });
</script>
