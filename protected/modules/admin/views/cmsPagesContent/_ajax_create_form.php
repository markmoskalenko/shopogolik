
    <div id='cms-pages-content-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новый контент страницы')?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
   <?php
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        	'id'=>'cms-pages-content-create-form',
	        'enableAjaxValidation'=>false,
            'enableClientValidation'=>false,
            'method'=>'post',
            'action'=>array("cmsPagesContent/create"),
	        'type'=>'horizontal',
	        'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
                    'clientOptions'=>array(
                        'validateOnType'=>true,
                        'validateOnSubmit'=>true,
                        'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model,'page_id'); ?>
                <?php echo $form->textField($model,'page_id',array('class'=>'span12','maxlength'=>255)); ?>
                <?php echo $form->error($model,'page_id'); ?>

                <?php echo $form->labelEx($model,'content_data'); ?>
                <?php echo $form->textArea($model,'content_data',array('rows'=>6, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'content_data'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'lang'); ?>
                <?php echo $form->textField($model,'lang',array('class'=>'span12','maxlength'=>8)); ?>
                <?php echo $form->error($model,'lang'); ?>

                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textArea($model,'description',array('class'=>'span12','rows'=>6)); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'title'); ?>
                <?php echo $form->textField($model,'title',array('class'=>'span12','maxlength'=>1024)); ?>
                <?php echo $form->error($model,'title'); ?>

                <?php echo $form->labelEx($model,'keywords'); ?>
                <?php echo $form->textArea($model,'keywords',array('class'=>'span12','rows'=>6)); ?>
                <?php echo $form->error($model,'keywords'); ?>
            </div>
        </div>

  </div><!--end modal body-->

</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

    <!--Script sevtion-->
<script type="text/javascript">
function create()
 {
   var data=$("#cms-pages-content-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/cmsPagesContent/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#cms-pages-content-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('cms-pages-content-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#cms-pages-content-create-form').each (function(){
  this.reset();
   });
  $('#cms-pages-content-view-modal').modal('hide');
  $('#cms-pages-content-create-modal').modal({
   show:true
  });
}
</script>
