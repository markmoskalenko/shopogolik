 <div id='cms-pages-content-view-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-body" style="height:700px;">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	  <div id="cms-pages-content-view-modal-container"></div>
    </div>

 </div><!--end modal-->

<script>
function renderView(id)
{
 var data="id="+id;
  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsPagesContent/view"); ?>',
   data:data,
success:function(data){
                 $('#cms-pages-content-view-modal-container').html(data); 
                 $('#cms-pages-content-view-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },
  dataType:'html'
  });
}
</script>