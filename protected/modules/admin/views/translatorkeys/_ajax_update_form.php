<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update_form.php">
* </description>
**********************************************************************************************************************/?>
<div id='translatorkeys-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Изменение ключа bing')?> <strong style="color:#0093f5;">#<?php echo $model->id; ?></strong></h3>
    </div>
    <div class="modal-body">
    <div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	    'id'=>'translatorkeys-update-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("translatorkeys/update"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

        <?php echo $form->hiddenField($model,'id',array()); ?>
        <?php echo $form->labelEx($model,'key'); ?>
        <?php echo $form->textField($model,'key',array('class'=>'span8','maxlength'=>512)); ?>
        <?php echo $form->error($model,'key'); ?>

        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->labelEx($model,'type'); ?>
                <?php echo $form->textField($model,'type',array('class'=>'span12','maxlength'=>512)); ?>
                <?php echo $form->error($model,'type'); ?>

                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled',array('class'=>'span12')); ?>
                <?php echo $form->error($model,'enabled'); ?>

                <?php echo $form->labelEx($model,'banned'); ?>
                <?php echo $form->textField($model,'banned',array('class'=>'span12')); ?>
                <?php echo $form->error($model,'banned'); ?>

                <?php echo $form->labelEx($model,'banned_date'); ?>
                <?php echo $form->textField($model,'banned_date',array('class'=>'span12')); ?>
                <?php echo $form->error($model,'banned_date'); ?>
            </div>
            <div class="span6">
                <?php echo $form->labelEx($model,'descr'); ?>
                <?php echo $form->textArea($model,'descr',array('rows'=>8, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'descr'); ?>
            </div>
        </div>
  </div><!--end modal body-->

</div>
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            //'id'=>'sub2',
            'type'=>'info',
            'icon'=>'ok white',
            'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'update();'),
        ));
        ?>
    </div><!--end modal footer-->
    <?php $this->endWidget(); ?>
</div><!--end modal-->



