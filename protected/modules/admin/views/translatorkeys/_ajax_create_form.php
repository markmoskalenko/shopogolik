<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_create_form.php">
* </description>
**********************************************************************************************************************/?>
    <div id='translatorkeys-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новый ключ bing')?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
   <?php
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'translatorkeys-create-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("translatorkeys/create"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
          'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>

    	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        <?php echo $form->labelEx($model,'key'); ?>
        <?php echo $form->textField($model,'key',array('class'=>'span10','maxlength'=>512)); ?>
        <?php echo $form->error($model,'key'); ?>

        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model,'type'); ?>
                <?php echo $form->textField($model,'type',array('size'=>60,'maxlength'=>512)); ?>
                <?php echo $form->error($model,'type'); ?>

                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled'); ?>
                <?php echo $form->error($model,'enabled'); ?>

                <?php echo $form->labelEx($model,'banned'); ?>
                <?php echo $form->textField($model,'banned'); ?>
                <?php echo $form->error($model,'banned'); ?>

                <?php echo $form->labelEx($model,'banned_date'); ?>
                <?php echo $form->textField($model,'banned_date'); ?>
                <?php echo $form->error($model,'banned_date'); ?>
            </div>
            <div class="span8">
                <?php echo $form->labelEx($model,'descr'); ?>
                <?php echo $form->textArea($model,'descr',array('rows'=>9, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'descr'); ?>
            </div>
        </div>

  </div>
  
</div><!--end modal body-->
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

<!--Script Section-->
<script type="text/javascript">
function create()
 {
   var data=$("#translatorkeys-create-form").serialize();
   jQuery.ajax({
   type: 'POST',
   url: '<?php
   echo Yii::app()->createAbsoluteUrl("admin/translatorkeys/create"); ?>',
   data:data,
   success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#translatorkeys-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('translatorkeys-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#translatorkeys-create-form').each (function(){
  this.reset();
   });
  $('#translatorkeys-view-modal').modal('hide');
  $('#translatorkeys-create-modal').modal({
   show:true
  });
}
</script>
