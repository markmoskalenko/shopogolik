<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="excelReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      key		</th>
 		<th width="80px">
		      type		</th>
 		<th width="80px">
		      enabled		</th>
 		<th width="80px">
		      banned		</th>
 		<th width="80px">
		      banned_date		</th>
 		<th width="80px">
		      descr		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->key; ?>
		</td>
       		<td>
			<?php echo $row->type; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       		<td>
			<?php echo $row->banned; ?>
		</td>
       		<td>
			<?php echo $row->banned_date; ?>
		</td>
       		<td>
			<?php echo $row->descr; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
