<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update_form.php">
* </description>
**********************************************************************************************************************/?>
<div id='brands-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Изменить бренд')?> <strong style="color:#0093f5;">#<?php echo $model->id; ?></strong></h3>
    </div>
    <div class="modal-body">
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    	'id'=>'brands-update-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("brands/update"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
    <?php echo $form->hiddenField($model,'id',array()); ?>
        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'name'); ?>

                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled'); ?>
                <?php echo $form->error($model,'enabled'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'img_src'); ?>
                <?php echo $form->textField($model,'img_src',array('size'=>60,'maxlength'=>512)); ?>
                <?php echo $form->error($model,'img_src'); ?>

                <?php echo $form->labelEx($model,'vid'); ?>
                <?php echo $form->textField($model,'vid',array('size'=>20,'maxlength'=>20)); ?>
                <?php echo $form->error($model,'vid'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'query'); ?>
                <?php echo $form->textField($model,'query',array('size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'query'); ?>

                <?php echo $form->labelEx($model,'url'); ?>
                <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'url'); ?>
            </div>
			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'meta_desc_ru'); ?>
					  <?php echo $form->textField($model,'meta_desc_ru',array('size'=>60,'maxlength'=>512)); ?>
					  <?php echo $form->error($model,'meta_desc_ru'); ?>
				  </div>

			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'meta_keeword_ru'); ?>
					  <?php echo $form->textField($model,'meta_keeword_ru',array('size'=>60,'maxlength'=>512)); ?>
					  <?php echo $form->error($model,'meta_keeword_ru'); ?>
				  </div>

			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'page_title_ru'); ?>
					  <?php echo $form->textField($model,'page_title_ru',array('size'=>60,'maxlength'=>512)); ?>
					  <?php echo $form->error($model,'page_title_ru'); ?>
				  </div>

			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'meta_desc_en'); ?>
					  <?php echo $form->textField($model,'meta_desc_en',array('size'=>60,'maxlength'=>512)); ?>
					  <?php echo $form->error($model,'meta_desc_en'); ?>
				  </div>

			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'meta_keeword_en'); ?>
					  <?php echo $form->textField($model,'meta_keeword_en',array('size'=>60,'maxlength'=>512)); ?>
					  <?php echo $form->error($model,'meta_keeword_en'); ?>
				  </div>

			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'page_title_en'); ?>
					  <?php echo $form->textField($model,'page_title_en',array('size'=>60,'maxlength'=>512)); ?>
					  <?php echo $form->error($model,'page_title_en'); ?>
				  </div>

			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'page_desc_ru'); ?>
					  <?php echo $form->textArea($model,'page_desc_ru',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'page_desc_ru'); ?>
				  </div>

			  				  <div class="span4">
					  <?php echo $form->labelEx($model,'page_desc_en'); ?>
					  <?php echo $form->textArea($model,'page_desc_en',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'page_desc_en'); ?>
				  </div>
        </div>
  </div><!--end modal body-->

</div>
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            //'id'=>'sub2',
            'type'=>'info',
            'icon'=>'ok white',
            'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'update();'),
        ));
        ?>
    </div><!--end modal footer-->
    <?php $this->endWidget(); ?>
</div><!--end modal-->



