<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_form.php">
* </description>
**********************************************************************************************************************/?>
<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'brands-form',
	'enableAjaxValidation'=>false,
        'method'=>'post',
	'type'=>'horizontal',
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data'
	)
)); ?>
     	<fieldset>
		<legend>
			<p class="note"><?=Yii::t('admin','Поля')?> <span class="required">*</span> <?=Yii::t('admin','обязательны')?>.</p>
		</legend>

	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'enabled',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'img_src',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'vid',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'query',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'meta_desc_ru',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'meta_keeword_ru',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'page_title_ru',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'meta_desc_en',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'meta_keeword_en',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'page_title_en',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textAreaRow($model,'page_desc_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'page_desc_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

                        </div>   
  </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
                        'icon'=>'ok white',  
			'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
		)); ?>
              <?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'reset',
                        'icon'=>'remove',  
			'label'=>Yii::t('admin','Сброс'),
		)); ?>
	</div>
</fieldset>

<?php $this->endWidget(); ?>

</div>
