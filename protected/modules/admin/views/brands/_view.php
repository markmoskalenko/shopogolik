<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_view.php">
* </description>
**********************************************************************************************************************/?>
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
	<?php echo CHtml::encode($data->enabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('img_src')); ?>:</b>
	<?php echo CHtml::encode($data->img_src); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vid')); ?>:</b>
	<?php echo CHtml::encode($data->vid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('query')); ?>:</b>
	<?php echo CHtml::encode($data->query); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_desc_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_desc_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keeword_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keeword_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->page_title_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_desc_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_desc_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keeword_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keeword_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_title_en')); ?>:</b>
	<?php echo CHtml::encode($data->page_title_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_desc_ru')); ?>:</b>
	<?php echo CHtml::encode($data->page_desc_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_desc_en')); ?>:</b>
	<?php echo CHtml::encode($data->page_desc_en); ?>
	<br />

	*/ ?>

</div>