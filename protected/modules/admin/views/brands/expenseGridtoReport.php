<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="expenseGridtoReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      name		</th>
 		<th width="80px">
		      enabled		</th>
 		<th width="80px">
		      img_src		</th>
 		<th width="80px">
		      vid		</th>
 		<th width="80px">
		      query		</th>
 		<th width="80px">
		      url		</th>
 		<th width="80px">
		      meta_desc_ru		</th>
 		<th width="80px">
		      meta_keeword_ru		</th>
 		<th width="80px">
		      page_title_ru		</th>
 		<th width="80px">
		      meta_desc_en		</th>
 		<th width="80px">
		      meta_keeword_en		</th>
 		<th width="80px">
		      page_title_en		</th>
 		<th width="80px">
		      page_desc_ru		</th>
 		<th width="80px">
		      page_desc_en		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->name; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       		<td>
			<?php echo $row->img_src; ?>
		</td>
       		<td>
			<?php echo $row->vid; ?>
		</td>
       		<td>
			<?php echo $row->query; ?>
		</td>
       		<td>
			<?php echo $row->url; ?>
		</td>
       		<td>
			<?php echo $row->meta_desc_ru; ?>
		</td>
       		<td>
			<?php echo $row->meta_keeword_ru; ?>
		</td>
       		<td>
			<?php echo $row->page_title_ru; ?>
		</td>
       		<td>
			<?php echo $row->meta_desc_en; ?>
		</td>
       		<td>
			<?php echo $row->meta_keeword_en; ?>
		</td>
       		<td>
			<?php echo $row->page_title_en; ?>
		</td>
       		<td>
			<?php echo $row->page_desc_ru; ?>
		</td>
       		<td>
			<?php echo $row->page_desc_en; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
