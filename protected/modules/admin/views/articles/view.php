<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->title,
);

$this->menu=array(	
	array('label'=>Yii::t('admin','Добавить страницу'), 'url'=>array('create')),
	array('label'=>Yii::t('admin','Изменить эту страницу'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('admin','Удалить эту страницу'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('admin','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('admin','Управление страницами'), 'url'=>array('admin')),
);
?>

<h1><?=Yii::t('admin','Просмотр страницы ')?>"<?php echo $model->title; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'meta_desc',
		'meta_keeword',
		'text',
		'lang',
		'url',
	),
)); ?>
