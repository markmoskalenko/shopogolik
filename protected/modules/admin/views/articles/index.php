<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Страницы'),
);

$this->menu=array(
	array('label'=>Yii::t('admin','Добавить страницу'), 'url'=>array('create')),
	array('label'=>Yii::t('admin','Управление страницами'), 'url'=>array('admin')),
);
?>

<h1><?=Yii::t('admin','Статичные страницы')?></h1>
<div style="float: left; width: 100%;">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'id'=>'articles-listview',
    'ajaxUpdate'=>true,
)); ?>
</div>
