<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_search.php">
* </description>
**********************************************************************************************************************/?><div class="search form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>"search-articles-form"
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lang'); ?>
		<?php echo $form->dropDownList($model,'lang',array(
                    'ru'=>Yii::t('admin','Русский'),
                        'en'=>Yii::t('admin','Английский')
                )); ?>
	</div>

	<div class="row buttons">
      <?    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        //'id'=>'sub2',
        'type'=>'primary',
        'icon'=>'ok white',
        'label'=>Yii::t('admin','Поиск'),
        'htmlOptions'=>array('onclick'=>'searchArticles(this); return false;'),
      ));
      ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->