<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="admin.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	Yii::t('admin','Страницы')=>array('index'),
  Yii::t('admin','Управление'),
);


?>

<h1><?=Yii::t('admin','Управление страницами')?></h1>

<div >
<a href="articles/create" onclick="getContent(this,'<?=Yii::t('admin','Новая страница')?>'); return false;"><?=Yii::t('admin','Добавить страницу')?></a>
<?php
$this->renderPartial('_search',array(
	'model'=>$model,
));
?>
</div>

<div style="float: left; width: 100%;">

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'articles-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{items}{pager}',
	'columns'=>array(
	  array('name'=>'id',
     'type'=>'raw', //$model->id
            'value'=>'CHtml::link($data->id, array("articles/update", "id"=>$data->id), array("onclick"=>"getContent(this,\"'.Yii::t('main','Страница').' №$data->id\");return false;"))',
//            'value'=>'CHtml::link("редактировать", array("articles/update", "id"=>$data->id), array("onclick"=>"getContent(this,\"Страница №$data->id\");return false;"))',
        ),
      array('name'=>'title',
        'value'=>'$data->title." (".$data->url.")"',
        ),
      array('name'=>'url',
        'type'=>'raw',
        'value'=>'CHtml::link($data->url, array("/article/index", "url"=>$data->url), array("target"=>"_blank"))',
      ),
      array('name'=>'lang',
        ),
		/*
		'url',
		*/
/*		array(
            'type'=>'raw',
            'value'=>'CHtml::link("редактировать", array("articles/update", "id"=>$data->id), array("onclick"=>"getContent(this,\"Страница №$data->id\");return false;"))',
        ),
      */
	),
//  'hasFooter'=>true,
)); ?>
<div>
