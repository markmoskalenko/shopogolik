<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_form.php">
* </description>
**********************************************************************************************************************/?>
<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>"article-form-".$model->id,
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_desc'); ?>
		<?php echo $form->textArea($model,'meta_desc',array('rows'=>3,'cols'=>'80','maxlength'=>256)); ?>
		<?php echo $form->error($model,'meta_desc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_keeword'); ?>
		<?php echo $form->textArea($model,'meta_keeword',array('rows'=>3,'cols'=>'80','maxlength'=>256)); ?>
		<?php echo $form->error($model,'meta_keeword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
     <?php
     $editor=new SRichTextarea();
     $editor->init();
     $editor->model=$model;
     $editor->attribute='text';
     $editor->htmlOptions=array('rows'=>8, 'cols'=>80);
     $editor->run();
      ?>
    <br/>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang'); ?>
		<?php echo $form->dropDownList($model,'lang',array(
                        'ru'=>Yii::t('admin','Русский'),
                        'en'=>Yii::t('admin','Английский')
                )); ?>
		<?php echo $form->error($model,'lang'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row buttons">
		<input type="button" onclick="saveArticle(<?=$model->id?>); return false;" value="<?=Yii::t('admin','Сохранить')?>">
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->