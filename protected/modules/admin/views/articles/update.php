<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="update.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Страница')=>array('index'),
	$model->title=>array('view','id'=>$model->id),
  Yii::t('admin','Изменение'),
);

$this->menu=array(	
	array('label'=>Yii::t('admin','Добавить страницу'), 'url'=>array('create')),
	array('label'=>Yii::t('admin','Посмотреть эту страницу'), 'url'=>array('/article/index', 'url'=>$model->url)),
	array('label'=>Yii::t('admin','Управление страницами'), 'url'=>array('admin')),
);
?>

<h1><?=Yii::t('admin','Изменение страницы ')?>"<?php echo $model->title; ?>"</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model),false,true); ?>