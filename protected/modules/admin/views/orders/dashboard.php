<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="dashboard.php">
* </description>
**********************************************************************************************************************/?>
<h3><?=Yii::t('admin','Заказы по статусам')?>:</h3>
<?
$this->widget('bootstrap.widgets.TbGridView', array(
  'id'=>'ordersByStatuses-grid',
  'dataProvider'=>$ordersByStatusesDataProvider,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{items}{pager}',
  'columns'=>array(
    array(
      'type'=>'raw',
      'value'=>'"
		      <a href=\'#dashboard".$data[\'value\']."\' class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
		      <a href=\'/admin/orders/index/type/".$data[\'value\']."\' onclick=\'getContent(this,\"".Yii::t(\'admin\',\'Заказы\').\': \'.Yii::t(\'admin\', $data[\'name\'])."\"); return false;\' class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
      'htmlOptions'=>array('style'=>'width:80px;')
    ),
    array('header'=>Yii::t('admin','Статус заказа'),
      'name'=>'name',
      'type'=>'raw',
      'value'=> function ($data) {
            echo '<a href="/admin/orders/index/type/'.$data['value'].
              '" onclick="getContent(this,\''.Yii::t('admin','Заказы').': '.Yii::t('admin', Yii::t('admin',$data['name'])).'\'); return false;">'
              .(($data['manual']==1)?'<b>':'').Yii::t('admin',$data['name']).(($data['manual']==1)?'</b>':'').'</a>';
      },
    ),
    array('header'=>Yii::t('admin','Заказов'),
      'name'=>'count'),
    array('header'=>Yii::t('admin','На сумму'),
      'name'=>'totalsum'),
    array('header'=>Yii::t('admin','Оплачено'),
      'name'=>'totalpayed'),
    array('header'=>Yii::t('admin','Не оплачено'),
      'name'=>'totalnopayed'),
    array('header'=>Yii::t('admin','Изменено'),
      'type'=>'raw',
      'value'=>'($data["lastdate"]) ? date("d.m.Y H:i",$data["lastdate"]) : ""'),
    array('header'=>Yii::t('admin','Описание'),
      'name'=>'descr'),
  ),
));
?>
<?foreach ($ordersByStatusesArray as $ordersByStatuses) {
//  if ($ordersByStatuses['count']>0) {
$this->widget('application.modules.admin.components.widgets.OrdersListBlock', array(
  'type' => $ordersByStatuses['value'],
  'name' => $ordersByStatuses['name'],
  'idPrefix' =>'dashboard',
  'pageSize' =>10,
));
//  }
}
?>