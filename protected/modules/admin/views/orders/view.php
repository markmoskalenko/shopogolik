<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<? $debug=false;
   $checkout_order_reconfirmation_needed=DSConfig::getVal('checkout_order_reconfirmation_needed')==1;
   $billing_use_operator_account=DSConfig::getVal('billing_use_operator_account')==1;?>
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
  'htmlOptions'=>array(
    'class'=>''
  )
));
$this->widget('bootstrap.widgets.TbMenu', array(
  'type'=>'pills',
  /* type	string		Menu type. Valid values are tabs, pills and list.
  scrollspy	string|array		Scrollspy target or configuration.
  stacked	boolean	false	Whether the menu should appear vertically stacked.
  dropup	boolean	false	Whether dropdown menus should drop up instead. */
  'items'=>array(
/*
   Товары
   Заказчик
   Оператор
   Экспорт
   XML
   Excel
   Печать
   PDF
   Накладная
   Удалить
   array("users/view", "id"=>$order->uid),
   array("title"=>Yii::t('admin',"Профиль клиента"),"onclick"=>"getContent(this,\"Пользователь №$order->uid\");return false;")
*/
    array('label'=>Yii::t('admin','Товары'), 'icon'=>'icon-tasks', 'url'=>'#order-items-'.$order->id),
    array('label'=>Yii::t('admin','Заказчик'), 'icon'=>'icon-user', 'url'=>array("users/view", "id"=>$order->uid),
          'linkOptions'=>array('onclick'=>"getContent(this,\"".Yii::t('main','Заказчик')." №$order->uid\");return false;"),),
    array('label'=>Yii::t('admin','Оператор'), 'icon'=>'icon-user-md', 'url'=>array("users/view", "id"=>$order->manager),
          'linkOptions'=>array('onclick'=>"getContent(this,\"".Yii::t('main','Оператор')." №$order->manager\");return false;"),),
    array('label'=>Yii::t('admin','Экспорт'), 'icon'=>'icon-download', 'url'=>'javascript:void(0);',
      'items'=>array(
        array('label'=>Yii::t('admin','XML'), 'icon'=>'icon-sitemap', 'url'=>Yii::app()->controller->createUrl('GenerateXML',array('id'=>$order->id)), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
        array('label'=>Yii::t('admin','Excel'), 'icon'=>'icon-calendar', 'url'=>Yii::app()->controller->createUrl('GenerateExcel',array('id'=>$order->id)), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
      ),
    ),
    array('label'=>Yii::t('admin','Печать'), 'icon'=>'icon-print', 'url'=>'javascript:void(0);',
          'items'=>array(
//            array('label'=>Yii::t('admin','PDF'), 'icon'=>'icon-book', 'url'=>Yii::app()->controller->createUrl('GeneratePdf'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
            array('label'=>Yii::t('admin','Накладная'), 'icon'=>'icon-barcode', 'url'=>Yii::app()->controller->createUrl('GenerateInvoice',array('id'=>$order->id)), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
          ),
    ),
    //array('label'=>Yii::t('admin','Удалить'), 'icon'=>'icon-remove', 'url'=>'#order-items'),
  ),
));
$this->endWidget();
?>
<!-- new ordr header -->
<div class="row-fluid">
<div class="span5">
<? // ////////////////////// Заказ ////////////////////////////////////////////////// // ?>
<div class="ordr-header-block" id="headerblock-id-<?=$order->id?>">
  <h3><i class="icon-shopping-cart"></i><?=Yii::t('main','Заказ')?></h3>
    <div class="form" style="margin: 3px 0;">
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
          'id'=>'order-update-form-id-'.$order->id,
          'enableAjaxValidation'=>false,
          'enableClientValidation'=>false,
          'method'=>'post',
          'action'=>array("/admin/orders/update"),
          'type'=>'horizontal',
        )); ?>
            <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
            <div class="control-group">
                <?
                echo $form->hiddenField($order,'id',array());
                echo $form->uneditableRow($order, 'orderCode');
                echo $form->uneditableRow($order, 'textdate');
                echo $form->uneditableRow($order, 'orderAge');
                ?>
                <? if (count($order->ordersItemsLegend)>0) { ?>
                <div class="control-group">
                    <label class="control-label"><?=Yii::t('admin','Статусы лотов')?></label>
                    <div class="controls">
                    <? foreach ($order->ordersItemsLegend as $itemStatus) {?>
                            <span style="<?=$itemStatus['excluded']?'color:red;':''?>"><?=Yii::t('main',$itemStatus['name'])?></span>: <?=$itemStatus['cnt']?>&nbsp;
                    <? } ?>
                    </div>
                </div>
                <? } ?>
                <? if ($order->ordersItemsFromOneSeller>0) { ?>
                    <div class="control-group">
                        <label class="control-label"><?=Yii::t('admin','Товаров у одного продавца')?></label>
                        <div class="controls">
                                <?=$order->ordersItemsFromOneSeller?>
                        </div>
                    </div>
                <? } ?>
            </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<? // ////////////////////// Оператор /////////////////////////////////////////////// // ?>
<div class="ordr-header-block" id="headerblock-operator-<?=$order->id?>">
  <h3><i class="icon-user"></i><?=Yii::t('main','Заказчик и оператор')?></h3>
        <div class="form"  style="margin: 3px 0;">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'id'=>'order-update-form-operator-'.$order->id,
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>false,
                'method'=>'post',
                'action'=>array("/admin/orders/update"),
                'type'=>'horizontal',
            )); ?>
                <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
                <div class="control-group">
                    <?  echo $form->hiddenField($order,'id',array());
                    if((Yii::app()->user->notInRole(array('superAdmin','topManager'))||$debug)||$order->frozen) {
                    echo $form->uneditableRow($order,'manager');
                    } else {
                    echo $form->dropDownListRow($order,'manager',$order->allowedManagers,array('style'=>'width:100%;'));
                    }
                    echo $form->uneditableRow($order,'managerBalance');
                    echo $form->uneditableRow($order,'userName');
                    echo CHtml::link("<span class=\"icon-user\" style=\"cursor: pointer;position: relative;top: -22px;left:90px;margin:0;\"></span>",
                        array("users/view", "id"=>$order->uid),
                        array("title"=>Yii::t('admin',"Профиль клиента"),"onclick"=>"getContent(this,\"Пользователь №$order->uid\");return false;"));
                        echo $form->uneditableRow($order,'userBalance');
                    ?>
                    <h4>
                        <?php
                        if (!$order->frozen) {
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType'=>'ajaxSubmit',
                            'type'=>'info',
                            'size'=>'mini',
                            'icon'=>'ok white',
                            'htmlOptions'=>array('title'=>Yii::t('admin','Сохранить'),),
                            'label'=>Yii::t('admin',''),
                            'url'=>'/admin/orders/update',
                            'ajaxOptions'=> array(
                            'complete'=>'js:function(jqXHR, textStatus){ reloadSelectedTab(); dsAlert(\''.Yii::t('admin','Сохранено').'\',\''.Yii::t('admin','Подтверждение').'\',true);}',
                            ),
                        ));
                        ?>
                        <?php
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType'=>'reset',
                            'type'=>'danger',
                            'size'=>'mini',
                            'icon'=>'remove white',
                            'htmlOptions'=>array('title'=>Yii::t('admin','Отмена')),
                            'label'=>Yii::t('admin',''),
                        ));
                        }
                        ?>
                    </h4>
                </div>
        </div>
        <?php $this->endWidget(); ?>

    </div>
<? // ///////////////////// Статус /////////////////////////////////////////////////// // ?>
<div class="ordr-header-block" id="headerblock-status-<?=$order->id?>">
  <h3><i class="icon-time"></i><?=Yii::t('main','Статус')?></h3>
    <div class="form" style="margin: 3px 0;">
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
          'id'=>'order-update-form-status-'.$order->id,
          'enableAjaxValidation'=>false,
          'enableClientValidation'=>false,
          'method'=>'post',
          'action'=>array("/admin/orders/update"),
          'type'=>'horizontal',
        )); ?>
            <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
            <div class="control-group">
                <?  echo $form->hiddenField($order,'id',array());
                if((Yii::app()->user->notInRole(array('superAdmin','topManager','orderManager'))||$debug)) {
                    echo $form->uneditableRow($order->statuses,'name',array('size'=>60,'maxlength'=>128));
                } else {
                    $allowedStatuses=$order->allowedStatuses;
                    $allowedStatuses[$order->statuses->value]=Yii::t('main',$order->statuses->name);
                    echo $form->dropDownListRow($order,'status',$allowedStatuses);
                }
                echo $form->uneditableRow($order,'extstatusname');

                if(Yii::app()->user->notInRole(array('superAdmin','topManager'))||$debug) {
                    echo $form->checkboxRow($order, 'frozen',array('disabled'=>true));
                } else {
                    echo $form->checkboxRow($order, 'frozen');
                }

                ?>
                <h4>
                   <?php
                   $this->widget('bootstrap.widgets.TbButton', array(
                       'buttonType'=>'ajaxSubmit',
                       'type'=>'info',
                       'size'=>'mini',
                       'icon'=>'ok white',
                       'htmlOptions'=>array('title'=>Yii::t('admin','Сохранить'),
                           //                     'confirm' => Yii::t('admin','Вы уверены, что хотите изменить статус заказа?')
                       ),
                       'label'=>Yii::t('admin',''),
                       'url'=>'/admin/orders/update',
                       'ajaxOptions'=> array(
                           'complete'=>'js:function(jqXHR, textStatus){reloadSelectedTab(); dsAlert(\''.Yii::t('admin','Сохранено').'\',\''.Yii::t('admin','Подтверждение').'\',true);}',
                       ),
                   ));
                   ?>
                   <?php
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType'=>'reset',
                        'type'=>'danger',
                        'size'=>'mini',
                        'icon'=>'remove white',
                        'htmlOptions'=>array('title'=>Yii::t('admin','Отмена')),
                        'label'=>Yii::t('admin',''),
                    ));
                    ?>
                </h4>
            </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<? // //////////////////// Логистика ///////////////////////////////////////////////// // ?>
<div class="ordr-header-block" id="headerblock-logistic-<?=$order->id?>">
  <h3><i class="icon-truck"></i><?=Yii::t('main','Логистика')?></h3>
    <div class="form"  style="margin: 3px 0;">
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'id'=>'order-update-form-logistic-'.$order->id,
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>false,
            'method'=>'post',
            'action'=>array("/admin/orders/update"),
            'type'=>'horizontal',
        )); ?>
            <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
            <div class="control-group">
                <!--            <div class="span4"> -->
                <?  echo $form->hiddenField($order,'id',array()); ?>

                <table border="1" cellspacing="1" cellpadding="1" style="width:95%;margin:10px 3%;">
                    <tr>
                        <th width="25%" scope="col">&nbsp;</th>
                        <th width="25%" scope="col"><?=Yii::t('admin','В заказе')?></th>
                        <th width="25%" scope="col"><?=Yii::t('admin','По факту')?></th>
                        <th width="25%" scope="col"><?=Yii::t('admin','По лотам')?></th>
                    </tr>
                    <tr>
                        <td scope="row"><?=Yii::t('admin','Вес заказа, грамм')?></td>
                        <td><?=$form->textField($order,'weight',array('size'=>8,'readonly'=>true))?></td>
                        <td><?=$form->textField($order,'manual_weight',array('size'=>8,'readonly'=>($order->status!='IN_PROCESS')||$order->frozen))?>
                        <td><?=$form->textField($order->calculated,'actual_lots_weight',array('size'=>8,'readonly'=>true))?>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="table-select <?=(($order->manual_weight>0)?'green':'red')?>"><?=($order->manual_weight>0)?$order->weight-$order->manual_weight:0?></td>
                        <td class="table-select <?=(($order->manual_weight>0)?'green':'red')?>"><?=$order->weight-$order->calculated->actual_lots_weight?></td>
                    </tr>
                </table>
                <?
                echo $form->textAreaRow($order,'fullAddress',array('readonly'=>true,'style'=>'border-radius:0;width:90%;height: 70px;font-size:12px;resize:none;'));
                echo $form->dropDownListRow($order,'delivery_id',$order->allowedDeliveries,array('disabled'=>($order->status!='IN_PROCESS')||$order->frozen));
                echo $form->textFieldRow($order,'store_id',array('readonly'=>($order->status!='IN_PROCESS')));
                echo $form->textFieldRow($order,'code',array('readonly'=>($order->status!='IN_PROCESS')));
                ?>
                <h4>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'ajaxSubmit',
                    'type'=>'info',
                    'size'=>'mini',
                    'icon'=>'ok white',
                    'htmlOptions'=>array('title'=>Yii::t('admin','Сохранить')),
                    'label'=>Yii::t('admin',''),
                    'url'=>'/admin/orders/update',
                    'ajaxOptions'=> array(
                    'complete'=>'js:function(jqXHR, textStatus){reloadSelectedTab(); dsAlert(\''.Yii::t('admin','Сохранено').'\',\''.Yii::t('admin','Подтверждение').'\',true);}',
                    ),
                ));
                ?>
                <?php
                    $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'reset',
                    'type'=>'danger',
                    'size'=>'mini',
                    'icon'=>'remove white',
                    'htmlOptions'=>array('title'=>Yii::t('admin','Отмена')),
                    'label'=>Yii::t('admin',''),
                ));
                ?>
                </h4>
            </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<? // //////////////////// Стоимость ////////////////////////////////////////////////// // ?>
<div class="ordr-header-block" id="headerblock-prices-<?=$order->id?>">
  <h3><i class="icon-money"></i><?=Yii::t('main','Стоимость')?></h3>
    <div class="form" style="margin: 3px 0;">
    &nbsp;&nbsp;<label class="control-label">
    <?=Yii::t('admin','Курс').' '.DSConfig::getSiteCurrency().' '.Yii::t('admin','за').' 1 cny '.Yii::t('admin','заказ/текущий')?>: <?=
    Formulas::convertCurrency(1,'cny',DSConfig::getSiteCurrency(),4,$order->date)?> /
    <?=Formulas::convertCurrency(1,'cny',DSConfig::getSiteCurrency(),4)?> / <?=Formulas::getCurrencyRate('cny',$order->date)?> / <?=Formulas::getCurrencyRate('cny',time())?>
    </label>
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'id'=>'order-update-form-prices-'.$order->id,
          'enableAjaxValidation'=>false,
          'enableClientValidation'=>false,
            'method'=>'post',
            'action'=>array("/admin/orders/update"),
            'type'=>'horizontal',

        )); ?>
            <?php echo $form->errorSummary($order,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
            <div class="control-group">
                <?  echo $form->hiddenField($order,'id',array()); ?>
                <table class="nostyle" border="0" cellspacing="1" cellpadding="1"  style="width:95%;margin:10px 3%;">
                    <tr>
                        <th width="25%" scope="col">&nbsp;</th>
                        <th width="25%" scope="col"><?=Yii::t('admin','В заказе')?></th>
                        <th width="25%" scope="col"><?=Yii::t('admin','По факту')?></th>
                        <th width="25%" scope="col"><?=Yii::t('admin','По лотам')?></th>
                    </tr>
                  <tr>
                    <td scope="row"><?=Yii::t('admin','Кол-во товаров, шт')?></td>
                    <td title="<?=$order->calculated->varReport('items_count')?>"><?=$form->textField($order->calculated,'items_count',array('size'=>8,'readonly'=>true))?></td>
                    <td title="<?=$order->calculated->varReport('actual_items_count')?>"><?=$form->textField($order->calculated,'actual_items_count',array('size'=>8,'readonly'=>true))?></td>
                    <td>-</td>
                  </tr>
                  <? $diff_items_count= new FormulasVar(round($order->calculated->actual_items_count-$order->calculated->items_count),array(),
                    Yii::t('main','Разница количества штук товаров в лотах'));
                  if ($diff_items_count->val!=0||$debug) { ?>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="table-select <?=(($diff_items_count->val>0)?'green':'red')?>" title="<?=$diff_items_count->description?>"><?=$diff_items_count->val?></td>
                    <td class="table-select <?=(($diff_items_count->val>0)?'green':'red')?>">&nbsp;</td>
                  </tr>
                  <? } ?>
                    <tr>
                        <td scope="row"><?=Yii::t('admin','Себестоимость товаров')?>, CNY</td>
                        <td>-</td>
                        <td title="<?=$order->calculated->varReport('actual_taobao_price')?>"><?=$form->textField($order->calculated,'actual_taobao_price',array('size'=>8,'readonly'=>true))?></td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td scope="row"><?=Yii::t('admin','Себестоимость доставки')?>, CNY</td>
                        <td>-</td>
                        <td title="<?=$order->calculated->varReport('actual_taobao_express_fee')?>"><?=$form->textField($order->calculated,'actual_taobao_express_fee',array('size'=>8,'readonly'=>true))?>
                            <input type="hidden" id="OrderCalculated_actual_taobao_express_fee_inCurr" value="<?=
                            Formulas::convertCurrency($order->calculated->actual_taobao_express_fee,'cny',DSConfig::getSiteCurrency())?>">
                            <? if (DSConfig::getVal('checkout_order_reconfirmation_needed')==1) {?>
                            <a href="#" onclick="$('#Order_manual_delivery',$('#order-update-form-prices-<?=$order->id?>')).val($('#OrderCalculated_actual_taobao_express_fee_inCurr',$('#order-update-form-prices-<?=$order->id?>')).val());return false;">
                                <i class="icon-arrow-down"></i></a>
                            <? } ?>
                        </td>
                        <td>-</td>
                    </tr>
                  <tr>
                    <td scope="row"><?=Yii::t('admin','Расходы на закупку')?>, CNY</td>
                    <td title="<?=$order->calculated->varReport('taobao_total')?>"><?=$form->textField($order->calculated,'taobao_total',array('size'=>8,'readonly'=>true))?></td>
                    <td title="<?=$order->calculated->varReport('actual_taobao_total')?>"><?=$form->textField($order->calculated,'actual_taobao_total',array('size'=>8,'readonly'=>true))?></td>
                      <td>-</td>
                  </tr>
                  <? $diff_taobao_total= new FormulasVar(round($order->calculated->taobao_total-$order->calculated->actual_taobao_total,2),array(),
                    Yii::t('main','Разница в цене закупки на таобао'));
                  if (abs($diff_taobao_total->cval)>0.01||$debug) { ?>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td class="table-select <?=(($diff_taobao_total->cval>0)?'green':'red')?>" title="<?=$diff_taobao_total->description?>"><?=DSConfig::getCurrency(true,false,'cny').$diff_taobao_total->cval?></td>
                      <td class="table-select <?=(($diff_taobao_total->cval>0)?'green':'red')?>">&nbsp;</td>
                    </tr>
                  <? } ?>
                  <? if (DSConfig::getSiteCurrency()!='cny') { ?>
                  <tr>
                    <td scope="row"><?=Yii::t('admin','Расходы на закупку')?>, <?=strtoupper(DSConfig::getSiteCurrency())?></td>
                    <td title="<?=$order->calculated->varReport('taobao_total_curr')?>"><?=$form->textField($order->calculated,'taobao_total_curr',array('size'=>8,'readonly'=>true))?></td>
                    <td title="<?=$order->calculated->varReport('actual_taobao_total_curr')?>"><?=$form->textField($order->calculated,'actual_taobao_total_curr',array('size'=>8,'readonly'=>true))?></td>
                    <td>-</td>
                  </tr>
                  <? } ?>
                  <tr style="border-bottom: solid 1px #C9E0ED;"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                    <td scope="row"><?=Yii::t('admin','Цена доставки')?>, <?=strtoupper(DSConfig::getSiteCurrency())?></td>
                    <td><?=$form->textField($order,'delivery',array('size'=>8,'readonly'=>true))?></td>
                    <td><?=$form->textField($order,'manual_delivery',array('size'=>8,'readonly'=>($order->status!='IN_PROCESS')||$order->frozen))?>
                        <a href="#" onclick="$('#Order_manual_delivery',$('#order-update-form-prices-<?=$order->id?>')).val($('#OrderCalculated_actual_lots_delivery',$('#order-update-form-prices-<?=$order->id?>')).val());return false;">
                        <i class="icon-arrow-left"></i></a>
                    </td>
                    <td title="<?=$order->calculated->varReport('actual_lots_delivery')?>">
                        <?=$form->textField($order->calculated,'actual_lots_delivery',array('size'=>8,'readonly'=>true))?></td>
                  </tr>
                  <? $diff_delivery= new FormulasVar(round((($order->manual_delivery)?$order->manual_delivery:$order->delivery)-$order->delivery,2),array(),
                    Yii::t('main','Разница в цене доставки'));
                     $diff_delivery_lots= new FormulasVar(round($order->delivery-$order->calculated->actual_lots_delivery,2),array(),
                    Yii::t('main','Разница в цене доставки по лотам'));
                  if (abs($diff_delivery_lots->cval)>0.01||$debug) { ?>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="table-select <?=(($diff_delivery->cval>0)?'green':'red')?>" title="<?=$diff_delivery->description?>"><?=$diff_delivery->cval?></td>
                    <td class="table-select <?=(($diff_delivery_lots->cval>0)?'green':'red')?>" title="<?=$diff_delivery_lots->description?>"><?=$diff_delivery_lots->cval?></td>
                  </tr>
                  <? } ?>
                  <tr>
                    <td scope="row"><?=Yii::t('admin','Цена товаров')?>, <?=strtoupper(DSConfig::getSiteCurrency())?></td>
                    <td><?=$form->textField($order,'sum',array('size'=>8,'readonly'=>true))?></td>
                    <td><?=$form->textField($order,'manual_sum',array('size'=>8,'readonly'=>($order->status!='IN_PROCESS')||$order->frozen))?>
                        <a href="#" onclick="$('#Order_manual_sum',$('#order-update-form-prices-<?=$order->id?>')).val($('#OrderCalculated_actual_lots_summ',$('#order-update-form-prices-<?=$order->id?>')).val());return false;">
                        <i class="icon-arrow-left"></i></a>
                    </td>
                    <td title="<?=$order->calculated->varReport('actual_lots_summ')?>">
                        <?=$form->textField($order->calculated,'actual_lots_summ',array('size'=>8,'readonly'=>true))?></td>
                  </tr>
                  <? $diff_summ= new FormulasVar(round((($order->manual_sum)?$order->manual_sum:$order->sum)-$order->sum,2),array(),
                    Yii::t('main','Разница в цене товаров'));
                  $diff_summ_lots= new FormulasVar(round($order->sum-$order->calculated->actual_lots_summ,2),array(),
                    Yii::t('main','Разница в цене товаров по лотам'));
                  if (abs($diff_summ_lots->cval)>0.01||$debug) { ?>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="table-select <?=(($diff_summ->cval>0)?'green':'red')?>" title="<?=$diff_summ->description?>"><?=$diff_summ->cval?></td>
                    <td class="table-select <?=(($diff_summ_lots->cval>0)?'green':'red')?>" title="<?=$diff_summ_lots->description?>"><?=$diff_summ_lots->cval?></td>
                  </tr>
                  <? } ?>
                  <tr>
                    <td scope="row"><?=Yii::t('admin','Цена, итого')?>, <?=strtoupper(DSConfig::getSiteCurrency())?></td>
                    <td title="<?=$order->calculated->varReport('order_total')?>"><?=$form->textField($order->calculated,'order_total',array('size'=>8,'readonly'=>true))?></td>
                    <td title="<?=$order->calculated->varReport('manual_total')?>"><?=$form->textField($order->calculated,'manual_total',array('size'=>8,'readonly'=>true))?></td>
                    <td title="<?=$order->calculated->varReport('actual_lots_total')?>"><?=$form->textField($order->calculated,'actual_lots_total',array('size'=>8,'readonly'=>true))?></td>
                  </tr>
                  <? $diff_total= new FormulasVar(round((($order->calculated->manual_total)?$order->calculated->manual_total:$order->calculated->order_total)-$order->calculated->order_total,2),array(),
                    Yii::t('main','Разница в цене заказа, итого'));
                  $diff_total_lots= new FormulasVar(round($order->calculated->order_total-$order->calculated->actual_lots_total,2),array(),
                    Yii::t('main','Разница в цене заказа по лотам'));
                  if (abs($diff_total_lots->cval)>0.01||$debug) { ?>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="table-select <?=(($diff_total->cval>0)?'green':'red')?>" title="<?=$diff_total->description?>. <?=(($diff_total->cval>0)?Yii::t('admin','Требуется доплата'):Yii::t('admin','Сумма переплаты'))?>"><?=$diff_total->cval?></td>
                    <td class="table-select <?=(($diff_total_lots->cval>0)?'green':'red')?>" title="<?=$diff_total_lots->description?>"><?=$diff_total_lots->cval?></td>
                  </tr>
                  <? } ?>

                  <tr>
                    <td scope="row"><?=Yii::t('admin','Оплачено')?>, <?=strtoupper(DSConfig::getSiteCurrency())?></td>
                    <td>-</td>
                    <td title="<?=$order->calculated->varReport('payments_sum')?>"><?=$form->textField($order->calculated,'payments_sum',array('size'=>8,'readonly'=>true))?></td>
                    <td>-</td>
                  </tr>
                  <? if (abs($order->calculated->payments_saldo)>0.01||$debug) { ?>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="table-select <?=(($order->calculated->payments_saldo>0)?'green':'red')?>" title="<?=$order->calculated->varReport('payments_saldo')?>"><?=$order->calculated->payments_saldo?></td>
                    <td class="table-select <?=(($order->calculated->payments_saldo>0)?'green':'red')?>">&nbsp;</td>
                  </tr>
                  <? } ?>
                </table>
                <? if ($order->calculated->payments_saldo>0||$debug) { ?>
                <?=$form->checkBoxRow($order,'do_moneyback');?>
                <? } ?>
              <span title="<?=$order->calculated->varReport('realProfit')?>"><?=$form->textFieldRow($order->calculated,'realProfit',array('size'=>8,'readonly'=>true))?></span>
              <? if ($order->calculated->operatorProfit!=0||$debug) { ?>
                <span title="<?=$order->calculated->varReport('operatorProfit')?>"></span><?=$form->textFieldRow($order->calculated,'operatorProfit',array('size'=>8,'readonly'=>true,))?></span>
              <? } ?>
               <span title="<?=$order->calculated->varReport('siteProfit')?>"><?=$form->textFieldRow($order->calculated,'siteProfit',array('size'=>8,'readonly'=>true,'style'=>'padding:10px 50px;height:20px;font-size:22px !important;text-align:center;'))?></span>
                <h4>
                    <?php
                    if (!$order->frozen) {
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType'=>'ajaxSubmit',
                        'type'=>'info',
                        'size'=>'mini',
                        'icon'=>'retweet white',
                        'htmlOptions'=>array('title'=>Yii::t('admin','Пересчитать'),
                        //                     'onclick'=>"var recalculated = 1;
                        //                     return confirm('".Yii::t('admin','Вес и суммы по лотам будут перенесены в заказ, переплата будет возвращена. Продолжить?')."');"
                        ),
                        'label'=>Yii::t('admin',''),
                        'url'=>'/admin/orders/update',
                        'ajaxOptions'=> array(
                          'beforeSend'=>"js:function(jqXHR, plainObject){ var result=dsConfirm('".Yii::t('admin','Вес и суммы по лотам будут перенесены в заказ, затем можно будет вернуть переплату. Продолжить?')."','".
                            Yii::t('admin','Перерасчёт заказа')."',false);
                            if (result) {
                            $('#Order_sum').val($('#OrderCalculated_actual_lots_summ').val());
                            $('#Order_delivery').val($('#OrderCalculated_actual_lots_delivery').val());
                            $('#Order_weight').val($('#OrderCalculated_actual_lots_weight').val());
                            }
                            return result;
                            }",
                          'complete'=>'js:function(jqXHR, textStatus){reloadSelectedTab(); dsAlert(\''.Yii::t('admin','Сохранено').'\',\''.Yii::t('admin','Подтверждение').'\',true);
                          }',
                        ),
                      ));
                    ?>
                    <?php
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType'=>'ajaxSubmit',
                        'type'=>'info',
                        'size'=>'mini',
                        'icon'=>'ok white',
                        'htmlOptions'=>array('title'=>Yii::t('admin','Сохранить')),
                        'label'=>Yii::t('admin',''),
                        'url'=>'/admin/orders/update',
                        'ajaxOptions'=> array(
                        'complete'=>'js:function(jqXHR, textStatus){reloadSelectedTab(); dsAlert(\''.Yii::t('admin','Сохранено').'\',\''.Yii::t('admin','Подтверждение').'\',true);}',
                        ),
                    ));
                    ?>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'reset',
                    'type'=>'danger',
                    'size'=>'mini',
                    'icon'=>'remove white',
                    'htmlOptions'=>array('title'=>Yii::t('admin','Отмена')),
                    'label'=>Yii::t('admin',''),
                ));
                    }
                ?>
                </h4>
            </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
</div>
<div class="span7">
<div class="ordr-header-block" id="headerblock-messages-<?=$order->id?>">
      <? Yii::app()->controller->renderPartial('/orders/ordercommentsevents', array(
        'order' => $order,
      ), FALSE, TRUE) ?>
<!-- ========================= Коммернатии ============================ -->
</div>
</div>
</div>

<!-- Items block -->
<div class="ordr-header-block" id="headerblock-items-<?=$order->id?>" name="order-items-<?=$order->id?>">
  <div class="form">
  <fieldset style="height: 100% !important">
    <h3><i class="icon-list"></i><?=Yii::t('admin','Список товаров')?></h3>

<?php
$ordersItemsDataProvider = new CArrayDataProvider($order->ordersItems, array(
  'id' => 'ordersItemsDataProvider-'.$order->id,
  'keyField' => 'id',
  'pagination' => array(
    'pageSize' => 20,
  ),
));

$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => 'ordersItemsGrid-'.$order->id,
    'dataProvider' => $ordersItemsDataProvider,//$model->search(),
     'enableSorting' =>FALSE,
     'hideHeader' => true,
    'type'         => 'condensed',//striped
    'template'     => '{summary}{pager}{summary}{items}',
    'pagerCssClass'=>'pagination order-items',
     'pager'=>array(
       'class'=>'COrderItemsPager',
       'orderItems'=> $order->ordersItems,
       'firstPageLabel'=>'',
       'lastPageLabel'=>'',
       'htmlOptions'=>array('class'=>'paginator'),
       //'nextPageLabel'=>'',
       //'prevPageLabel'=>'',
       'footer'=>'',
       'header'=>'',
       'maxButtonCount'=>1000,
     ),
    'columns'      => array(
      array(
//        'header' =>Yii::t('admin','Ссылка'),
        'type'  => 'raw',
        'value' => function ($data,$row) {
              Yii::app()->controller->widget('application.components.widgets.OrderItem', array(
                  'orderItem' => $data,
                  'order'=>false,
//                  'readOnly' => $order->frozen,
                  'allowDelete' => FALSE,
                  'adminMode' =>TRUE,
                  'publicComments'=>FALSE,
                  'imageFormat' => '_250x250.jpg',
                  'lazyLoad' => FALSE,
                  'light'=>true,
                ));
          }
      ),
    ),
  ));

/*
foreach($order->ordersItems as $i=>$item) {
    if ($i>9) continue;
    ?>
  <? $this->widget('application.components.widgets.OrderItem', array(
    'orderItem' => $item,
    'order'=>$order,
    'readOnly' => $order->frozen,
    'allowDelete' => FALSE,
    'adminMode' =>TRUE,
    'publicComments'=>FALSE,
    'imageFormat' => '_250x250.jpg',
    'lazyLoad' => FALSE,
  ));
  ?>
     <? } */?>
</fieldset>
  </div>
</div>
<!-- end of Items block -->
<? $this->renderPartial('_ajax_update_item',array('oid'=>$order->id)); ?>
<script>
  $(function() {
    $( "#headerblock-id-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: false
      //disabled: true
    });
    $( "#headerblock-operator-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: false
      //disabled: true
    });
    $( "#headerblock-status-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: 0
      //disabled: true
    });
    $( "#headerblock-logistic-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: false
      //disabled: true
    });
    $( "#headerblock-prices-<?=$order->id?>" ).accordion({
      collapsible: true,
      active: 0
      //disabled: true
    });
  });
//  ordr-header-block
</script>
