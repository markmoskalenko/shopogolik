<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ordercommentsevents.php">
* </description>
**********************************************************************************************************************/?>
<div class="subtabs" id="headerblock-messages-subtabs">
  <ul>
    <li><a href="#tab-comments"><?= Yii::t('admin', 'Комментарии') ?></a></li>
    <li><a href="#tab-orderpayments"><?=Yii::t('main','Платежи')?></a></li>
    <li><a href="#tab-history"><?= Yii::t('main', 'История заказа') ?></a></li>
  </ul>
  <div id="tab-comments" style="padding: 0 !important;overflow-y: hidden; overflow-x: hidden;" >
    <? $this->widget('application.components.widgets.OrderCommentsBlock', array(
      'orderId'       => $order->id,
      'orderItemId'   => FALSE,
      'public'        => FALSE,
      'showInternals' => 1,
      'pageSize'      => 1000,
      'imageFormat'   => '_200x200.jpg',
    ));
    ?>
  </div>
  <div id="tab-orderpayments" style="padding: 0 !important;">
    <? $this->widget('application.components.widgets.OrderPaymentsBlock', array(
      'orderId'  => $order->id,
      'pageSize' => 1000,
    ));
    ?>
  </div>
  <div id="tab-history" style="padding: 0 !important;">
    <?$this->widget('application.components.widgets.EventsBlock', array(
      'subjectId'  => $order->id,
      'eventsType' => '^Order\.|^OrdersItems\.',
      'pageSize'   => 1000,
    ));
    ?>
  </div>
</div>

<script>
  $(function makeSubTabs() {
    $(".subtabs").tabs({
      cache: false
    });
  });
</script>