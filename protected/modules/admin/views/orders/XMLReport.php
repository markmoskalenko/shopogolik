<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="XMLReport.php">
* </description>
**********************************************************************************************************************/?>
<?
echo '<?xml version="1.0" encoding="UTF-8" ?>';?>
<order>
  <? if ($order) { ?>
    <? //==================================================================================================================?>
    <base title="<?= Yii::t('admin', 'Базовые параметры') ?>">
    <?= Utils::ARtoXMLnode($order, 'id') ?>
    <?= Utils::ARtoXMLnode($order, 'orderCode') ?>
    <?= Utils::ARtoXMLnode($order, 'textdate') ?>
    <?= Utils::ARtoXMLnode($order, 'orderAge') ?>
    </base>
    <manager title="<?= Yii::t('admin', 'Менеджер') ?>">
      <?= Utils::ARtoXMLnode($order->managers, 'email') ?>
      <?= Utils::ARtoXMLnode($order, 'managerBalance') ?>
    </manager>
    <customer title="<?= Yii::t('admin', 'Заказчик') ?>">
      <?= Utils::ARtoXMLnode($order, 'userName') ?>
      <?= Utils::ARtoXMLnode($order, 'userBalance') ?>
    </customer>
    <status title="<?= Yii::t('admin', 'Статус') ?>">
      <?= Utils::ARtoXMLnode($order->statuses, 'name') ?>
      <?= Utils::ARtoXMLnode($order, 'extstatusname') ?>
    </status>
    <logistic title="<?= Yii::t('admin', 'Логистика') ?>">
      <?= Utils::ARtoXMLnode($order, 'weight') ?>
      <?= Utils::ARtoXMLnode($order, 'manual_weight') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'actual_lots_weight') ?>
      <?= Utils::ARtoXMLnode($order, 'fullAddress') ?>
      <?= Utils::ARtoXMLnode($order, 'delivery_id') ?>
      <?= Utils::ARtoXMLnode($order, 'store_id') ?>
      <?= Utils::ARtoXMLnode($order, 'code') ?>
    </logistic>
    <price title="<?= Yii::t('admin', 'Цены') ?>">
      <?= Utils::ARtoXMLnode($order->calculated, 'items_count') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'actual_items_count') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'taobao_total') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'actual_taobao_total') ?>
      <?= Utils::ARtoXMLnode($order, 'delivery') ?>
      <?= Utils::ARtoXMLnode($order, 'manual_delivery') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'actual_lots_delivery') ?>
      <?= Utils::ARtoXMLnode($order, 'sum') ?>
      <?= Utils::ARtoXMLnode($order, 'manual_sum') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'actual_lots_summ') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'order_total') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'manual_total') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'actual_lots_total') ?>
      <?= Utils::ARtoXMLnode($order->calculated, 'payments_sum') ?>
    </price>
    <items>
      <?php foreach ($order->ordersItems as $item) { ?>
        <item>
          <?= Utils::ARtoXMLnode($item, 'id') ?>
          <?= Utils::ARtoXMLnode($item, 'iid') ?>
          <?= Utils::ARtoXMLnode($item, 'props') ?>
          <?= Utils::ARtoXMLnode($item, 'input_props') ?>
          <?= Utils::ARtoXMLnode($item, 'pic_url') ?>
          <?= Utils::ARtoXMLnode($item, 'title') ?>
          <?= Utils::ARtoXMLnode($item, 'sku_id') ?>
          <?= Utils::ARtoXMLnode($item, 'status_text') ?>
          <?= Utils::ARtoXMLnode($item, 'status') ?>
          <?= Utils::ARtoXMLnode($item, 'tid') ?>
          <?= Utils::ARtoXMLnode($item, 'track_code') ?>
          <? if (($item->taobao_promotion_price < $item->taobao_price) && ($item->taobao_promotion_price > 0)) {
            $declaredPrice = $item->taobao_promotion_price;
            echo Utils::ARtoXMLnode($item, 'taobao_promotion_price');
          }
          else {
            $declaredPrice = $item->taobao_price;
            echo Utils::ARtoXMLnode($item, 'taobao_price');
          }
          ?>
          <?= Utils::ARtoXMLnode($item, 'calculated_actualPrice') ?>
          <?= Utils::ARtoXMLnode($item, 'weight') ?>
          <?= Utils::ARtoXMLnode($item, 'calculated_actualWeight') ?>
          <?= Utils::ARtoXMLnode($item, 'express_fee') ?>
          <?= Utils::ARtoXMLnode($item, 'calculated_actualExpressFee') ?>
          <?= Utils::ARtoXMLnode($item, 'num') ?>
          <?= Utils::ARtoXMLnode($item, 'actual_num') ?>
          <?= Utils::ARtoXMLnode($item, 'calculated_lotPrice') ?>
          <?= Utils::ARtoXMLnode($item, 'actual_lot_price') ?>
          <?= Utils::ARtoXMLnode($item, 'calculated_lotWeight') ?>
          <?= Utils::ARtoXMLnode($item, 'actual_lot_weight') ?>
          <?= Utils::ARtoXMLnode($item, 'calculated_lotExpressFee') ?>
          <?= Utils::ARtoXMLnode($item, 'actual_lot_express_fee') ?>
          <params>
            <?php if (isset($item->input_props_array)) {
              $input_props_array = $item->input_props_array;
            }
            else {
              $input_props_array = json_decode($item->input_props);
            }
            if (!$input_props_array || ($input_props_array == NULL)) {
              $input_props_array = array();
            }
            foreach ($input_props_array as $name => $value) {
              ?>
              <param>
              <?=Utils::ARtoXMLnode($value,'name_zh')?>
              <?=Utils::ARtoXMLnode($value,'value_zh')?>
              </param>
            <? } ?>
          </params>
          <urls>
            <itemOnTaobao>http://item.taobao.com/item.htm?id=<?= $item->iid ?></itemOnTaobao>
            <sellerRateOnTaobao>http://rate.taobao.com/user-rate-<?= $item->seller_id; ?>.html</sellerRateOnTaobao>
            <shopOnTaobao>http://shopsearch.taobao.com/search?q=<?= $item->seller_nick ?></shopOnTaobao>
            <sellerItemsOnTaobao>
              http://s.taobao.com/search?app=usersearch&amp;user_id=<?= $item->seller_id; ?></sellerItemsOnTaobao>
            <cabinetOnTaobao>
              http://trade.taobao.com/trade/detail/trade_item_detail.htm?bizOrderId=<?= $item->tid ?></cabinetOnTaobao>
          </urls>
        </item>
      <? } ?>
    </items>
    <? //==================================================================================================================?>
  <? } ?>
</order>