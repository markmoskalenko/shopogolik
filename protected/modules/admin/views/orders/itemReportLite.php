<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="itemReport.php">
* </description>
**********************************************************************************************************************/?>
<? if (($item) && ($order)) { ?>
  <table cellspacing=0>
    <tr>
      <td valign=top><img src="/img/barcode/code/<?=($item->track_code)?$item->track_code:'NOT DEFINED'?>"></td>
    </tr>
    <tr>
      <td valign=top style="text-align: center; font-family: courier, monospace; font-weight: 700;"><?='UID:'.$order->uid.' ID:'.$item->oid.'-'.$item->id?></td>
    </tr>
  </table>
<? }