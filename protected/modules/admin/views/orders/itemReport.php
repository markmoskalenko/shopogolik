<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="itemReport.php">
* </description>
**********************************************************************************************************************/?>
<? if (($item) && ($order)) { ?>
  <hr/>
  <br/>
  <table border=1 cellspacing=0 cellpadding=3 width="420px">
    <tr>
      <td colspan=4 valign=top><p><img src="/img/barcode/code/<?=($order->store_id)? $order->store_id : 'NOT DEFINED'?>"></td>
    </tr>
    <tr>
      <td width=204 valign=top><?=Yii::t('admin','Заказчик и получатель заказа')?>:</td>
      <td colspan=3 valign=top><?=$order->userName?><hr><?=$order->fullAddress?></td>
    </tr>
    <tr>
      <td width=204 valign=top><?=Yii::t('admin','Менеджер заказа')?>:</td>
      <td width=601 valign=top><?=$order->managers->email?></td>
      <td width=438 valign=top><?=Yii::t('admin','Служба доставки')?>:</td>
      <td width=158 valign=top><?=$order->delivery_id?></td>
    </tr>
    <tr>
      <td width=204 valign=top><?=Yii::t('admin','Приход на склад')?>:</td>
      <td width=601 valign=top>&nbsp;</td>
      <td width=438 valign=top><?=Yii::t('admin','Вес лота')?>:</td>
      <td width=158 valign=top>&nbsp;</td>
    </tr>
    <tr>
      <td width=204 valign=top><?=Yii::t('admin','Расход со склада')?>:</td>
      <td width=601 valign=top>&nbsp;</td>
      <td width=438 valign=top><?=Yii::t('admin','Кол-во')?>:</td>
      <td width=158 valign=top><p><?=($item->actual_num) ? $item->actual_num : $item->num?></td>
    </tr>
    <tr>
      <td width=204 valign=top><img src="<?= Img::getImagePath($item->pic_url, '_200x200.jpg') ?>" alt=""/></td>
      <td colspan=3 valign=top>
        <?= (preg_match('/editTranslation|<translation/s',$item->title)) ? $item->title : Yii::app()->DanVitTranslator->translateText($item->title, 'zh-CHS', Utils::TransLang()); ?>
        <br/><br/>
        <?= $item->title ?>
        <?php if (isset($item->input_props_array)) {
          $input_props_array = $item->input_props_array;
        }
        else {
          $input_props_array = json_decode($item->input_props);
        }
        if (!$input_props_array || ($input_props_array == null)) {
          $input_props_array = array();
        }
        foreach ($input_props_array as $name => $value) {
          ?>
          <br/><br/>
          <strong><?= (preg_match('/editTranslation|<translation/s',$value->name)) ? $value->name : Yii::app()->DanVitTranslator->translateText($value->name, 'zh-CHS', Utils::TransLang()); ?></strong>:
          <?= (preg_match('/editTranslation|<translation/s',$value->value)) ? $value->value : Yii::app()->DanVitTranslator->translateText($value->value, 'zh-CHS', Utils::TransLang()); ?>
          <? if (isset($value->name_zh) && isset($value->value_zh)) { ?>
            <br/><strong><?= $value->name_zh; ?>:</strong> <?= $value->value_zh; ?>
          <? } ?>
        <? } ?>
      </td>
    </tr>
    <tr>
      <td width=204 valign=top><?=Yii::t('admin','Складской ID заказа')?>:</td>
      <td colspan=3 valign=top><img src="/img/barcode/code/<?= $order->uid . '-' . $item->oid . '-' . $item->id ?>"></td>
    </tr>
    <tr>
      <td width=204 valign=top><?=Yii::t('admin','Поставщик')?>:</td>
      <td colspan=3 valign=top><?=$item->seller_nick.' ID:'.$item->seller_id?></td>
    </tr>
  </table>
  <br/>
  <hr/>
<? }