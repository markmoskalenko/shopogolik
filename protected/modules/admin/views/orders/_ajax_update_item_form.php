<div id='orders-items-update-modal-<?=$model->oid?>' class="modal hide orderitem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <h3><?=Yii::t('admin','Редактирование лота заказа')?>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </h3>
    </div>
    <div class="modal-body">
        <? Yii::app()->controller->widget('application.components.widgets.OrderItem', array(
        'orderItem' => $model,
        'order'=>false,
     // 'readOnly' => $order->frozen,
        'allowDelete' => FALSE,
        'adminMode' =>TRUE,
        'publicComments'=>FALSE,
        'imageFormat' => '_250x250.jpg',
        'lazyLoad' => FALSE,
        'light'=>false,
        'dialog'=>true,
        ));
        ?>
    </div>
</div><!--end modal-->



