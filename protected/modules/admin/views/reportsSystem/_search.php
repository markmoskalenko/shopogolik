<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_search.php">
* </description>
**********************************************************************************************************************/?>
<?php  $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'search-reports-system-form',
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
));  ?>

<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'id',array('class'=>'span3')); ?>
        <?php echo $form->textFieldRow($model,'internal_name',array('class'=>'span10','maxlength'=>512)); ?>
        <?php echo $form->textFieldRow($model,'name',array('class'=>'span10','maxlength'=>2048)); ?>

        <?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
        <?php echo $form->textAreaRow($model,'query',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
    </div>
    <div class="span6">
        <?php echo $form->textFieldRow($model,'type',array('class'=>'span10','maxlength'=>512)); ?>
        <?php echo $form->textFieldRow($model,'group',array('class'=>'span10','maxlength'=>512)); ?>
        <?php echo $form->textFieldRow($model,'enabled',array('class'=>'span10')); ?>

        <?php echo $form->textAreaRow($model,'data_props',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
        <?php echo $form->textAreaRow($model,'view_props',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
    </div>
</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'info', 'icon'=>'search white', 'label'=>Yii::t('admin','Поиск'))); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button',  'type'=>'danger', 'icon'=>'icon-remove-sign white', 'label'=>Yii::t('admin','Сброс'))); ?>
	</div>

<?php $this->endWidget(); ?>

<?php /* Alexys $cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile(Yii::app()->request->baseUrl.'/themes/admin/css/jquery-ui.css');
*/
?>	
   <script>
	$(".btnreset").click(function(){
		$(":input","#search-reports-system-form").each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		if (type == "text" || type == "password" || tag == "textarea") this.value = "";
		else if (type == "checkbox" || type == "radio") this.checked = false;
		else if (tag == "select") this.selectedIndex = "";
	  });
	});
   </script>

