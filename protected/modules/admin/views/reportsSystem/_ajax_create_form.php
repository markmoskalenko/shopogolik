<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_ajax_create_form.php">
 * </description>
 **********************************************************************************************************************/
?>
<div id='reports-system-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?= Yii::t('admin', 'Новый отчёт') ?></h3>
  </div>
  <div class="modal-body">
    <div class="form">
      <?php
      $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'reports-system-create-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'                 => 'post',
        'action'                 => array("reportsSystem/create"),
        'type'                   => 'horizontal',
        'htmlOptions'            => array(
          'onsubmit' => "return false;", /* Disable normal form submit */
        ),
        'clientOptions'          => array(
          'validateOnType'   => TRUE,
          'validateOnSubmit' => TRUE,
          'afterValidate'    => 'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
        ),
      )); ?>

        <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
        <?php echo $form->errorSummary($model, 'Opps!!!', NULL, array('class' => 'alert alert-error span12')); ?>

        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->labelEx($model, 'internal_name'); ?>
                <?php echo $form->textField($model, 'internal_name', array('size' => 60,'class'=>'span12', 'maxlength' => 512)); ?>
                <?php echo $form->error($model, 'internal_name'); ?>

                <?php echo $form->labelEx($model, 'description'); ?>
                <?php echo $form->textArea($model, 'description', array('rows' => 6,'class'=>'span12', 'cols' => 50)); ?>
                <?php echo $form->error($model, 'description'); ?>

                <?php echo $form->labelEx($model, 'query'); ?>
                <?php echo $form->textArea($model, 'query', array('rows' => 6,'class'=>'span12', 'cols' => 50)); ?>
                <?php echo $form->error($model, 'query'); ?>

            </div>
            <div class="span6">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('size' => 60,'class'=>'span12','maxlength' => 2048)); ?>
                <?php echo $form->error($model, 'name'); ?>

                <?php echo $form->labelEx($model, 'data_props'); ?>
                <?php echo $form->textArea($model, 'data_props', array('rows' => 6,'class'=>'span12', 'cols' => 50)); ?>
                <?php echo $form->error($model, 'data_props'); ?>

                <?php echo $form->labelEx($model, 'view_props'); ?>
                <?php echo $form->textArea($model, 'view_props', array('rows' => 6,'class'=>'span12', 'cols' => 50)); ?>
                <?php echo $form->error($model, 'view_props'); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model, 'type'); ?>
                <?php echo $form->textField($model, 'type', array('size' => 60,'class'=>'span12', 'maxlength' => 512)); ?>
                <?php echo $form->error($model, 'type'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model, 'group'); ?>
                <?php echo $form->textField($model, 'group', array('size' => 60,'class'=>'span12', 'maxlength' => 512)); ?>
                <?php echo $form->error($model, 'group'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model, 'enabled'); ?>
                <?php echo $form->textField($model, 'enabled', array('size' => 60,'class'=>'span3')); ?>
                <?php echo $form->error($model, 'enabled'); ?>
            </div>
        </div>
    </div>

  </div><!--end modal body-->
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'  => 'submit',
                'type'        => 'info',
                'icon'        => 'ok white',
                'label'       => $model->isNewRecord ? Yii::t('admin', 'Добавить') : Yii::t('admin', 'Сохранить'),
                'htmlOptions' => array('onclick' => 'create();'),
            )
        );
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'type'        => 'danger',
            'buttonType' => 'reset',
            'icon'       => 'remove white',
            'label'      => Yii::t('admin', 'Сброс'),
        )); ?>
    </div><!--end modal footer-->
    <?php $this->endWidget(); ?>

</div><!--end modal-->

<!--Script section-->
<script type="text/javascript">
  function create() {
    var data = $("#reports-system-create-form").serialize();
    jQuery.ajax({
    type: 'POST',
    url: '<?php
    echo Yii::app()->createAbsoluteUrl("admin/reportsSystem/create"); ?>',
    data: data,
    success: function (data) {
        //alert("succes:"+data);
        if (data != "false") {
          $('#reports-system-create-modal').modal('hide');
          renderView(data);
          $.fn.yiiGridView.update('reports-system-grid', {
          });
        }
      },
      error: function (data) { // if error occured
        alert("Error occured, please try again");
        alert(data);
      },
      dataType: 'html'
    });
  }
  function renderCreateForm() {
    $('#reports-system-create-form').each(function () {
      this.reset();
    });
    $('#reports-system-view-modal').modal('hide');
    $('#reports-system-create-modal').modal({
      show: true
    });
  }
</script>
