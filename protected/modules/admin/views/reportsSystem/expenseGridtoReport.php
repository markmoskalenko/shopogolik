<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="expenseGridtoReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      internal_name		</th>
 		<th width="80px">
		      name		</th>
 		<th width="80px">
		      description		</th>
 		<th width="80px">
		      query		</th>
 		<th width="80px">
		      data_props		</th>
 		<th width="80px">
		      view_props		</th>
 		<th width="80px">
		      type		</th>
 		<th width="80px">
		      group		</th>
 		<th width="80px">
		      enabled		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->internal_name; ?>
		</td>
       		<td>
			<?php echo $row->name; ?>
		</td>
       		<td>
			<?php echo $row->description; ?>
		</td>
       		<td>
			<?php echo $row->query; ?>
		</td>
       		<td>
			<?php echo $row->data_props; ?>
		</td>
       		<td>
			<?php echo $row->view_props; ?>
		</td>
       		<td>
			<?php echo $row->type; ?>
		</td>
       		<td>
			<?php echo $row->group; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
