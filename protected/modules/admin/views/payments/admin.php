<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="admin.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	'Payments'=>array('index'),
	'Manage',
);

$this->menu=array(
  array('label'=>Yii::t('admin','Список'),'url'=>array('index')),
  array('label'=>Yii::t('admin','Добавить'),'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('payment-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?=Yii::t('admin','Управление платежами')?></h1>

<p>
  <?=Yii::t('admin','Вы можете использовать операторы сравнения')?> (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>)</p>

<?php echo CHtml::link(Yii::t('admin','Расширенный поиск'),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'payment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        'uid',
		'id',
		'sum',
		'description',
		'status',
      array(
        'type' => 'raw',
        'name' => 'date',
        'value' => "date('Y-m-d H:i:s', $data->date)",
      ),
      array (
        'type'=>'raw',
        'name'=>'email',
        'value'=>'$data->u->email',
      ),
      array (
        'type'=>'raw',
        'name'=>'username',
        'value'=>'trim($data->u->lastname." ".$data->u->firstname)',
      ),
      array (
        'type'=>'raw',
        'name'=>'phone',
        'value'=>'$data->u->phone',
      ),
		/*
		'check_summ',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
