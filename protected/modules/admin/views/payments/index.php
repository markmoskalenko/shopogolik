<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs = array(
  'Payments',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').slideToggle('fast');
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('payment-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<!--<h1><? //=Yii::t('admin','Платежи')?></h1>
<hr /> -->

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
  'htmlOptions' => array(
    'class' => ''
  )
));
$this->widget('bootstrap.widgets.TbMenu', array(
  'type'  => 'pills',
  'items' => array(
//		array('label'=>Yii::t('admin','Создать'), 'icon'=>'icon-plus', 'url'=>'javascript:void(0);','linkOptions'=>array('onclick'=>'renderCreateForm()')),
    //array('label'=>Yii::t('admin','Список'), 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('index'),'active'=>true, 'linkOptions'=>array()),
    array('label'       => Yii::t('admin', 'Поиск'),
          'icon'        => 'icon-search',
          'url'         => '#',
          'linkOptions' => array('class' => 'search-button')
    ),
    array('label'       => Yii::t('admin', 'PDF'),
          'icon'        => 'icon-download',
          'url'         => Yii::app()->controller->createUrl('GeneratePdf'),
          'linkOptions' => array('target' => '_blank'),
          'visible'     => TRUE
    ),
    array('label'       => Yii::t('admin', 'Excel'),
          'icon'        => 'icon-download',
          'url'         => Yii::app()->controller->createUrl('GenerateExcel'),
          'linkOptions' => array('target' => '_blank'),
          'visible'     => TRUE
    ),
  ),
));
$this->endWidget();
?>



<div class="search-form" style="display:none">
  <?php $this->renderPartial('_search', array(
    'model' => $model,
  )); ?>
</div><!-- search-form -->


<?php

$this->widget('bootstrap.widgets.TbGridView', array(
  'id'           => 'payment-grid',
  'dataProvider' => $model->search(),
  'filter'       => $model,
  'type'         => 'striped bordered condensed',
  'template'     => '{summary}{items}{pager}',
  'columns'      => array(
    'id',
    'sum',
    'description',
    array(
      'name'   => 'status',
      'filter' => array(
        1 => Yii::t('admin', 'Зачисление или возврат средств') . '(1)',
        2 => Yii::t('admin', 'Снятие средств') . '(2)',
        3 => Yii::t('admin', 'Ожидание зачисления средств') . '(3)',
        4 => Yii::t('admin', 'Отмена ожидания зачисления средств') . '(4)',
        5 => Yii::t('admin', 'Отправка внутреннего перевода средств') . '(5)',
        6 => Yii::t('admin', 'Получение внутреннего перевода средств') . '(6)',
      ),
      'value'  => '$data->text_status." (".$data->status.")"',
    ),
    array(
      'type'  => 'raw',
      'name'  => 'date',
      'value' => 'date("Y-m-d H:i:s", $data->date)',
    ),
    array(
          'type'  => 'raw',
          'name'  => 'uid',
          'value' => 'CHtml::link($data->uid, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->u->email."\");return false;"))',
        ),
        array(
          'type'  => 'raw',
          'name'  => 'email',
          'value' => 'CHtml::link($data->u->email, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->u->email."\");return false;"))',
        ),
        array(
          'type'  => 'raw',
          'name'  => 'username',
          'value' => 'trim($data->u->lastname." ".$data->u->firstname)',
        ),
        array(
          'type'  => 'raw',
          'name'  => 'phone',
          'value' => '$data->u->phone',
        ),
        /*
        'check_summ',
        */
    array(

      'type'        => 'raw',
      'value'       => '"
		      <a href=\'javascript:void(0);\' onclick=\'renderView(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
		      <a href=\'javascript:void(0);\' onclick=\'renderUpdateForm(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
//		      <a href=\'javascript:void(0);\' onclick=\'delete_record(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-trash\'></i></a>
      'htmlOptions' => array('style' => 'width:70px;')
    ),

  ),
));

$this->renderPartial("_ajax_update");
// $this->renderPartial("_ajax_create_form",array("model"=>$model));
$this->renderPartial("_ajax_view");

?>


<script type="text/javascript">
  function delete_record(id) {

    if (!confirm("Are you sure you want delete this?"))
      return;

    //  $('#ajaxtest-view-modal').modal('hide');

    var data = "id=" + id;


    jQuery.ajax({
      type: 'POST',
      url: '<?php echo Yii::app()->createAbsoluteUrl("admin/payments/delete"); ?>',
      data: data,
      success: function (data) {
        if (data == "true") {
          $('#payment-view-modal').modal('hide');
          $.fn.yiiGridView.update('payment-grid', {

          });

        }
        else
          alert("deletion failed");
      },
      error: function (data) { // if error occured
        alert(JSON.stringify(data));
        alert("Error occured, please try again");
        //  alert(data);
      },

      dataType: 'html'
    });

  }
</script>

<style type="text/css" media="print">
  body {
    visibility: hidden;
  }

  .printableArea {
    visibility: visible;
  }
</style>
<script type="text/javascript">
  function printDiv() {

    window.print();

  }
</script>
 

