<div id="cms-pages-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#cms-pages-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsPages/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#cms-pages-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('cms-pages-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#cms-pages-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsPages/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#cms-pages-update-modal-container').html(data); 
                 $('#cms-pages-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
