
    <div id='cms-pages-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новая страница')?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
   <?php
   $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    	'id'=>'cms-pages-create-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("cmsPages/create"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
                    'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->labelEx($model,'page_id'); ?>
                <?php echo $form->textField($model,'page_id',array('class'=>'span12','maxlength'=>255)); ?>
                <?php echo $form->error($model,'page_id'); ?>

                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled',array('class'=>'span3')); ?>
                <?php echo $form->error($model,'enabled'); ?>
            </div>
            <div class="span6">
                <?php echo $form->labelEx($model,'url'); ?>
                <?php echo $form->textField($model,'url',array('class'=>'span12','maxlength'=>255)); ?>
                <?php echo $form->error($model,'url'); ?>

                <?php echo $form->labelEx($model,'SEO'); ?>
                <?php echo $form->textField($model,'SEO',array('class'=>'span3')); ?>
                <?php echo $form->error($model,'SEO'); ?>
            </div>
        </div>

  </div><!--end modal body-->

</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove white',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

<!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#cms-pages-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/cmsPages/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#cms-pages-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('cms-pages-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#cms-pages-create-form').each (function(){
  this.reset();
   });
  $('#cms-pages-view-modal').modal('hide');
  $('#cms-pages-create-modal').modal({
   show:true
  });
}
</script>
