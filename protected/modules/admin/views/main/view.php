<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<h3><?=Yii::t('admin','Рабочий стол')?></h3>
<?//$userAsManager?>
<div class="row-fluid">
<div class="span6">
<div class="summary"><?=Yii::t('main','Добро пожаловать')?>, <?=$userAsManager->firstname?>!</div>
   <div class="hello">
    <p><?=Yii::t('main','Номер счета')?>:&nbsp;<span><?=Yii::app()->user->getPersonalAccount()?></span></p>
    <? if (true) { ?>
    <p><?=Yii::t('main','Ваш персональный промо-код')?>:&nbsp;<span><?=Users::getPromoByUid(Yii::app()->user->id)?></span></p>
      <? } ?>
    <p><?=Yii::t('main','Остаток на счете')?>:
        <span><?=Formulas::priceWrapper(Formulas::convertCurrency(Users::getBalance(Yii::app()->user->id),DSConfig::getVal('site_currency'),DSConfig::getCurrency()),DSConfig::getCurrency());?></span</p>
    <? if ($userAsManager->manager) {?>
    <p><?=Yii::t('main','Ваш персональный менеджер')?>:
        <?=Yii::t('main','EMail')?>:<a href="email:<?=$userAsManager->manager->email?>"><?=$userAsManager->manager->email?></a></p>
     <? if ($userAsManager->manager->skype) {?>
     <p><?=Yii::t('main','Skype')?>:<a href="skype:<?=$userAsManager->manager->skype?>"><?=$userAsManager->manager->skype?></a></p>
        <? } ?>
     <? if ($userAsManager->manager->vk) {?>
     <p><?=Yii::t('main','Vk')?>:<a href="<?=$userAsManager->manager->vk?>"><?=$userAsManager->manager->vk?></a></p>
        <? } ?>
      <? } ?>
  </div>

<?/**
      <div>
          <? //Обработано заказов за 6 месяцев ?>
          <?$this->widget('application.components.widgets.ReportBlock', array(
              'report' => ReportsSystem::getReport('MANAGER_OREDRS_6MONTH', array(':manager'=>$userAsManager->uid),1),
          ));
          ?>
      </div>
      <div>
          <? //Распределение Ваших незакрытых заказов по статусам ?>
          <?$this->widget('application.components.widgets.ReportBlock', array(
              'report' => ReportsSystem::getReport('MANAGER_OREDRS_BY_STATUSES', array(':manager'=>$userAsManager->uid),1),
          ));
          ?>
      </div>
 ***/?>
    <h3><?=Yii::t('admin','Новости проекта');?></h3>
    <div class="poject-news">
    <? $this->renderPartial('whatsnew',array(),false,true); ?>
    </div>
</div>
  <div class="span6">
    <div class="summary"><?=Yii::t('admin','Новости')?></div>
       <?  $this->widget('application.modules.admin.components.widgets.AdminNewsBlock', array(
      'id' =>'admin-news-desktop',
      'pageSize' =>10,
      ));
    ?>
</div>
</div>
<div class="row-fluid">
    <div class="span12">
            <?
            $this->widget('application.modules.admin.components.widgets.OrdersListBlock', array(
                'type' => null,
                'name' => Yii::t('admin','Заказы Ваших клиентов по статусам'),
                'idPrefix' =>'desktop',
                'narrowView'=>true,
                'manager'=>Yii::app()->user->id,
                'filter'=>true,
                'pageSize' =>10,
            ));
            ?>
    </div>
</div>
<div class="row-fluid">
<div class="span6"> <!-- События по заказам -->
<h3><?=Yii::t('admin','События по заказам')?></h3>
<div style="display: block;height: 500px;overflow-x: auto;overflow-y: scroll;">
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'orders-events-desktop',
    'dataProvider' => $userAsManager->usersOrdersEvents,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}', //{summary}{pager}
//    'ajaxUpdate'=>'headerblock-messages-subtabs',
    //'afterAjaxUpdate'=>'function () {makeSubTabs();}',
    'columns' => array(
//      'id',
//      'oid',
        array(
            'type'  => 'raw',
            'name'  => 'subject_id',
            'header'=>Yii::t('admin','Заказ'),
            'value' => function ($data) {
                    $res=CHtml::link($data['subject_id'], array('orders/view', 'id'=>$data["subject_id"]),
                        array('title'=>Yii::t('admin','Профиль заказа'),
                            'onclick'=>'getContent(this,"'.Yii::t('admin', 'Заказ') . ' '. $data['subject_id'].'");return false;'));
                    $res=$res."&nbsp;".
                        CHtml::link('<span class="icon-zoom-in" style="display:inline-block;cursor: pointer;"></span>',
                            array("orders/view", "id"=>$data["subject_id"]),
                            array("class"=>"order_open",
                                "title"=>Yii::t("admin","Профиль заказа"),
                                'onclick'=>'getContent(this,"'.Yii::t('admin', 'Заказ') . ' '. $data['subject_id'].'");return false;'));
                    return $res;
                }
        ),
        array(
            'name'=>'date',
            'header'=>Yii::t('admin','Дата'),
        ),
        array(
            'name'=>'eventName',
            'header'=>Yii::t('admin','Событие'),
        ),
        array(
            'name'=>'subject_value',
            'header'=>Yii::t('admin','Параметры'),
        ),
        array(
            'name'=>'fromName',
            'header'=>Yii::t('admin','Инициатор'),
        ),
    ),
));
?>
        </div>
</div>
<div class="span6">
<h3><?=Yii::t('admin','Сообщения по заказам')?></h3> <!-- Сообщения по заказам -->
<div style="display: block;height: 500px;overflow-y: auto; overflow-x: auto;">
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'grid-orders-comments-desktop',
    'dataProvider' => $userAsManager->usersOrdersMessages,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}', //{summary}{pager}
    'columns' => array(
        array(
            'type'  => 'raw',
            'name'  => 'obj_id',
            'header'=>Yii::t('admin','Заказ'),
            'value' => function ($data) {
                    $res=CHtml::link($data['obj_id'], array('orders/view', 'id'=>$data["obj_id"]),
                        array('title'=>Yii::t('admin','Профиль заказа'),
                            'onclick'=>'getContent(this,"'.Yii::t('admin', 'Заказ') . ' '. $data['obj_id'].'");return false;'));
                    $res=$res."&nbsp;".
                        CHtml::link('<span class="icon-zoom-in" style="display:inline-block;cursor: pointer;"></span>',
                            array("orders/view", "id"=>$data["obj_id"]),
                            array("class"=>"order_open",
                                "title"=>Yii::t("admin","Профиль заказа"),
                                'onclick'=>'getContent(this,"'.Yii::t('admin', 'Заказ') . ' '. $data['obj_id'].'");return false;'));
                    return $res;
                }
        ),
        array('name'=>'fromName',
            'header'=>Yii::t('admin','Отправитель'),
            'htmlOptions'=>array('style'=>'width:50px;font-size:0.9em;'),
        ),
        array('name'=>'date',
            'header'=>Yii::t('admin','Дата'),
            'htmlOptions'=>array('style'=>'width:45px;font-size:0.9em;'),
        ),
        array('name'=>'message',
            'header'=>Yii::t('admin','Сообщение'),
            'type'=>'html',
            'htmlOptions'=>array('style'=>'width:auto;'),
        ),
    ),
));
?>
        </div>
</div>
</div>
<hr/>
<div>
<h3><?=Yii::t('admin','Ваши клиенты')?></h3>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'users-grid-desktop',
    'dataProvider'=>$userAsManager->users->search(20),
    'filter'=>$userAsManager->users,
    'type'=>'striped bordered condensed',
    'template'=>'{summary}{items}{pager}',
    'columns'=>array(
        array(
            'type' => 'raw',
            'filter'=>false,
            'name' => 'uid',
            'value' => 'CHtml::link($data->uid, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->email."\");return false;"))',
        ),
        'firstname',
        'lastname',
        array(
            'type' => 'raw',
            'name' => 'email',
            'value' => 'CHtml::link($data->email, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль пользователя"),"onclick"=>"getContent(this,\"".$data->email."\");return false;"))',
        ),
        array(
            'name'=>'status',
            'class' => 'CCheckBoxColumn',
            'checked'=>'$data->status==1',
            'header'=>Yii::t('admin','Вкл.'),
            //'disabled'=>'true',
            'selectableRows'=>0,
        ),
        'paymentsSUM',
        'ordersCNT',
        'ordersLast',
        /*
        'created',
        'patroname',
        'role',
        'lastname',
        'country',
        'city',
        'index',
        'address',
        'phone',
        'alias',
        'birthday',
        */
    ),
));
?>
</div>
<div>
<h3><?=Yii::t('admin','История Вашего счета')?></h3>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => 'payment-grid-desktop',
    'dataProvider' => $userAsManager->payments->search(20),
    'filter'       => $userAsManager->payments,
    'type'         => 'striped bordered condensed',
    'template'     => '{summary}{items}{pager}',
    'columns'      => array(
        'id',
        'sum',
        'description',
        array(
            'name'   => 'status',
            'filter' => array(
                1 => Yii::t('admin', 'Зачисление или возврат средств') . '(1)',
                2 => Yii::t('admin', 'Снятие средств') . '(2)',
                3 => Yii::t('admin', 'Ожидание зачисления средств') . '(3)',
                4 => Yii::t('admin', 'Отмена ожидания зачисления средств') . '(4)',
                5 => Yii::t('admin', 'Отправка внутреннего перевода средств') . '(5)',
                6 => Yii::t('admin', 'Получение внутреннего перевода средств') . '(6)',
            ),
            'value'  => '$data->text_status." (".$data->status.")"',
        ),
        array(
            'type'  => 'raw',
            'name'  => 'date',
            'value' => 'date("Y-m-d H:i:s", $data->date)',
        ),
    ),
));
?>
</div>