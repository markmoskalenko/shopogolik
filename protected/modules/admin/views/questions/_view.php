<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_view.php">
* </description>
**********************************************************************************************************************/?>
<?
$q_satuses = array(
                1   =>  Yii::t('admin','На рассмотрении'),
                2   =>  Yii::t('admin','Получен ответ'),
                3   =>  Yii::t('admin','Закрыто'),
);

$category_values = array(
    1 => Yii::t('admin','Общие вопросы'),
    2 => Yii::t('admin','Вопросы по моему заказу'),
    3 => Yii::t('admin','Рекламация'),
    4 => Yii::t('admin','Возврат денег'),
    5 => Yii::t('admin','Оптовые заказы'),
);


?>

<div class="view">

    <b><?=Yii::t('admin','Номер')?>:</b> Q0000<?=CHtml::encode($data->id)?><br />
    <b><?=Yii::t('admin','Категория')?>:</b> <?=CHtml::encode($category_values[$data->category])?>
    <b><?=Yii::t('admin','Тема')?>:</b> <?=$data->theme?>
    <?if(!empty($data->file)){?>
        <b><?=Yii::t('admin','Файл')?>:</b> <a href="/upload/<?=$data->file?>"><?=$data->file?></a>
    <?}?>

    <b><?=Yii::t('admin','Дата обращения')?>:</b>
    <?=date("d.m.Y H:i",$data->date)?>
    <br />

    <b><?=Yii::t('admin','Дата последнего изменения')?>:</b>
    <?=$data->date_change ? date("d.m.Y H:i",$data->date_change) : date("d.m.Y H:i",$data->date)?>
    <br />

    <b><?=Yii::t('admin','Тема обращения')?>:</b>
    <?php echo CHtml::encode($data->theme); ?>
    <br />

    <b><?=Yii::t('admin','Статус')?>:</b>
    <?php echo $q_satuses[$data->status]?>
    <br />

    <?if($data->order_id){?>
        <b><?=Yii::t('admin','Номер заказа')?>:</b>
        <?php echo $data->order_id?>
        <br />
    <?}?>

    <a href="<?=Yii::app()->createUrl('/admin/questions/view',array('id'=>$data->id))?>" onclick="getContent(this,'<?=Yii::t('main','Вопрос')?> №'+<?=$data->id?>); return false;"><?=Yii::t('admin','просмотр')?></a>
</div>