<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_view.php">
* </description>
**********************************************************************************************************************/?>
<div class="view<?=($data->status == 3)?' answer':''?>">
    <b>E-mail:</b> <?=$data->email?><br />
    <?=date("d.m.Y H:i:s",$data->date)?>
    <? if($data->uid!==FALSE) { ?>, <?=Yii::t('admin','пользователь')?> <?=(isset($data->u->firstname))?$data->u->firstname.' '.$data->u->lastname:Yii::t('admin','без регистрации')?>
    <?=Yii::t('admin','написал')?>:<? } ?>
    <hr />
    <p><?=$data->question?></p>

<? if($data->status == 1) { ?>

<div class="view answer">
    <div class="form">
        <form action="<?=$this->createUrl('/admin/questions/save')?>" id="message-answer-<?=$data->id?>" method="post">
            <div class="row">
                <label><?=Yii::t('admin','Ответ')?>:</label>
                <?=CHtml::textArea('Message[question]')?>
                <?=CHtml::hiddenField('Message[id]',$data->id)?>
                <?=CHtml::hiddenField('Message[qid]',$data->qid)?>
            </div>
            <div class="row buttons">
              <?    $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                //'id'=>'sub2',
                'type'=>'primary',
                'icon'=>'ok white',
                'label'=>Yii::t('admin','Ответить'),
                'htmlOptions'=>array('onclick'=>'saveQuestionForm('.$data->id.'); return false;'),
              ));
              ?>
            </div>
        </form>
    </div>
</div>
<? }  /* elseif(!empty ($answers[$data->id])) { ?>
<?php $answer =  $answers[$data->id]; ?>
<div class="view answer">
    <p><?=Yii::t('admin','Ответ')?>:</p>
    <b>E-mail:</b> <?=$answer->email?><br />
    <?=date("d.m.Y H:i:s",$answer->date)?>
    <? if($answer->uid!==FALSE){?>, <?=Yii::t('admin','пользователь')?>: <?=$answer->user->firstname.' '.$answer->user->lastname?>
      <?=Yii::t('admin','написал')?>:<? } ?>
    <hr />
    <p><?=$answer->question?></p>
</div>
<? } */ ?>

</div>