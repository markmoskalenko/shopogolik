<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	Yii::t('admin','Вопросы'),
);
$q_satuses = array(
  1   =>  Yii::t('admin','На рассмотрении'),
  2   =>  Yii::t('admin','Получен ответ'),
  3   =>  Yii::t('admin','Закрыто'),
);

$category_values = array(
  1 => Yii::t('admin','Общие вопросы'),
  2 => Yii::t('admin','Вопросы по моему заказу'),
  3 => Yii::t('admin','Рекламация'),
  4 => Yii::t('admin','Возврат денег'),
  5 => Yii::t('admin','Оптовые заказы'),
);
?>
<h1><?=Yii::t('admin','Вопрос в службу поддержки')?></h1>
<b><?=Yii::t('admin','Номер')?>:</b> Q0000<?=CHtml::encode($question->id)?><br />
<b><?=Yii::t('admin','Категория')?>:</b> <?=CHtml::encode($category_values[$question->category])?>
<h3></h3><b><?=Yii::t('admin','Тема обращения')?>:</b>
<?php echo CHtml::encode($question->theme); ?></h3>
<br />
<b><?=Yii::t('admin','Дата обращения')?>:</b>
<?=date("d.m.Y H:i",$question->date)?>
<br />

<b><?=Yii::t('admin','Дата последнего изменения')?>:</b>
<?=$question->date_change ? date("d.m.Y H:i",$question->date_change) : date("d.m.Y H:i",$question->date)?>
<br />

<b><?=Yii::t('admin','Статус')?>:</b>
<?php echo $q_satuses[$question->status]?>
<br />

<?if($question->order_id){?>
    <b><?=Yii::t('admin','Номер заказа')?>:</b>
    <?php echo $question->order_id?>
    <br />
<?}?>

<div style="float: left; width: 100%;">
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$messages,
        'itemView'=>'_viewForm',
        'id'=>'questions-listview-'.$question->id,
        'ajaxUpdate'=>true,
      )); ?>
</div>