<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<div style="width:100%;">
<div class="summary"><?=Yii::t('admin','Состояние кэша')?>:</div>
    <div class="row-fluid">
        <div class="span3">
            <label><strong><?=Yii::t('admin','Путь к кэшу')?>:</strong></label>
            <?=Yii::app()->fileCache->cachePath?>
            <?$cachedata=Cache::getDirectorySize(Yii::app()->fileCache->cachePath); ?>
        </div>
        <div class="span3">
            <label><strong><?=Yii::t('admin','Количество файлов')?>:</strong></label>
            <?=$cachedata['count'];?>
        </div>
        <div class="span3">
            <label><strong><?=Yii::t('admin','Занято кэшем на диске')?>:</strong></label>
            <?=Cache::sizeFormat($cachedata['size']);?>
        </div>
        <div class="span3">
            <label><strong><?=Yii::t('admin','Свободно на диске')?>:</strong></label>
            <?=Cache::sizeFormat($cachedata['free']);?>
        </div>
    </div>
<hr/>
  <div>
    <?    $this->widget('bootstrap.widgets.TbButton', array(
      'buttonType'=>'submit',
      //'id'=>'sub2',
      'type'=>'danger',
      'icon'=>'remove white',
      'label'=>Yii::t('admin','Очистить старый кэш'),
      'htmlOptions'=>array('class'=>'btn-block','onclick'=>'clearCache(this); return false;','formaction'=>Yii::app()->createUrl('/admin/cache/clear',array('all'=>false))),
    ));
    ?>
    <?    $this->widget('bootstrap.widgets.TbButton', array(
      'buttonType'=>'submit',
      //'id'=>'sub2',
      'type'=>'danger',
      'icon'=>'remove white',
      'label'=>Yii::t('admin','Очистить весь кэш'),
      'htmlOptions'=>array('class'=>'btn-block','onclick'=>'clearCache(this); return false;','formaction'=>Yii::app()->createUrl('/admin/cache/clear',array('all'=>true))),
    ));
    ?>
  </div>
</div>