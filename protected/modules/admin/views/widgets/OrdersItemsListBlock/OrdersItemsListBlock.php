<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersListBlock.php">
* </description>
**********************************************************************************************************************/?>
<div id="<?= $idPrefix . $type ?>">
  <h3><a name="<?= $idPrefix . $type ?>"><?= Yii::t('admin',$name) ?></a></h3>
  <?
  try {
    $columns = array();

    $columns[] = array(
      'type'  => 'raw',
      'name'  => 'id',
      'value' => 'CHtml::link($data->oid."-".$data->id, array("ordersItems/view", "id"=>$data->id), array("title"=>Yii::t("admin","Профиль лота"),"onclick"=>"getContent(this,\"' . Yii::t("admin", "Лот") . '\"+\" \"+\"$data->oid\"+\"-\"+\"$data->id\");return false;"))."&nbsp;".
          CHtml::link("<span class=\"icon-zoom-in\" style=\"display:inline-block;cursor: pointer;\"></span>",
            array("ordersItems/view", "id"=>$data->id), array("class"=>"orderItem_open","title"=>Yii::t("admin","Профиль лота"),"onclick"=>"getContent(this,\"' . Yii::t("admin", "Лот") . '\"+\" \"+\"$data->oid\"+\"-\"+\"$data->id\");return false;"))',
    );

      $columns[] = array(
        'type'  => 'raw',
        'name'  => 'oid',
        'value' => 'CHtml::link($data->uid."-".$data->oid, array("orders/view", "id"=>$data->oid), array("title"=>Yii::t("admin","Профиль заказа"),"onclick"=>"getContent(this,\"' . Yii::t("admin", "Заказ") . '\"+\" \"+\"$data->uid\"+\"-\"+\"$data->oid\");return false;"))."&nbsp;".
          CHtml::link("<span class=\"icon-zoom-in\" style=\"display:inline-block;cursor: pointer;\"></span>",
            array("orders/view", "id"=>$data->oid), array("class"=>"order_open","title"=>Yii::t("admin","Профиль заказа"),"onclick"=>"getContent(this,\"' . Yii::t("admin", "Заказ") . '\"+\" \"+\"$data->uid\"+\"-\"+\"$data->oid\");return false;"))',
      );

      $columns[] = array(
        'header'      => Yii::t('admin', 'Товар'),
        'type'        => 'raw',
        'value'       => 'OrdersItems::getOrderItemPreview($data->id,"_60x60.jpg")',
        'htmlOptions' => array('style' => 'width:65px;height:65px;padding:5px;')
      );

    $columns[] = array(
      'name'   => 'status',
      'filter' => OrdersItemsStatuses::getStatusListForFilter(),
      'value'  => function ($data, $row) {
          return Yii::t('admin',$data->statusname);
        }
    );

    $columns[] = array('name' => 'textdate');
    if (!$narrowView) {
      $columns[] = array(
        'header' => Yii::t('admin', 'Шт.'),
        'value'  => '$data->num',
      );
    }
    $columns[] = array(
      'name'   => 'ordr_user_email',
      'type'   => 'raw',
      'header' => Yii::t('main','Клиент'),
      'value'  => 'CHtml::link($data->ordr_user_email." ID:".$data->uid, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль клиента"),"onclick"=>"getContent(this,\"$data->ordr_user_email\");return false;"))."<br/>".
          CHtml::link("<span class=\"icon-zoom-in\" style=\"display:inline-block;cursor: pointer;\"></span>",
            array("users/view", "id"=>$data->uid), array("class"=>"order_open","title"=>Yii::t("admin","Профиль клиента"),"onclick"=>"getContent(this,\"$data->ordr_user_email\");return false;"))',
    );
    if (!$narrowView) {
      $columns[] = array(
        'type'   => 'raw',
        'name'   => 'address',
        'header' => Yii::t('admin', 'Получатель'),
        'value'  => '$data->firstname. " ".$data->lastname.", ".$data->country.", ".$data->city.", ".$data->address." ".$data->phone',
      );
    }

    if (!$narrowView) {
      $columns[] = array(
        'type'  => 'raw',
        'name'  => 'ordr_operator_email',
        'value' => function ($data) {
            $res = CHtml::link($data->ordr_operator_email,
              array(
                "users/view",
                "id" => $data->mid
              ),
              array(
                "title"   => Yii::t('admin', "Профиль оператора"),
                "onclick" => "getContent(this,\"" . $data->ordr_operator_email . "\");return false;"
              )
            );
            return $res;
          },
      );
    }
    /*      array(
            'type'=>'raw',
            'value'=>'"
                  <a href=\'/admin/orders/index/type/".$data[\'value\']."\' onclick=\'getContent(this,\"".Yii::t(\'admin\',\'Заказы\').\': \'.Yii::t(\'admin\', $data[\'name\'])."\"); return false;\' class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
                 "',
            'htmlOptions'=>array('style'=>'width:40px;')
          ),
    */

  } catch (Exception $e) {
    LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
    //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
    echo $e['message']; //die;
    $columns = array();
  }
  //-------------------------------------------
  /*    bootstrap.widgets.TbGridView
       'type'=>'striped bordered condensed',
        'template'=>'{summary}{pager}{items}{pager}',
   */
  try {
    $this->widget('bootstrap.widgets.TbGridView', array(
      'id'           => $idPrefix . 'ordersItems-grid-' . $type,
      'dataProvider' => $dataProvider,
      'filter'       => $filter,
      'type'         => 'striped bordered condensed',
      'template'     => '{summary}{items}{pager}',
      'columns'      => $columns,
    ));
  } catch (Exception $e) {
    echo print_r($e,true);
    if (isset($e->type) && isset($e->source) && isset($e->trace)) {
    LogSiteErrors::logError($e->type . ' ' . $e->source, '', $e->trace);
    }
    //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
  }
  ?>
</div>