<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CatTreeNodeBlock.php">
* </description>
**********************************************************************************************************************/?><div class="cat-tree-node" id="cat-tree-node-<?=$nodeData['pkid']?>">
<table>
  <tr>
    <td align="left" style="width: 150px;">
      <a href="javascript:void(0);" onclick="renderView('<?=$nodeData['pkid']?>')" class="btn btn-small view"><i class="icon-eye-open"></i></a>
      <a href="javascript:void(0);" onclick="renderUpdateForm('<?=$nodeData['pkid']?>')" class="btn btn-small view"><i class="icon-pencil"></i></a>
      <a href="javascript:void(0);" onclick="delete_record('<?=$nodeData['pkid']?>')"   class="btn btn-small view"><i class="icon-trash"></i></a>
        <? if (($nodeData['cid']>0) || $nodeData['query']) { ?>
        <a class="btn btn-small view" target="_blank" href="http://list.taobao.com/itemlist/default.htm?cat=<?=$nodeData['cid']?>&q=<?=urlencode($nodeData['query'])?>&_input_charset=utf-8">
            <i title="Taobao.com" class="icon-external-link"></i>
        </a>
        <? } ?>
    </td>
    <td align="left" style="width: 350px;">
      <? $text=$nodeData[$lang];
      if ($nodeData['status']!=1) {
       $text='<i>'.$text.'</i>';
       }
      if ($nodeData['manual']==1) {
        $text='<b>'.$text.'</b>';
      } ?>
      <div id="cat-tree-node-text-<?=$nodeData['pkid']?>"><?=$text;?></div>
    </td>
    <td align="left">
      <strong>№:</strong> <?=$nodeData['order_in_level']?>&nbsp;
      <strong>id:</strong> <?=$nodeData['pkid']?>&nbsp;
      <strong>cid:</strong> <?=$nodeData['cid']?>&nbsp;
      <? if ($nodeData['query']) {?>
      <strong>q:</strong> <?=$nodeData['query']?>&nbsp;
      <? } ?>
    </td>
  </tr>
</table>
</div>
