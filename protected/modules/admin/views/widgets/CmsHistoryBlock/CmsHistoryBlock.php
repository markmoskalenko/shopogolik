<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AdminNewsBlock.php">
* </description>
**********************************************************************************************************************/?>
<div class="cms-history-block">
  <?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'grid-'.$id,
    'dataProvider' => $dataProvider,
    'type' => 'striped bordered condensed',
    'template' => '{items}{pager}', //{summary}{pager}
    'columns' => array(
/*      array('name'=>'username',
            'htmlOptions'=>array('style'=>'width:50px;font-size:0.9em;'),
      ),
      array('name'=>'date',
            'htmlOptions'=>array('style'=>'width:45px;font-size:0.9em;'),
      ),
*/
      array('name'=>'date',
            'header'=>Yii::t('admin','История'),
            'type'=>'raw',
            //'htmlOptions'=>array('style'=>'width:auto;'),
            'value' => function ($data) {
              $res='<a href="#" title="'.Yii::t('admin','Восстановить').'" onclick="cmsHistoryRestore('.$data->id.'); return false;">'.$data->date.'</a>';
              return $res;
            },
      ),
    ),
  ));
  ?>
</div>