<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ReportBlock.php">
* </description>
**********************************************************************************************************************/?>
<h2><?=$report->name?></h2>
  <? if (in_array($report->type,array('DEFAULT','TABLE'))) { ?>
<?
    $gridViewProps=array(
      'id' => 'grid-' . $report->internal_name,
      'dataProvider' => $report->data,
      'type' => 'striped bordered condensed',
      'template' => '{summary}{items}{pager}', //{summary}{pager}
    );
    if ($report->viewProps) {
      $gridViewProps['columns']=$report->viewProps;
    }
    $this->widget('bootstrap.widgets.TbGridView', $gridViewProps);
?>
<?  } elseif (in_array($report->type,array('CHART'))) { ?>
<?
    $this->widget(
      'chartjs.widgets.'.$report->viewProps,$report->dataProps);
?>
<?  } ?>

