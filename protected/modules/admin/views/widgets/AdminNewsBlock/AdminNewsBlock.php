<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AdminNewsBlock.php">
* </description>
**********************************************************************************************************************/?>
<div class="comment-block" style="top:-20px;">
  <div class="comment-btn">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
      'buttonType'=>'submit',
      'type'=>'primary',
      'icon'=>'ok white',
      'label'=>Yii::t('admin','Создать'),
      'htmlOptions'=>array('class'=>'btn btn-info btn-mini btn-primary','onclick'=>'$("#new-message-'.$id.'").dialog("open");false;'),
    ));
    ?>
  </div>
  <?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'grid-'.$id,
    'dataProvider' => $dataProvider,
    'type' => 'striped bordered condensed',
    'template' => '{items}{pager}', //{summary}{pager}
    'columns' => array(
/*      array('name'=>'username',
            'htmlOptions'=>array('style'=>'width:50px;font-size:0.9em;'),
      ),
      array('name'=>'date',
            'htmlOptions'=>array('style'=>'width:45px;font-size:0.9em;'),
      ),
*/
      array('name'=>'message',
            'type'=>'html',
            'htmlOptions'=>array('style'=>'width:auto;'),
            'value' => function ($data) {
              $res='<strong>'.$data->username.'&nbsp;&nbsp;'.$data->date.'</strong><br/>'.$data->message;
              return $res;
            },
      ),
    ),
  ));
  ?>
</div>
<div id="new-message-<?=$id?>" title="<?=Yii::t('main','Новое сообщение')?>" style="display: none;">
      <form id="new-message-form-<?=$id?>">
        <input type="hidden" name="AdminNews[uid]" value="<?=Yii::app()->user->id?>" />
        <textarea cols="80" rows="3" name="AdminNews[message]" id="message-<?=$id?>"></textarea>
      </form><br/>
      <button id="change" onclick="
        $('#new-message-<?=$id?>').dialog('close');
        var msg = $('#new-message-form-<?=$id?>').serialize();
        $.post('/admin/adminNews/create',msg, function(){
        $('#message-<?=$id?>').val('');
        $.fn.yiiGridView.update('grid-<?=$id?>');
        },'text');
       return false;"><?=Yii::t('admin','Сохранить')?></button>
      &nbsp;&nbsp;
      <button id="cancel" onclick="$('#new-message-<?=$id?>').dialog('close');
        $('#message-<?=$id?>').val('');
        return false;"><?=Yii::t('admin','Отмена')?></button>
      <br><br>
</div>
<script type="text/javascript">
  $("#new-message-<?=$id?>").dialog({
    autoOpen: false,
    width: "auto",
    modal: true,
    resizable: false
  });
</script>

