<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersListBlock.php">
* </description>
**********************************************************************************************************************/?>
<div id="<?= $idPrefix . $type ?>">
  <h3><a name="<?= $idPrefix . $type ?>"><?= Yii::t('admin',$name) ?></a></h3>
  <?
  try {
    $columns = array();
    $columns[] = array(
      'type'  => 'raw',
      'name'  => 'id',
      'value' => 'CHtml::link($data->uid."-".$data->id, array("orders/view", "id"=>$data->id), array("title"=>Yii::t("admin","Профиль заказа"),"onclick"=>"getContent(this,\"' . Yii::t("admin", "Заказ") . '\"+\" \"+\"$data->uid\"+\"-\"+\"$data->id\");return false;"))."&nbsp;".
          CHtml::link("<span class=\"icon-zoom-in\" style=\"display:inline-block;cursor: pointer;\"></span>",
            array("orders/view", "id"=>$data->id), array("class"=>"order_open","title"=>Yii::t("admin","Профиль заказа"),"onclick"=>"getContent(this,\"' . Yii::t("admin", "Заказ") . '\"+\" \"+\"$data->uid\"+\"-\"+\"$data->id\");return false;"))',
    );
    $columns[] = array(
      'name'   => 'status',
      'filter' => OrdersStatuses::getStatusListForFilter(),
      'value'  => function ($data, $row) {
          if ($data->extstatusname) {
            $extstatus = " (" . $data->extstatusname . ")";
          }
          else {
            $extstatus = "";
          }
          return Yii::t('admin',$data->statusname) . Yii::t('admin',$extstatus);
        }
    );

    $columns[] = array('name' => 'textdate');
    if (!$narrowView) {
      $columns[] = array(
        'header' => Yii::t('admin', 'Товаров / шт.'),
        'value'  => '$data->goods_count." / ".$data->items_count',
      );
    }
    $columns[] = array(
      'header'      => Yii::t('admin', 'Товары'),
      'type'        => 'raw',
      'value'       => 'Order::getOrderItemsPreview($data->id,"_60x60.jpg",3,$data->items_legend)',
      'htmlOptions' => array('style' => 'min-width:196px;height:96px;padding:5px;')
    );
    if (!$narrowView) {
      $columns[] = array(
        'name'   => 'weight',
        'filter' => FALSE
      );
    }
    $columns[] = array(
        'header' => Yii::t('admin', 'Служба'),
        'name' => 'delivery_id');
    $columns[] = array(
        'name'   => 'delivery',
        'filter' => false,
        'value'=>'(is_numeric($data->manual_delivery)&& $data->manual_delivery>=0)?$data->manual_delivery:$data->delivery',
    );
    $columns[] = array(
        'header' => Yii::t('admin', 'Сумма'),
        'name'   => 'sum',
        'filter' => false,
        'value'=>'(is_numeric($data->manual_sum)&& $data->manual_sum>=0)?$data->manual_sum:$data->sum',
    );
    $columns[] = array(
      'header' => Yii::t('admin', 'Оплачено'),
      'name'   => 'id',
      'filter' => false,
      'value'=>'OrdersPayments::getPaymentsSumForOrder($data->id)',
    );
    $columns[] = array(
      'name'   => 'topay',
      'filter' => false,
    );
      $columns[] = array(
        'name'   => 'items_payment',
        'filter' => false,
      );
    $columns[] = array(
      'name'   => 'ordr_user_email',
      'type'   => 'raw',
      'header' => Yii::t('main','Клиент'),
      'value'  => 'CHtml::link($data->ordr_user_email." ID:".$data->uid, array("users/view", "id"=>$data->uid), array("title"=>Yii::t("admin","Профиль клиента"),"onclick"=>"getContent(this,\"$data->ordr_user_email\");return false;"))."<br/>".
          CHtml::link("<span class=\"icon-zoom-in\" style=\"display:inline-block;cursor: pointer;\"></span>",
            array("users/view", "id"=>$data->uid), array("class"=>"order_open","title"=>Yii::t("admin","Профиль клиента"),"onclick"=>"getContent(this,\"$data->ordr_user_email\");return false;"))',
    );
    if (!$narrowView) {
      $columns[] = array(
        'type'   => 'raw',
        'name'   => 'address',
        'header' => Yii::t('admin', 'Получатель'),
        'value'  => '$data->firstname. " ".$data->lastname.", ".$data->country.", ".$data->city.", ".$data->address." ".$data->phone',
      );
    }
    if (!$narrowView) {
      $columns[] = array(
        'type'  => 'raw',
        'name'  => 'ordr_operator_email',
        'value' => function ($data) {
            $res = CHtml::link($data->ordr_operator_email,
              array(
                "users/view",
                "id" => $data->manager
              ),
              array(
                "title"   => Yii::t('admin', "Профиль оператора"),
                "onclick" => "getContent(this,\"" . $data->ordr_operator_email . "\");return false;"
              )
            );
            return $res;
          },
      );
    }
    /*      array(
            'type'=>'raw',
            'value'=>'"
                  <a href=\'/admin/orders/index/type/".$data[\'value\']."\' onclick=\'getContent(this,\"".Yii::t(\'admin\',\'Заказы\').\': \'.Yii::t(\'admin\', $data[\'name\'])."\"); return false;\' class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
                 "',
            'htmlOptions'=>array('style'=>'width:40px;')
          ),
    */

  } catch (Exception $e) {
    LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
    //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
    echo $e['message']; //die;
    $columns = array();
  }
  //-------------------------------------------
  /*    bootstrap.widgets.TbGridView
       'type'=>'striped bordered condensed',
        'template'=>'{summary}{pager}{items}{pager}',
   */
  try {
    $this->widget('bootstrap.widgets.TbGridView', array(
      'id'           => $idPrefix . 'orders-grid-' . $type,
      'dataProvider' => $dataProvider,
      'filter'       => $filter,
      'type'         => 'striped bordered condensed',
      'template'     => '{summary}{items}{pager}',
      'columns'      => $columns,
    ));
  } catch (Exception $e) {
    echo print_r($e,true);
    if (isset($e->type) && isset($e->source) && isset($e->trace)) {
    LogSiteErrors::logError($e->type . ' ' . $e->source, '', $e->trace);
    }

    //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
  }
  ?>
</div>