<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="update.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this FeaturedController */
/* @var $model Featured */


/* $this->menu=array(
	array('label'=>'List Featured', 'url'=>array('index')),
	array('label'=>'Create Featured', 'url'=>array('create')),
	array('label'=>'View Featured', 'url'=>array('view', 'id'=>$model->num_iid)),
	array('label'=>'Manage Featured', 'url'=>array('admin')),
);
*/
?>

<h1>Update Featured <?php echo $model->num_iid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>