<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_view.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this FeaturedController */
/* @var $data Featured */
?>

<!-- <div class="view"> -->

<?
  $item = new stdClass();
  $item->iid = $data['num_iid'];
  $item->post_fee = $data['express_fee'];
  $item->express_fee = $data['express_fee'];
  $item->ems_fee = $data['express_fee'];
  $resUserPrice = Formulas::getUserPrice(
    array(
      'price'=>$data['price'],
      'count'=>1,
      'deliveryFee'=>($data['express_fee'])?$data['express_fee']:0,
      'postageId'=>FALSE,
      'sellerNick'=>FALSE,
      //'asHtml' => TRUE,
    ));
 $item->userPrice=$resUserPrice->price;
 $economy=$resUserPrice->discount;
 $postFee=$resUserPrice->delivery;
  $item->pic_url = $data['pic_url'];
  $item->cat_title = $data['title_' . Utils::AppLang()];
?>

  <div class="product" id="item<?=$item->iid?>">
    <div style="text-align: right;">
    <a class="icon-remove" style="display:inline-block; cursor: pointer;"
       title="<?=Yii::t('admin','Удалить')?>" href="<?=Yii::app()->createUrl('/admin/featured/delete/',array('id'=>$item->iid))?>"
       onclick="deleteFeaturedItem(this,<?=$item->iid?>); return false;"></a>
    </div>
    <div class="product-image">
      <a target="_blank" href="<?= Yii::app()->createUrl('/item/index', array('iid' => $item->iid)) ?>">
        <img src="<?=Img::getImagePath($item->pic_url,'_160x160.jpg',false)?>"/>
      </a>
    </div>
    <div class="product-price"><?=$item->userPrice?></div>
    <div class="product-title">
      <?echo $item->cat_title?>
    </div>

  </div>


<!-- </div> -->