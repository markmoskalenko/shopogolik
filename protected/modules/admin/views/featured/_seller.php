<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_seller.php">
* </description>
**********************************************************************************************************************/?>
<?php foreach($sellers as $seller):?>
<div class="view fseller" id="seller<?=$seller['id']?>">
    <p>
        Ник:
        <strong><?=$seller['iid']?></strong>
    </p>
    <a href="<?=Yii::app()->createUrl('/admin/featured/delete/',array('id'=>$seller['id']))?>" onclick="deleteFseller(this,<?=$seller['id']?>); return false;"><?=Yii::t('admin','Удалить')?></a>
</div>
<?php endforeach ?>
