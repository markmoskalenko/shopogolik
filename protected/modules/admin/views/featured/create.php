<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="create.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this FeaturedController */
/* @var $model Featured */

$this->breadcrumbs=array(
	'Featureds'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Featured', 'url'=>array('index')),
	array('label'=>'Manage Featured', 'url'=>array('admin')),
);
?>

<h1>Create Featured</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>