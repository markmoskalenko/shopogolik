<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="admin.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this FeaturedController */
/* @var $model Featured */

$this->breadcrumbs=array(
	'Featureds'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Featured', 'url'=>array('index')),
	array('label'=>'Create Featured', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#Featured-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Featureds</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'Featured-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{pager}{items}{pager}',
	'columns'=>array(
		'num_iid',
		'date',
		'cid',
		'express_fee',
		'price',
		'promotion_price',
		/*
		'pic_url',
		'title_zh',
		'title_en',
		'title_ru',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
