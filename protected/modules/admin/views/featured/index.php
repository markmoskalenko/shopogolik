<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this FeaturedController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('admin','Рекомендованные товары'),
);

/*$this->menu=array(
	array('label'=>'Create Featured', 'url'=>array('create')),
	array('label'=>'Manage Featured', 'url'=>array('admin')),
);
*/
?>

<h1><?=Yii::t('admin','Рекомендованные товары')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$model->search(),
	'itemView'=>'_view',
    'enableSorting'=>false,
    'itemsCssClass'=>'products-list',
    'id'=>'featured-listview',
     'ajaxUpdate'=>true,

)); ?>
