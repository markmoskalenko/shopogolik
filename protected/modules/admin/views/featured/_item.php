<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_item.php">
* </description>
**********************************************************************************************************************/?><div class="view fitem" id="item<?=$item->id?>">
    <form action="<?=Yii::app()->createUrl('/admin/featured/title')?>" method="POST">
        <input type="text" size="50" name="title" value="<?=$item->title?>" />
      <?    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        //'id'=>'sub2',
        'type'=>'primary',
        'icon'=>'ok white',
        'label'=>Yii::t('admin','Сохранить'),
        'htmlOptions'=>array('onclick'=>'addFeaturedTitle(this);return false;'),
      ));
      ?>
        <input type="hidden" value="<?=$item->iid?>" name="iid" />
    </form>
    <a href="<?=Yii::app()->createUrl('/item/index',array('iid'=>$item->iid))?>" onclick="window.open(this.href);return false;">
        <img src="<?=$item->pic_url?>_40x40.jpg" alt="" />
    </a><br />
    <a href="<?=Yii::app()->createUrl('/admin/featured/delete/',array('id'=>$item->id))?>" onclick="deleteFeaturedItem(this,<?=$item->id?>); return false;"><?=Yii::t('admin','Удалить')?></a>
</div>
