<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this FeaturedController */
/* @var $model Featured */

$this->breadcrumbs=array(
	'Featureds'=>array('index'),
	$model->num_iid,
);

$this->menu=array(
	array('label'=>'List Featured', 'url'=>array('index')),
	array('label'=>'Create Featured', 'url'=>array('create')),
	array('label'=>'Update Featured', 'url'=>array('update', 'id'=>$model->num_iid)),
	array('label'=>'Delete Featured', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->num_iid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Featured', 'url'=>array('admin')),
);
?>

<h1>View Featured #<?php echo $model->num_iid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'num_iid',
		'date',
		'cid',
		'express_fee',
		'price',
		'promotion_price',
		'pic_url',
		'title_zh',
		'title_en',
		'title_ru',
	),
)); ?>
