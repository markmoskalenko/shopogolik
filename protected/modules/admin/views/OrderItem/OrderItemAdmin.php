<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrderItemAdmin.php">
* </description>
**********************************************************************************************************************/?>
<?
$edit = (Yii::app()->user->inRole(array('orderManager')) && ($order->status=='IN_PROCESS'))||
  Yii::app()->user->inRole(array('superAdmin', 'topManager'));
if ($light) {
    $edit=false;
} else { ?>
    <script type="text/javascript">
    //$("#order-item-update-form-<?=$item->oid?>").remove();
    </script>
<? } ?>
<div class="ordr-header-block" id="headerblock-item-<?=$item->id?>">

<div class="form" style="border-left: 8px solid <?="#".substr("000000".dechex($item->seller_id),-6)?> !important;">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id'                     => 'order-item-update-form-' . $item->oid.($light?'-light':''),
  'enableAjaxValidation'   => false,
  'enableClientValidation' => false,
  'method'                 => 'post',
  'action'                 => array("/admin/orders/updateitem"),
  'type'                   => 'horizontal',
  'htmlOptions' => ($light)?array():array('onsubmit'=>"return false;",),
));
?>

<h3 style="background: rgba(27, 184, 0, 0.35);padding-bottom: 5px;">
    <i class="icon-tag"></i>
    <a name="headerblock-item-<?=$item->id ?>">
        <?= Yii::t('main', 'Лот заказа') . ' ' . $item->oid . '-' . $item->id ?>
    </a>
</h3>
<input type="hidden" name="orderItemId" value="<?= $item->id ?>"/>
<input type="hidden" name="iid" value="<?= $item->iid ?>"/>
<input type="hidden" name="params" value="<?= (isset($item->props)) ? $item->props : $item->input_props ?>"/>

<div class="cart-table">
  <div class="row-fluid">
      <div class="span6 any-res"><!-- Parameters -->
      <fieldset style="margin:0 10px 10px 10px;"><h3><i class="icon-font"></i><?= Yii::t('admin', 'Название и параметры') ?></h3>
          <div class="ordr-header-block-parameters">
              <? $originalTitle=trim((preg_match('/editTranslation|<translation/s',$item->title)) ? $item->title : Yii::app()->DanVitTranslator->translateText($item->title, 'zh-CHS', Utils::TransLang()));
              if ($originalTitle!=trim($item->title)) {?>
                  <?=$originalTitle?>
                  <br/><br/>
              <? } ?>
              <?= $item->title ?>
              <div class="separator"></div>
              <div class="product-image"><!-- image -->
                  <ul class="hoverbox">
                      <li>
                          <a target="_blank" href="<?=Yii::app()->createUrl('/item/index', array('iid' => $item->iid)) ?>">
                              <? if ($lazyLoad) { ?>
                                  <img class="lazy"
                                       src="<?= Yii::app()->request->baseUrl ?>/themes/<?= Yii::app()->theme->name ?>/images/zoomloader.gif"
                                       data-original="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
                                  <noscript><img src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt=""/></noscript>
                              <?
                              }
                              else {
                                  ?>
                                  <img class="img-responsive" src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
                              <? } ?>
                              <img class="preview img-responsive" src="<?= Img::getImagePath($item->pic_url, $imageFormat) ?>" alt="" title=""/>
                          </a>
                      </li>
                  </ul>
              </div>
              <?php 
			  $input_props_array = array();
			  if (isset($item->input_props_array)) {
                  $input_props_array = $item->input_props_array;
              }
              else {
                  $input_props_array = json_decode($item->input_props);
              }
              if (!isset($input_props_array) || !$input_props_array || ($input_props_array == null)) {
                  $input_props_array = array();
              }
              foreach ($input_props_array as $name => $value) {
                  ?>
                  <strong><?= (preg_match('/editTranslation|<translation/s',$value->name)) ? 
				  $value->name
				  : 
				  Yii::app()->DanVitTranslator->translateText($value->name, 'zh-CHS', Utils::TransLang()); ?></strong>:
                  <?= (preg_match('/editTranslation|<translation/s',$value->value)) ? $value->value : Yii::app()->DanVitTranslator->translateText($value->value, 'zh-CHS', Utils::TransLang()); ?>
                  <? if (isset($value->name_zh) && isset($value->value_zh)) { ?><br/>
                      <span style="font-weight: 700;line-height: 2em;">&nbsp;</span>
                      <strong><?= $value->name_zh; ?>:</strong> <?= $value->value_zh; ?><br/>
                  <? } ?>
              <? } ?>
          </div>
      </fieldset>
  </div>
      <div class="span4 any-res">
      <fieldset style="margin:0 10px 10px 10px;">
      <h3><i class="icon-gift"></i><?=Yii::t('admin','Вес, количество и стоимость')?></h3>
          <table border="0" cellspacing="1" cellpadding="1" style="width:95%;margin:10px 3%;">
          <tr>
            <th scope="col">&nbsp;</th>
            <th scope="col"><?= Yii::t('admin', 'В заказе') ?></th>
            <th scope="col"><?= Yii::t('admin', 'По факту') ?></th>
            <th scope="col"><?= Yii::t('admin', 'В расчете') ?></th>
          </tr>
          <tr>
            <td class="font-block" scope="row"><?= Yii::t('admin', 'Стоимость  закупки 1 шт') ?></td>
            <td>
              <? if (($item->taobao_promotion_price < $item->taobao_price) && ($item->taobao_promotion_price > 0)) {
                $declaredPrice = $item->taobao_promotion_price;
                echo $form->TextField($item, 'taobao_promotion_price', array(
                  'readonly' => true,
                  'title'    => Yii::t('admin', 'Заявленная при оформлени заказа стоимость 1 единицы товара (скидка)')
                ));
              }
              else {
                $declaredPrice = $item->taobao_price;
                echo $form->textField($item, 'taobao_price', array(
                  'readonly' => true,
                  'title'    => Yii::t('admin', 'Заявленная при оформлени заказа стоимость 1 единицы товара')
                ));
              }
              ?>
            </td>
            <td title="<?=$item->varReport('calculated_actualPrice')?>"><? echo $form->textField($item, 'calculated_actualPrice', array(
                'readonly' => true,
              ));?>
            </td>
            <td class="table-select"><?=sprintf('%01.2f',round($declaredPrice-$item->calculated_actualPrice,2))?></td>
          </tr>
          <tr>
              <td scope="row"><?= Yii::t('admin', 'Вес 1  шт') ?></td>
              <td>
                <? echo $form->textField($item, 'weight', array(
                    'readonly' => true,
                    'title'    => Yii::t('admin', 'Заявленный при оформлени заказа вес 1 единицы товара, в граммах')
                )); ?>
                </td>
              <td title="<?=$item->varReport('calculated_actualWeight')?>"><? echo $form->textField($item, 'calculated_actualWeight', array(
                  'readonly' => true,
                )); ?>
              </td>
              <td class="table-select"><?=round($item->weight-$item->calculated_actualWeight)?></td>
          </tr>
          <tr>
            <td scope="row"><?= Yii::t('admin', 'Доставка  по Китаю 1 шт') ?></td>
            <td><? echo $form->textField($item, 'express_fee', array(
                'readonly' => true,
                'title'    => Yii::t('admin', 'Заявленная при оформлени заказа стоимость доставки 1 ед по Китаю')
              )); ?></td>
            <td title="<?=$item->varReport('calculated_actualExpressFee')?>"><? echo $form->textField($item, 'calculated_actualExpressFee', array(
                'readonly' => true,
              )); ?>
            </td>
            <td class="table-select"><?=sprintf('%01.2f',round($item->express_fee-$item->calculated_actualExpressFee,2))?></td>
          </tr>
          <tr>
            <td scope="row"><?= Yii::t('admin', 'Кол-во  товаров в лоте') ?></td>
            <td><? echo $form->textField($item, 'num', array(
                'readonly' => true,
                'title'    => Yii::t('admin', 'Заявленное при оформлени заказа количество единиц товара в лоте')
              )); ?></td>
            <td><? echo $form->textField($item, 'actual_num', array(
                'readonly' => !$edit,
                'title'    => Yii::t('admin', 'Актуальное количество товаров в лоте, которое закупается')
              )); ?>
            </td>
            <td class="table-select"><?=($item->num==$item->actual_num)?$item->num:round($item->actual_num-$item->num)?></td>
          </tr>
          <tr>
            <td scope="row"><?= Yii::t('admin', 'Цена  лота') ?></td>
            <td title="<?=$item->varReport('calculated_lotPrice')?>"><? echo $form->textField($item, 'calculated_lotPrice', array(
                'readonly' => true,
              )); ?></td>
            <td><? echo $form->textField($item, 'actual_lot_price', array(
                'readonly' => !$edit,
                'title'    => Yii::t('admin', 'Актуальная стоимость закупки всего лота')
              )); ?>
            </td>
            <td class="table-select"><?=sprintf('%01.2f',round($item->calculated_lotPrice-$item->actual_lot_price,2))?></td>
          </tr>
          <tr>
            <td scope="row"><?= Yii::t('admin', 'Вес  лота') ?></td>
            <td title="<?=$item->varReport('calculated_lotWeight')?>"><? echo $form->textField($item, 'calculated_lotWeight', array(
                'readonly' => true,
              )); ?></td>
            <td><? echo $form->textField($item, 'actual_lot_weight', array(
                'readonly' => !$edit,
                'title'    => Yii::t('admin', 'Актуальный вес всего лота на складе')
              )); ?>
            </td>
            <td class="table-select"><?=round($item->calculated_lotWeight-$item->actual_lot_weight)?></td>
          </tr>
          <tr>
            <td scope="row"><?= Yii::t('admin', 'Доставка  лота') ?></td>
            <td  title="<?=$item->varReport('calculated_lotExpressFee')?>"><? echo $form->textField($item, 'calculated_lotExpressFee', array(
                'readonly' => true,
              )); ?></td>
            <td><? echo $form->textField($item, 'actual_lot_express_fee', array(
                'readonly' => !$edit,
                'title'    => Yii::t('admin', 'Актуальная стоимость доставки по Китаю всего лота')
              )); ?>
            </td>
            <td class="table-select"><?=sprintf('%01.2f',round($item->calculated_lotExpressFee-$item->actual_lot_express_fee,2))?></td>
          </tr>

                <? if ((DSConfig::getVal('billing_use_operator_account')==1)&&($item->calculated_operatorProfit!=0)
                ) { ?>
          <tr>
            <td scope="row"><?= Yii::t('admin', 'Возможный бонус менеджера') ?></td>
            <td title="<?=$item->varReport('calculated_operatorProfit')?>">
                <? echo $form->textField($item, 'calculated_operatorProfit', array(
                  'readonly' => true,
              )); ?>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
                  <? } ?>
        </table>


        </fieldset>
      </div>
      <div class="span2 any-res">
          <fieldset style="margin:0 10px 10px 10px;">
              <h3><i class="icon-barcode"></i><?=Yii::t('admin','Идентификация и статус')?></h3>
                  <? echo $form->errorSummary($item, 'Opps!!!', null, array('class' => 'alert alert-error span12')); ?>
                          <?= $form->uneditableRow($item, 'iid', array(
                              'title' => Yii::t('admin', 'Идентификатор товара на таобао')
                          )); ?>
                          <?= $form->uneditableRow($item, 'sku_id', array('title' => Yii::t('admin', 'SKU (Stock Keeping Unit) - идентификатор товарной позиции, артикул. Код выбранного цвета, размера, комплектации'))); ?>
                          <? if (!$edit) {
                              $item->status_text=OrdersItemsStatuses::getStatusName($item->status);
                              echo $form->uneditableRow($item, 'status_text', array('title' => Yii::t('admin', 'Статус обработки лота')));
                          }
                          else {
                              echo $form->dropDownListRow($item, 'status', OrdersItemsStatuses::getOrderItemStatusesList(), array('title' => Yii::t('admin', 'Статус обработки лота')));
                          }?>
                          <? if (!$edit) {
                              echo $form->uneditableRow($item, 'tid', array('title' => Yii::t('admin', 'TID, Trade id, Order Number - идентификатор сделки по закупке лота на таобао. Связывает заказ на нашем сайте и на taobao. Может быть несколько, через запятуБЮ')));
                          }
                          else {
                              echo $form->textFieldRow($item, 'tid', array('title' => Yii::t('admin', 'TID, Trade id, Order Number - идентификатор сделки по закупке лота на таобао. Связывает заказ на нашем сайте и на taobao. Может быть несколько, через запятую')));
                          } ?>
                          <? if (!$edit) {
                              echo $form->uneditableRow($item, 'track_code', array('title' => Yii::t('admin', 'Трек-код заказа (он же out_sid, Waybill number, но не Logistics ID) - необходим для идентификации посылки при получении на склад. Может быть несколько, через запятую')));
                          }
                          else {
                              echo $form->textFieldRow($item, 'track_code', array('title' => Yii::t('admin', 'Трек-код заказа (он же out_sid, Waybill number, но не Logistics ID) - необходим для идентификации посылки при получении на склад. Может быть несколько, через запятую')));
                          }?>

<? if (!$light) { ?>
    </fieldset>
    </div>
    <div class="ordr-header-block-buttons">
    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'info', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>Yii::t('admin', 'Товар'), 'items'=>array(
                array('label'=>Yii::t('admin', 'На taobao'), 'url'=>'http://item.taobao.com/item.htm?id='.$item->iid, 'linkOptions'=>array('target'=>'_blank')),
                array('label'=>Yii::t('admin', 'На сайте'), 'url'=>Yii::app()->createUrl('/item/index', array('iid' => $item->iid)), 'linkOptions'=>array('target'=>'_blank')),
            )),
        ),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'info', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>Yii::t('admin', 'Продавец'), 'items'=>array(
                array('label'=>Yii::t('admin', 'Магазин tmall').' ('.$item->seller_nick.')', 'url'=>'"http://shopsearch.taobao.com/search?q='.$item->seller_nick, 'linkOptions'=>array('target'=>'_blank')),
                array('label'=>Yii::t('admin', 'Товары продавца'), 'url'=>'http://s.taobao.com/search?app=usersearch&user_id='.$item->seller_id, 'linkOptions'=>array('target'=>'_blank')),
            )),
        ),
    )); ?>

    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'info', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>Yii::t('admin', 'Кабинет'), 'items'=>array(
                array('label'=>Yii::t('admin','Заказ в кабинете taobao'), 'url'=>'http://trade.taobao.com/trade/detail/trade_item_detail.htm?bizOrderId='.$item->tid, 'linkOptions'=>array('target'=>'_blank')),
            )),
        ),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'info', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>Yii::t('admin', 'Печать'), 'items'=>array(
                array('label'=>Yii::t('admin','Карта лота'), 'url'=>'/admin/orders/GenerateItemReport/itemId/'.$item->id, 'linkOptions'=>array('target'=>'_blank')),
                array('label'=>Yii::t('admin','Штрих-код'), 'url'=>'/admin/orders/GenerateItemReport/itemId/'.$item->id.'/view/itemReportLite', 'linkOptions'=>array('target'=>'_blank')),
            )),
        ),
    )); ?>
    <?php /*
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'  => 'ajaxSubmit',
        'type'        => 'info',
        'size'        => 'mini',
        'icon'        => 'ok white',
        'htmlOptions' => array('style'=>'margin-left:35px;','title' => Yii::t('admin', 'Сохранить')),
        'label'       => Yii::t('admin', ''),
        'url'         => '/admin/orders/updateitem',
        'ajaxOptions' => array(
          'complete' => 'js:function(){reloadSelectedTab(); dsAlert(\''.Yii::t('admin','Сохранено').'\',\''.Yii::t('admin','Подтверждение').'\',true);}',
        ),
      ));
    */ ?>

    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'        => 'info',
        'size'        => 'large',
        'icon'        => 'ok white',
        'htmlOptions' => array('style'=>'margin-left:35px;','title' => Yii::t('admin', 'Сохранить'),'onclick'=>'update(\''.$item->oid.'\',\''.$item->id.'\');'),
        'label'       => Yii::t('admin', ''),
    ));
    ?>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'  => 'reset',
        'type'        => 'danger',
        'size'        => 'large',
        'icon'        => 'remove white',
        'htmlOptions' => array('title' => Yii::t('admin', 'Отмена')),
        'label'       => Yii::t('admin', ''),
    ));
    ?>
    </div>
<? } else { ?>
    <span class="ordr-header-block-buttons">
    <a href="http://item.taobao.com/item.htm?id=<?=$item->iid?>" target="_blank" class="btn btn-info btn-large" title="<?=Yii::t('admin','На taobao')?>" ><i class="icon-fullscreen icon-white"></i></a>
<? if ($item->seller_nick) {?>
    <a href="http://shopsearch.taobao.com/search?q=<?=$item->seller_nick?>" target="_blank" class="btn btn-info btn-large" title="<?=Yii::t('admin', 'Магазин продавца')?>" ><i class="icon-tasks icon-white"></i></a>
<? } ?>
<? if ($item->tid) {?>
    <a href="http://trade.taobao.com/trade/detail/trade_item_detail.htm?bizOrderId=<?=$item->tid?>" target="_blank" class="btn btn-info btn-large" title="<?=Yii::t('admin','Заказ в кабинете taobao')?>" ><i class="icon-shopping-cart icon-white"></i></a>
<? } ?>
    <a href="javascript:void(0);" onclick="renderUpdateForm('<?=$item->id?>','<?=$item->oid?>')" title="<?=Yii::t('admin','Редактировать')?>" class="btn btn-info btn-large"  ><i class="icon-pencil icon-white"></i></a>

<? } ?>
    </span>
      </div>
  </div>
</div>
<?php $this->endWidget(); ?>

<div class="clear"></div>

<div>
  <? if (is_a($item, 'customCart')) { ?>
  <?
  }
  else { if (!$dialog) {
    ?>
    <? $this->widget('application.components.widgets.OrderCommentsBlock', array(
      'orderId'       => FALSE,
      'orderItemId'   => $item->id,
      'showInternals' => $adminMode ? 1 : 0,
      'public'        => $publicComments,
      'pageSize'      => 5,
      'imageFormat'   => '_200x200.jpg',
    ));
    ?>
  <? }
  } ?>
</div>

</div>
<? if (!$light) { ?>
<script type="text/javascript">
    function update(oid,iid)
    {

        var data=$("#order-item-update-form-"+oid).serialize();

        jQuery.ajax({
            type: 'POST',
            url: '<?=Yii::app()->createAbsoluteUrl("admin/orders/updateItem"); ?>',
            data:data,
            success:function(data){
                if(data!="false")
                {
                    $('#orders-items-update-modal-'+oid).modal('hide');
                    // renderView(data);
                   // $("#order-item-update-form-"+oid).remove();
                    $.fn.yiiGridView.update('ordersItemsGrid-'+oid, {
                    });
                    dsAlert('<?=Yii::t('admin','Сохранено')?>','<?=Yii::t('admin','Подтверждение')?>',true);
                }

            },
            error: function(data) { // if error occured
                alert(JSON.stringify(data));

            },

            dataType:'html'
        });

    }
</script>
<? }?>