<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this FeaturedController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs=array(
	Yii::t('admin','Менеджер файлов'),
);
// Elfinder
$assetsUrl = Yii::app()->getAssetManager()->publish(
  Yii::getPathOfAlias('ext.elrte.lib'),
  false,
  -1,
  YII_DEBUG
);
$cs=Yii::app()->clientScript;
$cs->registerCssFile($assetsUrl.'/elfinder/css/elfinder.min.css');
$cs->registerCssFile($assetsUrl.'/elfinder/css/theme.css');
$cs->registerScriptFile($assetsUrl.'/elfinder/js/elfinder.min.js');
$cs->registerScriptFile($assetsUrl.'/elfinder/js/i18n/elfinder.ru.js');
?>

<h1><?=Yii::t('admin','Менеджер файлов')?></h1>
<div id="standalone-filemanager"></div>
<script type="text/javascript" charset="utf-8">
    // Documentation for client options:
    // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
    $(document).ready(function() {
        $('#standalone-filemanager').elfinder({
            url : '/admin/fileman/index'  // connector URL (REQUIRED)
            // , lang: 'ru'                    // language (OPTIONAL)
            ,resizable: false
            ,height: '1024px'
            ,width: '100%'
            ,showFiles: '2'
        });
    });
</script>