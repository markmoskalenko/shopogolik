<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_create_form.php">
* </description>
**********************************************************************************************************************/?>
    <div id='ordersstatuses-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новый статус заказа')?></h3>
    </div>
    
    <div class="modal-body">
    <div class="form">
    <?php
   
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	    'id'=>'ordersstatuses-create-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("ordersstatuses/create"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
          'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>

    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
       <div class="row-fluid">
           <div class="span6">
               <?php echo $form->labelEx($model,'value'); ?>
               <?php echo $form->textField($model,'value',array('class'=>'span12','maxlength'=>128)); ?>
               <?php echo $form->error($model,'value'); ?>

               <?php echo $form->labelEx($model,'name'); ?>
               <?php echo $form->textField($model,'name',array('class'=>'span12','maxlength'=>256)); ?>
               <?php echo $form->error($model,'name'); ?>

               <?php echo $form->labelEx($model,'manual'); ?>
               <?php echo $form->textField($model,'manual'); ?>
               <?php echo $form->error($model,'manual'); ?>

               <?php echo $form->labelEx($model,'order_in_process'); ?>
               <?php echo $form->textField($model,'order_in_process'); ?>
               <?php echo $form->error($model,'order_in_process'); ?>

               <?php echo $form->labelEx($model,'enabled'); ?>
               <?php echo $form->textField($model,'enabled'); ?>
               <?php echo $form->error($model,'enabled'); ?>

               <?php echo $form->labelEx($model,'descr'); ?>
               <?php echo $form->textArea($model,'descr',array('rows'=>6, 'class'=>'span12')); ?>
               <?php echo $form->error($model,'descr'); ?>
           </div>
           <div class="span6">
               <?php echo $form->labelEx($model,'aplyment_criteria'); ?>
               <?php echo $form->textArea($model,'aplyment_criteria',array('rows'=>6, 'class'=>'span12')); ?>
               <?php echo $form->error($model,'aplyment_criteria'); ?>

               <?php echo $form->labelEx($model,'auto_criteria'); ?>
               <?php echo $form->textArea($model,'auto_criteria',array('rows'=>6, 'class'=>'span12')); ?>
               <?php echo $form->error($model,'auto_criteria'); ?>
           </div>
       </div>
  </div><!--end div form-->
  
   </div><!--end modal footer-->
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove white',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div>
        <?php $this->endWidget(); ?>
</div><!--end modal-->

<!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#ordersstatuses-create-form").serialize();
  jQuery.ajax({
    type: 'POST',
    url: '<?php
    echo Yii::app()->createAbsoluteUrl("admin/ordersStatuses/create"); ?>',
    data:data,
    success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#ordersstatuses-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('ordersstatuses-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#ordersstatuses-create-form').each (function(){
  this.reset();
   });
  $('#ordersstatuses-view-modal').modal('hide');
  $('#ordersstatuses-create-modal').modal({
   show:true
  });
}
</script>
