<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="expenseGridtoReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      value		</th>
 		<th width="80px">
		      name		</th>
 		<th width="80px">
		      descr		</th>
 		<th width="80px">
		      manual		</th>
 		<th width="80px">
		      aplyment_criteria		</th>
 		<th width="80px">
		      auto_criteria		</th>
 		<th width="80px">
		      order_in_process		</th>
 		<th width="80px">
		      enabled		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->value; ?>
		</td>
       		<td>
			<?php echo $row->name; ?>
		</td>
       		<td>
			<?php echo $row->descr; ?>
		</td>
       		<td>
			<?php echo $row->manual; ?>
		</td>
       		<td>
			<?php echo $row->aplyment_criteria; ?>
		</td>
       		<td>
			<?php echo $row->auto_criteria; ?>
		</td>
       		<td>
			<?php echo $row->order_in_process; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
