<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Настройки'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').slideToggle('fast');
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('config-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

?>

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
	'htmlOptions'=>array(
		'class'=>''
	)
));
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
//		array('label'=>Yii::t('admin','Создать'), 'icon'=>'icon-plus', 'url'=>'javascript:void(0);','linkOptions'=>array('onclick'=>'renderCreateForm()')),
                //array('label'=>Yii::t('admin','Список'), 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('index'),'active'=>true, 'linkOptions'=>array()),
		array('label'=>Yii::t('admin','Поиск'), 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
//		array('label'=>Yii::t('admin','PDF'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GeneratePdf'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
		array('label'=>Yii::t('admin','Excel'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateExcel'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
	),
));
$this->endWidget();
?>



<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
  'id' => 'config-grid',
  'dataProvider' => $model->search(),
  'filter' => $model,
  'type' => 'striped bordered condensed',
  'template' => '{summary}{items}{pager}',
  'columns' => array(
    array(
      'name' => 'id',
      'filter' => array(
        'billing_' => Yii::t('admin', 'billing - Расчеты с менеджерами'),
        'checkout_' => Yii::t('admin', 'checkout - Оформление заказа'),
        'delivery_' => Yii::t('admin', 'delivery - Доставка'),
        'ext_' => Yii::t('admin', 'ext - Внешние сервисы'),
        'featured_' => Yii::t('admin', 'featured - Рекомендованные товары'),
        'keys_' => Yii::t('admin', 'keys - Ключи переводчика'),
        'price_' => Yii::t('admin', 'price - Ценообразование'),
        'profiler_' => Yii::t('admin', 'profiler - Профайлер'),
        'proxy_' => Yii::t('admin', 'proxy - Настройки DSProxy'),
        'rate_' => Yii::t('admin', 'rate - Курсы валют'),
        'search_' => Yii::t('admin', 'search - Поиск'),
        'seo_' => Yii::t('admin', 'seo - Поисковая оптимизация'),
        'site_' => Yii::t('admin', 'site - Общие настройки сайта'),
        'skidka_' => Yii::t('admin', 'skidka - Скидки от количества'),
        'translator_' => Yii::t('admin', 'translator - Настройки переводчика'),
        'weight_' => Yii::t('admin', 'weight - Расчет веса'),
      ),
      'type'=>'raw',
      'value' => '$data->id.Utils::getHelp($data->id)',
    ),

    array(
      'name' => 'label',
      'type' => 'raw',
      'value' => function ($data) {
                return Yii::t('admin',$data->label);
        },
    ),

    array(
      'name' => 'value',
      'type' => 'raw',
      'value' => function ($data) {
          if (mb_strlen($data->value) > 64) {
            return htmlentities(mb_substr($data->value, 0, 64) . "...", NULL, 'UTF-8');
          }
          else {
            return htmlentities($data->value, NULL, 'UTF-8');
          }
        },
    ),
    array(
      'name' => 'default_value',
      'type' => 'raw',
      'value' => function ($data) {
          if (mb_strlen($data->default_value) > 64) {
            return htmlentities(mb_substr($data->default_value, 0, 64) . "...", NULL, 'UTF-8');
          }
          else {
            return htmlentities($data->default_value, NULL, 'UTF-8');
          }
        },
    ),
//		'in_wizard',
    array(

      'type' => 'raw',
      'value' => '"
		      <a href=\'javascript:void(0);\' onclick=\"renderView(\'".$data->id."\')\"   class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
		      <a href=\'javascript:void(0);\' onclick=\"renderUpdateForm(\'".$data->id."\')\"   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
      'htmlOptions' => array('style' => 'width:150px;')
    ),

  ),
));


$this->renderPartial("_ajax_update");
$this->renderPartial("_ajax_create_form", array("model" => $model));
$this->renderPartial("_ajax_view");

 ?>

 
<script type="text/javascript"> 
function delete_record(id)
{
 
  if(!confirm("Are you sure you want delete this?"))
   return;
   
 //  $('#ajaxtest-view-modal').modal('hide');
 
 var data="id="+id;
 

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/config/delete"); ?>',
   data:data,
success:function(data){
                 if(data=="true")
                  {
                     $('#config-view-modal').modal('hide');
                     $.fn.yiiGridView.update('config-grid', {
                     
                         });
                 
                  } 
                 else
                   alert("deletion failed");
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
       //  alert(data);
    },

  dataType:'html'
  });

}
</script>

<style type="text/css" media="print">
body {visibility:hidden;}
.printableArea{visibility:visible;} 
</style>
<script type="text/javascript">
function printDiv()
{

window.print();

}
</script>
 

