<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update_form.php">
* </description>
**********************************************************************************************************************/?>
<div id='config-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
   
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Изменения параметра')?>&nbsp;<strong style="color:#0093f5;"><?php echo $model->id; ?></strong></h3>
    </div>
    
    <div class="modal-body">
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'config-update-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("config/update"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
	
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
    <div class="control-group">
        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->hiddenField($model,'id',array()); ?>
                <?php echo $form->labelEx($model,'label'); ?>
                <?php echo $form->textField($model,'label',array('class'=>'span12','maxlength'=>256,'readonly'=>'readonly')); ?>
                <?php echo $form->error($model,'label'); ?>
            </div>
			<div class="span6">
                <?php echo $form->labelEx($model,'value'); ?>
                <?php echo $form->textField($model,'value',array('class'=>'span12','rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'value'); ?>

                <?php echo $form->labelEx($model,'default_value'); ?>
                <?php echo $form->textField($model,'default_value',array('rows'=>6,'class'=>'span12', 'cols'=>80,'readonly'=>'readonly')); ?>
                <?php echo $form->error($model,'default_value'); ?>
            </div>
		</div>
    </div>
 </div>

  </div><!--end modal body-->
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            //'id'=>'sub2',
            'type'=>'info',
            'icon'=>'ok white',
            'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'update();'),
        ));
        ?>
    </div><!--end modal footer-->
<?php $this->endWidget(); ?>
</div><!--end modal-->



