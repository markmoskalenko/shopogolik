<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="syssettings.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Настройки системы'),
);

$this->menu=array(
	array('label'=>Yii::t('admin','Добавить параметр'), 'url'=>array('create')),
);
?>

<h1><?=Yii::t('admin','Настройки системы')?></h1>

<?php foreach($systemsettings as $setting) { ?>
    <a href="<?=$this->createUrl('/admin/config/update/id/'.$setting['id'])?>" onclick="getContent(this,'<?=$setting['label']?>');return false;">
     <div class="view">
     <b><?=$setting['label']?>:</b><span class="icon-pencil"></span><br>
     <?=$setting['id']?>
     <b><?=$setting['value']?></b>
    </div></a>
<?php } ?>