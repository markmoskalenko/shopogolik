<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="extupdate.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Настройки')=>array('index'),
	$settings->label=>array('view','id'=>$settings->id),
  Yii::t('admin','Изменение'),
);

$this->menu=array(
        array('label'=>Yii::t('admin','Цены на доставку'), 'url'=>array('delivery')),
	array('label'=>Yii::t('admin','Список параметров'), 'url'=>array('index')),
	array('label'=>Yii::t('admin','Добавить параметр'), 'url'=>array('create')),
        array('label'=>Yii::t('admin','Просмотр параметра'), 'url'=>array('view', 'id'=>$settings->id)),
	array('label'=>Yii::t('admin','Управление настройками'), 'url'=>array('admin')),
);
?>

<h3><?=Yii::t('admin','Изменение параметра')?> "<?=$settings->id?>"</h3>

<?php echo $this->renderPartial('_updateform', array('model'=>$settings)); ?>