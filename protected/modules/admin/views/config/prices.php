<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="prices.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Настройки'),
); ?>

<h2><?=Yii::t('admin','Основная наценка')?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
  'id'=>'config-grid-maink',
  'dataProvider'=>$mainK,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{items}{pager}',
  'enableSorting'=>false,
  'columns'=>array(
    'id',
    'label',
    'value',
    'default_value',
//		'in_wizard',
    array(

      'type'=>'raw',
      'value'=>'"
		      <a href=\'javascript:void(0);\' onclick=\"renderUpdateForm(\'".$data->id."\')\"   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
      'htmlOptions'=>array('style'=>'width:150px;')
    ),

  ),
));
?>

<h2><?=Yii::t('admin','Скидка от суммы')?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
  'id'=>'config-grid-pricerates',
  'dataProvider'=>$priceRates,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{items}{pager}',
  'enableSorting'=>false,
  'columns'=>array(
    'id',
    'label',
    'value',
    'default_value',
//		'in_wizard',
    array(

      'type'=>'raw',
      'value'=>'"
		      <a href=\'javascript:void(0);\' onclick=\"renderUpdateForm(\'".$data->id."\')\"   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
      'htmlOptions'=>array('style'=>'width:150px;')
    ),

  ),
));
?>

<h2><?=Yii::t('admin','Скидка от количества')?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
  'id'=>'config-grid-countrates',
  'dataProvider'=>$countRates,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{items}{pager}',
  'enableSorting'=>false,
  'columns'=>array(
    'id',
    'label',
    'value',
    'default_value',
//		'in_wizard',
    array(

      'type'=>'raw',
      'value'=>'"
		      <a href=\'javascript:void(0);\' onclick=\"renderUpdateForm(\'".$data->id."\')\"   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
      'htmlOptions'=>array('style'=>'width:150px;')
    ),

  ),
));
?>

<h2><?=Yii::t('admin','Курсы валют')?></h2>
<b><?=Yii::t('admin','Автообновление')?>: </b><?=(DSConfig::getVal('rates_auto_update')==1)?Yii::t('admin','включено'):Yii::t('admin','выключено')?><br/>
<b><?=Yii::t('admin','Последнее обновление')?>: </b><?=(DSConfig::getVal('rates_auto_update_last_time'))?date('Y-m-d H:i',DSConfig::getVal('rates_auto_update_last_time')):Yii::t('admin','не обновлялось')?><br/>
<?php
$currs=Utils::getCurrencyRatesFromBank();
$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'config-grid-currencyrates',
	'dataProvider'=>$currencyRates,
        'type'=>'striped bordered condensed',
        'template'=>'{summary}{items}{pager}',
  'enableSorting'=>false,
	'columns'=>array(
		'id',
		'label',
		'value',
		'default_value',
      array (
        'header'=>Yii::t('main','Курс ЦБР к рублю'),
        'value'=>'Utils::getCurrencyRatesFromBank($data->id)',
      ),
//		'in_wizard',
               array(
		     
		      'type'=>'raw',
		       'value'=>'"
		      <a href=\'javascript:void(0);\' onclick=\"renderUpdateForm(\'".$data->id."\')\"   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		     "',
		      'htmlOptions'=>array('style'=>'width:150px;')  
		     ),
        
	),
)); 
 $this->renderPartial("_ajax_update");
 $this->renderPartial("_ajax_create_form",array("model"=>$model));
 $this->renderPartial("_ajax_view");
 ?>

 
<script type="text/javascript"> 
function delete_record(id)
{
 
  if(!confirm("Are you sure you want delete this?"))
   return;
   
 //  $('#ajaxtest-view-modal').modal('hide');
 
 var data="id="+id;
 

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/config/delete"); ?>',
   data:data,
success:function(data){
                 if(data=="true")
                  {
                     $('#config-view-modal').modal('hide');
                    $.fn.yiiGridView.update('config-grid', {

                    });
                    $.fn.yiiGridView.update('config-grid-maink', {

                    });
                    $.fn.yiiGridView.update('config-grid-currencyrates', {

                    });
                    $.fn.yiiGridView.update('config-grid-pricerates', {

                    });
                    $.fn.yiiGridView.update('config-grid-countrates', {

                    });
                 
                  } 
                 else
                   alert("deletion failed");
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
       //  alert(data);
    },

  dataType:'html'
  });

}
</script>

<style type="text/css" media="print">
body {visibility:hidden;}
.printableArea{visibility:visible;} 
</style>
<script type="text/javascript">
function printDiv()
{

window.print();

}
</script>
 

