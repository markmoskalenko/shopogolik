<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="delivery.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Доставка'),
);

$this->menu=array(
	array('label'=>Yii::t('admin','Добавить параметр'), 'url'=>array('create')),
);
?>

<h1><?=Yii::t('admin','Доставка - настройки')?></h1>

<?php foreach($settings as $setting):?>
<div class="view">
  <b><?=$setting['id']?></b><br/>
    <i><?=$setting['label']?>:</i><br/><?=DSConfig::formatXML(nl2br(htmlspecialchars($setting['value'], ENT_QUOTES)))?><br />
    <a href="<?=$this->createUrl('/admin/config/update/id/'.$setting['id'])?>" onclick="getContent(this,'<?=$setting['id']?>');return false;"><b><?=Yii::t('admin','изменить')?></b></a>
</div>
<?php endforeach?>