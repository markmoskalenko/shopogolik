<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="parserupdate.php">
* </description>
**********************************************************************************************************************/?>
<h3><?=Yii::t('admin','Настройки DropShop grabber')?></h3>

<div id="dsg-tabs" style="overflow-y: auto">
  <ul>
    <li><a href="#tab-dsg-edit"><?=Yii::t('admin','XML')?></a></li>
<!-- <li><a href="#tab-dsg-debug"><?//=Yii::t('main','Отладка')?></a></li> -->
  </ul>
  <div id="tab-dsg-edit">
    <? echo $this->renderPartial('_parserform', array('model'=>$settings)); ?>
  </div>
<!--  <div id="tab-dsg-debug">
    <?// echo $this->renderPartial('_parserdebugform', array()); ?>
  </div> -->
</div>


<script>
  $(function makeDSGSubTabs() {
    $("#dsg-tabs" ).tabs({
      cache: false
    });
  });
</script>