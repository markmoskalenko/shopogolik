<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_parserform.php">
* </description>
**********************************************************************************************************************/?>
<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>"config-form-$model->id",
	'enableAjaxValidation'=>false,
)); ?>
  <? if (in_array($model->id, array('search_CategoriesUpdate','search_DropShop_grabbers'))) { ?>
    <div class="row buttons">
      <?    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        //'id'=>'sub2',
        'type'=>'info',
        'icon'=>'ok white',
        'label'=>Yii::t('admin','Обновить версию'),
        'htmlOptions'=>array('class'=>'btn-block','onclick'=>"UpdateParamFromProxy('".$model->id."'); return false;"),
      ));
      ?>
    </div>
  <? } ?>
	<?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model,'id'); ?>
    <div class="row">
		<?//php echo $form->labelEx($model,'label'); ?>
		<?//php echo $form->error($model,'label'); ?>
	</div>
	<div class="row">
		<?php echo $model->label; ?><br/>
        <?php
      $editor=new SRichTextarea();
      $editor->init();
      $editor->model=$model;
      $editor->attribute='value';
      $editor->run();
        ?>
		<?php echo $form->error($model,'value'); ?>
	</div>
  <div class="row buttons">
<?    $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    //'id'=>'sub2',
    'type'=>'info',
    'icon'=>'ok white',
    'label'=>Yii::t('admin','Сохранить'),
    'htmlOptions'=>array('class'=>'btn-block','onclick'=>'saveConfig("'.$model->id.'"); return false;'),
    ));
?>
  </div>
<?php $this->endWidget(); ?>

</div><!-- form -->