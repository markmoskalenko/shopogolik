<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_parserdebugform.php">
* </description>
**********************************************************************************************************************/?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
  'id'=>'banners-update-form',
  'enableAjaxValidation'=>false,
  'enableClientValidation'=>false,
  'method'=>'post',
  'action'=>array("banners/update"),
  'type'=>'horizontal',
  'htmlOptions'=>array(
    'onsubmit'=>"return false;",/* Disable normal form submit */
    //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
  ),

)); ?>
  <fieldset>
    <?// echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
    <div class="control-group">
      <div class="span4">
        <?// echo $form->hiddenField($model,'id',array()); ?>
        <div class="row">
          <?// echo $form->labelEx($model,'img_src'); ?>
          <?// echo $form->textField($model,'img_src',array('size'=>60,'maxlength'=>1024)); ?>
          <?// echo $form->error($model,'img_src'); ?>
        </div>
     </div>
    </div>

    </div><!--end modal body-->

    <div class="modal-footer">
      <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
          'buttonType'=>'submit',
          //'id'=>'sub2',
          'type'=>'primary',
          'icon'=>'ok white',
          'label'=>Yii::t('admin','Сохранить'),
          'htmlOptions'=>array('onclick'=>'update();'),
        ));
        ?>
      </div>
    </div><!--end modal footer-->
  </fieldset>
<?php $this->endWidget(); ?>