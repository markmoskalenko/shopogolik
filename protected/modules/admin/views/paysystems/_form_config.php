<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_form_config.php">
* </description>
**********************************************************************************************************************/?><div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pay-systems-form-'.$model->id,
	'enableAjaxValidation'=>false,
        'method'=>'post',
	'type'=>'horizontal',
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data'
	)
)); ?>
     	<fieldset>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

   <div class="control-group">
			<div>

	<?php echo $form->textFieldRow($model,'enabled',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'logo_img',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'int_name',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'name_ru',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'name_en',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textAreaRow($model,'descr_ru',array('rows'=>6, 'cols'=>150, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'descr_en',array('rows'=>6, 'cols'=>150, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'parameters',array('rows'=>6, 'cols'=>150, 'class'=>'span8')); ?>



              <?
              //echo $form->textAreaRow($model,'form_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8'));
              echo $form->labelEx($model,'form_ru'); ?>
              <div class="elrte-place" style="width: 800px; display: block">
                <? $editor=new SRichTextarea();
                $editor->init();
                $editor->model=$model;
                $editor->attribute='form_ru';
                //$editor->htmlOptions=array('rows'=>8, 'cols'=>80);
                $editor->run();
                ?>
              </div>

	<?
    //echo $form->textAreaRow($model,'form_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8'));
    echo $form->labelEx($model,'form_en'); ?>
    <div class="elrte-place" style="width: 800px; display: block">
    <? $editor=new SRichTextarea();
       $editor->init();
       $editor->model=$model;
       $editor->attribute='form_en';
       //$editor->htmlOptions=array('rows'=>8, 'cols'=>80);
       $editor->run();
     ?>
    </div>
   </div>
  </div>

	<div class="form-actions">
		<?php
        $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
                        'icon'=>'ok white',  
			'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
          'htmlOptions'=>array('onclick'=>"savePaySystem(".$model->id."); return false;",),
		)); ?>
              <?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'reset',
                        'icon'=>'remove',  
			'label'=>Yii::t('admin','Сброс'),
		)); ?>
	</div>
</fieldset>

<?php $this->endWidget(); ?>

</div>
