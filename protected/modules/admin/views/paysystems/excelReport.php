<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="excelReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      enabled		</th>
 		<th width="80px">
		      logo_img		</th>
 		<th width="80px">
		      int_name		</th>
 		<th width="80px">
		      name_ru		</th>
 		<th width="80px">
		      name_en		</th>
 		<th width="80px">
		      descr_ru		</th>
 		<th width="80px">
		      descr_en		</th>
 		<th width="80px">
		      parameters		</th>
 		<th width="80px">
		      form_ru		</th>
 		<th width="80px">
		      form_en		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       		<td>
			<?php echo $row->logo_img; ?>
		</td>
       		<td>
			<?php echo $row->int_name; ?>
		</td>
       		<td>
			<?php echo $row->name_ru; ?>
		</td>
       		<td>
			<?php echo $row->name_en; ?>
		</td>
       		<td>
			<?php echo $row->descr_ru; ?>
		</td>
       		<td>
			<?php echo $row->descr_en; ?>
		</td>
       		<td>
			<?php echo $row->parameters; ?>
		</td>
       		<td>
			<?php echo $row->form_ru; ?>
		</td>
       		<td>
			<?php echo $row->form_en; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
