<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_create_form.php">
* </description>
**********************************************************************************************************************/?>
    <div id='pay-systems-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Добавить платёжную систему')?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
    <?php
   
    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pay-systems-create-form',
	'enableAjaxValidation'=>false,
    'enableClientValidation'=>false,
    'method'=>'post',
    'action'=>array("paysystems/create"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
          'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>

    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
       <div class="row-fluid">
       <div class="span6">
           <?php echo $form->labelEx($model,'enabled'); ?>
           <?php echo $form->textField($model,'enabled',array('class'=>'span3')); ?>
           <?php echo $form->error($model,'enabled'); ?>

           <?php echo $form->labelEx($model,'logo_img'); ?>
           <?php echo $form->textField($model,'logo_img',array('class'=>'span12','maxlength'=>512)); ?>
           <?php echo $form->error($model,'logo_img'); ?>

           <?php echo $form->labelEx($model,'int_name'); ?>
           <?php echo $form->textField($model,'int_name',array('class'=>'span12','maxlength'=>256)); ?>
           <?php echo $form->error($model,'int_name'); ?>

           <?php echo $form->labelEx($model,'descr_ru'); ?>
           <?php echo $form->textArea($model,'descr_ru',array('rows'=>6, 'class'=>'span12')); ?>
           <?php echo $form->error($model,'descr_ru'); ?>

           <?php echo $form->labelEx($model,'descr_en'); ?>
           <?php echo $form->textArea($model,'descr_en',array('rows'=>6, 'class'=>'span12')); ?>
           <?php echo $form->error($model,'descr_en'); ?>

           <?php echo $form->labelEx($model,'parameters'); ?>
           <?php echo $form->textArea($model,'parameters',array('rows'=>6, 'class'=>'span12')); ?>
           <?php echo $form->error($model,'parameters'); ?>
       </div>
       <div class="span6">
           <?php echo $form->labelEx($model,'name_ru'); ?>
           <?php echo $form->textField($model,'name_ru',array('class'=>'span12','maxlength'=>256)); ?>
           <?php echo $form->error($model,'name_ru'); ?>

           <?php echo $form->labelEx($model,'name_en'); ?>
           <?php echo $form->textField($model,'name_en',array('class'=>'span12','maxlength'=>256)); ?>
           <?php echo $form->error($model,'name_en'); ?>

           <?php echo $form->labelEx($model,'form_ru'); ?>
           <?php echo $form->textArea($model,'form_ru',array('rows'=>6, 'class'=>'span12')); ?>
           <?php echo $form->error($model,'form_ru'); ?>

           <?php echo $form->labelEx($model,'form_en'); ?>
           <?php echo $form->textArea($model,'form_en',array('rows'=>6, 'class'=>'span12')); ?>
           <?php echo $form->error($model,'form_en'); ?>
       </div>
   </div>
 </div><!--end modal body-->
  
</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->
<!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#pay-systems-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
   url: '<?php echo Yii::app()->createAbsoluteUrl("admin/paysystems/create"); ?>',
   data:data,
   success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#pay-systems-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('pay-systems-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#pay-systems-create-form').each (function(){
  this.reset();
   });
  $('#pay-systems-view-modal').modal('hide');
  $('#pay-systems-create-modal').modal({
   show:true
  });
}
</script>
