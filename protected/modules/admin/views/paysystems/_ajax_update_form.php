<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update_form.php">
* </description>
**********************************************************************************************************************/?>
<div id='pay-systems-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
   
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Изменение платёжной системы ')?><strong style="color:#0093f5;">#<?php echo $model->id; ?></strong></h3>
    </div>
    
    <div class="modal-body">
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pay-systems-update-form',
	'enableAjaxValidation'=>false,
    'enableClientValidation'=>false,
    'method'=>'post',
    'action'=>array("paysystems/update"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
       <div class="row-fluid">
        <div class="span12">
            <?php echo $form->hiddenField($model,'id',array()); ?>

            <?php echo $form->labelEx($model,'enabled'); ?>
            <?php echo $form->textField($model,'enabled',array('class'=>'span12')); ?>
            <?php echo $form->error($model,'enabled'); ?>
        </div>
       </div>
       <div class="row-fluid">
        <div class="span6">
            <?php echo $form->labelEx($model,'logo_img'); ?>
            <?php echo $form->textField($model,'logo_img',array('class'=>'span12','maxlength'=>512)); ?>
            <?php echo $form->error($model,'logo_img'); ?>

            <?php echo $form->labelEx($model,'int_name'); ?>
            <?php echo $form->textField($model,'int_name',array('class'=>'span12','maxlength'=>256)); ?>
            <?php echo $form->error($model,'int_name'); ?>

            <?php echo $form->labelEx($model,'descr_ru'); ?>
            <?php echo $form->textArea($model,'descr_ru',array('rows'=>6, 'class'=>'span12')); ?>
            <?php echo $form->error($model,'descr_ru'); ?>

            <?php echo $form->labelEx($model,'descr_en'); ?>
            <?php echo $form->textArea($model,'descr_en',array('rows'=>6, 'class'=>'span12')); ?>
            <?php echo $form->error($model,'descr_en'); ?>
        </div>
        <div class="span6">
            <?php echo $form->labelEx($model,'name_ru'); ?>
            <?php echo $form->textField($model,'name_ru',array('class'=>'span12','maxlength'=>256)); ?>
            <?php echo $form->error($model,'name_ru'); ?>

            <?php echo $form->labelEx($model,'name_en'); ?>
            <?php echo $form->textField($model,'name_en',array('class'=>'span12','maxlength'=>256)); ?>
            <?php echo $form->error($model,'name_en'); ?>

            <?php echo $form->labelEx($model,'form_ru'); ?>
            <?php echo $form->textArea($model,'form_ru',array('rows'=>6, 'class'=>'span12')); ?>
            <?php echo $form->error($model,'form_ru'); ?>

            <?php echo $form->labelEx($model,'form_en'); ?>
            <?php echo $form->textArea($model,'form_en',array('rows'=>6,'class'=>'span12')); ?>
            <?php echo $form->error($model,'form_en'); ?>
        </div>
       </div>
       <div class="row-fluid">
        <div class="span12">
            <?php echo $form->labelEx($model,'parameters'); ?>
            <?php echo $form->textArea($model,'parameters',array('rows'=>6, 'class'=>'span12')); ?>
            <?php echo $form->error($model,'parameters'); ?>
        </div>
       </div>
</div>

</div><!--end modal body-->
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            //'id'=>'sub2',
            'type'=>'info',
            'icon'=>'ok white',
            'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'update();'),
        ));
        ?>
    </div><!--end modal footer-->

<?php $this->endWidget(); ?>
</div><!--end modal-->



