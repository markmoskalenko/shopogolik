<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_create_form.php">
* </description>
**********************************************************************************************************************/?>
    <div id='banrules-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новое условие');?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
   <?php
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    	'id'=>'banrules-create-form',
    	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("banrules/create"),
    	'type'=>'horizontal',
    	'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
                  'clientOptions'=>array(
                  'validateOnType'=>true,
                  'validateOnSubmit'=>true,
                  'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
  
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

        <div class="row-fluid">
            <div class="span5">
                <?php echo $form->labelEx($model,'request_rule'); ?>
                <?php echo $form->textField($model,'request_rule',array('class'=>'span12','maxlength'=>4000)); ?>
                <?php echo $form->error($model,'request_rule'); ?>

                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled',array('class'=>'span12')); ?>
                <?php echo $form->error($model,'enabled'); ?>
            </div>
            <div class="span7">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textArea($model,'description',array('rows'=>7, 'class'=>'span12')); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
        </div>
			  	  <div class="row-fluid">
					  <?php echo $form->labelEx($model,'rule_order'); ?>
					  <?php echo $form->textField($model,'rule_order'); ?>
					  <?php echo $form->error($model,'rule_order'); ?>
				  </div>
  </div>
  
</div><!--end modal body-->
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

<!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#banrules-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/banrules/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#banrules-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('banrules-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#banrules-create-form').each (function(){
  this.reset();
   });
  $('#banrules-view-modal').modal('hide');
  $('#banrules-create-modal').modal({
   show:true
  });
}
</script>
