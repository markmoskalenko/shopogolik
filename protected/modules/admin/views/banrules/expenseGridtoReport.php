<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="expenseGridtoReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      description		</th>
 		<th width="80px">
		      request_rule		</th>
 		<th width="80px">
		      rule_order		</th>
 		<th width="80px">
		      enabled		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->description; ?>
		</td>
       		<td>
			<?php echo $row->request_rule; ?>
		</td>
       		<td>
			<?php echo $row->rule_order; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
