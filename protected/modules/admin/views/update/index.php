<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
/* @var $this UpdateController */

$this->breadcrumbs=array(
	'Utilites',
);
?>

<h1><?=Yii::t('admin','Обновления')?></h1>
<hr/>
<!-- ================================================ -->
<div class="form">
<form id="command1">
  <div class="row">
   <?=Yii::t('admin','Сформировать обновление')?>
  </div>
  <div class="row buttons">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
      'buttonType'=>'ajaxSubmit',
      'type'=>'info',
      'icon'=>'legal white',
      'label'=>Yii::t('admin','Сделать обновление'),
//    'loadingText'=>Yii::t('admin','Выполняется...'),
//    'completeText'=>Yii::t('admin','Выполнено'),
      'url'=>Yii::app()->createURL('/admin/update/command',array('cmd'=>'make_update')),
      'htmlOptions'=>array('class'=>'btn-block'),
      'ajaxOptions'=>array('update'=>'#resultBlock1','timeout'=>'300000'),
    ));
    ?>
   </div>
  <div id="resultBlock1"></div>
</form>
</div>
  <!-- ================================================ -->
<div class="form">
  <form id="command2">
    <div class="row">
      <?=Yii::t('admin','Отмена изменений')?>
    </div>
    <div class="row buttons">
      <?php
      $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'ajaxSubmit',
        'type'=>'success',
        'icon'=>'cloud-download white',
        'label'=>Yii::t('admin','Откатить изменения'),
//      'loadingText'=>Yii::t('admin','Выполняется...'),
//      'completeText'=>Yii::t('admin','Выполнено'),
        'url'=>Yii::app()->createURL('/admin/update/command',array('cmd'=>'get_update')),
        'htmlOptions'=>array('class'=>'btn-block'),
        'ajaxOptions'=>array('update'=>'#resultBlock2','timeout'=>'300000'),
      ));
      ?>
    </div>
      <select size="3" name="backup">
          <?php
          $dir = realpath(Yii::app()->basePath.'/..').'/backup';
          if (is_dir($dir)) {
             $files = scandir($dir);    //сканируем (получаем массив файлов)
             array_shift($files); // удаляем из массива '.'
             array_shift($files); // удаляем из массива '..'
             for($i=0; $i<sizeof($files); $i++) {
               if ($i==0) {
                 //$s = substr($files[$i],0,strpos($files[$i],'_'));
                 echo '<option selected value="'.$files[$i].'">'.$files[$i].'</option>';
               }
               else {
                 echo '<option value="'.$files[$i].'">'.$files[$i].'</option>';
               }
             }
          };
          ?>
      </select>
    <div id="resultBlock2"></div>
  </form>
</div>
<!-- ================================================ -->
<div class="form">
    <form id="command3">
        <div class="row">
            <?=Yii::t('admin','Применить обновление')?>
        </div>
        <div class="row buttons">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'ajaxSubmit',
                'type'=>'danger',
                'icon'=>'play white',
                'label'=>Yii::t('admin','Применить обновление'),
//      'loadingText'=>Yii::t('admin','Выполняется...'),
//      'completeText'=>Yii::t('admin','Выполнено'),
                'url'=>Yii::app()->createURL('/admin/update/command',array('cmd'=>'apply_update')),
                'htmlOptions'=>array('class'=>'btn-block'),
                'ajaxOptions'=>array('update'=>'#resultBlock3','timeout'=>'300000'),
            ));
            ?>
        </div>
        <div id="resultBlock3"></div>
    </form>
</div>