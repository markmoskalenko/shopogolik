<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_form.php">
 * </description>
 **********************************************************************************************************************/?>
<div class="row-fluid">
    <div class="span10" style="border:solid 1px #c9e0ed;">
        <div class="form" style="margin-bottom:0;">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'id'=>'cms-menus-form-'.$model->id,
                'enableAjaxValidation'=>false,
                'method'=>'post',
                'type'=>'horizontal',
                'htmlOptions'=>array(
                    'enctype'=>'multipart/form-data'
                )
            )); ?>

            <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

                    <?php echo $form->checkBoxRow($model,'enabled',array('class'=>'span2')); ?>
                    <?php echo $form->checkBoxRow($model,'SEO',array('class'=>'span2')); ?>
                    <?php echo $form->textFieldRow($model,'menu_id',array('class'=>'span11','maxlength'=>255)); ?>
                    <?php echo $form->textAreaRow($model,'menu_data',array('rows'=>10, 'cols'=>50, 'class'=>'span11')); ?>

            <div class="modal-footer" style="margin:0 !important;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>"saveForm('cms-menus-form-".$model->id."'); return false;",),
                )); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'reset',
                    'type'=>'danger',
                    'icon'=>'remove white',
                    'label'=>Yii::t('admin','Сброс'),
                )); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div>

    </div>
    <div class="span2">
        <div id="cms-menus-content-history-<?=$model->id?>">
            <?  $this->widget('application.modules.admin.components.widgets.CmsHistoryBlock', array(
                'id' =>'cms_menus_content',
                'tableName' => 'cms_menus_content',
                'contentId' => $model->menu_id,
                'pageSize' =>50,
            ));
            ?>
        </div>
    </div>
</div>
<br>



  <div id="cms-menu-content-filemanager-<?=$model->id?>"></div>
  <script type="text/javascript" charset="utf-8">
    // Documentation for client options:
    // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
    $(document).ready(function() {
      $('#cms-menu-content-filemanager-<?=$model->id?>').elfinder({
        url : '/admin/fileman/index'  // connector URL (REQUIRED)
        // , lang: 'ru'                    // language (OPTIONAL)
        ,resizable: false
        ,height: '600px'
        ,width: '83%'
      });
    });
  </script>

