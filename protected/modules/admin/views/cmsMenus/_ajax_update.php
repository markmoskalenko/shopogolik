<div id="cms-menus-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#cms-menus-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsMenus/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#cms-menus-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('cms-menus-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#cms-menus-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsMenus/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#cms-menus-update-modal-container').html(data); 
                 $('#cms-menus-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
