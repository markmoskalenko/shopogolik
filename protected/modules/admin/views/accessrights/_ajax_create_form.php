<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_create_form.php">
* </description>
**********************************************************************************************************************/?>
    <div id='accessrights-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новая роль')?></h3>
    </div>
    
    <div class="modal-body">
    
    <div class="form">

   <?php
   
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	    'id'=>'accessrights-create-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("accessrights/create"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
                    'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>
    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

        <?php echo $form->labelEx($model,'role'); ?>
        <?php echo $form->textField($model,'role',array('size'=>60,'maxlength'=>64)); ?>
        <?php echo $form->error($model,'role'); ?>

        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textArea($model,'description',array('rows'=>6,'class'=>'span12','cols'=>50)); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'allow'); ?>
                <?php echo $form->textArea($model,'allow',array('rows'=>6,'class'=>'span12','cols'=>50)); ?>
                <?php echo $form->error($model,'allow'); ?>
            </div>
            <div class="span4">
                <?php echo $form->labelEx($model,'deny'); ?>
                <?php echo $form->textArea($model,'deny',array('rows'=>6,'class'=>'span12','cols'=>50)); ?>
                <?php echo $form->error($model,'deny'); ?>
            </div>
        </div>
  </div><!--end modal body-->

    </div><!--end modal body-->
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove white',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

<!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#accessrights-create-form").serialize();
   jQuery.ajax({
   type: 'POST',
   url: '<?php
   echo Yii::app()->createAbsoluteUrl("admin/accessrights/create"); ?>',
   data:data,
   success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#accessrights-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('accessrights-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#accessrights-create-form').each (function(){
  this.reset();
   });
  $('#accessrights-view-modal').modal('hide');
  $('#accessrights-create-modal').modal({
   show:true
  });
}
</script>