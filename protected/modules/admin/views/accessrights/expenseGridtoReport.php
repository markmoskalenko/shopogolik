<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="expenseGridtoReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      role		</th>
 		<th width="80px">
		      description		</th>
 		<th width="80px">
		      allow		</th>
 		<th width="80px">
		      deny		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->role; ?>
		</td>
       		<td>
			<?php echo $row->description; ?>
		</td>
       		<td>
			<?php echo $row->allow; ?>
		</td>
       		<td>
			<?php echo $row->deny; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
