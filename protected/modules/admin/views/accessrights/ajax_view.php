<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ajax_view.php">
* </description>
**********************************************************************************************************************/?>
<?php
    echo "<div class='printableArea'>";
    echo "<h1>Права доступа #".$model->role."</h1>";
            $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'info',
            'icon'=>'plus white',
			'label'=>Yii::t('admin','Создать'),
			'htmlOptions'=>array('onclick'=>'renderCreateForm();'),
		));
		echo " ";
            $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'success',
            'icon'=>'edit white',
			'label'=>Yii::t('admin','Правка'),
			'htmlOptions'=>array('onclick'=>'renderUpdateForm('.$model->role.');'),
		));
		echo " ";
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'danger',
            'icon'=>'trash white',
			'label'=>Yii::t('admin','Удалить'),
			'htmlOptions'=>array('onclick'=>'delete_record('.$model->role.');'),
		));
		echo " ";
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'warning',
            'icon'=>'print white',
			'label'=>Yii::t('admin','Печать'),
			'htmlOptions'=>array('onclick'=>'print();'),
		));
    echo "<hr/>";
	         $this->widget('bootstrap.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
						'role',
                		'description',
		                'allow',
		                'deny',
			),
		));
	         echo "</div>";