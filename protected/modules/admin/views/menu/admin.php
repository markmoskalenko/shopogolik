<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="admin.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	'Main Menus'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>Yii::t('admin','Список'),'url'=>array('index')),
    array('label'=>Yii::t('admin','Добавить'),'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('main-menu-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?=Yii::t('admin','Управление меню категорий')?></h1>

<p>
  <?=Yii::t('admin','Вы можете использовать операторы сравнения')?> (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>)</p>

<?php echo CHtml::link(Yii::t('admin','Advanced Search'),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'main-menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'cid',
		'ru',
		'en',
		'parent',
		'status',
		/*
		'meta_desc_ru',
		'meta_keeword_ru',
		'page_title_ru',
		'meta_desc_en',
		'meta_keeword_en',
		'page_title_en',
		'page_desc_ru',
		'page_desc_en',
		'url',
		'zh',
		'query',
		'level',
		'order_in_level',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
