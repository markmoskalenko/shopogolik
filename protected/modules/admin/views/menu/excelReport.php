<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="excelReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      cid		</th>
 		<th width="80px">
		      ru		</th>
 		<th width="80px">
		      en		</th>
 		<th width="80px">
		      parent		</th>
 		<th width="80px">
		      status		</th>
 		<th width="80px">
		      meta_desc_ru		</th>
 		<th width="80px">
		      meta_keeword_ru		</th>
 		<th width="80px">
		      page_title_ru		</th>
 		<th width="80px">
		      meta_desc_en		</th>
 		<th width="80px">
		      meta_keeword_en		</th>
 		<th width="80px">
		      page_title_en		</th>
 		<th width="80px">
		      page_desc_ru		</th>
 		<th width="80px">
		      page_desc_en		</th>
 		<th width="80px">
		      url		</th>
 		<th width="80px">
		      zh		</th>
 		<th width="80px">
		      query		</th>
 		<th width="80px">
		      level		</th>
 		<th width="80px">
		      description		</th>
 		<th width="80px">
		      order_in_level		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->cid; ?>
		</td>
       		<td>
			<?php echo $row->ru; ?>
		</td>
       		<td>
			<?php echo $row->en; ?>
		</td>
       		<td>
			<?php echo $row->parent; ?>
		</td>
       		<td>
			<?php echo $row->status; ?>
		</td>
       		<td>
			<?php echo $row->meta_desc_ru; ?>
		</td>
       		<td>
			<?php echo $row->meta_keeword_ru; ?>
		</td>
       		<td>
			<?php echo $row->page_title_ru; ?>
		</td>
       		<td>
			<?php echo $row->meta_desc_en; ?>
		</td>
       		<td>
			<?php echo $row->meta_keeword_en; ?>
		</td>
       		<td>
			<?php echo $row->page_title_en; ?>
		</td>
       		<td>
			<?php echo $row->page_desc_ru; ?>
		</td>
       		<td>
			<?php echo $row->page_desc_en; ?>
		</td>
       		<td>
			<?php echo $row->url; ?>
		</td>
       		<td>
			<?php echo $row->zh; ?>
		</td>
       		<td>
			<?php echo $row->query; ?>
		</td>
       		<td>
			<?php echo $row->level; ?>
		</td>
       		<td>
			<?php echo $row->order_in_level; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
