<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="index.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
	'Main Menus',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').slideToggle('fast');
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('main-menu-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

?>
<div class="row-fluid">
<div class="span4">
<h1><?=Yii::t('admin','Меню категорий')?></h1>
</div>
<div class="span8">
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
	'htmlOptions'=>array(
		'class'=>''
	)
));
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
        array('label'=>Yii::t('admin','Очистить кэш'), 'icon'=>'icon-refresh', 'url'=>'javascript:void(0);','linkOptions'=>array('onclick'=>'clearMenuCache()')),
		array('label'=>Yii::t('admin','Создать'), 'icon'=>'icon-plus', 'url'=>'javascript:void(0);','linkOptions'=>array('onclick'=>'renderCreateForm()')),
                //array('label'=>Yii::t('admin','Список'), 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('index'),'active'=>true, 'linkOptions'=>array()),
//		array('label'=>Yii::t('admin','Поиск'), 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
//      array('label'=>Yii::t('admin','PDF'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GeneratePdf'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
		array('label'=>Yii::t('admin','Excel'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateExcel'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
        array('label'=>Yii::t('admin','Обновить с taoabao'),
              'icon'=>'icon-download-alt',
              'url'=>'#',
              'linkOptions'=>array('onclick'=>'loadCatalogFromTaobao(); return false;',
              'title'=>Yii::t('admin','Внимание! Обновление всех категорий может вызвать различные деструктивные и побочные эффекты. Используйте только если адекватно понимаете, для чего!')
              ,'visible'=>true),),
          array('label'=>Yii::t('admin','Обновить meta и HFURL'),
                'icon'=>'icon-gears',
                'url'=>'#',
                'linkOptions'=>array('onclick'=>'updateMetaAndHFURL(); return false;',
                                     'title'=>Yii::t('admin','Внимание! Перестроение meta и HFURL приведет к удалению заданных ранее значений! Используйте с умом!')
                ,'visible'=>true),),
         )));
$this->endWidget();
?>
    </div>
</div>
<div style="float: left;" id="jsupdate-res"><?=Yii::t('admin','Ожидание...')?></div>
<br/>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div id="menu-tabs" style="overflow-y: auto">
  <ul>
    <li><a href="#tab-menu-tree"><?=Yii::t('admin','Структура')?></a></li>
    <li><a href="#tab-menu-table"><?=Yii::t('main','Таблица')?></a></li>
  </ul>
  <div id="tab-menu-tree">
      <div class="row-fluid">
          <div class="span10">
              <table width="100%" border="1" cellspacing="1" cellpadding="1">
                  <tr>
                      <td width="220px">
                          <div id="menu-tree-view-left">
                              <h1><?= Yii::t('main', 'Категории товаров') ?></h1>
                              <? $this->widget('application.components.widgets.CategoriesMenuBlock',array(
                                  'adminMode'=>true,
                              ));
                              ?>
                          </div>
                      </td>
                      <td>

                          <div id="menu-tree-view-right">
                              <h1 style="padding-bottom: 15px;"><?=Yii::t('admin','Категории - подробно')?></h1>
                              <?
                              if ($model->parent==null) {
                                  $id='root';
                              } else {
                                  $id=$model->parent;
                              }
                              $this->widget('CTreeView',
                                  array(
                                      'id'=>'mainmenu-tree-'.$id,
                                      'data' => $tree,
                                      'prerendered'=>false,
                                      'htmlOptions' => array('class' => 'admin-category-tree-node')
                                  ));
                              ?>
                          </div>
                      </td>
                  </tr>
              </table>
          </div>
          <div class="span2">
              <br/><h1><?=Yii::t('admin','Сохранение')?></h1>
            <input id="new-stored-categories-name" type="text" value="<?=Yii::t('main','Новый сохраненный каталог')?>">
            <input type="button" class="btn btn-info" value="<?=Yii::t('main','Сохранить')?>" onclick="menuStorageCommand('save',$('#new-stored-categories-name').val())">
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
              'id'=>'admin-menu-storage-grid',
              'dataProvider'=>$storedDataProvider,
              'type'=>'striped bordered condensed',
              'template'=>'{items}{pager}',//'{summary}{pager}{items}{pager}',
              'columns'=>array(
                array('name'=>'max_store_date',
                      'header'=>Yii::t('admin','Дата'),
                ),
                array('name'=>'store_name',
                      'header'=>Yii::t('admin','Описание'),
                ),
                array(
                  'type'=>'raw',
                  'value'=>'"
		      <a href=\'javascript:void(0);\' onclick=\'menuStorageCommand(\"restore\",\"".$data[\'store_name\']."\")\'   class=\'btn btn-small view\'  ><i class=\'icon-folder-open\'></i></a>
		      <a href=\'javascript:void(0);\' onclick=\'menuStorageCommand(\"delete\",\"".$data[\'store_name\']."\")\'   class=\'btn btn-small view\'  ><i class=\'icon-trash\'></i></a>
		     "',
                  'htmlOptions'=>array('style'=>'width:70px;')
                ),
              ),
            ));
            ?>
          </div>
      </div>

  </div>
  <div id="tab-menu-table">

              <?php $this->widget('bootstrap.widgets.TbGridView',array(
                  'id'=>'main-menu-grid',
                  'dataProvider'=>$model->search(50),
                  'filter'=>$model,
                  'type'=>'striped bordered condensed',
                  'template'=>'{summary}{items}{pager}',
                  'columns'=>array(
                      'cid',
                      'query',
                      'zh',
                      'ru',
                      'en',
                      array(
                          'name'=>'status',
                          'header'=>Yii::t('admin','Статус'),
                      ),
                      /*		'id',
                              'cid',
                              'ru',
                              'en',
                              'parent',
                              'status',
                              'meta_desc_ru',
                              'meta_keeword_ru',
                              'page_title_ru',
                              'meta_desc_en',
                              'meta_keeword_en',
                              'page_title_en',
                              'page_desc_ru',
                              'page_desc_en',
                              'url',
                              'zh',
                              'query',
                              'level',
                              'order_in_level',
                              */
                      array(

                          'type'=>'raw',
                          'value'=>'"
		      <a href=\'javascript:void(0);\' onclick=\'renderView(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-eye-open\'></i></a>
		      <a href=\'javascript:void(0);\' onclick=\'renderUpdateForm(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-pencil\'></i></a>
		      <a href=\'javascript:void(0);\' onclick=\'delete_record(".$data->id.")\'   class=\'btn btn-small view\'  ><i class=\'icon-trash\'></i></a>
		     "',
                          'htmlOptions'=>array('style'=>'width:150px;')
                      ),

                  ),
              )); ?>

  </div>
</div>
<?
 $this->renderPartial("_ajax_update");
 $this->renderPartial("_ajax_create_form",array("model"=>$model));
 $this->renderPartial("_ajax_view");
 ?>

 
<script type="text/javascript"> 
function delete_record(id)
{
  if(!confirm("<?=Yii::t('admin','Вы уверены, что хотите удалить эту категорию?')?>"))
   return;
 //  $('#ajaxtest-view-modal').modal('hide');
//    admin-category-tree-node-1139
 var data="id="+id;
  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/menu/delete"); ?>',
   data:data,
success:function(data){
                 if(data=="true")
                  {
                      $('#admin-category-tree-node-'+id).remove();
                     $('#main-menu-view-modal').modal('hide');
                     $.fn.yiiGridView.update('main-menu-grid', {
                         });
                  }
                 else
                   alert("<?=Yii::t('admin','Ошибка удаления категории')?>");
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
       //  alert(data);
    },
  dataType:'html'
  });
}
</script>

<style type="text/css" media="print">
body {visibility:hidden;}
.printableArea{visibility:visible;} 
</style>
<script type="text/javascript">
function printDiv()
{

window.print();

}
</script>

<script>
  $(function makeMenuSubTabs() {
    $("#menu-tabs" ).tabs({
      cache: false
    });
  });
</script>
