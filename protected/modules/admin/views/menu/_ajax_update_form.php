<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_ajax_update_form.php">
* </description>
**********************************************************************************************************************/?>
<div id='main-menu-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Изменение категории')?> <strong style="color:#0093f5;">#<?php echo $model->id; ?></strong></h3>
    </div>
    <div class="modal-body">
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    	'id'=>'main-menu-update-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("menu/update"),
	    //'type'=>'horizontal',
	    'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
)); ?>
        <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
        <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        <?php echo $form->hiddenField($model,'id',array()); ?>
        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->textFieldRow($model,'cid',array('class'=>'span12'),(!$model['cid'])?array('value'=>0):array());?>
                <?php echo $form->dropDownListRow($model,'parent',MainMenu::getCatList(),array('class'=>'span12')); ?>
                <?php echo $form->textFieldRow($model,'status',array('class'=>'span12','maxlength'=>32)); ?>
            </div>
            <div class="span6">
                <?php echo $form->textFieldRow($model,'query',array('class'=>'span12','maxlength'=>512)); ?>
                <?php echo $form->textFieldRow($model,'url',array('class'=>'span12','maxlength'=>512)); ?>
                <?php echo $form->textFieldRow($model,'order_in_level',array('class'=>'span12')); ?>
            </div>
        </div>
        <?php echo $form->textAreaRow($model,'zh',array('class'=>'span10','rows'=>2,'maxlength'=>512)); ?>
        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->textAreaRow($model,'ru',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'page_title_ru',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'meta_desc_ru',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'meta_keeword_ru',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'page_desc_ru',array('class'=>'span12','rows'=>2,)); ?>
            </div>
            <div class="span6">
                <?php echo $form->textAreaRow($model,'en',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'page_title_en',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'meta_desc_en',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'meta_keeword_en',array('class'=>'span12','rows'=>2,'maxlength'=>512)); ?>
                <?php echo $form->textAreaRow($model,'page_desc_en',array('class'=>'span12','rows'=>2,)); ?>
            </div>
        </div>
        <div class="row-fluid">
            <?php echo $form->textAreaRow($model,'decorate',array('class'=>'span10','rows'=>2,)); ?>
        </div>
        <?php //echo $form->textFieldRow($model,'level'); ?>

  </div><!--end modal body-->
  


<?php $this->endWidget(); ?>
</div>
    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            //'id'=>'sub2',
            'type'=>'info',
            'icon'=>'ok white',
            'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'update();'),
        ));
        ?>
    </div><!--end modal footer-->
</div><!--end modal-->



