<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_view.php">
* </description>
**********************************************************************************************************************/?>
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cid')); ?>:</b>
	<?php echo CHtml::encode($data->cid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ru')); ?>:</b>
	<?php echo CHtml::encode($data->ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('en')); ?>:</b>
	<?php echo CHtml::encode($data->en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent')); ?>:</b>
	<?php echo CHtml::encode($data->parent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_desc_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_desc_ru); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keeword_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keeword_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->page_title_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_desc_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_desc_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keeword_en')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keeword_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_title_en')); ?>:</b>
	<?php echo CHtml::encode($data->page_title_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_desc_ru')); ?>:</b>
	<?php echo CHtml::encode($data->page_desc_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page_desc_en')); ?>:</b>
	<?php echo CHtml::encode($data->page_desc_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zh')); ?>:</b>
	<?php echo CHtml::encode($data->zh); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('query')); ?>:</b>
	<?php echo CHtml::encode($data->query); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level')); ?>:</b>
	<?php echo CHtml::encode($data->level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_in_level')); ?>:</b>
	<?php echo CHtml::encode($data->order_in_level); ?>
	<br />

	*/ ?>

</div>