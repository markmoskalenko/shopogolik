<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_script')); ?>:</b>
	<?php echo CHtml::encode($data->job_script); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_start_time')); ?>:</b>
	<?php echo CHtml::encode($data->job_start_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_stop_time')); ?>:</b>
	<?php echo CHtml::encode($data->job_stop_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_interval')); ?>:</b>
	<?php echo CHtml::encode($data->job_interval); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_description')); ?>:</b>
	<?php echo CHtml::encode($data->job_description); ?>
	<br />


</div>