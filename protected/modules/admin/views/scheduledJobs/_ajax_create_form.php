
    <div id='scheduled-jobs-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Create scheduled-jobs</h3>
    </div>
    
    <div class="modal-body">
    
    <div class="form">

   <?php
   
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'scheduled-jobs-create-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("scheduled-jobs/create"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
          'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
                                    

            ),                  
  
)); ?>
     	<fieldset>
		<legend>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		</legend>

	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">
			
							  <div class="row">
					  <?php echo $form->labelEx($model,'job_script'); ?>
					  <?php echo $form->textArea($model,'job_script',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'job_script'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_start_time'); ?>
					  <?php echo $form->textField($model,'job_start_time'); ?>
					  <?php echo $form->error($model,'job_start_time'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_stop_time'); ?>
					  <?php echo $form->textField($model,'job_stop_time'); ?>
					  <?php echo $form->error($model,'job_stop_time'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_interval'); ?>
					  <?php echo $form->textField($model,'job_interval',array('size'=>20,'maxlength'=>20)); ?>
					  <?php echo $form->error($model,'job_interval'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_description'); ?>
					  <?php echo $form->textArea($model,'job_description',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'job_description'); ?>
				  </div>

			  
                        </div>   
  </div>

  </div><!--end modal body-->
  
  <div class="modal-footer">
	<div class="form-actions">

		<?php
		
		 $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
                        'icon'=>'ok white', 
			'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
            'htmlOptions'=>array('onclick'=>'create();'),
			)
			
		);
		
		?>
              <?php
 $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'reset',
                        'icon'=>'remove',  
			'label'=>Yii::t('admin','Сброс'),
		)); ?>
	</div> 
   </div><!--end modal footer-->	
</fieldset>

<?php
 $this->endWidget(); ?>

</div>

</div><!--end modal-->

<script type="text/javascript">
function create()
 {
 
   var data=$("#scheduled-jobs-create-form").serialize();
     


  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/scheduled-jobs/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#scheduled-jobs-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('scheduled-jobs-grid', {
                     
                         });
                   
                 }
                 
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },

  dataType:'html'
  });

}

function renderCreateForm()
{
  $('#scheduled-jobs-create-form').each (function(){
  this.reset();
   });

  
  $('#scheduled-jobs-view-modal').modal('hide');
  
  $('#scheduled-jobs-create-modal').modal({
   show:true
   
  });
}

</script>
