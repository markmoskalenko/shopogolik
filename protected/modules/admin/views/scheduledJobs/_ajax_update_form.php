    <div id='scheduled-jobs-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
   
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Update scheduled-jobs #<?php echo $model->id; ?></h3>
    </div>
    
    <div class="modal-body">
 
    
    
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'scheduled-jobs-update-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("scheduled-jobs/update"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
	
)); ?>
     	<fieldset>
		<legend>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		</legend>

	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">
			
			<?php echo $form->hiddenField($model,'id',array()); ?>
			
	               				  <div class="row">
					  <?php echo $form->labelEx($model,'job_script'); ?>
					  <?php echo $form->textArea($model,'job_script',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'job_script'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_start_time'); ?>
					  <?php echo $form->textField($model,'job_start_time'); ?>
					  <?php echo $form->error($model,'job_start_time'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_stop_time'); ?>
					  <?php echo $form->textField($model,'job_stop_time'); ?>
					  <?php echo $form->error($model,'job_stop_time'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_interval'); ?>
					  <?php echo $form->textField($model,'job_interval',array('size'=>20,'maxlength'=>20)); ?>
					  <?php echo $form->error($model,'job_interval'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'job_description'); ?>
					  <?php echo $form->textArea($model,'job_description',array('rows'=>6, 'cols'=>50)); ?>
					  <?php echo $form->error($model,'job_description'); ?>
				  </div>

			  
                        </div>   
  </div>

  </div><!--end modal body-->
  
  <div class="modal-footer">
	<div class="form-actions">

	                
		<?php		
		 $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			//'id'=>'sub2',
			'type'=>'primary',
                        'icon'=>'ok white', 
			'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
			'htmlOptions'=>array('onclick'=>'update();'),
		));
		
		?>
             
	</div> 
   </div><!--end modal footer-->	
</fieldset>

<?php $this->endWidget(); ?>

</div>


</div><!--end modal-->



