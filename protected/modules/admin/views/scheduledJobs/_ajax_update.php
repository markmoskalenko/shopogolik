<div id="scheduled-jobs-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#scheduled-jobs-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/scheduled-jobs/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#scheduled-jobs-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('scheduled-jobs-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#scheduled-jobs-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/scheduled-jobs/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#scheduled-jobs-update-modal-container').html(data); 
                 $('#scheduled-jobs-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
