    <div id='cms-email-events-update-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Изменение почтового события')?> <strong style="color:#0093f5;"> #<?php echo $model->id; ?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cms-email-events-update-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("cmsEmailEvents/update"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
                               'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ update(); } " /* Do ajax call when user presses enter key */
                            ),               
)); ?>
        <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
        <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span9">
			<?php echo $form->hiddenField($model,'id',array()); ?>
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'template'); ?>
                      <?php echo $form->textArea($model,'template',array('rows'=>12, 'cols'=>250)); ?>
					  <?php echo $form->error($model,'template'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'class'); ?>
					  <?php echo $form->textField($model,'class',array('size'=>60,'maxlength'=>255)); ?>
					  <?php echo $form->error($model,'class'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'action'); ?>
					  <?php echo $form->textField($model,'action',array('size'=>60,'maxlength'=>255)); ?>
					  <?php echo $form->error($model,'action'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'condition'); ?>
					  <?php echo $form->textArea($model,'condition',array('rows'=>6, 'cols'=>250)); ?>
					  <?php echo $form->error($model,'condition'); ?>
				  </div>

			  				  <div class="row">
					  <?php echo $form->labelEx($model,'recipients'); ?>
					  <?php echo $form->textArea($model,'recipients',array('rows'=>6, 'cols'=>250)); ?>
					  <?php echo $form->error($model,'recipients'); ?>
				  </div>
                <div class="row">
                    <?php echo $form->labelEx($model,'regular'); ?>
                    <?php echo $form->textField($model,'regular',array('size'=>20,'maxlength'=>20)); ?>
                    <?php echo $form->error($model,'regular'); ?>
                </div>
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'enabled'); ?>
					  <?php echo $form->textField($model,'enabled'); ?>
					  <?php echo $form->error($model,'enabled'); ?>
				  </div>
                <div class="row">
                    <?php echo $form->labelEx($model,'tests'); ?>
                    <?php echo $form->textArea($model,'tests',array('rows'=>6, 'cols'=>250)); ?>
                    <?php echo $form->error($model,'tests'); ?>
                </div>
  </div><!--end modal body-->

</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                //'id'=>'sub2',
                'type'=>'info',
                'icon'=>'ok white',
                'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
                'htmlOptions'=>array('onclick'=>'update();'),
            ));

            ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->
