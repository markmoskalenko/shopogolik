<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_form.php">
 * </description>
 **********************************************************************************************************************/?>
<div class="row-fluid">
    <div class="span10">
<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cms-email-events-form-'.$model->id,
	'enableAjaxValidation'=>false,
        'method'=>'post',
	'type'=>'horizontal',
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data'
	)
)); ?>
     	<fieldset>
                <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

                <div class="control-group">
			<div class="span4">
	<?php echo $form->textAreaRow($model,'template',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'class',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'action',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'condition',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'recipients',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <?php echo $form->textAreaRow($model,'tests',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'enabled',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'regular',array('class'=>'span5','maxlength'=>20)); ?>

                        </div>   
  </div>

                <div class="form-actions">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType'=>'submit',
                        'type'=>'info',
                        'size'=>'mini',
                        'icon'=>'ok white',
                        'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
                        'htmlOptions'=>array('onclick'=>"saveForm('cms-email-events-form-".$model->id."'); return false;",),
                    )); ?>
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'type'=>'danger',
                        'size'=>'mini',
                        'buttonType'=>'reset',
                        'icon'=>'remove white',
                        'label'=>Yii::t('admin','Сброс'),
                    )); ?>
                </div>
            </fieldset>

            <?php $this->endWidget(); ?>

</div>
