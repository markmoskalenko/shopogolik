<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      template		</th>
 		<th width="80px">
		      class		</th>
 		<th width="80px">
		      action		</th>
 		<th width="80px">
		      condition		</th>
 		<th width="80px">
		      recipients		</th>
 		<th width="80px">
		      enabled		</th>
 		<th width="80px">
		      regular		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->template; ?>
		</td>
       		<td>
			<?php echo $row->class; ?>
		</td>
       		<td>
			<?php echo $row->action; ?>
		</td>
       		<td>
			<?php echo $row->condition; ?>
		</td>
       		<td>
			<?php echo $row->recipients; ?>
		</td>
       		<td>
			<?php echo $row->enabled; ?>
		</td>
       		<td>
			<?php echo $row->regular; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
