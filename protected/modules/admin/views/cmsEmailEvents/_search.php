<?php  $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'search-cms-email-events-form',
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
));  ?>


	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'template',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'class',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'action',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'condition',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'recipients',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

<?php echo $form->textAreaRow($model,'tests',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'enabled',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'regular',array('class'=>'span5','maxlength'=>20)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'search white', 'label'=>Yii::t('admin','Поиск'))); ?>
               <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'danger','icon'=>'icon-remove-sign white', 'label'=>Yii::t('admin','Сброс'))); ?>
	</div>

<?php $this->endWidget(); ?>


<?php /* Alexys $cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile(Yii::app()->request->baseUrl.'/themes/admin/css/jquery-ui.css');
*/
?>	
   <script>
	$(".btnreset").click(function(){
		$(":input","#search-cms-email-events-form").each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		if (type == "text" || type == "password" || tag == "textarea") this.value = "";
		else if (type == "checkbox" || type == "radio") this.checked = false;
		else if (tag == "select") this.selectedIndex = "";
	  });
	});
   </script>

