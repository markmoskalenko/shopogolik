<div id="cms-email-events-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#cms-email-events-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsEmailEvents/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#cms-email-events-update-modal').modal('hide');
     //             renderView(data);
                  $.fn.yiiGridView.update('cms-email-events-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#cms-email-events-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsEmailEvents/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#cms-email-events-update-modal-container').html(data); 
                 $('#cms-email-events-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
