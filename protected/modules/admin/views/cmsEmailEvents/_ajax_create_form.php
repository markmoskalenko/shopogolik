
    <div id='cms-email-events-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новое почтовое событие')?></h3>
    </div>
    
    <div class="modal-body">
    
    <div class="form">

   <?php
   
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cms-email-events-create-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("cmsEmailEvents/create"),
	'type'=>'horizontal',
	'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
          'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>

    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>
        		
   <div class="control-group">		
			<div class="span4">
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'template'); ?>
                      <?php echo $form->textArea($model,'template',array('rows'=>12, 'cols'=>150)); ?>
					  <?php echo $form->error($model,'template'); ?>
				  </div>
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'class'); ?>
					  <?php echo $form->textField($model,'class',array('size'=>60,'maxlength'=>255)); ?>
					  <?php echo $form->error($model,'class'); ?>
				  </div>
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'action'); ?>
					  <?php echo $form->textField($model,'action',array('size'=>60,'maxlength'=>255)); ?>
					  <?php echo $form->error($model,'action'); ?>
				  </div>
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'condition'); ?>
					  <?php echo $form->textArea($model,'condition',array('rows'=>6, 'cols'=>150)); ?>
					  <?php echo $form->error($model,'condition'); ?>
				  </div>
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'recipients'); ?>
					  <?php echo $form->textArea($model,'recipients',array('rows'=>6, 'cols'=>150)); ?>
					  <?php echo $form->error($model,'recipients'); ?>
				  </div>
                <div class="row">
                    <?php echo $form->labelEx($model,'tests'); ?>
                    <?php echo $form->textArea($model,'tests',array('rows'=>12, 'cols'=>150)); ?>
                    <?php echo $form->error($model,'tests'); ?>
                </div>

                              <div class="row">
                    <?php echo $form->labelEx($model,'regular'); ?>
                    <?php echo $form->textField($model,'regular',array('size'=>20,'maxlength'=>20)); ?>
                    <?php echo $form->error($model,'regular'); ?>
                  </div>
			  				  <div class="row">
					  <?php echo $form->labelEx($model,'enabled'); ?>
					  <?php echo $form->textField($model,'enabled'); ?>
					  <?php echo $form->error($model,'enabled'); ?>
				  </div>
                        </div>
  </div><!--end modal body-->
  
</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove white',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
   </div><!--end modal footer-->	

        <?php $this->endWidget(); ?>
</div><!--end modal-->

    <!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#cms-email-events-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/cmsEmailEvents/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#cms-email-events-create-modal').modal('hide');
                  //renderView(data);
                    $.fn.yiiGridView.update('cms-email-events-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#cms-email-events-create-form').each (function(){
  this.reset();
   });
  $('#cms-email-events-view-modal').modal('hide');
  $('#cms-email-events-create-modal').modal({
   show:true
  });
}
</script>
