 <div id='cms-email-events-view-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	  <div id="cms-email-events-view-modal-container">
	  </div>
    </div>
</div><!--end modal-->
<script>
function renderView(id)
{
 
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsEmailEvents/view"); ?>',
   data:data,
success:function(data){
                 $('#cms-email-events-view-modal-container').html(data);
                     $('#cms-email-events-view-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>