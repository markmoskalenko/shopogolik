    <?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ajax_view.php">
* </description>
**********************************************************************************************************************/?>
<?php
    echo "<div class='printableArea'>";
    echo "<h1>Ошибка #".$model->id."</h1>";
            $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'info',
            'icon'=>'edit white',
			'label'=>Yii::t('admin','Правка'),
			'htmlOptions'=>array('onclick'=>'renderUpdateForm('.$model->id.');'),
		));
		echo " ";
		    $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'danger',
            'icon'=>'trash white',
			'label'=>Yii::t('admin','Удалить'),
			'htmlOptions'=>array('onclick'=>'delete_record('.$model->id.');'),
		));
		echo " ";
		    $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'button',
			'type'=>'warning',
            'icon'=>'print white',
			'label'=>Yii::t('admin','Печать'),
			'htmlOptions'=>array('onclick'=>'print();'),
		));
    echo "<hr/>";
	         $this->widget('bootstrap.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
						'id',
		                'error_message',
		                'error_description',
		                'error_label',
		                'error_date',
		                'error_request',
			),
		));
	         echo "</div>";