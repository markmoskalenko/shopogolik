<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="excelReport.php">
* </description>
**********************************************************************************************************************/?>
<?php if ($model !== null):?>
<table border="1">

	<tr>
		<th width="80px">
		      id		</th>
 		<th width="80px">
		      error_message		</th>
 		<th width="80px">
		      error_description		</th>
 		<th width="80px">
		      error_label		</th>
 		<th width="80px">
		      error_date		</th>
 		<th width="80px">
		      error_request		</th>
 	</tr>
	<?php foreach($model as $row): ?>
	<tr>
        		<td>
			<?php echo $row->id; ?>
		</td>
       		<td>
			<?php echo $row->error_message; ?>
		</td>
       		<td>
			<?php echo $row->error_description; ?>
		</td>
       		<td>
			<?php echo $row->error_label; ?>
		</td>
       		<td>
			<?php echo $row->error_date; ?>
		</td>
       		<td>
			<?php echo $row->error_request; ?>
		</td>
       	</tr>
     <?php endforeach; ?>
</table>
<?php endif; ?>
