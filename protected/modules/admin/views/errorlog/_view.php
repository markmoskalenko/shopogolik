<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_view.php">
* </description>
**********************************************************************************************************************/?><div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('error_message')); ?>:</b>
	<?php echo CHtml::encode($data->error_message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('error_description')); ?>:</b>
	<?php echo CHtml::encode($data->error_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('error_label')); ?>:</b>
	<?php echo CHtml::encode($data->error_label); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('error_date')); ?>:</b>
	<?php echo CHtml::encode($data->error_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('error_request')); ?>:</b>
	<?php echo CHtml::encode($data->error_request); ?>
	<br />


</div>