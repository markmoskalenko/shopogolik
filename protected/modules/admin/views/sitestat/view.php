<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<?
if (Utils::TransLang() == 'ru') {
//   setlocale(LC_ALL, 'rus');
  $date = date('d.m.Y, H:i:s');
}
else {
  $date = date('d.m.Y, H:i:s');
}
$this->widget('application.components.widgets.ReportsBlock', array(
  'title'=>Yii::t('admin','Аналитика на').' '.$date,
  'group'=>'DEFAULT',
));

$this->widget('application.components.widgets.ReportsBlock', array(
  'title' => Yii::t('admin', 'Статистика на') . ' ' . $date,
  'group' => 'ANALYZE',
));
?>