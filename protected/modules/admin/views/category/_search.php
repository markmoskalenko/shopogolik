<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_search.php">
* </description>
**********************************************************************************************************************/?>
<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>"search-categories-form"
)); ?>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->label($model,'cid'); ?>
            <?php echo $form->textField($model,'cid',array('class'=>'span12')); ?>

            <?php echo $form->label($model,'ru'); ?>
            <?php echo $form->textField($model,'ru',array('class'=>'span12')); ?>

            <?php echo $form->label($model,'onmain'); ?>
            <?php echo $form->checkBox($model,'onmain'); ?>
        </div>
        <div class="span6">
            <?php echo $form->label($model,'parent'); ?>
            <?php echo $form->textField($model,'parent',array('class'=>'span12')); ?>

            <?php echo $form->label($model,'en'); ?>
            <?php echo $form->textField($model,'en',array('class'=>'span12')); ?>

            <?php echo $form->label($model,'status'); ?>
            <?php echo $form->checkBox($model,'status'); ?>
        </div>
    </div>

        <div class="row buttons">
          <? $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            //'id'=>'sub2',
            'type'=>'info',
            'icon'=>'ok white',
            'label'=>Yii::t('admin','Поиск'),
            'htmlOptions'=>array('class'=>'btn-block','onclick'=>'searchCategories(this); return false;'),
          ));
          ?>          
        </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->