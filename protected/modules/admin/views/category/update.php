<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="update.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Категории taobao')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
  Yii::t('admin','Управление'),
);

$this->menu=array(
	array('label'=>Yii::t('admin','Список категорий'), 'url'=>array('index')),
	array('label'=>Yii::t('admin','Просмотр категории'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('admin','Управление категориями'), 'url'=>array('admin')),
);
?>

<h1><?=Yii::t('admin','Изменение категории taobao')?> <strong style="color:#0093f5;">#<?php echo $model->ru; ?></strong></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>