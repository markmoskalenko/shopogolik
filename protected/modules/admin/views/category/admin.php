<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="admin.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Категории')=>array('index'),
  Yii::t('admin','Управление'),
);



?>

<h1><?=Yii::t('admin','Управление категориями taobao')?></h1>
<div>
  <?=Yii::t('admin','Вы можете использовать различные операторы сравнения для поиска категорий.')?> (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
  or <b>=</b>)
</div>
<div  >
  <?php $this->renderPartial('_search',array(
    'model'=>$model,
  ),false,true); ?>
</div><!-- search-form -->
<?
if ($model->parent==null) {
  $id='root';
} else {
  $id=$model->parent;
}

 $this->widget('CTreeView',
  array(
    'id'=>'category-tree-'.$id,
    'data' => $tree
  ));

?>

<?
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'category-grid-'.$id,
	'dataProvider'=>$model->search(),
	'filter'=>$model->search()->model,
  'type'=>'striped bordered condensed',
  'template'=>'{summary}{pager}{items}{pager}',
	'columns'=>array(
		array(
                    'name'=>'cid',
                    'type'=>'raw',
                    'value'=>'($data->is_parent==1)?
                        CHtml::link($data->cid,Yii::app()->createUrl("/admin/category/admin",array("cid"=>$data->cid)), array("onclick"=>"getContent(this,\"'.Yii::t('main','Категории от').' №$data->cid\");return false;","title"=>Yii::t("admin","Просмотр подкатегорий"))):$data->cid',
                ),
		'ru',
		'en',
                array(
                          'name'=>'onmain',
                          'class' => 'CCheckBoxColumn',
                          'checked'=>'$data->onmain==1',
                          'header'=>Yii::t('admin','На главной'),
                          //'disabled'=>'true',
                          'selectableRows'=>0,

                ),
              array(
                'name'=>'status',
                'class' => 'CCheckBoxColumn',
                'checked'=>'$data->status==1',
                'header'=>Yii::t('admin','Вкл.'),
                //'disabled'=>'true',
                'selectableRows'=>0,
                            ),
              array(
                              'type'=>'raw',
                              'value'=>'CHtml::link(Yii::t("admin","редактировать"), array("category/update", "id"=>$data->id), array("onclick"=>"getContent(this,\"'.Yii::t('main','Категория').' №$data->cid\");return false;","title"=>Yii::t("admin","Изменить параметры категории")))',
                          ),
	),
    ));  ?>
