<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="view.php">
* </description>
**********************************************************************************************************************/?>
<?php
$this->breadcrumbs=array(
  Yii::t('admin','Категории')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('admin','Список категорий'), 'url'=>array('index')),
	array('label'=>Yii::t('admin','Управление категориями'), 'url'=>array('admin')),
);
?>

<h1>Просмотр категории #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                'id',
		'cid',
		'ru',
		'en',
		'parent',
                array(
                    'name' => 'is_parent',
                    'type'=>'raw',
                    'value'=>($model->is_parent==1)?Yii::t('admin',"Да"):Yii::t('admin',"Нет"),
                ),
		array(
                    'name' => 'onmain',
                    'type'=>'raw',
                    'value'=>($model->onmain==1)?Yii::t('admin',"Да"):Yii::t('admin',"Нет"),
                ),
		array(
                    'name' => 'status',
                    'type'=>'raw',
                    'value'=>($model->status==1)?Yii::t('admin',"Да"):Yii::t('admin',"Нет"),
                ),		
	),
)); ?>
