<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="_form.php">
* </description>
**********************************************************************************************************************/?>
<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>"category-form-$model->id",
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
  <div class="row">
    <?php echo $form->hiddenField($model,'cid'); ?>
  </div>

  <div class="row">
    <?php echo $form->hiddenField($model,'parent'); ?>
  </div>

  <div class="row">
    <?php echo $form->hiddenField($model,'is_parent'); ?>
  </div>

  <div class="row">
    <?php echo $form->hiddenField($model,'id'); ?>
  </div>

    <div class="row-fluid">
        <div class="span10">
            <?php echo $form->labelEx($model,'url'); ?>
            <?php echo $form->textField($model,'url', array('class'=>'span12','maxlength'=>256)); ?>
            <?php echo $form->error($model,'url'); ?>
        </div>
        <div class="span1">
            <?php echo $form->labelEx($model,'onmain'); ?>
            <?php echo $form->checkBox($model,'onmain', array('class'=>'span12')); ?>
            <?php echo $form->error($model,'onmain'); ?>
        </div>
        <div class="span1">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->checkBox($model,'status', array('class'=>'span12')); ?>
            <?php echo $form->error($model,'status'); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
                <div class="summary"><?=Yii::t('admin','Русский перево1д');?></div><br>
                    <?php echo $form->labelEx($model,'ru'); ?>
                    <?php echo $form->textField($model,'ru',array('class'=>'span12','maxlength'=>256)); ?>
                    <?php echo $form->error($model,'ru'); ?>

                    <?php echo $form->labelEx($model,'page_desc_ru'); ?>
                    <?php echo $form->textArea($model,'page_desc_ru',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'page_desc_ru'); ?>

                    <?php echo $form->labelEx($model,'meta_desc_ru'); ?>
                    <?php echo $form->textArea($model,'meta_desc_ru',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'meta_desc_ru'); ?>

                    <?php echo $form->labelEx($model,'meta_keeword_ru'); ?>
                    <?php echo $form->textArea($model,'meta_keeword_ru',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'meta_keeword_ru'); ?>

                    <?php echo $form->labelEx($model,'page_title_ru'); ?>
                    <?php echo $form->textArea($model,'page_title_ru',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'page_title_ru'); ?>
        </div>
        <div class="span6">
                <div class="summary"><?=Yii::t('admin','Англ. перевод');?></div><br>
                    <?php echo $form->labelEx($model,'en'); ?>
                    <?php echo $form->textField($model,'en',array('class'=>'span12','maxlength'=>256)); ?>
                    <?php echo $form->error($model,'en'); ?>

                    <?php echo $form->labelEx($model,'page_desc_en'); ?>
                    <?php echo $form->textArea($model,'page_desc_en',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'page_desc_en'); ?>

                    <?php echo $form->labelEx($model,'meta_desc_en'); ?>
                    <?php echo $form->textArea($model,'meta_desc_en',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'meta_desc_en'); ?>

                    <?php echo $form->labelEx($model,'meta_keeword_en'); ?>
                    <?php echo $form->textArea($model,'meta_keeword_en',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'meta_keeword_en'); ?>

                    <?php echo $form->labelEx($model,'page_title_en'); ?>
                    <?php echo $form->textArea($model,'page_title_en',array('class'=>'span12','rows'=>5)); ?>
                    <?php echo $form->error($model,'page_title_en'); ?>
        </div>
    </div>






	<div class="row buttons">
      <?    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        //'id'=>'sub2',
        'type'=>'info',
        'icon'=>'ok white',
        'label'=>Yii::t('admin','Сохранить'),
        'htmlOptions'=>array('style'=>'float:right;margin-top:25px;','onclick'=>'saveCategory('.$model->id.'); return false;'),
      ));
      ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->