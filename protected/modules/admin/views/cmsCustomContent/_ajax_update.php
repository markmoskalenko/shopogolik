<div id="cms-custom-content-update-modal-container" >

</div>

<script type="text/javascript">
function update()
 {
  
   var data=$("#cms-custom-content-update-form").serialize();

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsCustomContent/update"); ?>',
   data:data,
success:function(data){
                if(data!="false")
                 {
                  $('#cms-custom-content-update-modal').modal('hide');
                  //renderView(data);
                  $.fn.yiiGridView.update('cms-custom-content-grid', {
                     
                         });
                 }
                 
              },
   error: function(data) { // if error occured
          alert(JSON.stringify(data)); 

    },

  dataType:'html'
  });

}

function renderUpdateForm(id)
{
 
   $('#cms-custom-content-view-modal').modal('hide');
 var data="id="+id;

  jQuery.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("admin/cmsCustomContent/update"); ?>',
   data:data,
success:function(data){
                 // alert("succes:"+data); 
                 $('#cms-custom-content-update-modal-container').html(data); 
                 $('#cms-custom-content-update-modal').modal('show');
              },
   error: function(data) { // if error occured
           alert(JSON.stringify(data)); 
         alert("Error occured, please try again");
    },

  dataType:'html'
  });

}
</script>
