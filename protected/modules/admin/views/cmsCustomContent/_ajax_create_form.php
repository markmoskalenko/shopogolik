    <div id='cms-custom-content-create-modal' class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?=Yii::t('admin','Новый контент блока')?></h3>
    </div>
    <div class="modal-body">
    <div class="form">
   <?php
         $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	    'id'=>'cms-custom-content-create-form',
	    'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'method'=>'post',
        'action'=>array("cmsCustomContent/create"),
	    'type'=>'horizontal',
	    'htmlOptions'=>array(
	                        'onsubmit'=>"return false;",/* Disable normal form submit */
                            ),
                    'clientOptions'=>array(
                    'validateOnType'=>true,
                    'validateOnSubmit'=>true,
                    'afterValidate'=>'js:function(form, data, hasError) {
                                     if (!hasError)
                                        {    
                                          create();
                                        }
                                     }'
            ),
)); ?>

    <h6><?=Yii::t('admin','Поля обозначенные ')?><span class="required">*&nbsp;</span><?=Yii::t('admin','обязательны для заполнения')?></h6>
	<?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->labelEx($model,'content_id'); ?>
                <?php echo $form->textField($model,'content_id',array('class'=>'span12','maxlength'=>255)); ?>
                <?php echo $form->error($model,'content_id'); ?>
            </div>
            <div class="span3">
                <?php echo $form->labelEx($model,'lang'); ?>
                <?php echo $form->textField($model,'lang',array('class'=>'span12','maxlength'=>8)); ?>
                <?php echo $form->error($model,'lang'); ?>
            </div>
            <div class="span3">
                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled',array('class'=>'span12')); ?>
                <?php echo $form->error($model,'enabled'); ?>
            </div>
        </div>



					  <?php echo $form->labelEx($model,'content_data'); ?>
					  <?php echo $form->textArea($model,'content_data',array('rows'=>10, 'class'=>'span10')); ?>
					  <?php echo $form->error($model,'content_data'); ?>


  </div><!--end modal body-->
  
</div>
        <div class="modal-footer">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'info',
                    'icon'=>'ok white',
                    'label'=>$model->isNewRecord ? Yii::t('admin','Добавить') : Yii::t('admin','Сохранить'),
                    'htmlOptions'=>array('onclick'=>'create();'),
                )
            );
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'reset',
                'type'=>'danger',
                'icon'=>'remove white',
                'label'=>Yii::t('admin','Сброс'),
            )); ?>
        </div><!--end modal footer-->
        <?php $this->endWidget(); ?>
</div><!--end modal-->

    <!--Script section-->
<script type="text/javascript">
function create()
 {
   var data=$("#cms-custom-content-create-form").serialize();
  jQuery.ajax({
   type: 'POST',
    url: '<?php
 echo Yii::app()->createAbsoluteUrl("admin/cmsCustomContent/create"); ?>',
   data:data,
success:function(data){
                //alert("succes:"+data); 
                if(data!="false")
                 {
                  $('#cms-custom-content-create-modal').modal('hide');
                  renderView(data);
                    $.fn.yiiGridView.update('cms-custom-content-grid', {
                         });
                 }
              },
   error: function(data) { // if error occured
         alert("Error occured, please try again");
         alert(data);
    },
  dataType:'html'
  });
}
function renderCreateForm()
{
  $('#cms-custom-content-create-form').each (function(){
  this.reset();
   });
  $('#cms-custom-content-view-modal').modal('hide');
  $('#cms-custom-content-create-modal').modal({
   show:true
  });
}
</script>
