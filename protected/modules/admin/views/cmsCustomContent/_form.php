<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="_form.php">
 * </description>
 **********************************************************************************************************************/?>
<div class="row-fluid">
    <div class="span10">
        <div class="form">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'id'=>'cms-custom-content-form-'.(($model->id)?$model->id:$model->content_id),
                'enableAjaxValidation'=>false,
                'method'=>'post',
                'type'=>'horizontal',
                'htmlOptions'=>array(
                'enctype'=>'multipart/form-data'
                )
            )); ?>
            <fieldset>
                <?php echo $form->errorSummary($model,'Opps!!!', null,array('class'=>'alert alert-error span12')); ?>

                <div class="control-group">
                        <div class="row-fluid">
                            <div class="span5">
                                <?php echo $form->textFieldRow($model,'content_id',array('class'=>'span11','maxlength'=>255)); ?>
                            </div>
                            <div class="span2">
                                <?php echo $form->dropDownListRow($model,'lang',cms::getLangList(),array('style'=>'width:50px;','class'=>'','maxlength'=>4)); ?>
                            </div>
                            <div class="span5">
                                <?php echo $form->checkBoxRow($model,'enabled',array('class'=>'')); ?>
                            </div>
                        </div>
                        <div class="row" style="margin: 0 10px;">
                            <?php echo $form->labelEx($model,'content_data'); ?>
                            <?php
                            $editor=new SRichTextarea();
                            $editor->init();
                            $editor->model=$model;
                            $editor->attribute='content_data';
                            $editor->htmlOptions=array('rows'=>20, 'cols'=>280);
                            $editor->run(true);
                            ?>
                            <?php echo $form->error($model,'content_data'); ?>
                        </div>
                        <?// echo $form->textAreaRow($model,'content_data',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
                </div>

                <div class="form-actions">
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType'=>'submit',
                        'type'=>'info',
                        'size'=>'mini',
                        'icon'=>'ok white',
                        'label'=>$model->isNewRecord ? Yii::t('admin','Создать') : Yii::t('admin','Сохранить'),
                        'htmlOptions'=>array('onclick'=>"saveForm('cms-custom-content-form-".(($model->id)?$model->id:$model->content_id)."'); return false;",),
                    )); ?>
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'type'=>'danger',
                        'size'=>'mini',
                        'buttonType'=>'reset',
                        'icon'=>'remove white',
                        'label'=>Yii::t('admin','Сброс'),
                    )); ?>
                </div>
            </fieldset>

            <?php $this->endWidget(); ?>

        </div>
    </div>
    <div class="span2">
        <div id="cms-custom-content-history-<?=$model->id?>">
            <?  $this->widget('application.modules.admin.components.widgets.CmsHistoryBlock', array(
                'id' =>'cms_custom_content',
                'tableName' => 'cms_custom_content',
                'contentId' => $model->content_id,
                'contentLang' =>$model->lang,
                'pageSize' =>50,
            ));
            ?>
        </div>
    </div>
</div>

<div id="cms-custom-content-filemanager-<?=$model->id?>"></div>
<script type="text/javascript" charset="utf-8">
    // Documentation for client options:
    // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
    $(document).ready(function() {
        $('#cms-custom-content-filemanager-<?=$model->id?>').elfinder({
            url : '/admin/fileman/index'  // connector URL (REQUIRED)
            // , lang: 'ru'                    // language (OPTIONAL)
            ,resizable: false
            ,height: '400px'
            ,width: '83%'
            ,showFiles: '2'
        });
    });
</script>