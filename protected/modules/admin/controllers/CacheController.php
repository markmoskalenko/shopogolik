<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CacheController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class CacheController extends CustomAdminController
{

    public function actionIndex()
    {
                $this->pageTitle = Yii::t('admin','Управление кэшем');
        $this->renderPartial('index');
    }

        public function actionClear($all=false)
        {
          if ($all) {
            if ($all==2) {
              MainMenu::clearMenuCache();
              echo Yii::t('admin',"Кеш меню обновлен!");
            } else {
                    Yii::app()->fileCache->flush();
                    Yii::app()->cache->flush();
            echo Yii::t('admin',"Кеш полностью очищен!");
            }
          } else {
            Yii::app()->fileCache->gc();
              Yii::app()->cache->gc();
            echo Yii::t('admin',"Старый кэш очищен!");
          }
        }
}