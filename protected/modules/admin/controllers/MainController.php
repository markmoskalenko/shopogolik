<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="MainController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class MainController extends CustomAdminController {

  public function actionIndex() {
    $userAsManager = Users::getUserAsManager(Yii::app()->user->id);
    $this->render('view', array('userAsManager' => $userAsManager));
  }

  public function actionView() {
    $this->renderPartial('index');
  }

  public function actionDashboard() {
    $userAsManager = Users::getUserAsManager(Yii::app()->user->id);
    $this->renderPartial('view', array('userAsManager' => $userAsManager), FALSE, TRUE);
  }

  public function actionCheckOrder($uid = NULL) {
    if ($uid) {
      $order = Order::model()->find("manager=:uid AND status in ('IN_PROCESS') and round((t.delivery+t.sum),2)>=(select round(ifnull(sum(summ),0),2) as `sum` from orders_payments op
where op.oid = t.id)
            and t.delivery>0", array(':uid' => $uid));
      if ($order) {
        echo $order->id;
      }
    }
    Yii::app()->end();
  }

  public function actionOpen($url,$tabName) {
//http://777.danvit.net/admin/main/open?url=admin/users/view/id/2136&tabName=test@mail.ru
    $userAsManager = Users::getUserAsManager(Yii::app()->user->id);
Yii::app()->clientScript->registerScript('actionOpen', "
getContent('".Yii::app()->createUrl(urldecode($url))."','".urldecode($tabName)."');
",CClientScript::POS_READY);
    $this->render('view', array('userAsManager' => $userAsManager));
  }

  public function actionError() {
    $this->pageTitle = Yii::t('main', 'Ошибка');
    $error = Yii::app()->errorHandler->error;
    if ($error) {
      if ($error['code'] == 403) {
        $this->redirect('/user/login');
        return;
      }
      LogSiteErrors::logError($error['type'] . ' ' . $error['source'] . ' ' . $error['file'] . ': ' . $error['line'], $error['message'], $error['trace']);
      if (Yii::app()->request->isAjaxRequest) {
        /*
            code - the HTTP status code (e.g. 403, 500)
            type - the error type (e.g. 'CHttpException', 'PHP Error')
            message - the error message
            file - the name of the PHP script file where the error occurs
            line - the line number of the code where the error occurs
            trace - the call stack of the error
            source - the context source code where the error occurs

         */
        //print_r($error);
        echo 'Error: ' . $error['type'] . ': ' . $error['message'];
        echo "\n";
        echo $error['source'];
        echo "\n";
        echo $error['file'] . ': ' . $error['line'];
        echo "\n";
        echo $error['trace'];
      }
      else {
        $this->render('error', Array('error' => $error));
      }
    }
  }
}