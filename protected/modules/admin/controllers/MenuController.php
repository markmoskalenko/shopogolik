<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="MenuController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class MenuController extends CustomAdminController {
  public $breadcrumbs;
  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = 'main';

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView() {
    $id = $_REQUEST["id"];

    if (Yii::app()->request->isAjaxRequest) {
      $this->renderPartial('ajax_view', array(
        'model' => $this->loadModel($id),
      ));

    }
    else {
      $this->render('view', array(
        'model' => $this->loadModel($id),
      ));
    }
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new MainMenu;

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "mainmenu-create-form");
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['MainMenu'])) {
        $model->attributes = $_POST['MainMenu'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }
    }
    else {
      if (isset($_POST['MainMenu'])) {
        $model->attributes = $_POST['MainMenu'];
        if ($model->save()) {
          $this->redirect(array('view', 'id' => $model->id));
        }

      }

      $this->render('create', array(
        'model' => $model,
      ));
    }
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate() {

    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : $_REQUEST["MainMenu"]["id"];
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "mainmenu-update-form");

    if (Yii::app()->request->isAjaxRequest) {

      if (isset($_POST['MainMenu'])) {

        $model->attributes = $_POST['MainMenu'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }

      $this->renderPartial('_ajax_update_form', array(
        'model' => $model,
      ));
      return;

    }

    if (isset($_POST['MainMenu'])) {
      $model->attributes = $_POST['MainMenu'];
      if ($model->save()) {
        $this->redirect(array('view', 'id' => $model->id));
      }
    }

    $this->render('update', array(
      'model' => $model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete() {
    $id = $_POST["id"];

    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset(Yii::app()->request->isAjaxRequest)) {
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
      }
      else {
        echo "true";
      }
    }
    else {
      if (!isset($_GET['ajax'])) {
        throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
      }
      else {
        echo "false";
      }
    }
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $session = new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model = new MainMenu('search');
    $model->unsetAttributes(); // clear any default values

    if (isset($_GET['MainMenu'])) {
      $model->attributes = $_GET['MainMenu'];

      if (!empty($model->id)) {
        $criteria->addCondition('id = "' . $model->id . '"');
      }

      if (!empty($model->cid)) {
        $criteria->addCondition('cid = "' . $model->cid . '"');
      }

      if (!empty($model->ru)) {
        $criteria->addCondition('ru = "' . $model->ru . '"');
      }

      if (!empty($model->en)) {
        $criteria->addCondition('en = "' . $model->en . '"');
      }

      if (!empty($model->parent)) {
        $criteria->addCondition('parent = "' . $model->parent . '"');
      }

      if (!empty($model->status)) {
        $criteria->addCondition('status = "' . $model->status . '"');
      }

      if (!empty($model->meta_desc_ru)) {
        $criteria->addCondition('meta_desc_ru = "' . $model->meta_desc_ru . '"');
      }

      if (!empty($model->meta_keeword_ru)) {
        $criteria->addCondition('meta_keeword_ru = "' . $model->meta_keeword_ru . '"');
      }

      if (!empty($model->page_title_ru)) {
        $criteria->addCondition('page_title_ru = "' . $model->page_title_ru . '"');
      }

      if (!empty($model->meta_desc_en)) {
        $criteria->addCondition('meta_desc_en = "' . $model->meta_desc_en . '"');
      }

      if (!empty($model->meta_keeword_en)) {
        $criteria->addCondition('meta_keeword_en = "' . $model->meta_keeword_en . '"');
      }

      if (!empty($model->page_title_en)) {
        $criteria->addCondition('page_title_en = "' . $model->page_title_en . '"');
      }

      if (!empty($model->page_desc_ru)) {
        $criteria->addCondition('page_desc_ru = "' . $model->page_desc_ru . '"');
      }

      if (!empty($model->page_desc_en)) {
        $criteria->addCondition('page_desc_en = "' . $model->page_desc_en . '"');
      }

      if (!empty($model->url)) {
        $criteria->addCondition('url = "' . $model->url . '"');
      }

      if (!empty($model->zh)) {
        $criteria->addCondition('zh = "' . $model->zh . '"');
      }

      if (!empty($model->query)) {
        $criteria->addCondition('query = "' . $model->query . '"');
      }

      if (!empty($model->level)) {
        $criteria->addCondition('level = "' . $model->level . '"');
      }

      if (!empty($model->order_in_level)) {
        $criteria->addCondition('order_in_level = "' . $model->order_in_level . '"');
      }

    }
    $session['MainMenu_records'] = MainMenu::model()->findAll($criteria);
      //Yii::app()->fileCache->delete('MainMenu-admin-getTree');
//      $cache = Yii::app()->fileCache->get('MainMenu-admin-getTree');
//      if (($cache == FALSE)) {
        $tree = MainMenu::getMainMenu(1, Utils::TransLang(), ((isset($_POST['cid']))?$_POST['cid']:1),array(0,1,2,3));
//        Yii::app()->fileCache->set('MainMenu-admin-getTree', array($tree), 60 * 60 * (int) DSConfig::getVal('search_cache_ttl_other'));
//      }
//      else {
//        list($tree) = $cache;
//      }
    $storedCount = Yii::app()->db->createCommand("select count(0) from (SELECT 'x' FROM categories_ext_storage group by store_name) ss")
      ->queryScalar();
    $sql = "SELECT store_name, max(store_date) as max_store_date FROM categories_ext_storage group by store_name order by max_store_date desc";
    $storedDataProvider = new CSqlDataProvider($sql, array(
        'id'             => 'stored_dataProvider',
        'keyField'       => 'store_name',
        'totalItemCount' => $storedCount,
        'pagination'     => array(
          'pageSize' => 40,
        )
      )
    );

    $this->render('index', array(
      'model' => $model,
      'tree'  => $tree,
      'storedDataProvider'=>$storedDataProvider,
    ));

  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new MainMenu('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['MainMenu'])) {
      $model->attributes = $_GET['MainMenu'];
    }

    $this->render('admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = MainMenu::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model, $form_id) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionGenerateExcel() {
    $session = new CHttpSession;
    $session->open();

    if (isset($session['MainMenu_records'])) {
      $model = $session['MainMenu_records'];
    }
    else {
      $model = MainMenu::model()->findAll();
    }

    Yii::app()->request->sendFile(date('YmdHis') . '.xls',
      $this->renderPartial('excelReport', array(
        'model' => $model
      ), TRUE,false,true)
    );
  }

  public function actionGeneratePdf() {
    $session = new CHttpSession;
    $session->open();
    Yii::import('application.extensions.ajaxgii.bootstrap.*');
    require_once('tcpdf/tcpdf.php');
    if (Utils::AppLang() == 'ru') {
      require_once('tcpdf/config/lang/rus.php');
    }
    else {
      require_once('tcpdf/config/lang/eng.php');
    }

    if (isset($session['MainMenu_records'])) {
      $model = $session['MainMenu_records'];
    }
    else {
      $model = MainMenu::model()->findAll();
    }

    $html = $this->renderPartial('expenseGridtoReport', array(
      'model' => $model
    ), TRUE);

    //die($html);

    $pdf = new TCPDF();
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(Yii::app()->name);
    $pdf->SetTitle('MainMenu Report');
    $pdf->SetSubject('MainMenu Report');
    //$pdf->SetKeywords('example, text, report');
    $pdf->SetHeaderData('', 0, "Report", '');
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by " . Yii::app()->name, "");
    $pdf->setHeaderFont(Array('helvetica', '', 8));
    $pdf->setFooterFont(Array('helvetica', '', 6));
    $pdf->SetMargins(15, 18, 15);
    $pdf->SetHeaderMargin(5);
    $pdf->SetFooterMargin(10);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->AddPage();
    $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
    $pdf->LastPage();
    $pdf->Output("MainMenu_002.pdf", "I");
  }

//===========================================================
  public function actionLoadFromTaobao() {
    if (isset($_POST['loadFromTaobao']) && ($_POST['loadFromTaobao'] == 1)) {
      if (MainMenu::updateVirtualFromTaobao()) {
        echo Yii::t('admin', "Каталог полностью обновлен с taobao.");
      }
      else {
        echo Yii::t('admin', "Ошибка обновления каталога с taobao - повторите попытку обновления позже.");
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function actionUpdateHFURL() {
    if (isset($_POST['offset']) && isset($_POST['count']) && isset($_POST['internal'])) {
      echo MainMenu::updateMetaAndHFURLs($_POST['offset'], $_POST['count'], $_POST['internal']);
      return TRUE;
    }
    return FALSE;
  }

  public function actionCreateex($id) {
    $model = new MainMenu();
    /*
      `cid` int(11) DEFAULT NULL,
      `ru` varchar(256) DEFAULT NULL,
      `en` varchar(256) DEFAULT NULL,
      `parent` int(11) DEFAULT NULL,
      `status` smallint(6) DEFAULT '1',
      `meta_desc_ru` varchar(300) DEFAULT NULL,
      `meta_keeword_ru` varchar(250) DEFAULT NULL,
      `page_title_ru` varchar(100) DEFAULT NULL,
      `meta_desc_en` varchar(300) DEFAULT NULL,
      `meta_keeword_en` varchar(250) DEFAULT NULL,
      `page_title_en` varchar(100) DEFAULT NULL,
      `page_desc_ru` text,
      `page_desc_en` text,
      `url` varchar(256) DEFAULT NULL,
      `cn` varchar(256) DEFAULT NULL,
      `query` varchar(256) DEFAULT NULL,
      `level` int(11) DEFAULT NULL,
      `description` longtext,
      `order_in_level` int(11) DEFAULT '0',
    */
    $model->cid = 0;
    $model->ru = Yii::t('main','Новая категория');
    $model->en = 'New category';
    $model->zh = 'New category';
    $model->parent = $id;
    $model->status = 0;
    $model->query = '产品';
    $model->url = 'mainmenu-new-child-category-for-' . $id;

    $model->save();
    $res = new stdClass();
    $res->id = Yii::app()->db->getLastInsertID();
    echo CJSON::encode($res);
    Yii::app()->end();
  }

  public function actionDeleteex($id) {
    $model = MainMenu::model()->findByPk($id);
    if ($model != NULL) {
      $model->delete();
    }
    return;
  }

  public function actionStorage($command, $name) {
     echo MainMenu::storageCommand($command,$name);
  }

}
