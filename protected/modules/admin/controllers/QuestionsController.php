<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="QuestionsController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class QuestionsController extends CustomAdminController {
  public $breadcrumbs;
  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = 'main';

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id) {

    $question = Question::model()->findByPk($id);
    $messages = Message::model()->getQuestionThred($id);
    $this->renderPartial('view', array(
      'question' => $question,
      'messages' => $messages,
    ), FALSE, TRUE);
  }

  public function actionSave() {
    if (isset($_POST['Message'])) {
      $message = new Message;
      $mes['parent'] = $_POST['Message']['id'];
      // статус сообщения "ответ администратора"
      $mes['status'] = 3;
      $mes['question'] = $_POST['Message']['question'];
      $mes['uid'] = Yii::app()->user->id;
      $mes['email'] = Yii::app()->user->email;
      $mes['qid'] = $_POST['Message']['qid'];;
      $mes['date'] = time();
      $message->attributes = $mes;
      //находим вопрос
      $question = Question::model()->findByPk($message->qid);
      if (isset($question->id)) {
        $question->date_change = time();
        if ($question->status == 1) {
          $question->status = 2;
        }
        $question->save(FALSE);
      }

      if ($message->save()) {

        // статус сообщения "есть ответ"
        $message = Message::model()->findByPk($_POST['Message']['id']);
        $message->status = 2;
        $message->save();
        echo Yii::t('admin', "Ответ сохранен");
      }
    }
    else {
      echo Yii::t('admin', "Неверный запрос");
    }
  }

  public function actionIndex() {
    $session = new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();
    $criteria->order = 't.status ASC, t.`date` DESC';
    $model = new Question('search');
    $model->unsetAttributes(); // clear any default values

    if (isset($_GET['Question'])) {
      $model->attributes = $_GET['Question'];
      if (!empty($model->id)) {
        $criteria->addCondition('id = "' . $model->id . '"');
      }
      if (!empty($model->theme)) {
        $criteria->addCondition("theme like '%" . $model->theme . "%'");
      }
      if (!empty($model->date)) {
        $criteria->addCondition("FROM_UNIXTIME(t.date,'%d.%m.%Y %h:%i') like '%" . (string)$model->date . "%'");
      }
//            if (!empty($model->uid)) {
//      $criteria->addCondition('uid = ' . Yii::app()->user->id);
//            }
      if (!empty($model->category)) {
        $criteria->addCondition('category like "%' . $model->category . '%"');
      }
      if (!empty($model->date_change)) {
        $criteria->addCondition("FROM_UNIXTIME(date_change,'%d.%m.%Y %h:%i') like '%" . (string)$model->date_change . "%'");
      }
      if (!empty($model->order_id)) {
        $criteria->addCondition('order_id = "' . $model->order_id . '"');
      }
      if (!empty($model->status)) {
        $criteria->addCondition('status = "' . $model->status . '"');
      }
    }
    $session['Question_records'] = Question::model()->findAll($criteria);

    /*    $dataProvider=new CActiveDataProvider('Question',array(
          'sort'=> array(
            'defaultOrder'=>'date_change IS NULL DESC, date_change desc, date desc',
          ),
          'pagination'=>array(
            'pageSize'=>100,
          ),
        ));
    */
    $this->renderPartial('index', array(
      'model' => $model,
    ), FALSE, TRUE);
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = Question::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model, $form_id) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionGenerateExcel() {
    $session = new CHttpSession;
    $session->open();

    if (isset($session['Question_records'])) {
      $model = $session['Question_records'];
    }
    else {
      $model = Question::model()->findAll();
    }


    Yii::app()->request->sendFile(date('YmdHis') . '.xls',
      $this->renderPartial('excelReport', array(
        'model' => $model
      ), TRUE,false,true)
    );
  }

  public function actionGeneratePdf() {
    $session = new CHttpSession;
    $session->open();
    Yii::import('application.extensions.ajaxgii.bootstrap.*');
    require_once('tcpdf/tcpdf.php');
    if (Utils::AppLang() == 'ru') {
      require_once('tcpdf/config/lang/rus.php');
    }
    else {
      require_once('tcpdf/config/lang/eng.php');
    }

    if (isset($session['Question_records'])) {
      $model = $session['Question_records'];
    }
    else {
      $model = Question::model()->findAll();
    }


    $html = $this->renderPartial('expenseGridtoReport', array(
      'model' => $model
    ), TRUE);

    //die($html);

    $pdf = new TCPDF();
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(Yii::app()->name);
    $pdf->SetTitle('Question Report');
    $pdf->SetSubject('Question Report');
    //$pdf->SetKeywords('example, text, report');
    $pdf->SetHeaderData('', 0, "Report", '');
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by " . Yii::app()->name, "");
    $pdf->setHeaderFont(Array('helvetica', '', 8));
    $pdf->setFooterFont(Array('helvetica', '', 6));
    $pdf->SetMargins(15, 18, 15);
    $pdf->SetHeaderMargin(5);
    $pdf->SetFooterMargin(10);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->AddPage();
    $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
    $pdf->LastPage();
    $pdf->Output("Question_002.pdf", "I");
  }
}