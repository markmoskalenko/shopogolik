<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="OrdersItemsController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class OrdersItemsController extends CustomAdminController
{
    public function actionDashboard()
    {
//=================================================================================
        $uid = null;
        if (Yii::app()->user->inRole(array('superAdmin', 'topManager'))) {
            $manager = null;
        } elseif (Yii::app()->user->inRole(array('orderManager'))) {
            $manager = Yii::app()->user->id;
        } else {
            $uid = Yii::app()->user->id;
            $manager = null;
        }
//=================================================================================
        $ordersItemsByStatusesArray = OrdersItemsStatuses::getAllStatusesListAndItemsCount($uid, $manager);
        $ordersItemsByStatusesDataProvider = new CArrayDataProvider($ordersItemsByStatusesArray, array(
          'id' => 'ordersItemsByStatuses',
            /*      'sort'=>array(
                    'attributes'=>array(
                      'id', 'username', 'email',
                    ),
                  ),
            */
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
        $this->renderPartial(
          'dashboard',
          array(
            'ordersItemsByStatusesDataProvider' => $ordersItemsByStatusesDataProvider,
            'ordersItemsByStatusesArray'        => $ordersItemsByStatusesArray,
          ),
          false,
          true
        );
    }

    /**
     * Lists all models.
     */

    public function actionIndex($type = NULL) {
        $name = OrdersItemsStatuses::getStatusName($type);
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('index', array(
                'type' => $type,
                'name' => $name,
              ), FALSE, TRUE);
        }
        else {
            $this->render('index', array(
                'type' => $type,
                'name' => $name,
              ));
        }
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        try {
            $uid = NULL;
            if (Yii::app()->user->inRole(array('superAdmin', 'topManager'))) {
                $manager = NULL;
            }
            elseif (Yii::app()->user->inRole(array('orderManager'))) {
                $manager = Yii::app()->user->id;
            }
            else {
                $uid = Yii::app()->user->id;
                $manager = NULL;
            }
            $order = OrdersItems::getOrderItem($id, $uid, $manager);
            if (!isset($order)) {
                throw new CHttpException(400, Yii::t('admin', 'У вас нет прав для просмотра даного лота'));
            }
            Yii::app()->controller->pageTitle = Yii::t('admin', 'Лот №') . $order->id . '-' . $id;
            Yii::app()->controller->breadcrumbs = array(
              Yii::t('admin', 'Лоты') => '/admin/ordersItems',
              Yii::app()->controller->pageTitle
            );
//======================================================================================================================
            if(Yii::app()->request->isAjaxRequest && isset($_GET['EventsLog_page'])
                //  &&(strpos($_GET['ajax'],'grid-')===0)
            ) {
                $this->renderPartial('/orders/ordercommentsevents', array(
                    'order' => $order,
                  ), FALSE, TRUE);
                Yii::app()->end();
            }
            $this->renderPartial('/ordersItems/view', array(
                'order' => $order,
              ), FALSE, TRUE);
//      }
        } catch (Exception $e) {
            print_r($e);
            //LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
            //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
            echo $e['message'];
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = OrdersItems::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model, $form_id)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGenerateExcel()
    {
        $session = new CHttpSession;
        $session->open();

        if (isset($session['OrdersItems_records'])) {
            $model = $session['OrdersItems_records'];
        } else {
            $model = OrdersItems::model()->findAll();
        }

        Yii::app()->request->sendFile(
          date('YmdHis') . '.xls',
          $this->renderPartial(
            'excelReport',
            array(
              'model' => $model
            ),
            true,false,true
          )
        );
    }
}
