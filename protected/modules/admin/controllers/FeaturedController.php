<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="FeaturedController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class FeaturedController extends CustomAdminController {
  public $defaultAction = 'index';

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id) {
    $this->render('view', array(
      'model' => $this->loadModel($id),
    ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new Featured;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if (isset($_POST['Featured'])) {
      $model->attributes = $_POST['Featured'];
      if ($model->save()) {
        $this->redirect(array('view', 'id' => $model->num_iid));
      }
    }

    $this->render('create', array(
      'model' => $model,
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id) {
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if (isset($_POST['Featured'])) {
      $model->attributes = $_POST['Featured'];
      if ($model->save()) {
        $this->redirect(array('view', 'id' => $model->num_iid));
      }
    }

    $this->render('update', array(
      'model' => $model,
    ));
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $session=new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();
    $model=new Featured('search');
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['Featured']))
    {
      $model->attributes=$_GET['Featured'];
    }
    $session['Featured_records']=Featured::model()->findAll($criteria);
    $this->renderPartial('index', array(
      'model' => $model,
    ),FALSE,TRUE);
  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new Featured('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['Featured'])) {
      $model->attributes = $_GET['Featured'];
    }

    $this->renderPartial('admin', array(
      'model' => $model,
    ), FALSE, TRUE);
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer $id the ID of the model to be loaded
   * @return Featured the loaded model
   * @throws CHttpException
   */
  public function loadModel($id) {
    $model = Featured::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param Featured $model the model to be validated
   */
  protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'Featured-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionDelete($id = FALSE) {
    if (!$id) {
      throw new CHttpException(404, 'Not Found');
    }
    Yii::app()->db->createCommand()->delete('featured', 'num_iid=:id', array(':id' => $id));
    /*$this->redirect('/admin/featured/index');*/
    echo Yii::t('admin', 'Товар удален из рекомендованного!');
    return;
  }

  public function actionAdd($id = FALSE) {
    if (!$id) {
      throw new CHttpException(404, 'Not Found');
    }
    $intIid = $id;
    $command = Yii::app()->db->createCommand("select 'x' from log_items_requests ii
      where ii.num_iid=:iid  limit 1")
      ->bindParam(':iid', $intIid, PDO::PARAM_STR);
    $res = $command->queryAll();
    if ($res) {
      $command = Yii::app()->db->createCommand("insert IGNORE into featured
    (`num_iid`,`date`,`cid`,`express_fee`,`price`,`promotion_price`,`pic_url`,`seller_rate`,`title_zh`,`title_en`,`title_ru`)
  select
    ii.`num_iid`, Now(), ii.`cid`,ii.`express_fee`,ii.`price`,ii.`promotion_price`,
  ii.`pic_url`, ii.seller_rate, cc.zh, cc.en, cc.ru
  from log_items_requests ii
  left join categories_ext cc on cc.cid=ii.cid and ii.cid<>0
  where ii.num_iid=:iid  limit 1")
        ->bindParam(':iid', $intIid, PDO::PARAM_STR);
      $command->execute();
      echo Yii::t('admin', 'Товар добавлен в рекомендованное!');
      return;
    }
    else {
      $command = Yii::app()->db->createCommand("select 'x' from log_item_requests ii
      where ii.num_iid=:iid  limit 1")
        ->bindParam(':iid', $intIid, PDO::PARAM_STR);
      $res = $command->queryAll();
      if ($res) {
        $command = Yii::app()->db->createCommand("insert IGNORE into featured
    (`num_iid`,`date`,`cid`,`express_fee`,`price`,`promotion_price`,`pic_url`,`seller_rate`,`title_zh`,`title_en`,`title_ru`)
  select
    ii.`num_iid`, Now(), ii.`cid`,ii.`express_fee`,ii.`price`,ii.`promotion_price`,
  ii.`pic_url`, ii.seller_rate, cc.zh, cc.en, cc.ru
  from log_item_requests ii
  left join categories_ext cc on cc.cid=ii.cid and ii.cid<>0
  where ii.num_iid=:iid  limit 1")
          ->bindParam(':iid', $intIid, PDO::PARAM_STR);
        $command->execute();
        echo Yii::t('admin', 'Товар добавлен в рекомендованное!');
        return;
      }
      else {
        echo Yii::t('admin', 'Ошибка добавления в рекомендованное!');
        return;
      }
    }
  }

}