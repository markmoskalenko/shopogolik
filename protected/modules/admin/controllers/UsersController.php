<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="UsersController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class UsersController extends CustomAdminController {
  public $breadcrumbs;
  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = 'main';

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id = FALSE) {
    if (!isset($id) || $id === FALSE) {
      $id = $_REQUEST["id"];

      if (Yii::app()->request->isAjaxRequest) {
        $this->renderPartial('ajax_view', array(
          'model' => $this->loadModel($id),
        ));

      }
      else {
        $this->render('view', array(
          'model' => $this->loadModel($id),
        ));
      }
    }
    else {
      $model = Users::getUser($id);
        $userCart = Cart::getUserCart($id);
      $this->renderPartial('update', array(
        'model' => $model,
        'userCart' => $userCart,
      ), FALSE, TRUE);
    }
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new Users;

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "users-create-form");
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['Users'])) {
        $model->attributes = $_POST['Users'];
        if (strlen($model->password) != 32) {
          $model->password = md5($model->password);
        }
        if ($model->save()) {
          echo $model->uid;
        }
        else {
          echo "false";
        }
        return;
      }
    }
    else {
      if (isset($_POST['Users'])) {
        $model->attributes = $_POST['Users'];
        if ($model->save()) {
          $this->redirect(array('view', 'id' => $model->uid));
        }

      }

      $this->render('create', array(
        'model' => $model,
      ));
    }
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id = FALSE) {
    if ($id === FALSE) {
      $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : $_REQUEST["Users"]["uid"];
    }
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "users-update-form");

    if (Yii::app()->request->isAjaxRequest) {

      if (isset($_POST['Users'])) {

        $model->attributes = $_POST['Users'];
        if (strlen($model->password) != 32) {
          $model->password = md5($model->password);
        }
        if ($model->save()) {
          echo Yii::t('admin', 'Параметры пользователя сохранены');
        }
        else {
          echo Yii::t('admin', 'Ошибка сохранения параметров пользователя');
        }
        return;
      }

      $this->renderPartial('_ajax_update_form', array(
        'model' => $model,
      ));
      return;

    }

    if (isset($_POST['Users'])) {
      $model->attributes = $_POST['Users'];
      if ($model->save()) {
        $this->redirect(array('view', 'id' => $model->uid));
      }
    }

    $this->render('update', array(
      'model' => $model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete() {
    $id = $_POST["id"];

    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset(Yii::app()->request->isAjaxRequest)) {
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
      }
      else {
        echo "true";
      }
    }
    else {
      if (!isset($_GET['ajax'])) {
        throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
      }
      else {
        echo "false";
      }
    }
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $session = new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model = new Users('search');
    $model->unsetAttributes(); // clear any default values

    if (isset($_GET['Users'])) {
      $model->attributes = $_GET['Users'];

      if (!empty($model->uid)) {
        $criteria->addCondition('uid = "' . $model->uid . '"');
      }

      if (!empty($model->sex)) {
        $criteria->addCondition('sex = "' . $model->sex . '"');
      }

      if (!empty($model->firstname)) {
        $criteria->addCondition('firstname like "%' . $model->firstname . '%"');
      }

      if (!empty($model->email)) {
        $criteria->addCondition('email like "%' . $model->email . '%"');
      }

      if (!empty($model->status)) {
        $criteria->addCondition('status = "' . $model->status . '"');
      }

      if (!empty($model->created)) {
        $criteria->addCondition('created = "' . $model->created . '"');
      }

      if (!empty($model->patroname)) {
        $criteria->addCondition('patroname = "' . $model->patroname . '"');
      }

      if (!empty($model->role)) {
        $criteria->addCondition('role like "%' . $model->role . '%"');
      }

      if (!empty($model->lastname)) {
        $criteria->addCondition('lastname like "%' . $model->lastname . '%"');
      }

      if (!empty($model->country)) {
        $criteria->addCondition('country like "%' . $model->country . '%"');
      }

      if (!empty($model->city)) {
        $criteria->addCondition('city like "%' . $model->city . '%"');
      }

      if (!empty($model->index)) {
        $criteria->addCondition('index = "' . $model->index . '"');
      }

      if (!empty($model->address)) {
        $criteria->addCondition('address like "%' . $model->address . '%"');
      }

      if (!empty($model->phone)) {
        $criteria->addCondition('phone like "' . $model->phone . '%"');
      }

      if (!empty($model->alias)) {
        $criteria->addCondition('alias like "%' . $model->alias . '%"');
      }

      if (!empty($model->birthday)) {
        $criteria->addCondition('birthday = "' . $model->birthday . '"');
      }

      if (!empty($model->skidka)) {
        $criteria->addCondition('skidka = "' . $model->skidka . '"');
      }

    }
    $session['Users_records'] = Users::model()->findAll($criteria);

    $this->render('index', array(
      'model' => $model,
    ));

  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new Users('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['Users'])) {
      $model->attributes = $_GET['Users'];
    }

    $this->render('admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = Users::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    return $model;
  }

  public function actionBalance() {
    if (isset($_POST['operation'])) {
      $payment = new Payment;
      if ($_POST['operation'] == 1) {
        $payment->sum = (float) $_POST['sum'];
      }
      else {
        $payment->sum = 0 - (float) $_POST['sum'];
      }
      $payment->description = $_POST['desc'];
      $payment->uid = $_POST['Users']['uid'];
      $payment->date = time();
      $payment->status = $_POST['operation'];
      if ($payment->save()) {
          if ($_POST['operation'] == 1) {
              $mess=Yii::t('admin', "На счет добавлено ") . Formulas::priceWrapper($_POST['sum'],DSConfig::getVal('site_currency'));
          }
          else {
              $mess=Yii::t('admin', "Со счета списано ") . Formulas::priceWrapper($_POST['sum'],DSConfig::getVal('site_currency'));
          }
      }
      else {
        $mess=Yii::t('admin', 'Произошла ошибка пополнения счёта');
      }
        Yii::app()->user->setFlash('user', $mess);
        echo $mess;
    }
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model, $form_id) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionGenerateMailList($criteria='1=1'){
    header('Content-Encoding: UTF-8');
    header('Content-type: text/plain; charset=UTF-8');
    $filename = 'users.txt';
    header('Content-Disposition: attachment; filename="'.$filename.'"'); //charset=UTF-8;
    //$csv="\xEF\xBB\xBF";
    $users=Users::model()->findAllBySql('select uu.email,uu.firstname,uu.lastname from users uu where '.$criteria);
    if ($users) {
      foreach ($users as $user) {
        echo $user->firstname.' '.$user->lastname.' <'.$user->email.'>'."\r\n";
      }
    }
    Yii::app()->end();
  }

  public function actionGenerateExcel() {
    $session = new CHttpSession;
    $session->open();

    if (isset($session['Users_records'])) {
      $model = $session['Users_records'];
    }
    else {
      $model = Users::model()->findAll();
    }

    Yii::app()->request->sendFile(date('YmdHis') . '.xls',
      $this->renderPartial('excelReport', array(
        'model' => $model
      ), TRUE,false,true)
    );
  }

  public function actionGeneratePdf() {
    $session = new CHttpSession;
    $session->open();
    Yii::import('application.extensions.ajaxgii.bootstrap.*');
    require_once('tcpdf/tcpdf.php');
    if (Utils::AppLang() == 'ru') {
      require_once('tcpdf/config/lang/rus.php');
    }
    else {
      require_once('tcpdf/config/lang/eng.php');
    }

    if (isset($session['Users_records'])) {
      $model = $session['Users_records'];
    }
    else {
      $model = Users::model()->findAll();
    }

    $html = $this->renderPartial('expenseGridtoReport', array(
      'model' => $model
    ), TRUE);

    //die($html);

    $pdf = new TCPDF();
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(DSConfig::getVal('site_name'));
    $pdf->SetTitle('Users Report');
    $pdf->SetSubject('Users Report');
    //$pdf->SetKeywords('example, text, report');
    $pdf->SetHeaderData('', 0, "Report", '');
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by " . DSConfig::getVal('site_name'), "");
    $pdf->setHeaderFont(Array('helvetica', '', 8));
    $pdf->setFooterFont(Array('helvetica', '', 6));
    $pdf->SetMargins(15, 18, 15);
    $pdf->SetHeaderMargin(5);
    $pdf->SetFooterMargin(10);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->AddPage();
    $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
    $pdf->LastPage();
    $pdf->Output("Users_002.pdf", "I");
  }
}
