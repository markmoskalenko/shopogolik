<?php

class CmsCustomContentController extends CustomAdminController {
  public $breadcrumbs;
  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = 'main';

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView() {
    $id = $_REQUEST["id"];

    if (Yii::app()->request->isAjaxRequest) {
      $this->renderPartial('ajax_view', array(
        'model' => $this->loadModel($id),
      ));

    }
    else {
      $this->render('view', array(
        'model' => $this->loadModel($id),
      ));
    }
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new CmsCustomContent;

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "cmscustomcontent-create-form");
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['CmsCustomContent'])) {
        $model->attributes = $_POST['CmsCustomContent'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }
    }
    else {
      if (isset($_POST['CmsCustomContent'])) {
        $model->attributes = $_POST['CmsCustomContent'];
        if ($model->save()) {
          $this->redirect(array('view', 'id' => $model->id));
        }

      }

      $this->render('create', array(
        'model' => $model,
      ));
    }
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id = FALSE) {
      try {
    if ($id == FALSE) {
      $standalone = FALSE;
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : $_REQUEST["CmsCustomContent"]["id"];
    $enabled = isset($_REQUEST["enabled"]) ? $_REQUEST["enabled"] : $_REQUEST["CmsCustomContent"]["enabled"];
    }
    else {
      $standalone = TRUE;
    }
    $model = $this->loadModel($id,true);
    if (!$model) {
      $model = new CmsCustomContent();
      $model->content_id=$id;
      $model->enabled=(isset($_REQUEST["CmsCustomContent"]["enabled"])?$_REQUEST["CmsCustomContent"]["enabled"]:0);
      $model->lang=(isset($_REQUEST["CmsCustomContent"]["lang"])?$_REQUEST["CmsCustomContent"]["lang"]:'*');
    }

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "cmscustomcontent-update-form");

    if (Yii::app()->request->isAjaxRequest) {

      if (isset($_POST['CmsCustomContent'])) {

        $model->attributes=$_POST['CmsCustomContent'];
        if ($model->save()) {
          if (!$standalone) {
          echo $model->id;
        }
        else {
            echo Yii::t('admin', 'Параметры сохранены');
          }
        }
        else {
          echo "false";
        }
        return;
      }
      if (!$standalone) {
      $this->renderPartial('_ajax_update_form', array(
        'model' => $model,
      ));
      }
      else {
        $this->renderPartial('update', array(
          'model' => $model,
        ), FALSE, TRUE);
      }

      return;

    }

    if (isset($_POST['CmsCustomContent'])) {
      $model->attributes = $_POST['CmsCustomContent'];
      if ($model->save()) {
        // $this->redirect(array('view', 'id' => $model->id));
      }
    }
    else {
      $this->renderPartial('update', array(
      'model' => $model,
      ), FALSE, TRUE);
    }
      } catch (Exception $e) {
          print_r($e);
      }
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete() {
    $id = $_POST["id"];

    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset(Yii::app()->request->isAjaxRequest)) {
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
      }
      else {
        echo "true";
      }
    }
    else {
      if (!isset($_GET['ajax'])) {
        throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
      }
      else {
        echo "false";
      }
    }
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $session = new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model = new CmsCustomContent('search');
    $model->unsetAttributes(); // clear any default values

    if (isset($_GET['CmsCustomContent'])) {
      $model->attributes = $_GET['CmsCustomContent'];

      if (!empty($model->id)) {
        $criteria->addCondition('id = "' . $model->id . '"');
      }

      if (!empty($model->content_id)) {
        $criteria->addCondition('content_id = "' . $model->content_id . '"');
      }

      if (!empty($model->lang)) {
        $criteria->addCondition('lang = "' . $model->lang . '"');
      }

      if (!empty($model->content_data)) {
        $criteria->addCondition('content_data = "' . $model->content_data . '"');
      }

      if (!empty($model->enabled)) {
        $criteria->addCondition('enabled = "' . $model->enabled . '"');
      }

    }
    $session['CmsCustomContent_records'] = CmsCustomContent::model()->findAll($criteria);

    $this->render('index', array(
      'model' => $model,
    ));

  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new CmsCustomContent('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['CmsCustomContent'])) {
      $model->attributes = $_GET['CmsCustomContent'];
    }

    $this->render('admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id,$force=false) {
    if ($force) {
      $model = CmsCustomContent::model()->findBySql('select * from cms_custom_content where id=:id or content_id=:id',array(':id'=>$id));
    } else {
      $model = CmsCustomContent::model()->findByPk($id);
    }
    if ($model === NULL &&(!$force)) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }

    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model, $form_id) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionGenerateExcel() {
    $session = new CHttpSession;
    $session->open();

    if (isset($session['CmsCustomContent_records'])) {
      $model = $session['CmsCustomContent_records'];
    }
    else {
      $model = CmsCustomContent::model()->findAll();
    }

    Yii::app()->request->sendFile(date('YmdHis') . '.xls',
      $this->renderPartial('excelReport', array(
        'model' => $model
      ), TRUE,false,true)
    );
  }

  public function actionGeneratePdf() {
    $session = new CHttpSession;
    $session->open();
    Yii::import('application.extensions.ajaxgii.bootstrap.*');
    require_once('tcpdf/tcpdf.php');
    if (Utils::AppLang() == 'ru') {
      require_once('tcpdf/config/lang/rus.php');
    }
    else {
      require_once('tcpdf/config/lang/eng.php');
    }

    if (isset($session['CmsCustomContent_records'])) {
      $model = $session['CmsCustomContent_records'];
    }
    else {
      $model = CmsCustomContent::model()->findAll();
    }

    $html = $this->renderPartial('expenseGridtoReport', array(
      'model' => $model
    ), TRUE);

    //die($html);

    $pdf = new TCPDF();
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(Yii::app()->name);
    $pdf->SetTitle('CmsCustomContent Report');
    $pdf->SetSubject('CmsCustomContent Report');
    //$pdf->SetKeywords('example, text, report');
    $pdf->SetHeaderData('', 0, "Report", '');
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by " . Yii::app()->name, "");
    $pdf->setHeaderFont(Array('helvetica', '', 8));
    $pdf->setFooterFont(Array('helvetica', '', 6));
    $pdf->SetMargins(15, 18, 15);
    $pdf->SetHeaderMargin(5);
    $pdf->SetFooterMargin(10);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->AddPage();
    $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
    $pdf->LastPage();
    $pdf->Output("CmsCustomContent_002.pdf", "I");
  }
}
