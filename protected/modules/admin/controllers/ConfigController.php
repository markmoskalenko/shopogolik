<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="ConfigController.php">
* </description>
**********************************************************************************************************************/?>
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
class ConfigController extends CustomAdminController {

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id = FALSE) {
    if (!$id) {
      $id = $_REQUEST["id"];
    }
    if (Yii::app()->request->isAjaxRequest) {
      $this->renderPartial('ajax_view', array(
        'model' => $this->loadModel($id),
      ));

    }
    else {
      $this->render('view', array(
        'model' => $this->loadModel($id),
      ));
    }
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new DSConfig;

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "config-create-form");
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['DSConfig'])) {
        $model->attributes = $_POST['DSConfig'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }
    }
    else {
      if (isset($_POST['DSConfig'])) {
        $model->attributes = $_POST['DSConfig'];
        if ($model->save()) {
          $this->redirect(array('view', 'id' => $model->id));
        }

      }

      $this->render('create', array(
        'model' => $model,
      ));
    }
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id = FALSE) {
    if ($id == FALSE) {
      $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : $_REQUEST["DSConfig"]["id"];
      $newTab = FALSE;
    }
    else {
      $newTab = TRUE;
    }
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "config-update-form");

    if (Yii::app()->request->isAjaxRequest) {

      if (isset($_POST['DSConfig'])) {

        $model->attributes = $_POST['DSConfig'];
        if ($model->save()) {
          if (!$newTab) {
            echo $model->id;
          }
          else {
            echo Yii::t('admin', 'Параметр сохранен.');
          }
        }
        else {
          echo "false";
        }
        return;
      }
      if (!$newTab) {
        $this->renderPartial('_ajax_update_form', array(
          'model' => $model,
        ));
      }
      else {
        $this->renderPartial('extupdate', array(
          'settings' => $model,
        ), FALSE, TRUE);
      }

      return;

    }


    if (isset($_POST['DSConfig'])) {
      $model->attributes = $_POST['DSConfig'];
//		    if(
      $model->save();
//            )
//			    $this->redirect(array('view','id'=>$model->id));
    }
    else {

      $this->renderPartial('update', array(
        'model' => $model,
      ), FALSE, TRUE);
    }
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id = FALSE) {
    if (!$id) {
      $id = $_POST["id"];
    }
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset(Yii::app()->request->isAjaxRequest)) {
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
      }
      else {
        echo "true";
      }
    }
    else {
      if (!isset($_GET['ajax'])) {
        throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
      }
      else {
        echo "false";
      }
    }
  }

  /**
   * Lists all models.
   */

  public function actionIndex() {
    $session = new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model = new DSConfig('search');
    $model->unsetAttributes(); // clear any default values

    if (isset($_GET['DSConfig'])) {
      $model->attributes = $_GET['DSConfig'];
      if (!empty($model->id)) {
        $criteria->addCondition('id = "' . $model->id . '"');
      }
      if (!empty($model->label)) {
        $criteria->addCondition('label = "' . $model->label . '"');
      }
      if (!empty($model->value)) {
        $criteria->addCondition('value = "' . $model->value . '"');
      }
      if (!empty($model->default_value)) {
        $criteria->addCondition('default_value = "' . $model->default_value . '"');
      }
      if (!empty($model->in_wizard)) {
        $criteria->addCondition('in_wizard = "' . $model->in_wizard . '"');
      }
    }
    $session['Config_records'] = DSConfig::model()->findAll($criteria);
    $this->renderPartial('index', array(
      'model' => $model,
    ), FALSE, TRUE);
  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new DSConfig('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['DSConfig'])) {
      $model->attributes = $_GET['DSConfig'];
    }

    $this->renderPartial('admin', array(
      'model' => $model,
    ), FALSE, TRUE);
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = DSConfig::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, Yii::t('admin', 'Запрашиваемая страница не найдена.'));
    }
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model, $form_id) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionGenerateExcel() {
    $session = new CHttpSession;
    $session->open();

    if (isset($session['Config_records'])) {
      $model = $session['Config_records'];
    }
    else {
      $model = DSConfig::model()->findAll();
    }


    Yii::app()->request->sendFile(date('YmdHis') . '.xls',
      $this->renderPartial('excelReport', array(
        'model' => $model
      ), TRUE,false,true)
    );
  }

  public function actionGeneratePdf() {
    $session = new CHttpSession;
    $session->open();
    Yii::import('application.extensions.ajaxgii.bootstrap.*');
    require_once('tcpdf/tcpdf.php');
    if (Utils::AppLang() == 'ru') {
      require_once('tcpdf/config/lang/rus.php');
    }
    else {
      require_once('tcpdf/config/lang/eng.php');
    }

    if (isset($session['Config_records'])) {
      $model = $session['Config_records'];
    }
    else {
      $model = DSConfig::model()->findAll();
    }


    $html = $this->renderPartial('expenseGridtoReport', array(
      'model' => $model
    ), TRUE);

    //die($html);

    $pdf = new TCPDF();
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(Yii::app()->name);
    $pdf->SetTitle('DSConfig Report');
    $pdf->SetSubject('DSConfig Report');
    //$pdf->SetKeywords('example, text, report');
    $pdf->SetHeaderData('', 0, "Report", '');
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by " . Yii::app()->name, "");
    $pdf->setHeaderFont(Array('helvetica', '', 8));
    $pdf->setFooterFont(Array('helvetica', '', 6));
    $pdf->SetMargins(15, 18, 15);
    $pdf->SetHeaderMargin(5);
    $pdf->SetFooterMargin(10);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->AddPage();
    $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
    $pdf->LastPage();
    $pdf->Output("Config_002.pdf", "I");
  }

  public function actionPrices() {
    $session = new CHttpSession;
    $session->open();
//    $criteria = new CDbCriteria();
    $model = new DSConfig('search');
    $model->unsetAttributes(); // clear any default values

    $criteria = new CDbCriteria;
//    $criteria->select = 'label,id,value';
    $criteria->condition = "id LIKE 'rate\_%'";
    $criteria->order = 'id ASC';
    $currencyRates = new CActiveDataProvider(DSConfig::model(), array(
      'criteria' => $criteria,
      'pagination' => array(
        'pageSize' => 200,
      ),
    ));

    $criteria = new CDbCriteria;
//    $criteria->select = 'label,id,value';
    $criteria->condition = 'id REGEXP \'^price_[0-9]+_[0-9]+\'';
    $criteria->order = 'SUBSTRING_INDEX(SUBSTRING_INDEX(id,\'_\',2),\'_\',-1)+0';
    $priceRates = new CActiveDataProvider(DSConfig::model(), array(
      'criteria' => $criteria,
      'pagination' => array(
        'pageSize' => 200,
      ),
    ));

    $criteria = new CDbCriteria;
//    $criteria->select = 'label,id,value';
    $criteria->condition = 'id REGEXP \'^skidka_[0-9]+_[0-9]+\'';
    $criteria->order = 'SUBSTRING_INDEX(SUBSTRING_INDEX(id,\'_\',2),\'_\',-1)+0';
    $countRates = new CActiveDataProvider(DSConfig::model(), array(
      'criteria' => $criteria,
      'pagination' => array(
        'pageSize' => 200,
      ),
    ));

    $criteria = new CDbCriteria;
//    $criteria->select = 'label,id,value';
    $criteria->condition = "id = 'price_main_k'";
    $mainK = new CActiveDataProvider(DSConfig::model(), array(
      'criteria' => $criteria,
      'pagination' => array(
        'pageSize' => 200,
      ),
    ));

    if (isset($_GET['DSConfig'])) {
      $model->attributes = $_GET['DSConfig'];
      if (!empty($model->id)) {
        $criteria->addCondition('id = "' . $model->id . '"');
      }
      if (!empty($model->label)) {
        $criteria->addCondition('label = "' . $model->label . '"');
      }
      if (!empty($model->value)) {
        $criteria->addCondition('value = "' . $model->value . '"');
      }
      if (!empty($model->default_value)) {
        $criteria->addCondition('default_value = "' . $model->default_value . '"');
      }
      if (!empty($model->in_wizard)) {
        $criteria->addCondition('in_wizard = "' . $model->in_wizard . '"');
      }
    }
    $session['Config_records'] = DSConfig::model()->findAll($criteria);
    $this->renderPartial('prices', array(
      'model' => $model,
      'currencyRates' => $currencyRates,
      'priceRates' => $priceRates,
      'countRates' => $countRates,
      'mainK' => $mainK,
    ), FALSE, TRUE);
  }

  public function actionDelivery() {
    $settings = new CDbCriteria;
    $settings->select = 'label,id,value';
    $settings->condition = "id LIKE 'delivery%'";
    $settings->order = 'id ASC';
    $settingsdata = DSConfig::model()->findAll($settings);
//print_r($systemsettings); die;
    $this->renderPartial('delivery', array(
      'settings' => $settingsdata
    ), FALSE, TRUE);
  }

  public function actionSysSettings() {
    $syssettings = new CDbCriteria;
    $syssettings->select = 'label,id,value';
    $syssettings->condition = "id NOT LIKE 'rate_%' AND id NOT LIKE 'price_%'
    AND id NOT LIKE 'skidka_%' AND id NOT LIKE 'ext_%' AND id NOT LIKE 'delivery%' AND id NOT LIKE 'keys%'";
    // AND id NOT LIKE 'keys%'
    $syssettings->order = 'id ASC';
    $systemsettings = DSConfig::model()->findAll($syssettings);
//print_r($systemsettings); die;
    $this->renderPartial('syssettings', array(
      'systemsettings' => $systemsettings
    ), FALSE, TRUE);
  }

  public function actionExtSettings() {
    $settings = new CDbCriteria;
    $settings->select = 'label,id,value';
    $settings->condition = "id LIKE 'ext_%'";
    $settings->order = 'id ASC';
    $settingsdata = DSConfig::model()->findAll($settings);
//print_r($systemsettings); die;
    $this->renderPartial('extsettings', array(
      'settings' => $settingsdata
    ), FALSE, TRUE);
  }

  public function actionCatParser() {
    $settingsdata = DSConfig::model()->findByPk('search_CategoriesUpdate');
    if (isset($_POST['DSConfig'])) {
      $settingsdata->attributes = $_POST['DSConfig'];
      //$model->value = (float)strtr($model->value,array(','=>'.'));
      $settingsdata->save();
      if (Yii::app()->request->isAjaxRequest) {
        echo Yii::t('admin', "Запись сохранена");
        return;
      }
    }
    $this->renderPartial('extupdate', array(
      'settings' => $settingsdata
    ), FALSE, TRUE);
  }

  public function actionSearchParser() {
    $settingsdata = DSConfig::model()->findByPk('search_DropShop_grabbers');
    if (isset($_POST['DSConfig'])) {
      $settingsdata->attributes = $_POST['DSConfig'];
      //$model->value = (float)strtr($model->value,array(','=>'.'));
      $settingsdata->save();
      if (Yii::app()->request->isAjaxRequest) {
        echo Yii::t('admin', "Запись сохранена");
        return;
      }
    }
    $this->renderPartial('parserupdate', array(
      'settings' => $settingsdata
    ), FALSE, TRUE);
  }

  public function actionUpdateParamFromProxy($param) {
    try {

      $model = $this->loadModel($param);
      if (!$model) {
        echo Yii::t('admin', 'Ошибка обновления!');
      }
      //http://proxy.dropshop.pro/Updates/get/name/search_CategoriesUpdate
      $url = 'http://' . DSConfig::getVal('proxy_address') . '/Updates/get/name/' . $param;
      $res = Utils::getHttpDocument($url);
      if (isset($res->data)) {
        $new_xml = simplexml_load_string($res->data, NULL, LIBXML_NOCDATA);
        $xml = simplexml_load_string(DSConfig::getVal($param), NULL, LIBXML_NOCDATA);
        if (isset($xml->attributes()->ver) && (isset($new_xml->attributes()->ver))) {
          if ((string) $xml->attributes()->ver == (string) $new_xml->attributes()->ver) {
            //-----------------
            echo Yii::t('admin', 'Версии совпадают, обновление не требуется') . ' ( ver.' . (string) $new_xml->attributes()->ver . ')';
            //-----------------
          }
          else {
            $model->value = $res->data;
            $model->save();
            echo Yii::t('admin', 'Обновлено до версии ') . (string) $new_xml->attributes()->ver;
          }
        }
      }
    } catch (Exception $e) {
      echo Yii::t('admin', 'Ошибка обновления!');
    }
  }


}
