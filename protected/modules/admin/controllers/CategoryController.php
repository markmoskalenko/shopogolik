<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CategoryController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class CategoryController extends CustomAdminController {
  public $defaultAction = 'admin';

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id) {
    $this->renderPartial('view', array(
      'model' => $this->loadModel($id),
    ), FALSE, TRUE);
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new Category;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if (isset($_POST['Category'])) {
      $model->attributes = $_POST['Category'];
      if ($model->save()) {
        echo Yii::t('admin', "Запись сохранена");
      }
    }
    else {
      $this->renderPartial('create', array(
        'model' => $model,
      ), FALSE, TRUE);
    }


  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id) {
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if (isset($_POST['Category'])) {
      $model->attributes = $_POST['Category'];
      if ($model->save()) /* $this->redirect(array('view','id'=>$model->id));*/ {
        MainMenu::clearMenuCache();
        echo Yii::t('admin', "Запись сохранена");
      }
    }
    else {
      $this->renderPartial('update', array(
        'model' => $model,
      ), FALSE, TRUE);
    }


  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id) {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax'])) {
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
      }
    }
    else {
      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $dataProvider = new CActiveDataProvider('Category');
    $this->renderPartial('index', array(
      'dataProvider' => $dataProvider,
    ), FALSE, TRUE);
  }

  /**
   * Manages all models.
   */
  public function actionAdmin($cid = NULL) {
    $model = new Category('search');
    $model->unsetAttributes(); // clear any default values
    $model->parent = $cid;

    if (isset($_POST['Category'])) {
      $model->attributes = $_POST['Category'];
    }
    elseif (isset($_GET['Category'])) {
      $model->attributes = $_GET['Category'];
    }
    if (!$model->onmain) {
      $model->unsetAttributes(array('onmain'));
    }
    if (!$model->status) {
      $model->unsetAttributes(array('status'));
    }
    if ($cid == NULL) {
      $tree = Category::getTree(FALSE, Utils::TransLang(), 1, 0, 2);
    }
    else {
      $tree = Category::getTree(FALSE, Utils::TransLang(), $cid, 0, 3);
    }
//      Yii::app()->clientscript->scriptMap['ds.admin.interface.js'] = false;
//      Yii::app()->clientscript->scriptMap['ds.admin.logic.js'] = false;
    $this->renderPartial('admin', array(
      'model' => $model,
      'tree' => $tree
    ), FALSE, TRUE);
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = Category::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'category-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }
}
