<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="PaymentsController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class PaymentsController extends CustomAdminController {
  public $breadcrumbs;
  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = 'main';

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView() {
    $id = $_REQUEST["id"];

    if (Yii::app()->request->isAjaxRequest) {
      $this->renderPartial('ajax_view', array(
        'model' => $this->loadModel($id),
      ));

    }
    else {
      $this->render('view', array(
        'model' => $this->loadModel($id),
      ));
    }
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new Payment;

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "payment-create-form");
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['Payment'])) {
        $model->attributes = $_POST['Payment'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }
    }
    else {
      if (isset($_POST['Payment'])) {
        $model->attributes = $_POST['Payment'];
        if ($model->save()) {
          $this->redirect(array('view', 'id' => $model->id));
        }

      }

      $this->render('create', array(
        'model' => $model,
      ));
    }
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate() {

    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : $_REQUEST["Payment"]["id"];
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "payment-update-form");

    if (Yii::app()->request->isAjaxRequest) {

      if (isset($_POST['Payment'])) {

        $model->attributes = $_POST['Payment'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }

      $this->renderPartial('_ajax_update_form', array(
        'model' => $model,
      ));
      return;

    }

    if (isset($_POST['Payment'])) {
      $model->attributes = $_POST['Payment'];
      if ($model->save()) {
        $this->redirect(array('view', 'id' => $model->id));
      }
    }

    $this->render('update', array(
      'model' => $model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete() {
    $id = $_POST["id"];

    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset(Yii::app()->request->isAjaxRequest)) {
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
      }
      else {
        echo "true";
      }
    }
    else {
      if (!isset($_GET['ajax'])) {
        throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
      }
      else {
        echo "false";
      }
    }
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $session = new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model = new Payment('search');
    $model->unsetAttributes(); // clear any default values

    if (isset($_GET['Payment']) && is_array($_GET['Payment'])) {
      $model->attributes = $_GET['Payment'];

//      $model->text_status=$_GET['Payment']['text_status'];
      if (isset($_GET['Payment']['email']))
      $model->email=$_GET['Payment']['email'];
      if (isset($_GET['Payment']['username']))
      $model->username=$_GET['Payment']['username'];
      if (isset($_GET['Payment']['phone']))
      $model->phone=$_GET['Payment']['phone'];
      if (!empty($model->id)) {
        $criteria->addCondition('id = "' . $model->id . '"');
      }
      if (!empty($model->sum)) {
        $criteria->addCondition('sum = "' . $model->sum . '"');
      }
      if (!empty($model->description)) {
        $criteria->addCondition('description like "%' . $model->description . '%"');
      }
     if (!empty($model->status)) {
        $criteria->addCondition('t.status = "' . $model->status . '"');
      }
/*      if (!empty($model->email)) {
        $criteria->addCondition('u.email like "%' . $model->email . '%"');
      }
      if (!empty($model->username)) {
        $criteria->addCondition('u.username like "%' . $model->username . '%"');
      }
      if (!empty($model->phone)) {
        $criteria->addCondition('u.phone like "%' . $model->phone . '%"');
      }
*/
      if (!empty($model->date)) {
        $criteria->addCondition("FROM_UNIXTIME(date,'%Y-%m-%d %h:%i') like '%" . $model->text_date . "%'");
      }
      if (!empty($model->uid)) {
        $criteria->addCondition('t.uid = "' . $model->uid . '"');
      }
      if (!empty($model->check_summ)) {
        $criteria->addCondition('check_summ = "' . $model->check_summ . '"');
      }

    }
    $session['Payment_records'] = Payment::model()->findAll($criteria);

    $this->render('index', array(
      'model' => $model,
    ));

  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new Payment('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['Payment'])) {
      $model->attributes = $_GET['Payment'];
    }

    $this->render('admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = Payment::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model, $form_id) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionGenerateExcel() {
    $session = new CHttpSession;
    $session->open();

    if (isset($session['Payment_records'])) {
      $model = $session['Payment_records'];
    }
    else {
      $model = Payment::model()->findAll();
    }

    Yii::app()->request->sendFile(date('YmdHis') . '.xls',
      $this->renderPartial('excelReport', array(
        'model' => $model
      ), TRUE,false,true)
    );
  }

  public function actionGeneratePdf() {
    $session = new CHttpSession;
    $session->open();
    Yii::import('application.extensions.ajaxgii.bootstrap.*');
    require_once('tcpdf/tcpdf.php');
    if (Utils::AppLang() == 'ru') {
      require_once('tcpdf/config/lang/rus.php');
    }
    else {
      require_once('tcpdf/config/lang/eng.php');
    }

    if (isset($session['Payment_records'])) {
      $model = $session['Payment_records'];
    }
    else {
      $model = Payment::model()->findAll();
    }

    $html = $this->renderPartial('expenseGridtoReport', array(
      'model' => $model
    ), TRUE);

    //die($html);

    $pdf = new TCPDF();
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(Yii::app()->name);
    $pdf->SetTitle('Payment Report');
    $pdf->SetSubject('Payment Report');
    //$pdf->SetKeywords('example, text, report');
    $pdf->SetHeaderData('', 0, "Report", '');
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by " . Yii::app()->name, "");
    $pdf->setHeaderFont(Array('helvetica', '', 8));
    $pdf->setFooterFont(Array('helvetica', '', 6));
    $pdf->SetMargins(15, 18, 15);
    $pdf->SetHeaderMargin(5);
    $pdf->SetFooterMargin(10);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->AddPage();
    $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
    $pdf->LastPage();
    $pdf->Output("Payment_002.pdf", "I");
  }
}
