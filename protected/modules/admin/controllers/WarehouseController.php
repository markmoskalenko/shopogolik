<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="WarehouseController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class WarehouseController extends CustomAdminController {

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$id=$_REQUEST["id"];
	     
	       if(Yii::app()->request->isAjaxRequest)
	       {
	         $this->renderPartial('ajax_view',array(
			'model'=>$this->loadModel($id),
		));
	         
	       }
	       else
	       {
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	       }
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	
            $model=new WarehouseItems;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model,"warehouseitems-create-form");
            if(Yii::app()->request->isAjaxRequest)
	       {
		    if(isset($_POST['WarehouseItems']))
		    {
			    $model->attributes=$_POST['WarehouseItems'];
			    if($model->save())
			    {
			      echo $model->id;
			    }
			    else
			    {
			      echo "false";
			    } 
			    return;
		    }
	       }
	       else
	       {
	           if(isset($_POST['WarehouseItems']))
		    {
			    $model->attributes=$_POST['WarehouseItems'];
			    if($model->save())
			     $this->redirect(array('view','id'=>$model->id));
			
		    }
               
		    $this->render('create',array(
			    'model'=>$model,
		    ));
	       }	
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
      
	    $id=isset($_REQUEST["id"])?$_REQUEST["id"]:$_REQUEST["WarehouseItems"]["id"];
	    $model=$this->loadModel($id);
			    
	    // Uncomment the following line if AJAX validation is needed
	      $this->performAjaxValidation($model,"warehouseitems-update-form");
	    
	  if(Yii::app()->request->isAjaxRequest)
	    {
	    
		if(isset($_POST['WarehouseItems']))
		{
		  
			$model->attributes=$_POST['WarehouseItems'];
			if($model->save())
			{
			  echo $model->id;
			}
			else
			{
			  echo "false";
			}
			return;
		}
		    
		  $this->renderPartial('_ajax_update_form',array(
		    'model'=>$model,
		    ));
		  return; 
	    
	    }
	    

	    if(isset($_POST['WarehouseItems']))
	    {
		    $model->attributes=$_POST['WarehouseItems'];
		    if($model->save())
			    $this->redirect(array('view','id'=>$model->id));
	    }

	    $this->render('update',array(
		    'model'=>$model,
	    ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
	        $id=$_POST["id"];
	   
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset(Yii::app()->request->isAjaxRequest))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			else
			   echo "true";
		}
		else
		{
		    if(!isset($_GET['ajax']))
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		    else
			   echo "false"; 	
	        }	
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $session=new CHttpSession;
            $session->open();		
            $criteria = new CDbCriteria();            

                $model=new WarehouseItems('search');
                $model->unsetAttributes();  // clear any default values

                if(isset($_GET['WarehouseItems']))
		{
                        $model->attributes=$_GET['WarehouseItems'];
			
			
                   	
                       if (!empty($model->id)) $criteria->addCondition('id = "'.$model->id.'"');
                     
                    	
                       if (!empty($model->tid)) $criteria->addCondition('tid = "'.$model->tid.'"');
                     
                    	
                       if (!empty($model->date_in)) $criteria->addCondition('date_in = "'.$model->date_in.'"');
                     
                    	
                       if (!empty($model->date_out)) $criteria->addCondition('date_out = "'.$model->date_out.'"');
                     
                    	
                       if (!empty($model->uid_in)) $criteria->addCondition('uid_in = "'.$model->uid_in.'"');
                     
                    	
                       if (!empty($model->uid_out)) $criteria->addCondition('uid_out = "'.$model->uid_out.'"');
                     
                    	
                       if (!empty($model->store_id)) $criteria->addCondition('store_id = "'.$model->store_id.'"');

		}
                 $session['WarehouseItems_records']=WarehouseItems::model()->findAll($criteria); 
       

                $this->render('index',array(
			'model'=>$model,
		));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new WarehouseItems('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['WarehouseItems']))
			$model->attributes=$_GET['WarehouseItems'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=WarehouseItems::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model,$form_id)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']===$form_id)
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionGenerateExcel()
	{
            $session=new CHttpSession;
            $session->open();		
            
             if(isset($session['WarehouseItems_records']))
               {
                $model=$session['WarehouseItems_records'];
               }
               else
                 $model = WarehouseItems::model()->findAll();

		
		Yii::app()->request->sendFile(date('YmdHis').'.xls',
			$this->renderPartial('excelReport', array(
				'model'=>$model
			), true,false,true)
		);
	}
        public function actionGeneratePdf() 
	{
           $session=new CHttpSession;
           $session->open();
		Yii::import('application.extensions.ajaxgii.bootstrap.*');
		require_once('tcpdf/tcpdf.php');
        if (Utils::AppLang()=='ru') {
        require_once('tcpdf/config/lang/rus.php');
        } else {
         require_once('tcpdf/config/lang/eng.php');
        }

             if(isset($session['WarehouseItems_records']))
               {
                $model=$session['WarehouseItems_records'];
               }
               else
                 $model = WarehouseItems::model()->findAll();



		$html = $this->renderPartial('expenseGridtoReport', array(
			'model'=>$model
		), true);
		
		//die($html);
		
		$pdf = new TCPDF();
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor(Yii::app()->name);
		$pdf->SetTitle('WarehouseItems Report');
		$pdf->SetSubject('WarehouseItems Report');
		//$pdf->SetKeywords('example, text, report');
		$pdf->SetHeaderData('', 0, "Report", '');
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by ".Yii::app()->name, "");
		$pdf->setHeaderFont(Array('helvetica', '', 8));
		$pdf->setFooterFont(Array('helvetica', '', 6));
		$pdf->SetMargins(15, 18, 15);
		$pdf->SetHeaderMargin(5);
		$pdf->SetFooterMargin(10);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetFont('dejavusans', '', 7);
		$pdf->AddPage();
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->LastPage();
		$pdf->Output("WarehouseItems_002.pdf", "I");
	}

  public function actionIncome()
  {
    $session=new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model=new WarehouseItems('search');
    $model->unsetAttributes();  // clear any default values

    if(isset($_GET['WarehouseItems']))
    {
      $model->attributes=$_GET['WarehouseItems'];



      if (!empty($model->id)) $criteria->addCondition('id = "'.$model->id.'"');


      if (!empty($model->tid)) $criteria->addCondition('tid = "'.$model->tid.'"');


      if (!empty($model->date_in)) $criteria->addCondition('date_in = "'.$model->date_in.'"');


      if (!empty($model->date_out)) $criteria->addCondition('date_out = "'.$model->date_out.'"');


      if (!empty($model->uid_in)) $criteria->addCondition('uid_in = "'.$model->uid_in.'"');


      if (!empty($model->uid_out)) $criteria->addCondition('uid_out = "'.$model->uid_out.'"');


      if (!empty($model->store_id)) $criteria->addCondition('store_id = "'.$model->store_id.'"');


    }
    $session['WarehouseItems_records']=WarehouseItems::model()->findAll($criteria);


    $this->renderPartial('index',array(
      'model'=>$model,false,true
    ));

  }

  public function actionExpenditure()
  {
    $session=new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model=new WarehouseItems('search');
    $model->unsetAttributes();  // clear any default values

    if(isset($_GET['WarehouseItems']))
    {
      $model->attributes=$_GET['WarehouseItems'];



      if (!empty($model->id)) $criteria->addCondition('id = "'.$model->id.'"');


      if (!empty($model->tid)) $criteria->addCondition('tid = "'.$model->tid.'"');


      if (!empty($model->date_in)) $criteria->addCondition('date_in = "'.$model->date_in.'"');


      if (!empty($model->date_out)) $criteria->addCondition('date_out = "'.$model->date_out.'"');


      if (!empty($model->uid_in)) $criteria->addCondition('uid_in = "'.$model->uid_in.'"');


      if (!empty($model->uid_out)) $criteria->addCondition('uid_out = "'.$model->uid_out.'"');


      if (!empty($model->store_id)) $criteria->addCondition('store_id = "'.$model->store_id.'"');


    }
    $session['WarehouseItems_records']=WarehouseItems::model()->findAll($criteria);


    $this->renderPartial('index',array(
      'model'=>$model,false,true
    ));

  }

  public function actionUnfound()
  {
    $session=new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model=new WarehouseItems('search');
    $model->unsetAttributes();  // clear any default values

    if(isset($_GET['WarehouseItems']))
    {
      $model->attributes=$_GET['WarehouseItems'];



      if (!empty($model->id)) $criteria->addCondition('id = "'.$model->id.'"');


      if (!empty($model->tid)) $criteria->addCondition('tid = "'.$model->tid.'"');


      if (!empty($model->date_in)) $criteria->addCondition('date_in = "'.$model->date_in.'"');


      if (!empty($model->date_out)) $criteria->addCondition('date_out = "'.$model->date_out.'"');


      if (!empty($model->uid_in)) $criteria->addCondition('uid_in = "'.$model->uid_in.'"');


      if (!empty($model->uid_out)) $criteria->addCondition('uid_out = "'.$model->uid_out.'"');


      if (!empty($model->store_id)) $criteria->addCondition('store_id = "'.$model->store_id.'"');


    }
    $session['WarehouseItems_records']=WarehouseItems::model()->findAll($criteria);


    $this->renderPartial('index',array(
      'model'=>$model,false,true
    ));

  }

  public function actionMap()
  {
    $session=new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model=new WarehouseItems('search');
    $model->unsetAttributes();  // clear any default values

    if(isset($_GET['WarehouseItems']))
    {
      $model->attributes=$_GET['WarehouseItems'];



      if (!empty($model->id)) $criteria->addCondition('id = "'.$model->id.'"');


      if (!empty($model->tid)) $criteria->addCondition('tid = "'.$model->tid.'"');


      if (!empty($model->date_in)) $criteria->addCondition('date_in = "'.$model->date_in.'"');


      if (!empty($model->date_out)) $criteria->addCondition('date_out = "'.$model->date_out.'"');


      if (!empty($model->uid_in)) $criteria->addCondition('uid_in = "'.$model->uid_in.'"');


      if (!empty($model->uid_out)) $criteria->addCondition('uid_out = "'.$model->uid_out.'"');


      if (!empty($model->store_id)) $criteria->addCondition('store_id = "'.$model->store_id.'"');


    }
    $session['WarehouseItems_records']=WarehouseItems::model()->findAll($criteria);


    $this->renderPartial('index',array(
      'model'=>$model,false,true
    ));

  }


}
