<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="FilemanController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class FilemanController extends CustomAdminController
{

	public function actionIndex()
	{
      $elFinderPath = Yii::getPathOfAlias('ext.elrte.lib.elfinder.php');

      include $elFinderPath.'/elFinderConnector.class.php';
      include $elFinderPath.'/elFinder.class.php';
      include $elFinderPath.'/elFinderVolumeDriver.class.php';
      include $elFinderPath.'/elFinderVolumeLocalFileSystem.class.php';

      function access($attr, $path, $data, $volume) {
        return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
          ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
          :  null;                                    // else elFinder decide it itself
      }

      $opts = array(
        // 'debug' => true,
        'roots' => array(
          array(
            'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
            'path'          => Yii::getPathOfAlias('webroot').'/upload',         // path to files (REQUIRED)
            'URL'           => '/upload', // URL to files (REQUIRED)
            'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
          ),
          array(
            'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
            'path'          => Yii::getPathOfAlias('webroot').'/images',         // path to files (REQUIRED)
            'URL'           => '/images', // URL to files (REQUIRED)
            'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
          ),
          array(
            'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
            'path'          => Yii::getPathOfAlias('webroot').'/themes/'.DSConfig::getVal('site_front_theme'),         // path to files (REQUIRED)
            'URL'           => '/themes/'.DSConfig::getVal('site_front_theme'), // URL to files (REQUIRED)
            'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
          ),
          array(
            'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
            'path'          => Yii::getPathOfAlias('webroot').'/themes/'.DSConfig::getVal('site_front_theme').'/images/banners',         // path to files (REQUIRED)
            'URL'           => '/themes/'.DSConfig::getVal('site_front_theme').'/images/banners', // URL to files (REQUIRED)
            'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
          )
        )
      );

      // run elFinder
      $connector = new elFinderConnector(new elFinder($opts));
      $connector->run();
      exit;
	}

    public function actionView()
    {
        $this->renderPartial('view', array(),FALSE,TRUE);
    }
}