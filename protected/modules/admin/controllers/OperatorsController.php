<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OperatorsController.php">
* </description>
**********************************************************************************************************************/?>
<?php
class OperatorsController extends CustomAdminController
{

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $model=$this->loadModel($id);


            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);



            $this->renderPartial('update',array(
                'model'=>$model,
            ),false,true);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $model=new Users;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Users']))
            {
                $model->attributes=$_POST['Users'];
                if($model->save())
                    $this->redirect(array('view','id'=>$model->uid));
            }

            $this->render('create',array(
                'model'=>$model,
            ),false,true);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id=null)
	{
            if(is_null($id)) $id=$_POST['Users']['uid'];
            $model=$this->loadModel($id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Users']))
            {
                $model->attributes=$_POST['Users'];
                if($model->save()) echo Yii::t('admin',"Данные сохранены");
                else echo Yii::t('admin',"Ошибка сохранения модели");

            }else echo Yii::t('admin',"Неверный запрос");



	}

	public function actionOrdersum($id=null)
	{
            if(is_null($id)) $id=$_POST['uid'];
            if(isset($_POST['date_from'])) $stDate=$_POST['date_from'];
            else  $stDate=date('Y-m').'-01';
            if(isset($_POST['date_to'])) $endDate=$_POST['date_to'];
            else  $endDate=date('Y-m-d');
            $model=$this->loadModel($id);
            $aFilterSummOrder = array(
                'manager'=>$model->uid,
                'date_from'=>$stDate,
                'date_to'=>$endDate
            );
            $sumstr = $model->getOperatorOrdersSumm($aFilterSummOrder,true);
            echo '<b>'.Yii::t('admin','Сумма всех заказов за период c ').$stDate.Yii::t('admin',' по ').$endDate.' - '.$sumstr.'</b>';
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            if(Yii::app()->request->isPostRequest)
            {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
            else
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $dataProvider = new CActiveDataProvider('Users', array(
                'criteria'=>array(
                    'condition'=>"role = 'manager'",
                ),
                'pagination'=>array(
                'pageSize'=>50,
                ),
            ));



            if (Yii::app()->request->isAjaxRequest) {
                $this->renderPartial('index',array(
                    'dataProvider'=>$dataProvider,
                    ),false,true);
            }
            else {
                $this->render('index',array(
                    'dataProvider'=>$dataProvider,
                    ));

            }
	}

    public function actionList() {
      $criteria = new CDbCriteria;
      $criteria->condition="role not in ('user','admin','translator')";
/*
            // Необработанные заказы
            $neworders = Order::model()->findAll('manager=:uid AND status = 1',array(':uid'=>$data->uid));
            // Задержка закупа
            $devilvorders = Order::model()->findAll('manager=:uid AND status = 2',array(':uid'=>$data->uid));
            // Задержка поставки
            $getorders = Order::model()->findAll('manager=:uid AND status = 8',array(':uid'=>$data->uid));

            $orders = Order::model()->findAll("manager=:uid AND status != 1 AND date > ".strtotime('1 '.date('M').' '.date('Y')),array(':uid'=>$data->uid));
            $sum = 0;
            foreach($orders as $o) {
                $d = CJSON::decode($o->data);
                $sum += $o->sum;
            }

 */
      $criteria->select = "t.*, FROM_UNIXTIME(t.created,'%d.%m.%Y %h:%i') AS created,
 (select count(0) from orders oo where manager=t.uid and oo.status = 'IN_PROCESS') as ordr_operator_inprocess,
 (select count(0) from orders oo where manager=t.uid and oo.status = 'PAUSED') as ordr_operator_delay_buy,
 (select count(0) from orders oo where manager=t.uid and oo.status = 'SEND_TO_CUSTOMER') as ordr_operator_send,
 (select FROM_UNIXTIME(MAX(oo.date),'%d.%m.%Y %h:%i') from orders oo where manager=t.uid) as ordr_operator_activity,
   (SELECT concat('<b>+</b>',sum(if((oo.status IN ('SEND_TO_CUSTOMER','RECEIVED_BY_CUSTOMER')), 1, 0)),'/',
       round(sum(if((oo.status IN ('SEND_TO_CUSTOMER','RECEIVED_BY_CUSTOMER')),
       (select sum(summ) as `sum` from orders_payments op
where op.oid = oo.id), 0)), 2),'<br/>',
       '<b>?</b>',sum(if((oo.status IN ('IN_PROCESS')), 1, 0)),'/',
       round(sum(if((oo.status IN ('IN_PROCESS')),
       (select sum(summ) as `sum` from orders_payments op
where op.oid = oo.id), 0)), 2),'<br/>',
       '<b>~</b>',round(avg((select sum(summ) as `sum` from orders_payments op
where op.oid = oo.id)), 2))
  FROM orders oo where oo.manager=t.uid) as ordr_operator_stat";

        $dataProvider = new CActiveDataProvider('Users', array(
            'criteria'=>$criteria,
            'pagination'=>array(
            'pageSize'=>50,
            ),
        ));

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('list', array(
                'dataProvider' => $dataProvider,
            ),false,true);
        }
        else {
            $this->render('list', array(
                'dataProvider' => $dataProvider,
                ));
        }
    }

    public function actionBegin() {
        Yii::app()->request->cookies['opMode'] = new CHttpCookie('opMode', 1);

        $this->redirect("/admin/main");
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
            $model=Users::model()->findByPk($id);
            if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
            return $model;
	}

    public function actionBalance()
    {

    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{

	}
}

