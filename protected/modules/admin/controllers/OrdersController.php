<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="OrdersController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class OrdersController extends CustomAdminController
{

    public function actionDashboard()
    {
//=================================================================================
        $uid = null;
        if (Yii::app()->user->inRole(array('superAdmin', 'topManager'))) {
            $manager = null;
        } elseif (Yii::app()->user->inRole(array('orderManager'))) {
            $manager = Yii::app()->user->id;
        } else {
            $uid = Yii::app()->user->id;
            $manager = null;
        }
//=================================================================================
        $ordersByStatusesArray = OrdersStatuses::getAllStatusesListAndOrderCount($uid, $manager);
        $ordersByStatusesDataProvider = new CArrayDataProvider($ordersByStatusesArray, array(
          'id'         => 'ordersByStatuses',
            /*      'sort'=>array(
                    'attributes'=>array(
                      'id', 'username', 'email',
                    ),
                  ),
            */
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
        $this->renderPartial(
          'dashboard',
          array(
            'ordersByStatusesDataProvider' => $ordersByStatusesDataProvider,
            'ordersByStatusesArray'        => $ordersByStatusesArray,
          ),
          false,
          true
        );
    }

    public function actionIndex($type = null)
    {
        $name = OrdersStatuses::getStatusName($type);
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial(
              'index',
              array(
                'type' => $type,
                'name' => $name,
              ),
              false,
              true
            );
        } else {
            $this->render(
              'index',
              array(
                'type' => $type,
                'name' => $name,
              )
            );
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate()
    {
        $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : $_REQUEST["Order"]["id"];
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model, "order-update-form");
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['Order'])) {
                $model->attributes = $_POST['Order'];
                if (isset($_POST['Order']['do_moneyback'])) {
                    $model->do_moneyback = $_POST['Order']['do_moneyback'];
                } else {
                    $model->do_moneyback = 0;
                }
                if ($model->save()) {
                    echo $model->id;
                } else {
                    echo "false";
                }
                return;
            }
            echo "false";
            return;
        }
        if (isset($_POST['Order'])) {
            $model->attributes = $_POST['Order'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
    }

    /**
     * Updates a particular model item.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdateItem()
    {
        if (isset($_REQUEST["orderItemId"])) {
            $itemId = $_REQUEST["orderItemId"];
        } else {
            return;
        }
        $model = OrdersItems::model()->findByPk($itemId);
        if (!$model) {
            return 'false';
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model, "ordersitems-update-form");
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['OrdersItems'])) {
                $model->attributes = $_POST['OrdersItems'];
                if ($model->save()) {
                    echo $model->id;
                } else {
                    echo "false";
                }
                return;
            }
            //echo 'false';
            $this->renderPartial(
              '_ajax_update_item_form',
              array(
                'model' => $model,
              )
            );
            return;
        }
        if (isset($_POST['OrdersItems'])) {
            $model->attributes = $_POST['OrdersItems'];
            if ($model->save()) {
                echo $model->id;
            } else {
                echo "false";
            }
            return;
        }
    }

    public function actionView($id)
    {
//        try {
            $uid = null;
            if (Yii::app()->user->inRole(array('superAdmin', 'topManager'))) {
                $manager = null;
            } elseif (Yii::app()->user->inRole(array('orderManager'))) {
                $manager = Yii::app()->user->id;
            } else {
                $uid = Yii::app()->user->id;
                $manager = null;
            }
            $order = Order::getOrder($id, $uid, $manager);
            if (!isset($order)) {
               echo '<div><hr/><strong>'.Yii::t('admin', 'У вас нет прав для просмотра даного заказа').'</strong><hr/></div>';
                Yii::app()->end();
                //throw new CHttpException(400, Yii::t('admin', 'У вас нет прав для просмотра даного заказа'));
            }
            Yii::app()->controller->pageTitle = Yii::t('admin', 'Заказ №') . $order->uid . '-' . $order->id;
            Yii::app()->controller->breadcrumbs = array(
              Yii::t('admin', 'Заказы') => '/admin/orders',
              Yii::app()->controller->pageTitle
            );
//======================================================================================================================
            if (Yii::app()->request->isAjaxRequest && isset($_GET['EventsLog_page'])
                //  &&(strpos($_GET['ajax'],'grid-')===0)
            ) {
                $this->renderPartial(
                  '/orders/ordercommentsevents',
                  array(
                    'order' => $order,
                  ),
                  false,
                  true
                );
                Yii::app()->end();
            }
            $this->renderPartial(
              '/orders/view',
              array(
                'order' => $order,
              ),
              false,
              true
            );
//      }
/*        } catch (Exception $e) {
            print_r($e);
            //LogSiteErrors::logError($e['type'] . ' ' . $e['source'] . ' ' . $e['file'] . ': ' . $e['line'], $e['message'], $e['trace']);
            //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
            echo $e['message'];
        }
*/
    }

    public function actionSave()
    {
        if (isset($_POST['Order'])) {
            $order = Order::model()->findByPk($_POST['Order']['id']);
            $order->attributes = $_POST['Order'];
            $order->save();
            /*      if (7 == $order->status) {
                    //проверка на вип сумма всех заказов со статусом 7 пользователя превышает 2000 дол
                    $sql = 'SELECT  SUM(sum)
                                        FROM `orders`
                                        WHERE `uid` = ' . $order->uid . '
                                        AND `status` = 7';
                    $summa = Yii::app()->db->createCommand($sql)->queryScalar();
                    if ($summa > 2000) {
                      //апдейт поля вип = 1
                      $vip = Users::model()->findByPk($order->uid);
                      $vip->vip = 1;
                      $vip->save();
                      echo "Новый ВИП клиент.";
                      // отправить сообщение клиенту с поздравлениями о присвоеном статусе ВИП
                    }
                  }
            */
            echo Yii::t('admin', 'Заказ сохранен');
        } else {
            echo Yii::t('admin', 'Попытка сохранить потерпела неудачу');
        }
        //$this->redirect('/admin/orders');
    }

    public function actionRefund($pid, $msg = null)
    {
        if (empty($msg)) {
            $msg = Yii::t('admin', "Невозможно закупить товар");
        }
        $orderItem = OrdersItems::model()->findByPk($pid);
        if (in_array($orderItem->status, OrdersItemsStatuses::getOrderItemExcludedStatusesArray())) {
            echo Yii::t('admin', "Ошибка: товар уже удален");
            return;
        }
        $orderItem->status = 4;
        $orderItem->save();
        //  $data = CJSON::decode($orderItem->data);
        $datas = array(
          'iid'     => $orderItem['iid'],
          'pic_url' => $orderItem['pic_url']
        );

        if (OrdersPayments::payForOrder(
          $orderItem->oid,
          -$orderItem->price,
          Yii::t('admin', 'Возврат средств за товар. Заказ') . ' №' . $orderItem->oid
        )
        ) {
            //Получаем данные о заказе
            $order = Order::model()->findByPk($orderItem->oid);
            $order->sum = $order->sum - $orderItem->price;
            $order->save();
            //Создаем оповещение пользователю  - сообщение пользователю проходит через  $payment = new Payment;
            $notice = new UserNotice;
            $notice->msg = $msg;
            $notice->uid = $order->uid;
            $notice->status = 1;
            $notice->data = CJSON::encode($datas);
            $notice->save();
        } else {
            echo Yii::t('admin', 'Возврат средств не прошел! Провердите возврат вручную.');
        }
        echo Yii::t('admin', 'Заказ товара отменен!');
        Yii::app()->end();
    }

    public function actionSave_Comment($id, $comment = null)
    {
        $p = OrdersItems::model()->findByPk($id);

        $p->comment = $comment;
        if ($p->save()) {
            echo Yii::t('admin', 'Комментарий сохранен');
        } else {
            echo Yii::t('admin', 'Ошибка при сохранении комментария');
        }
    }

    public function actionSaveTotalPrice()
    {
        if (!(isset($_POST['OrdersForm']['price']))) {
            echo 'error';
            return false;
        }
        $order = Order::model()->findByPk($_POST['OrdersForm']['id']);
        $actionDebt = $_POST['OrdersForm']['price'] - $order->sum;
//--- разруливаем задолженность ---
        // $actionDebt - это скока надо доплатить
        if ($actionDebt > 0) {
            $order->debt = $order->debt + $actionDebt;
            $paymentDebt = $actionDebt;
        } elseif ($actionDebt < 0) {
            $paymentDebt = $order->debt + $actionDebt;
            $order->debt = 0;
        } else {
            $paymentDebt = 0;
        }
//---------------------------------
        $order->sum = $_POST['OrdersForm']['price'];
//===================================================
        if ($paymentDebt <= 0) {
//TODO      $order->status = 6; Готов к отправке
            if ($paymentDebt < 0) {
                //Возвращаем стоимость товара
//    if (OrdersPayments::payForOrder($orderItem->oid, -$orderItem->price, Yii::t('admin', 'Возврат средств за товар. Заказ').' №'. $orderItem->oid)) {
                $payment = new Payment;
                $payment->sum = abs($paymentDebt);
                $payment->uid = $order->uid;
                $payment->date = time();
                $payment->status = 1;
                $payment->description = Yii::t('admin', 'Возврат средств по заказу №') .
                  $order->id . ' ' . Yii::t('admin', '(Переплата)');
                $payment->save($order);

                $notice = new UserNotice;
//        $datas = array('oid' => $order->id);
                $notice->sum = abs($paymentDebt);
                $notice->msg = Yii::t('admin', 'Возврат средств по заказу №') .
                  $order->id . ' ' . Yii::t('admin', '(Переплата)');
                $notice->uid = $order->uid;
                $notice->status = 3;
                $notice->save();
            }
        } else {
            //Требуется доплата за доставку
//TODO      $order->status = 5; Ожидает доплаты
            //
            $notice = new UserNotice;
            $datas = array('oid' => $order->id);
            $notice->msg = Yii::t('admin', 'Заказ №') . $order->id .
              Yii::t('admin', " требует доплаты.");
            $notice->sum = $paymentDebt;
            $notice->uid = $order->uid;
            $notice->status = 2;
            $notice->data = CJSON::encode($datas);
            $notice->save();
        }
//===================================================
        if ($order->save()) {
            echo Yii::t('admin', "Стоимость товаров заказа изменена");
            return true;
        } else {
            echo 'error';
            return false;
        }
    }

    public function actionSaveTotalDelivery()
    {
        if (!(isset($_POST['OrdersForm']['delivery']))) {
            echo 'error';
            return false;
        }
        $order = Order::model()->findByPk($_POST['OrdersForm']['id']);
        $new_delivery = $_POST['OrdersForm']['delivery'];
        $checkout_order_reconfirmation_needed = DSConfig::getVal('checkout_order_reconfirmation_needed') == 1;
        if (!$checkout_order_reconfirmation_needed) {
//====================================================
            $actionDebt = $new_delivery - $order->delivery;

//--- разруливаем задолженность ---
            // $actionDebt - это скока надо доплатить
            if ($actionDebt > 0) {
                $order->debt = $order->debt + $actionDebt;
                $paymentDebt = $actionDebt;
            } elseif ($actionDebt < 0) {
                $paymentDebt = $order->debt + $actionDebt;
                $order->debt = 0;
            } else {
                $paymentDebt = 0;
            }
//---------------------------------

            $order->delivery = $new_delivery;
            if ($paymentDebt <= 0) {
//TODO        $order->status = 6; Готов к отправке
                if ($paymentDebt < 0) {
                    //Возвращаем стоимость товара
                    $payment = new Payment;
                    $payment->sum = abs($paymentDebt);
                    $payment->uid = $order->uid;
                    $payment->date = time();
                    $payment->status = 1;
                    $payment->description = Yii::t('admin', 'Возврат средств по заказу №') .
                      $order->id . ' ' . Yii::t('admin', '(Переплата веса)');
                    $payment->save($order);

                    $notice = new UserNotice;
                    //         $datas = array('oid' => $order->id);
                    $notice->sum = abs($paymentDebt);
                    $notice->msg = Yii::t('admin', 'Возврат средств по заказу №') .
                      $order->id . ' ' . Yii::t('admin', '(Переплата веса)');
                    $notice->uid = $order->uid;
                    $notice->status = 3;
                    $notice->save();
                }
            } else {
                //Требуется доплата за доставку
//TODO        $order->status = 5; Ожидает доплаты
                //
                $notice = new UserNotice;
                $datas = array('oid' => $order->id);
                $notice->msg = Yii::t('admin', 'Заказ №') .
                  $order->id . Yii::t('admin', " требует доплаты за доставку.");
                $notice->sum = $paymentDebt;
                $notice->uid = $order->uid;
                $notice->status = 2;
                $notice->data = CJSON::encode($datas);
                $notice->save();
            }
//====================================================
        } else {
//====================================================
            $order->debt = $actionDebt = $new_delivery - $order->delivery;
            $order->delivery = $new_delivery;
//TODO      $order->status = 102; Новый (не подтверждён)
            $notice = new UserNotice;
            $datas = array('oid' => $order->id);
            $notice->msg = Yii::t('admin', 'Заказ №') .
              $order->id . Yii::t('admin', " требует Вашего подтверджения стоимости доставки и оплаты.");
            $notice->sum = $actionDebt;
            $notice->uid = $order->uid;
            $notice->status = 2;
            $notice->data = CJSON::encode($datas);
            $notice->save();
//====================================================
        }
        if ($order->save()) {
            echo Yii::t('admin', "Стоимость доставки заказа изменена");
            return true;
        } else {
            echo 'error';
            return false;
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Order::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model, $form_id)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGenerateExcel($id)
    {
        $order = Order::getOrder($id, null, null);
        if ($order) {
            Yii::app()->request->sendFile(
              'Order-' . $order->id . '-' . date('YmdHi') . '.xls',
              $this->renderPartial(
                'excelReport',
                array(
                  'order' => $order
                ),
                true,false,true
              )
            );
        }
    }

    public function actionGenerateXML($id)
    {
        $order = Order::getOrder($id, null, null);
        if ($order) {
            Yii::app()->request->sendFile(
              'Order-' . $order->id . '-' . date('YmdHi') . '.xml',
              $this->renderPartial(
                'XMLReport',
                array(
                  'order' => $order
                ),
                true,false,true
              )
            );
        }
    }

    public function actionGenerateInvoice($id)
    {
        $order = Order::getOrder($id, null, null);
        if ($order) {
            Yii::app()->request->sendFile(
              'OrderInvoice-' . $order->id . '-' . date('YmdHi') . '.xls',
              $this->renderPartial(
                'invoiceReport',
                array(
                  'order' => $order
                ),
                true,false,true
              )
            );
        }
    }

    public function actionGenerateItemReport($itemId,$view='itemReport')
    {
        $item = OrdersItems::model()->findByPk($itemId);
        if ($item) {
            $order = Order::getOrder($item->oid, null, null);
            $this->renderPartial($view, array('order' => $order, 'item' => $item,), false, false);
        }
    }

}