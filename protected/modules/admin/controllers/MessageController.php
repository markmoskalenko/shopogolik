<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="MessageController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class MessageController extends CustomAdminController {
  public $breadcrumbs;
  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = 'main';

  public function actionSendNote() {
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['message']) && isset($_POST['message']['uid']) && isset($_POST['message']['message'])) {
        $notice = new UserNotice;
        $notice->msg = $_POST['message']['message'];
        $notice->uid = $_POST['message']['uid'];
        $notice->status = 6;
        $notice->data = CJSON::encode($_POST['message']);
        $notice->save();
      }
    }
  }

  public function actionSendMail() {
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['message']) && isset($_POST['message']['uid']) && isset($_POST['message']['message'])) {
          if ($_POST['message']['uid']=='all') {
              Mail::sendMailToAll($_POST['message']['message']);
          } else {
              Mail::sendMailToUser($_POST['message']['uid'], $_POST['message']['message']);
          }
      }
    }
  }
}
