<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="UpdateController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class UpdateController extends CustomAdminController {
  private $user_side = true; //$debug = (($_SERVER['SERVER_NAME'] == '777.danvit.net') || FALSE);
  public function actionIndex() {
    $this->renderPartial('index', array(), FALSE, TRUE);
  }
  public function init(){
    $this->user_side= !(($_SERVER['SERVER_NAME'] == '777.danvit.net'));
    //$this->user_side= !(($_SERVER['SERVER_NAME'] == 'demo.dropshop.pro'));
  }
  public function actionCommand($cmd) {
    switch ($cmd) {
      case 'make_update':
        if ($this -> user_side===false)
        $this->cmd_make_update();
        break;
      case 'get_update':
        $this->cmd_get_update();
        break;
      case 'apply_update':
        $this->cmd_apply_update();
        break;
      default:
        echo 'No command specified';
    }
  }

  private function getData($url) {  //Get data from curl
    $result = new stdClass();
    $result -> content = Null;
    $result -> headers = Null;
    $result -> info = Null;
    $result -> curl_error = Null;
    $ch = curl_init($url);
    $fp = fopen('php://temp', 'w+');
    $fh = fopen('php://temp', 'w+');
    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 5.1; rv:25.0) Gecko/20100101 Firefox/25.0');
    curl_setopt($ch,CURLINFO_HEADER_OUT,true);
    curl_setopt($ch,CURLOPT_ENCODING,'');
    curl_setopt($ch,CURLOPT_FILE, $fp);
    curl_setopt($ch,CURLOPT_WRITEHEADER,$fh);
    curl_exec($ch);
    if (!curl_errno($ch)) {
      $result -> info = curl_getinfo($ch);
      fseek($fh,0);
      $fs = fstat($fh);
      if ($fs['size']>0) $result -> headers = fread($fh,$fs['size']);

      fseek($fp,0);
      $fs = fstat($fp);
      if ($fs['size']>0) $result -> content = fread($fp,$fs['size']);
    }
    else {
      $result -> curl_error = curl_error($ch);
    };
    fclose($fh);
    fclose($fp);
    curl_close($ch);
    return $result;
  }

  private function prepare_folder ($path) {
    if (!file_exists($path)) {
      mkdir($path);
    }
    else {
      if (!is_dir($path)) {
        unlink($path);
        mkdir($path);
      }
      else {
        passthru('rm -Rf '.$path.'/*');
      }
    }
  }

  private function get_current_version () {
    $s = '';
    $p = 'update_version';
    $cmd = Yii::app()->db->createCommand('SELECT value FROM config WHERE id = :id');
    $cmd ->bindParam(':id',$p,PDO::PARAM_STR);
    $row = $cmd->queryRow();
    if ($row!=false) {
      $s = $row['value'];
    }
    return $s;
  }

  private function get_version_info () {
    $s = '';
    $p = 'update_version_info';
    $cmd = Yii::app()->db->createCommand('SELECT value FROM config WHERE id = :id');
    $cmd ->bindParam(':id',$p,PDO::PARAM_STR);
    $row = $cmd->queryRow();
    if ($row!=false) {
      $s = $row['value'];
    }
    return $s;
  }

  private function get_update_file_url () {
    $s = '';
    $p = 'update_file_url';
    $cmd = Yii::app()->db->createCommand('SELECT value FROM config WHERE id = :id');
    $cmd ->bindParam(':id',$p,PDO::PARAM_STR);
    $row = $cmd->queryRow();
    if ($row!=false) {
      $s = $row['value'];
    }
    return $s;
  }

  private function get_update_md5_url () {
    $s = '';
    $p = 'update_md5_url';
    $cmd = Yii::app()->db->createCommand('SELECT value FROM config WHERE id = :id');
    $cmd ->bindParam(':id',$p,PDO::PARAM_STR);
    $row = $cmd->queryRow();
    if ($row!=false) {
      $s = $row['value'];
    }
    return $s;
  }

  private function get_version_url () {
    $s = '';
    $p = 'update_ver_url';
    $cmd = Yii::app()->db->createCommand('SELECT value FROM config WHERE id = :id');
    $cmd ->bindParam(':id',$p,PDO::PARAM_STR);
    $row = $cmd->queryRow();
    if ($row!=false) {
      $s = $row['value'];
    }
    return $s;
  }

  private function get_version_info_url () {
    $s = '';
    $p = 'update_info_url';
    $cmd = Yii::app()->db->createCommand('SELECT value FROM config WHERE id = :id');
    $cmd ->bindParam(':id',$p,PDO::PARAM_STR);
    $row = $cmd->queryRow();
    if ($row!=false) {
      $s = $row['value'];
    }
    return $s;
  }

  private function make_new_version () {
    return 'now';
  }

  private function log($fname,$mess) {
    //if (!file_exists($fname)) return;
    $cmess = $mess;
    $fp = fopen($fname, 'c+');
    fseek($fp,0,SEEK_END);
    fwrite($fp,$cmess,strlen($cmess));
    fclose($fp);
  }

  private function cmd_make_update() {
    @ini_set('max_execution_time', 3000);
    echo "Make update ...<br>";

    $s = Yii::app()->db->connectionString;
    $st = 'dbname=';
    $i = strpos($s,$st);
    if ($i===false) {
      echo 'Error ! Database name not found in connection string -> '.$s." <br>";
      return;
    };
    $s = substr($s,$i+strlen($st));
    $i = strpos($s,';');
    if ($i!==false) {
      $s = substr($s,0,$i-1);
    }
    $dump_mysql = 'mysqldump --user='.Yii::app()->db->username.' --password='.Yii::app()->db->password.
          ' --skip-comments --default_character_set '.Yii::app()->db->charset.' '.$s.' --triggers ';

    $base_path = realpath(Yii::app()->basePath.'/..');
    $path = $base_path.'/updates/';
    $up_name = $this -> get_current_version();
    $up_src = 'updates/tables/ updates/files/';
    echo "Prepare tables ... ";
    $cmd = Yii::app()->db->createCommand("SELECT tname,utype FROM update_tbl WHERE NOT (tname='update_tbl') ");
    $rows = $cmd->queryAll();
    if ($rows!=false) {
      $this -> prepare_folder($path.'tables');
      $tables = array();
      foreach ($rows as $name=>$value) {
        echo "<br>";
        echo 'Table -> '.$value['tname'].' ...';
        if (($value['utype']=='s') || ($value['utype']=='c')) {
          $cmd = Yii::app()->db->createCommand('SHOW CREATE TABLE '.$value['tname']);
          $row = $cmd->queryRow();
          if ($row!=false) {
            $s = $row['Create Table']; //Create table scrypt
            $s = rtrim (preg_replace('/(\/\*.*?\*\/)|(#.*?$)|(--\s.*?$)/sm','',$s));
            $fn = $path.'tables/'.$value['tname'];
            $fp = fopen($fn, 'w');
            fwrite($fp,$s,strlen($s));
            fclose($fp);

            $res = preg_match_all('/\s+CONSTRAINT\s.+?REFERENCES\s+`*([\w_]+)`*\s*\(/ism',$s,$ret);
            if ($res>0) {
              $ref = array_unique($ret[1]);
              if (($key = array_search($value['tname'], $ref)) !== false) {
                unset($ref[$key]);
              }
              if (count($ref)==0) {
                $tables[] = array ('name' => $value['tname'], 'prior' => 0, 'refs' => null);
              }else {
                $tables[] = array ('name' => $value['tname'], 'prior' => -1, 'refs' => $ref);
              };
            }else {
              $tables[] = array ('name' => $value['tname'], 'prior' => 0, 'refs' => null);
            };

            $tname = $value['tname']; //Create triggers scrypt
            $cmd = Yii::app()->db->createCommand('SHOW TRIGGERS WHERE `Table` = :name');
            $cmd ->bindParam(':name',$tname,PDO::PARAM_STR);
            $trows = $cmd->queryAll();
            if ($trows!=false) {
              $fn = $path.'tables/'.$tname.'.triggers.sql';
              $fp = fopen($fn, 'w');
              foreach ($trows as $name=>$tvalue) {
                $cmd = Yii::app()->db->createCommand('SHOW CREATE TRIGGER '.$tvalue ['Trigger']);
                $trow = $cmd->queryRow();
                if ($trow!=false) {
                  $s = $trow['SQL Original Statement'];
                  $s = preg_replace('/\sDEFINER\s*=\s*`.+?@`.+?`/s',' ',$s);
                  $s = rtrim (preg_replace('/((?<=\sTRIGGER\s)|(?<=\sON\s))\s*[`].+?[`]\./s','',$s));
                  $s = 'DELIMITER ;;'.chr(10).'/*!50003 '.$s.' */;;'.chr(10).'DELIMITER ;'.chr(10);
                  //$s = $s .';'. chr(10);
                  fwrite($fp,$s,strlen($s));
                }
              }
              fclose($fp);
            };
            echo "done";
          }else {
            echo "fail";
          }
        };
        if ($value['utype']=='w')  {
          //passthru('/home/777/scrypt/make_dump '.$value['tname'].' '.$path.'tables/'.' c',$ret);
          passthru($dump_mysql.'-c '.$value['tname'].'>'.$path.'tables/'.$value['tname'].'.sql',$ret);
          if ($ret==0) {
            $fn = $path.'tables/'.$value['tname'].'.sql';
            $fp = fopen($fn, 'r+');
            $s = fread($fp,filesize($fn));
            if (strpos($s,'TRIGGER')!==false) {
              $s = preg_replace('/\sDEFINER\s*=\s*`.+?@`.+?`/s',' ',$s);
              $s = rtrim (preg_replace('/((?<=\sTRIGGER\s)|(?<=\sON\s))\s*[`].+?[`]\./s','',$s));
              ftruncate($fp,0);
              fseek($fp,0);
              fwrite($fp,$s,strlen($s));
            };
            fclose($fp);
            echo "done";
          }else {
            echo "fail";
          }
        };
        if ($value['utype']=='c')  {
          echo "<br>";
          echo 'Update data table -> '.$value['tname'].' ...';
          $s = $dump_mysql.' --no-create-info --insert-ignore '.$value['tname'].'>'.$path.'tables/'.$value['tname'].'.sql';
          passthru($s,$ret);
          if ($ret==0) {
            $fn = $path.'tables/'.$value['tname'].'.sql';
            $fp = fopen($fn, 'r+');
            $s = fread($fp,filesize($fn));
            if (strpos($s,'TRIGGER')!==false) {
              $s = preg_replace('/\sDEFINER\s*=\s*`.+?@`.+?`/s',' ',$s);
              $s = rtrim (preg_replace('/((?<=\sTRIGGER\s)|(?<=\sON\s))\s*[`].+?[`]\./s','',$s));
              ftruncate($fp,0);
              fseek($fp,0);
              fwrite($fp,$s,strlen($s));
            };
            fclose($fp);
            echo "done";
          }else {
            echo "fail";
          }
          //passthru('/home/777/scrypt/make_dump '.$value['tname'].' '.$path.'tables/'.' tc --insert-ignore',$ret);
        };
      };

      function cmp($a, $b) {
        if ($a['prior'] == $b['prior']) {
         return 0;
        }
        return ($a['prior'] < $b['prior']) ? -1 : 1;
      }

      echo "<br>";
      echo 'Update table -> update_tbl ...';

      $go = true;
      while ($go) {
        $go = false;
        uasort($tables,'cmp');
        foreach ($tables as $i => $table) {
          if ($table['prior']!=-1) break;
          $prior = 1;
          $is_break = false;
          foreach ($table['refs'] as $j => $ref) {
            $is_break = false;
            foreach ($tables as $k => $tbl) {
              if ($ref == $tbl['name']) {
                if ($tbl['prior']==-1) {
                  $go = true;
                  $is_break = true;
                  break;
                }else {
                  $prior = $prior + $tbl['prior'];
                  //$prior = max($prior,$tbl['prior']+1);
                }
              }
            };
            if ($is_break) break;
          };
          if (!$is_break) {
            $tables[$i]['prior'] = $prior;
          }
        }
      }
      $cmd = Yii::app()->db->createCommand('UPDATE update_tbl SET prior = 0');
      $cmd ->execute();
      foreach ($tables as $i => $table) {
        $cmd = Yii::app()->db->createCommand('UPDATE update_tbl SET prior = :prior WHERE tname = :name');
        $cmd ->bindParam(':prior',$table['prior'],PDO::PARAM_INT);
        $cmd ->bindParam(':name',$table['name'],PDO::PARAM_STR);
        $cmd ->execute();
      };
      //passthru('/home/777/scrypt/make_dump '.'update_tbl'.' '.$path.'tables/'.' c',$ret);
      passthru($dump_mysql.'-c '.'update_tbl'.'>'.$path.'tables/'.'update_tbl'.'.sql',$ret);
      if ($ret == 0) {
        echo "done<br>";
      }else {
        echo "fail<br>";
      }
      echo "done.<br>";
    }
    else {
      echo "failed. Not found tables.<br>";
    }

    echo "Prepare files ... ";
    $this -> prepare_folder($path.'files');
    $cmd = Yii::app()->db->createCommand('SELECT fname,ftype,utype FROM update_src');
    $rows = $cmd->queryAll();
    if ($rows!=false) {
      foreach ($rows as $name=>$value) {
        if (($value['utype']=='w')) {
          if ($value['ftype']=='d') $up_src = $up_src.' '.substr($value['fname'],1).'/';
          elseif ($value['ftype']=='f') $up_src = $up_src.' '.substr($value['fname'],1);
        };
        if (($value['utype']=='c')) {
          if (($value['ftype']=='d') || ($value['ftype']=='f')) {
            $s = $value['fname'];
            $s = substr($s,0,strrpos($s,'/'));
            passthru('mkdir -p '.$path.'/files'.$s);
            passthru('cp -R '.$base_path.$value['fname'].' '.$path.'/files'.$value['fname']);
          }
        }
      };
      //$up_src = '"'.trim( $up_src).'"';
      $up_src = trim( $up_src);
      echo "done.<br>";
    }
    else {
      echo "failed. Not found sources.<br>";
    }

    echo "Create archive ... ";

    $fp = fopen($path.$up_name, 'w');
    $ss = '#!/bin/bash'.chr(10);
    fwrite($fp,$ss,strlen($ss));
    $ss = 'cd '.$base_path.'/'.chr(10);
    fwrite($fp,$ss,strlen($ss));
    $ss = 'tar cfz '.$path.$up_name.'.tar.gz '.$up_src;
    fwrite($fp,$ss,strlen($ss));
    fclose($fp);
    $ss = 'chmod 777 '.$path.$up_name;
    passthru($ss,$ret);
    $ss = $path.$up_name;
    //passthru('/home/777/scrypt/make_update '.$up_name.' '.$up_src,$ret);
    passthru($ss,$ret);
    if ($ret!=0) {
      echo "fail ! Error -> ".$ret." <br>";
      return true;
    }else {
      echo "done.<br>";
      $fn = $path.$up_name.'.tar.gz';
      $fp = fopen($fn, 'r');
      $fs = fstat($fp);
      echo  'Update file complite : '.$fn.'<br>Size : '.$fs['size'].' bytes ';
      fclose($fp);
      $md5ch = md5_file($fn);
      $fn = $path.$up_name.'.md5';
      $fp = fopen($fn, 'w');
      fwrite($fp,$md5ch,strlen($md5ch));
      fclose($fp);
      echo '  md5 -> '.$md5ch;
    }
    passthru('rm -Rf '.$ss);
    passthru('rm -Rf '.$path.'tables');
    passthru('rm -Rf '.$path.'files');
    return true;
  }

  private function cmd_get_update() {
    if ($this -> user_side===false) {
      if (isset ($_GET['version'])) {
        $ver = $this -> get_current_version();
        echo $ver;
        return true;
      };
      if (isset ($_GET['info'])) {
        $info = $this -> get_version_info();
        echo $info;
        return true;
      };
      if (isset ($_GET['file'])) {
        $path = realpath(Yii::app()->basePath.'/..');
        $path = $path.'/updates/';
        $up_name = $this -> get_current_version();
        $fn = $path.$up_name.'.tar.gz';
        if (is_file($fn)) {
          $fp = fopen($fn, 'r');
          $fl = fread($fp, filesize($fn));
          fclose($fp);
          echo $fl;
          unset($fl);
        };
        return true;
      };
      if (isset ($_GET['md5'])) {
        $path = realpath(Yii::app()->basePath.'/..');
        $path = $path.'/updates/';
        $up_name = $this -> get_current_version();
        $fn = $path.$up_name.'.md5';
        if (is_file($fn)) {
          $fp = fopen($fn, 'r');
          $fl = fread($fp, filesize($fn));
          fclose($fp);
          echo $fl;
          unset($fl);
        };
        return true;
      };
    };
    if (isset ($_POST['backup'])) {
      $s = $_POST['backup'];
      $s = substr($s,0,strpos($s,'_'));
      $this -> rollback_update($s);
      return true;
    }
    return true;
  }

  private function alter_table($regex,$tname,$asrc,$adst) {
    $fchange = false;
    if (preg_match($regex,$asrc,$ms)>0) {
      $fchange = true;
      if (preg_match($regex,$adst,$md)>0) {
        $fchange = ($ms[1] !== $md[1]);
      }
    }
    if ($fchange) {
      $s = 'ALTER TABLE `'.$tname.'` '.$ms[0].";".chr(10);
      return $s;
    }else {
      return null;
    }
  }

  private function rollback_update ($uname) {
    @ini_set('max_execution_time', 3000);
    $s = Yii::app()->db->connectionString;
    $st = 'dbname=';
    $i = strpos($s,$st);
    if ($i===false) {
      echo 'Error ! Database name not found in connection string -> '.$s." <br>";
      return;
    };
    $s = substr($s,$i+strlen($st));
    $i = strpos($s,';');
    if ($i!==false) {
      $s = substr($s,0,$i-1);
    }
    $dump_mysql = 'mysql --user='.Yii::app()->db->username.' --password='.Yii::app()->db->password.
      ' --default_character_set '.Yii::app()->db->charset.' '.$s.' <';
    $cur_ver = $this -> get_current_version();
    echo 'Current version: '.$cur_ver." <br>";
    echo 'Rollback to version: '.$uname." <br>";
    $base_path = realpath(Yii::app()->basePath.'/..');
    $backup_path = $base_path.'/backup';
    //$this -> prepare_folder ($backup_path.'/'.$uname);
    $tbl_path = $backup_path.'/tables';
    $upname = $backup_path.'/'.$uname.'_backup.tar.gz';
    if (! file_exists($upname) ) {
      echo 'Error ! Backup file version: '.$uname.' Not found in -> '.$upname."<br>";
      return;
    }
    echo 'Unpack backup ... ';
    //$s = 'tar xvzf '.$upname.' -C'.$backup_path.'/'.$uname.' >/dev/null 2>/dev/null';
    $s = 'tar xvzf '.$upname.' -C'.$base_path.'/'.' >/dev/null 2>/dev/null';
    passthru($s,$ret);
    if ($ret!=0) {
      echo 'Error unpack! Code: '.$ret." <br>";
      passthru('rm -Rf '.$tbl_path);
      //passthru('rm -Rf '.$backup_path.'/'.$uname);
      return true;
    };
    echo 'done. <br>';
    echo 'Update tables...  <br>';

    if ((! file_exists($tbl_path.'/update_tbl.sql')) || (! file_exists($tbl_path.'/update_src.sql'))) {
       echo "fail ! Backup file corrupt. <br>";
       return true;
    };
    $sc = $dump_mysql.$tbl_path.'/update_tbl.sql';
    passthru($sc,$ret);
    if ($ret!=0) {
      echo "fail ! Backup file corrupt. <br>";
      return true;
    }

    $cmd = Yii::app()->db->createCommand("SELECT tname,prior FROM update_tbl ORDER BY prior ASC");
    $rows = $cmd->queryAll();
    if ($rows!=false) {
      foreach ($rows as $name=>$value) {
        echo '  Table -> '.$value['tname'].' restore ... ';
        $sc = $dump_mysql.$tbl_path.'/'.$value['tname'].'.sql';
        passthru($sc,$ret);
        if ($ret==0) {
          echo "ok !<br>";
        }else {
          echo "fail !<br>";
        }
      }
    }
    passthru('rm -Rf '.$backup_path.'/tables');
    echo 'Complete!';
  }

  private function cmd_apply_update() {

    //$this -> rollback_update('now'); return;

    function trim_value(&$trvalue) { $trvalue = rtrim(trim($trvalue),',');}
    @ini_set('max_execution_time', 3000);
    $base_path = realpath(Yii::app()->basePath.'/..');
    $path = $base_path.'/updates/';
    $this -> log($path.'update.log',chr(10));
    $s = Yii::app()->db->connectionString;
    $st = 'dbname=';
    $i = strpos($s,$st);
    if ($i===false) {
      $this -> log($path.'update.log','Error ! Database name not found in connection string -> '.chr(10));
      echo 'Error ! Database name not found in connection string -> '.$s." <br>";
      return;
    };
    $s = substr($s,$i+strlen($st));
    $i = strpos($s,';');
    if ($i!==false) {
      $s = substr($s,0,$i-1);
    }
    $dump_mysql = 'mysql --user='.Yii::app()->db->username.' --password='.Yii::app()->db->password.
        ' --default_character_set '.Yii::app()->db->charset.' '.$s.' <';
    $this -> log($path.'update.log',$dump_mysql.chr(10));
    $dump_mysql_backup = 'mysqldump --user='.Yii::app()->db->username.' --password='.Yii::app()->db->password.
        ' --default_character_set '.Yii::app()->db->charset.' -c '.$s.' ';
    //return;
    //$dump_mysql = 'mysql --user=777 --password=Q1w2e3r4t5 --default_character_set utf8 111 <';
    //$dump_mysql_backup = 'mysqldump --user=777 --password=Q1w2e3r4t5 --default_character_set utf8 -c 111 ';

    $ver_url  = $this -> get_version_url();
    $info_url  = $this -> get_version_info_url();
    $file_url = $this -> get_update_file_url();
    $cur_ver = $this -> get_current_version();
    $md5_url = $this -> get_update_md5_url();
    $new_ver = 'Not available';
    $res = $this -> getData($ver_url);
    if (isset($res->info)) {
      if (($res->info['http_code']==200) or ($res->info['http_code']==206)) {
        $new_ver = $res-> content;
      }
    };
    unset($res);
    //$new_ver = '1';$this -> log($path.'update.log',.chr(10));
    echo 'Current version: '.$cur_ver." <br>";
    $this -> log($path.'update.log','Current version: '.$cur_ver.chr(10));
    echo 'New version: '.$new_ver." <br>";
    $this ->log($path.'update.log','New version: '.$new_ver.chr(10));
    if ( ($new_ver=='Not available') || ($new_ver==$cur_ver) ) {
      $this ->log($path.'update.log','No need to update');
      echo 'No need to update';
      return true;
    };
    $info = 'Not available';
    $res = $this -> getData($info_url);
    if (isset($res->info)) {
      if (($res->info['http_code']==200) or ($res->info['http_code']==206)) {
        $info = $res-> content;
      }
    };
    echo "New content on version '".$new_ver."': <br>".$info." <br>";
    $this ->log($path.'update.log','New content on version '.$new_ver.' : '.$info.chr(10));
    //$up_file = $path.$new_ver.'/'.$new_ver.'.tar.gz';
    $up_file = $path.'/'.$new_ver.'.tar.gz';

    $is_ready = false;
    if (file_exists($up_file)) {
      $md5_src = md5_file($up_file);
      echo 'Update file found, md5 = '.$md5_src.' -> check md5 for download...';
      $this ->log($path.'update.log','Update file found, md5 = '.$md5_src.' -> check md5 for download...');
      $res = $this -> getData($md5_url);
      if (isset($res->info)) {
        if (($res->info['http_code']==200) or ($res->info['http_code']==206)) {
          echo ' md5 = '.$res-> content;
          $this ->log($path.'update.log',' md5 = '.$res-> content);
          if ($md5_src==$res-> content) {
            echo ' correct. Download not needed.'." <br>";
            $this ->log($path.'update.log',' correct. Download not needed.'.chr(10));
            $is_ready = true;
          }else {
            echo ' not correct.'." <br>";
            $this ->log($path.'update.log',' not correct.'.chr(10));
          }
        }
        else {
          echo ' Error get md5: '.$res->info['http_code']." <br>";
          $this ->log($path.'update.log',' Error get md5 : '.$res->info['http_code'].chr(10));
        }
      }
      else {
        echo ' Error curl: '.$res->curl_error." <br>";
        $this ->log($path.'update.log',' Error curl: '.$res->curl_error.chr(10));
      };
      unset($res);
    };
    //$this -> prepare_folder ($path.$new_ver);
    if (!$is_ready) {
      echo 'Download update ... ';
      $this ->log($path.'update.log','Download update ... ');
      $res = $this -> getData($file_url);
      if (isset($res->info)) {
        if (($res->info['http_code']==200) or ($res->info['http_code']==206)) {
          $fp = fopen($up_file, 'w');
          fwrite($fp,$res-> content,strlen($res-> content));
          fclose($fp);
          echo strlen($res-> content)." bytes done. <br>";
          $this ->log($path.'update.log',strlen($res-> content).' bytes done.'.chr(10));
        }
        else {
          echo 'Error get update: '.$res->info['http_code']." <br>";
          $this ->log($path.'update.log','Error get update: '.$res->info['http_code'].chr(10));
          return true;
        }
      }
      else {
        echo 'Error curl: '.$res->curl_error." <br>";
        $this ->log($path.'update.log','Error curl: '.$res->curl_error.chr(10));
        return true;
      };
      unset($res);
    };
    //Make backup *******************************************************************************
    echo 'Backup ... ';
    $this ->log($path.'update.log','Backup ... ');
    $backup_path = $base_path.'/backup';
    $this -> prepare_folder ($backup_path);
    $this -> prepare_folder ($backup_path.'/tables');
    //passthru('tar xvzf '.$up_file.' -C'.$path.$new_ver.' updates/tables/update_tbl.sql updates/tables/update_src.sql'.' >/dev/null 2>/dev/null',$ret);
    passthru('tar xvzf '.$up_file.' -C'.$path.' updates/tables/update_tbl.sql updates/tables/update_src.sql'.' >/dev/null 2>/dev/null',$ret);
    if ($ret!=0) {
      echo 'Error unpack! Code: '.$ret." <br>";
      $this ->log($path.'update.log','Error unpack! Code: '.$ret.chr(10));
      passthru('rm -Rf '.$up_file);
      return true;
    };
    //$tbl_path = $path.$new_ver.'/updates/tables/';
    $tbl_path = $path.'/updates/tables/';
    if ((! file_exists($tbl_path.'update_tbl.sql')) || (! file_exists($tbl_path.'update_src.sql'))) {
      echo "fail ! Update corrupt. Not find files. <br>";
      $this ->log($path.'update.log','fail ! Update corrupt. Not find files.'.chr(10));
      return true;
    };
    $s = $dump_mysql.$tbl_path.'update_tbl.sql';
    passthru($s,$ret);
    if ($ret!=0) {
      echo $s.' <br>';
      echo '<br>';
      echo "fail ! Update corrupt. Not apply tables table  <br>";
      $this ->log($path.'update.log','fail ! Update corrupt. Not apply tables table'.chr(10));
      return true;
    }
    $s = $dump_mysql.$tbl_path.'update_src.sql';
    passthru($s,$ret);
    if ($ret!=0) {
      echo $s.' <br>';
      echo '<br>';
      echo "fail ! Update corrupt. Not apply sources table <br>";
      $this ->log($path.'update.log','fail ! Update corrupt. Not apply sources table'.chr(10));
      return true;
    };
    echo '<br>';
    echo 'Prepare files ... ';
    $this ->log($path.'update.log',chr(10).'Prepare files ... ');
    $up_src = 'backup/tables/';
    $this -> prepare_folder($backup_path);
    $cmd = Yii::app()->db->createCommand('SELECT fname,ftype FROM update_src');
    $rows = $cmd->queryAll();
    if ($rows!=false) {
      foreach ($rows as $name=>$value) {
        if ($value['ftype']=='d') $up_src = $up_src.' '.substr($value['fname'],1).'/';
        elseif ($value['ftype']=='f') $up_src = $up_src.' '.substr($value['fname'],1);
      };
      $up_src =trim( $up_src);
      echo "done.<br>";
      $this ->log($path.'update.log','done'.chr(10));
    }
    else {
      echo "failed. Not found sources.<br>";
      $this ->log($path.'update.log','failed. Not found sources.'.chr(10));
    }
    echo 'Prepare tables ... ';
    $this ->log($path.'update.log','Prepare tables ... ');
    $cmd = Yii::app()->db->createCommand("SELECT tname FROM update_tbl");
    $rows = $cmd->queryAll();
    if ($rows!=false) {
      $this -> prepare_folder($backup_path.'/tables');
      foreach ($rows as $name=>$value) {
        $ss = $dump_mysql_backup.$value['tname'].' >'.$backup_path.'/tables/'.$value['tname'].'.sql';
        passthru($ss,$ret);
      };
      echo "done.<br>";
      $this ->log($path.'update.log','done.'.chr(10));
    }
    else {
      echo "failed. Not found tables.<br>";
      $this ->log($path.'update.log','failed. Not found tables.'.chr(10));
    }
    echo 'Make ... ';
    $this ->log($path.'update.log','Make ... ');
    //$fp = fopen($path.$new_ver.'/'.$new_ver, 'w');
    $fp = fopen($path.'/'.$new_ver, 'w');
    $ss = '#!/bin/bash'.chr(10);
    fwrite($fp,$ss,strlen($ss));
    $ss = 'cd '.$base_path.'/'.chr(10);
    fwrite($fp,$ss,strlen($ss));
    $ss = 'tar cfz '.$backup_path.'/'.$new_ver.'_backup.tar.gz '.$up_src;
    fwrite($fp,$ss,strlen($ss));
    fclose($fp);
    $ss = 'chmod 777 '.$path.'/'.$new_ver;
    passthru($ss,$ret);
    $ss = $path.'/'.$new_ver;
    passthru($ss,$ret);
    if ($ret!=0) {
       echo 'fail ! Error -> '.$ret." <br>";
       $this ->log($path.'update.log','fail ! Error -> '.$ret.chr(10));
       return true;
    }else {
       echo "done.<br>";
       $this ->log($path.'update.log','done.'.chr(10));
    }
    passthru('rm -Rf '.$ss);
    passthru('rm -Rf '.$backup_path.'/tables');
    passthru('rm -Rf '.$path.'updates');
    echo "Created. <br>";
    $this ->log($path.'update.log','Created.'.chr(10));
    //backup *******************************************************************************
    $tbl_path = $path.'tables/';
    echo 'Unpack update ... ';
    $this ->log($path.'update.log','Unpack update ... ');
    //passthru('tar xvzf '.$up_file.' -C'.$path.$new_ver.' >/dev/null 2>/dev/null',$ret);
    passthru('tar xvzf '.$up_file.' -C'.$base_path.' >/dev/null 2>/dev/null',$ret);
    if ($ret!=0) {
      echo 'Error unpack! Code: '.$ret." <br>";
      $this ->log($path.'update.log','Error unpack! Code: '.$ret.chr(10));
      passthru('rm -Rf '.$up_file);
      return true;
    };
    //passthru('rm -Rf '.$up_file);
    echo 'done. <br>';
    $this ->log($path.'update.log','done.'.chr(10));
    echo 'Update tables...  <br>';
    $this ->log($path.'update.log','Update tables...'.chr(10));
    $cmd = Yii::app()->db->createCommand("SELECT tname,utype,prior FROM update_tbl WHERE Not (tname LIKE 'update%') ORDER BY prior ASC");
    $rows = $cmd->queryAll();
    if ($rows!=false) {
      foreach ($rows as $name=>$value) {
        if ($value['utype']=='w') {
          echo "<br>";
          echo '  Table -> '.$value['tname'].' rewrite ... ';
          $this ->log($path.'update.log',chr(10).'  Table -> '.$value['tname'].' rewrite ... ');
          $sc = $dump_mysql.$tbl_path.$value['tname'].'.sql';
          passthru($sc,$ret);
          if ($ret==0) {
            echo "ok !<br>";
            $this ->log($path.'update.log','ok !'.chr(10));
          }else {
            echo "fail !<br>";
            $this ->log($path.'update.log','fail !'.chr(10));
          }
        }
        if (($value['utype']=='s') || ($value['utype']=='c')) {
          $fn = $tbl_path.$value['tname'];
          $sc = $dump_mysql.$fn;
          echo "<br>";
          echo '  Table -> '.$value['tname'].' restruct ... ';
          $this ->log($path.'update.log',chr(10).'  Table -> '.$value['tname'].' restruct ... ');
          if (file_exists($fn)) {
            $fp = fopen($fn, 'r');
            $udt = fread($fp, filesize($fn));
            fclose($fp);
            $s = '';
            $cmd = Yii::app()->db->createCommand('SHOW CREATE TABLE '.$value['tname']);
            try {
              $row = $cmd->queryRow();
              if ($row!=false) {
                $s = $row['Create Table'];
                $s = rtrim(preg_replace('/(\/\*.*?\*\/)|(#.*?$)|(--\s.*?$)/sm','',$s));
              };
            } catch (Exception $e) {
                echo ' not found ! Create new ... ';
                $this ->log($path.'update.log',' not found ! Create new ... ');
                $s = '';
            };
            if ($s=='') { //Create new table
              passthru($sc,$ret);
              if ($ret==0) {
                echo "ok !<br>";
                $this ->log($path.'update.log','ok !'.chr(10));
              }else {
                echo "fail !<br>";
                $this ->log($path.'update.log','fail !'.chr(10));
              }
            }else {
              $asrc = null;$adst = null; //Check for ALTER TABLE
              $tscrypt = null;
              $i = strrpos($s,chr(10),1);
              if ($i!==false) {
                $adst = substr($s,$i+3);
                $s = substr($s,0,$i);
              };
              $i = strrpos($udt,chr(10),1);
              if ($i!==false) {
                $asrc = substr($udt,$i+3);
                $udt = substr($udt,0,$i);
              };
              if ((isset($asrc))&&(isset($adst))) {
                $sr = $this -> alter_table('/ENGINE\s*=\s*(.*?)[\s$]/i',$value['tname'],$asrc,$adst);
                If (isset($sr)) $tscrypt = $tscrypt.$sr;
                $sr = $this -> alter_table('/DEFAULT CHARSET\s*=\s*(.*?)[\s$]/i',$value['tname'],$asrc,$adst);
                If (isset($sr)) $tscrypt = $tscrypt.$sr;
                $sr = $this -> alter_table('/ROW_FORMAT\s*=\s*(.*?)[\s$]/i',$value['tname'],$asrc,$adst);
                If (isset($sr)) $tscrypt = $tscrypt.$sr;
                $sr = $this -> alter_table('/COMMENT\s*=\s*(\'.*?\')/',$value['tname'],$asrc,$adst);
                If (isset($sr)) $tscrypt = $tscrypt.$sr;
              }
              if ($udt != $s) {
                $src = explode("\n",$udt);
                unset($src[0]);
                array_walk($src, 'trim_value');
                $dst = explode("\n",$s);
                unset($dst[0]);
                array_walk($dst, 'trim_value');
                $change = array();
                foreach ($src as $i => $sval) { //Test changes
                  if (($key = array_search($sval,$dst)) !== false) {
                    $i = strpos($sval,'FOREIGN KEY');
                    if (($i===false)) {
                      unset($dst[$key]);
                    }
                    else {
                      $change[] = $sval;
                    }
                  }else {
                    $change[] = $sval;
                  }
                }
                foreach ($dst as $i => $sval) { //Remove or update
                  $f = substr($sval,0,1);
                  if ($f == '`') { //Remove or update field
                    $field = substr($sval,0, strpos($sval,'`',1)+1);
                    $is_up = false; $key = null;
                    foreach ($change as $j => $cval) {
                      $pos = strpos($cval,$field);
                      if ($pos===0) {
                        $is_up = true;
                        $key = $j;
                        break;
                      }
                    }
                    if ($is_up) {
                      $tscrypt = $tscrypt.'ALTER TABLE `'.$value['tname'].'` MODIFY COLUMN '.$change[$key].";".chr(10);
                      unset($change[$key]);
                    }
                    else {
                      $tscrypt = $tscrypt.'ALTER TABLE `'.$value['tname'].'` DROP COLUMN '.$field.";".chr(10);
                    }
                  }else { //Remove key or constraint
                     $str = trim (substr($sval,strpos($sval,'`',1)+1));
                     $idx =  trim (substr($str,0,strpos($str,'`')));
                     $i = strpos($sval,'FOREIGN KEY');
                     if ( ($i!==false)) { //Remove FOREIGN KEY
                       $tscrypt = 'ALTER TABLE `'.$value['tname'].'` DROP FOREIGN KEY `'.$idx."`;".chr(10).$tscrypt;
                     }else { //Remove key or index
                       $i = strpos($sval,'PRIMARY KEY');
                       if ( ($i!==false)) { //Remove PRIMARY KEY
                         $tscrypt = $tscrypt.'ALTER TABLE `'.$value['tname'].'` DROP PRIMARY KEY `'.$idx."`;".chr(10);
                       }else { //Remove index
                         $tscrypt = $tscrypt.'ALTER TABLE `'.$value['tname'].'` DROP INDEX `'.$idx."`;".chr(10);
                       }
                     }
                  }
                }
                foreach ($change as $i => $sval) { //Add
                  $f = substr($sval,0,1);
                  if ($f == '`') { //Add field
                    $tscrypt = $tscrypt.'ALTER TABLE `'.$value['tname'].'` ADD COLUMN '.$sval.";".chr(10);
                  }else { //Add key or constraint
                    $tscrypt = $tscrypt.'ALTER TABLE `'.$value['tname'].'` ADD '.$sval.";".chr(10);
                  }
                }
              }else {
                //echo "no more change. ok !<br>";
              }
              if (isset($tscrypt)) {
                //echo "<br>";

                $tname = $value['tname']; //Triggers remove & add
                $dtriggers = '';
                $cmd = Yii::app()->db->createCommand('SHOW TRIGGERS WHERE `Table` = :name');
                $cmd ->bindParam(':name',$tname,PDO::PARAM_STR);
                $trows = $cmd->queryAll();
                if ($trows!=false) {
                  foreach ($trows as $name=>$tvalue) {
                    $dtriggers =$dtriggers.'DROP TRIGGER IF EXISTS '.$tvalue ['Trigger'].';'.chr(10);
                  }
                };
                $atriggers = '';
                $fn = $tbl_path.$tname.'.triggers.sql';
                if (file_exists($fn)) {
                  $fp = fopen($fn, 'r');
                  $atriggers = fread($fp,filesize($fn));
                  //$atriggers = 'DELIMITER ;;'.chr(10).'/*!50003 '.$atriggers.' */;;'.chr(10).'DELIMITER ;'.chr(10);
                  //$insrtion = ' DEFINER=`'.Yii::app()->db->username.'`@`%`';
                  //$atriggers = preg_replace('/CREATE\s+TRIGGER/is','CREATE '.$insrtion.' TRIGGER',$atriggers);
                  fclose($fp);
                };

                $spf = 'LOCK TABLES `'.$value['tname'].'` WRITE;'.chr(10).
                    'SET FOREIGN_KEY_CHECKS=0;'.chr(10).
                    'SET UNIQUE_CHECKS=0;'.chr(10).
                    'ALTER TABLE `'.$value['tname'].'` DISABLE KEYS;'.chr(10);
                $spp = 'ALTER TABLE `'.$value['tname'].'` ENABLE KEYS;'.chr(10).
                    'SET UNIQUE_CHECKS=1;'.chr(10).
                    'SET FOREIGN_KEY_CHECKS=1;'.chr(10).
                    'UNLOCK TABLES;';
                $tscrypt = $spf.$dtriggers.$tscrypt.$atriggers.$spp;
                $fn = $tbl_path.$value['tname'].'_upd.sql';
                $fp = fopen($fn, 'w');
                fwrite($fp,$tscrypt,strlen($tscrypt));
                fclose($fp);
                passthru($dump_mysql.$tbl_path.$value['tname'].'_upd.sql',$ret);
                if ($ret!=0) {
                  echo 'fail ! Error -> '.$ret.' -> Unlock table ... ';
                  $this ->log($path.'update.log','fail ! Error -> '.$ret.' -> Unlock table ... ');
                  $tscrypt ='SET UNIQUE_CHECKS=1;'.chr(10).
                        'SET FOREIGN_KEY_CHECKS=1;'.chr(10).
                        'UNLOCK TABLES;';
                  $fn = $tbl_path.$value['tname'].'_unlock.sql';
                  $fp = fopen($fn, 'w');
                  fwrite($fp,$tscrypt,strlen($tscrypt));
                  fclose($fp);
                  passthru($dump_mysql.$tbl_path.$value['tname'].'_unlock.sql',$ret);
                  if ($ret!=0) {
                    echo "done. <br>";
                    $this ->log($path.'update.log','done.'.chr(10));
                  }else {
                    echo 'fail ! Error -> '.$ret." <br>";
                    $this ->log($path.'update.log','fail ! Error -> '.$ret.chr(10));
                  }
                }else {
                  echo "ok ! <br>";
                  $this ->log($path.'update.log','ok !'.chr(10));
                }
              }else {
                echo "Not modyfied. ok !<br>";
                $this ->log($path.'update.log','Not modyfied. ok !'.chr(10));
              }
            }
            if ($value['utype']=='c') {
              $sc = $sc.'.sql';
              echo '  Table -> '.$value['tname'].' data update ... ';
              $this ->log($path.'update.log','  Table -> '.$value['tname'].' data update ... ');
              $cmd = Yii::app()->db->createCommand('DELETE FROM `'.$value['tname'].'` WHERE is_update = 1');
              $rc = $cmd->execute();
              passthru($sc,$ret);
              if ($ret==0) {
                echo "ok !<br>";
                $this ->log($path.'update.log','ok !'.chr(10));
              }else {
                echo "fail !<br>";
                $this ->log($path.'update.log','fail !'.chr(10));
              }
            }
          }else {
            echo "not found !<br>";
            $this ->log($path.'update.log','not found !'.chr(10));
          }
        }
      }
    };
    echo 'Complete!';
    $this ->log($path.'update.log','Complete!'.chr(10));
    //passthru('rm -Rf '.$path.'tables');
    //passthru('rm -Rf '.$path.$new_ver);
    return true;
  }
}