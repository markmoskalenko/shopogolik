<?php

class CmsHistoryController extends CustomAdminController {
  public $breadcrumbs;
  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = 'main';

  public function actionRestore() {
    // Uncomment the following line if AJAX validation is needed
     if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['id'])) {
          $model=CmsContentHistory::model()->findByPk($_POST['id']);
        if ($model) {
          $content=$model->content;
          $content_id=$model->content_id;
          $lang=$model->lang;

          if ($model['table_name']=='cms_menus') {
            Yii::app()->db->createCommand("update cms_menus tt
            set tt.menu_data = :menu_data
            where tt.menu_id=:menu_id")
              ->bindParam( ':menu_data',$content,PDO::PARAM_STR)
              ->bindParam(':menu_id',$content_id,PDO::PARAM_STR)
              ->execute();
          } elseif ($model['table_name']=='cms_pages_content') {
            Yii::app()->db->createCommand("update cms_pages_content tt
            set tt.content_data = :content_data
            where tt.content_id=:content_id and tt.lang=:lang")
              ->bindParam( ':content_data',$content,PDO::PARAM_STR)
              ->bindParam(':content_id',$content_id,PDO::PARAM_STR)
              ->bindParam(':lang',$lang,PDO::PARAM_STR)
              ->execute();
          } elseif ($model['table_name']=='cms_custom_content') {
            Yii::app()->db->createCommand("update cms_custom_content tt
            set tt.content_data = :content_data
            where tt.content_id=:content_id and tt.lang=:lang")
            ->bindParam( ':content_data',$content,PDO::PARAM_STR)
            ->bindParam(':content_id',$content_id,PDO::PARAM_STR)
            ->bindParam(':lang',$lang,PDO::PARAM_STR)
            ->execute();
          }
          echo Yii::t('admin', 'Контент восстановлен из истории изменений');
        } else {
          echo Yii::t('admin', 'Ошибка восстановления контента из истории изменений');
        }
        }
        return;
      }
    }
}
