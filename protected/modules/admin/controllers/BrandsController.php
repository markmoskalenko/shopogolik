<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="BrandsController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class BrandsController extends CustomAdminController
{
 // public $defaultAction = 'admin';
        public $breadcrumbs;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='main';

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$id=$_REQUEST["id"];
	     
	       if(Yii::app()->request->isAjaxRequest)
	       {
	         $this->renderPartial('ajax_view',array(
			'model'=>$this->loadModel($id),
		));
	         
	       }
	       else
	       {
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	       }
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	
            $model=new Brands;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model,"brands-create-form");
            if(Yii::app()->request->isAjaxRequest)
	       {
		    if(isset($_POST['Brands']))
		    {
			    $model->attributes=$_POST['Brands'];
			    if($model->save())
			    {
			      echo $model->id;
			    }
			    else
			    {
			      echo "false";
			    } 
			    return;
		    }
	       }
	       else
	       {
	           if(isset($_POST['Brands']))
		    {
			    $model->attributes=$_POST['Brands'];
			    if($model->save())
			     $this->redirect(array('view','id'=>$model->id));
			
		    }
               
		    $this->render('create',array(
			    'model'=>$model,
		    ));
	       }	
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
      
	    $id=isset($_REQUEST["id"])?$_REQUEST["id"]:$_REQUEST["Brands"]["id"];
	    $model=$this->loadModel($id);
			    
	    // Uncomment the following line if AJAX validation is needed
	      $this->performAjaxValidation($model,"brands-update-form");
	    
	  if(Yii::app()->request->isAjaxRequest)
	    {
	    
		if(isset($_POST['Brands']))
		{
		  
			$model->attributes=$_POST['Brands'];
			if($model->save())
			{
			  echo $model->id;
			}
			else
			{
			  echo "false";
			}
			return;
		}
		    
		  $this->renderPartial('_ajax_update_form',array(
		    'model'=>$model,
		    ));
		  return; 
	    
	    }
	    

	    if(isset($_POST['Brands']))
	    {
		    $model->attributes=$_POST['Brands'];
		    if($model->save())
			    $this->redirect(array('view','id'=>$model->id));
	    }

	    $this->render('update',array(
		    'model'=>$model,
	    ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
	        $id=$_POST["id"];
	   
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset(Yii::app()->request->isAjaxRequest))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			else
			   echo "true";
		}
		else
		{
		    if(!isset($_GET['ajax']))
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		    else
			   echo "false"; 	
	        }	
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $session=new CHttpSession;
            $session->open();		
            $criteria = new CDbCriteria();            

                $model=new Brands('search');
                $model->unsetAttributes();  // clear any default values

                if(isset($_GET['Brands']))
		{
                        $model->attributes=$_GET['Brands'];
			
			
                   	
                       if (!empty($model->id)) $criteria->addCondition('id = "'.$model->id.'"');
                     
                    	
                       if (!empty($model->name)) $criteria->addCondition('name = "'.$model->name.'"');
                     
                    	
                       if (!empty($model->enabled)) $criteria->addCondition('enabled = "'.$model->enabled.'"');
                     
                    	
                       if (!empty($model->img_src)) $criteria->addCondition('img_src = "'.$model->img_src.'"');
                     
                    	
                       if (!empty($model->vid)) $criteria->addCondition('vid = "'.$model->vid.'"');
                     
                    	
                       if (!empty($model->query)) $criteria->addCondition('query = "'.$model->query.'"');
                     
                    	
                       if (!empty($model->url)) $criteria->addCondition('url = "'.$model->url.'"');
                     
                    	
                       if (!empty($model->meta_desc_ru)) $criteria->addCondition('meta_desc_ru = "'.$model->meta_desc_ru.'"');
                     
                    	
                       if (!empty($model->meta_keeword_ru)) $criteria->addCondition('meta_keeword_ru = "'.$model->meta_keeword_ru.'"');
                     
                    	
                       if (!empty($model->page_title_ru)) $criteria->addCondition('page_title_ru = "'.$model->page_title_ru.'"');
                     
                    	
                       if (!empty($model->meta_desc_en)) $criteria->addCondition('meta_desc_en = "'.$model->meta_desc_en.'"');
                     
                    	
                       if (!empty($model->meta_keeword_en)) $criteria->addCondition('meta_keeword_en = "'.$model->meta_keeword_en.'"');
                     
                    	
                       if (!empty($model->page_title_en)) $criteria->addCondition('page_title_en = "'.$model->page_title_en.'"');
                     
                    	
                       if (!empty($model->page_desc_ru)) $criteria->addCondition('page_desc_ru = "'.$model->page_desc_ru.'"');
                     
                    	
                       if (!empty($model->page_desc_en)) $criteria->addCondition('page_desc_en = "'.$model->page_desc_en.'"');
                     
                    			
		}
                 $session['Brands_records']=Brands::model()->findAll($criteria); 
       

                $this->render('index',array(
			'model'=>$model,
		));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Brands('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Brands']))
			$model->attributes=$_GET['Brands'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Brands::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model,$form_id)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']===$form_id)
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionGenerateExcel()
	{
            $session=new CHttpSession;
            $session->open();		
            
             if(isset($session['Brands_records']))
               {
                $model=$session['Brands_records'];
               }
               else
                 $model = Brands::model()->findAll();

		
		Yii::app()->request->sendFile(date('YmdHis').'.xls',
			$this->renderPartial('excelReport', array(
				'model'=>$model
			), true,false,true)
		);
	}
        public function actionGeneratePdf() 
	{
           $session=new CHttpSession;
           $session->open();
		Yii::import('application.extensions.ajaxgii.bootstrap.*');
		require_once('tcpdf/tcpdf.php');
      if (Utils::AppLang()=='ru') {
        require_once('tcpdf/config/lang/rus.php');
      } else {
        require_once('tcpdf/config/lang/eng.php');
      }

             if(isset($session['Brands_records']))
               {
                $model=$session['Brands_records'];
               }
               else
                 $model = Brands::model()->findAll();



		$html = $this->renderPartial('expenseGridtoReport', array(
			'model'=>$model
		), true);
		
		//die($html);
		
		$pdf = new TCPDF();
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor(DSConfig::getVal('site_name'));
		$pdf->SetTitle('Brands Report');
		$pdf->SetSubject('Brands Report');
		//$pdf->SetKeywords('example, text, report');
		$pdf->SetHeaderData('', 0, "Report", '');
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Report by ".DSConfig::getVal('site_name'), "");
		$pdf->setHeaderFont(Array('helvetica', '', 8));
		$pdf->setFooterFont(Array('helvetica', '', 6));
		$pdf->SetMargins(15, 18, 15);
		$pdf->SetHeaderMargin(5);
		$pdf->SetFooterMargin(10);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetFont('dejavusans', '', 7);
		$pdf->AddPage();
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->LastPage();
		$pdf->Output("Brands_002.pdf", "I");
	}

    public function actionUpdateMeta() {
        if(Brands::updateAllMeta()){
            echo 'OK';
        } else {
            echo 'ERROR';
        };
    }

}
