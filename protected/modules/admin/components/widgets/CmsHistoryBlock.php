<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CmsHistoryBlock.php">
* </description>
**********************************************************************************************************************/?>
<?php

class CmsHistoryBlock extends CustomWidget {
 
  public $tableName = '';
  public $contentId = '';
  public $contentLang='';
  public $pageSize = 50;

  public function run() {
    $model=new CmsContentHistory('Search');
    $model->table_name=$this->tableName;
    $model->content_id=$this->contentId;
    $model->lang=$this->contentLang;
/*
		$criteria->compare('id',$this->id,true);
		$criteria->compare('table_name',$this->table_name);
		$criteria->compare('content_id',$this->content_id);
		$criteria->compare('lang',$this->lang);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('content',$this->content,true);
*/
      $dataProvider = $model->search($this->pageSize);

    $this->render('application.modules.admin.views.widgets.CmsHistoryBlock.CmsHistoryBlock', array(
      'dataProvider' => $dataProvider,
      'id' => $this->tableName.'-'.$this->contentId,
    ));
  }
}
