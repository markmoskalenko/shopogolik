<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AdminNewsBlock.php">
* </description>
**********************************************************************************************************************/?>
<?php

class AdminNewsBlock extends CustomWidget {

  public $id = FALSE;
  public $pageSize = 25;

  public function run() {
    $model=new AdminNews('Search');
      $dataProvider = $model->search($this->pageSize);

    $this->render('application.modules.admin.views.widgets.AdminNewsBlock.AdminNewsBlock', array(
      'dataProvider' => $dataProvider,
      'id' => $this->id,
    ));
  }
}
