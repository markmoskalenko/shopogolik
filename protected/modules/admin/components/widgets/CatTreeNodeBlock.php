<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CatTreeNodeBlock.php">
* </description>
**********************************************************************************************************************/?>
<?php

class CatTreeNodeBlock extends CustomWidget {

  public $nodeData = FALSE;
  public $lang = 'ru';
  public function run() {
      if ($this->nodeData) {
    $this->render('application.modules.admin.views.widgets.CatTreeNodeBlock.CatTreeNodeBlock', array(
      'nodeData' => $this->nodeData,
      'lang' => $this->lang,
    ));
      };
  }
}
