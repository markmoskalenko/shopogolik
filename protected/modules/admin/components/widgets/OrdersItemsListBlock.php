<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersListBlock.php">
* </description>
**********************************************************************************************************************/?>
<?php

class OrdersItemsListBlock extends CustomWidget {

  public $type = FALSE;
  public $name = FALSE;
  public $pageSize = 50;
  public $narrowView = FALSE;
  public $manager = NULL;
  public $idPrefix = '';
  public $uid = NULL;
//  public $manager = null;
  public $filter = FALSE;

  public function run() {
    if ($this->name == FALSE) {
      $this->name = Yii::t('admin', 'Лоты');
    }
//=================================================================================
    if ($this->manager == NULL) {
      if ($this->uid == NULL) {
        if (Yii::app()->user->inRole(array('superAdmin', 'topManager'))) {
          $this->manager = NULL;
        }
        elseif (Yii::app()->user->inRole(array('orderManager'))) {
          $this->manager = Yii::app()->user->id;
        }
        else {
          $this->uid = Yii::app()->user->id;
          $this->manager = NULL;
        }
      }
    }
    $dataProvider = OrdersItems::getAdminOrdersItemsList($this->type, $this->uid, $this->manager, $this->pageSize);
    if ($this->filter) {
      $filter = $dataProvider->model;
    }
    else {
      $filter = NULL;
    }
//=================================================================================
    $this->render('application.modules.admin.views.widgets.OrdersItemsListBlock.OrdersItemsListBlock', array(
      'dataProvider' => $dataProvider,
      'type'         => $this->type,
      'name'         => $this->name,
      'idPrefix'     => $this->idPrefix,
      'filter'       => $filter,
      'narrowView'   => $this->narrowView,
    ));
  }
}
