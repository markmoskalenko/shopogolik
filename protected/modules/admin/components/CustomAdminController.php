<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="CustomAdminController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class CustomAdminController extends CustomController {

    public $breadcrumbs=array();

    public $menu = array();

    public $manager = '';

  public function init() {
//      header('Access-Control-Allow-Origin: *');
    parent::init();
  }

    function __construct($id, $module = null,$view_admin = null) {
     // Yii::app()->language='en';
      //register translation messages from module dbadmin
       if (isset(Yii::app()->request->cookies['lang'])) {
        Yii::app()->language= Yii::app()->request->cookies['lang']->value;}
      else {
        Yii::app()->language='en';
      }
        if (Yii::app()->getBaseUrl(true)== 'http://'.DSConfig::getVal('site_domain')) {
            $cookie = new CHttpCookie('lang', Yii::app()->language);
            $cookie->expire = time() + 60 * 60 * 24 * 180;
            Yii::app()->request->cookies['lang'] = $cookie;
        }

      //so no need do add to config/main.php
        $this->manager = (Yii::app()->user->checkAccess('admin/main/*')) ? 0 : Yii::app()->user->id;
//======================
//      if ($id<>'main') {
//      Yii::app()->clientscript->scriptMap['jquery.js'] = false;
//      Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
//      Yii::app()->clientscript->scriptMap['jquery-ui.min.js'] = false;
/*      }else {
        Yii::app()->clientscript->scriptMap['jquery.js'] = false;
        Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
        Yii::app()->clientscript->scriptMap['jquery-ui.min.js'] = false;
      }
*/
//======================
//--      Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl .'/themes/admin/css/'.'easyui.css');
//--      Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl .'/themes/admin/css/'.'icon.css');
//      Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl .'/themes/admin/css/'.'pager.css');
//--      Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl .'/themes/admin/js/'.'jquery.easyui.min.js',CClientScript::POS_END);
//--      Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl .'/themes/admin/js/'.'easyui-lang-ru.js',CClientScript::POS_END);
//       Yii::app()->clientScript->registerCoreScript('jquery');
      Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl .'/themes/admin/js/'.'ds.admin.logic.js',CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl .'/themes/admin/js/'.'ds.admin.interface.js',CClientScript::POS_END);
        parent::__construct($id, $module);

//        echo '<pre>';
//        print_r($_GET);
//        echo '</pre>';
//        die();
    }
  public function filters()
  {
    return array(
      array('application.components.RightsFilter'), array('application.components.PostprocessFilter'),// perform access control for CRUD operations
    );
  }

}

