<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="SRichTextarea.php">
* </description>
**********************************************************************************************************************/?>
<?php

Yii::import('ext.elrte.SElrteArea');

/**
 * Draw textarea widget
 */
class SRichTextarea extends SElrteArea
{
	public function setModel($model)
	{
		$this->model=$model;
	}
}
