<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="IntSearch.php">
* </description>
**********************************************************************************************************************/?>
<?php
class IntSearch {
  public $query;
  private static $searchRules=array(
      'users'=>array(
        'query'=>"select concat('Users',uu.uid) as id, uu.uid as pk, 'Users' as type, :query as query
from users uu
where uu.uid like :query
or uu.email = :query
or uu.lastname = :query
or uu.firstname = :query
or uu.patroname = :query
or uu.role = :query
or uu.phone = :query
or uu.country = :query
or uu.city = :query
or uu.`index` = :query
or uu.address = :query
or uu.skype = :query
or uu.vk = :query
union
select concat('Users',uu.uid) as id, uu.uid as pk, 'Users' as type, :query as query
from users uu
where uu.uid like concat('%',:query,'%')
or uu.email like concat('%',:query,'%')
or uu.lastname like concat('%',:query,'%')
or uu.firstname like concat('%',:query,'%')
or uu.patroname like concat('%',:query,'%')
or uu.role like concat('%',:query,'%')
or uu.phone like concat('%',:query,'%')
or uu.country like concat('%',:query,'%')
or uu.city like concat('%',:query,'%')
or uu.`index` like concat('%',:query,'%')
or uu.address like concat('%',:query,'%')
or uu.skype like concat('%',:query,'%')
or uu.vk like concat('%',:query,'%')"),
    'orders'=>array(
      'query'=>"select concat('Order',oo.id) as id, oo.id as pk, 'Order' as type, :query as query
from orders oo
where oo.id = :query
or concat(oo.uid,'-',oo.id) = :query
or oo.status = :query
or oo.code = :query
or oo.delivery_id = :query
union
select concat('Order',oo.id) as id, oo.id as pk, 'Order' as type, :query as query
from orders oo
where oo.id like concat('%',:query,'%')
or concat(oo.uid,'-',oo.id) like concat('%',:query,'%')
or oo.status like concat('%',:query,'%')
or oo.code like concat('%',:query,'%')
or oo.delivery_id like concat('%',:query,'%')"),
    'ordersItems'=>array(
      'query'=>"select concat('OrdersItems',oi.id) as id, oi.id as pk, 'OrdersItems' as type, :query as query
from orders_items oi, orders_items_statuses ois
where oi.status=ois.id
and(
oi.id = :query
or concat(oi.oid,'-',oi.id) = :query
or ois.name = :query
or oi.tid rlike concat('^[[:space:]]*',:query,'[[:space:]]*$')
or oi.track_code rlike concat('^[[:space:]]*',:query,'[[:space:]]*$')
)
union
select concat('OrdersItems',oi.id) as id, oi.id as pk, 'OrdersItems' as type, :query as query
from orders_items oi, orders_items_statuses ois
where oi.status=ois.id
and(
oi.id like concat('%',:query,'%')
or concat(oi.oid,'-',oi.id) like concat('%',:query,'%')
or ois.name like concat('%',:query,'%')
-- or oi.tid like concat('%',:query,'%')
-- or oi.track_code like concat('%',:query,'%')
)"),

  );
  public function Search() {
    if (($this->query=='')||is_null($this->query)) {
   return false;
    } else {
      $intQuery=str_replace(' ','%',$this->query);
      $res=array();
      foreach (self::$searchRules as $searchRule) {
        $sql=$searchRule['query'];
        $sqlRes=Yii::app()->db->createCommand($sql)
          ->bindParam(':query',$intQuery,PDO::PARAM_STR)
          ->queryAll();
        if ($sqlRes) {
          $res=array_merge($res,$sqlRes);
        }
      }
      $result = new CArrayDataProvider($res, array(
        'id' => 'searchResults',
        'keyField' => 'id',
          /*      'sort'=>array(
                  'attributes'=>array(
                    'id', 'username', 'email',
                  ),
                ),
          */
        'pagination' => array(
          'pageSize' => 25,
        ),
      ));
      return $result;
    }
  }
}

