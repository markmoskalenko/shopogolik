<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="InstallForm.php">
* </description>
**********************************************************************************************************************/?>
<?php

class InstallForm extends CFormModel {
  public $billing_use_operator_account = 1; // Отображать ли и использовать ли счёт оператора для начисления премий и бонусов
  public $checkout_delivery_needed = 1; // Нужно ли при оформлении заказа считать и вводить службу доставки
  public $checkout_order_reconfirmation_needed = 0; // Подтверждение заказа покупателем после его расчёта менеджером
  public $checkout_payment_needed = 1; // Нужно ли при оформлении заказа его оплачивать
  public $checkout_reorder_needed = 1; // Использовать ли дозаказ?
  public $checkout_weight_needed = 1; // Нужно ли покупателю при заказе вводить вес
  public $price_0_10 = 1; // Скидка на товары 0-9.99 юаней, например, 0.9 - это скидка в 10%
  public $price_10000_1000000000 = 0.85; // Скидка на товары дороже 10000 юаней
  public $price_1000_2000 = 0.93; // Скидка на товары 1000-1999.99 юаней
  public $price_10_60 = 0.99; // Скидка на товары 10-59.99 юаней
  public $price_150_200 = 0.96; // Скидка на товары 150-199.99 юаней
  public $price_2000_5000 = 0.92; // Скидка на товары 2000-4999.99 юаней
  public $price_200_500 = 0.95; // Скидка на товары 200 – 499.99 юаней
  public $price_5000_10000 = 0.9; // Скидка на товары 5000-9999.99 юаней
  public $price_500_1000 = 0.94; // Скидка на товары 500-999.99 юаней
  public $price_60_90 = 0.98; // Скидка на товары 60-89.99 юаней
  public $price_90_150 = 0.97; // Скидка на товары 90-149.99 юаней
  public $price_main_k = 1.7; // Основной коэффициент в цене, например 1.5 это наценка в 50%
  public $rate_byr = 0.00364; // Курс белорусского рубля к основной валюте
  public $rate_cny = 5.157672; // Курс юаня к основной валюте
  public $rate_eur = 41.15; // Курс евро к основной валюте
  public $rate_kzt = 0.22; // Курс тенге к основной валюте
  public $rate_rur = 1; // Курс рубля к основной валюте
  public $rate_uah = 3.85; // Курс гривны к основной валюте
  public $rate_usd = 31.72; // Курс доллара к основной валюте
  public $seo_sitemap_static = '<url><loc>http://demo.dropshop.pro</loc></url>'; // Статическая часть sitemap.xml
  public $site_currency = 'usd'; // Главная валюта сайта, валюта админки и кабинета
  public $site_currency_block = 'usd,eur,rur,uah,byr,kzt,cny'; // Валюты, которые доступны пользователю на сайте (маленькими буквами, через запятую, без пробелов, в одну строку)
  public $site_domain = 'demo.dropshop.pro'; // Домен сайта
  public $site_name = 'DropShop.pro'; // Название сайта
  public $skidka_0_10 = 1; // Скидка на количество 0-10
  public $skidka_10000_1000000000 = 0.99; // Скидка на количество более 10000
  public $skidka_1000_2000 = 0.85; // Скидка на количество 1000-2000
  public $skidka_10_60 = 0.95; // Скидка на количество 10-60
  public $skidka_150_200 = 0.95; // Скидка на количество 150-200
  public $skidka_2000_5000 = 0.9; // Скидка на количество 2000-5000
  public $skidka_200_500 = 0.9; // Скидка на количество 200-500
  public $skidka_5000_10000 = 0.9; // Скидка на количество 5000-10000
  public $skidka_500_1000 = 0.9; // Скидка на количество 500-1000
  public $skidka_60_90 = 0.95; // Скидка на количество 60-90
  public $skidka_90_150 = 0.95; // Скидка на количество 90-150
  public $taobao_EnabelDiscounts = 1; // Использовать ли скидки от таобао?

  function rules() {
    return array(
      array('billing_use_operator_account', 'numerical'),
      // 1 - Отображать ли и использовать ли счёт оператора для начисления премий и бонусов
      array('checkout_delivery_needed', 'numerical'),
      // 1 - Нужно ли при оформлении заказа считать и вводить службу доставки
      array('checkout_delivery_sum_needed', 'numerical'),
        // 1 - Нужно ли при оформлении заказа считать службу доставки
      array('checkout_order_reconfirmation_needed', 'numerical'),
      // 0 - Подтверждение заказа покупателем после его расчёта менеджером
      array('checkout_payment_needed', 'numerical'),
      // 1 - Нужно ли при оформлении заказа его оплачивать
      array('checkout_reorder_needed', 'numerical'),
      // 1 - Использовать ли дозаказ?
      array('checkout_weight_needed', 'numerical'),
      // 1 - Нужно ли покупателю при заказе вводить вес
      array('price_0_10', 'numerical'),
      // 1 - Скидка на товары 0-9.99 юаней, например, 0.9 - это скидка в 10%
      array('price_10000_1000000000', 'numerical'),
      // 0.85 - Скидка на товары дороже 10000 юаней
      array('price_1000_2000', 'numerical'),
      // 0.93 - Скидка на товары 1000-1999.99 юаней
      array('price_10_60', 'numerical'),
      // 0.99 - Скидка на товары 10-59.99 юаней
      array('price_150_200', 'numerical'),
      // 0.96 - Скидка на товары 150-199.99 юаней
      array('price_2000_5000', 'numerical'),
      // 0.92 - Скидка на товары 2000-4999.99 юаней
      array('price_200_500', 'numerical'),
      // 0.95 - Скидка на товары 200 – 499.99 юаней
      array('price_5000_10000', 'numerical'),
      // 0.9 - Скидка на товары 5000-9999.99 юаней
      array('price_500_1000', 'numerical'),
      // 0.94 - Скидка на товары 500-999.99 юаней
      array('price_60_90', 'numerical'),
      // 0.98 - Скидка на товары 60-89.99 юаней
      array('price_90_150', 'numerical'),
      // 0.97 - Скидка на товары 90-149.99 юаней
      array('price_main_k', 'numerical'),
      // 1.7 - Основной коэффициент в цене, например 1.5 это наценка в 50%
      array('rate_byr', 'numerical'),
      // 0.00364 - Курс белорусского рубля к основной валюте
      array('rate_cny', 'numerical'),
      // 5.157672 - Курс юаня к основной валюте
      array('rate_eur', 'numerical'),
      // 41.15 - Курс евро к основной валюте
      array('rate_kzt', 'numerical'),
      // 0.22 - Курс тенге к основной валюте
      array('rate_rur', 'numerical'),
      // 1 - Курс рубля к основной валюте
      array('rate_uah', 'numerical'),
      // 3.85 - Курс гривны к основной валюте
      array('rate_usd', 'numerical'),
      // 31.72 - Курс доллара к основной валюте
      array('skidka_0_10', 'numerical'),
      // 1 - Скидка на количество 0-10
      array('skidka_10000_1000000000', 'numerical'),
      // 0.99 - Скидка на количество более 10000
      array('skidka_1000_2000', 'numerical'),
      // 0.85 - Скидка на количество 1000-2000
      array('skidka_10_60', 'numerical'),
      // 0.95 - Скидка на количество 10-60
      array('skidka_150_200', 'numerical'),
      // 0.95 - Скидка на количество 150-200
      array('skidka_2000_5000', 'numerical'),
      // 0.9 - Скидка на количество 2000-5000
      array('skidka_200_500', 'numerical'),
      // 0.9 - Скидка на количество 200-500
      array('skidka_5000_10000', 'numerical'),
      // 0.9 - Скидка на количество 5000-10000
      array('skidka_500_1000', 'numerical'),
      // 0.9 - Скидка на количество 500-1000
      array('skidka_60_90', 'numerical'),
      // 0.95 - Скидка на количество 60-90
      array('skidka_90_150', 'numerical'),
      // 0.95 - Скидка на количество 90-150
      array('taobao_EnabelDiscounts', 'numerical'),
      // 1 - Использовать ли скидки от таобао?
    );
  }

  public function attributeLabels() {
    return array(
      'billing_use_operator_account' => Yii::t('admin', 'Отображать ли и использовать ли счет оператора для начисления премий и бонусов'),
      'checkout_delivery_needed' => Yii::t('admin', 'Нужно ли при оформлении заказа считать и вводить службу доставки'),
      'checkout_delivery_sum_needed' => Yii::t('admin', 'Нужно ли при оформлении заказа считать службу доставки'),
      'checkout_order_reconfirmation_needed' => Yii::t('admin', 'Подтверждение заказа покупателем после его расчета менеджером'),
      'checkout_payment_needed' => Yii::t('admin', 'Нужно ли при оформлении заказа его оплачивать'),
      'checkout_reorder_needed' => Yii::t('admin', 'Использовать ли дозаказ?'),
      'checkout_weight_needed' => Yii::t('admin', 'Нужно ли покупателю при заказе вводить вес'),
      'price_0_10' => Yii::t('admin', 'Скидка на товары 0-9.99 юаней, например, 0.9 - это скидка в 10%'),
      'price_10000_1000000000' => Yii::t('admin', 'Скидка на товары дороже 10000 юаней'),
      'price_1000_2000' => Yii::t('admin', 'Скидка на товары 1000-1999.99 юаней'),
      'price_10_60' => Yii::t('admin', 'Скидка на товары 10-59.99 юаней'),
      'price_150_200' => Yii::t('admin', 'Скидка на товары 150-199.99 юаней'),
      'price_2000_5000' => Yii::t('admin', 'Скидка на товары 2000-4999.99 юаней'),
      'price_200_500' => Yii::t('admin', 'Скидка на товары 200 – 499.99 юаней'),
      'price_5000_10000' => Yii::t('admin', 'Скидка на товары 5000-9999.99 юаней'),
      'price_500_1000' => Yii::t('admin', 'Скидка на товары 500-999.99 юаней'),
      'price_60_90' => Yii::t('admin', 'Скидка на товары 60-89.99 юаней'),
      'price_90_150' => Yii::t('admin', 'Скидка на товары 90-149.99 юаней'),
      'price_main_k' => Yii::t('admin', 'Основной коэффициент в цене, например 1.5 это наценка в 50%'),
      'rate_byr' => Yii::t('admin', 'Курс белорусского рубля к основной валюте'),
      'rate_cny' => Yii::t('admin', 'Курс юаня к основной валюте'),
      'rate_eur' => Yii::t('admin', 'Курс евро к основной валюте'),
      'rate_kzt' => Yii::t('admin', 'Курс тенге к основной валюте'),
      'rate_rur' => Yii::t('admin', 'Курс рубля к основной валюте'),
      'rate_uah' => Yii::t('admin', 'Курс гривны к основной валюте'),
      'rate_usd' => Yii::t('admin', 'Курс доллара к основной валюте'),
      'seo_sitemap_static' => Yii::t('admin', 'Статическая часть sitemap.xml'),
      'site_currency' => Yii::t('admin', 'Главная валюта сайта, валюта админки и кабинета'),
      'site_currency_block' => Yii::t('admin', 'Валюты, которые доступны пользователю на сайте (маленькими буквами, через запятую, без пробелов, в одну строку)'),
      'site_domain' => Yii::t('admin', 'Домен сайта'),
      'site_name' => Yii::t('admin', 'Название сайта'),
      'skidka_0_10' => Yii::t('admin', 'Скидка на количество 0-10'),
      'skidka_10000_1000000000' => Yii::t('admin', 'Скидка на количество более 10000'),
      'skidka_1000_2000' => Yii::t('admin', 'Скидка на количество 1000-2000'),
      'skidka_10_60' => Yii::t('admin', 'Скидка на количество 10-60'),
      'skidka_150_200' => Yii::t('admin', 'Скидка на количество 150-200'),
      'skidka_2000_5000' => Yii::t('admin', 'Скидка на количество 2000-5000'),
      'skidka_200_500' => Yii::t('admin', 'Скидка на количество 200-500'),
      'skidka_5000_10000' => Yii::t('admin', 'Скидка на количество 5000-10000'),
      'skidka_500_1000' => Yii::t('admin', 'Скидка на количество 500-1000'),
      'skidka_60_90' => Yii::t('admin', 'Скидка на количество 60-90'),
      'skidka_90_150' => Yii::t('admin', 'Скидка на количество 90-150'),
      'taobao_EnabelDiscounts' => Yii::t('admin', 'Использовать ли скидки от таобао?'),
    );
  }

}