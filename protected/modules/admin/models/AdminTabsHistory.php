<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AdminTabsHistory.php">
* </description>
**********************************************************************************************************************/?>
<?php

/**
 * This is the model class for table "admin_tabs_history".
 *
 * The followings are the available columns in table 'admin_tabs_history':
 * @property integer $id
 * @property string $href
 * @property string $name
 * @property string $title
 * @property integer $uid
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Users $u
 */
class AdminTabsHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_tabs_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('href, name, uid, date', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('href, title', 'length', 'max'=>1024),
			array('name', 'length', 'max'=>512),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, href, name, title, uid, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('admin','id записи истории'),
			'href' => Yii::t('admin','Ссылка'),
			'name' => Yii::t('admin','История вкладок'),
			'title' => Yii::t('admin','title ссылки'),
			'uid' => Yii::t('admin','Uid'),
			'date' => Yii::t('admin','Дата'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('href',$this->href,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminTabsHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

  public static function getHistory($uid = FALSE) {
    if ($uid === FALSE) {
      $_uid = Yii::app()->user->id;
    }
    else {
      $_uid = $uid;
    }
    $criteria = new CDbCriteria;
    $criteria->select = 't.*';
    $criteria->condition = 'uid=:uid';
    $criteria->group = 'href';
    $criteria->limit = 20;
    $criteria->params = array(':uid' => $_uid);
    $criteria->order = 'max(`date`) DESC';
    $history = new CActiveDataProvider(self::model(), array(
      'criteria' => $criteria,
      'sort' => FALSE,
      'pagination' => FALSE,
//      'pagination' => array(
//        'pageSize' => 20,
//      ),
    ));
    return $history;
  }

}
