<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersForm.php">
* </description>
**********************************************************************************************************************/?>
<?php

class OrdersForm extends CFormModel {

   public $weight;
   public $calc_price;
   public $oid;    
   
   public $sid;
   
   public $sum;
   public $desc;

   function rules() {
        return array(            
            array('weight', 'required', 'on'=>'weight'),
            array('weight', 'numerical','min'=>0,'on'=>'weight'),
            array('oid', 'required', 'on'=>'oid'),
            array('oid','numerical','min'=>0,'on'=>'oid'),
            array('sid', 'required', 'on'=>'sid'),            
            array('sum,desc','required','on'=>'balance'),
            array('sum','numerical'),
        );
    }
    
}