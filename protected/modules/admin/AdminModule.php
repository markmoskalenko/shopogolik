<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="AdminModule.php">
* </description>
**********************************************************************************************************************/?>
<?php

class AdminModule extends CWebModule
{   public $defaultController = 'Main';
	public function init()
	{
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));

      //register translation messages from module dbadmin
      //so no need do add to config/main.php
/*      Yii::app()->setComponents(
        array('messages' => array(
          'class'=>'CPhpMessageSource',
          'basePath'=>'protected/modules/admin/messages',
        )));
*/
      Yii::app()->setComponents(array(
        'errorHandler'=>array(
          'errorAction'=>'/admin/main/error',
        ),

      ));



      Yii::app()->theme = 'admin';
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
          // you may place customized code here
			return true;
		}
		else
			return false;
	}
}
