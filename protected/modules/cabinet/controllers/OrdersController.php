<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="OrdersController.php">
* </description>
**********************************************************************************************************************/?>
<?php

class OrdersController extends CustomFrontController {
    public $layout = '//layouts/my_account';

  public function filters() {
    return array_merge (
      array (
        'Rights', // perform access control for CRUD operations
      ),
      parent::filters()
    );
  }

  function actionDelete($oid) {
    $order = Order::model()->findByPk($oid);
    if ($order) {
      $uid = Yii::app()->user->id;
      if (OrdersStatuses::isAllowedStatusForOrder('CANCELED_BY_CUSTOMER', $oid, $uid, NULL)) {
        $order->status = 'CANCELED_BY_CUSTOMER';
        $order->save();
        Yii::app()->user->setFlash('cabinet', Yii::t('main', 'Заказ был отменен'));
      }
    }
    $this->redirect('/cabinet');
    //$this->redirect(Yii::app()->request->urlReferrer);
  }

  function actionIndex($type = 'IN_PROCESS') {
    $status = OrdersStatuses::model()->find('value=:value', array(':value' => $type));
    $this->pageTitle = Yii::t('main', $status->name);
    $this->body_class = 'cabinet';
    $this->breadcrumbs = array(
      Yii::t('main', 'Личный кабинет') => '/cabinet',
      Yii::t('main', 'Заказы') => '/cabinet/orders',
      $this->pageTitle
    );
    $uid = Yii::app()->user->id;
    $orders = Order::getOrdersList($type, $uid);
    $statuses = OrdersStatuses::getAllStatusesListAndOrderCount($uid);
          $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.orders.list', array('orders' => $orders, 'statuses' => $statuses, 'type' => $type));
  }

  function GetOrderPrice($iid, $count, $price = FALSE) {
    $item = new Item($iid, TRUE);
    if ($price) {
      $resUserPrice = Formulas::getUserPrice(
        array(
          'price'=>$price,
          'count'=>$count,
          'deliveryFee'=>$item->top_item->express_fee,
          'postageId'=>$item->taobao_item->postage_id,
          'sellerNick'=>$item->taobao_item->nick,
        ));
      $price_res=$resUserPrice->price;
    }
    else {
      $resUserPrice = Formulas::getUserPrice(
        array(
          'price'=>$item->top_item->price,
          'count'=>$count,
          'deliveryFee'=>$item->top_item->express_fee,
          'postageId'=>$item->taobao_item->postage_id,
          'sellerNick'=>$item->taobao_item->nick,
        ));
      $price_res=$resUserPrice->price;
    }
    return $price_res;
  }


  public function actionSave() {
// TODO Alexy Тут чего-то с валютой беда - надо разбираться.
    $order = Order::model()->findByPk($_POST['order_id']);

    $order->comment = NULL;
    $order->comment = htmlspecialchars(trim($_POST['comment']));

    $total_summ = 0;
    $fact_summ_payed = 0;
    $parcels_change = array();

    foreach ($_POST['description'] as $key => $val) {
      $orderItem = OrdersItems::model()->findByPk($key);
      if ($orderItem) {
//                $item = CJSON::decode($orderItem->data);
        //$price_item = floatval($item['priceUserFinal']/$item['num']);
        $orderItem['price_user_final'] = $this->GetOrderPrice($orderItem['iid'], $_POST['count'][$key]);
        $orderItem['desc'] = $val;

        $fact_summ = $this->GetOrderPrice($orderItem['iid'], $orderItem['num']);
        $fact_summ_payed += $orderItem['price_user_final'] - $fact_summ;

        $total_summ += $orderItem['price_user_final'];

        $orderItem['sum'] = Formulas::priceWrapper($orderItem['price_user_final'], DSConfig::getVal('site_currency'));
        $orderItem['num'] = $_POST['count'][$key];

//                $orderItem->data = CJSON::encode($item);
        $orderItem->price = $orderItem['price_user_final'];
        $parcels_change[] = $orderItem;
        //$orderItem->save();
      }
    }
    if ($order->sum <> $total_summ) {
      if ($fact_summ_payed < Users::getBalance(Yii::app()->user->id)) {
        $difference = $order->sum - $total_summ;

        foreach ($parcels_change as $orderItem) {
          $orderItem->update();
        }

        $order->sum = $total_summ;
        $order->update();

        $payment = new Payment;
        $payment->sum = $difference;
        $payment->description = $difference < 0 ? 'Доплата по заказу №' . $order->id : 'Возвращение средств с заказа №' . $order->id;
        $payment->date = time();
        $payment->uid = Yii::app()->user->id;
        $payment->status = 2;

        $payment->save();


        Yii::app()->user->setFlash('cabinet', Yii::t('main', 'Заказ был обновлен'));
        $this->redirect(Yii::app()->request->urlReferrer);

      }
      else {
        Yii::app()->user->setFlash('cabinet', Yii::t('main', 'У вас на счету недостаточно средств'));
        $this->redirect(Yii::app()->request->urlReferrer);
      }
    }
    else {
      $order->update();
      foreach ($parcels_change as $orderItem) {
        $orderItem->update();
      }
      Yii::app()->user->setFlash('cabinet', Yii::t('main', 'Заказ был обновлен'));
      $this->redirect(Yii::app()->request->urlReferrer);
    }
  }


  function actionView($id) {
    $uid = Yii::app()->user->id;
    $order = Order::getOrder($id, $uid, NULL);
    if ($order == FALSE) {
      throw new CHttpException (404);
    }
    $this->body_class = 'cabinet';
    $this->pageTitle = 'Заказ №' . $order->uid . '-' . $order->id;
    $this->breadcrumbs = array(
      Yii::t('main', 'Личный кабинет') => '/cabinet',
      Yii::t('main', 'Заказы') => '/cabinet/orders',
      $this->pageTitle
    );

    $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.orders.view', array('order' => $order));
  }

}