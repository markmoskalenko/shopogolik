<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="ProfileController.php">
 * </description>
 **********************************************************************************************************************/?>
<?php

class ProfileController extends CustomFrontController {
    public function filters() {
        return array_merge (
            array (
                'Rights', // perform access control for CRUD operations
            ),
            parent::filters()
        );
    }
    public $layout = '//layouts/my_account';

       public function actionIndex() {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Личные данные');
        $this->breadcrumbs = array(
            Yii::t('main', 'Личный кабинет') => '/cabinet',
            $this->pageTitle
        );

        $model = new CabinetForm('profile');
        $form = new CForm('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile._profile', $model);

        $user = Users::model()->findByPk(Yii::app()->user->id);

        $data = array();
        if ($user) {
            foreach ($user as $k => $v) {
                if ($k == 'birthday') {
                    $birthday = $v ? explode('-', $v) : NULL;
                    if (is_array($birthday) && count($birthday) == 3) {
                        $data['y_birth'] = $birthday[0];
                        $data['m_birth'] = $birthday[1];
                        $data['d_birth'] = $birthday[2];
                    }
                }
                if ($k == 'password') {
                    $data[$k] = '';
                }
                elseif ($k == 'alias' && empty($v)) {
                    $data['alias'] = $user['firstname'] . ' ' . $user['lastname'];
                }
                else {
                    $data[$k] = $v;
                }
            }
        }
        $model->attributes = $data;
        if (isset($_POST['CabinetForm'])) {
            $model->attributes = $_POST['CabinetForm'];
            if ($model->validate()) {
                if (md5($model->password) == $user->password) {
                    $user->firstname = $model->firstname;
                    $user->patroname = $model->patroname;
                    $user->lastname = $model->lastname;
                    $user->skype = $model->skype;
                    $user->vk = $model->vk;
                    if ($model->promo_code != '') {
                        $user->default_manager = Users::getUidByPromo($model->promo_code);
                    }
                    if (!$user->default_manager) {
                        $user->default_manager=users::getFirstSuperAdminId();
                    }
                    $user->alias = $model->alias ? $model->alias : $model->firstname . ' ' . $model->lastname;
                    $user->birthday = $model->y_birth . '-' . $model->m_birth . '-' . $model->d_birth;
                    //$user->sex = $model->sex;
                    $user->save();
                    Yii::app()->user->setFlash('index',
                        Yii::t('main', 'Данные успешно обновлены')
                    );
                    $this->redirect('/cabinet/profile/index');
                }
                else {
                    Yii::app()->user->setFlash('index',
                        Yii::t('main', 'Пароль не верен')
                    );
                }
            }
        }
        $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.index', array('form' => $form));
    }

    function actionEmail() {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Изменить EMail');
        $this->breadcrumbs = array(
            Yii::t('main', 'Личный кабинет') => '/cabinet',
            $this->pageTitle
        );

        $model = new CabinetForm('email');
        $form = new CForm('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile._email', $model);

        $user = Users::model()->findByPk(Yii::app()->user->id);

        //$model->email = $user->email;

        if (isset($_POST['CabinetForm'])) {
            $model->attributes = $_POST['CabinetForm'];

            if ($model->validate()) {
                if (md5($model->password) == $user->password) {

                    $email = Yii::app()->db->createCommand()
                        ->select()
                        ->from('users')
                        ->where('uid!=:uid AND email=:email', array(':uid' => Yii::app()->user->id, ':email' => $model->email))
                        ->queryScalar();
                    if (!$email) {

                        $user->email = $model->email;
                        $user->save();

                        $identity = new UserIdentity($user->email, $user->password, TRUE);
                        $identity->setState('email', $model->email);

                        Yii::app()->user->setFlash('email',
                            Yii::t('main', 'EMail успешно изменен')
                        );
                    }
                    else {
                        Yii::app()->user->setFlash('email',
                            Yii::t('main', 'Данный EMail уже зарегистрирован в базе')
                        );
                    }
                }
                else {
                    Yii::app()->user->setFlash('email',
                        Yii::t('main', 'Вы ввели не верный пароль')
                    );
                }
                $this->redirect('/cabinet/profile/email');
            }
        }

        $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.email', array('form' => $form, 'user' => $user));
    }

    function actionPassword() {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Изменить пароль');
        $this->breadcrumbs = array(
            Yii::t('main', 'Личный кабинет') => '/cabinet',
            $this->pageTitle
        );

        $model = new CabinetForm('password');
        $form = new CForm('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile._password', $model);

        if (isset($_POST['CabinetForm'])) {
            $model->attributes = $_POST['CabinetForm'];

            if ($model->validate()) {
                $user = Users::model()->findByPk(Yii::app()->user->id);
                if (md5($model->password) == $user->password) {
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $user->password = md5($model->new_password);
                    $user->save();
                    Yii::app()->user->setFlash('password',
                        Yii::t('main', 'Пароль успешно изменен')
                    );
                }
                else {
                    Yii::app()->user->setFlash('password',
                        Yii::t('main', 'Вы ввели не верный пароль')
                    );
                }
                $this->redirect('/cabinet/profile/password');
            }
        }

        $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.password', array('form' => $form));
    }

    public function actionDeleteaddress($id) {
        $model = Addresses::model()->findByPk($id);
        $model->enabled=0;
        if ($model->update()) {
            Yii::app()->user->setFlash('address',
                Yii::t('main', 'Адрес успешно удален')
            );
        }

        $this->redirect('/cabinet/profile/address');

    }

    public function actionUpdateaddress($id) {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Адрес');
        $this->breadcrumbs = array(
            Yii::t('main', 'Личный кабинет') => '/cabinet',
            $this->pageTitle
        );

        $model = new CabinetForm('address');
        $form = new CForm('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.addressForm', $model);
        $address = Addresses::model()->findByPk($id);

        $data = array();
        if ($address) {
            foreach ($address as $k => $v) {
                $data[$k] = $v;
            }
        }

        $model->attributes = $data;

        if (isset($_POST['CabinetForm'])) {
            $model->attributes = $_POST['CabinetForm'];

            if ($model->validate()) {
                $address->country = $model->country;
                $address->city = $model->city;
                $address->index = $model->index;
                $address->phone = $model->phone;
                $address->address = $model->address;
                $address->firstname = $model->firstname;
                $address->patroname = $model->patroname;
                $address->lastname = $model->lastname;
                $address->region = $model->region;
                $address->save();
                Yii::app()->user->setFlash('address',
                    Yii::t('main', 'Адрес успешно обновлен')
                );
                $this->redirect('/cabinet/profile/address');
            }

        }

        $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.create_address', array('form' => $form));

    }

    public function actionCreateaddress() {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Адрес');
        $this->breadcrumbs = array(
            Yii::t('main', 'Личный кабинет') => '/cabinet',
            $this->pageTitle
        );

        $model = new CabinetForm('address');
        $form = new CForm('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.addressForm', $model);

        $address = new Addresses;

        if (isset($_POST['CabinetForm'])) {
            $model->attributes = $_POST['CabinetForm'];

            if ($model->validate()) {
                $address->uid = Yii::app()->user->id;
                $address->country = $model->country;
                $address->city = $model->city;
                $address->index = $model->index;
                $address->phone = $model->phone;
                $address->address = $model->address;
                $address->firstname = $model->firstname;
                $address->patroname = $model->patroname;
                $address->lastname = $model->lastname;
                $address->region = $model->region;
                Yii::app()->user->setFlash('address',
                    Yii::t('main', 'Новый адрес успешно создан')
                );

                $address->save();
                $this->redirect('/cabinet/profile/address');
            }

        }

        $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.create_address', array('form' => $form));

    }

    function actionAddress() {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Список адресов');
        $this->breadcrumbs = array(
            Yii::t('main', 'Личный кабинет') => '/cabinet',
            $this->pageTitle
        );

        $addresses = Yii::app()->db->createCommand()
            ->select('*')
            ->from('addresses')
            ->where('uid=:uid and enabled=1', array(':uid' => Yii::app()->user->id))
            //->order('date DESC')
            ->queryAll();

        $model = new CabinetForm('address');
        $form = new CForm('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.addressForm', $model);

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $data = array();
        if ($user) {
            foreach ($user as $k => $v) {
                $data[$k] = $v;
            }
        }
        $model->attributes = $data;
        if (isset($_POST['CabinetForm'])) {
            $model->attributes = $_POST['CabinetForm'];

//                echo $model->phone;
//                if($model->validate())
//                {
//                    $user->country = $model->country;
//                    $user->city = $model->city;
//                    $user->index = $model->index;
//                    $user->phone = $model->phone;
//                    $user->address = $model->address;
//                    $user->save();                 
//                    $this->redirect('/cabinet/profile/address');
//                }                
        }

        $this->render('webroot.themes.'.DSConfig::getVal('site_front_theme').'.views.cabinet.profile.address', array(
            'countries' => Deliveries::getCountries(),
            'form'      => $form,
            'addresses' => $addresses
        ));
    }

}