<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="BalanceController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class BalanceController extends CustomFrontController
{
    public $layout = '//layouts/my_account';
/*
      1 Зачисление или возврат средств
      2 Снятие средств
      3 Ожидание зачисления средств
      4 Отмена ожидания зачисления средств
      5 Отправка внутреннего перевода средств
      6 Получение внутреннего перевода средств
      7 Зачисление бонуса или прибыли
      8 Вывод средств из системы
*/
    public function filters()
    {
        return array_merge(
          array(
            'Rights', // perform access control for CRUD operations
          ),
          parent::filters()
        );
    }

    function actionIndex($payment_email = null)
    {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Выписка по счету');
        $this->breadcrumbs = array(
          Yii::t('main', 'Личный кабинет') => '/cabinet',
          $this->pageTitle
        );
        $uid = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->select = "t.*,FROM_UNIXTIME(date,'%d.%m.%Y %h:%i') as text_date,
     case when t.status=1 then '" . Yii::t('main', 'Зачисление или возврат средств') . "'
      when t.status=2 then '" . Yii::t('main', 'Снятие средств') . "'
      when t.status=3 then '" . Yii::t('main', 'Ожидание зачисления средств') . "'
      when t.status=4 then '" . Yii::t('main', 'Отмена ожидания зачисления средств') . "'
      when t.status=5 then '" . Yii::t('main', 'Отправка внутреннего перевода средств') . "'
      when t.status=6 then '" . Yii::t('main', 'Получение внутреннего перевода средств') . "'
      when t.status=7 then '" . Yii::t('main', 'Зачисление бонуса или прибыли') . "'
      when t.status=8 then '" . Yii::t('main', 'Вывод средств из системы') . "'
      else '" . Yii::t('main', 'Не определено (ошибка?)') . "' end as text_status";
        $criteria->condition = 'uid=:uid';
        $criteria->params = array(':uid' => $uid);
        $criteria->order = 'date DESC';

        if (isset($_POST['yt0'])) {
            $date_from = new DateTime($_POST['date_from']);
            $date_to = new DateTime($_POST['date_to']);
            $date_to_str = $date_to->format('Y-m-d');
            $date_to->modify('+ 1 day');
            $criteria->condition = 'uid=:uid AND `date` >=' . $date_from->format(
                'U'
              ) . ' AND status IN(1,2,5,6,7,8) AND `date` <= ' . $date_to->format('U');
            //$string = ' AND date >='.$date_from->format('U').' AND datе <= '.$date_to->format('U');
        } else {

            //вычисляем дату начала, это сегодняшняя дата минус месяц
            $date_from = new DateTime(date('Y-m-d'));
            $date_from->modify("-1 month");

            //вычисляем дату конца
            $date_to = new DateTime(date('Y-m-d'));
            $date_to_str = date('Y-m-d');
            $date_to->modify('+ 1 day');
            //$date_to->modify('- 1 day');

            //$date_to = new DateTime($_POST['date_to']);
//     $criteria->condition='uid=:uid AND `date` >=' . $date_from->format('U') . ' AND status IN(1,2,5,6) AND `date` <= ' . $date_to->format('U');
        }

        $payments = new CActiveDataProvider(Payment::model(), array(
          'criteria'   => $criteria,
          'pagination' => array(
            'pageSize' => 50,
          ),
        ));

        $render = array(
          'payments'  => $payments,
          'date_from' => $date_from->format('Y-m-d'),
          'date_to'   => $date_to_str
        );
        $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.balance', $render);
    }

    function actionTransfer()
    {

        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Перевод денег на другой счет');
        $this->breadcrumbs = array(
          Yii::t('main', 'Личный кабинет') => '/cabinet',
          $this->pageTitle
        );

        $model = new AccountForm('transfer');
        $form = new CForm(array(
          'elements' => array(
            'id_account' => array(
              'type'      => 'text',
              'maxlength' => 128,
            ),
            'summ'       => array(
              'type'      => 'text',
              'maxlength' => 128,
            ),
            'password'   => array(
              'type'      => 'password',
              'maxlength' => 128,
            ),
          ),
          'buttons'  => array(
            'submit' => array(
              'type'  => 'submit',
              'class' => 'blue-btn bigger',
              'style' => 'width:130px;',
              'label' => Yii::t('main', 'Перевести деньги'),
            ),
          ),

        ), $model);

        if (isset($_POST['AccountForm'])) {
            $model->attributes = $_POST['AccountForm'];
            if ($model->validate()) {
                $flag_error = false;
                $identity = new UserIdentity(Yii::app()->user->email, $_POST['AccountForm']['password']);
                if (!$identity->authenticate()) {
                    $flag_error = Yii::t('main', 'Вы ошиблись при вводе пароля - повторите');
                } else {
                    $destUid = Yii::app()->user->getIdByPersonalAccount($_POST['AccountForm']['id_account']);
                    $destUser = Users::model()->findByPk($destUid);

                    if (is_null($destUid) || is_null($destUser)) {
                        $flag_error = Yii::t('main', 'Не существует пользователя с таким персональным счетом');
                    }
                    if ($destUid == Yii::app()->user->id) {
                        $flag_error = Yii::t('main', 'Нельзя делать перевод на свой собственный счет');
                    }
                    if (!$flag_error) {
                        $available_funds = Users::getBalance(Yii::app()->user->id);
                        $post_summ = str_replace(',', '.', $_POST['AccountForm']['summ']);
                        if ($available_funds < (float) $_POST['AccountForm']['summ']) {
                            $flag_error = Yii::t(
                              'main',
                              'Ваш баланс не должен быть меньше суммы, которую Вы хотите перевести'
                            );
                        }
                    }
                }
                if ($flag_error) {
                    Yii::app()->user->setFlash(
                      'transfer',
                      Yii::t('main', "Ошибка") . ': ' . $flag_error
                    );
                } else {
                    $payment_from = new Payment;
                    $payment_from->sum = floatval(-1 * $post_summ);
                    $payment_from->check_summ = $payment_from->sum; // = -1 * Formulas::convertCurrency($post_summ, DSConfig::getVal('site_currency'), 'rur');
                    $payment_from->date = time();
                    $payment_from->description = Yii::t(
                        'main',
                        'Перевод денег на счет'
                      ) . ' №' . $_POST['AccountForm']['id_account'];
                    $payment_from->uid = Yii::app()->user->id;
                    $payment_from->status = 5;
                    $payment_from->save();

                    $payment_to = new Payment;
                    $payment_to->sum = floatval($post_summ);
                    $payment_to->check_summ = $payment_to->sum; //Formulas::convertCurrency($post_summ, DSConfig::getVal('site_currency'), 'rur');
                    $payment_to->date = time();
                    $payment_to->description = Yii::t('main', 'Получение денег со счета') . ' №' . Yii::app(
                      )->user->getPersonalAccount();
                    $payment_to->uid = $destUid;
                    $payment_to->status = 6;
                    $payment_to->save();

                    Yii::app()->user->setFlash(
                      'transfer',
                      Yii::t('main', 'Перевод успешно осуществлен')
                    );

                    $this->redirect('/cabinet/balance/transfer');
                }
            }
        }

        $this->render(
          'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.transfer',
          array('form' => $form)
        );

    }

    function actionStatement()
    {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Информация о счете');
        $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.statement', array());
    }

    function actionPayment($sum = false)
    {
        $this->body_class = 'cabinet';
        $this->pageTitle = Yii::t('main', 'Пополнить счет');
        $this->breadcrumbs = array(
          Yii::t('main', 'Личный кабинет') => '/cabinet',
          $this->pageTitle
        );
        $model = new CabinetForm('payment');
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $model->attributes = $user->attributes;
        $model->publicAccount = Yii::app()->user->getPersonalAccount();
//    $model->phone = (isset($user->phone)) ? $user->phone : '';
        if (isset($_POST['CabinetForm'])) {
            $model->attributes = $_POST['CabinetForm'];
            $model->sum = (float) strtr($model->sum, array(',' => '.'));
            if (isset($_POST['preference']) && $model->validate()) {
                $model_array = array();
                foreach ($model as $i => $v) {
                    $model_array[$i] = $v;
                }
                switch ($_POST['preference']) {
                    case 'cash':
                        if ($this->payCash($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'robokassa':
                        if ($this->payRobokassa($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'liqpay':
                        if ($this->payLiqpay($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'order':
                        if ($this->payOrder($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'order_alt':
                        if ($this->payOrder($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'webmoney':
                        if ($this->payWebmoney($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'qiwi':
                        if ($this->payQiwi($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'paypal':
                        if ($this->payPaypal($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
                    case 'yandexmoney':
                        if ($this->payYandexmoney($model_array, $_POST['preference'])) {
                            return;
                        }
                        break;
		                case 'walletone':
	                     if ($this->payWalletone($model_array, $_POST['preference'])) {
	                         return;
	                     }
	                     break;
		                case 'zpayment':
	                     if ($this->payZpayment($model_array, $_POST['preference'])) {
	                         return;
	                     }
                     break;
                    default:
                        Yii::app()->user->setFlash(
                          'payment',
                          Yii::t('main', 'Ошибка: Выбранная Вами платёжная система не обслуживается!')
                        );
                }
            } else {
                Yii::app()->user->setFlash(
                  'payment',
                  Yii::t(
                    'main',
                    'Ошибка: Вы не выбрали платёжную систему или неверно ввели сумму и номер мобильного телефона.'
                  )
                );
            }
        }
// default rendering
        if ($sum) {
            $model->sum = $sum;
        } else {
            $model->sum = 100;
        }
        // рендерим представление
        $paySystems = PaySystems::model()->findAll('enabled=1 order by id');
        $this->render(
          'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.payment',
          array('model' => $model, 'check' => false, 'paySystems' => $paySystems)
        );
        //---------------------
    }

    public function actionOrder($oid)
    {
        $order = Order::model()->findByPk($oid);
        if (!$order) {
            Yii::app()->user->setFlash('user', Yii::t('main', 'Такого заказа не существует!'));
            $this->redirect('/cabinet');
        }
        $balance = Users::getBalance(Yii::app()->user->id);
        $orderSum = ($order->manual_sum) ? $order->manual_sum : $order->sum;
        $orderDelivery = ($order->manual_delivery) ? $order->manual_delivery : $order->delivery;
        $orderPayed = OrdersPayments::getPaymentsSumForOrder($order->id);
        $paymentSum = round($orderDelivery + $orderSum - $orderPayed, 2);

        if ($paymentSum > $balance) {
            Yii::app()->user->setFlash('user', Yii::t('main', 'На вашем счету недостаточно средств'));
        } else {

            if (($orderPayed <= 0)) {
                $paymentDescription = Yii::t('main', 'Оплата заказа') . ' №' . $oid;
            } else {
                $paymentDescription = Yii::t('main', 'Доплата по заказу') . ' № ' . $oid;
            }

            if (OrdersPayments::payForOrder($oid, $paymentSum, $paymentDescription)) {
                $notices = UserNotice::model()->findAll('uid=:uid', array(':uid' => Yii::app()->user->id));
                foreach ($notices as $note) {
                    $note->delete();
                }
                /*          if (($checkout_order_reconfirmation_needed)) {
                            $order->status = 1;
                          }
                          else {
                            $order->status = $order->status == 5 ? 1 : 6;
                          }
                          $order->save();
                */
                Yii::app()->user->setFlash('user', Yii::t('main', 'Заказ оплачен!'));
            }
        }
        if (isset($GLOBALS['_SERVER']['HTTP_REFERER'])) {
            $this->redirect($GLOBALS['_SERVER']['HTTP_REFERER']);
        } else {
            $this->redirect('/cabinet');
        }
    }
//=============================================================
    private function logActionCall($sender) {
        if ($sender) {
            $log=new PaySystemsLog();
            $log->date = date("Y-m-d H:i:s", time());
            $log->from_ip = $_SERVER['REMOTE_ADDR'];
            $log->action=$this->action->id;
            $log->sender = $sender;
            $log->data = serialize($_POST);
            $log->save();
        }
    }

/* ***************************************************************************************
 *                                                                   PAY FORMS FUNCTIONS *
 * **************************************************************************************/
    private function  payCash($data, $type)
    {
        $data['sum'] = number_format((float) $data['sum'], 2, '.', '');
        $this->render(
          'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.payment_offline',
          array('data' => $data, 'type' => $type)
        );
        return true;
    }

    private function payRobokassa($data, $type)
    {
        /*    Передаваемые параметры:
        <html>
        <form action="{#url}" method="POST">
        <input type="hidden" name="MrchLogin" value="{#MrchLogin}">
        <input type="hidden" name="OutSum" value="{sum}">
        <input type="hidden" name="InvId" value="{InvId}">
        <input type="hidden" name="Desc" value="{Desc}">
        <input type="hidden" name="SignatureValue" value="{SignatureValue}">
        <input type="hidden" name="Shp_item" value="{Shp_item}">
        <input type="hidden" name="IncCurrLabel" value="{IncCurrLabel}">
        <input type="hidden" name="Culture" value="{Culture}">
        <input type="submit" value="{#SubmitValue}">
        </form>
        </html>
        */
// Оплата заданной суммы с выбором валюты на сайте ROBOKASSA
// Payment of the set sum with a choice of currency on site ROBOKASSA
// регистрационная информация (логин, пароль #1)
// registration info (login, password #1)

//==========================
        $paySystem = (PaySystems::model()->find('int_name=:type', array('type' => $type)));
        $parameters = $paySystem['parameters'];
        $params = (array) simplexml_load_string($parameters, null, LIBXML_NOCDATA);
        $intDefCurrency = (string) $params['intDefCurrency'];
        $siteCurrency = DSConfig::getVal('site_currency');
        $intSumm = number_format((float) $data['sum'], 2, '.', '');
        $OutSum = number_format(
          (float) Formulas::convertCurrency($data['sum'], $siteCurrency, $intDefCurrency),
          2,
          '.',
          ''
        );
        $data['sum'] = $OutSum;
        $data['intSumm'] = $intSumm;
        $payment = new Payment;
        $payment->status = 3;
        $payment->sum = $intSumm;
        $payment->check_summ = $OutSum; //round($OutSum * DSConfig::model()->findByPk('rate_usd')->value, 2);
        $payment->date = time();
        $payment->description = Yii::t('main', 'Заявка на пополнение RoboKassa');
        $payment->uid = Yii::app()->user->id;
        $payment->save();
// ID заказа
        $data['InvId'] = $payment->id;
// ID товара, в нашем случае ID пользователя
        $data['Shp_item'] = Yii::app()->user->id;

        $MrchLogin = (string) $params['MrchLogin'];
        $password1 = (string) $params['Password1'];
// предлагаемая валюта платежа
// default payment e-currency
        $data['IncCurrLabel'] = '';
// язык
// language
        $data['Culture'] = Utils::TransLang();

        $data['site_currency'] = $siteCurrency;

// формирование подписи
// generate signature
        $data['SignatureValue'] = md5(
          $MrchLogin . ':' . $OutSum . ':' . $data['InvId'] . ':' . $password1 . ':' . 'Shp_item=' . $data['Shp_item']
        );
        $this->render(
          'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.payment_online',
          array('data' => $data, 'type' => $type)
        );
        return true;
    }

    private function payOrder($data, $type)
    {
        $this->render(
          'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.payment_offline',
          array('data' => $data, 'type' => $type)
        );
        return true;
    }

    private function payWebmoney($data, $type)
    {
        return false;
    }

	private function payQiwi($data, $type)
 {
  /*****************
   *  Идентификатор пользователя: 17989754
   * Пароль: YYJnYwVFboTgxrscJI02
   */

  // Оплата заданной суммы на сайте Visa Qiwi Wallet
  // Payment of the set sum on  Visa Qiwi Wallet  site

  //==========================
  $paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => $type ) ) );
  $parameters = $paySystem[ 'parameters' ];
  $params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );

  $intDefCurrency = 'rur';
  $siteCurrency = DSConfig::getVal( 'site_currency' );
  $intSumm = number_format( (float)$data[ 'sum' ], 2, '.', '' );
  $OutSum = number_format(
   (float)Formulas::convertCurrency( $data[ 'sum' ], $siteCurrency, $intDefCurrency ),
   2,
   '.',
   ''
  );
  $data[ 'sum' ] = $OutSum;
  $data[ 'intSumm' ] = $intSumm;
  $payment = new Payment;
  $payment->status = 3;
  $payment->sum = $intSumm;
  $payment->check_summ = $OutSum; //round($OutSum * DSConfig::model()->findByPk('rate_usd')->value, 2);
  $payment->date = time();
  $payment->description = Yii::t( 'main', 'Заявка на пополнение Qiwi' );
  $payment->uid = Yii::app()->user->id;
  $payment->save();

// ID заказа
  $data[ 'InvId' ] = $payment->id;
  $data[ 'site_currency' ] = $siteCurrency;
// ID пользователя
   $data['User_id'] = Yii::app()->user->id;
   $data['Success_Url'] = $params['SuccessUrl'].'/payid/'.$payment->id;
   $data['Fail_Url'] = $params['FailUrl'].'/payid/'.$payment->id;

  $this->render(
       'webroot.themes.' . DSConfig::getVal( 'site_front_theme' ) . '.views.cabinet.payment_online',
        array ( 'data' => $data, 'type' => $type )
  );

  return false;
 }

    /*    Передаваемые параметры:
  <?xml version="1.0" encoding='UTF-8' ?>
  <paySystem>
  <name><![CDATA[liqpay]]></name>
  <url><![CDATA[https://www.liqpay.com/?do=clickNbuy]]></url>
  <operation_xml><![CDATA[<request>
      <version>1.2</version>
      <merchant_id>777danvitnet</merchant_id>
      <result_url>http://777.danvit.net/ru/cabinet/balance/paysuccess/sender/liqpay</result_url>
      <server_url>http://777.danvit.net/ru/cabinet/balance/payresult/sender/liqpay</server_url>
      <order_id>{InvId}</order_id>
      <amount>{sum}</amount>
      <currency>UAH</currency>
      <description>Payment from user #{Shp_item}</description>
      <default_phone>{userPhone}</default_phone>
      <pay_way>card,liqpay</pay_way>
      <goods_id>{Shp_item}</goods_id>
  </request>]]></operation_xml>

  <merchant_sig>123123123</merchant_sig>
  <intDefCurrency>uah</intDefCurrency>
  </paySystem>

  <form action="#url" method="POST" />
          <input type="hidden" name="operation_xml" value="{#operation_xml}" />
          <input type="hidden" name="signature" value="{signature}" />
  <input type="submit" value="Перейти к оплате">
  </form>


    */
    private function payLiqpay($data, $type)
    {
        $paySystem = PaySystems::model()->find('int_name=:type', array('type' => $type));
        $parameters = $paySystem['parameters'];
        $params = (array) simplexml_load_string($parameters, null, LIBXML_NOCDATA);
        $intDefCurrency = (string) $params['intDefCurrency'];
        $siteCurrency = DSConfig::getVal('site_currency');
        $intSumm = number_format((float) $data['sum'], 2, '.', '');
        $OutSum = number_format(
          (float) Formulas::convertCurrency($data['sum'], $siteCurrency, $intDefCurrency),
          2,
          '.',
          ''
        );
        $data['sum'] = $OutSum;
        $data['intSumm'] = $intSumm;

        $payment = new Payment;
        $payment->status = 3;
        $payment->sum = (float) str_replace(' ', '', $intSumm);
        $payment->check_summ = (float) str_replace(
          ' ',
          '',
          $OutSum
        ); //round($OutSum * DSConfig::model()->findByPk('rate_usd')->value, 2);
        $payment->date = time();
        $payment->description = Yii::t('main', 'Заявка на пополнение LiqPay');
        $payment->uid = Yii::app()->user->id;
        $payment->save();

// ID заказа
        $data['InvId'] = $payment->id;
// ID товара, в нашем случае ID пользователя
        $data['Shp_item'] = Yii::app()->user->id;
        $data['site_currency'] = $siteCurrency;
        $operation_xml = PaySystems::preRenderForm($data, $type, (string) $params['operation_xml']);
        $data['operation_xml'] = base64_encode($operation_xml);

        $data['merchant_sig'] = (string) $params['merchant_sig'];
        $data['signature'] = base64_encode(sha1($data['merchant_sig'] . $operation_xml . $data['merchant_sig'], 1));

// формирование подписи
// generate signature
//    $data['SignatureValue'] = md5($MrchLogin . ':' . $OutSum . ':' . $data['InvId'] . ':' . $password1 . ':' . 'Shp_item=' . $data['Shp_item']);
        $this->render(
          'webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.payment_online',
          array('data' => $data, 'type' => $type)
        );
        return true;
    }

    private function payPaypal($data, $type)
    {
        return false;
    }

    private function payYandexmoney( $data, $type )
    {
	    /* Передаваемые параметры:

	       <form method=" POST " action="{#url}" target="_blank">
	       <input name="receiver" value="{#Receiver}" type="hidden">
	       <input name="formcomment" value="Пополнение счёта пользователя #{Shp_item}" type="hidden">
	       <input name="short-dest" value="Пополнение счёта пользователя #{Shp_item}" type="hidden">
	       <input name="label" value="{InvId}" type="hidden">
	       <input name="quickpay-form" value="shop" type="hidden">
	       <input name="targets" value="Пополнение счёта пользователя #{Shp_item}" type="hidden">
	       <input name="sum" value="{sum}" data-type="number" type="hidden">
	       <input name="paymentType" value="PC" type="hidden">
	       <br>
	       <input value="Перейти к оплате" onclick="window.history.back();" type="submit">
	       </form>
	     */
	    // Оплата заданной суммы на сайте Яндекс.Деньги
	    // Payment of the set sum on  Яндекс.Деньги  site
	    // регистрационная информация (receiver)
	    // registration info (receiver)

	    //==========================
	    $paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => $type ) ) );
	    $parameters = $paySystem[ 'parameters' ];
	    $params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );

	    $intDefCurrency = 'rur';
	    $siteCurrency = DSConfig::getVal( 'site_currency' );
	    $intSumm = number_format( (float)$data[ 'sum' ], 2, '.', '' );
	    $OutSum = number_format(
		    (float)Formulas::convertCurrency( $data[ 'sum' ], $siteCurrency, $intDefCurrency ),
		    2,
		    '.',
		    ''
	    );
	    $data[ 'sum' ] = $OutSum;
	    $data[ 'intSumm' ] = $intSumm;
	    $payment = new Payment;
	    $payment->status = 3;
	    $payment->sum = $intSumm;
	    $payment->check_summ = $OutSum; //round($OutSum * DSConfig::model()->findByPk('rate_usd')->value, 2);
	    $payment->date = time();
	    $payment->description = Yii::t( 'main', 'Заявка на пополнение Яндекс.Деньги' );
	    $payment->uid = Yii::app()->user->id;
	    $payment->save();

// ID заказа
	    $data[ 'InvId' ] = $payment->id;
	    $data[ 'site_currency' ] = $siteCurrency;
// ID пользователя
      $data['User_id'] = Yii::app()->user->id;

	    $this->render(
	         'webroot.themes.' . DSConfig::getVal( 'site_front_theme' ) . '.views.cabinet.payment_online',
		         array ( 'data' => $data, 'type' => $type )
	    );

	    return false;
    }

		private function payWalletone ( $data, $type )
	  {
		    /* Передаваемые параметры:
		    <form method="POST" action="{#url}" target="_blank" accept-charset="UTF-8">
		    <input name="WMI_MERCHANT_ID" value="{MerchantId}" type="hidden">
		    <input name="WMI_PAYMENT_AMOUNT" value="{sum}" type="hidden">
		    <input name="WMI_CURRENCY_ID" value="643" type="hidden">
		    <input name="WMI_DESCRIPTION" value="{Description}" type="hidden">
		    <input name="WMI_PAYMENT_NO" value="{InvId}" type="hidden">
		    <input name="WMI_SUCCESS_URL" value="{SuccessUrl}" type="hidden">
		    <input name="WMI_FAIL_URL" value="{FailUrl}" type="hidden">
		    <input name="WMI_SIGNATURE" value="{Signature}" type="hidden">
		    <br>
		    <input value="Перейти к оплате" onclick="window.history.back();" type="submit">
		    </form>
		    */
	    // Оплата заданной суммы на сайте Единая касса
	    // Payment of the set sum on  Wallet One  site
	    // регистрационная информация (WMI_MERCHANT_ID)
	    // registration info (WMI_MERCHANT_ID)

	    //==========================
	    $paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => $type ) ) );
	    $parameters = $paySystem[ 'parameters' ];
	    $params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );

	    $intDefCurrency = 'rur';
	    $siteCurrency = DSConfig::getVal( 'site_currency' );
	    $intSumm = number_format( (float)$data[ 'sum' ], 2, '.', '' );
	    $OutSum = number_format(
		    (float)Formulas::convertCurrency( $data[ 'sum' ], $siteCurrency, $intDefCurrency ),
		    2,
		    '.',
		    ''
	    );
	    $data[ 'sum' ] = $OutSum;
	    $data[ 'intSumm' ] = $intSumm;
	    $payment = new Payment;
	    $payment->status = 3;
	    $payment->sum = $intSumm;
	    $payment->check_summ = $OutSum; //round($OutSum * DSConfig::model()->findByPk('rate_usd')->value, 2);
	    $payment->date = time();
	    $payment->description = Yii::t( 'main', 'Заявка на пополнение Единая касса' );
	    $payment->uid = Yii::app()->user->id;
	    $payment->save();

// ID заказа
	    $data[ 'InvId' ] = $payment->id;
	    $data[ 'site_currency' ] = $siteCurrency;
// ID пользователя
      $data['User_id'] = Yii::app()->user->id;
      $data['Description'] =

/**** действуем согласно мануалу 	http://www.walletone.com/ru/merchant/documentation/ */
			$fields = array();

      $fields["WMI_MERCHANT_ID"]    = $params['Merchant_id'];
      $fields["WMI_PAYMENT_AMOUNT"] = $data[ 'sum' ];
      $fields["WMI_CURRENCY_ID"]    = "643";
			$fields["WMI_DESCRIPTION"]    = "BASE64:".base64_encode("Пополнение счёта пользователя #".$data['User_id']);
      $fields["WMI_PAYMENT_NO"]     = $data[ 'InvId' ];
      $fields["WMI_SUCCESS_URL"]    = $params['Success_url'];
      $fields["WMI_FAIL_URL"]       = $params['Fail_url'];

			//Сортировка значений внутри полей
      foreach($fields as $name => $val)
      {
        if (is_array($val))
        {
           usort($val, "strcasecmp");
           $fields[$name] = $val;
        }
      }

			// Формирование сообщения, путем объединения значений формы,
			// отсортированных по именам ключей в порядке возрастания.
			uksort( $fields, "strcasecmp" );
			$fieldValues = "";

			foreach ( $fields as $value )
			{
				if ( is_array( $value ) )
					foreach ( $value as $v )
					{
						//Конвертация из текущей кодировки (UTF-8)
						//необходима только если кодировка магазина отлична от Windows-1251
						$v = $this->win2utf( $v, "uw" ); //iconv( "utf-8", "windows-1251", $v );
						$fieldValues .= $v;
					}
				else
				{
					//Конвертация из текущей кодировки (UTF-8)
					//необходима только если кодировка магазина отлична от Windows-1251
					$value = $this->win2utf( $value, "uw" ); //iconv( "utf-8", "windows-1251", $value );
					$fieldValues .= $value;
				}
			}

			// Формирование значения параметра WMI_SIGNATURE, путем
			// вычисления отпечатка, сформированного выше сообщения,
			// по алгоритму MD5 и представление его в Base64

			$signature = base64_encode( pack( "H*", md5( $fieldValues . $params['SecretKey'] ) ) );

			//Добавление параметра WMI_SIGNATURE в словарь параметров формы
			//$fields[ "WMI_SIGNATURE" ] = $signature;
/*************************************************** End of doc.code */
			$data['MerchantId'] = $params['Merchant_id'];
			$data['Description'] = $fields["WMI_DESCRIPTION"];
			$data['SuccessUrl'] = $params['Success_url'];
			$data['FailUrl'] = $params['Fail_url'];
			$data['Signature'] = $signature;

	    $this->render(
	         'webroot.themes.' . DSConfig::getVal( 'site_front_theme' ) . '.views.cabinet.payment_online',
		         array ( 'data' => $data, 'type' => $type )
	    );

	    return false;
	  }

		private function payZpayment  ( $data, $type )
		{
      /* Передаваемые параметры:
			<form action="{#url}" method="POST" target="_blank" name="pay">
			<input name="LMI_PAYEE_PURSE" value="{#ZPShopId}" type="hidden">
			<input name="LMI_PAYMENT_AMOUNT" value="{sum}" type="hidden">
			<input name="LMI_PAYMENT_DESC" value="Пополнение счёта пользователя #{Shp_item}" type="hidden">
			<input name="LMI_PAYMENT_NO" value="{InvId}" type="hidden">
			<br>
			<input value="Перейти к оплате" onclick="window.history.back();" type="submit">
			</form>
	     */
	    // Оплата заданной суммы на сайте Z-Payment
	     // Payment of the set sum on  Z-Payment  site
      // регистрационная информация (receiver)
      // registration info (receiver)

	    //==========================
	    $paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => $type ) ) );
	    $parameters = $paySystem[ 'parameters' ];
	    $params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );

	    $intDefCurrency = 'rur';
	    $siteCurrency = DSConfig::getVal( 'site_currency' );
	    $intSumm = number_format( (float)$data[ 'sum' ], 2, '.', '' );
	    $OutSum = number_format(
		    (float)Formulas::convertCurrency( $data[ 'sum' ], $siteCurrency, $intDefCurrency ),
		    2,
		    '.',
		    ''
	    );
	    $data[ 'sum' ] = $OutSum;
	    $data[ 'intSumm' ] = $intSumm;
	    $payment = new Payment;
	    $payment->status = 3;
	    $payment->sum = $intSumm;
	    $payment->check_summ = $OutSum; //round($OutSum * DSConfig::model()->findByPk('rate_usd')->value, 2);
	    $payment->date = time();
	    $payment->description = Yii::t( 'main', 'Заявка на пополнение Z-Payment' );
	    $payment->uid = Yii::app()->user->id;
	    $payment->save();

// ID заказа
	    $data[ 'InvId' ] = $payment->id;
	    $data[ 'site_currency' ] = $siteCurrency;
// ID пользователя
      $data['Shp_item'] = Yii::app()->user->id;

	    $this->render(
	         'webroot.themes.' . DSConfig::getVal( 'site_front_theme' ) . '.views.cabinet.payment_online',
		         array ( 'data' => $data, 'type' => $type )
	    );
	    return false;
		}

/* ***************************************************************************************
 *                                                                  PAY RESULT FUNCTIONS *
 * **************************************************************************************/
    public function  actionPayResult($sender = false)
    {
	    header('Access-Control-Allow-Origin: *');
        $this->logActionCall($sender);
//        Utils::debugLog(print_r($_SERVER, true));
        // robokassa
        if ($sender == 'robokassa') {
            $paySystem = (PaySystems::model()->find('int_name=:type', array('type' => $sender)));
            $parameters = $paySystem['parameters'];
            $params = (array) simplexml_load_string($parameters, null, LIBXML_NOCDATA);

            $password2 = (string) $params['Password2'];

// чтение параметров
// read parameters
            $sum = $_POST['OutSum'];
            $InvId = $_POST['InvId'];
            $Shp_item = $_POST['Shp_item'];
            $SignatureValue = strtoupper($_POST["SignatureValue"]);
            $mySignatureValue = strtoupper(md5($sum . ':' . $InvId . ':' . $password2 . ':' . 'Shp_item=' . $Shp_item));

// проверка корректности подписи
// check signature
            if ($SignatureValue != $mySignatureValue) {
                echo "bad sign\n";//.$SignatureValue.' <> '.$mySignatureValue;
             //   echo "<br/>".$sum . ':' . $InvId . ':' . $password2 . ':' . 'Shp_item=' . $Shp_item ;
                Yii::app()->end();
                exit();
            }
//== OK ===================================================
            $payment = Payment::model()->findByPk($InvId);
            if ($payment != false) {
                $payment->status = 1;
                $payment->description = Yii::t('main', 'Зачисление средств robokassa');
                $payment->date = time();
                $payment->save();
            }
// признак успешно проведенной операции
// success
            echo 'OK' . $InvId . "\n";
            Yii::app()->end();
        }
//============================================================================
        if ($sender == 'liqpay') {
            if (!isset($_POST['operation_xml']) || !isset($_POST['signature'])) {
                echo "bad sign\n"; //TODO check needed response
                Yii::app()->end();
            }
            $operation_xml = $_POST['operation_xml'];
            $signature = $_POST['signature'];

            $xml_decoded = base64_decode($operation_xml);
            /*
            <response>
                  <version>1.2</version>
                  <merchant_id></merchant_id>
                  <order_id> ORDER_123456</order_id>
                  <amount>1.01</amount>
                  <currency>UAH</currency>
                  <description>Comment</description>
                  <status>success</status>
                  <code></code>
                  <transaction_id>31</transaction_id>
                  <pay_way>card</pay_way>
                  <sender_phone>+3801234567890</sender_phone>
                  <goods_id>1234</goods_id>
                  <pays_count>5</pays_count>
            </response>
                 Примечание, по тегам
                merchant_id - id мерчанта
                order_id - id заказа
                amount - стоимость
                currency - Валюта
                description - Описание
                status - статус транзакции
                code - код ошибки (если есть ошибка)
                transaction_id - id транзакции в системе LiqPay
                pay_way - способ которым оплатит покупатель(если не указывать то он сам выбирает, с карты или с телефона(liqpay, card))
                sender_phone - телефон оплативший заказ
                goods_id - id товара в счетчике покупок (если был передан) NEW!
                pays_count - число завершенных покупок данного товара (если был передан goods_id) NEW!

                *Примеры статусов
                status="success" - покупка совершена
                status="failure" - покупка отклонена
                status="wait_secure" - платеж находится на проверке

             */
            $paySystem = (PaySystems::model()->find('int_name=:type', array('type' => $sender)));
            $parameters = $paySystem['parameters'];
            $params = (array) simplexml_load_string($parameters, null, LIBXML_NOCDATA);
            $merchant_sig = (string) $params['merchant_sig'];
            $resp_signature = base64_encode(sha1($merchant_sig . $xml_decoded . $merchant_sig, 1));
            if ($signature != $resp_signature) {
                echo "bad sign\n";
                Yii::app()->end();
                exit();
            }

            $response = (array) simplexml_load_string($xml_decoded, null, LIBXML_NOCDATA);

// чтение параметров
// read parameters
            $InvId = $response['order_id'];
            $payment = Payment::model()->findByPk($InvId);
            if ($payment != false) {
                if ($response['status'] == 'success') {
//== OK ===================================================
                    $payment->status = 1;
                    $payment->description = Yii::t('main', 'Зачисление средств liqpay');
                    $payment->date = time();
                    $payment->comment = $xml_decoded;
                    $payment->save();
                    Yii::app()->user->setFlash(
                      'payment',
                      Yii::t('main', 'Ваш счёт пополнен на ') . $payment->sum . ' ' . DSConfig::getVal('site_currency')
                    );
                } else {
                    $payment->status = 4;
                    $payment->description = Yii::t('main', 'Отмена пополнения счёта liqpay');
                    $payment->date = time();
                    $payment->comment = $xml_decoded;
                    $payment->save();
                    Yii::app()->user->setFlash('payment', Yii::t('main', 'Ошибка пополнения счёта') . ' (liqpay)');
                    $this->redirect('/cabinet/balance/payment');
                }
            }
// признак успешно проведенной операции
// success
            echo 'OK' . $InvId . "\n";
            Yii::app()->end();
        }

		    // yandexmoney
		    if ( $sender == 'yandexmoney' )
		    {
			    /*  response
					"test_notification":"true",
					"sender":"41001000040",
					"amount":"413.45",
					"operation_id":"test-notification",
					"sha1_hash":"9d46d116251022a8d9aee32684c6bcb57a73a0fa",
					"notification_type":"p2p-incoming",
					"codepro":"false",
					"label":"",
					"datetime":"2014-09-25T12:06:57Z",
					"currency":"643"
					-----------------

					test_notification         - bool - тестовое уведомление
					sender                    - номер счета/кошелька
					amount                    - Сумма операции
					operation_id              - Идентификатор операции в истории счета получателя
					sha1_hash                 - SHA-1 hash параметров уведомления
					notification_type         - "p2p-incoming" -операция с кошельком
					codepro                   - bool - перевод защищен кодом протекции
					label                     - Метка платежа
					datetime                  - дата время перевода
					currency                  - валюта, всегда 643
			 */

			    $paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => $sender ) ) );
			    $parameters = $paySystem[ 'parameters' ];
			    $params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );

			    $word = $_POST[ 'notification_type' ] . '&' .
				    $_POST[ 'operation_id' ] . '&' .
				    $_POST[ 'amount' ] . '&' .
				    $_POST[ 'currency' ] . '&' .
				    $_POST[ 'datetime' ] . '&' .
				    $_POST[ 'sender' ] . '&' .
				    $_POST[ 'codepro' ] . '&' .
				    $params[ 'SecretWord' ] . '&' .
				    $_POST[ 'label' ];

// чтение параметров
// read parameters
			    $InvId = $_POST[ 'label' ];
			    $payment = Payment::model()->findByPk( $InvId );
			    if ( $payment != false )
			    {
				    // проверяем хеш
				    if ( $_POST[ 'sha1_hash' ] == sha1( $word ) )
				    {
//== OK ===================================================
					    $payment->status = 1;
					    $payment->description = Yii::t( 'main', 'Зачисление средств yandexmoney' );
					    $payment->date = time();
					    $payment->save();
					    Yii::app()->user->setFlash(
					                    'payment',
						                    Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' .
						                    DSConfig::getVal( 'site_currency' )
					    );
				    }
			    }
			    Yii::app()->end();

		    }  // End if yandexmoney

		    // walletone
		    if ( $sender == 'walletone' )
		    {
			    if(isset($_POST[ 'WMI_PAYMENT_NO' ]))
			    {
				    $InvId = $_POST[ 'WMI_PAYMENT_NO' ];
				    $payment = Payment::model()->findByPk( $InvId );

				    if($payment->status == 1)
				    {
					    Yii::app()->user->setFlash(
						    'payment',
						    Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' .
						    DSConfig::getVal( 'site_currency' )
					    );
				    }
				    else if($payment->status == 4) {
					    Yii::app()->user->setFlash('payment', Yii::t('main', 'Пополнние счёта отменено'));
					    $this->W1_answer("Ok", "Сообщение принято");
				    }
				    else if($payment->status == 3) {
					    $this->W1_PayRes('result');
				    }
			    }
			    else
				    $this->W1_answer("Retry", "Отсутствует параметр WMI_PAYMENT_NO");
			    Yii::app()->end();

		    } // End if walletone

	    // Z-Payment
	    if ( $sender == 'zpayment' )
	    {
			    /*  response
			     Array
			     (
			         [LMI_PAYEE_PURSE] => 15669
			         [LMI_PAYMENT_AMOUNT] => 2.00
			         [LMI_PAYMENT_NO] => 60
			         [LMI_PAYER_WM] => ZP82586516
			         [LMI_SYS_TRANS_NO] => 3271930
			         [LMI_MODE] => 0
			         [LMI_SYS_INVS_NO] => 7901080
			         [LMI_PAYER_PURSE] => ZP82586516
			         [LMI_SYS_TRANS_DATE] => 20141028 12:38:36
			         [LMI_HASH] => 40150C7372E8945E8A2CBB6C4C870A16
			         [ZP_SUMMA_SELLER] => 2.00
			         [ZP_CURRENCY_INVOICE] => RUR
			         [ZP_TYPE_PAY] => ZP_ZP
			     )
			    */

		  	$paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => 'zpayment' ) ) );
		    $parameters = $paySystem[ 'parameters' ];
		    $params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );

		  	$error = false;
		    //Проверяем номер магазина
		    if ( $_POST[ 'LMI_PAYEE_PURSE' ] != $params[ 'ZPShopId' ] )
		    {
		     $error = true;
		     $err_text = "Id магазина не соответсвует настройкам сайта!";
		    }

		    if ( !$error )
		    {
		     // чтение параметров
		     // read parameters
		     $InvId = $_POST[ 'LMI_PAYMENT_NO' ];
		     $payment = Payment::model()->findByPk( $InvId );
			    if ( $payment != false )
			    {
				    if ( $payment->sum == $_POST[ 'LMI_PAYMENT_AMOUNT' ] )
				    {
					    //Расчет контрольного хеша из полученных переменных и Ключа мерчанта
					    $CalcHash = md5(
						    $_POST[ 'LMI_PAYEE_PURSE' ] .
						    $_POST[ 'LMI_PAYMENT_AMOUNT' ] .
						    $_POST[ 'LMI_PAYMENT_NO' ] .
						    $_POST[ 'LMI_MODE' ] .
						    $_POST[ 'LMI_SYS_INVS_NO' ] .
						    $_POST[ 'LMI_SYS_TRANS_NO' ] .
						    $_POST[ 'LMI_SYS_TRANS_DATE' ] .
						    $params[ 'SecretKey' ] .
						    $_POST[ 'LMI_PAYER_PURSE' ] .
						    $_POST[ 'LMI_PAYER_WM' ] );

					    //Сравниваем значение расчетного хеша с полученным
					    if ( $_POST[ 'LMI_HASH' ] == strtoupper( $CalcHash ) )
					    {
						    //== OK ===================================================
						    $payment->status = 1;
						    $payment->description = Yii::t( 'main', 'Зачисление средств zpayment' );
						    $payment->date = time();
						    $payment->save();
						    Yii::app()->user->setFlash(
							    'payment',
							    Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' . DSConfig::getVal( 'site_currency' )
						    );
						    $err_text = 'Ваш счёт пополнен на ' . $payment->sum;
						    echo 'YES';
					    }
					    else
					    {
						    $payment->status = 4;
						    $payment->description = Yii::t( 'main', 'Отмена пополнения счёта zpayment' );
						    $payment->date = time();
						    $payment->save();
						    Yii::app()->user->setFlash( 'payment', Yii::t( 'main', 'Ошибка пополнения счёта' ) . ' (liqpay)' );
						    $err_text = 'Ошибка пополнения счёта';
						    echo 'YES';
					    }
				    }
				    else
				    {
					    $error = true;
					    $err_text = "Сумма оплаты не соответсвует сумме заказа!";
				    }
			    }
			    else
			    {
				    $error = true;
				    $err_text = "Номер счета не соответсвует заказу!";
			    }
		    }
		    Yii::app()->end();
	    } // zpayment
    }

	public function actionPaySuccess( $sender = false, $payid=false )
	{
		header('Access-Control-Allow-Origin: *');
		$this->logActionCall( $sender );
//		Utils::debugLog( print_r( $_SERVER, true ) );
		// robokassa
		try
		{
			if ( $sender == 'robokassa' )
			{
				if ( !( isset( $_POST[ 'OutSum' ] ) && isset( $_POST[ 'InvId' ] ) && isset( $_POST[ 'Shp_item' ] ) &&
					isset( $_POST[ 'SignatureValue' ] ) )
				)
				{
					$this->redirect( '/' );
					//throw new CHttpException(404,Yii::t('main','Not Found'));
				}
				$paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => $sender ) ) );
				$parameters = $paySystem[ 'parameters' ];
				$params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );

				$password1 = (string)$params[ 'Password1' ];

// чтение параметров
// read parameters
				$sum = $_POST[ 'OutSum' ];
				$InvId = $_POST[ 'InvId' ];
				$Shp_item = $_POST[ 'Shp_item' ];
				$SignatureValue = strtoupper( $_POST[ 'SignatureValue' ] );
				$mySignatureValue = strtoupper(
					md5( $sum . ':' . $InvId . ':' . $password1 . ':' . 'Shp_item=' . $Shp_item )
				);

// проверка корректности подписи
// check signature
				if ( $SignatureValue == $mySignatureValue )
				{
					$payment = Payment::model()->findByPk( $InvId );
					if ( $payment != false )
					{
						Yii::app()->user->setFlash(
							'payment',
							Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' . DSConfig::getVal(
								'site_currency'
							)
						);
						$this->redirect( '/cabinet/balance' );
					}
				}
				else
				{
					Yii::app()->user->setFlash( 'payment', Yii::t( 'main', 'Ошибка пополнения счёта' ) );
					$this->redirect( '/cabinet/balance/payment' );
				}
			}

			if ( $sender == 'liqpay' )
			{
				$this->redirect( '/cabinet/balance/index' );
			}

			// walletone
			if ( $sender == 'walletone' )
			{
				if ( isset( $_POST[ 'WMI_PAYMENT_NO' ] ) )
				{
					$InvId = $_POST[ 'WMI_PAYMENT_NO' ];
					$payment = Payment::model()->findByPk( $InvId );

					if ( $payment->status == 1 )
					{
						Yii::app()->user->setFlash(
							'payment',
							Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' .
							DSConfig::getVal( 'site_currency' )
						);
						echo 'YES';   // передаем YES как признак успешно проведенной операции
					}
					else if ( $payment->status == 4 )
					{
						Yii::app()->user->setFlash( 'payment', Yii::t( 'main', 'Пополнние счёта отменено' ) );
						echo 'YES';   // передаем YES как признак успешно проведенной операции
					}
					else if ( $payment->status == 3 )
					{
						$this->W1_PayRes( 'success' );
					}
				}
				else
					$this->W1_answer( "Retry", "Отсутствует параметр WMI_PAYMENT_NO" );

				$this->redirect( '/cabinet/balance/index' );

			}  // End if walletone

			// Z-Payment
			if ( $sender == 'zpayment' )
			{
				/* response
				  Array
				  (
				      [LMI_PAYMENT_NO] => 60
				      [LMI_SYS_INVS_NO] => 7901080
				      [LMI_SYS_TRANS_NO] => 3271930
				      [LMI_SYS_TRANS_DATE] => 20141028 12:38:36
				      [Submit] => Вернуться в магазин
				  )
				*/
				if ( isset( $_POST[ 'LMI_PAYMENT_NO' ] ) )
				{
					$InvId = $_POST[ 'LMI_PAYMENT_NO' ];
					$payment = Payment::model()->findByPk( $InvId );

					if ( $payment != false )
					{
						Yii::app()->user->setFlash(
							'payment',
							Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' .
							DSConfig::getVal( 'site_currency' )
						);
					}
				}

				$this->redirect( '/cabinet/balance/index' );
			} // zpayment

			// Qiwi
			if ( $sender == 'qiwi' )
			{
				$InvId = $payid;
				$payment = Payment::model()->findByPk($InvId);
				            if ($payment != false) {
				                $payment->status = 1;
				                $payment->description = Yii::t('main', 'Зачисление средств Qiwi');
				                $payment->date = time();
				                $payment->save();

					            Yii::app()->user->setFlash(
					      							'payment',
					      							Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' .
					      							DSConfig::getVal( 'site_currency' )
					      						);
				            }
				$this->redirect( '/cabinet/balance/index' );
			} // Qiwi
		}
		catch ( Exception $e )
		{
			throw new CHttpException( 404, Yii::t( 'main', 'Not Found' ) );
		}
	}

	public function actionPayFail( $sender = false, $payid=false )
	{
		$this->logActionCall( $sender );
		// robokassa
		try
		{
			if ( $sender == 'robokassa' )
			{
				if ( !( isset( $_POST[ 'OutSum' ] ) && isset( $_POST[ 'InvId' ] ) && isset( $_POST[ 'Shp_item' ] ) ) )
				{
					$this->redirect( '/' );
					//throw new CHttpException(404,Yii::t('main','Not Found'));
				}
// чтение параметров
// read parameters
				$sum = $_POST[ 'OutSum' ];
				$InvId = $_POST[ 'InvId' ];
				$Shp_item = $_POST[ 'Shp_item' ];
// проверка корректности подписи
// check signature
				$payment = Payment::model()->findByPk( $InvId );
				if ( $payment != false )
				{
					$payment->status = 4;
					$payment->description = Yii::t( 'main', 'Отмена пополнения счёта RoboKassa' );
					$payment->date = time();
					$payment->save();
					Yii::app()->user->setFlash( 'payment', Yii::t( 'main', 'Пополнние счёта отменено' ) );
				}
				$this->redirect( '/cabinet/balance/payment' );
			}

			// walletone
			if ( $sender == 'walletone' )
			{
				if ( isset( $_POST[ 'WMI_PAYMENT_NO' ] ) )
				{
					$InvId = $_POST[ 'WMI_PAYMENT_NO' ];
					$payment = Payment::model()->findByPk( $InvId );

					if ( $payment->status == 1 )
					{
						Yii::app()->user->setFlash(
							'payment',
							Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' .
							DSConfig::getVal( 'site_currency' )
						);
					}
					else if ( $payment->status == 4 )
					{
						Yii::app()->user->setFlash( 'payment', Yii::t( 'main', 'Пополнние счёта отменено' ) );
						$this->W1_answer( "Ok", "Сообщение принято" );
					}
					else if ( $payment->status == 3 )
					{
						$this->W1_PayRes( 'fail' );
					}
				}
				else
					$this->W1_answer( "Retry", "Отсутствует параметр WMI_PAYMENT_NO" );

				$this->redirect( '/cabinet/balance/payment' );
			}  // End if walletone

						// Z-Payment
			if ( $sender == 'zpayment' )
			{
				/* response
				  Array
				  (
				      [LMI_PAYMENT_NO] => 60
				      [LMI_SYS_INVS_NO] => 7901080
				      [LMI_SYS_TRANS_NO] => 3271930
				      [LMI_SYS_TRANS_DATE] => 20141028 12:38:36
				      [Submit] => Вернуться в магазин
				  )
				*/
				if ( isset( $_POST[ 'LMI_PAYMENT_NO' ] ) )
				{
					$InvId = $_POST[ 'LMI_PAYMENT_NO' ];
					$payment = Payment::model()->findByPk( $InvId );

					if ( $payment != false )
					{
						Yii::app()->user->setFlash( 'payment',
							Yii::t( 'main', 'Пополнение счёта отменено' ) );
					}
				}

				$this->redirect( '/cabinet/balance/index' );
			} // zpayment

			// Qiwi
			if ( $sender == 'qiwi' )
			{
				$InvId = $payid;
				$payment = Payment::model()->findByPk($InvId);
        if ($payment != false) {
            $payment->status = 4;
            $payment->description = Yii::t('main', 'Отмена пополнения счёта Qiwi');
            $payment->date = time();
            $payment->save();

          Yii::app()->user->setFlash(
                  'payment',
						      Yii::t( 'main', 'Пополнение счёта отменено' ) );
        }
				$this->redirect( '/cabinet/balance/index' );
			} // Qiwi
		}
		catch ( Exception $e )
		{
			throw new CHttpException( 404, Yii::t( 'main', 'Not Found' ) );
		}
	}

	private function W1_PayRes($from)
	{
		/*
		 response
(
	[WMI_AUTO_ACCEPT] => 1
	[WMI_CREATE_DATE] => 2014-10-09+05%3a36%3a01
	[WMI_CURRENCY_ID] => 643
												//  Пополнение+счёта+пользователя+#2173
	[WMI_DESCRIPTION] => %cf%ee%ef%ee%eb%ed%e5%ed%e8%e5+%f1%f7%b8%f2%e0+%ef%ee%eb%fc%e7%ee%e2%e0%f2%e5%eb%ff+%232173
	[WMI_EXPIRED_DATE] => 2015-01-09+05%3a36%3a01
	[WMI_EXTERNAL_ACCOUNT_ID] => 410011332741938
	[WMI_FAIL_URL] => http%3a%2f%2fyouzz.ru%2fru%2fcabinet%2fbalance%2fpayfail%2fsender%2fwalletone
	[WMI_LAST_NOTIFY_DATE] => 2014-10-09+05%3a36%3a38
	[WMI_MERCHANT_ID] => 106653785691
	[WMI_NOTIFY_COUNT] => 1
	[WMI_ORDER_ID] => 346260718384
	[WMI_ORDER_STATE] => Accepted
	[WMI_PAYMENT_AMOUNT] => 1.00
	[WMI_PAYMENT_NO] => 9132
	[WMI_PAYMENT_TYPE] => YandexMoneyRUB
	[WMI_SUCCESS_URL] => http%3a%2f%2fyouzz.ru%2fru%2fcabinet%2fbalance%2fpaysuccess%2fsender%2fwalletone
	[WMI_UPDATE_DATE] => 2014-10-09+05%3a36%3a35
	[WMI_SIGNATURE] => zlK%2fVzAJfARfpPHnDdPrqg%3d%3d
)
*/
		$paySystem = ( PaySystems::model()->find( 'int_name=:type', array ( 'type' => 'walletone' ) ) );
		$parameters = $paySystem[ 'parameters' ];
		$params = (array)simplexml_load_string( $parameters, null, LIBXML_NOCDATA );


		if ( !isset( $_POST[ "WMI_SIGNATURE" ] ) )
			$this->W1_answer( "Retry", "Отсутствует параметр WMI_SIGNATURE" );

		if ( !isset( $_POST[ "WMI_PAYMENT_NO" ] ) )
			$this->W1_answer( "Retry", "Отсутствует параметр WMI_PAYMENT_NO" );

		if ( !isset( $_POST[ "WMI_ORDER_STATE" ] ) )
			$this->W1_answer( "Retry", "Отсутствует параметр WMI_ORDER_STATE" );

		// Извлечение всех параметров POST-запроса, кроме WMI_SIGNATURE

		foreach ( $_POST as $name => $value )
		{
			if ( $name !== "WMI_SIGNATURE" )
				$par[ $name ] = $value;
		}

		// Сортировка массива по именам ключей в порядке возрастания
		// и формирование сообщения, путем объединения значений формы

		uksort( $par, "strcasecmp" );
		$values = "";

		foreach ( $par as $name => $value )
		{
			//Конвертация из текущей кодировки (UTF-8)
			//необходима только если кодировка магазина отлична от Windows-1251
//echo '<br /><br />value: '.$value;
			$value = $this->win2utf( $value, "uw" ); //iconv( "UTF-8", "WINDOWS-1251//IGNORE", $value );
			$values .= $value;
		}

		// Формирование подписи для сравнения ее с параметром WMI_SIGNATURE

		$signature = base64_encode( pack( "H*", md5( $values . $params[ 'SecretKey' ] ) ) );


//			    echo 'signature: '.$signature.'<br /><br />';
//			    echo 'SecretKey: '.$params['SecretKey'].'<br /><br />';
		//Сравнение полученной подписи с подписью W1

		if ( $signature == $_POST[ "WMI_SIGNATURE" ] )
		{
			//== OK ===================================================
			// чтение параметров
			// read parameters
			$InvId = $_POST[ 'WMI_PAYMENT_NO' ];
			$payment = Payment::model()->findByPk( $InvId );
			if ( $payment != false )
			{
				if ( strtoupper( $_POST[ "WMI_ORDER_STATE" ] ) == "ACCEPTED" )
				{
					$payment->status = 1;
					$payment->description = Yii::t( 'main', 'Зачисление средств walletone' );
					$payment->date = time();
					$payment->save();

					Yii::app()->user->setFlash( 'payment',
						Yii::t( 'main', 'Ваш счёт пополнен на ' ) . $payment->sum . ' ' .
						DSConfig::getVal( 'site_currency' )
					);

				$this->W1_answer( "Ok", "Заказ #" . $_POST[ "WMI_PAYMENT_NO" ] . " оплачен!" );
				}
				else if ( $from == 'fail' )
				{
					$payment->status = 4;
					$payment->description = Yii::t( 'main', 'Зачисление средств walletone' );
					$payment->date = time();
					$payment->save();

					Yii::app()->user->setFlash('payment', Yii::t('main', 'Пополнние счёта отменено'));
	        $this->W1_answer("Ok", "Сообщение принято");
				}
			}
		}
		else // Подпись не совпадает, возможно вы поменяли настройки интернет-магазина
			$this->W1_answer( "Retry", "Неверная подпись " . $_POST[ "WMI_SIGNATURE" ] );
	}

	private function W1_answer( $result, $description )
	{
		print "WMI_RESULT=" . strtoupper( $result ) . "&";
		print "WMI_DESCRIPTION=" . urlencode( $description );
		exit();
	}

	public function actionReadSer( $id = 0 )
	{
		$res = PaySystemsLog::model()->findByPk( $id );
		$data = unserialize( $res[ 'data' ] );
		echo '<pre>';
		print_r( $data );
		echo '</pre>';

	}

	/** Замена функции iconv()
	 * @param $txt
	 * @param string $dir
	 * @return mixed
	 */
	private function win2utf( $txt, $dir = "wu" )
	{
		// cp1251
		$in_arr = array (
			chr( 208 ), chr( 192 ), chr( 193 ), chr( 194 ),
			chr( 195 ), chr( 196 ), chr( 197 ), chr( 168 ),
			chr( 198 ), chr( 199 ), chr( 200 ), chr( 201 ),
			chr( 202 ), chr( 203 ), chr( 204 ), chr( 205 ),
			chr( 206 ), chr( 207 ), chr( 209 ), chr( 210 ),
			chr( 211 ), chr( 212 ), chr( 213 ), chr( 214 ),
			chr( 215 ), chr( 216 ), chr( 217 ), chr( 218 ),
			chr( 219 ), chr( 220 ), chr( 221 ), chr( 222 ),
			chr( 223 ), chr( 224 ), chr( 225 ), chr( 226 ),
			chr( 227 ), chr( 228 ), chr( 229 ), chr( 184 ),
			chr( 230 ), chr( 231 ), chr( 232 ), chr( 233 ),
			chr( 234 ), chr( 235 ), chr( 236 ), chr( 237 ),
			chr( 238 ), chr( 239 ), chr( 240 ), chr( 241 ),
			chr( 242 ), chr( 243 ), chr( 244 ), chr( 245 ),
			chr( 246 ), chr( 247 ), chr( 248 ), chr( 249 ),
			chr( 250 ), chr( 251 ), chr( 252 ), chr( 253 ),
			chr( 254 ), chr( 255 )
		);

		// utf-8
		$out_arr = array (
			chr( 208 ) . chr( 160 ), chr( 208 ) . chr( 144 ), chr( 208 ) . chr( 145 ),
			chr( 208 ) . chr( 146 ), chr( 208 ) . chr( 147 ), chr( 208 ) . chr( 148 ),
			chr( 208 ) . chr( 149 ), chr( 208 ) . chr( 129 ), chr( 208 ) . chr( 150 ),
			chr( 208 ) . chr( 151 ), chr( 208 ) . chr( 152 ), chr( 208 ) . chr( 153 ),
			chr( 208 ) . chr( 154 ), chr( 208 ) . chr( 155 ), chr( 208 ) . chr( 156 ),
			chr( 208 ) . chr( 157 ), chr( 208 ) . chr( 158 ), chr( 208 ) . chr( 159 ),
			chr( 208 ) . chr( 161 ), chr( 208 ) . chr( 162 ), chr( 208 ) . chr( 163 ),
			chr( 208 ) . chr( 164 ), chr( 208 ) . chr( 165 ), chr( 208 ) . chr( 166 ),
			chr( 208 ) . chr( 167 ), chr( 208 ) . chr( 168 ), chr( 208 ) . chr( 169 ),
			chr( 208 ) . chr( 170 ), chr( 208 ) . chr( 171 ), chr( 208 ) . chr( 172 ),
			chr( 208 ) . chr( 173 ), chr( 208 ) . chr( 174 ), chr( 208 ) . chr( 175 ),
			chr( 208 ) . chr( 176 ), chr( 208 ) . chr( 177 ), chr( 208 ) . chr( 178 ),
			chr( 208 ) . chr( 179 ), chr( 208 ) . chr( 180 ), chr( 208 ) . chr( 181 ),
			chr( 209 ) . chr( 145 ), chr( 208 ) . chr( 182 ), chr( 208 ) . chr( 183 ),
			chr( 208 ) . chr( 184 ), chr( 208 ) . chr( 185 ), chr( 208 ) . chr( 186 ),
			chr( 208 ) . chr( 187 ), chr( 208 ) . chr( 188 ), chr( 208 ) . chr( 189 ),
			chr( 208 ) . chr( 190 ), chr( 208 ) . chr( 191 ), chr( 209 ) . chr( 128 ),
			chr( 209 ) . chr( 129 ), chr( 209 ) . chr( 130 ), chr( 209 ) . chr( 131 ),
			chr( 209 ) . chr( 132 ), chr( 209 ) . chr( 133 ), chr( 209 ) . chr( 134 ),
			chr( 209 ) . chr( 135 ), chr( 209 ) . chr( 136 ), chr( 209 ) . chr( 137 ),
			chr( 209 ) . chr( 138 ), chr( 209 ) . chr( 139 ), chr( 209 ) . chr( 140 ),
			chr( 209 ) . chr( 141 ), chr( 209 ) . chr( 142 ), chr( 209 ) . chr( 143 )
		);

		if ( $dir == 'wu' )
			$txt = str_replace( $in_arr, $out_arr, $txt );
		else
			$txt = str_replace( $out_arr, $in_arr, $txt );
		return $txt;
	}
//==================================================================

}