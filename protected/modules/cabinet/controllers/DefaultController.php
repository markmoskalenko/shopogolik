<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="DefaultController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?
class DefaultController extends CustomFrontController {
  public function filters() {
    return array_merge(
      array(
        'Rights', // perform access control for CRUD operations
      ),
      parent::filters()
    );
  }

  public function actionIndex() {
      $this->layout = '//layouts/my_account';
    $this->body_class = 'cabinet';
    $this->pageTitle = Yii::t('main', 'Личный кабинет');
    $this->breadcrumbs = array(
      $this->pageTitle,
    );
    $uid = Yii::app()->user->id;
    $ordersByStatuses = OrdersStatuses::getAllStatusesListAndOrderCount($uid);
    $user = Users::getUser($uid);
    $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.index', array(
      'user'             => $user,
      'ordersByStatuses' => $ordersByStatuses
    ));
  }
}