<?/*******************************************************************************************************************
 * This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
 * Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
 * All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
 * You can't use this file without of the author's permission.
 * ====================================================================================================================
 * <description file="FavoriteController.php">
 * </description>
 **********************************************************************************************************************/
?>
<?php

class FavoriteController extends CustomFrontController {
public $layout = '//layouts/my_account';
  /**
   * @return array action filters
   */
  public function filters() {
    return array_merge (
      array (
        'Rights', // perform access control for CRUD operations
      ),
      parent::filters()
    );
  }


  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView() {
    $id = $_REQUEST["id"];

    if (Yii::app()->request->isAjaxRequest) {
      $this->renderPartial('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.ajax_view', array(
        'model' => $this->loadModel($id),
      ));

    }
    else {
      $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.view', array(
        'model' => $this->loadModel($id),
      ));
    }
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new Favorite;

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "favorite-create-form");
    if (Yii::app()->request->isAjaxRequest) {
      if (isset($_POST['Favorite'])) {
        $model->attributes = $_POST['Favorite'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }
    }
    else {
      if (isset($_POST['Favorite'])) {
        $model->attributes = $_POST['Favorite'];
        if ($model->save()) {
          $this->redirect(array('view', 'id' => $model->id));
        }

      }

      $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.create', array(
        'model' => $model,
      ));
    }
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate() {

    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : $_REQUEST["Favorite"]["id"];
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model, "favorite-update-form");

    if (Yii::app()->request->isAjaxRequest) {

      if (isset($_POST['Favorite'])) {

        $model->attributes = $_POST['Favorite'];
        if ($model->save()) {
          echo $model->id;
        }
        else {
          echo "false";
        }
        return;
      }

      $this->renderPartial('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite._ajax_update_form', array(
        'model' => $model,
      ));
      return;

    }

    if (isset($_POST['Favorite'])) {
      $model->attributes = $_POST['Favorite'];
      if ($model->save()) {
        $this->redirect(array('view', 'id' => $model->id));
      }
    }

    $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.update', array(
      'model' => $model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id = FALSE) {
    if (!$id) {
      $id = $_GET["id"];
    }
    // we only allow deletion via POST request
    if ($id > 0) {
      $this->loadModel($id)->delete();
      echo Yii::t('main', 'Тоавр удалён из избранного');
      Yii::app()->end();
    }
    elseif ($id == -1) {
      try {
        Favorite::model()->deleteAll('uid=:uid', array(':uid' => Yii::app()->user->id));
      } catch (Exception $e) {
        $this->redirect(Yii::app()->request->urlReferrer);
      }
      $this->redirect(Yii::app()->request->urlReferrer);
    }
    else {
      $this->redirect(Yii::app()->request->urlReferrer);
    }
  }

  /**
   * Lists all models.
   */
  public function actionList($cid=0) {
    $this->pageTitle = Yii::t('main', 'Товары');
    $this->body_class = 'cabinet';
    $this->breadcrumbs = array(
      Yii::t('main', 'Личный кабинет') => '/cabinet',
      $this->pageTitle
    );
    $uid = Yii::app()->user->id;
    if (($uid === FALSE) || ($uid == NULL)) {
      echo Yii::t('main', 'У Вас нет избранных товаров.');
      return;
    }
      $lang = Utils::TransLang();
      $favoriteMenu = Favorite::getFavoriteMenu($uid,$lang);
      $iCid=$cid;
    $count = Yii::app()->db->createCommand("SELECT COUNT(0) FROM favorites where uid=:uid and (cid=:cid or :cid=0)")
      ->bindParam(':uid', $uid, PDO::PARAM_INT)
      ->bindParam(':cid', $iCid, PDO::PARAM_INT)
      ->queryScalar();
    $sql = "SELECT * FROM favorites where uid=:uid and (cid=:cid or :cid=0) order by date desc";
    $dataProvider = new CSqlDataProvider($sql, array(
        'params'         => array(':uid' => $uid,':cid'=>$iCid),
        'id'             => 'favorites_dataProvider',
        'keyField'       => 'id',
        'totalItemCount' => $count,
        'pagination'     => array(
          'pageSize' => 40,
        )
      )
    );
//--------------------------- for export --------
/*    $exportData = Favorite::getExportData($uid);
    $categoriesCount = count($exportData->categoriesDistinct);
    $itemsCount = count($exportData->items);
*/
//-----------------------------------------------
    $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.list', array(
      'dataProvider' => $dataProvider,
      'favoriteMenu'=>$favoriteMenu,
//      'categoriesCount' =>$categoriesCount,
//      'itemsCount' => $itemsCount,
    ));
  }

  /**
   * Lists all models.
   */
  public function actionIndex() {
    $session = new CHttpSession;
    $session->open();
    $criteria = new CDbCriteria();

    $model = new Favorite('search');
    $model->unsetAttributes(); // clear any default values

    if (isset($_GET['Favorite'])) {
      $model->attributes = $_GET['Favorite'];

      if (!empty($model->id)) {
        $criteria->addCondition('id = "' . $model->id . '"');
      }

      if (!empty($model->uid)) {
        $criteria->addCondition('uid = "' . $model->uid . '"');
      }

      if (!empty($model->num_iid)) {
        $criteria->addCondition('num_iid = "' . $model->num_iid . '"');
      }

      if (!empty($model->date)) {
        $criteria->addCondition('date = "' . $model->date . '"');
      }

      if (!empty($model->cid)) {
        $criteria->addCondition('cid = "' . $model->cid . '"');
      }

      if (!empty($model->express_fee)) {
        $criteria->addCondition('express_fee = "' . $model->express_fee . '"');
      }

      if (!empty($model->price)) {
        $criteria->addCondition('price = "' . $model->price . '"');
      }

      if (!empty($model->promotion_price)) {
        $criteria->addCondition('promotion_price = "' . $model->promotion_price . '"');
      }

      if (!empty($model->pic_url)) {
        $criteria->addCondition('pic_url = "' . $model->pic_url . '"');
      }

      if (!empty($model->seller_rate)) {
        $criteria->addCondition('seller_rate = "' . $model->seller_rate . '"');
      }

    }
    $session['Favorite_records'] = Favorite::model()->findAll($criteria);

    $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.index', array(
      'model' => $model,
    ));

  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new Favorite('search');
    $model->unsetAttributes(); // clear any default values
    if (isset($_GET['Favorite'])) {
      $model->attributes = $_GET['Favorite'];
    }

    $this->render('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.admin', array(
      'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = Favorite::model()->findByPk($id);
    if ($model === NULL) {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model, $form_id) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionGenerateExcel() {
    $session = new CHttpSession;
    $session->open();

    if (isset($session['Favorite_records'])) {
      $model = $session['Favorite_records'];
    }
    else {
      $model = Favorite::model()->findAll();
    }

    Yii::app()->request->sendFile(date('YmdHis') . '.xls',
      $this->renderPartial('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.excelReport', array(
        'model' => $model
      ), TRUE,false,true)
    );
  }

  public function actionGeneratePdf() {
    $session = new CHttpSession;
    $session->open();
    Yii::import('application.extensions.ajaxgii.bootstrap.*');
    require_once('tcpdf/tcpdf.php');
    if (Utils::AppLang() == 'ru') {
      require_once('tcpdf/config/lang/rus.php');
    }
    else {
      require_once('tcpdf/config/lang/eng.php');
    }

    if (isset($session['Favorite_records'])) {
      $model = $session['Favorite_records'];
    }
    else {
      $model = Favorite::model()->findAll();
    }

    $html = $this->renderPartial('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.expenseGridtoReport', array(
      'model' => $model
    ), TRUE);

    //die($html);

    $pdf = new TCPDF();
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(Yii::app()->name);
    $pdf->SetTitle('Favorite Report');
    $pdf->SetSubject('Favorite Report');
    //$pdf->SetKeywords('example, text, report');
    $pdf->SetHeaderData('', 0, "Report", '');
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Example Report by " . Yii::app()->name, "");
    $pdf->setHeaderFont(Array('helvetica', '', 8));
    $pdf->setFooterFont(Array('helvetica', '', 6));
    $pdf->SetMargins(15, 18, 15);
    $pdf->SetHeaderMargin(5);
    $pdf->SetFooterMargin(10);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->AddPage();
    $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
    $pdf->LastPage();
    $pdf->Output("Favorite_002.pdf", "I");
  }

  public function actionAdd($iid, $download = TRUE) {
      $lang = Utils::TransLang();
      $uid = Yii::app()->user->id;
      Yii::app()->cache->delete('Favorites-menu-getTree-' . $lang.'-'.$uid);
    if (($uid === FALSE) || ($uid === NULL)) {
      $msg = Yii::t('main', 'Для добавления товаров в избранное Вам необходимо зарегистрироваться!');
    }
    else {
      $num_iid = $iid;
      if (!$download) {
        $command = Yii::app()->db->createCommand("insert ignore into favorites
              (`uid`,`num_iid`,`date`,`cid`,`express_fee`,`price`,`promotion_price`,`pic_url`,`seller_rate`)
               select
               :uid,`num_iid`,Now(),`cid`,`express_fee`,`price`,`promotion_price`,`pic_url`,`seller_rate`
                from log_items_requests rr
               where rr.`num_iid`=:num_iid order by rr.`date` DESC LIMIT 1")
          ->bindParam(':uid', $uid, PDO::PARAM_INT)
          ->bindParam(':num_iid', $num_iid, PDO::PARAM_STR);
        $command->execute();
        $msg = Yii::t('main', 'Товар добавлен в избранное');
      }
      else {
//===================================================
        $item = new Item($iid, TRUE);
        if (!isset($item->taobao_item) || !isset($item->taobao_item->num_iid) || ($item->taobao_item->num_iid == 0)) {
          $msg = Yii::t('main', 'Ошибка получения данных о товаре - повторите попытку позже...');
        }
        else {
          $taobaoItem = $item->taobao_item;
          $taobaoItemSerialized = serialize($taobaoItem);
          $command = Yii::app()->db->createCommand("INSERT INTO `favorites`
       (`uid`,`num_iid`,`date`,`cid`,`express_fee`,`price`,`promotion_price`,`pic_url`,`seller_rate`,`dsg_item`)
        VALUES (:uid,:num_iid,Now(),:cid,:express_fee,:price,:promotion_price,:pic_url,:seller_rate,:dsg_item)
        ON DUPLICATE KEY UPDATE `dsg_item`=:dsg_item")
            ->bindParam(':uid', $uid, PDO::PARAM_INT)
            ->bindParam(':num_iid', $num_iid, PDO::PARAM_STR)
            ->bindParam(':cid', $taobaoItem->cid, PDO::PARAM_STR)
            ->bindParam(':express_fee', $taobaoItem->express_fee, PDO::PARAM_STR)
            ->bindParam(':price', $taobaoItem->price, PDO::PARAM_STR)
            ->bindParam(':promotion_price', $taobaoItem->promotion_price, PDO::PARAM_STR)
            ->bindParam(':pic_url', $taobaoItem->pic_url, PDO::PARAM_STR)
            ->bindParam(':seller_rate', $taobaoItem->valReviewsApi->dsr->gradeAvg, PDO::PARAM_STR)
            ->bindParam(':dsg_item', $taobaoItemSerialized, PDO::PARAM_STR);
          $command->execute();

          $msg = Yii::t('main', 'Товар добавлен в избранное');
        }
//===================================================
      }
    }
    echo $msg;
    Yii::app()->end();
//            Yii::app()->user->setFlash('cabinet',$msg);
//            $this->redirect(Yii::app()->request->urlReferrer);
  }

  public function actionGetYML($uid) {
    $exportData = Favorite::getExportData($uid);
    Yii::app()->request->sendFile('dropshop-items-' . $uid . '.xml', //date('YmdHi')
      $this->renderPartial('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.YMLReport', array(
        'exportData' => $exportData,
      ), TRUE,false,true)
    );
  }
  public function actionGetCSV($uid) {
    $exportData = Favorite::getExportData($uid);
    Yii::app()->request->sendFile('dropshop-items-' . $uid  . '.csv', //date('YmdHi')
      $this->renderPartial('webroot.themes.' . DSConfig::getVal('site_front_theme') . '.views.cabinet.favorite.CSVReport', array(
        'exportData' => $exportData,
      ), TRUE)
    );
  }
}
