<?php
header('content-type:application/json; charset=utf-8');
require_once dirname(__FILE__).'/postcalc_light_lib.php';

if ( isset($_GET['post_index']) ) {
    $post_index=htmlspecialchars($_GET['post_index'], ENT_QUOTES);
    echo postcalc_autocomplete($post_index, $arrPostcalcConfig['autocomplete_items']);
}

