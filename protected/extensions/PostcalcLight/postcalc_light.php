<?php 
// Загружаем библиотеку
require_once dirname(__FILE__).'/postcalc_light_lib.php';
extract($arrPostcalcConfig,EXTR_PREFIX_ALL,'postcalc_config');
// Инициализируем значения полей формы
$postcalc_from = ( isset($_GET['postcalc_from']) ) ? $_GET['postcalc_from'] : $postcalc_config_default_from;
$postcalc_to = ( isset($_GET['postcalc_to']) ) ? $_GET['postcalc_to'] : '190000';
$postcalc_weight = ( isset($_GET['postcalc_weight'])) ? $_GET['postcalc_weight'] : 1000;
$postcalc_valuation = ( isset($_GET['postcalc_valuation']) ) ?$_GET['postcalc_valuation']:1000;
$postcalc_country=(isset($_GET['postcalc_country'])) ? $_GET['postcalc_country'] : 'RU';
// Выдаем заголовок с указанием на кодировку
header("Content-Type: text/html; charset=$postcalc_config_cs");
?>
<!DOCTYPE html>
<html>
    <head>
        <style>

            #postcalc {width: auto;margin: -10px 0;font-size: 14px;font-family: 'PT Sans Narrow', sans-serif !important;}
            #postcalc_form {margin: 0px 100px;}
            #postcalc_form label {display: inline-block; width: 10em; }
            #postcalc_form legend {padding:0.5 1.5em; }
            #postcalc_loader {left:17em;top:5em}
            #postcalc_country  { width:20em }
            #postcalc_from  { width:20em }
            #postcalc_to { width:20em }
            #postcalc_from_to {margin:10px 100px;}
            #postcalc_from_to TD { vertical-align: top }
            #postcalc_table {margin:0 100px;WIDTH: 80%;border:0;}
            #postcalc_table TR { }
            #postcalc_table TD { padding:0.2em;padding-left:1em }
            #postcalc_table TH  { padding:0.5em; }
            .postcalc_error { background-color:#FDD; color:red }
            .ui1-widget-header {color: #EAF5F7;font-size:18px;padding: 5px 0;font-weight:700;}
            .ui-widget input, .ui-widget select {margin: 5px 0; font-size: 0.9em !important;font-family: 'PT Sans Narrow', sans-serif !important;border: solid 1px grey;padding: 2px 5px;}
            .ui-button {
                border: none;
                background-color: transparent;
                text-transform: uppercase;
                border-bottom: 1px dotted #575555;
                color: #575555;
                font-size: 14px;
                cursor: pointer;
                font-family: 'PT Sans Narrow', sans-serif !important;
                margin: 10px 142px;
            }
            @font-face {
                font-family: 'PT Sans Narrow';
                src: local('PT Sans Narrow'), local('PTSans-Narrow'), url('<?= $this->frontThemePath ?>/assets/fonts/ptn57f.woff') format('woff');
                font-weight: normal;
                font-style: normal;
            }
            @font-face {
                font-family: 'PT Sans Narrow';
                src: local('PT Sans Narrow Bold'), local('PTSans-NarrowBold'), url('<?= $this->frontThemePath ?>/assets/fonts/ptn77f.woff') format('woff');
                font-weight: bold;
                font-style: normal;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="<?= $this->frontThemePath ?>/css/colors.css"/>
        <script src='//yandex.st/jquery/1.8.3/jquery.min.js'></script>

<? /*
       <script src='//yandex.st/jquery-ui/1.9.2/jquery-ui.min.js'></script>
       <link rel='stylesheet' href='//yandex.st/jquery-ui/1.10.4/themes/start/jquery-ui.min.css' type='text/css' media='screen' />
       <script src='//yandex.st/jquery/1.10.2/jquery.min.js'></script>
       <script src='//yandex.st/jquery-ui/1.10.4/jquery-ui.min.js'></script>
*/?>
       <script type="text/javascript">
            $(function() {
                var iframe = $('#postcalc-light', parent.document.body);
                iframe.height($(document.body).height()+10);
            });
       </script>
       <script>
       $(function(){
            // Стилизуем кнопки.
            $( ".ui-button").button();
            // Всплывающие сообщения
            $("[id^='postcalc_']").tooltip({ show: false, hide: false });
            <?php if ( !$arrPostcalcConfig['to_list'] ): ?>
            postcalc_autocomplete('postcalc_to');
            <?php endif; ?>
            <?php if ( !$arrPostcalcConfig['from_list'] ): ?>
            postcalc_autocomplete('postcalc_from');
            <?php endif; ?>
     function postcalc_autocomplete(field_name){

         $("#"+field_name).autocomplete({
           source: function(request,response) {
              $.ajax({
                url: 'postcalc_light_autocomplete.php',
                dataType: "json",
                data: {
                // Переменные, которые отправляются на сервер. Значение из привязанного комбо - request.term
                   post_index: request.term
                },
                success: function(data,textStatus) {
                  // Если вернули пустой массив - не найдено ничего
                  if (!data.length) {
                      alert('Ошибка - не найдено ни одного ОПС!');
                  } else {
                     response($.map(data, function(item) {
                    return {
                      label: item.label
                    }
                  }));
                  }
                },
                error: function(jqXHR,textStatus,errorThrown){
                  var s='';
                  $.each(jqXHR,function(key,value){
                      s+=key+'=>'+value+"\n";
                  });
                  alert("<pre>An error occurred! \njqXHR: "+jqXHR+"\ntextStatus: "+textStatus+"\nerrorThrown: "+errorThrown+"\nRESPONSE:"+jqXHR.responseText+"</pre>");
                }
              });
            },    
            minLength: 3,
            autoFocus: true,
            delay: 200
          });
        }
       });
       </script>
    </head>
    <body>
        <div id="postcalc" class="ui-widget">
        <form id="postcalc_form">

        <div class="ui1-widget-header ui1-corner-all site_main_color" >Расчет стоимости доставки Почтой России</div>
       <span style="display:<?= ($postcalc_config_hide_from) ? 'none' : 'block' ?>">
       <label for="postcalc_from" title="Отделение связи отправителя - шестизначный индекс или местоположение EMS (название региона или центр региона)"> Откуда </label>
       <?php if ( $postcalc_config_from_list ):?>
       <select  id="postcalc_from" name="postcalc_from">
           <?=  postcalc_make_select($arrPostcalcLocations,$postcalc_from) ?>
       </select>
       <?php else: ?>
       <input type="text"  id="postcalc_from" name="postcalc_from" size="20" value="<?=$postcalc_from?>"/>
       <?php endif; ?>
       <br>
       </span>
       <label for="postcalc_to" title="Отделение связи получателя - шестизначный индекс  или местоположение EMS (название региона или центр региона)"> Куда отправляем</label>
       <?php if ( $postcalc_config_to_list ):?>
       <select  id="postcalc_to" name="postcalc_to">
           <?=  postcalc_make_select($arrPostcalcLocations,$postcalc_to) ?>
       </select>
       <?php else: ?>
       <input type="text"  id="postcalc_to" name="postcalc_to" size="20" value="<?=$postcalc_to?>"/>
       <?php endif; ?>
       <br>
       <label for="postcalc_weight" title="Вес отправления в граммах - от 1 до 100000"> Вес а граммах</label>
       <input type="text" required id="postcalc_weight" name="postcalc_weight" size="6" value="<?=$postcalc_weight?>"/>
       <br>
        <label for="postcalc_valuation" title="Оценка товарного вложения в рублях - от 0 до 100000">Оценка, руб. </label>
        <input type="text" required id="postcalc_valuation" name="postcalc_valuation" size="6" value="<?=$postcalc_valuation?>"/>
        <br>
        <span style="display:block">
        <label for="postcalc_country" title="Страна назначения">Страна</label>
        <select id="postcalc_country" name="postcalc_country" size="1" >
            <?= postcalc_make_select($arrPostcalcCountries,$postcalc_country) ?>
        </select>
        </span>
		<input type="submit" value="Рассчитать!" class="ui-button" onclick="javascript:ldr=document.getElementById('postcalc_loader');ldr.style.display='block';" id='postcalc_form_submit'">

        </form>
    <div id='postcalc_loader' style='position: absolute;display:none'><img src='/images/ajax-loader.gif' alt='Индикатор загрузки'></div>
<?php
if ( isset($_GET['postcalc_from']) ) {
    
// Обращаемся к функции getPostcalc
$arrResponse=postcalc_request($_GET['postcalc_from'],$_GET['postcalc_to'],$_GET['postcalc_weight'],$_GET['postcalc_valuation'],$_GET['postcalc_country']);

// Если вернулась строка - это сообщение об ошибке.
if ( !is_array($arrResponse) ) {
    echo "<span class='postcalc_error'>Произошла ошибка:</span><br> $arrResponse";
    if ( count(error_get_last()) ){
        $arrError=error_get_last();
        echo "<br><span class='postcalc_error'>Ошибка PHP, строка $arrError[line] в файле $arrError[file]:</span><br> $arrError[message]";
    };
} else {
// Вернулся массив, Status=='OK'. 
// Откуда и Куда
echo "
    <table id='postcalc_from_to' >
    <tr>
        <td class='site_main_color'><b>Откуда:</b></td>
        <td>{$arrResponse['Откуда']['Индекс']}, {$arrResponse['Откуда']['Название']}
       <!--     <br> {$arrResponse['Откуда']['Адрес']} 
            <br> {$arrResponse['Откуда']['Телефон']}</td>-->
    </tr>
    <tr>
        <td class='site_main_color'><b>Куда:</b></td>
";
if ($_GET['postcalc_country']=='RU') {
echo "
        <td>{$arrResponse['Куда']['Индекс']}, {$arrResponse['Куда']['Название']}
            <br>{$arrResponse['Куда']['Адрес']} 
            <br>{$arrResponse['Куда']['Телефон']}</td>
    </tr>
    </table>
	
";
} else {
echo "
        <td>Международная доставка: {$arrPostcalcCountries[$_GET['postcalc_country']]}</td>
    </tr>
    </table>
";
}
// Выводим таблицу отправлений
echo "
<table id='postcalc_table' class='ui-widget-content ui-corner-all'>
<tr class='site_main_color1'><th>Название</th><th>Доставка</th><th>Сроки</th></tr>
";

// Выводим список тарифов
$counter=0;
foreach ( $arrResponse['Отправления'] as $parcel ) {
    if ($parcel['Доставка']) {
	echo "<tr";
        // Расцветка четных полос. 
        if ( $counter % 2 ) echo " class='site_main_color2'";
        echo "><td>$parcel[Название] </td><td>".number_format($parcel['Доставка'],2,',',' ')." руб.</td><td class='center'>$parcel[СрокДоставки]</td></tr>\n";
    }
    $counter++;
}
echo '</table>';

}

}
?>
*Стоимость упаковки не включена.
            </div>
        </body>
</html>