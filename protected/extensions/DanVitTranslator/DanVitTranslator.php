<?php

class DanVitTranslator extends CComponent
{

    public $translator_block_mode_enabled = false;
    public $translator_clear_text_in_params = false;
    public $translator_edit_url = '';
    public $editableByRole = false;
    private $_translatedText;
    public $memCacheTtl=7200;

    private $_success = false;

    public function init()
    {
        if (class_exists('DSConfig', false)) {
            $this->translator_block_mode_enabled = DSConfig::getVal('translator_block_mode_enabled') == 1;
        }
        if (class_exists('DSConfig', false)) {
            $this->editableByRole = Yii::app()->user->notInRole(array('guest', 'user'));
        }
    }

    public function translateBlock($translations)
    {
        $result = $translations;
        if (is_array($translations)) {
            foreach ($translations as $i => $translation) {
                $attributes = $this->getAttributes(html_entity_decode($translation[0], ENT_COMPAT, 'UTF-8'));
                if (!$attributes) {
                    continue;
                }
                $res = preg_match(
                  '/<translation.*?>(.*?)(?:<translate.*?\>.*?<\/translate\>)*<\/translation>/isu',
                  html_entity_decode($translation[0], ENT_COMPAT, 'UTF-8'),
                  $texts
                );
                if (!$res) {
                    continue;
                }
                $editable = isset($attributes['editable']) && ($attributes['editable'] == 1);
                $clear = isset($attributes['clear']) && ($attributes['clear'] == 1);
                $text = $texts[1];
//--------------------------
                if (isset($attributes['type'])) {
                    if ($attributes['type'] == 'parseCids') {
                        $textTranslated = $this->translateCid(
                          0,
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          false,
                          $editable,
                          $clear
                        ); //$queryRec->cid
                    } elseif ($attributes['type'] == 'parseQuery') {
                        $textTranslated = $this->translateQuery(
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          $editable
                        );
                    } elseif ($attributes['type'] == 'parseSuggestions') {
                        $textTranslated = $this->translateSuggestion(
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          false,
                          $editable,
                          $clear
                        );
                    } elseif ($attributes['type'] == 'item_attributes') {
                        $textTranslated = $this->translateAttrProp(
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          false,
                          $editable,
                          $clear
                        );
                    } elseif ($attributes['type'] == 'parseFilter') {
                        $textTranslated = $this->translateText(
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          false,
                          false,
                          true,
                          $editable,
                          $clear
                        );
                        //$this->translateProp($cid, $props[0], $queryRec->title, $from, $to, !$strong);
                        //$res = $this->translateVal($cid, $props[0], $props[1], $value->values_title, $from, $to, TRUE);
                    } elseif ($attributes['type'] == 'parseMultiFilter') {
                        $textTranslated = $this->translateText(
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          false,
                          false,
                          true,
                          $editable,
                          $clear
                        );
                        //$this->translateProp($cid, $props[0], $queryRec->title, $from, $to, !$strong);
                        //$res = $this->translateVal($cid, $props[0], $props[1], $value->values_title, $from, $to, TRUE);
                    } elseif ($attributes['type'] == 'prepareProps') {
                        $int_text = $text;
                        if (in_array($attributes['from'], array('zh-CHS', 'zh-CN'))) {
//---------- Очищаем значения свойств от мусора в скобках
//【比图片深点】
// （换码运费自理）
                            if ($this->translator_clear_text_in_params) {
                                $clear_text = trim(preg_replace('/【.*?】/u', '', $int_text));
                                if ($clear_text != '') {
                                    $int_text = $clear_text;
                                }
//(适合115-130斤）
                                $clear_text = trim(preg_replace('/[\(（].*?[）\)]/u', '', $int_text));
                                if ($clear_text != '') {
                                    $int_text = $clear_text;
                                }
                                $clear_text = trim(preg_replace('/".*?"/u', '', $int_text));
                                if ($clear_text != '') {
                                    $int_text = $clear_text;
                                }
                                if (preg_match("/^([0-9\.\-\sXLMSABCDEF]+)/su", $int_text, $matches)) {
                                    $int_text = $matches[1];
                                }
                            }
                            if (!preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $int_text)) {
                                $textTranslated = $int_text;
                            } else {
                                //public function translateText($query = '', $from = 'zh-CN', $to = 'ru', $strong = FALSE, $intOnly = FALSE, $extOutput = TRUE, $editable = FALSE,$clear=FALSE) {
                                $textTranslated = $this->translateText(
                                  $int_text,
                                  $attributes['from'],
                                  $attributes['to'],
                                  true,
                                  false,
                                  true,
                                  $editable,
                                  $clear
                                );
                            }

                        } else {
                            $textTranslated = $this->translateText(
                              $text,
                              $attributes['from'],
                              $attributes['to'],
                              false,
                              false,
                              true,
                              $editable,
                              $clear
                            );
                        }
                        //$this->translateProp($cid, $props[0], $queryRec->title, $from, $to, !$strong);
                        //$res = $this->translateVal($cid, $props[0], $props[1], $value->values_title, $from, $to, TRUE);
                    } elseif ($attributes['type'] == 'parseText') {
                        $textTranslated = $this->translateText(
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          false,
                          false,
                          true,
                          $editable,
                          $clear
                        );
                    } else {
                        $textTranslated = $this->translateText(
                          $text,
                          $attributes['from'],
                          $attributes['to'],
                          false,
                          false,
                          true,
                          $editable,
                          $clear
                        );
                    }
                } else {
                    $textTranslated = $this->translateText(
                      $text,
                      $attributes['from'],
                      $attributes['to'],
                      false,
                      false,
                      true,
                      $editable,
                      $clear
                    );
                }
//--------------------------
                $result[$i][1] = $textTranslated;
            }
        }
        return $result;
    }

    public function translateArray($queryArray, $type, $from = 'zh-CN', $to = 'ru', $strong = false)
    {
        // $type in parseCids, parseSuggestions, parseFilter, parseMultiFilter
        $wordArray = array();
//== Начали предварительный перевод и набивку массива для внешнего перевода =====
        foreach ($queryArray as $i => $queryRec) {
            if ($type == 'parseCids') {
                if (!$this->translator_block_mode_enabled) {
                    $res = $this->translateCid($queryRec->cid, $queryRec->title, $from, $to, !$strong);
                } else {
                    $this->_success = false;
                }
                if ($this->_success) {
                    $queryRec->title = $res;
                } else {
                    $wordArray[$i] = new stdClass();
                    $wordArray[$i]->{$from} = $queryRec->title;
                    $wordArray[$i]->{$to} = $queryRec->title;
                    $wordArray[$i]->tag = 'p';
                    $wordArray[$i]->from = $from;
                    $wordArray[$i]->to = $to;
                }
            } elseif ($type == 'parseSuggestions') {
                if (!$this->translator_block_mode_enabled) {
                    $res = $this->translateSuggestion($queryRec->title, $from, $to, !$strong);
                } else {
                    $this->_success = false;
                }
                if ($this->_success) {
                    $queryRec->title = $res;
                } else {
                    $wordArray[$i] = new stdClass();
                    $wordArray[$i]->{$from} = $queryRec->title;
                    $wordArray[$i]->{$to} = $queryRec->title;
                    $wordArray[$i]->tag = 'p';
                    $wordArray[$i]->from = $from;
                    $wordArray[$i]->to = $to;
                }
            } elseif ($type == 'parseQuery') {
                if (!$this->translator_block_mode_enabled) {
                    $res = $this->translateQuery($queryRec->title, $from, $to);
                } else {
                    $this->_success = false;
                }
                if ($this->_success) {
                    $queryRec->title = $res;
                } else {
                    $wordArray[$i] = new stdClass();
                    $wordArray[$i]->{$from} = $queryRec->title;
                    $wordArray[$i]->{$to} = $queryRec->title;
                    $wordArray[$i]->tag = 'p';
                    $wordArray[$i]->from = $from;
                    $wordArray[$i]->to = $to;
                }
            } elseif ($type == 'item_attributes') {
//== Translate prop ================================
                if (!$this->translator_block_mode_enabled) {
                    $res = $this->translateAttrProp($queryRec->prop, $from, $to, !$strong);
                } else {
                    $this->_success = false;
                }
                if ($this->_success) {
                    $queryRec->prop = $res;
                } else {
                    $wordArray[$i] = new stdClass();
                    $wordArray[$i]->{$from} = $queryRec->prop;
                    $wordArray[$i]->{$to} = $queryRec->prop;
                    $wordArray[$i]->tag = 'p';
                    $wordArray[$i]->from = $from;
                    $wordArray[$i]->to = $to;
                }
                if (!$this->translator_block_mode_enabled) {
                    $res = $this->translateAttrProp($queryRec->val, $from, $to, !$strong);
                } else {
                    $this->_success = false;
                }
                if ($this->_success) {
                    $queryRec->val = $res;
                } else {
                    $wordArray[1000 + $i] = new stdClass();
                    $wordArray[1000 + $i]->{$from} = $queryRec->val;
                    $wordArray[1000 + $i]->{$to} = $queryRec->val;
                    $wordArray[1000 + $i]->tag = 'p';
                    $wordArray[1000 + $i]->from = $from;
                    $wordArray[1000 + $i]->to = $to;
                }
            } elseif ($type == 'parseFilter') {
//== Translate title ================================
                if ((isset($queryRec->values)) && (isset($queryRec->values[0]->values_props))) {
                    $props = explode(':', $queryRec->values[0]->values_props);
                    if ((isset($props[0])) && (isset($queryRec->values)) && (isset($queryRec->values[0]->values_cid))) {
                        $cid = $queryRec->values[0]->values_cid;
                        if (!$this->translator_block_mode_enabled) {
                            $res = $this->translateProp($cid, $props[0], $queryRec->title, $from, $to, !$strong);
                        } else {
                            $this->_success = false;
                        }
                        if ($this->_success) {
                            $queryRec->title = $res;
                        } else {
                            $wordArray[$i] = new stdClass();
                            $wordArray[$i]->{$from} = $queryRec->title;
                            $wordArray[$i]->{$to} = $queryRec->title;
                            $wordArray[$i]->tag = 'p';
                            $wordArray[$i]->from = $from;
                            $wordArray[$i]->to = $to;
                        }
                        //== Translate value ================================
                        if (!isset($wordArray[$i])) {
                            $wordArray[$i] = new stdClass();
                        }
                        $wordArray[$i]->values = array();
                        foreach ($queryRec->values as $j => $value) {
                            if (isset($value->values_props)) {
                                $props = explode(':', $value->values_props);
                                if ((isset($props[0])) && (isset($props[1])) && (isset($value->values_cid))) {
                                    $cid = $value->values_cid;
                                    if (($props[0] != 20000) || true) {
                                        if (!$this->translator_block_mode_enabled) {
                                            $res = $this->translateVal(
                                              $cid,
                                              $props[0],
                                              $props[1],
                                              $value->values_title,
                                              $from,
                                              $to,
                                              true
                                            );
                                        } else {
                                            $this->_success = false;
                                        }
                                        if ($this->_success) {
                                            $value->values_title = $res;
                                        } else {
                                            $wordArray[$i]->values[$j] = new stdClass();
                                            $wordArray[$i]->values[$j]->{$from} = $value->values_title;
                                            $wordArray[$i]->values[$j]->{$to} = $value->values_title;
                                            $wordArray[$i]->values[$j]->tag = 'p';
                                            $wordArray[$i]->values[$j]->from = $from;
                                            $wordArray[$i]->values[$j]->to = $to;
                                        }
                                    } else {
                                        //TODO: Странно, но на английский здесь переводится в 10 раз медленнее
                                    }
                                }
                            }
                        }
                    }
                }
            } elseif ($type == 'parseMultiFilter') {
//== Translate title ================================
                if ((isset($queryRec->values)) && (isset($queryRec->values[0]->values_props))) {
                    $props = explode(':', $queryRec->values[0]->values_props);
                    if ((isset($props[0])) && (isset($queryRec->values)) && (isset($queryRec->values[0]->values_cid))) {
                        $cid = $queryRec->values[0]->values_cid;
                        if (!$this->translator_block_mode_enabled) {
                            $res = $this->translateProp($cid, $props[0], $queryRec->title, $from, $to, !$strong);
                        } else {
                            $this->_success = false;
                        }
                        if ($this->_success) {
                            $queryRec->title = $res;
                        } else {
                            $wordArray[$i] = new stdClass();
                            $wordArray[$i]->{$from} = $queryRec->title;
                            $wordArray[$i]->{$to} = $queryRec->title;
                            $wordArray[$i]->tag = 'p';
                            $wordArray[$i]->from = $from;
                            $wordArray[$i]->to = $to;
                        }
                        //== Translate value ================================
                        if (!isset($wordArray[$i])) {
                            $wordArray[$i] = new stdClass();
                        }
                        $wordArray[$i]->values = array();
                        foreach ($queryRec->values as $j => $value) {
                            if (isset($value->values_props)) {
                                $props = explode(':', $value->values_props);
                                if ((isset($props[0])) && (isset($props[1])) && (isset($value->values_cid))) {
                                    $cid = $value->values_cid;
                                    if (($props[0] != 20000) || true) {
                                        if (!$this->translator_block_mode_enabled) {
                                            $res = $this->translateVal(
                                              $cid,
                                              $props[0],
                                              $props[1],
                                              $value->values_title,
                                              $from,
                                              $to,
                                              true
                                            );
                                        } else {
                                            $this->_success = false;
                                        }
                                        if ($this->_success) {
                                            $value->values_title = $res;
                                        } else {
                                            $wordArray[$i]->values[$j] = new stdClass();
                                            $wordArray[$i]->values[$j]->{$from} = $value->values_title;
                                            $wordArray[$i]->values[$j]->{$to} = $value->values_title;
                                            $wordArray[$i]->values[$j]->tag = 'p';
                                            $wordArray[$i]->values[$j]->from = $from;
                                            $wordArray[$i]->values[$j]->to = $to;
                                        }
                                    } else {
                                        //TODO: Странно, но на английский здесь переводится в 10 раз медленнее
                                    }
                                }
                            }
                        }
                    }
                }
            } elseif ($type == 'prepareProps') {
                if ((isset($queryRec->childs)) && (isset($queryRec->cid)) && (isset($queryRec->childs[0]->vid))) {
                    $pid = $i;
                    $vid = $queryRec->childs[0]->vid;
                    $cid = $queryRec->cid;
                    if ((isset($pid)) && (isset($vid)) && (isset($cid))) {
                        if (!$this->translator_block_mode_enabled) {
                            $res = $this->translateProp($cid, $pid, $queryRec->name, $from, $to, !$strong); //
                        } else {
                            $this->_success = false;
                        }
                        if ($this->_success) {
                            $queryRec->name = $res;
                        } else {
                            $wordArray[$i] = new stdClass();
                            $wordArray[$i]->{$from} = $queryRec->name;
                            $wordArray[$i]->{$to} = $queryRec->name;
                            $wordArray[$i]->tag = 'p';
                            $wordArray[$i]->from = $from;
                            $wordArray[$i]->to = $to;
                        }
                        //== Translate value ================================
                        if (!isset($wordArray[$i])) {
                            $wordArray[$i] = new stdClass();
                        }
                        $wordArray[$i]->values = array();
                        foreach ($queryRec->childs as $j => $value) {
                            if (isset($value->vid)) {
                                $vid = $value->vid;
                                if ((isset($pid)) && (isset($vid)) && (isset($cid))) {
                                    if (($pid != 20000) || true) {
                                        if (!$this->translator_block_mode_enabled) {
                                            $res = $this->translateVal(
                                              $cid,
                                              $pid,
                                              $vid,
                                              $value->name,
                                              $from,
                                              $to,
                                              !$strong
                                            ); //
                                        } else {
                                            $this->_success = false;
                                        }
                                        if ($this->_success) {
                                            $value->name = $res;
                                        } else {
                                            $wordArray[$i]->values[$j] = new stdClass();
                                            $wordArray[$i]->values[$j]->{$from} = $value->name;
                                            $wordArray[$i]->values[$j]->{$to} = $value->name;
                                            $wordArray[$i]->values[$j]->tag = 'p';
                                            $wordArray[$i]->values[$j]->from = $from;
                                            $wordArray[$i]->values[$j]->to = $to;
                                        }
                                    } else {
                                        //TODO: Странно, но на английский здесь переводится в 10 раз медленнее
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                return;
            }
        }
//== Закончили предварительный перевод и набивку массива для внешнего перевода =====
        if ($type == 'parseCids') {
            if ((count($wordArray) > 0) && !$this->translator_block_mode_enabled) {
                $xmlQuery = '';
                foreach ($wordArray as $i => $word) {
                    if (isset($word->tag)) {
                        $xmlQuery = $xmlQuery . '<' . $word->tag . '>' . $word->{$from} . '</' . $word->tag . '>';
//            $xmlQuery = $xmlQuery .' '. $word->{$from};
                    }
                }
                $translatedQuery = Yii::app()->BingTranslator->translateText($xmlQuery, $from, $to, false);
                $res = preg_match_all('/<\s*[p]\s*>(.*?)<\s*\/\s*[p]\s*>/i', $translatedQuery, $translatedQueries);
                $j = 0;
                if ($res > 0) {
                    foreach ($wordArray as $i => $word) {
                        if (isset($translatedQueries[1][$j])) {
                            $wordArray[$i]->{$to} = $translatedQueries[1][$j];
                        }
                        $j = $j + 1;
                    }
                }
            }
//===================
            foreach ($wordArray as $i => $word) {
                if ((isset($wordArray[$i])) && (isset($wordArray[$i]->{$to}))) {
//+++++ New word +++++++++++++++++++++
                    $textCn = $wordArray[$i]->{$from};
                    if ($to == 'ru') {
                        $textRu = $wordArray[$i]->{$to};
                        $textEn = '';
                    } else {
                        $textEn = $wordArray[$i]->{$to};
                        $textRu = '';
                    }
                    $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                    $r = $this->markupTranslation(
                      $textCn,
                      $from,
                      $to,
                      $wordArray[$i]->{$to},
                      $type,
                      'plain',
                      $id,
                      $this->editableByRole
                    );
                    $queryArray[$i]->title = $r;
//++++++++++++++++++++++++++++++++++++
                }
            }
        } elseif ($type == 'parseSuggestions') {
            if ((count($wordArray) > 0) && !$this->translator_block_mode_enabled) {
                $xmlQuery = '';
                foreach ($wordArray as $i => $word) {
                    if (isset($word->tag)) {
                        $xmlQuery = $xmlQuery . '<' . $word->tag . '>' . $word->{$from} . '</' . $word->tag . '>';
//            $xmlQuery = $xmlQuery .' '. $word->{$from};
                    }
                }
                $translatedQuery = Yii::app()->BingTranslator->translateText($xmlQuery, $from, $to, false);
                $res = preg_match_all('/<\s*[p]\s*>(.*?)<\s*\/\s*[p]\s*>/i', $translatedQuery, $translatedQueries);
                $j = 0;
                if ($res > 0) {
                    foreach ($wordArray as $i => $word) {
                        if (isset($translatedQueries[1][$j])) {
                            $wordArray[$i]->{$to} = $translatedQueries[1][$j];
                        }
                        $j = $j + 1;
                    }
                }
            }
//===================
            foreach ($wordArray as $i => $word) {
                if ((isset($wordArray[$i])) && (isset($wordArray[$i]->{$to}))) {
//+++++ New word +++++++++++++++++++++
                    $textCn = $wordArray[$i]->{$from};
                    if ($to == 'ru') {
                        $textRu = $wordArray[$i]->{$to};
                        $textEn = '';
                    } else {
                        $textEn = $wordArray[$i]->{$to};
                        $textRu = '';
                    }
                    $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                    $r = $this->markupTranslation(
                      $textCn,
                      $from,
                      $to,
                      $wordArray[$i]->{$to},
                      $type,
                      'plain',
                      $id,
                      $this->editableByRole
                    );
                    $queryArray[$i]->title = $r;
//++++++++++++++++++++++++++++++++++++
                }
            }
        } elseif ($type == 'item_attributes') {
            if ((count($wordArray) > 0) && !$this->translator_block_mode_enabled) {
                $xmlQuery = '';
                foreach ($wordArray as $i => $word) {
                    if (isset($word->tag)) {
                        $xmlQuery = $xmlQuery . '<' . $word->tag . '>' . $word->{$from} . '</' . $word->tag . '>';
//            $xmlQuery = $xmlQuery .' '. $word->{$from};
                    }
                }
                $translatedQuery = Yii::app()->BingTranslator->translateText($xmlQuery, $from, $to, false);
                $res = preg_match_all('/<\s*[p]\s*>(.*?)<\s*\/\s*[p]\s*>/i', $translatedQuery, $translatedQueries);
                $j = 0;
                if ($res > 0) {
                    foreach ($wordArray as $i => $word) {
                        if (isset($translatedQueries[1][$j])) {
                            $wordArray[$i]->{$to} = $translatedQueries[1][$j];
                        }
                        $j = $j + 1;
                    }
                }
            }
//===================
            foreach ($wordArray as $i => $word) {
                if ((isset($wordArray[$i])) && (isset($wordArray[$i]->{$to}))) {
//+++++ New word +++++++++++++++++++++
                    $textCn = $wordArray[$i]->{$from};
                    if ($to == 'ru') {
                        $textRu = $wordArray[$i]->{$to};
                        $textEn = '';
                    } else {
                        $textEn = $wordArray[$i]->{$to};
                        $textRu = '';
                    }
                    $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                    $r = $this->markupTranslation(
                      $textCn,
                      $from,
                      $to,
                      $wordArray[$i]->{$to},
                      $type,
                      'plain',
                      $id,
                      $this->editableByRole
                    );
                    if ($i < 1000) {
                        $queryArray[$i]->prop = $r;
                    } else {
                        $queryArray[$i - 1000]->val = $r;
                    }
//++++++++++++++++++++++++++++++++++++
                }
            }
        } elseif ($type == 'parseFilter') {
            if ((count($wordArray) > 0) && !$this->translator_block_mode_enabled) {
                $xmlQuery = '';
                foreach ($wordArray as $i => $word) {
                    if (isset($word->tag)) {
                        $xmlQuery = $xmlQuery . '<' . $word->tag . '>' . $word->{$from} . '</' . $word->tag . '>';
                    }
                    if (isset($word->values)) {
                        foreach ($word->values as $wordValue) {
                            if (isset($wordValue->tag)) {
                                $xmlQuery = $xmlQuery . '<' . $wordValue->tag . '>' . $wordValue->{$from} . '</' . $wordValue->tag . '>';
                            }
                        }
                    }
                }
                if ($xmlQuery != '') {
                    $translatedQuery = Yii::app()->BingTranslator->translateText($xmlQuery, $from, $to, false);
//echo $xmlQuery;
                    $res = preg_match_all('/<\s*[p]\s*>(.*?)<\s*\/\s*[p]\s*>/i', $translatedQuery, $translatedQueries);
                    $j = 0;
                    if ($res > 0) {
                        foreach ($wordArray as $i => $word) {
                            if (isset($word->{$to})) {
                                if (isset($translatedQueries[1][$j])) {
                                    $wordArray[$i]->{$to} = $translatedQueries[1][$j];
                                }
                                $j = $j + 1;
                            }
                            if (isset($word->values)) {
                                foreach ($word->values as $k => $value) {
                                    if (isset($value->{$to})) {
                                        if (isset($translatedQueries[1][$j])) {
                                            $wordArray[$i]->values[$k]->{$to} = $translatedQueries[1][$j];
                                        }
                                        $j = $j + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
//===================
            foreach ($wordArray as $i => $word) {
                if ((isset($wordArray[$i])) && (isset($wordArray[$i]->{$to}))) {
//+++++ New word +++++++++++++++++++++
                    $textCn = $wordArray[$i]->{$from};
                    if ($to == 'ru') {
                        $textRu = $wordArray[$i]->{$to};
                        $textEn = '';
                    } else {
                        $textEn = $wordArray[$i]->{$to};
                        $textRu = '';
                    }
                    $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                    $r = $this->markupTranslation(
                      $textCn,
                      $from,
                      $to,
                      $wordArray[$i]->{$to},
                      $type,
                      'plain',
                      $id,
                      $this->editableByRole
                    );
                    $queryArray[$i]->title = $r;
//++++++++++++++++++++++++++++++++++++
                }
                if (isset($word->values)) {
                    foreach ($word->values as $k => $value) {
                        $textRu = '';
                        $textEn = '';
                        //if ((isset($word->values[$k])) && (isset($word->values[$k]->{$to}))) {
                        if ((isset($value)) && (isset($value->{$to}))) {
//+++++ New word +++++++++++++++++++++
                            //$textCn=$word->values[$k]->{$from};
                            $textCn = $value->{$from};
                            if ($to == 'ru') {
                                $textRu = $value->{$to};
                                $textEn = '';
                            } else {
                                $textEn = $value->{$to};
                                $textRu = '';
                            }
                            $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                            $r = $this->markupTranslation(
                              $textCn,
                              $from,
                              $to,
                              $value->{$to},
                              $type,
                              'plain',
                              $id,
                              $this->editableByRole
                            );
                            $queryArray[$i]->values[$k]->values_title = $r;
//++++++++++++++++++++++++++++++++++++
                        }
                    }
                }
            }
        } elseif ($type == 'parseMultiFilter') {
            if ((count($wordArray) > 0) && !$this->translator_block_mode_enabled) {
                $xmlQuery = '';
                foreach ($wordArray as $i => $word) {
                    if (isset($word->tag)) {
                        $xmlQuery = $xmlQuery . '<' . $word->tag . '>' . $word->{$from} . '</' . $word->tag . '>';
                    }
                    if (isset($word->values)) {
                        foreach ($word->values as $wordValue) {
                            if (isset($wordValue->tag)) {
                                $xmlQuery = $xmlQuery . '<' . $wordValue->tag . '>' . $wordValue->{$from} . '</' . $wordValue->tag . '>';
                            }
                        }
                    }
                }
                if ($xmlQuery != '') {
                    $translatedQuery = Yii::app()->BingTranslator->translateText($xmlQuery, $from, $to, false);
//echo $xmlQuery;
                    $res = preg_match_all('/<\s*[p]\s*>(.*?)<\s*\/\s*[p]\s*>/i', $translatedQuery, $translatedQueries);
                    $j = 0;
                    if ($res > 0) {
                        foreach ($wordArray as $i => $word) {
                            if (isset($word->{$to})) {
                                if (isset($translatedQueries[1][$j])) {
                                    $wordArray[$i]->{$to} = $translatedQueries[1][$j];
                                }
                                $j = $j + 1;
                            }
                            if (isset($word->values)) {
                                foreach ($word->values as $k => $value) {
                                    if (isset($value->{$to})) {
                                        if (isset($translatedQueries[1][$j])) {
                                            $wordArray[$i]->values[$k]->{$to} = $translatedQueries[1][$j];
                                        }
                                        $j = $j + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
//===================
            foreach ($wordArray as $i => $word) {
                if ((isset($wordArray[$i])) && (isset($wordArray[$i]->{$to}))) {
//+++++ New word +++++++++++++++++++++
                    $textCn = $wordArray[$i]->{$from};
                    if ($to == 'ru') {
                        $textRu = $wordArray[$i]->{$to};
                        $textEn = '';
                    } else {
                        $textEn = $wordArray[$i]->{$to};
                        $textRu = '';
                    }
                    $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                    $r = $this->markupTranslation(
                      $textCn,
                      $from,
                      $to,
                      $wordArray[$i]->{$to},
                      $type,
                      'plain',
                      $id,
                      $this->editableByRole
                    );
                    $queryArray[$i]->title = $r;
//++++++++++++++++++++++++++++++++++++
                }
                if (isset($word->values)) {
                    foreach ($word->values as $k => $value) {
                        if ((isset($value)) && (isset($value->{$to}))) {
                            $textRu = '';
                            $textEn = '';
//+++++ New word +++++++++++++++++++++
                            $textCn = $word->values[$k]->{$from};
                            if ($to == 'ru') {
                                $textRu = $value->{$to};
                                $textEn = '';
                            } else {
                                $textEn = $value->{$to};
                                $textRu = '';
                            }
                            $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                            $r = $this->markupTranslation(
                              $textCn,
                              $from,
                              $to,
                              $value->{$to},
                              $type,
                              'plain',
                              $id,
                              $this->editableByRole
                            );
                            $queryArray[$i]->values[$k]->values_title = $r;
//++++++++++++++++++++++++++++++++++++
                        }
                    }
                }
            }
        } elseif ($type == 'prepareProps') {
            if ((count($wordArray) > 0) && !$this->translator_block_mode_enabled) {
                $xmlQuery = '';
                foreach ($wordArray as $i => $word) {
                    if (isset($word->tag)) {
                        $xmlQuery = $xmlQuery . '<' . $word->tag . '>' . $word->{$from} . '</' . $word->tag . '>';
                    }
                    if (isset($word->values)) {
                        foreach ($word->values as $wordValue) {
                            if (isset($wordValue->tag)) {
                                $xmlQuery = $xmlQuery . '<' . $wordValue->tag . '>' . $wordValue->{$from} . '</' . $wordValue->tag . '>';
                            }
                        }
                    }
                }
                if ($xmlQuery != '') {
                    $translatedQuery = Yii::app()->BingTranslator->translateText($xmlQuery, $from, $to, false);
//echo $xmlQuery;
                    $res = preg_match_all('/<\s*[p]\s*>(.*?)<\s*\/\s*[p]\s*>/i', $translatedQuery, $translatedQueries);
                    $j = 0;
                    if ($res > 0) {
                        foreach ($wordArray as $i => $word) {
                            if (isset($word->{$to})) {
                                if (isset($translatedQueries[1][$j])) {
                                    $wordArray[$i]->{$to} = $translatedQueries[1][$j];
                                }
                                $j = $j + 1;
                            }
                            if (isset($word->values)) {
                                foreach ($word->values as $k => $value) {
                                    if (isset($value->{$to})) {
                                        if (isset($translatedQueries[1][$j])) {
                                            $wordArray[$i]->values[$k]->{$to} = $translatedQueries[1][$j];
                                        }
                                        $j = $j + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
//===================
            foreach ($wordArray as $i => $word) {
                if ((isset($wordArray[$i])) && (isset($wordArray[$i]->{$to}))) {
//+++++ New word +++++++++++++++++++++
                    $textCn = $wordArray[$i]->{$from};
                    if ($to == 'ru') {
                        $textRu = $wordArray[$i]->{$to};
                        $textEn = '';
                    } else {
                        $textEn = $wordArray[$i]->{$to};
                        $textRu = '';
                    }
                    $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                    $r = $this->markupTranslation(
                      $textCn,
                      $from,
                      $to,
                      $wordArray[$i]->{$to},
                      $type,
                      'plain',
                      $id,
                      $this->editableByRole
                    );
                    $queryArray[$i]->name = $r;
//++++++++++++++++++++++++++++++++++++
                }
                if (isset($word->values)) {
                    foreach ($word->values as $k => $value) {
                        if ((isset($value)) && (isset($value->{$to}))) {
//+++++ New word +++++++++++++++++++++
                            $textEn = '';
                            $textRu = '';
                            $textCn = $value->{$from};
                            if ($to == 'ru') {
                                $textRu = $value->{$to};
                                $textEn = '';
                            } else {
                                $textEn = $value->{$to};
                                $textRu = '';
                            }
                            $id = $this->saveTranslation($from, $textCn, $textRu, $textEn);
                            $r = $this->markupTranslation(
                              $textCn,
                              $from,
                              $to,
                              $value->{$to},
                              $type,
                              'plain',
                              $id,
                              $this->editableByRole
                            );
                            $queryArray[$i]->childs[$k]->name = $r;
//++++++++++++++++++++++++++++++++++++
                        }
                    }
                }
            }
        } else {
            return;
        }
    }

    public function translateText(
      $query = '',
      $from = 'zh-CN',
      $to = 'ru',
      $strong = false,
      $intOnly = false,
      $extOutput = true,
      $editable = false,
      $clear = false
    ) {
        $cahceTag='translateText-'.$query.$from.$to.($strong?'true':'false').($intOnly?'true':'false').
          ($extOutput?'true':'false').($editable?'true':'false').($clear?'true':'false');
        $cache=false;
        if (isset(Yii::app()->memCache)) $cache = Yii::app()->memCache->get($cahceTag);
        if ($cache!==false) {
            $this->_success = true;
            return $cache;
        }
        $translationType = 'plain';
        $rowID = 0;
        $res = '';
        try {
            $this->_success = false;
            if ($query == '' || $to == '' || $from == '') {
                return false;
            } else {
                $query = trim($query);
                if (substr($from, 0, 2) == substr($to, 0, 2)) {
                    return $query;
                }
                if (!$this->translator_block_mode_enabled) {
                    //zh-CHS
                    try {
                        if (in_array($from,array('zh-CHS','zh-CN'))) {

                            if (!preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $query)) {
                                $res = $query;
                            } else {
                                $command = Yii::app()->db->createCommand(
                                  "
                                   select rr.id, rr.TEXT_RU, rr.TEXT_EN from (select dd.id, dd.TEXT_RU, dd.TEXT_EN from cn_dic dd
                                       where TEXT_ZH_LENGTH=CHAR_LENGTH(:query) and dd.TEXT_ZH = :query
                                     order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_ZH_LENGTH), dd.FREQ desc) rr LIMIT 1
                                     "
                                )->bindParam(":query", $query, PDO::PARAM_STR);
                                $row = $command->queryRow();
                                /* if (!$strong && (!$row['TEXT_RU'])) {
                                    $command = Yii::app()->db->createCommand(
                                      "
                                       select rr.id, rr.TEXT_RU, rr.TEXT_EN from (select dd.id, dd.TEXT_RU, dd.TEXT_EN from cn_dic dd
                                           where MATCH (dd.TEXT_ZH) AGAINST (:query)
                                         order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_ZH_LENGTH), dd.FREQ desc) rr LIMIT 1
                                         "
                                    )->bindParam(":query", $query, PDO::PARAM_STR);
                                    $row = $command->queryRow();
                                } */
                                if ($row) {
                                    if ($to == 'ru') {
//                if (($row['TEXT_RU']) && (strlen($row['TEXT_RU']) > 0)) {
                                        $res = $row['TEXT_RU'];
//                } else {
//                  $res = $row['TEXT_EN'];
//                }
                                    } else {
                                        $res = $row['TEXT_EN'];
                                    }
                                }
                            }
                        } elseif ($from == 'ru') {
                            $command = Yii::app()->db->createCommand(
                              "
                                            select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                   where dd.TEXT_RU=:query
                                 order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_RU_LENGTH), dd.FREQ desc) rr LIMIT 1
                                 "
                            )->bindParam(":query", $query, PDO::PARAM_STR);
                            $row = $command->queryRow();
                            /* if (!$strong && (!$row['TEXT_ZH'])) {
                                $command = Yii::app()->db->createCommand(
                                  "
                                                select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                       where MATCH (dd.TEXT_RU) AGAINST (:query)
                                     order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_RU_LENGTH), dd.FREQ desc) rr LIMIT 1
                                     "
                                )->bindParam(":query", $query, PDO::PARAM_STR);
                                $row = $command->queryRow();
                            } */
                            if ($row) {
                                $res = $row['TEXT_ZH'];
                            }
                        } elseif ($from == 'en') {
                            $command = Yii::app()->db->createCommand(
                              "
                                            select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                   where dd.TEXT_EN=:query
                                 order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_EN_LENGTH), dd.FREQ desc) rr LIMIT 1
                                 "
                            )->bindParam(":query", $query, PDO::PARAM_STR);
                            $row = $command->queryRow();
                            /* if (!$strong && (!$row['TEXT_ZH'])) {
                                $command = Yii::app()->db->createCommand(
                                  "
                                                select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                       where MATCH (dd.TEXT_EN) AGAINST (:query)
                                     order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_EN_LENGTH), dd.FREQ desc) rr LIMIT 1
                                     "
                                )->bindParam(":query", $query, PDO::PARAM_STR);
                                $row = $command->queryRow();
                            } */
                            if ($row) {
                                $res = $row['TEXT_ZH'];
                            }
                        }
                        if ((isset($row) && $row['id'])) {
                            $rowID = $row['id'];
                        } else {
                            $rowID = 0;
                        }
                    } catch (Exception $e) {
                        $res = ""; //.$query;
                    }
//            if (strlen($res) > 0 && $res) {
//                $res = $res."*";
//            }

                    if (((strlen($res) == 0 || !$res || ($res == null))) && (!$intOnly)) {
                        $res = Yii::app()->BingTranslator->translateText($query, $from, $to); //$ps[$j]
//========= Cache translation =============================
                        if (in_array($from, array('en', 'en-US'))) {
                            $resEn = $query;
                            if ((in_array($to, array('ru', 'ru-RU')))) {
                                $resRu = $res;
                                $resCn = $this->translateText($query, $from, 'zh-CN', false, true, false);
                                if ($resCn == $query) {
                                    $resCn = '';
                                }
//              $resCn = Yii::app()->BingTranslator->translateText($query, $from, 'zh-CN');
                                //допереводим на китайский
                            } else {
                                $resRu = $this->translateText($query, $from, 'ru', false, true, false);
                                if ($resRu == $query) {
                                    $resRu = '';
                                }
//              $resRu = Yii::app()->BingTranslator->translateText($query, $from, 'ru');
                                $resCn = $res;
                                //допереводим на русский
                            }
                        } elseif (in_array($from, array('ru', 'ru-RU'))) {
                            $resRu = $query;
                            if ((in_array($to, array('en', 'en-US')))) {
                                $resCn = $this->translateText($query, $from, 'zh-CN', false, true, false);
                                if ($resCn == $query) {
                                    $resCn = '';
                                }
//              $resCn = Yii::app()->BingTranslator->translateText($query, $from, 'zh-CN');
                                $resEn = $res;
                                //допереводим на китайский
                            } else {
                                $resCn = $res;
                                $resEn = $this->translateText($query, $from, 'en', false, true, false);
                                if ($resEn == $query) {
                                    $resEn = '';
                                }
//              $resEn = Yii::app()->BingTranslator->translateText($query, $from, 'en');
                                //допереводим на английский
                            }
                        } else {
                            $resCn = $query;
                            if ((in_array($to, array('en', 'en-US')))) {
                                $resRu = $this->translateText($query, $from, 'ru', false, true, false);
                                if ($resRu == $query) {
                                    $resRu = '';
                                }
//              $resRu = Yii::app()->BingTranslator->translateText($query, $from, 'ru');
                                $resEn = $res;
                                //допереводим на русский
                            } else {
                                $resRu = $res;
                                $resEn = $this->translateText($query, $from, 'en', false, true, false);
                                if ($resEn == $query) {
                                    $resEn = '';
                                }
//              $resEn = Yii::app()->BingTranslator->translateText($query, $from, 'en');
                                //допереводим на английский
                            }
                        }

                        if (((in_array($to, array('ru', 'ru-RU'))) && (strlen($resRu) != 0 && $resRu != false)) ||
                          ((in_array($to, array('en', 'en-US'))) && (strlen($resEn) != 0 && $resEn != false)) ||
                          ((in_array($to, array('zh-CHS', 'zh-CN'))) && (strlen($resCn) != 0 && $resCn != false))
                        ) {
//          if (!(((strlen($resRu) == 0 || !$resRu)) || ((strlen($resEn) == 0 || !$resEn)) || ((strlen($resCn) == 0 || !$resCn)))) {
                            $lastRecID = $this->saveTranslation($from, $resCn, $resRu, $resEn);
                            if ($lastRecID > 0) {
                                $rowID = $lastRecID;
                            }
                        }
//=========================================================
                    }
//===== finalBlock ======
                    if (strlen($res) == 0 || !$res) {
                        $this->_translatedText = $query;
                        $this->_success = false;
                        return $this->_translatedText;
                    } else {
                        $this->_translatedText = trim($res);
                        $this->_success = true;
                        if ($extOutput) {
                            $result= $this->markupTranslation(
                              $query,
                              $from,
                              $to,
                              $this->_translatedText,
                              'parseText',
                              $translationType,
                              $rowID,
                              $editable || $this->editableByRole,
                              $clear
                            );
                        } else {
                            $result = $this->_translatedText;
                        }
                        if (isset(Yii::app()->memCache) && $rowID && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
                        return $result;
                    }
                } else {
                    //============================== block mode
                    if (in_array($from,array('zh-CHS','zh-CN'))) {

                        if (!preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $query)) {
                            $this->_success = true;
                            return $query;
                        }
                    }
                    $this->_success = false;
                    if ($extOutput) {
                        $result=$this->markupTranslation(
                          $query,
                          $from,
                          $to,
                          $query,
                          'parseText',
                          $translationType,
                          $rowID,
                          $editable || $this->editableByRole,
                          $clear
                        );
                    } else {
                        $result= $query;
                    }
                    if (isset(Yii::app()->memCache) && $rowID && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
                    return $result;
                    //=========================================
                }
            }
        } catch (Exception $e) {
            $this->_translatedText = $query;
            $this->_success = false;
            return $this->_translatedText;
        }
    }

    public function translateLocal($query = '', $from = 'zh-CN', $to = 'ru', $strong = false, $intOnly = true)
    {
        $cahceTag='translateLocal'.$query.$from.$to.($strong?'true':'false').($intOnly?'true':'false');
        $cache=false;
        if (isset(Yii::app()->memCache)) $cache = Yii::app()->memCache->get($cahceTag);
        if ($cache!==false) {
            $this->_success = true;
            return $cache;
        }


        $translationType = 'plain';
        $rowID = 0;
        $res = '';
        try {
            $this->_success = false;
            if ($query == '' || $to == '' || $from == '') {
                return false;
            } else {
                $query = trim($query);
                //zh-CHS
                try {
                    if (in_array($from,array('zh-CHS','zh-CN'))) {

                        if (!preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $query)) {
                            $res = $query;
                        } else {
                            $command = Yii::app()->db->createCommand(
                              "
                               select rr.id, rr.TEXT_RU, rr.TEXT_EN from (select dd.id, dd.TEXT_RU, dd.TEXT_EN from cn_dic dd
                                   where TEXT_ZH_LENGTH=CHAR_LENGTH(:query) and dd.TEXT_ZH = :query
                                 order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_ZH_LENGTH), dd.FREQ desc) rr LIMIT 1
                                 "
                            )->bindParam(":query", $query, PDO::PARAM_STR);
                            $row = $command->queryRow();
                            /* if (!$strong && (!$row['TEXT_RU'])) {
                                $command = Yii::app()->db->createCommand(
                                  "
                                   select rr.id, rr.TEXT_RU, rr.TEXT_EN from (select dd.id, dd.TEXT_RU, dd.TEXT_EN from cn_dic dd
                                       where MATCH (dd.TEXT_ZH) AGAINST (:query)
                                     order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_ZH_LENGTH), dd.FREQ desc) rr LIMIT 1
                                     "
                                )->bindParam(":query", $query, PDO::PARAM_STR);
                                $row = $command->queryRow();
                            } */
                            if ($row) {
                                if ($to == 'ru') {
//                if (($row['TEXT_RU']) && (strlen($row['TEXT_RU']) > 0)) {
                                    $res = $row['TEXT_RU'];
//                } else {
//                  $res = $row['TEXT_EN'];
//                }
                                } else {
                                    $res = $row['TEXT_EN'];
                                }
                            }
                        }
                    } elseif ($from == 'ru') {
                        $command = Yii::app()->db->createCommand(
                          "
                                        select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                               where dd.TEXT_RU=:query
                             order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_RU_LENGTH), dd.FREQ desc) rr LIMIT 1
                             "
                        )->bindParam(":query", $query, PDO::PARAM_STR);
                        $row = $command->queryRow();
                        /* if (!$strong && (!$row['TEXT_ZH'])) {
                            $command = Yii::app()->db->createCommand(
                              "
                                            select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                   where MATCH (dd.TEXT_RU) AGAINST (:query)
                                 order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_RU_LENGTH), dd.FREQ desc) rr LIMIT 1
                                 "
                            )->bindParam(":query", $query, PDO::PARAM_STR);
                            $row = $command->queryRow();
                        } */
                        if ($row) {
                            $res = $row['TEXT_ZH'];
                        }
                    } elseif ($from == 'en') {
                        $command = Yii::app()->db->createCommand(
                          "
                                        select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                               where dd.TEXT_EN=:query
                             order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_EN_LENGTH), dd.FREQ desc) rr LIMIT 1
                             "
                        )->bindParam(":query", $query, PDO::PARAM_STR);
                        $row = $command->queryRow();
                        /* if (!$strong && (!$row['TEXT_ZH'])) {
                            $command = Yii::app()->db->createCommand(
                              "
                                            select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                   where MATCH (dd.TEXT_EN) AGAINST (:query)
                                 order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_EN_LENGTH), dd.FREQ desc) rr LIMIT 1
                                 "
                            )->bindParam(":query", $query, PDO::PARAM_STR);
                            $row = $command->queryRow();
                        } */
                        if ($row) {
                            $res = $row['TEXT_ZH'];
                        }
                    }
                    if ((isset($row) && $row['id'])) {
                        $rowID = $row['id'];
                    } else {
                        $rowID = 0;
                    }
                } catch (Exception $e) {
                    $res = ""; //.$query;
                }
//            if (strlen($res) > 0 && $res) {
//                $res = $res."*";
//            }
                if (in_array($from,array('zh-CHS','zh-CN'))) {
                    if (preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $res)) {
                        $res = false;
                    }
                }
                if (((strlen($res) == 0 || !$res || ($res == null))) && (!$intOnly)) {
                    $res = Yii::app()->BingTranslator->translateText($query, $from, $to); //$ps[$j]
//========= Cache translation =============================
                    if (in_array($from, array('en', 'en-US'))) {
                        $resEn = $query;
                        if ((in_array($to, array('ru', 'ru-RU')))) {
                            $resRu = $res;
                            $resCn = $this->translateText($query, $from, 'zh-CN', false, true, false);
                            if ($resCn == $query) {
                                $resCn = '';
                            }
//              $resCn = Yii::app()->BingTranslator->translateText($query, $from, 'zh-CN');
                            //допереводим на китайский
                        } else {
                            $resRu = $this->translateText($query, $from, 'ru', false, true, false);
                            if ($resRu == $query) {
                                $resRu = '';
                            }
//              $resRu = Yii::app()->BingTranslator->translateText($query, $from, 'ru');
                            $resCn = $res;
                            //допереводим на русский
                        }
                    } elseif (in_array($from, array('ru', 'ru-RU'))) {
                        $resRu = $query;
                        if ((in_array($to, array('en', 'en-US')))) {
                            $resCn = $this->translateText($query, $from, 'zh-CN', false, true, false);
                            if ($resCn == $query) {
                                $resCn = '';
                            }
//              $resCn = Yii::app()->BingTranslator->translateText($query, $from, 'zh-CN');
                            $resEn = $res;
                            //допереводим на китайский
                        } else {
                            $resCn = $res;
                            $resEn = $this->translateText($query, $from, 'en', false, true, false);
                            if ($resEn == $query) {
                                $resEn = '';
                            }
//              $resEn = Yii::app()->BingTranslator->translateText($query, $from, 'en');
                            //допереводим на английский
                        }
                    } else {
                        $resCn = $query;
                        if ((in_array($to, array('en', 'en-US')))) {
                            $resEn = $res;
                            $resRu = $this->translateText($query, $from, 'ru', false, true, false);
                            if ($resRu == $query) {
                                $resRu = '';
                            }
//              $resRu = Yii::app()->BingTranslator->translateText($query, $from, 'ru');
                            //допереводим на русский
                        } else {
                            $resRu = $res;
                            $resEn = $this->translateText($query, $from, 'en', false, true, false);
                            if ($resEn == $query) {
                                $resEn = '';
                            }
//              $resEn = Yii::app()->BingTranslator->translateText($query, $from, 'en');
                            //допереводим на английский
                        }
                    }

                    if (((in_array($to, array('ru', 'ru-RU'))) && (strlen($resRu) != 0 && $resRu != false)) ||
                      ((in_array($to, array('en', 'en-US'))) && (strlen($resEn) != 0 && $resEn != false)) ||
                      ((in_array($to, array('zh-CHS', 'zh-CN'))) && (strlen($resCn) != 0 && $resCn != false))
                    ) {
//          if (!(((strlen($resRu) == 0 || !$resRu)) || ((strlen($resEn) == 0 || !$resEn)) || ((strlen($resCn) == 0 || !$resCn)))) {
                        $lastRecID = $this->saveTranslation($from, $resCn, $resRu, $resEn);
                        if ($lastRecID > 0) {
                            $rowID = $lastRecID;
                        }
                    }
//=========================================================
                }
//===== finalBlock ======
                if (strlen($res) == 0 || !$res) {
                    $this->_translatedText = $query;
                    $this->_success = false;
                    return $this->_translatedText;
                } else {
                    $this->_translatedText = trim($res);
                    $this->_success = true;
                    $result=$this->_translatedText;
                    if (isset(Yii::app()->memCache) && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
                    return $result;
                }
            }
        } catch (Exception $e) {
            $this->_translatedText = $query;
            $this->_success = false;
            return $this->_translatedText;
        }
    }

    public function translateMessage($message, $from, $to)
    {
        $cahceTag='translateMessage-'.$message.$from.$to;
        $cache=false;
        if (isset(Yii::app()->memCache)) $cache = Yii::app()->memCache->get($cahceTag);
        if ($cache!==false) {
            $this->_success = true;
            return $cache;
        }
        $res = '';
        try {
            $this->_success = false;
            $res = Yii::app()->BingTranslator->translateText($message, $from, ($to == 'zh') ? 'zh-CHS' : $to); //$ps[$j]
//===== finalBlock ======
            if (strlen($res) == 0 || !$res) {
                $this->_translatedText = '';
                $this->_success = false;
                return $this->_translatedText;
            } else {
                $this->_translatedText = $res;
                $this->_success = true;
                $result=$this->_translatedText;
                if (isset(Yii::app()->memCache) && $result!=$message) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
                return $result;
            }
        } catch (Exception $e) {
            $this->_translatedText = '';
            $this->_success = false;
            return $this->_translatedText;
        }
    }

    public function translateQuery($query = '', $from = 'zh-CN', $to = 'ru', $editable = false)
    {

        $cahceTag='translateQuery-'.$query.$from.$to.($editable?'true':'false');
        $cache=false;
        if (isset(Yii::app()->memCache)) $cache = Yii::app()->memCache->get($cahceTag);
        if ($cache!==false) {
            $this->_success = true;
            return $cache;
        }

        $translationType = 'plain';
        $rowID = 0;
        $res = '';
        $preservedQuery = '';
        try {
            $this->_success = false;
            if ($query == '' || $to == '' || $from == '') {
                return false;
            } else {

                if (!$this->translator_block_mode_enabled) {
                    // Сохраняем английские слова, такие, как названия брендов
                    if (in_array($to, array('zh','zh-CHS','zh-CN')) && (in_array($from,array('ru','he')))) {
                        $words = preg_split('/\s/s',$query,-1,PREG_SPLIT_NO_EMPTY);
                        $query='';
                        foreach ($words as $word) {
                            if (preg_match('/^[a-z0-9\-\(\)\%\$\#\@\!\.\*\+]+$/is',$word)) {
                                $preservedQuery=$preservedQuery.' '.$word;
                            } else {
                                $query=$query.' '.$word;
                            }
                        }
                    }
                    $query = trim($query);
                    $preservedQuery = trim($preservedQuery);
                    //zh-CHS
                    try {
                        if (in_array($from,array('zh-CHS','zh-CN'))) {

                            if (!preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $query)) {
                                $res = $query;
                            } else {
                                $command = Yii::app()->db->createCommand(
                                  "
                                   select rr.id, rr.TEXT_RU, rr.TEXT_EN from (select dd.id, dd.TEXT_RU, dd.TEXT_EN from cn_dic dd
                                       where TEXT_ZH_LENGTH=CHAR_LENGTH(:query) and dd.TEXT_ZH = :query
                                     order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_ZH_LENGTH), dd.FREQ desc) rr LIMIT 1
                                     "
                                )->bindParam(":query", $query, PDO::PARAM_STR);
                                $row = $command->queryRow();
                                if ($row) {
                                    if ($to == 'ru') {
//                if (($row['TEXT_RU']) && (strlen($row['TEXT_RU']) > 0)) {
                                        $res = $row['TEXT_RU'];
//                } else {
//                  $res = $row['TEXT_EN'];
//                }
                                    } else {
                                        $res = $row['TEXT_EN'];
                                    }
                                }
                            }
                        } elseif ($from == 'ru') {
                            $command = Yii::app()->db->createCommand(
                              "
                                            select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                   where dd.TEXT_RU=:query
                                 order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_RU_LENGTH), dd.FREQ desc) rr LIMIT 1
                                 "
                            )->bindParam(":query", $query, PDO::PARAM_STR);
                            $row = $command->queryRow();
                            if ($row) {
                                $res = $row['TEXT_ZH'];
                            }
                        } elseif ($from == 'en') {
                            $command = Yii::app()->db->createCommand(
                              "
                                            select rr.id, UPPER(rr.TEXT_ZH) as TEXT_ZH  from (select dd.id, dd.TEXT_ZH from cn_dic dd
                                   where dd.TEXT_EN=:query
                                 order by dd.TRANSLATION_STATE desc, ABS(CHAR_LENGTH(:query)- dd.TEXT_EN_LENGTH), dd.FREQ desc) rr LIMIT 1
                                 "
                            )->bindParam(":query", $query, PDO::PARAM_STR);
                            $row = $command->queryRow();
                            if ($row) {
                                $res = $row['TEXT_ZH'];
                            }
                        }
                        if ((isset($row) && $row['id'])) {
                            $rowID = $row['id'];
                        } else {
                            $rowID = 0;
                        }
                    } catch (Exception $e) {
                        $res = ""; //.$query;
                    }
//            if (strlen($res) > 0 && $res) {
//                $res = $res."*";
//            }

                    if (((strlen($res) == 0 || !$res || ($res == null)))) {
                        $res = Yii::app()->BingTranslator->translateText($query, $from, $to); //$ps[$j]
//========= Cache translation =============================
                        if (in_array($from, array('en', 'en-US'))) {
                            $resEn = $query;
                            if ((in_array($to, array('ru', 'ru-RU')))) {
                                $resRu = $res;
                                $resCn = $this->translateText($query, $from, 'zh-CN', false, true, false);
                                if ($resCn == $query) {
                                    $resCn = 'false';
                                }
//              $resCn = Yii::app()->BingTranslator->translateText($query, $from, 'zh-CN');
                                //допереводим на китайский
                            } else {
                                $resRu = $this->translateText($query, $from, 'ru', false, true, false);
                                if ($resRu == $query) {
                                    $resRu = 'false';
                                }
//              $resRu = Yii::app()->BingTranslator->translateText($query, $from, 'ru');
                                $resCn = $res;
                                //допереводим на русский
                            }
                        } elseif (in_array($from, array('ru', 'ru-RU'))) {
                            $resRu = $query;
                            if ((in_array($to, array('en', 'en-US')))) {
                                $resCn = $this->translateText($query, $from, 'zh-CN', false, true, false);
                                if ($resCn == $query) {
                                    $resCn = 'false';
                                }
//              $resCn = Yii::app()->BingTranslator->translateText($query, $from, 'zh-CN');
                                $resEn = $res;
                                //допереводим на китайский
                            } else {
                                $resCn = $res;
                                $resEn = $this->translateText($query, $from, 'en', false, true, false);
                                if ($resEn == $query) {
                                    $resEn = 'false';
                                }
//              $resEn = Yii::app()->BingTranslator->translateText($query, $from, 'en');
                                //допереводим на английский
                            }
                        } else {
                            $resCn = $query;
                            if ((in_array($to, array('en', 'en-US')))) {
                                $resEn = $res;
                                $resRu = $this->translateText($query, $from, 'ru', false, true, false);
                                if ($resRu == $query) {
                                    $resRu = 'false';
                                }
//              $resRu = Yii::app()->BingTranslator->translateText($query, $from, 'ru');
                                //допереводим на русский
                            } else {
                                $resRu = $res;
                                $resEn = $this->translateText($query, $from, 'en', false, true, false);
                                if ($resEn == $query) {
                                    $resEn = 'false';
                                }
//              $resEn = Yii::app()->BingTranslator->translateText($query, $from, 'en');
                                //допереводим на английский
                            }
                        }

                        if (((in_array($to, array('ru', 'ru-RU'))) && (strlen($resRu) != 0 && $resRu != false)) ||
                          ((in_array($to, array('en', 'en-US'))) && (strlen($resEn) != 0 && $resEn != false)) ||
                          ((in_array($to, array('zh-CHS', 'zh-CN'))) && (strlen($resCn) != 0 && $resCn != false))
                        ) {
//          if (!(((strlen($resRu) == 0 || !$resRu)) || ((strlen($resEn) == 0 || !$resEn)) || ((strlen($resCn) == 0 || !$resCn)))) {
                            $lastRecID = $this->saveTranslation($from, $resCn, $resRu, $resEn);
                            if ($lastRecID > 0) {
                                $rowID = $lastRecID;
                            }
                        }
//=========================================================
                    }
//===== finalBlock ======
                    if (strlen($res) == 0 || !$res) {
                        $this->_translatedText = trim($query.' '.$preservedQuery);
                        $this->_success = false;
                        return $this->_translatedText;
                    } else {
                        $this->_translatedText = trim($res.' '.$preservedQuery);
                        $this->_success = true;
                        $result= $this->markupTranslation(
                          $query,
                          $from,
                          $to,
                          $this->_translatedText,
                          'parseQuery',
                          $translationType,
                          $rowID,
                          $editable || $this->editableByRole
                        );
                        if (isset(Yii::app()->memCache) && $rowID  && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
                        return $result;
                    }
                } else {
                    //============================== block mode - immediate remote translation ===================================
                    $this->_success = false;
                    $remoteQuery = array(
                      array(
                        0 => $this->markupTranslation(
                            $query,
                            $from,
                            $to,
                            $query,
                            'parseQuery',
                            $translationType,
                            $rowID,
                            $editable || $this->editableByRole
                          ),
                      ),
                    );
                    $res = Utils::getRemoteTranslation($remoteQuery);
// ==== end of post and get data to translator ====================
                    if (($res->data) && ($res->info['http_code'] < 400)) {
                        $translations = unserialize($res->data);
                        if (isset($translations[0][1])) {
                            $result = $translations[0][1];
                        } else {
                            $result = $remoteQuery[0][0];
                        }
                    } else {
                        $result = $remoteQuery[0][0];
                    }
                    if (isset(Yii::app()->memCache) && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
                    return $result;
//            return $this->removeOnlineTranslation($result);
                    //============================================================================================================
                }
            }
        } catch (Exception $e) {
            $this->_translatedText = $query;
            $this->_success = false;
            return $this->_translatedText;
        }
    }

    private function translateCid(
      $cid = 0,
      $query = '',
      $from = 'zh-CN',
      $to = 'ru',
      $intOnly = false,
      $editable = false,
      $clear = false
    ) {
//        $cid=0;
        $cahceTag='translateCid-'.$cid.'-'.$query.$from.$to.($intOnly?'true':'false').
          ($editable?'true':'false').($clear?'true':'false');
        $cache=false;
        if (isset(Yii::app()->memCache)) $cache = Yii::app()->memCache->get($cahceTag);
        if ($cache!==false) {
            $this->_success = true;
            return $cache;
        }

        $translationType = 'cid';
        $rowID = 0;
        if (!$cid) {
            $cid = 0;
        }
        $this->_success = false;
        $res = '';
        //zh-CHS
        if ($cid != 0) {
            try {
//=================================
                if (in_array($from,array('zh-CHS','zh-CN'))) {
                    $translationType = 'cid_ext';
                    $command = Yii::app()->db->createCommand(
                      "
                       select cc.id, cc.RU, cc.EN from categories_ext cc
                      where cc.CID=" . $cid . " limit 1"
                    );
                    $row = $command->queryRow();
                    if ($to == 'ru') {
                        if (($row['RU']) && (strlen($row['RU']) > 0)) {
                            $res = $row['RU'];
                        } else {
                            $res = $row['EN'];
                        }
                    } else {
                        $res = $row['EN'];
                    }
//--------------------------
                    if (strlen($res) == 0 || !$res) {
                        $translationType = 'cid';
                        $command = Yii::app()->db->createCommand(
                          "
                           select cc.id, cc.RU, cc.EN from categories cc
                          where cc.CID=" . $cid . " limit 1"
                        );
                        $row = $command->queryRow();
                        if ($to == 'ru') {
                            if (($row['RU']) && (strlen($row['RU']) > 0)) {
                                $res = $row['RU'];
                            } else {
                                $res = $row['EN'];
                            }
                        } else {
                            $res = $row['EN'];
                        }
                    }
//--------------------------
                } //=================================
                elseif ($from == 'ru') {
                    $translationType = 'cid_ext';
                    $command = Yii::app()->db->createCommand(
                      "
                       select cc.id, cc.ZH from categories_ext cc
                      where cc.CID=" . $cid . "  limit 1"
                    );
                    $row = $command->queryRow();
                    $res = $row['ZH'];
//--------------------------
                    if (strlen($res) == 0 || !$res) {
                        $translationType = 'cid';
                        $command = Yii::app()->db->createCommand(
                          "
                           select cc.id, cc.ZH from categories cc
                          where cc.CID=" . $cid . "  limit 1"
                        );
                        $row = $command->queryRow();
                        $res = $row['ZH'];
                    }
//--------------------------
                } //=================================
                elseif ($from == 'en') {
                    $translationType = 'cid_ext';
                    $command = Yii::app()->db->createCommand(
                      "
                          select cc.id, cc.ZH from categories_ext cc
                            where  cc.CID=" . $cid . " limit 1"
                    );
                    $row = $command->queryRow();
                    $res = $row['ZH'];
//--------------------------
                    if (strlen($res) == 0 || !$res) {
                        $translationType = 'cid';
                        $command = Yii::app()->db->createCommand(
                          "
                              select cc.id, cc.ZH from categories cc
                                where  cc.CID=" . $cid . " limit 1"
                        );
                        $row = $command->queryRow();
                        $res = $row['ZH'];
                    }
//--------------------------
                }
                if ((isset($row) && $row['id'])) {
                    $rowID = $row['id'];
                } else {
                    $rowID = 0;
                }
            } catch (Exception $e) {
                return $this->translateText($query, $from, $to, false, $intOnly);
            }
        }
//=================================
        if (strlen($res) == 0 || !$res) {
            return $this->translateText($query, $from, $to, false, $intOnly);
        }
        if (strlen($res) == 0 || !$res) {
            $this->_translatedText = $query;
            $this->_success = false;
            return $this->_translatedText;
        } else {
            $this->_translatedText = trim($res);
            $this->_success = true;
            $result = $this->markupTranslation(
              $query,
              $from,
              $to,
              $this->_translatedText,
              'parseCids',
              $translationType,
              $rowID,
              $editable || $this->editableByRole,
              $clear
            );
            if (isset(Yii::app()->memCache) && $rowID  && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
            return $result;
        }
    }

    private function translateSuggestion(
      $query,
      $from = 'zh-CN',
      $to = 'ru',
      $intOnly = false,
      $editable = false,
      $clear = false
    ) {
        $translationType = 'suggestion';
        return $this->translateText($query, $from, $to, false, $intOnly, true, $editable, $clear);
        /*
            if (strlen($res) == 0 || !$res) {
              $this->_translatedText = $query;
              $this->_success = FALSE;
              return $this->_translatedText;
            }
            else {
              $this->_translatedText = $res;
              $this->_success = TRUE;
              return $this->markupTranslation($query, $from, $to, $this->_translatedText, $translationType, 0);
            }
        */
    }

    private function translateAttrProp(
      $query,
      $from = 'zh-CN',
      $to = 'ru',
      $intOnly = false,
      $editable = false,
      $clear = false
    ) {
        $translationType = 'suggestion';
        return $this->translateText($query, $from, $to, false, $intOnly, true, $editable, $clear);
        /*
            if (strlen($res) == 0 || !$res) {
              $this->_translatedText = $query;
              $this->_success = FALSE;
              return $this->_translatedText;
            }
            else {
              $this->_translatedText = $res;
              $this->_success = TRUE;
              return $this->markupTranslation($query, $from, $to, $this->_translatedText, $translationType, 0);
            }
        */
    }

    private function translateVal(
      $cid = 0,
      $pid = 0,
      $vid = 0,
      $query = '',
      $from = 'zh-CN',
      $to = 'ru',
      $intOnly = false,
      $editable = false,
      $clear = false
    ) {
        $cahceTag='translateVal-'.$cid.'-'.$pid.'-'.$vid.'-'.$query.$from.$to.($intOnly?'true':'false').
          ($editable?'true':'false').($clear?'true':'false');
        $cache=false;
        if (isset(Yii::app()->memCache)) $cache = Yii::app()->memCache->get($cahceTag);
        if ($cache!==false) {
            $this->_success = true;
            return $cache;
        }

        $translationType = 'vid';
        $rowID = 0;
        if (!$cid) {
            $cid = 0;
        }
        $this->_success = false;
        $res = '';
        //zh-CHS
        $int_query = $query;
        try {
            if (in_array($from,array('zh-CHS','zh-CN'))) {
//---------- Очищаем значения свойств от мусора в скобках
//【比图片深点】
// （换码运费自理）
                $clear_query = trim(preg_replace('/【.*?】/u', '', $int_query));
                if ($clear_query != '') {
                    $int_query = $clear_query;
                }
//(适合115-130斤）
                $clear_query = trim(preg_replace('/[\(（].*?[）\)]/u', '', $int_query));
                if ($clear_query != '') {
                    $int_query = $clear_query;
                }
                $clear_query = trim(preg_replace('/".*?"/u', '', $int_query));
                if ($clear_query != '') {
                    $int_query = $clear_query;
                }
                if (!preg_match("/[\x{4E00}-\x{9FA5}]{1}/u", $int_query)) {
                    $res = $int_query;
                } else {
                    $command = Yii::app()->db->createCommand(
                      "
                       select vv.id, vv.ZH, vv.RU, vv.EN from categories_props_vals vv
                      where (" . $cid . "=0 or vv.CID=" . $cid . ") and
      vv.PID=" . $pid . " and vv.VID=" . $vid . " and (vv.ZH=TRIM(:query) or :query is null)
      limit 1
   "
                    )->bindParam(":query", $int_query, PDO::PARAM_STR);
                    $row = $command->queryRow();
                    if ($row) {
                        if ($to == 'ru') {
                            if (($row['RU']) && (strlen($row['RU']) > 0)) {
                                $res = $row['RU'];
                            } else {
                                $res = $row['EN'];
                            }
                        } else {
                            $res = $row['EN'];
                        }
                    }
                }
            } elseif ($from == 'ru') {
                $command = Yii::app()->db->createCommand(
                  "
                   select vv.id, vv.ZH from categories_props_vals vv
                  where (" . $cid . "=0 or vv.CID=" . $cid . ") and
   vv.PID=" . $pid . " and vv.VID=" . $vid . " limit 1
   "
                );
                $row = $command->queryRow();
                if ($row) {
                    $res = $row['ZH'];
                }
            } elseif ($from == 'en') {
                $command = Yii::app()->db->createCommand(
                  "
                   select vv.id, vv.ZH from categories_props_vals vv
                  where (" . $cid . "=0 or vv.CID=" . $cid . ") and
   vv.PID=" . $pid . " and vv.VID=" . $vid . " limit 1
   "
                );
                $row = $command->queryRow();
                if ($row) {
                    $res = $row['ZH'];
                }
            }
            if (isset($row) && $row['id']) {
                $rowID = $row['id'];
            } else {
                $rowID = 0;
            }
        } catch (Exception $e) {
            return $this->translateText($int_query, $from, $to, true, $intOnly, true, $editable, $clear);
        }
        if (strlen($res) == 0 || !$res) {
            return $this->translateText($int_query, $from, $to, true, $intOnly, true, $editable, $clear);
        }
        if (strlen($res) == 0 || !$res) {
            $this->_translatedText = $int_query;
            $this->_success = false;
            return $this->_translatedText;
        } else {
            $this->_translatedText = trim($res);
            $this->_success = true;
            $result = $this->markupTranslation(
              $int_query,
              $from,
              $to,
              $this->_translatedText,
              'parseFilter',
              $translationType,
              $rowID,
              $editable || $this->editableByRole,
              $clear
            );
            if (isset(Yii::app()->memCache) && $rowID  && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
            return $result;
        }
    }

    private function translateProp(
      $cid = 0,
      $pid = 0,
      $query = '',
      $from = 'zh-CN',
      $to = 'ru',
      $intOnly = false,
      $editable = false,
      $clear = false
    ) {
        $cahceTag='translateProp-'.$cid.'-'.$pid.'-'.$query.$from.$to.($intOnly?'true':'false').
          ($editable?'true':'false').($clear?'true':'false');
        $cache=false;
        if (isset(Yii::app()->memCache)) $cache = Yii::app()->memCache->get($cahceTag);
        if ($cache!==false) {
            $this->_success = true;
            return $cache;
        }

//        $cid=0;
        $translationType = 'pid';
        $rowID = 0;
        if (!$cid) {
            $cid = 0;
        }
        $this->_success = false;
        //zh-CHS
        try {
            if (in_array($from,array('zh-CHS','zh-CN'))) {
                $command = Yii::app()->db->createCommand(
                  "
                   select pp.id, pp.RU,pp.EN from categories_props pp
                  where (" . $cid . "=0 or pp.CID=" . $cid . ") and
    pp.PID=" . $pid . " limit 1
   "
                );
                $row = $command->queryRow();
                if ($to == 'ru') {
                    if (($row['RU']) && (strlen($row['RU']) > 0)) {
                        $res = $row['RU'];
                    } else {
                        $res = $row['EN'];
                    }
                } else {
                    $res = $row['EN'];
                }
            } elseif ($from == 'ru') {
                $command = Yii::app()->db->createCommand(
                  "
                   select pp.id, pp.ZH from categories_props pp
                  where (" . $cid . "=0 or pp.CID=" . $cid . ") and
    pp.PID=" . $pid . " limit 1
   "
                );
                $row = $command->queryRow();
                $res = $row['ZH'];
            } elseif ($from == 'en') {
                $command = Yii::app()->db->createCommand(
                  "
                      select pp.id, pp.ZH from categories_props pp
                        where (" . $cid . "=0 or pp.CID=" . $cid . ") and
         pp.PID=" . $pid . " limit 1
   "
                );
                $row = $command->queryRow();
                $res = $row['ZH'];
            }
            if ((isset($row) && $row['id'])) {
                $rowID = $row['id'];
            } else {
                $rowID = 0;
            }
        } catch (Exception $e) {
            return $this->translateText($query, $from, $to, false, $intOnly, true, $editable, $clear);
        }
        if (isset($res) && strlen($res) == 0 || (isset($res) && !$res) || !isset($res)) {
            return $this->translateText($query, $from, $to, false, $intOnly, true, $editable, $clear);
        }
        if (isset($res) && strlen($res) == 0 || (isset($res) && !$res) || !isset($res)) {
            $this->_translatedText = $query;
            $this->_success = false;
            return $this->_translatedText;
        } else {
            if (!isset($res)) {
                $res = $query;
            }
            $this->_translatedText = trim($res);
            $this->_success = true;
            $result= $this->markupTranslation(
              $query,
              $from,
              $to,
              $this->_translatedText,
              'prepareProps',
              $translationType,
              $rowID,
              $editable || $this->editableByRole,
              $clear
            );
            if (isset(Yii::app()->memCache) && $rowID  && $result!=$query) Yii::app()->memCache->set($cahceTag,$result,$this->memCacheTtl);
            return $result;
        }
    }

    private function saveTranslation($srcLang, $textCn, $textRu, $textEn)
    {
        $found = true;
        $res = 0;
        if ($this->translator_block_mode_enabled || (!$this->_success)) {
            return $res;
        }
        $textCn = trim($textCn);
        $textRu = trim($textRu);
        $textEn = trim($textEn);

        if (in_array($srcLang, array('en-US', 'en'))) {
            if ((($textEn == $textRu) || ($textEn == $textCn)) || (($textRu == '') && ($textCn == ''))) {
                return $res;
            }
            $command = Yii::app()->db->createCommand(
              "select TEXT_ZH as zh, TEXT_RU as ru, TEXT_EN as en from cn_dic where TEXT_EN=:text LIMIT 1"
            )
              ->bindParam(':text', $textEn, PDO::PARAM_STR);
            $row = $command->queryRow();
            if ($row != false) {
                $command = Yii::app()->db->createCommand(
                  "update cn_dic set FREQ=FREQ+1,
                          TEXT_RU = IF((:ru is null) or (:ru=''),TEXT_RU,:ru),
                          TEXT_ZH = IF((:zh is null) or (:zh=''),TEXT_ZH,:zh),
                          TEXT_RU_LENGTH = CHAR_LENGTH(IF((:ru is null) or (:ru=''),TEXT_RU,:ru)),
                          TEXT_ZH_LENGTH = CHAR_LENGTH(IF((:zh is null) or (:zh=''),TEXT_ZH,:zh))
                           where TEXT_EN=:text"
                )
                  ->bindParam(':text', $textEn, PDO::PARAM_STR)
                  ->bindParam(':ru', $textRu, PDO::PARAM_STR)
                  ->bindParam(':zh', $textCn, PDO::PARAM_STR);
                $command->execute();
                $found = true;
            } else {
                $found = false;
            }
        } elseif (in_array($srcLang, array('ru-RU', 'ru'))) {
            if ((($textRu == $textEn) || ($textRu == $textCn)) || (($textEn == '') && ($textCn == ''))) {
                return $res;
            }
            $command = Yii::app()->db->createCommand(
              "select TEXT_ZH as zh, TEXT_RU as ru, TEXT_EN as en from cn_dic where TEXT_RU=:text LIMIT 1"
            )
              ->bindParam(':text', $textRu, PDO::PARAM_STR);
            $row = $command->queryRow();
            if ($row != false) {
                $command = Yii::app()->db->createCommand(
                  "update cn_dic set FREQ=FREQ+1,
                          TEXT_EN = IF((:en is null) or (:en=''),TEXT_EN,:en),
                          TEXT_ZH = IF((:zh is null) or (:zh=''),TEXT_ZH,:zh),
                          TEXT_EN_LENGTH = CHAR_LENGTH(IF((:en is null) or (:en=''),TEXT_EN,:en)),
                          TEXT_ZH_LENGTH = CHAR_LENGTH(IF((:zh is null) or (:zh=''),TEXT_ZH,:zh))
                           where TEXT_RU=:text"
                )
                  ->bindParam(':text', $textRu, PDO::PARAM_STR)
                  ->bindParam(':en', $textEn, PDO::PARAM_STR)
                  ->bindParam(':zh', $textCn, PDO::PARAM_STR);
                $command->execute();
                $found = true;
            } else {
                $found = false;
            }
        } else {
            if ((($textCn == $textEn) || ($textCn == $textRu)) || (($textEn == '') && ($textRu == ''))) {
                return $res;
            }
            $command = Yii::app()->db->createCommand(
              "select TEXT_ZH as zh, TEXT_RU as ru, TEXT_EN as en from cn_dic where TEXT_ZH_LENGTH=CHAR_LENGTH(:text) and TEXT_ZH=:text LIMIT 1"
            )
              ->bindParam(':text', $textCn, PDO::PARAM_STR);
            $row = $command->queryRow();
            if ($row != false) {
                $command = Yii::app()->db->createCommand(
                  "update cn_dic set FREQ=FREQ+1,
                          TEXT_EN = IF((:en is null) or (:en=''),TEXT_EN,:en),
                          TEXT_RU = IF((:ru is null) or (:ru=''),TEXT_RU,:ru),
                          TEXT_EN_LENGTH = CHAR_LENGTH(IF((:en is null) or (:en=''),TEXT_EN,:en)),
                          TEXT_RU_LENGTH = CHAR_LENGTH(IF((:ru is null) or (:ru=''),TEXT_RU,:ru))
                          where TEXT_ZH=:text"
                )
                  ->bindParam(':text', $textCn, PDO::PARAM_STR)
                  ->bindParam(':en', $textEn, PDO::PARAM_STR)
                  ->bindParam(':ru', $textRu, PDO::PARAM_STR);
                $command->execute();
                $found = true;
            } else {
                $found = false;
            }
        }
        if (!$found) {
            $FREQ = 1;
            $TRANSLATION_STATE = 0; //insert delayed
            $command = Yii::app()->db->createCommand(
              "insert into cn_dic (
SRC_LANG,FREQ,TRANSLATION_STATE,TEXT_ZH,TEXT_RU,TEXT_EN,TEXT_RU_LENGTH,TEXT_EN_LENGTH,TEXT_ZH_LENGTH)
                      values (
:SRC_LANG,:FREQ,:TRANSLATION_STATE,:TEXT_ZH,:TEXT_RU, :TEXT_EN, CHAR_LENGTH(:TEXT_RU),CHAR_LENGTH(:TEXT_EN),CHAR_LENGTH(:TEXT_ZH))"
            )
              ->bindParam(':SRC_LANG', $srcLang, PDO::PARAM_STR)
              ->bindParam(':FREQ', $FREQ, PDO::PARAM_INT)
              ->bindParam(':TRANSLATION_STATE', $TRANSLATION_STATE, PDO::PARAM_INT)
              ->bindParam(':TEXT_ZH', $textCn, PDO::PARAM_STR)
              ->bindParam(':TEXT_RU', $textRu, PDO::PARAM_STR)
              ->bindParam(':TEXT_EN', $textEn, PDO::PARAM_STR);
            $command->execute();
            $res = Yii::app()->db->getLastInsertID();
            if (!$res) {
                $res = Yii::app()->db->createCommand(
                  "select ifnull(id,0) from cn_dic
                                    where SRC_LANG=:SRC_LANG and TEXT_ZH=:TEXT_ZH and TEXT_RU=:TEXT_RU and TEXT_EN=:TEXT_EN"
                )
                  ->bindParam(':SRC_LANG', $srcLang, PDO::PARAM_STR)
                  ->bindParam(':TEXT_ZH', $textCn, PDO::PARAM_STR)
                  ->bindParam(':TEXT_RU', $textRu, PDO::PARAM_STR)
                  ->bindParam(':TEXT_EN', $textEn, PDO::PARAM_STR);
            }
        }
        return $res;
    }

    private function getAttributes($translation)
    {
        $res = preg_match('/<translation\s.*?>/isu', $translation, $attributesSection);
        if (!$res) {
            return false;
        }
        $res = preg_match_all('/\s([\w\d_-]+)=["](.+?)["]/isu', $attributesSection[0], $searchAttrinbutes);
        if (!$res) {
            return false;
        }
        $attributes = array();
        foreach ($searchAttrinbutes[1] as $j => $attribute) {
            $attributes[$attribute] = $searchAttrinbutes[2][$j];
        }
        if (!isset($attributes['from'])) {
            $attributes['from']='zh-CHS';
        }
        if (!isset($attributes['to'])) {
            $attributes['to']='ru';
        }
        return $attributes;
    }

    private function attributesToString($attribures)
    {
        $res = ' ';
        foreach ($attribures as $name => $value) {
            $res = $res . $name . '="' . $value . '"' . ' ';
        }
        return $res;
    }

    private function removeOnlineTranslation($s)
    {
        $res = preg_replace(
          '/\<translation.*?\>(.*)(?:<translate.*?\>.*?<\/translate\>)*\<\/translation\>/is',
          '$1',
          $s
        );
        $res = preg_replace('/\<span.*\>(.*)\<\/span\>/is', '$1', $res);
        $res = trim($res);
        return $res;
    }

    public function markupTranslation(
      $query,
      $from,
      $to,
      $translatedText,
      $translationType,
      $correctionType,
      $id,
      $editable = false,
      $clear = false,
      $extAttributes = array()
    ) {
        //==============================================================
        if (substr($from, 0, 2) == substr($to, 0, 2)) {
            return $query;
        }
        if (class_exists('DSConfig', false)) {
            $uid = Yii::app()->user->id;
        } else {
            $uid = -1;
        }
        if (class_exists('DSConfig', false)) {
            $this->translator_edit_url = DSConfig::getVal('translator_edit_url');
        }
        if (preg_match('/[<>]+|span/is', $translatedText) > 0) {
            $res = $this->removeOnlineTranslation($translatedText);
        } else {
            $res = $translatedText;
        }
//    if (in_array($to, array('zh-CHS', 'zh-CN', 'cn'))) {
//      return $res;
//    }
        $translated = ($this->_success) ? '1' : '0'; //$query!=$res
        if ($this->editableByRole || $editable) { // && ($id > 0)
            $title = htmlentities($correctionType . '[' . $id . ']' . ': ' . $query, ENT_QUOTES, 'UTF-8');
            $translateAttributes = array(
//        'href'       => 'javascript:void(0);',
              'onclick' => 'editTranslation(event,this.parentNode,\'' . $correctionType . '\',\'' . $id . '\'); stopPropagation(event); return false;',
            );
            $attribures = array(
//        'style'=> 'float:left;height:15px;',
              'editable'   => 1,
              'translated' => $translated,
              'url'        => $this->translator_edit_url,
              'type'       => $translationType,
              'from'       => $from,
              'to'         => $to,
              'uid'        => $uid,
              'id'         => $correctionType . $id,
              'title'      => $title,
            );
        } else {
            $title = htmlentities($query, ENT_QUOTES, 'UTF-8');
            $attribures = array(
              'editable'   => 0,
              'translated' => $translated,
//        'url'=>$url,
              'type'       => $translationType,
              'from'       => $from,
              'to'         => $to,
              'uid'        => $uid,
              'id'         => $correctionType . $id,
              'title'      => $title,
//        'href'=>'javascript:void(0);',
//        'onclick'=>'editTranslation(event,this,\'' . $correctionType . '\',\'' . $id . '\'); stopPropagation(event); return false;',
            );
        }
        $attribures = array_merge($attribures, $extAttributes);
        if (isset($translateAttributes)) {
            $translate = '<translate ' . $this->attributesToString(
                $translateAttributes
              ) . '><span class="ui-icon ui-icon-pencil"></span></translate>';
        } else {
            $translate = '';
        }
        if ($clear && $translated) {
            return $res;
        } else {
            $res = '<translation' . $this->attributesToString($attribures) . '>' . $res . $translate . '</translation>';
            $res = preg_replace("/[\r\n]+/", ' ', $res);
            return $res;
        }
        //==============================================================
    }
    public function deleteFromMemCache(){
/*
$cahceTag='translateText-'.$query.$from.$to.($strong?'true':'false').($intOnly?'true':'false').($extOutput?'true':'false').($editable?'true':'false').($clear?'true':'false');
$cahceTag='translateLocal'.$query.$from.$to.($strong?'true':'false').($intOnly?'true':'false');
$cahceTag='translateMessage-'.$message.$from.$to;
$cahceTag='translateQuery-'.$query.$from.$to.($editable?'true':'false');
$cahceTag='translateCid-'.$cid.'-'.$query.$from.$to.($intOnly?'true':'false').($editable?'true':'false').($clear?'true':'false');
$cahceTag='translateVal-'.$cid.'-'.$pid.'-'.$vid.'-'.$query.$from.$to.($intOnly?'true':'false').($editable?'true':'false').($clear?'true':'false');
$cahceTag='translateProp-'.$cid.'-'.$pid.'-'.$query.$from.$to.($intOnly?'true':'false').($editable?'true':'false').($clear?'true':'false');
*/
    }

}