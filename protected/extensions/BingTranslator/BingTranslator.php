<?php

class BingTranslator extends CComponent {

  private $_bingTranslateBaseUrl = 'http://api.microsofttranslator.com/';
  private $_bingTranslateBaseUrl_azure = 'https://api.datamarket.azure.com/Bing/MicrosoftTranslator/v1/Translate';
  private $_translatedText;
  private $_success = FALSE;
  private $source = '';
  private static $_curlObject = FALSE;
  private static $_keysExpired = FALSE;
  private static $_KeyTranslator = FALSE;

  function init() {

  }

  private function logTranslation($key, $query, $result) {

    $i_key = $key;
    $i_query = mb_strlen($query, 'UTF-8');
//    $i_crc =md5($query);
//    $i_squery = $query;
    if ($result) {
      $i_result = 1;
    }
    else {
      $i_result = 0;
    }
    try {
      $command = Yii::app()->db->createCommand("insert into log_translator_keys (`date`,`keyid`,`result`,`chars`,`function`)
values (Now(), (select tk.id from translator_keys tk where tk.`key`=:key limit 1),
        :result,:chars,null)")
        ->bindParam(':key', $i_key, PDO::PARAM_STR)
        ->bindParam(':result', $i_result, PDO::PARAM_INT)
        ->bindParam(':chars', $i_query, PDO::PARAM_INT);
      $command->execute();
      return TRUE;
    } catch (Exception $e) {
      return FALSE;
    }
  }

  /**
   * Возвращает ресурс curl c базовыми настройкми
   **/
  private static function getCurl($secure = FALSE) {
    try {
      if (gettype(self::$_curlObject) != 'resource') {
        $ch = curl_init();
        self::$_curlObject = $ch;
      }
      else {
        $ch = self::$_curlObject;
      }

      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:25.0) Gecko/20100101 Firefox/25.0');
      curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
//      curl_setopt($ch, CURLOPT_HEADER, TRUE); // Ото ж выводить ли хэдер в контент
      curl_setopt($ch, CURLOPT_ENCODING, ''); //gzip,deflate
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 7);
      curl_setopt($ch, CURLOPT_FORBID_REUSE, FALSE);
      $header = array();
      $header[] = "Connection: keep-alive";
      $header[] = "Keep-Alive: 30";
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      return $ch;
    } catch (Exception $e) {
      throw new CHttpException (500, Yii::t('main', 'System error: curl not installed or inited.'));
    }
  }

  /**
   * Translate the given text
   * @param string $text text to translate
   * @param string $from language to translate to
   * @param string $to language to translate from
   * @return boolean | string
   */
  public function translateTextEx($key, $type, $query = '', $from = 'zh-CHS', $to = '', $plainRes = FALSE) {
    self::$_KeyTranslator = new stdClass();
    self::$_KeyTranslator->type = $type;
    self::$_KeyTranslator->appId = $key;
    return $this->translateText($query, $from, $to, $plainRes);
  }

  public function translateText($query = '', $from = 'zh-CHS', $to = '', $plainRes = FALSE) {
    $this->_success = FALSE;
    $this->source = $query;
    if ($query == '' || $to == '' || $from == '') {
      return FALSE;
    }
    else {
      if (self::$_KeyTranslator == FALSE) {
        self::$_KeyTranslator = self::getKeyTranslator();
      }
      $KeyTranslator = self::$_KeyTranslator;
      if ($KeyTranslator == FALSE) {
        $this->_success = FALSE;
        if (class_exists('Profiler', FALSE)) {
          Profiler::message('BingV2', 'no keys available');
        }
        return FALSE;
      }
      if (self::$_keysExpired) {
        $this->_success = FALSE;
        if (class_exists('Profiler', FALSE)) {
          Profiler::message('BingV2', 'error or key expired');
        }
        return FALSE;
      }
      try {
        if ($KeyTranslator->type == 'BingV1') {
//====================================================================
          $url = "V2/Http.svc/Translate?appId=" . $KeyTranslator->appId . "&from=" . $from .
            "&to=" . $to . "&text=" . urlencode($query);
//            echo $url; die;

          try {
            $ch = self::getCurl(FALSE);
          } catch (Exception $e) {
            throw new CHttpException (500, Yii::t('main', 'System error: curl not installed or inited.'));
          }
          curl_setopt($ch, CURLOPT_URL, $this->_bingTranslateBaseUrl . $url);
          curl_setopt($ch, CURLOPT_REFERER, $this->_bingTranslateBaseUrl);

          $contents = curl_exec($ch);
          if (curl_errno($ch)>0) {
            throw new CHttpException(500, curl_error($ch));
          }
          if (!empty($contents)) {
            $xmlData = @simplexml_load_string($contents);
            if ($xmlData->body->h1 == "Argument Exception") {
              $this->_success = FALSE;
              $this->logTranslation($KeyTranslator->appId, $url, FALSE);
              if (class_exists('Profiler', FALSE)) {
                Profiler::message('BingV1', 'error');
              }
              return FALSE;
            }
            else {
              if ($plainRes) {
                $this->_translatedText = $contents;
                $this->_success = TRUE;
                $this->logTranslation($KeyTranslator->appId, $url, TRUE);
                return $this->_translatedText;
              }
              $this->_translatedText = (string) $xmlData;
              $this->_success = TRUE;
              $this->logTranslation($KeyTranslator->appId, $url, TRUE);
              $this->saveTranslation($from, ((in_array($to,array('zh-CHS','zh-CN','zh')))?$this->_translatedText:''),
                ((in_array($to,array('ru')))?$this->_translatedText:''),
                ((in_array($to,array('en')))?$this->_translatedText:''));
              return $this->_translatedText;
            }
          }
          else {
            throw new CHttpException(500, '(1) Error communcating with Bing Translate.');
          }
        }
        elseif ($KeyTranslator->type == 'BingV2') {
//====================================================================
          //$texts = json_encode($query);
          $langs = array(
            'en-US'  => 'en',
            'en'     => 'en',
            'ru-RU'  => 'ru',
            'ru'     => 'ru',
            'zh-CN'  => 'zh-CHS',
            'zh-CHS' => 'zh-CHS',
            'he'     => 'he',
            'de'     => 'de',
            'fr'     => 'fr',
            'es'     => 'es'
          );
          if ((!isset($langs[$to])) || (!isset($langs[$from]))) {
            //    echo '(2) Error communcating with Bing Translate.';
            throw new CHttpException(500, '(2) Error communcating with Bing Translate.');
          }

          $url = $this->_bingTranslateBaseUrl_azure;
          $url .= "?Text=%27" . urlencode($query) . "%27";
          $url .= "&To=%27" . $langs[$to] . "%27";
          $url .= "&From=%27" . $langs[$from] . "%27";
          $url .= "&\$top=1";
          $url .= "&\$format=json";

          try {
            $ch = self::getCurl(FALSE);
          } catch (Exception $e) {
            throw new CHttpException (500, Yii::t('main', 'System error: curl not installed or inited.'));
          }

//--------
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
          curl_setopt($ch, CURLOPT_USERPWD, "$KeyTranslator->appId:$KeyTranslator->appId"); //$key.':'.$key);

// Get the response from Bing
          try {
            $contents = curl_exec($ch);
            // curl_close($ch);
          } catch (Exception $e) {
            // echo $e->getMessage();
            if (class_exists('Profiler', FALSE)) {
              Profiler::message('BingV2', 'error curl');
            }
            if (class_exists('LogSiteErrors', FALSE)) {
              LogSiteErrors::logError('BingV2', 'Curl error: BingTranslator.php line 216');
            }
            //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
            return FALSE;
          }

//            }
//      $contents = $this->_remoteQuery($url);
          if (!empty($contents) && ($contents != 'Parameter: To has an invalid pattern of characters')) //
          {
            $jsonData = json_decode($contents);
            if (!(isset($jsonData->d->results[0]->Text))) {
              $this->_success = FALSE;
              $this->logTranslation($KeyTranslator->appId, $url, FALSE);
              self::$_keysExpired = TRUE;
              if (class_exists('Profiler', FALSE)) {
                Profiler::message('BingV2', 'Bing error or key expired!');
              }
              return FALSE;
            }
            else {
              if ($plainRes) {
                $this->_translatedText = $contents;
                $this->_success = TRUE;
                $this->logTranslation($KeyTranslator->appId, $url, TRUE);
                return $this->_translatedText;
              }
              $this->_translatedText = (string) $jsonData->d->results[0]->Text;
              $this->_success = TRUE;
              $this->logTranslation($KeyTranslator->appId, $url, TRUE);
              //saveTranslation($srcLang, $textCn, $textRu, $textEn)
              $textCn='';
              $textRu='';
              $textEn='';
              if (in_array($from,array('zh-CHS','zh-CN','zh'))){
                $textCn=$query;
              } elseif (in_array($from,array('ru'))) {
                $textRu=$query;
              } else {
                $textEn=$query;
              }
              if (in_array($to,array('zh-CHS','zh-CN','zh'))){
                $textCn=$this->_translatedText;
              } elseif (in_array($to,array('ru'))) {
                $textRu=$this->_translatedText;
              } else {
                $textEn=$this->_translatedText;
              }
              $this->saveTranslation($from, $textCn, $textRu, $textEn);
              return $this->_translatedText;
            }
          }
          else {
//              echo ('To has an invalid pattern of characters.');
            throw new CHttpException(500, '(3) Error communcating with Bing Translate. To has an invalid pattern of characters.');
          }
//====================================================================
        }
        else {
          if (class_exists('Profiler', FALSE)) {
            Profiler::message('Bing', 'error');
          }
          return FALSE;
        }

      } catch (Exception $e) {
        if (class_exists('Profiler', FALSE)) {
          Profiler::message('Bing', 'error');
        }
        //echo $e->getMessage();
        if (class_exists('LogSiteErrors', FALSE)) {
          LogSiteErrors::logError('Bing', 'Error: BingTranslator.php line 256');
        }
        //logError($error_label,$error_message=false,$error_description=false,$custom_data=false)
        return FALSE;
      }
    }
  }

  private static function getKeyTranslator() {
    //TODO Поменять на ивент снимающий бан раз в месяц 1-го числа
/*    if (((int) date('j') == 1) && (((int) date('i')) / 2 == intval(((int) date('i')) / 2))) {
      $command = Yii::app()->db->createCommand("update LOW_PRIORITY translator_keys tk
set tk.banned=0
where DATE_FORMAT(NOW() ,'%Y-%m-01')=DATE_FORMAT(CURDATE(),'%Y-%m-%d')
and tk.banned_date<CAST(DATE_ADD(NOW(), INTERVAL -10 MINUTE) AS DATETIME)");
      $command->execute();
    }
*/
    //TODO Бан ключа, если за последние 10 мин. (м.б 10 раз подряд) ключ выдал некорректные результаты
/*    if (((int) date('i')) / 2 == intval(((int) date('i')) / 2)) {
      $command = Yii::app()->db->createCommand("update LOW_PRIORITY translator_keys tk
set tk.banned=1,
tk.banned_date=CAST(NOW() AS DATETIME)
where (select count(0) from log_translator_keys lt where lt.keyid=tk.id and tk.banned<>1
and lt.`date`>CAST(DATE_ADD(NOW(), INTERVAL -15 MINUTE) AS DATETIME)
and lt.result=0)>20");
      $command->execute();
    }
*/
    //TODO Сделать равномерную выборку ключей (варианты - счетчик или лог)
    $command = Yii::app()->db->createCommand("SELECT tk1.id, tk1.`key`,tk1.type FROM translator_keys tk1
WHERE tk1.id > (SELECT FLOOR( MAX(tk2.id) * RAND()) FROM translator_keys tk2 where tk2.enabled=1 and tk2.banned=0)
 and tk1.enabled=1 and tk1.banned=0
ORDER BY tk1.id LIMIT 1");
    $row = $command->queryRow();
    if ($row != FALSE) {
      $key = new stdClass();
      $key->type = $row['type'];
      $key->appId = $row['key'];
    }
    else {
      $key = FALSE;
    }
    return $key;
  }

  private function saveTranslation($srcLang, $textCn, $textRu, $textEn) {
    $found = TRUE;
    $res = 0;
    $textCn = trim($textCn);
    $textRu = trim($textRu);
    $textEn = trim($textEn);

    if (in_array($srcLang, array('en-US', 'en'))) {
      if ((($textEn == $textRu) || ($textEn == $textCn)) || (($textRu == '') && ($textCn == ''))) {
        return $res;
      }
      $command = Yii::app()->db->createCommand("select TEXT_ZH as zh, TEXT_RU as ru, TEXT_EN as en from cn_dic where TEXT_EN=:text LIMIT 1")
        ->bindParam(':text', $textEn, PDO::PARAM_STR);
      $row = $command->queryRow();
      if ($row != FALSE) {
        $command = Yii::app()->db->createCommand("update LOW_PRIORITY cn_dic set FREQ=FREQ+1,
        TEXT_RU = IF((:ru is null) or (trim(:ru)=''),TEXT_RU,:ru),
        TEXT_ZH = IF((:zh is null) or (trim(:zh)=''),TEXT_ZH,:zh),
        TEXT_RU_LENGTH = CHAR_LENGTH(IF((:ru is null) or (trim(:ru)=''),TEXT_RU,:ru)),
        TEXT_ZH_LENGTH = CHAR_LENGTH(IF((:zh is null) or (trim(:zh)=''),TEXT_ZH,:zh))
         where TEXT_EN=:text")
          ->bindParam(':text', $textEn, PDO::PARAM_STR)
          ->bindParam(':ru', $textRu, PDO::PARAM_STR)
          ->bindParam(':zh', $textCn, PDO::PARAM_STR);
        $command->execute();
        $found = TRUE;
      }
      else {
        $found = FALSE;
      }
    }
    elseif (in_array($srcLang, array('ru-RU', 'ru'))) {
      if ((($textRu == $textEn) || ($textRu == $textCn)) || (($textEn == '') && ($textCn == ''))) {
        return $res;
      }
      $command = Yii::app()->db->createCommand("select TEXT_ZH as zh, TEXT_RU as ru, TEXT_EN as en from cn_dic where TEXT_RU=:text LIMIT 1")
        ->bindParam(':text', $textRu, PDO::PARAM_STR);
      $row = $command->queryRow();
      if ($row != FALSE) {
        $command = Yii::app()->db->createCommand("update LOW_PRIORITY cn_dic set FREQ=FREQ+1,
        TEXT_EN = IF((:en is null) or (trim(:en)=''),TEXT_EN,:en),
        TEXT_ZH = IF((:zh is null) or (trim(:zh)=''),TEXT_ZH,:zh),
        TEXT_EN_LENGTH = CHAR_LENGTH(IF((:en is null) or (trim(:en)=''),TEXT_EN,:en)),
        TEXT_ZH_LENGTH = CHAR_LENGTH(IF((:zh is null) or (trim(:zh)=''),TEXT_ZH,:zh))
         where TEXT_RU=:text")
          ->bindParam(':text', $textRu, PDO::PARAM_STR)
          ->bindParam(':en', $textEn, PDO::PARAM_STR)
          ->bindParam(':zh', $textCn, PDO::PARAM_STR);
        $command->execute();
        $found = TRUE;
      }
      else {
        $found = FALSE;
      }
    }
    else {
      if ((($textCn == $textEn) || ($textCn == $textRu)) || (($textEn == '') && ($textRu == ''))) {
        return $res;
      }
      $command = Yii::app()->db->createCommand("select TEXT_ZH as zh, TEXT_RU as ru, TEXT_EN as en from cn_dic where TEXT_ZH_LENGTH=CHAR_LENGTH(:text) and TEXT_ZH=:text LIMIT 1")
        ->bindParam(':text', $textCn, PDO::PARAM_STR);
      $row = $command->queryRow();
      if ($row != FALSE) {
        $command = Yii::app()->db->createCommand("update LOW_PRIORITY cn_dic set FREQ=FREQ+1,
        TEXT_EN = IF((:en is null) or (trim(:en)=''),TEXT_EN,:en),
        TEXT_RU = IF((:ru is null) or (trim(:ru)=''),TEXT_RU,:ru),
        TEXT_EN_LENGTH = CHAR_LENGTH(IF((:en is null) or (trim(:en)=''),TEXT_EN,:en)),
        TEXT_RU_LENGTH = CHAR_LENGTH(IF((:ru is null) or (trim(:ru)=''),TEXT_RU,:ru))
        where TEXT_ZH=:text")
          ->bindParam(':text', $textCn, PDO::PARAM_STR)
          ->bindParam(':en', $textEn, PDO::PARAM_STR)
          ->bindParam(':ru', $textRu, PDO::PARAM_STR);
        $command->execute();
        $found = TRUE;
      }
      else {
        $found = FALSE;
      }
    }
    if (!$found) {
      $FREQ = 1;
      $TRANSLATION_STATE = 0; //insert delayed
      $command = Yii::app()->db->createCommand("insert ignore into cn_dic (SRC_LANG,FREQ,
        TRANSLATION_STATE,TEXT_ZH,TEXT_RU,TEXT_EN,TEXT_RU_LENGTH,TEXT_EN_LENGTH,TEXT_ZH_LENGTH)
        values (:SRC_LANG,:FREQ,
        :TRANSLATION_STATE,:TEXT_ZH,:TEXT_RU,:TEXT_EN,CHAR_LENGTH(:TEXT_RU),CHAR_LENGTH(:TEXT_EN),CHAR_LENGTH(:TEXT_ZH))")
        ->bindParam(':SRC_LANG', $srcLang, PDO::PARAM_STR)
        ->bindParam(':FREQ', $FREQ, PDO::PARAM_INT)
        ->bindParam(':TRANSLATION_STATE', $TRANSLATION_STATE, PDO::PARAM_INT)
        ->bindParam(':TEXT_ZH', $textCn, PDO::PARAM_STR)
        ->bindParam(':TEXT_RU', $textRu, PDO::PARAM_STR)
        ->bindParam(':TEXT_EN', $textEn, PDO::PARAM_STR);
      $command->execute();
      $res = Yii::app()->db->getLastInsertID();
        if (!$res) {
            $res=Yii::app()->db->createCommand("select ifnull(id,0) from cn_dic
                  where SRC_LANG=:SRC_LANG and TEXT_ZH_LENGTH=CHAR_LENGTH(:TEXT_ZH) and TEXT_ZH=:TEXT_ZH and TEXT_RU=:TEXT_RU and TEXT_EN=:TEXT_EN")
              ->bindParam(':SRC_LANG', $srcLang, PDO::PARAM_STR)
              ->bindParam(':TEXT_ZH', $textCn, PDO::PARAM_STR)
              ->bindParam(':TEXT_RU', $textRu, PDO::PARAM_STR)
              ->bindParam(':TEXT_EN', $textEn, PDO::PARAM_STR);
        }
    }
    return $res;
  }
}