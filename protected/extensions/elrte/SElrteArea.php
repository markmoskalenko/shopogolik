<?php

class SElrteArea extends CInputWidget
{
	static $initialized=false;

	/**
	 * Initialize component
	 */
	public function init()
	{
		if(self::$initialized===false)
		{
			self::$initialized=true;
			$assetsUrl = Yii::app()->getAssetManager()->publish(
				Yii::getPathOfAlias('ext.elrte.lib'),
				false,
				-1,
				YII_DEBUG
			);

			$cs=Yii::app()->clientScript;
			$build=(YII_DEBUG)?'full':'min';
			// Elrte
			$cs->registerCssFile($assetsUrl.'/elrte/css/elrte.'.$build.'.css');
			$cs->registerScriptFile($assetsUrl.'/elrte/js/elrte.'.$build.'.js');
			$cs->registerScriptFile($assetsUrl.'/elrte/js/i18n/elrte.ru.js');
			// Elfinder
			$cs->registerCssFile($assetsUrl.'/elfinder/css/elfinder.'.$build.'.css');
			$cs->registerCssFile($assetsUrl.'/elfinder/css/theme.css');
			$cs->registerScriptFile($assetsUrl.'/elfinder/js/elfinder.'.$build.'.js');
			$cs->registerScriptFile($assetsUrl.'/elfinder/js/i18n/elfinder.ru.js');

			$cs->registerScriptFile($assetsUrl.'/helper.js');
		}
	
		parent::init();
	}

	public function run($forceWYSIWYG=false)
	{
    //  echo '* не забудьте нажать "дискетку" для сохранения изменений<br/>';
//		$theme = Yii::app()->settings->get('core', 'editorTheme');
//		if(!$theme)
			$theme = 'yii';
//		$height = Yii::app()->settings->get('core', 'editorHeight');
//		if($height < 50)
//      if (mb_strlen($searchitem, 'UTF-8'); $this->value
			$height = 'auto';
      $this->htmlOptions['height']='auto';
      $this->htmlOptions['width']='100%';
      if (isset($this->model->id)) {
      $this->htmlOptions['id']=$this->attribute.$this->model->id;
      }
	  list($name, $id) = $this->resolveNameID();
//$id='as1';$name='as1';

      $useRTF=false;
      $useTextField=false;
		if($this->hasModel()) {
          $useRTF=strpos($this->model[$this->attribute],'?xml')<=0;
          if (!$forceWYSIWYG) {
            $useTextField=(mb_strlen($this->model[$this->attribute], 'UTF-8')<=256);
          }
          if (!$useTextField) {
            if (!$useRTF) {
              $this->htmlOptions['cols']='140';
              $this->htmlOptions['rows']='25';
            }
			echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
          } else {
            echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
          }
        } else {
          $useRTF=strpos($this->value,'?xml')<=0;
          $useTextField=(mb_strlen($this->value, 'UTF-8')<=256);
          if (!$useRTF) {
            $this->htmlOptions['cols']='140';
            $this->htmlOptions['rows']='25';
          }
          if (!$useTextField) {
			echo CHtml::textArea($name, $this->value, $this->htmlOptions);
          } else {
            echo CHtml::textField($name, $this->value, $this->htmlOptions);
          }
        }
//      if (isset($this->model->id)) {
//		  $id=$id.$this->model->id;
//      }
//		if(Yii::app()->settings->get('core', 'editorAutoload'))
//	Yii::app()->clientScript->registerScript(__CLASS__.$id,'setupElrteEditor(\''.$id.'\', this, \''.$theme.'\', \''.$height.'\');', CClientScript::POS_READY);
//		else
      if ($useRTF &&(!$useTextField)) {
      Yii::app()->clientScript->registerScript(__CLASS__.$id,'setupElrteEditor(\''.$id.'\', this, \''.$theme.'\', \''.$height.'\');', CClientScript::POS_READY);
      }
	//		echo '<div class="hint"><a onclick="return setupElrteEditor(\''.$id.'\', this, \''.$theme.'\', \''.$height.'\');">WYSIWYG</a></div>';
	}

}
