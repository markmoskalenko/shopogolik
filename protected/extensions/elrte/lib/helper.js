
function setupElrteEditor(id, el_clicked, theme, height)
{
	$(el_clicked).hide();

	var lang = 'ru';
    var dialog;
	var opts = {
		lang         : lang, 
		styleWithCSS : false,
		height       : height,
		toolbar      : theme,
        absoluteURLs : true,
        allowSource : true,
//        resizable : false,
        fmAllow : true,
/*		fmOpen : function(callback) {
			$('<div />').dialogelfinder({
				url: 'admin/filemanager/index',
				lang: lang,
				commandsOptions: {
					getfile: {
						oncomplete: 'destroy'
					}
				},
				getFileCallback: callback
			});
		}
*/
        fmOpen : function(callback) {

            if (!dialog) {
                // create new elFinder
                dialog = $('<div />').dialogelfinder({
                    url : '/admin/fileman/index',
                    lang : 'ru',
                    commandsOptions : {
                        getfile : {
                            onlyURL  : true,
                            // allow to return multiple files info
                            multiple : false,
                            // allow to return filers info
                            folders  : false,
                            // action after callback (""/"close"/"destroy")
                            oncomplete : 'close'
                        }
                    },
                    getFileCallback : callback // передаем callback файловому менеджеру
                })
            } else {
                dialog.dialogelfinder('open')
            }
        }

	};

	//$('#'+id).elrte(opts);
    var editor = new elRTE(document.getElementById(id), opts);
    editor.tabsbar.children('.tab').click();
	return false;

}
