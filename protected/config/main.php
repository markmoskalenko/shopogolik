<?/*******************************************************************************************************************
* This file is the part of "DropShop" taobao(c) showcase project http://dropshop.pro
* Copyright (C) 2013 - 2014 DanVit Labs http://danvit.net
* All rights reserved and protected by law. Certificate #40514-UA 21.12.2013
* You can't use this file without of the author's permission.
* ====================================================================================================================
* <description file="main.php">
* </description>
**********************************************************************************************************************/?>
<?php
$db = require(__DIR__ . '/db.php');
Yii::app()->setId('0668553454'); //894a6c9c
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');
Yii::setPathOfAlias('chartjs', dirname(__FILE__) . '/../extensions/yii-chartjs');
Yii::setPathOfAlias('postcalc', dirname(__FILE__) . '/../extensions/PostcalcLight');
return array(
  'basePath' => dirname(__FILE__) . '/..',
  'name' => 'Шопоголик24',
  'theme' => '', // Для указания темы(шаблона) используйте параметр config.site_front_theme в БД
  'sourceLanguage'=>'ru',
  'language' => 'ru',
  'charset' => 'UTF-8',
  // autoloading model and component classes
  'import' => array(
    'application.controllers.custom.*',
    'application.models.custom.*',
    'application.models.*',
    'application.components.*',
  ),
  'preload' => array(
    'log',
    'bootstrap',
    'chartjs',
  ),
  'components' => array(
    'messages' => array(
      'class' => 'DsDbMessageSource',
      'sourceMessageTable'=> 't_source_message', // по умолчанию 'SourceMessage'
      'translatedMessageTable'=>'t_message', // по умолчанию 'Message'
      'cachingDuration'=>YII_DEBUG?0:3600, // кэширование
      'onMissingTranslation' => array('TranslationEventHandler', 'handleMissingTranslation'),
    ),
    'widgetFactory' => array(
      'widgets' => array(
        'CGridView' => array(
          'cssFile' => FALSE,
          'pager' => array('cssFile' => FALSE),
        ),
        'CListView' => array(
          'cssFile' => FALSE,
          'pager' => array('cssFile' => FALSE),
        ),
        'CTreeView' => array(
          'cssFile' => FALSE,
        ),
        'CDetailView' => array(
          'cssFile' => FALSE,
          //'pager'=>array('cssFile' => false),
        ),
        'CLinkPager' => array(
          //               'cssFile'=>false,

          'nextPageLabel' => '>',
          'prevPageLabel' => '<',
          'header' => '',
        ),

      ),
    ),
    'log' => array(
      'class' => 'CLogRouter',
      'routes' => array(
        array(
          'class' => 'CFileLogRoute',
          'enabled' => YII_DEBUG,
          'levels' => 'error, warning',
          //'levels'=>'error, warning, trace, profile, info',
        ),
        array(
          'class' => 'CProfileLogRoute',//'CWebLogRoute',
          //'enabled' => FALSE,
          'enabled' =>YII_PROFILING,
          'levels' => 'error, warning, profile',
          'showInFireBug' => true,
          //'levels'=>'error, warning, trace, profile, info',
        ),
      ),
    ),
    'db' => $db,
    'errorHandler' => array(
      'errorAction' => '/site/error',
    ),
    'user' => array(
      'allowAutoLogin' => TRUE,
      'loginUrl' => '/user/login',
      'class' => 'WebUser',
    ),
    'request'=>array(
     // 'enableCsrfValidation'=>true,
     // 'enableCookieValidation'=>true,
    ),
    'urlManager' => array(
      'class' => 'CUrlManager',
      'urlFormat' => 'path',
      'showScriptName' => FALSE,
      'rules' => array(
        array((file_exists('webroot'.'/sitemap.xml'))?'/sitemap.xml':'site/sitemap', 'pattern' => 'sitemap.xml', 'urlSuffix' => ''),
        array(
          'class' => 'application.components.langUrlRule',
        )
      )
    ),
    'clientScript' => array(
      'class' => 'application.extensions.NLSClientScript.NLSClientScript',
      //'excludePattern' => '/\.tpl/i', //js regexp, files with matching paths won't be filtered is set to other than 'null'
      //'includePattern' => '/\.php/', //js regexp, only files with matching paths will be filtered if set to other than 'null'

      'mergeJs' => FALSE,
      //true, //def:true
      'compressMergedJs' => FALSE,
      //def:false

      'mergeCss' => FALSE,
      //def:true
      'compressMergedCss' => FALSE,
      //def:false

      'mergeJsExcludePattern' => '/edit_area/',
      //won't merge js files with matching names

      'mergeIfXhr' => FALSE,
      //true, //def:false, if true->attempts to merge the js files even if the request was xhr (if all other merging conditions are satisfied)

      //'serverBaseUrl' => 'http://localhost', //can be optionally set here
      'mergeAbove' => 1,
      //def:1, only "more than this value" files will be merged,
      'curlTimeOut' => 30,
      //def:10, see curl_setopt() doc
      'curlConnectionTimeOut' => 30,
      //def:10, see curl_setopt() doc

      'appVersion' => 2.0
      //if set, it will be appended to the urls of the merged scripts/css
    ),
// LESS compiler support
    'assetManager'=>array(
      'class'=>'application.extensions.EAssetManager',
      'lessCompile'=>true,
      'lessCompiledPath'=>null,//'webroot.assets'
      'lessFormatter'=>(YII_DEBUG ? 'classic' : 'compressed'),
      'lessForceCompile'=>YII_DEBUG,
    ),

//=======================
    'DanVitTranslator' => array(
      'class' => 'application.extensions.DanVitTranslator.DanVitTranslator',
    ),
    'BingTranslator' => array(
      'class' => 'application.extensions.BingTranslator.BingTranslator',
    ),
    'authManager' => array(
      'class' => 'DVAuthManager',
      'defaultRoles' => array('guest'),
      'showErrors' => YII_DEBUG,
    ),
    'cache' => array(
      'class' => 'system.caching.CDbCache',
      'cacheTableName' => 'cache',
      'autoCreateCacheTable' => FALSE,
      'connectionID' => 'db',
      'gCProbability' => 50000,
    ),

/*    'cache' => array(
      'class' => 'system.caching.CApcCache',
    ),
*/
    'fileCache' => array(
      'class' => 'system.caching.CFileCache',
      'cachePath' => 'application.cache',
      'directoryLevel' => 1,
      'gCProbability' => 50000
    ),
/*    'memCache'=>array(
      'class'=>'CMemCache',
      'servers'=>array(
        array(
          'host'=>'127.0.0.1',
          'port'=>11211,
          'persistent'=>true,
        ),
      ),
    ),
*/
//=============================================
    'bootstrap' => array(
      'class' => 'bootstrap.components.Bootstrap',
//              'class'=>'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
//              'coreCss'=>true, // whether to register the Bootstrap core CSS (bootstrap.min.css)
//              'responsiveCss'=>false, // whether to register the Bootstrap responsive CSS (bootstrap-responsive.min.css)
      'plugins' => array(
        // Optionally you can configure the "global" plugins (button, popover, tooltip and transition)
        // To prevent a plugin from being loaded set it to false as demonstrated below
//                'transition'=>false, // disable CSS transitions
        'popover' => array(
          'options' => array(
            'placement' => 'right',
          ),
        ),
        'tooltip' => array(
//                  'selector'=>'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
          'options' => array(
            'placement' => 'right', // place the tooltips below instead
          ),
        ),
      ),
    ),
    'chartjs' => array('class' => 'chartjs.components.ChartJs'),
  ),
  'modules' => array(
    'admin',
    'cabinet'
  ),
);